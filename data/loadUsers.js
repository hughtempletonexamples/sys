/* eslint angular/log: 0, angular/timeout-service: 0*/

"use strict";

var RSVP = require("rsvp");
var firebaseUtil = require("./firebaseUtil");
var userData = require("./users.json");

var firebaseName;
var adminApp;
var clientApp;
var usersRef;
var users;

firebaseUtil.firebaseName()
  .then(setFirebaseName)
  .then(getUsers)
  .then(authenticate)
  .then(getExistingUsers)
  .then(getExistingUserEmails)
  .then(createUsers)
  .then(updateUserRecords)
  .then(reportSuccess)
  .catch(reportFailure);


function setFirebaseName(name) {
  firebaseName = name;
}

function getUsers() {
  users = userData[firebaseName] || defaultUsers();

  users.forEach(addUserFields);

  function defaultUsers() {
    var users = [];

    var email = firebaseUtil.userEmail();
    if (email) {
      users.push({email: email});
    }
    users.push(
      {email: "operations@example.com", roles: ["operations", "performance"], phone: ""},
      {email: "production@example.com", roles: ["production", "designer"], phone: ""},
      {email: "performance@example.com", roles: ["performance"], phone: ""},
      {email: "delivery-manager@example.com", roles: ["delivery-manager", "performance"], phone: ""}
    );
    return users;
  }

  function addUserFields(user) {
    if (!user.password) {
      user.password = "password";
    }
    if (!user.name) {
      var name = user.email.match(/^([^@]*)/)[0];
      name = name.replace(/(\.|\b[/*a-z])/g, function (match) {
        return match !== "." ? match.toUpperCase() : " ";
      });
      user.name = name;
    }
    if (!user.phone) {
      var phone = "";
      user.phone = phone;
    }
    if (!user.roles) {
      user.roles = [
        "operations", 
        "production", 
        "delivery-manager", 
        "designer", 
        "performance", 
        "site-ops", 
        "quantity-surveyor", 
        "engineer",
        "team",   
        "installer"
      ];
    }
  }
}

function authenticate(token) {
  adminApp = firebaseUtil.serverAuthentication(firebaseName);
  clientApp = firebaseUtil.webAuthentication(firebaseName);

  usersRef = adminApp.database().ref("users");
}

function getExistingUsers() {
  return usersRef.once("value");
}

function getExistingUserEmails(usersSnapshot) {
  var userMap = {};
  usersSnapshot.forEach(function (user) {
    userMap[user.val().email] = user.key;
  });
  return userMap;
}

function createUsers(userMap) {
  return createNewUsers()
    .then(function () {
      return userMap;
    });

  function createNewUsers() {
    return RSVP.all(users.filter(shouldCreate).map(sequentialCreateUser));
  }

  function shouldCreate(user) {
    return !userMap[user.email];
  }

  function sequentialCreateUser(user, index) {
    var result = RSVP.defer();

    setTimeout(timerCallback, 500 * index);

    return result.promise;

    function timerCallback() {
      createUser(user)
        .then(result.resolve)
        .catch(result.reject);
    }

  }

  function createUser(user) {
    console.log("createUser: " + user.email);
    return clientApp.auth().createUserWithEmailAndPassword(user.email, user.password)
      .then(function (userData) {
        userMap[user.email] = userData.uid;
      })
      .catch(function (error) {
        var message = error;
        if (error.code !== "AUTHENTICATION_DISABLED") {
          message = error + " [" + user.email + "]";
        }
        return RSVP.reject(message);
      });
  }
}

function updateUserRecords(userMap) {
  return RSVP.all(users.map(updateUserRecord));

  function updateUserRecord(user) {
    var userId = userMap[user.email];
    var userRecord = {
      email: user.email,
      name: user.name,
      phone: user.phone,
      roles: asMap(user.roles)
    };

    return usersRef.child(userId)
      .set(userRecord)
      .then(function () {
        return RSVP.resolve("Email: " + user.email + ", Name: " + user.name + ", Roles: " + user.roles + ", Phone: " + user.phone);
      });

    function asMap(roles) {
      var map = {};
      roles.forEach(function (role) {
        map[role] = true;
      });
      return map;
    }
  }
}

function reportSuccess(results) {
  results.forEach(function (result) {
    console.log(result);
  });
  console.error("Success: " + usersRef.toString());
  process.exit(0);
}

function reportFailure(error) {
  console.error(error);
  process.exit(1);
}
