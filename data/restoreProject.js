/* eslint angular/log: 0, angular/json-functions: 0 */

"use strict";

var fs = require("fs");
var RSVP = require("rsvp");
var firebaseUtil = require("./firebaseUtil");

var firebaseName;
var firebaseRef;

var projectId = process.argv[2];
var projectArchive;

firebaseUtil.firebaseName()
  .then(setFirebaseName)
  .then(authenticate)
  .then(readArchive)
  .then(addProjects)
  .then(addAreas)
  .then(addRevisions)
  .then(addPanels)
  .then(addProductionPlans)
  .then(addAuditEntries)
  .then(reportSuccess)
  .catch(reportFailure);

function setFirebaseName(name) {
  firebaseName = name;
}

function authenticate(token) {
  firebaseRef = firebaseUtil.serverAuthentication(firebaseName).database().ref();
}

function readArchive() {
  var result = RSVP.defer();
  fs.readFile("project-" + projectId + ".json", callback);

  return result.promise;

  function callback(error, data) {
    if (error) {
      result.reject(error);
    }
    else {
      projectArchive = JSON.parse(data);
      result.resolve();
    }
  }
}

function addProjects() {
  return addItems("projects");
}

function addAreas() {
  return addItems("areas");
}

function addRevisions() {
  return addItems("revisions");
}

function addPanels() {
  return addItems("panels");
}

function addProductionPlans() {
  return addItems("productionPlans/areas");
}

function addAuditEntries() {
  return addItems("auditEntries");
}

function reportSuccess() {
  console.error("Success: " + firebaseRef.toString());
  process.exit(0);
}

function reportFailure(error) {
  console.error(error);
  process.exit(1);
}

function addItems(rootKey) {
  var archiveItems = getArchiveItems();

  return RSVP.all(Object.keys(archiveItems).map(itemAdded));

  function itemAdded(key) {
    var item = archiveItems[key];
    return firebaseRef.child(rootKey).child(key).set(item);
  }

  function getArchiveItems() {
    return rootKey.split("/").reduce(deref, projectArchive);

    function deref(object, property) {
      return object[property] || {};
    }
  }
}
