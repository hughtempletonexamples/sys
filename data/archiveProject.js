/* eslint angular/log: 0, angular/json-functions: 0 */

"use strict";

var fs = require("fs");
var RSVP = require("rsvp");
var firebaseUtil = require("./firebaseUtil");
var request = require("request");

var firebaseName;
var adminApp;
var firebaseRef;

var projectId = process.argv[2];
var deleteData = process.argv[3] == "delete";
var databaseSecret;

var projectKey;
var projectArchive = {
  projects: {},
  areas: {},
  revisions: {},
  panels: {},
  auditEntries: {},
  productionPlans: {
    areas: {}
  }
};

var condemned = [];

var authToken;

firebaseUtil.firebaseName()
  .then(setFirebaseName)
  .then(checkDatabaseSecret)
  .then(authenticate)
  .then(getProject)
  .then(getProjectKey)
  .then(removeAreas)
  .then(removeRevisions)
  .then(removePanels)
  .then(removeProductionPlans)
  .then(removeAuditEntries)
  .then(writeArchive)
  .then(deleteCondemned)
  .then(reportSuccess)
  .catch(reportFailure);

function checkDatabaseSecret() {
  databaseSecret = process.env.INNVIEW_DATABASE_SECRET;

  if (databaseSecret == null) {
    reportFailure("You must specify INNVIEW_DATABASE_SECRET, containing a database secret obtained from the firebase console.");
  }
}

function setFirebaseName(name) {
  firebaseName = name;
}

function authenticate(token) {
  adminApp = firebaseUtil.serverAuthentication(firebaseName);

  firebaseRef = adminApp.database().ref();
}

function getProject() {
  var project = firebaseRef.child("projects").orderByChild("id").equalTo(projectId);

  return project.once("value");
}

function getProjectKey(projectsSnapshot) {
  projectsSnapshot.forEach(function (child) {
    projectKey = child.key;
    projectArchive.projects[child.key] = child.val();
    condemned.push(child.ref);
  });

  if (projectsSnapshot.numChildren() != 1) {
    return RSVP.reject("Project not found");
  }
  else {
    return RSVP.resolve();
  }
}

function removeAreas() {
  return removeItems("areas");
}

function removeRevisions() {
  return removeItems("revisions");
}

function removePanels() {
  return removeBigItems("panels");
}

function removeProductionPlans() {
  return removeItems("productionPlans/areas");
}

function removeAuditEntries() {
  return removeItems("auditEntries");
}

function writeArchive() {
  var result = RSVP.defer();
  fs.writeFile("project-" + projectId + ".json", JSON.stringify(projectArchive, null, "  "), callback);

  return result.promise;

  function callback(error) {
    if (error) {
      result.reject(error);
    }
    else {
      result.resolve();
    }
  }
}

function deleteCondemned() {
  if (deleteData) {
    return RSVP.all(condemned.map(remove));
  }

  function remove(ref) {
    return ref.remove();
  }
}

function reportSuccess() {
  console.error("Success: " + firebaseRef.toString());
  process.exit(0);
}

function reportFailure(error) {
  console.error(error);
  process.exit(1);
}

// Horrible hacky version of removeItems to cope with large number of panels.
// Without this, removeItems fails with TOO_BIG error from firebase.
function removeBigItems(rootKey) {

  var result = RSVP.defer();

  var url = "https://" + firebaseName + ".firebaseio.com/" + rootKey + ".json?auth=" + databaseSecret;

  var archiveItems = getArchiveItems();

  request({
    url: url,
    lengthjson: true
  }, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      var allItems = JSON.parse(body);

      console.log("Parsed response.");

      for (var itemKey in allItems) {
        var item = allItems[itemKey];

        if(projectKey === item.project) {
          archiveItems[itemKey] = item;
          condemned.push(firebaseRef.child(rootKey).child(itemKey));
        }
      }

      result.resolve();
    } else {
      console.error("Error: ", error);
      result.reject(error);
    }
  });

  return result.promise;

  function getArchiveItems() {
    return rootKey.split("/").reduce(deref, projectArchive);

    function deref(object, property) {
      return object[property];
    }
  }
}

function removeItems(rootKey) {
  var archiveItems = getArchiveItems();

  var itemsQuery = firebaseRef.child(rootKey).orderByChild("project").equalTo(projectKey);

  return itemsQuery.once("value")
    .then(itemsLoaded);

  function itemsLoaded(items) {
    items.forEach(function (item) {
      archiveItems[item.key] = item.val();
      condemned.push(item.ref);
    });
  }

  function getArchiveItems() {
    return rootKey.split("/").reduce(deref, projectArchive);

    function deref(object, property) {
      return object[property];
    }
  }
}
