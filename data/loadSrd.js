/* eslint angular/log: 0 */

"use strict";

var RSVP = require("rsvp");
var firebaseUtil = require("./firebaseUtil");
var standardReferenceData = require("./srd.json");

var firebaseName;
var srdRef;

firebaseUtil.firebaseName()
  .then(setFirebaseName)
  .then(authenticate)
  .then(dataSet)
  .then(reportSuccess)
  .catch(reportFailure);

function setFirebaseName(name) {
  firebaseName = name;
}

function authenticate(token) {
  srdRef = firebaseUtil.serverAuthentication(firebaseName).database().ref("srd");
}

function dataSet() {
  return srdRef.set(standardReferenceData);
}

function reportSuccess() {
  console.error("Success: " + srdRef.toString());
  process.exit(0);
}

function reportFailure(error) {
  console.error(error);
  process.exit(1);
}
