/* eslint angular/log: 0 */

"use strict";

var firebase = require("firebase");

// Rummages into firebase-tools internals to leverage authentication
var configstore = require("firebase-tools/lib/configstore");
var requireAuth = require("firebase-tools/lib/requireAccess.js");

var RSVP = require("rsvp");

module.exports.firebaseName = firebaseName;
module.exports.authToken = authToken;
module.exports.userEmail = userEmail;
module.exports.webAuthentication = webAuthentication;
module.exports.serverAuthentication = serverAuthentication;

function firebaseName() {
  var instanceName = process.env.INNVIEW_FIREBASE;
  if (instanceName) {
    // production uses a different naming scheme.
    if ("firebase-innview" === instanceName) {
      instanceName = "innview";
    }
    return RSVP.resolve(instanceName);
  }
  else {
    return RSVP.reject("Error: Environment variable INNVIEW_FIREBASE must be set");
  }
}

function authToken(firebase) {
  var options = {
    project: firebase,
    token: process.env.INNVIEW_FIREBASE_TOKEN
  };

  return requireAuth(options)
    .then(function () {
      return RSVP.resolve(options.dataToken);
    });
}

function userEmail() {
  var user = configstore.get("user");
  return user ? user.email : undefined;
}

function webAuthentication(instanceName) {
  return firebase.initializeApp({
    apiKey: process.env.INNVIEW_FIREBASE_APIKEY,
    authDomain: instanceName + ".firebaseapp.com",
    databaseURL: "http://" + instanceName + ".firebaseio.com"
  }, "client");
}

function serverAuthentication(instanceName) {
  var serviceAccount = process.env.INNVIEW_FIREBASE_SERVICE
        ? process.env.INNVIEW_FIREBASE_SERVICE
        : __dirname + "/../config/" + instanceName + "_service.json";
  return firebase.initializeApp({
    databaseURL: "http://" + instanceName + ".firebaseio.com",
    serviceAccount: serviceAccount
  }, "admin");
}
