/* eslint angular/log: 0 */

"use strict";

var RSVP = require("rsvp");
var firebaseUtil = require("./firebaseUtil");
var operativesData = require("./operatives.json");

var firebaseName;
var operativesRef;
var operatives;

firebaseUtil.firebaseName()
  .then(setFirebaseName)
  .then(getOperatives)
  .then(authenticate)
  .then(queryExistingOperatives)
  .then(loadExistingOperatives)
  .then(disableAllOperatives)
  .then(activateCurrentOperatives)
  .then(reportSuccess)
  .catch(reportFailure);

function setFirebaseName(name) {
  firebaseName = name;
}

function getOperatives() {
  operatives = operativesData[firebaseName] || operativesData["DEFAULT"];
}

function authenticate(token) {
  operativesRef = firebaseUtil.serverAuthentication(firebaseName).database().ref("operatives");
}

function queryExistingOperatives() {
  return operativesRef.once("value");
}

function loadExistingOperatives(dataSnapshot) {
  var operativesMap = new Map();
  dataSnapshot.forEach(function (user) {
    operativesMap.set(user.val().name, user.key);
  });
  return operativesMap;
}

function disableAllOperatives(operativesMap) {
  var existingKeys = Array.from(operativesMap.values());
  return RSVP.all(existingKeys.map(disableOperative))
    .then(function () {
      return operativesMap;
    });

  function disableOperative(operativeKey) {
    return operativesRef.child(operativeKey)
      .update({active: false});
  }
}

function activateCurrentOperatives(operativesMap) {
  var created = operatives.filter(absent).map(createOperative);
  var activated = operatives.filter(present).map(activateOperativeAndUpdatePin);

  return RSVP.all([].concat(created, activated));

  function present(operative) {
    return operativesMap.has(operative.name);
  }

  function absent(operative) {
    return !present(operative);
  }

  function createOperative(operative) {
    return operativesRef
      .push({name: operative.name, active: true, pin: operative.pin})
      .then(function () {
        return RSVP.resolve("Name: " + operative.name);
      });
  }

  function activateOperativeAndUpdatePin(operative) {
    return operativesRef.child(operativesMap.get(operative.name))
      .update({active: true, pin: operative.pin})
      .then(function () {
        return RSVP.resolve("Name: " + operative.name);
      });
  }
}

function reportSuccess(results) {
  results.forEach(function (result) {
    console.log(result);
  });
  console.error("Success: " + operativesRef.toString());
  process.exit(0);
}

function reportFailure(error) {
  console.error(error);
  process.exit(1);
}
