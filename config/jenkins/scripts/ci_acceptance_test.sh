#!/bin/sh

# This script fetches the ci build image from our docker registry and
# then runs the image to build and test the project.

echo "Ensuring that we are logged into the registry..."
docker login \
       -u "$DOCKER_REGISTRY_USERNAME" \
       -p "$DOCKER_REGISTRY_PASSWORD" \
       https://registry.blackpepper.co.uk/

# Pull the image from the registry
docker pull registry.blackpepper.co.uk/blackpepper/innovare-ci-build

# Install dependencies
docker run \
       --rm \
       -i \
       -v "$PWD":/app \
       -w /app \
       registry.blackpepper.co.uk/blackpepper/innovare-ci-build npm install

# Run the acceptance tests

# The slightly odd volume mapping to /tmp/service.json is because the
# service json file is configured on jenkins as a "secret file" then
# mapped to an environment variable (INNVIEW_FIREBASE_SERVICE), we
# need to mount this in the docker container, then set it's
# INNVIEW_FIREBASE_SERVICE environment variable to point to that
# location.
docker run \
       --rm \
       -i \
       --privileged \
       -v "$PWD":/app \
       -v "$INNVIEW_FIREBASE_SERVICE":/tmp/service.json \
       -w /app \
       -e DISPLAY=:1 \
       -e INNVIEW_FIREBASE=$INNVIEW_FIREBASE \
       -e INNVIEW_FIREBASE_TOKEN=$INNVIEW_FIREBASE_TOKEN \
       -e INNVIEW_FIREBASE_APIKEY=$INNVIEW_FIREBASE_APIKEY \
       -e INNVIEW_FIREBASE_SERVICE="/tmp/service.json" \
       registry.blackpepper.co.uk/blackpepper/innovare-ci-build npm run ci-acceptance
