#!/bin/sh

#
# THIS SCRIPT IS A PLACEHOLDER!
#

# Currently, this script fetches the ci build image from our docker
# registry and then runs the image to deploy to firebase.  It should
# really use a dockerised build, rather than rebuilding the project,
# before deploying it.

docker pull registry.blackpepper.co.uk/blackpepper/innovare-ci-build
docker run \
       --rm \
       -i \
       --privileged \
       -v "$PWD":/app \
       -w /app \
       -e DISPLAY=:1 \
       registry.blackpepper.co.uk/blackpepper/innovare-ci-build npm firebase-dploy
