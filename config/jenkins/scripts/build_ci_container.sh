#!/bin/sh

# This script is responsible for building the docker container that
# can be used to build the application on CI.

IMAGE="blackpepper/innovare-ci-build"
REGISTRY="registry.blackpepper.co.uk"

echo "Ensuring that we are logged into the registry..."
docker login \
       -u "$DOCKER_REGISTRY_USERNAME" \
       -p "$DOCKER_REGISTRY_PASSWORD" \
       https://registry.blackpepper.co.uk/

echo "Building docker image $IMAGE"
docker build --pull -t "$IMAGE" .

echo "Tagging as $REGISTRY/$IMAGE"
docker tag "$IMAGE" "$REGISTRY/$IMAGE"

echo "Pushing $IMAGE to $REGISTRY"
docker push "$REGISTRY/$IMAGE"
