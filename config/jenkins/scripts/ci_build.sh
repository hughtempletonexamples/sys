#!/bin/sh

# This script fetches the ci build image from our docker registry and
# then runs the image to build and test the project.

echo "Ensuring that we are logged into the registry..."
docker login \
       -u "$DOCKER_REGISTRY_USERNAME" \
       -p "$DOCKER_REGISTRY_PASSWORD" \
       https://registry.blackpepper.co.uk/

# Pull the image from the registry
docker pull registry.blackpepper.co.uk/blackpepper/innovare-ci-build

# Install dependencies
docker run \
       --rm \
       -i \
       -v "$PWD":/app \
       -w /app \
       registry.blackpepper.co.uk/blackpepper/innovare-ci-build npm install

# Perform the build
docker run \
       --rm \
       -i \
       --privileged \
       -v "$PWD":/app \
       -w /app \
       -e DISPLAY=:1 \
       -e INNVIEW_FIREBASE=$INNVIEW_FIREBASE \
       registry.blackpepper.co.uk/blackpepper/innovare-ci-build
