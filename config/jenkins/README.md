Authentication
--------------

Log in to Jenkins, and view your user configuration (you can navigate
via your name in the top-right).  Select "Show API Token...".

```
https://leeroy.blackpepper.co.uk/user/first.last/configure
```

Enter your user and API token into `~/.netrc` (file mode 600).

```
machine leeroy.blackpepper.co.uk login first.last password 0dbddb6485ca7ffb1ecc7fe71931fe59
```

Job Configurations
------------------

Job configurations are in the `jobs` sub-directory, with the file name
being `Job Name.xml`.

Import
------

The script `import` will either import/update the specified jobs to
Jenkins, or if `ALL` is specified, it will import/update all jobs which
have a configuration in the `jobs` sub-directory.

Export
------

The script `export` will either export the specified jobs from Jenkins,
or if `ALL` is specified, it will export all of the jobs which have a
configuration in the `jobs` sub-directory.
