Innovaré InnView Google Apps Script API
=======================================

Development
===========

https://developers.googleblog.com/2015/12/advanced-development-process-with-apps.html

Install GApps Node Module
-------------------------

```npm install -g node-google-apps-script```

Setup Google Drive API
--------------------

(Probably need to do this for a production project "innview" and add users to it? so that each person can authenticate, rather than sharing credentials)

1. Navigate to https://console.developers.google.com/
2. If using the old firebase api, then you will need to create a new project. Name it innview-devname, as per your firebase environment.
3. Enable the Google Drive API
4. Navigate to the Credentials view
5. Select ```Create credentials``` and choose ```OAuth Client ID```
6. Press the ```Configure consent screen``` button
7. Make sure your email address is entered and the project name, then save.
8. Select ```Other``` as the application type and name it ```Google Apps Forms``` and click ```Create```
9. Download the credentials as json, using the link on the right of the credentials entry.
10. Authenticate with gapps:
    ```gapps auth ~/Downloads/client_secret_1234567890-abcd.apps.googleusercontent.com.json```

Setup Google Script project
---------------------------

(Probably only need to do this once for production, for a shared script project, then commit the result.)

1. Go to Google Drive and click New > More > Connect more apps.
2. When the "Connect apps to Drive" window appears, type "script" into the search box and press Enter.
3. Click Connect next to the listing for Google Apps Script.
4. Now that you've connected the app, you can create a script by selecting New > More > Google Apps Script.
5. Save the mostly empty project and give it a name.
   Saving is important; the project is not in your Google Drive until it is saved.
6. Get your project ID from the address bar, located after /d/ and before /edit.
   For example, '//script.google.com/a/google.com/d/abc123-xyz098/edit?usp=drive_web'
7. Navigate to a directory where your Apps Script project will live and run gapps init <fileId> within your project directory.
   For example, ```gapps init abc123-xyz098```
   
Don't forget to add the FirebaseApp library dependency when setting up an environment for the innview api. https://sites.google.com/site/scriptsexamples/new-connectors-to-google-services/firebase


Create local config files
-------------------------

Because the gapps tool doesn't allow you to specify a config file, we
need to copy files about using our webpack and npm scripts. 

To enable local development, copy the ```api/env/local.js.orig``` to
```api/env/local.js``` and enter your firebase details. 

Similary copy ```api/env/local.gapps.config.json.orig``` to
```api/env/local.gapps.config.json``` and enter your project id.

Then, when you run ```npm run deploy-api``` it will copy
local.gapps.config.json to gapps.config.json in the project root and
webpack will build the script with the correct firebase url and token
(taken from local.js)
