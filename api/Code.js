"use strict";

var env = require("environment");
//var angular = require("angular");

function _getBase() {
  return FirebaseApp.getDatabaseByUrl(env.url, env.secret);
}

function getSuppliers() {
  return _getBase().getData("/srd/suppliers");
}

function storeFormData(formName, data) {
  storeFormData(formName, "", data);
}

function storeFormData(formName, key, data) {
  _getBase().setData("/googleforms/" + formName + "/" + key, data);
}

function getPanData(fromDate, toDate) {
  
  var operativeNames = {};
  var matchingAreas = {};
  var areaUnpublishedDate = {};
  var productTypeProductionLine = {};
  var matchingPanels = [];
  var date = new Date();
  
  if(fromDate === null && toDate === null){
    fromDate = date.setMonth(date.getMonth()-1);
    toDate = date.setMonth(date.getMonth()+1);
  }
  
  var allAreasDataRequest = [];
  var allPanelsDataRequest = [];
  
  var database = _getBase();
  
  var initData = database.getAllData(["projects", "operatives", "srd/productTypes"]);
  var projects = initData[0];
  var operatives = initData[1];
  var productTypes = initData[2];
  
  for (var productType in productTypes) {
    productTypeProductionLine[productType] = productTypes[productType].productionLine;
  }
  
  for (var operative in operatives) {
    operativeNames[operative] = operatives[operative].name;
  }
  
  for (var projectID in projects) {
    var project = projects[projectID];
    if (angular.isDefined(project.deliverySchedule.published)) {
      var queryData = {orderBy: "project", equalTo: projectID};
      var data = {
        path: "areas",
        optQueryParameters: queryData
      };
      allAreasDataRequest.push(data);
    }
  }
  
  var areasArray = database.getAllData(allAreasDataRequest);
  areasArray.forEach(function ( areas ) {
    findAreasWithMatchingDeliveryDate(areas);
  });
  
  for (var areaId in matchingAreas) {
    var queryData = {orderBy: "area", equalTo: areaId};
    var data = {
      path: "panels",
      optQueryParameters: queryData
    };
    allPanelsDataRequest.push(data);
  }
  
  var panelsArray = database.getAllData(allPanelsDataRequest);
  panelsArray.forEach(function ( panels ) {
    findMatchingPanels(panels);
  });
  
  var values = getValues();
                      
  function findMatchingPanels( panels ) {
    for (var panelID in panels) {
      var panel = panels[panelID];
      panel.deliveryDate = areaUnpublishedDate[panel.area];
      matchingPanels.push(panel);
    }
  }
  
  function findAreasWithMatchingDeliveryDate( areas ) {
    for (var areaID in areas) {
      var area = areas[areaID];
      var project = projects[area.project];
      if (angular.isDefined(area.revisions) && angular.isDefined(area.revisions[project.deliverySchedule.published])) {
        var revisionDate = area.revisions[project.deliverySchedule.published];
        if (revisionDate >= fromDate && revisionDate <= toDate) {
          matchingAreas[areaID] = area;
          areaUnpublishedDate[areaID] = area.revisions[project.deliverySchedule.published];
        }
      }
    }
  }
  
  function getValues() {

    var allPanelData = [];
    var header = ["dateCreated", "project", "panelNumber", "projectName", "jobsheetReference", "phase", "productionLine", "floor", "number", "designHeight", "designLength", "designArea", "designWeight", "designWidth", "designMP", "jobsheets", "dateCompleted", "actualHeight", "actualLength", "actualLRDiag", "actualRLDiag", "operatorSignoff", "supervisorSignoff", "completedStatus", "deliveryDate", "areaType", "panelType", "uploadDate", "buildStartDate", "buildStartTime", "buildFinishDate", "buildFinishTime", "assemblyDuration", "wipDuration", "LayupDuration", "buildDuration", "projectDatum", "line", "operatives", "startedOperatives", "finishAssemblyOperatives", "startLayupOperatives", "completedOperatives"];
    allPanelData.push(header);

    matchingPanels.forEach(function ( panel ) {

      var project = projects[panel.project];
      var area = matchingAreas[panel.area];

      var idParts = panel.id.split("-");
      var panelType = idParts[4];
      var panelNumber = idParts[5];
      var panelData = [];
      panelData.push(formatTimestamp(matchingAreas[panel.area].timestamps.created));
      panelData.push(project.id);
      panelData.push(panel.id);
      panelData.push(project.name);
      panelData.push(project.id + "-" + area.phase + "-" + area.floor + "-" + panelType);
      panelData.push(area.phase);
      panelData.push(productTypeProductionLine[panel.type]);
      panelData.push(area.floor);
      panelData.push(panelNumber);
      panelData.push(String(panel.dimensions.height));
      panelData.push(String(panel.dimensions.length));
      panelData.push(String(panel.dimensions.area));
      panelData.push(String(panel.dimensions.weight));
      panelData.push(String(angular.isDefined(panel.dimensions.width) ? panel.dimensions.width : panel.dimensions.depth));
      panelData.push("");
      panelData.push(area.phase + "-" + area.floor + "-" + panelType);
      panelData.push(formatTimestamp(getQAValue(panel.qa, "completed")));
      panelData.push(String(getQAValue(panel.qa, "midHeight")));
      var midLength = getQAValue(panel.qa, "midLength");
      panelData.push(String(midLength !== "" ? midLength : getQAValue(panel.qa, "midWidth")));
      panelData.push(String(getQAValue(panel.qa, "diagonalWidth")));
      panelData.push(String(getQAValue(panel.qa, "diagonalHeight")));
      panelData.push("");
      panelData.push("");
      panelData.push(determineCompletedStatus(panel.qa));
      panelData.push(panel.deliveryDate);
      panelData.push(area.type);
      panelData.push(panelType);
      panelData.push(formatTimestamp(panel.timestamps.created));
      var startedDate = getQAValue(panel.qa, "started");
      var finishAssembly = getQAValue(panel.qa, "finishAssembly");
      var startLayup = getQAValue(panel.qa, "startLayup");
      var finishDate = getQAValue(panel.qa, "completed");
      panelData.push(formatTimestamp(startedDate));
      panelData.push(formatTimestamp(startedDate));
      panelData.push(formatTimestamp(finishDate));
      panelData.push(formatTimestamp(finishDate));
      var buildDuration = getDuration(finishDate, startedDate);
      var wipDuration = getDuration(startLayup, finishAssembly);
      var assemblyDuration = getDuration(finishAssembly, startedDate);
      var LayupDuration = getDuration(finishDate, startLayup);
      panelData.push(String(assemblyDuration > 0 ? assemblyDuration : ""));
      panelData.push(String(wipDuration > 0 ? wipDuration : ""));
      panelData.push(String(LayupDuration > 0 ? LayupDuration : ""));
      panelData.push(String(buildDuration > 0 ? buildDuration : ""));
      panelData.push(project.datumType);
      panelData.push(String(angular.isDefined(area.line) ? area.line : 1));
      panelData.push(getOperatives(panel.qa, "operatives"));
      panelData.push(getOperatives(panel.qa, "startedOperatives"));
      panelData.push(getOperatives(panel.qa, "finishAssemblyOperatives"));
      panelData.push(getOperatives(panel.qa, "startLayupOperatives"));
      panelData.push(getOperatives(panel.qa, "completedOperatives"));
      allPanelData.push(panelData);

    });

    return allPanelData;

  }
  
  function determineCompletedStatus( qa ) {
    if (angular.isDefined(qa)) {
      if (angular.isDefined(qa["completed"])) {
        return "Completed";
      } else if (angular.isDefined(qa["started"])) {
        return "In Progress";
      }
    } else {
      return "Planned";
    }
  }
  function getDuration(finishDate, startedDate){
    if (finishDate !== "" && startedDate !== ""){
      return (finishDate - startedDate) / 1000;
    }else{
      return "";
    }
  }
  function getOperatives( qa, type ) {
    var operatives = [];
    if (angular.isDefined(qa) && angular.isDefined(qa[type])) {
      for (operative in qa[type]) {
        var opName = operativeNames[operative];
        operatives.push(opName);
      }
      return operatives.join(", ");
    } else {
      return "";
    }
  }
  function formatTimestamp( timeStamp ) {
    var date = new Date(timeStamp);
    return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
  }
  function getQAValue( qa, key ) {
    return angular.isDefined(qa) && qa[key] ? qa[key] : "";
  }
  
  return values;
  
}

module.exports = {
  getSuppliers: getSuppliers,
  storeFormData: storeFormData,
  getPanData: getPanData
};
