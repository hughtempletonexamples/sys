"use strict";

var webpack = require("webpack");
var path = require("path");
var CopyWebpackPlugin = require("copy-webpack-plugin");

var apiEntryPoint = path.resolve(__dirname, "Code.js");

var distDirectory = path.resolve(__dirname, "../", "dist", "api");

var innviewTarget = process.env.INNVIEW_FIREBASE;
var environment = "local";

switch (innviewTarget) {
case "innview-showcase":
  environment = "showcase";
  break;
case "innview":
  environment = "production";
  break;
default:
  environment = "local";
}

var config = {
  entry: {
    "api": apiEntryPoint
  },

  output: {
    libraryTarget: "this",
    path: distDirectory,
    filename: "[name].js"
  },

  plugins: [
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "env", environment + ".gapps.config.json"),
        to: path.resolve(__dirname, "../", "gapps.config.json"),
        force: true
      }
    ]),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
      compress: {
        warnings: false // Supress uglification warnings
      }
    })
  ],

  resolve: {
    alias: {
      environment: path.resolve(__dirname, "env", environment + ".js")
    }
  }
};

module.exports = config;
