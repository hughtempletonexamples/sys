Google announced an upgrade from Firebase 2 to Firebase 3 which introduced a new SDK and
a new platform by moving from firebase.com to firebase.google.com

This changed has introduced a new Firebase console, console.firebase.google.com
and requires application changes to adopt the new SDK.
The changes are described here: https://firebase.google.com/support/guides/firebase-web

Here's what I did to upgrade:
1. Migrate just my test database to FB3 (Firebase v3) (innview-fb3)
2. npm install -g firebase-tools@3.0.8 // This matches the version in package.json.
2. npm install
2. firebase logout
3. firebase login // Re-authenticate with Google in your browser.
4. \*firebase tools:migrate (\*this updates firebase.json, so may not
   be needed on dev machines if you've pulled from git as the change
   has already been pushed.)
5. package.json - updated and then run "npm update"
6. export INNVIEW_FIREBASE=innview-fb3
7. export INNVIEW_FIREBASE_APIKEY=AIcvSyCrj70UkPaByT2qRb...
 Replace these values with your own dev instance name and apiKey!
 
 The APIKEY is new for Firebase 3. Eg. AIcvSyCrj70UkPaByT2qRb...

 The apiKey comes from the firebase console. Select "Auth", then
 "SIGN-IN-METHOD" and then "WEB SETUP". Here are the docs
 https://firebase.google.com/docs/web/setup#project_setup
8. Use https://console.firebase.google.com/ to login and access
   migrated Firebase instances.

And here's how to switch back to the old version
1. npm install -g firebase-tools@2.2.1
2. firebase login
3. firebase list

Service Account: 

To deploy, we need to make use of a service account.

Details for creating one can be found here:
https://firebase.google.com/docs/server/setup

Save the downloaded json file as
`config/$INNVIEW_FIREBASE_service.json` The `data/firebaseUtil.js`
code will look for this file automatically, based on the value of your
INNVIEW_FIREBASE environment variable.

TODO:

4. Update main documentation

Replace this code:
//if (needTokenRefresh(authData)) {
//  logout();
//  return null;
//}
