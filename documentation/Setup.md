Setting up a development environment
====================================

Create Firebase instance
------------------------

1. Go to https://console.firebase.google.com/
2. Create new project named "innview-YOURNAME"
3. On the Authentication section, select the Sign-In Method tab and
   enable Email/Password
4. Click the Web Setup link in the top right of the page and make a
   note of the apiKey value


Set environment variables
-------------------------

On your local machine, set the following environment variables:

* INNVIEW_FIREBASE=innview-YOURNAME
* INNVIEW_FIREBASE_APIKEY=Your apiKey value

Install firebase tools
----------------------

npm install -g firebase-tools@3.0.8
firebase login

Check out code
--------------

Clone the repo:

    `git clone git@bitbucket.org:blackpeppersoftware/innview.git`

Then get the fb3 branch

    `git checkout spike-fb-sdk3`

finally, install the node dependencies:

    `npm install`

Create service account
----------------------

To deploy, we need to make use of a service account.

Details for creating one can be found here:
https://firebase.google.com/docs/server/setup

Save the downloaded json file as
`config/$INNVIEW_FIREBASE_service.json`, i.e.
`innview-YOURNAME_service.json`

The `data/firebaseUtil.js` code will look for this file automatically,
based on the value of your INNVIEW_FIREBASE environment variable.

Deploy
------

Deploy the code using the following  command:

    `npm run deploy`


You should then be able to access the application at
https://innview-YOURNAME.firebaseapp.com

You can login as either one of the canned users
(operations@example.com or production@example.com) or with your email
address. The passwrod for dev environments is "password"

