/* eslint angular/json-functions: 0 */
"use strict";

var webpack = require("webpack");
var path = require("path");
var CopyWebpackPlugin = require("copy-webpack-plugin");
var WebpackShellPlugin = require("webpack-shell-plugin");
var NgAnnotatePlugin = require("ng-annotate-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require("clean-webpack-plugin");

var mainEntryPoint = path.resolve(__dirname, "app", "configured_app.js");
var newuxEntryPoint = path.resolve(__dirname, "app", "configured_app-newux.js");
var qaEntryPoint = path.resolve(__dirname, "app", "configured_app-qa.js");
var publicDirectory = path.resolve(__dirname, "public");
var distDirectory = path.resolve(__dirname, "dist");

var production = process.env.NODE_ENV === "production";

var materialRequestForm;
switch (process.env.INNVIEW_FIREBASE) {
case "innview-showcase":
  materialRequestForm = "https://docs.google.com/a/blackpepper.co.uk/forms/d/e/1FAIpQLScfCL96fmDEGiNMA1HvIdiBxyw7-azcUn6eEr9PDkK05i9Zrw/viewform";
  break;
case "firebase-innview":
  materialRequestForm = "https://docs.google.com/forms/d/e/1FAIpQLSdGdncskhgpO721iD6FUgIRLZMQ35T1E9iv9128Jmv3ivNaGw/viewform";
  break;
default:
  materialRequestForm = "https://docs.google.com/a/blackpepper.co.uk/forms/d/e/1FAIpQLScfCL96fmDEGiNMA1HvIdiBxyw7-azcUn6eEr9PDkK05i9Zrw/viewform";
}

var config = {
  entry: {
    "innview-main": mainEntryPoint,
    "innview-newux": newuxEntryPoint,
    "innview-qa": qaEntryPoint
  },
  plugins: [
    new webpack.DefinePlugin({
      PRODUCTION: production,
      FIREBASE: "\"" + process.env.INNVIEW_FIREBASE + "\"",
      FIREBASEAPIKEY: "\"" + process.env.INNVIEW_FIREBASE_APIKEY + "\"",
      MATERIALREQUESTFORM: JSON.stringify(materialRequestForm)
    }),

    new ExtractTextPlugin("stylesheets/[name].css", {allChunks: true, disable: !production}),

    new webpack.optimize.CommonsChunkPlugin({ name: "innview-common", filename: "innview-common.js", minChunks: 2}),

    new CleanWebpackPlugin(["dist"], {
      root: path.resolve(__dirname),
      verbose: true,
      dry: false
    })
  ],
  output: {
    path: distDirectory,
    filename: "[name].js"
  },
  devServer: {
    contentBase: publicDirectory,
    port : 8080
  },
  devtool: "source-map",
  module: {
    loaders: [
      {
        test: /\.s?css$/,
        loader: ExtractTextPlugin.extract("style", "css?-url!sass")
      },
      {
        test: /\.html$/,
        loader: "file"
      },
      {
        test: /\.jpg$/,
        loader: "file"
      },
      { // for third-party minified scripts, don't process require()
        test: /\/xlsx\//,
        include: /(node_modules|bower_components)/,
        loader: "script"
      }
    ]
  }
};

if (production) {
  config.plugins.push(
    // Ensure files are copied from the public directory to the
    // distribution one
    new WebpackShellPlugin({ safe: true,
      onBuildEnd:[ 'pwd; echo \\"cp -R ' + publicDirectory + " " + distDirectory + '\\"; cp -R ' + publicDirectory + "/* " + distDirectory ]
    })
  );

  config.plugins.push(
    // Runs the ng-annotate pre-minimizer to insert AngularJS dependency
    // injection annotations.
    new NgAnnotatePlugin()
  );

  config.plugins.push(
    // This plugin optimizes chunks and modules by how much they are
    // used in the app.
    new webpack.optimize.DedupePlugin()
  );

  config.plugins.push(
    // This plugin prevents webpack from creating chunks that would be
    // too small to be worth loading separately
    new webpack.optimize.MinChunkSizePlugin({
      minChunkSize: 512000 // ~50kb
    })
  );
/*
  config.plugins.push(
    new webpack.optimize.CommonsChunkPlugin("innview-common.js")
  );
*/
  /*
  config.plugins.push(
    // This plugin minifies all the javascript code of the final bundle
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
      compress: {
        warnings: false // Supress uglification warnings
      }
    })
  );
  */
}

module.exports = config;
