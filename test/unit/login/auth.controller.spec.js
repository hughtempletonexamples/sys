"use strict";

var utils = require("../testUtils");

describe("auth", function () {

  var $q;
  var $controller;
  var $rootScope;
  var $location;

  var mockSchema;
  var mockAuth;

  beforeEach(angular.mock.module("innView.login", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    mockSchema = jasmine.createSpyObj("schema", ["login", "logout"]);
    mockAuth = null;

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("auth", function () {
        return mockAuth;
      });
    });
  });

  beforeEach(inject(function (_$q_, _$controller_, _$rootScope_, _$location_) {
    $q = _$q_;
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $location = _$location_;
  }));

  function createController(locals) {
    return $controller("AuthController", angular.extend(
      {
        $scope: $rootScope.$new()
      },
      locals));
  }

  describe("controller", function () {

    it("initialises", function () {
      $location.url("/login?from=%2Fpage");
      var controller = createController({$routeParams: {from: "/page"}});

      expect(controller.loggedOut).toEqual(false);
      expect(controller.errorMessage).toEqual(null);
      expect(controller.redirectTo).toEqual("/page");

      expect(controller.email).toEqual("");
      expect(controller.password).toEqual("");

      expect($location.url()).toEqual("/login");
    });

    it("login success", function () {
      $location.url("/login?from=%2Fpage");
      var controller = createController({$routeParams: {from: "/page"}});

      controller.email = "email";
      controller.password = "password";

      mockSchema.login.and.returnValue($q.resolve({}));

      controller.login(true);
      $rootScope.$apply();

      expect(mockSchema.login.calls.mostRecent().args).toEqual(["email", "password"]);
      expect($location.url()).toEqual("/page");
    });

    it("login failure", function () {
      $location.url("/login?from=%2Fpage");
      var controller = createController({$routeParams: {from: "/page"}});

      controller.email = "email";
      controller.password = "password";

      mockSchema.login.and.returnValue($q.reject());

      controller.login(true);
      $rootScope.$apply();

      expect($location.url()).toEqual("/login");
      expect(controller.errorMessage).toEqual("Unknown email or incorrect password");
    });

    it("logs out", function () {
      mockAuth = {uid: 1};

      var controller = createController({$routeParams: {logout: true}});

      expect(mockSchema.logout.calls.count()).toEqual(1);

      expect(controller.loggedOut).toEqual(true);
      expect(controller.errorMessage).toEqual(null);
      expect(controller.redirectTo).toEqual("/");

      expect(controller.email).toEqual("");
      expect(controller.password).toEqual("");
    });

  });
});
