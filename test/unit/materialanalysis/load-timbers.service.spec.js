"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("load panels service", function () {
  var scope;

  var mockXLSX;
  var worksheet;

  var firebaseRoot;
  var projectsRef;
  var panelsRef;
  var requiredPanelUsageRef;
  var cuttingListRef;
  var jobsheetsRef;
  var mockSchema;
  var mockAreasService;
  var mockPanelsService;
  var mockProjectsService;
  var mockFileReader;
  
  var requiredPanelUsage1;
  var requiredPanelUsage2;
  var requiredPanelUsage3;
  var requiredPanelUsage4;
  
  var cuttingList1;
  var cuttingList2;
  var cuttingList3;
  var cuttingList4;
  
  var loadTimbersService;
  
  var project1 = {
    $id: "PROJ1",
    "name": "PROJ1 Name",
    "id": "267",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV1"
    }
  };

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["getPanelById", "panelsForProject"]);
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProject"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");
    cuttingListRef = firebaseRoot.child("cuttingList");
    jobsheetsRef = firebaseRoot.child("jobsheets");
    
    utils.addQuerySupport(requiredPanelUsageRef);
    utils.addQuerySupport(cuttingListRef);
    utils.addQuerySupport(jobsheetsRef);
    utils.addQuerySupport(panelsRef);
    
    mockXLSX = {
      read: jasmine.createSpy("read"),
      utils: {
        decode_range: decode_range,
        encode_cell: encode_cell
      }
    };

    worksheet = {
      Sheets: []
    };

    mockXLSX.read.and.callFake(function () {
      return worksheet;
    });

    angular.mock.module(function ($provide) {
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.constant("FileReader", function () {
        return mockFileReader;
      });
      $provide.constant("XLSX", mockXLSX);
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  
  beforeEach(inject(function ($q) {
    mockProjectsService.getProject.and.callFake(function () {
      return $q.when(project1);
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    
    var panel1 = {
      id: "267-AR-P4-GF-I-010",
      project: "PROJ1"
    };

    var panel2 = {
      id: "267-AR-P4-GF-I-011",
      project: "PROJ1"
    };

    var panel3 = {
      id: "266-AR-P4-GF-I-010",
      project: "PROJ2"
    };

    panelsRef.set({
      "PROJ1PANEL1": panel1,
      "PROJ1PANEL2": panel2,
      "PROJ2PANEL1": panel3
    });

    mockPanelsService.panelsForProject.and.callFake(function (projectRef) {
      return $firebaseArray(panelsRef
        .orderByChild("project")
        .equalTo(projectRef));
    });
  }));

  beforeEach(inject(function (schema) {
    schema.getRoot().child("srd").set({
      "phases": {
        "P1": {},
        "P2": {}
      },
      "floors": {
        "GF": {},
        "1F": {}
      },
      "panelTypes": {
        "H": {},
        "L": {},
        "C": {},
        "I": {}
      },
      "productTypes": {
        "Ext": { "name": "External" },
        "Int": { "name": "Internal" },
        "Roof": { "name": "Roof" }
      }
    });
  }));

  beforeEach(function () {
    projectsRef.set(
      {
        "PROJ1": {
          "id": "214",
          "name": "Project One"
        },
        "PROJ2": {
          "id": "314",
          "name": "Project Two"
        }
      }
    );
  });
  
  beforeEach(function () {

    requiredPanelUsage1 = {
      "id": "214-DA-P2-GF-H-100",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 1,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 2343
      },
      "project": "PROJ1",
      "panel": "PANEL1"
    };
      
    requiredPanelUsage2 = {
      "id": "214-DA-P2-GF-H-100",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 5,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 2343
      },
      "project": "PROJ1",
      "panel": "PANEL2"
    };
    
    requiredPanelUsage3 = {
      "id": "214-DA-P2-GF-H-110",
      "jobsheet": "JS-214-P3-GF-H-003",
      "usageDetails": {
        "amount": 2,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 5000
      },
      "project": "PROJ1",
      "panel": "PANEL3"
    };
    
    requiredPanelUsage4 = {
      "id": "214-DA-P2-GF-H-110",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 2,
        "width": 38,
        "height": 114,
        "lenghtInMeters": 5000
      },
      "project": "PROJ1",
      "panel": "PANEL4"
    };
    
    cuttingList1 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":4686,
        "actual":0
      }
    };
    
    cuttingList2 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":9686,
        "actual":0
      }
    };
    
    cuttingList3 = {
      "type":"38 x 114",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
    
    cuttingList4 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
        
  });

  beforeEach(inject(function ($firebaseArray) {
    
    var projectAreas = {
      "PROJ1": {
        "AREA1": {
          "phase": "P2",
          "floor": "GF",
          "type": "Ext"
        },
        "AREA2": {
          "phase": "P2",
          "floor": "GF",
          "type": "Int"
        }
      },
      "PROJ2": {
        "AREA3": {
          "phase": "P2",
          "floor": "GF",
          "type": "Roof"
        }
      }
    };
    
    var areaPanels = {
      "214-DA-P2-GF-H-100": {
        "PANEL1": {
          id: "214-DA-P2-GF-H-100",
          project: "PROJ1",
          type: "Ext",
          area: "AREA1",
          dimensions: {
            length: 1164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        }
      },
      "214-DA-P2-GF-L-111": {
        "PANEL2": {
          id: "214-DA-P2-GF-L-111",
          project: "PROJ1",
          type: "Int",
          area: "AREA2",
          dimensions: {
            length: 2700,
            height: 270,
            width: 162,
            area: 0.7,
            weight: 30
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        }
      },
      "314-DA-P2-GF-C-112": {
        "PANEL3": {
          id: "314-DA-P2-GF-C-112",
          project: "PROJ2",
          type: "Roof",
          area: "AREA3",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        }
      }
    };
    
    mockAreasService.getProjectAreas.and.callFake(
      function (projectKey) {
        var areasRef = new MockFirebase("MockQuery://areas");
        areasRef.autoFlush(true);
        areasRef.set(projectAreas[projectKey]);

        return $firebaseArray(areasRef);
      });
      
    mockPanelsService.getPanelById.and.callFake(
      function (panelId) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);
        panelsRef.set(areaPanels[panelId]);

        return $firebaseArray(panelsRef);
      });
    
  }));

  beforeEach(inject(function ($rootScope, _loadTimbersService_) {
    scope = $rootScope;
    loadTimbersService = _loadTimbersService_;
  }));
  
  describe("required by panel", function () {

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1"
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1"
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA2"
    };

    beforeEach(function () {
      panelsRef.set({
        "PANEL1": panel1,
        "PANEL2": panel2,
        "PANEL3": panel3
      });
    });

    it("returns all required usage for a given panel", function () {
      
      requiredPanelUsageRef.set({
        "requiredPanelUsage1": requiredPanelUsage1,
        "requiredPanelUsage2": requiredPanelUsage2,
        "requiredPanelUsage3": requiredPanelUsage3,
        "requiredPanelUsage4": requiredPanelUsage4
      });
      
      var result = loadTimbersService.getRequiredByPanel("PANEL1");

      scope.$apply();
      expect(result.length).toEqual(1);
      expect(result[0]).toEqual(jasmine.objectContaining(requiredPanelUsage1));
    });
  });
  
  describe("loadTimberSchedule with CSV", function () {

    beforeEach(function () {

      mockFileReader = {
        onload: undefined,
        result: undefined,
        readAsBinaryString: jasmine.createSpy("readAsBinaryString")
      };

      mockFileReader.readAsBinaryString.and.callFake(function (blob) {
        mockFileReader.onload({target: {result: worksheet}});
      });

    });

    it("does panel exist in system check", function (done) {

      addCSV([     
      ["2343","38","109","1","0","0","38","109","90","0","0","0","0","0","90","0","2343","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","267-AR-P4-GF-I-010_34","JS-267-P4-GF-I-002","","0"],
      ["2229","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","2229","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","267-AR-P4-GF-I-010_30","JS-267-P4-GF-I-002","","0"],
      ["1159","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","1159","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","267-AR-P4-HH-I-010_4","JS-267-P4-GF-I-002","","0"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-267-P4-GF-I-002_Square.csv";
      
      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "Can not find '267-AR-P4-GF-I-010' in system!",
            "Can not find '267-AR-P4-GF-I-010' in system!",
            "Can not find '267-AR-P4-HH-I-010' in system!"
          ]);

          done();
        });

      scope.$apply();
    });
    
    it("reports if one of the panels timber length is not valid", function (done) {
      
      addCSV([     
      ["gg","38","109","1","0","0","38","109","90","0","0","0","0","0","90","0","2343","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_34","JS-214-P2-GF-H-002","","0"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-214-P2-GF-H-002_Square.csv";
      
      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-P2-GF-H-100' invalid timber length. Please check csv has all length fields"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports if panel does not match jobsheet in file name", function (done) {

      addCSV([     
      ["3232","38","109","1","0","0","38","109","90","0","0","0","0","0","90","0","2343","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_34","JS-214-P2-GF-H-002","","0"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-214-P3-GF-H-002_Square.csv";

      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "Panel does not match jobsheet in file name 'JS-214-P2-GF-H'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("loads panel details from sheet with CSV", function (done) {
      addCSV([     
      ["2343","38","109","1","0","0","38","109","90","0","0","0","0","0","90","0","2343","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_34","JS-214-P2-GF-H-002","","0"],
      ["2229","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","2229","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_30","JS-214-P2-GF-H-002","","0"],
      ["1159","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","1159","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_4","JS-214-P2-GF-H-002","","0"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-214-P2-GF-H-002_Square.csv";
      
      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          expect(result.panels.length).toEqual(2);
          expect(result.panels[0]).toEqual({
            "id":"214-DA-P2-GF-H-100",
            "jobsheet":"JS-214-P2-GF-H-002",
            "usageDetails":{
              "amount":1,
              "width":38,
              "height":109,
              "lenghtInMeters":2343
            },
            "project":"PROJ1",
            "panel":"PANEL1"
          });
          expect(result.panels[1]).toEqual({
            "id":"214-DA-P2-GF-H-100",
            "jobsheet":"JS-214-P2-GF-H-002",
            "usageDetails":{
              "amount":4,
              "width":38,
              "height":114,
              "lenghtInMeters":6776
            },
            "project":"PROJ1",
            "panel":"PANEL1"
          });
          expect(result.errors).toEqual([]);
          done();
        });

      scope.$apply();
    });
    
    it("does it add jobsheet to panel", function (done) {
      addCSV([     
      ["2343","38","109","1","0","0","38","109","90","0","0","0","0","0","90","0","2343","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_34","JS-214-P2-GF-H-002","","0"],
      ["2229","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","2229","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_30","JS-214-P2-GF-H-002","","0"],
      ["1159","38","114","2","0","0","38","114","90","0","0","0","0","0","90","0","1159","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","","C24","","17003AAG","","","","214-DA-P2-GF-H-100_4","JS-214-P2-GF-H-002","","0"]
      ]);

      var panel1 = {
        id: "panel1",
        project: "PROJ1"
      };

      var panel2 = {
        id: "panel2",
        project: "PROJ1"
      };

      var panel3 = {
        id: "panel3",
        project: "PROJ2"
      };

      beforeEach(function () {
        panelsRef.set({
          "PROJ1PANEL1": panel1,
          "PROJ1PANEL2": panel2,
          "PROJ2PANEL1": panel3
        });
      });

      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-214-P2-GF-H-002_Square.csv";
      
      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          expect(result.errors).toEqual([]);
        
          var savedPanelUsage = utils.getRefChild(panelsRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining({"jobSheet": 2}));
        
          done();
        });

      scope.$apply();
    });
    
  });

  describe("loadTimberSchedule with CSV for hundegger", function () {

    beforeEach(function () {

      mockFileReader = {
        onload: undefined,
        result: undefined,
        readAsBinaryString: jasmine.createSpy("readAsBinaryString")
      };

      mockFileReader.readAsBinaryString.and.callFake(function (blob) {
        mockFileReader.onload({target: {result: worksheet}});
      });

    });

    it("does panel exist in system check", function (done) {

      addCSV([
        ["Part No.","Part Name","Req.","Cut","Width","Height","Length","Unit","Gr","Profile","Roof","area","Comments"],
        ["1","010","1","2","38","109","RT+2343","020","C24","","","","1"],
        ["2","010","2","2","38","114","RT+2229","020","C24","","","","1"],
        ["3","010","2","2","38","114","RT+1159","020","C24","","","","1"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-267-P3-GF-I-002.csv";
      
      loadTimbersService.loadTimberSchedule(file, true)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "Can not find '267-P3-GF-I-010' in system!",
            "Can not find '267-P3-GF-I-010' in system!"
          ]);

          done();
        });

      scope.$apply();
    });
    
    it("reports if one of the panels timber length is not valid", function (done) {
      
      addCSV([
        ["Part No.","Part Name","Req.","Cut","Width","Height","Length","Unit","Gr","Profile","Roof","area","Comments"],
        ["1","010","1","2","38","109","RT","020","C24","","","","1"],
        ["2","010","2","2","38","114","RT","020","C24","","","","1"],
        ["3","010","2","2","38","114","RT","020","C24","","","","1"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-267-P4-GF-I-002.csv";
      
      //expect only 2 because same width and height is merged
      loadTimbersService.loadTimberSchedule(file, true)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "One of the timbers in '267-P4-GF-I-010' has not a valid length",
            "One of the timbers in '267-P4-GF-I-010' has not a valid length"
          ]);

          done();
        });

      scope.$apply();
    });

    it("loads panel details from sheet with CSV", function (done) {

      addCSV([
        ["Part No.","Part Name","Req.","Cut","Width","Height","Length","Unit","Gr","Profile","Roof","area","Comments"],
        ["1","010","1","2","38","109","RT+2343","020","C24","","","","1"],
        ["2","010","2","2","38","114","RT+2229","020","C24","","","","1"],
        ["3","010","2","2","38","114","RT+1159","020","C24","","","","1"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-267-P4-GF-I-002.csv";
      
      loadTimbersService.loadTimberSchedule(file, true)
        .then(function (result) {
          expect(result.panels.length).toEqual(2);
          expect(result.panels[0]).toEqual({
            "id":"267-AR-P4-GF-I-010",
            "jobsheet":"JS-267-P4-GF-I-002",
            "usageDetails":{
              "amount":1,
              "width":38,
              "height":109,
              "lenghtInMeters":2343
            },
            "project":"PROJ1",
            "panel":"PROJ1PANEL1"
          });
          expect(result.panels[1]).toEqual({
            "id":"267-AR-P4-GF-I-010",
            "jobsheet":"JS-267-P4-GF-I-002",
            "usageDetails":{
              "amount":4,
              "width":38,
              "height":114,
              "lenghtInMeters":6776
            },
            "project":"PROJ1",
            "panel":"PROJ1PANEL1"
          });
          expect(result.errors).toEqual([]);
          done();
        });

      scope.$apply();
    });
    
    it("does it add jobsheet to panel", function (done) {
      addCSV([
        ["Part No.","Part Name","Req.","Cut","Width","Height","Length","Unit","Gr","Profile","Roof","area","Comments"],
        ["1","010","1","2","38","109","RT+2343","020","C24","","","","1"],
        ["2","010","2","2","38","114","RT+2229","020","C24","","","","1"],
        ["3","010","2","2","38","114","RT+1159","020","C24","","","","1"]
      ]);

      var panel1 = {
        id: "panel1",
        project: "PROJ1"
      };

      var panel2 = {
        id: "panel2",
        project: "PROJ1"
      };

      var panel3 = {
        id: "panel3",
        project: "PROJ2"
      };

      beforeEach(function () {
        panelsRef.set({
          "PROJ1PANEL1": panel1,
          "PROJ1PANEL2": panel2,
          "PROJ2PANEL1": panel3
        });
      });

      var file = new Blob([worksheet],{"type":"text/csv"});
      file.name = "JS-267-P4-GF-I-002.csv";
      
      loadTimbersService.loadTimberSchedule(file, true)
        .then(function (result) {
          expect(result.errors).toEqual([]);
        
          var savedPanelUsage = utils.getRefChild(panelsRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining({"jobSheet": 2}));
        
          done();
        });

      scope.$apply();
    });
    
  });

  describe("addPanelsUsage", function () {

    it("saves new panels usage", function (done) {

      var panelUsage = {
        "id":"214-DA-P2-GF-H-100",
        "jobsheet":"JS-214-P2-GF-H-002",
        "usageDetails":{
          "amount":1,
          "width":38,
          "height":109,
          "lenghtInMeters":2343
        },
        "project":"PROJ1",
        "panel":"PANEL1"
      };

      loadTimbersService.updatePanelsUsage([panelUsage])
        .then(function (result) {
          var savedPanelUsage = utils.getRefChild(requiredPanelUsageRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(panelUsage));

          expect(result.total).toEqual(1);
          expect(result.details).toEqual(
            [
              {projectId: "214", projectName: "Project One", count: 1}
            ]
          );

          done();
        });

      scope.$apply();
    });

  });
  
  describe("addCuttingList", function () {
    
    var cuttingListFirebaseArray;
    var jobsheetsFirebaseArray;

    beforeEach(inject(function ($firebaseArray) {
      cuttingListFirebaseArray = $firebaseArray(cuttingListRef);
      jobsheetsFirebaseArray = $firebaseArray(jobsheetsRef);

      scope.$apply();
    }));

    it("adds cutting list and jobsheet", function (done) {
    
      requiredPanelUsageRef.set({
        "requiredPanelUsage1": requiredPanelUsage1,
        "requiredPanelUsage2": requiredPanelUsage2
      });

      var jobsheetId = ["JS-214-P2-GF-H-002"];
     
      loadTimbersService.createCuttingList(jobsheetId)
        .then(function (result) {
          
          var jobsheet = {};
          jobsheet.jobsheet = "JS-214-P2-GF-H-002";
          jobsheet.projectId = "214";
          jobsheet.cuttingList = {};
          
          
          angular.forEach(result[0], function (item, key) {
            jobsheet.cuttingList[key] = true;
          });
          
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList1));

          var savedPanelUsage = utils.getRefChild(jobsheetsRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(jobsheet));

          done();
        });

      scope.$apply();
    });
    
    it("adds multiple cutting list and jobsheet", function (done) {
    
      requiredPanelUsageRef.set({
        "requiredPanelUsage1": requiredPanelUsage1,
        "requiredPanelUsage2": requiredPanelUsage2,
        "requiredPanelUsage3": requiredPanelUsage3,
        "requiredPanelUsage4": requiredPanelUsage4
      });

      var jobsheetId = ["JS-214-P2-GF-H-002", "JS-214-P3-GF-H-003"];
     
      loadTimbersService.createCuttingList(jobsheetId)
        .then(function (result) {
          
          var jobsheet = {};
          jobsheet.jobsheet = "JS-214-P2-GF-H-002";
          jobsheet.projectId = "214";
          jobsheet.cuttingList = {};

          angular.forEach(result[0], function (item, key) {
            jobsheet.cuttingList[key] = true;
          });
          
          var jobsheet2 = {};
          jobsheet2.jobsheet = "JS-214-P3-GF-H-003";
          jobsheet2.projectId = "214";
          jobsheet2.cuttingList = {};

          angular.forEach(result[1], function (item, key) {
            jobsheet2.cuttingList[key] = true;
          });
         
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList2));

          var savedPanelUsage = utils.getRefChild(jobsheetsRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(jobsheet));
          
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 1);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList3));

          var savedPanelUsage = utils.getRefChild(jobsheetsRef, 1);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(jobsheet2));
          
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 2);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList4));
          
          done();
        });

      scope.$apply();
    });
    
    it("adds multiple cutting list types", function (done) {
    
      requiredPanelUsageRef.set({
        "requiredPanelUsage1": requiredPanelUsage1,
        "requiredPanelUsage2": requiredPanelUsage2,
        "requiredPanelUsage3": requiredPanelUsage3,
        "requiredPanelUsage4": requiredPanelUsage4
      });

      var jobsheetId = ["JS-214-P2-GF-H-002"];
     
      loadTimbersService.createCuttingList(jobsheetId)
        .then(function (result) {
          
          var jobsheet = {};
          jobsheet.jobsheet = "JS-214-P2-GF-H-002";
          jobsheet.projectId = "214";
          jobsheet.cuttingList = {};

          angular.forEach(result[0], function (item, key) {
            jobsheet.cuttingList[key] = true;
          });
         
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList1));

          var savedPanelUsage = utils.getRefChild(jobsheetsRef, 0);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(jobsheet));
          
          var savedPanelUsage = utils.getRefChild(cuttingListRef, 1);
          expect(savedPanelUsage).toEqual(jasmine.objectContaining(cuttingList3));

          done();
        });

      scope.$apply();
    });

  });

  function addCSV(data) {
    var lineArray = [];
    data.forEach(function (infoArray, index) {
      var line = infoArray.join(",");
      lineArray.push(line);
    });
    worksheet = lineArray.join("\n");
  }

  function encode_cell(cell) {
    return String.fromCharCode(65 + cell.c) + (cell.r + 1);
  }

  function decode_cell(cell_address) {
    var match = /^([A-Z])(\d+)$/.exec(cell_address);
    return {c: match[1].charCodeAt(0) - 65, r: match[2] - 1};
  }

  function encode_range(range) {
    return encode_cell(range.s) + ":" + encode_cell(range.e);
  }

  function decode_range(range) {
    var parts = range.split(":", 2);
    return {s: decode_cell(parts[0]), e: decode_cell(parts[1])};
  }
});
