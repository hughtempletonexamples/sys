"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Cubic Average Service", function () {
  var scope;
  var createdTimestamp;
  var systemTimestamp;
  

  var firebaseRoot;
  var projectsRef;
  var panelsRef;
  var requiredPanelUsageRef;
  var cuttingListRef;
  var jobsheetsRef;
  var defaultCuttingTypesPriceRef;
  var cuttingTypesPriceRef;
  var srdRef;
  
  var mockSchema;
  var mockAreasService;
  var mockPanelsService;
  var mockLoadTimbersService;

  var jobsheet1;
  var jobsheet2;
  
  var cuttingList1;
  var cuttingList2;
  var cuttingList3;
  
  var cubicAverageService;

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  beforeEach(function () {
    var now = new Date("2017-11-25");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsByCompleted"]);
    mockLoadTimbersService = jasmine.createSpyObj("loadTimbersService", ["getRequiredByPanel"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");
    cuttingListRef = firebaseRoot.child("cuttingList");
    jobsheetsRef = firebaseRoot.child("jobsheets");
    cuttingTypesPriceRef = firebaseRoot.child("cuttingTypesPrice");
    defaultCuttingTypesPriceRef = firebaseRoot.child("defaultCuttingTypesPrice");
    srdRef = firebaseRoot.child("srd");
    
    utils.addQuerySupport(requiredPanelUsageRef);
    utils.addQuerySupport(cuttingListRef);
    utils.addQuerySupport(jobsheetsRef);
    utils.addQuerySupport(panelsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("loadTimbersService", function () {
        return mockLoadTimbersService;
      });
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(function () {
    projectsRef.set(
      {
        "PROJ1": {
          "id": "214"
        },
        "PROJ2": {
          "id": "314"
        }
      }
    );
  });

  beforeEach(function () {
    srdRef.child("productTypes").set(
      {
        "Ext": {
          "name": "External",
          "productionLine": "SIP",
          "seq": 0
        },
        "ExtH": {
          "name": "Hybrid SIP",
          "productionLine": "HSIP",
          "seq": 2
        },
        "ExtF": {
          "name": "IFAST Wall",
          "productionLine": "IFAST",
          "seq": 2
        },
        "Int": {
          "name": "Internal",
          "productionLine": "TF",
          "seq": 1
        },
        "Floor": {
          "name": "Floor",
          "productionLine": "CASS",
          "seq": 3
        },
        "Roof": {
          "name": "Roof",
          "productionLine": "CASS",
          "seq": 4
        },
        "Anc": {
          "name": "Ancilliaries",
          "seq": 5
        },
        "P&B": {
          "name": "Posts and Beams",
          "seq": 6
        },
        "Steel": {
          "name": "Steel",
          "seq": 7
        },
        "Truss": {
          "name": "Trusses",
          "seq": 8
        }
      }
    );
  });

  beforeEach(inject(function ($firebaseArray) {
    
    var projectAreas = {
      "PROJ1": {
        "AREA1": {
          "phase": "P2",
          "floor": "GF",
          "type": "Ext"
        },
        "AREA2": {
          "phase": "P2",
          "floor": "GF",
          "type": "Int"
        },
        "AREA4": {
          "phase": "P2",
          "floor": "GF",
          "type": "ExtH"
        },
        "AREA5": {
          "phase": "P2",
          "floor": "GF",
          "type": "ExtF"
        }
      },
      "PROJ2": {
        "AREA3": {
          "phase": "P2",
          "floor": "GF",
          "type": "Roof"
        }
      }
    };
   
    var requiredPanelUsage = {
      "PROJ1AREA1PANEL1": {
        "requiredPanelUsage1": {
          "id": "214-DA-P2-GF-I-100",
          "jobsheet": "JS-214-P2-GF-I-002",
          "usageDetails": {
            "amount": 1,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 2343
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA1PANEL1"
        }
      },
      "PROJ1AREA1PANEL2": {
        "requiredPanelUsage2": {
          "id": "214-DA-P2-GF-I-111",
          "jobsheet": "JS-214-P2-GF-I-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA1PANEL2"
        }
      },
      "PROJ2AREA1PANEL1": {
        "requiredPanelUsage3": {
          "id": "314-DA-P2-GF-C-112",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL1"
        }
      },
      "PROJ2AREA1PANEL2": {
        "requiredPanelUsage4": {
          "id": "314-DA-P2-GF-C-113",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL2"
        }
      },
      "PROJ2AREA1PANEL3": {
        "requiredPanelUsage5": {
          "id": "314-DA-P2-GF-C-115",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 114,
            "lenghtInMeters": 2000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL4"
        }
      },
      "PROJ1AREA4PANEL1": {
        "requiredPanelUsage6": {
          "id": "214-DA-P2-GF-H-10",
          "jobsheet": "JS-214-P2-GF-H-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 114,
            "lenghtInMeters": 2000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL4"
        }
      },
      "PROJ1AREA4PANEL2": {
        "requiredPanelUsage7": {
          "id": "214-DA-P2-GF-I-101",
          "jobsheet": "JS-214-P2-GF-H-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 114,
            "lenghtInMeters": 2000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL4"
        }
      },
      "PROJ1AREA5PANEL3": {
        "requiredPanelUsage8": {
          "id": "214-DA-P2-GF-F-102",
          "jobsheet": "JS-214-P2-GF-F-002",
          "usageDetails": {
            "amount": 1,
            "width": 135,
            "height": 543,
            "lenghtInMeters": 2345
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA5PANEL1"
        }
      },
      "PROJ1AREA5PANEL1": {
        "requiredPanelUsage8": {
          "id": "214-DA-P2-GF-F-102",
          "jobsheet": "JS-214-P2-GF-F-002",
          "usageDetails": {
            "amount": 1,
            "width": 135,
            "height": 543,
            "lenghtInMeters": 2345
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA5PANEL1"
        }
      }      
    };
    
    mockAreasService.getProjectAreas.and.callFake(
      function (projectKey) {
        var areasRef = new MockFirebase("MockQuery://areas");
        areasRef.autoFlush(true);
        areasRef.set(projectAreas[projectKey]);

        return $firebaseArray(areasRef);
      });
      
    mockLoadTimbersService.getRequiredByPanel.and.callFake(
      function (panelId) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);
        panelsRef.set(requiredPanelUsage[panelId]);

        return $firebaseArray(panelsRef);
      });
      
    mockPanelsService.panelsByCompleted.and.callFake(
      function (start, end) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);

        var panel1 = {
          id: "214-DA-P2-GF-I-100",
          project: "PROJ1",
          type: "Ext",
          area: "AREA1",
          dimensions: {
            length: 3164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };

        var panel2 = {
          id: "214-DA-P2-GF-I-111",
          project: "PROJ1",
          type: "Int",
          area: "AREA1",
          dimensions: {
            length: 2700,
            height: 270,
            width: 162,
            area: 0.7,
            weight: 30
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 346172840
          }
        };
        //11/21/2017 @ 4:00pm (UTC)
        var panel3 = {
          id: "314-DA-P2-GF-C-112",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };
         //11/21/2017 @ 4:00pm (UTC)
        var panel4 = {
          id: "314-DA-P2-GF-C-113",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };
         //11/21/2017 @ 4:00pm (UTC)
        var panel5 = {
          id: "314-DA-P2-GF-C-114",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 651345680
          }
        };
        
        var panel6 = {
          id: "314-DA-P2-GF-C-115",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };
        
        var panel7 = {
          id: "214-DA-P2-GF-H-100",
          project: "PROJ1",
          type: "Ext",
          area: "AREA4",
          dimensions: {
            length: 3164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };
        
        var panel8 = {
          id: "214-DA-P2-GF-I-101",
          project: "PROJ1",
          type: "Ext",
          area: "AREA$",
          dimensions: {
            length: 1164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };

        var panel9 = {
          id: "214-DA-P2-GF-F-102",
          project: "PROJ1",
          type: "ExtF",
          area: "AREA5",
          dimensions: {
            length: 2345,
            height: 543,
            width: 135,
            area: 4,
            weight: 99
          },
          additionalInfo: {
            framingStyle: "IFAST 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };
        
        panelsRef.set({
          "PROJ1AREA1PANEL1": panel1,
          "PROJ1AREA1PANEL2": panel2,
          "PROJ2AREA1PANEL1": panel3,
          "PROJ2AREA1PANEL2": panel4,
          "PROJ2AREA1PANEL3": panel5,
          "PROJ2AREA1PANEL4": panel6,
          "PROJ1AREA4PANEL1": panel7,
          "PROJ1AREA4PANEL2": panel8,
          "PROJ1AREA5PANEL1": panel9
        });

        return $firebaseArray(panelsRef);
      });

  }));

  beforeEach(inject(function ($rootScope, _cubicAverageService_) {
    scope = $rootScope;
    cubicAverageService = _cubicAverageService_;
  }));
  
  describe("collected data", function () {

    it("returns material by panel data", function (done) {

      defaultCuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-10-20":2,
            "2017-11-25": 10
          },
          "38 x 114": {
            "2017-10-20": 2,
            "2017-11-23": 10
          },
          "38 x 135": {
            "2017-10-20": 2,
            "2017-11-23": 10
          }
        }
      );
    
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-11-22": 2
          },
          "38 x 114": {
            "2017-11-20": 2
          },
          "38 x 135": {
            "2017-11-20": 2
          }
        }
      );
      
      var data = cubicAverageService.cubicAverageList;
      
      var date = "2017-11-20";

      cubicAverageService.init(date)
        .then(function () {

          expect(Object.keys(data).length).toEqual(6);
        
          expect(data["DayType"]["2017-11-22"]["SIP"]["average"]).toEqual(19409412);
          expect(data["DayType"]["2017-11-22"]["SIP"]["panels"].length).toEqual(1);
          expect(data["DayType"]["2017-11-22"]["HSIP"]["average"]).toEqual(8664000);
          expect(data["DayType"]["2017-11-22"]["HSIP"]["panels"].length).toEqual(2);
          expect(data["DayType"]["2017-11-22"]["IFAST"]["average"]).toEqual(42975056.25);
          expect(data["DayType"]["2017-11-22"]["IFAST"]["panels"].length).toEqual(1);
          expect(data["DayType"]["2017-11-20"]["TF"]["average"]).toEqual(29585714.285714287);
          expect(data["DayType"]["2017-11-20"]["TF"]["panels"].length).toEqual(1);
          expect(data["DayType"]["2017-11-16"]["CASS"]["average"]).toEqual(1150555.5555555555);
          expect(data["DayType"]["2017-11-16"]["CASS"]["panels"].length).toEqual(2);
          expect(data["DayType"]["2017-11-17"]["CASS"]["average"]).toEqual(962666.6666666666);
          expect(data["DayType"]["2017-11-17"]["CASS"]["panels"].length).toEqual(1);

          expect(data["WeekType"]["2017-11-20"]["SIP"]["average"]).toEqual(19409412);
          expect(data["WeekType"]["2017-11-20"]["SIP"]["panels"].length).toEqual(1);
          expect(data["WeekType"]["2017-11-20"]["HSIP"]["average"]).toEqual(8664000);
          expect(data["WeekType"]["2017-11-20"]["HSIP"]["panels"].length).toEqual(2);
          expect(data["WeekType"]["2017-11-20"]["IFAST"]["average"]).toEqual(42975056.25);
          expect(data["WeekType"]["2017-11-20"]["IFAST"]["panels"].length).toEqual(1);
          expect(data["WeekType"]["2017-11-20"]["TF"]["average"]).toEqual(29585714.285714287);
          expect(data["WeekType"]["2017-11-20"]["TF"]["panels"].length).toEqual(1);
          expect(data["WeekType"]["2017-11-13"]["CASS"]["average"]).toEqual(618320.987654321);
          expect(data["WeekType"]["2017-11-13"]["CASS"]["panels"].length).toEqual(3);
          
          expect(data["MonthType"]["2017-11"]["SIP"]["average"]).toEqual(19409412);
          expect(data["MonthType"]["2017-11"]["SIP"]["panels"].length).toEqual(1);
          expect(data["MonthType"]["2017-11"]["HSIP"]["average"]).toEqual(8664000);
          expect(data["MonthType"]["2017-11"]["HSIP"]["panels"].length).toEqual(2);
          expect(data["MonthType"]["2017-11"]["IFAST"]["average"]).toEqual(42975056.25);
          expect(data["MonthType"]["2017-11"]["IFAST"]["panels"].length).toEqual(1);
          expect(data["MonthType"]["2017-11"]["TF"]["average"]).toEqual(29585714.285714287);
          expect(data["MonthType"]["2017-11"]["TF"]["panels"].length).toEqual(1);
          expect(data["MonthType"]["2017-11"]["CASS"]["average"]).toEqual(618320.987654321);
          expect(data["MonthType"]["2017-11"]["CASS"]["panels"].length).toEqual(3);
          
          done();
          
        });
        
      scope.$apply();
      
    });
  });
  
});
