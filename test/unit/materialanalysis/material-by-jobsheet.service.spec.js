"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Material By Jobsheet Service", function () {
  var scope;

  var firebaseRoot;
  
  var projectsRef;
  var panelsRef;
  var requiredPanelUsageRef;
  var cuttingListRef;
  var jobsheetsRef;
  var defaultCuttingTypesPriceRef;
  var cuttingTypesPriceRef;
  var srdRef;
  
  var mockSchema;
  var mockAreasService;
  var mockPanelsService;
  
  var requiredPanelUsage1;
  var requiredPanelUsage2;
  var requiredPanelUsage3;
  var requiredPanelUsage4;
  
  var jobsheet1;
  var jobsheet2;
  
  var cuttingList1;
  var cuttingList2;
  var cuttingList3;
  
  var materialByJobsheetService;

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["getPanelById"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");
    cuttingListRef = firebaseRoot.child("cuttingList");
    jobsheetsRef = firebaseRoot.child("jobsheets");
    cuttingTypesPriceRef = firebaseRoot.child("cuttingTypesPrice");
    defaultCuttingTypesPriceRef = firebaseRoot.child("defaultCuttingTypesPrice");
    srdRef = firebaseRoot.child("srd");
    
    utils.addQuerySupport(requiredPanelUsageRef);
    utils.addQuerySupport(cuttingListRef);
    utils.addQuerySupport(jobsheetsRef);
    utils.addQuerySupport(panelsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(function () {
    projectsRef.set(
      {
        "PROJ1": {
          "id": "214"
        },
        "PROJ2": {
          "id": "314"
        }
      }
    );
  });
  
  beforeEach(function () {
    srdRef.child("cuttingTypes").set(
      {
        "38 x 109": true,
        "38 x 114": true,
        "38 x 135": true
      }
    );
  });
  
  beforeEach(function () {

    requiredPanelUsage1 = {
      "id": "214-DA-P2-GF-H-100",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 1,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 2343
      },
      "project": "PROJ1",
      "panel": "PANEL1"
    };
      
    requiredPanelUsage2 = {
      "id": "214-DA-P2-GF-H-100",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 5,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 2343
      },
      "project": "PROJ1",
      "panel": "PANEL1"
    };
    
    requiredPanelUsage3 = {
      "id": "214-DA-P2-GF-H-110",
      "jobsheet": "JS-214-P3-GF-H-003",
      "usageDetails": {
        "amount": 2,
        "width": 38,
        "height": 109,
        "lenghtInMeters": 5000
      },
      "project": "PROJ1",
      "panel": "PANEL1"
    };
    
    requiredPanelUsage4 = {
      "id": "214-DA-P2-GF-H-110",
      "jobsheet": "JS-214-P2-GF-H-002",
      "usageDetails": {
        "amount": 2,
        "width": 38,
        "height": 114,
        "lenghtInMeters": 5000
      },
      "project": "PROJ1",
      "panel": "PANEL1"
    };
    
    jobsheet1 = {
      "jobsheet": "JS-214-P2-GF-H-002",
      "projectId": "214",
      "cuttingList": {
        "cuttingList1": true,
        "cuttingList2": true
      },
      "started": new Date("2017-11-20T10:30:00+01:00").getTime(),
      "completed": new Date("2017-11-20T10:30:00+01:00").getTime()
    };
          
    jobsheet2 = {
      "jobsheet": "JS-214-P2-GF-H-003",
      "projectId": "214",
      "cuttingList": {
        "cuttingList3": true
      },
      "started": new Date("2017-11-21T10:30:00+01:00").getTime(),
      "completed": new Date("2017-11-21T10:30:00+01:00").getTime()
    };
    
    cuttingList1 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":4686,
        "actual":1000
      }
    };
    
    cuttingList2 = {
      "type":"38 x 114",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
    
    cuttingList3 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
        
  });

  beforeEach(inject(function ($firebaseArray) {
    
    var projectAreas = {
      "PROJ1": {
        "AREA1": {
          "phase": "P2",
          "floor": "GF",
          "type": "Ext"
        },
        "AREA2": {
          "phase": "P2",
          "floor": "GF",
          "type": "Int"
        }
      },
      "PROJ2": {
        "AREA3": {
          "phase": "P2",
          "floor": "GF",
          "type": "Roof"
        }
      }
    };
    
    var areaPanels = {
      "214-DA-P2-GF-H-100": {
        "PANEL1": {
          id: "214-DA-P2-GF-H-100",
          project: "PROJ1",
          type: "Ext",
          area: "AREA1",
          dimensions: {
            length: 1164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        }
      },
      "214-DA-P2-GF-L-111": {
        "PANEL2": {
          id: "214-DA-P2-GF-L-111",
          project: "PROJ1",
          type: "Int",
          area: "AREA2",
          dimensions: {
            length: 2700,
            height: 270,
            width: 162,
            area: 0.7,
            weight: 30
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        }
      },
      "314-DA-P2-GF-C-112": {
        "PANEL3": {
          id: "314-DA-P2-GF-C-112",
          project: "PROJ2",
          type: "Roof",
          area: "AREA3",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          }
        } 
      }
    };
    
    mockAreasService.getProjectAreas.and.callFake(
      function (projectKey) {
        var areasRef = new MockFirebase("MockQuery://areas");
        areasRef.autoFlush(true);
        areasRef.set(projectAreas[projectKey]);

        return $firebaseArray(areasRef);
      });
      
    mockPanelsService.getPanelById.and.callFake(
      function (panelId) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);
        panelsRef.set(areaPanels[panelId]);

        return $firebaseArray(panelsRef);
      });
    
  }));

  beforeEach(inject(function ($rootScope, _materialByJobsheetService_) {
    scope = $rootScope;
    materialByJobsheetService = _materialByJobsheetService_;
  }));
  
  describe("collected data", function () {

    it("returns material by jobsheet data", function (done) {
      
      jobsheetsRef.set({
        "jobsheet1": jobsheet1,
        "jobsheet2": jobsheet2
      });
      
      cuttingListRef.set({
        "cuttingList1": cuttingList1,
        "cuttingList2": cuttingList2,
        "cuttingList3": cuttingList3
      });
      
      defaultCuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-10-20":2,
            "2017-11-25": 10
          },
          "38 x 114": {
            "2017-10-20": 2,
            "2017-11-23": 10
          },
          "38 x 135": {
            "2017-10-20": 2,
            "2017-11-23": 10
          }
        }
      );
    
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-11-22": 2
          },
          "38 x 114": {
            "2017-11-20": 2
          },
          "38 x 135": {
            "2017-11-20": 2
          }
        }
      );
      
      var data = materialByJobsheetService.jobsheetCuttingList;
     
      materialByJobsheetService.init()
        .then(function () {

          expect(Object.keys(data).length).toEqual(11);
        
          expect(data["JobSheetData"]["JS-214-P2-GF-H-002"]["totalRequired"]).toEqual(9686);
          expect(data["JobSheetData"]["JS-214-P2-GF-H-002"]["totalActual"]).toEqual(1000);
          expect(data["JobSheetData"]["JS-214-P2-GF-H-003"]["totalRequired"]).toEqual(5000);
          expect(data["JobSheetData"]["JS-214-P2-GF-H-003"]["totalActual"]).toEqual(0);
        
          expect(data["Day"]["2017-11-20"]["38 x 109"]["totalRequired"]).toEqual(4686);
          expect(data["Day"]["2017-11-20"]["38 x 109"]["requiredAmount"]).toEqual(46860);
          expect(data["Day"]["2017-11-20"]["38 x 109"]["totalActual"]).toEqual(1000);
          expect(data["Day"]["2017-11-20"]["38 x 109"]["actualAmount"]).toEqual(10000);
          expect(data["Day"]["2017-11-21"]["38 x 109"]["totalRequired"]).toEqual(5000);
          expect(data["Day"]["2017-11-21"]["38 x 109"]["requiredAmount"]).toEqual(50000);
          expect(data["Day"]["2017-11-21"]["38 x 109"]["totalActual"]).toEqual(0);
          expect(data["Day"]["2017-11-21"]["38 x 109"]["actualAmount"]).toEqual(0);
          expect(data["Week"]["2017-11-20"]["38 x 109"]["totalRequired"]).toEqual(9686);
          expect(data["Week"]["2017-11-20"]["38 x 109"]["requiredAmount"]).toEqual(96860);
          expect(data["Week"]["2017-11-20"]["38 x 109"]["totalActual"]).toEqual(1000);
          expect(data["Week"]["2017-11-20"]["38 x 109"]["actualAmount"]).toEqual(10000);
          expect(data["Month"]["2017-11"]["38 x 109"]["totalRequired"]).toEqual(9686);
          expect(data["Month"]["2017-11"]["38 x 109"]["requiredAmount"]).toEqual(96860);
          expect(data["Month"]["2017-11"]["38 x 109"]["totalActual"]).toEqual(1000);
          expect(data["Month"]["2017-11"]["38 x 109"]["actualAmount"]).toEqual(10000);
          
          expect(data["Day"]["2017-11-20"]["38 x 114"]["totalRequired"]).toEqual(5000);
          expect(data["Day"]["2017-11-20"]["38 x 114"]["requiredAmount"]).toEqual(10000);
          expect(data["Day"]["2017-11-20"]["38 x 114"]["totalActual"]).toEqual(0);
          expect(data["Day"]["2017-11-20"]["38 x 114"]["actualAmount"]).toEqual(0);
          expect(data["Week"]["2017-11-20"]["38 x 114"]["totalRequired"]).toEqual(5000);
          expect(data["Week"]["2017-11-20"]["38 x 114"]["requiredAmount"]).toEqual(10000);
          expect(data["Week"]["2017-11-20"]["38 x 114"]["totalActual"]).toEqual(0);
          expect(data["Week"]["2017-11-20"]["38 x 114"]["actualAmount"]).toEqual(0);
          expect(data["Month"]["2017-11"]["38 x 114"]["totalRequired"]).toEqual(5000);
          expect(data["Month"]["2017-11"]["38 x 114"]["requiredAmount"]).toEqual(10000);
          expect(data["Month"]["2017-11"]["38 x 114"]["totalActual"]).toEqual(0);
          expect(data["Month"]["2017-11"]["38 x 114"]["actualAmount"]).toEqual(0);
          
          expect(data["DayTotal"]["2017-11-20"]["totalRequired"]).toEqual(9686);
          expect(data["DayTotal"]["2017-11-20"]["requiredAmount"]).toEqual(56860);
          expect(data["DayTotal"]["2017-11-20"]["totalActual"]).toEqual(1000);
          expect(data["DayTotal"]["2017-11-20"]["actualAmount"]).toEqual(10000);
          expect(data["DayTotal"]["2017-11-21"]["totalRequired"]).toEqual(5000);
          expect(data["DayTotal"]["2017-11-21"]["requiredAmount"]).toEqual(50000);
          expect(data["DayTotal"]["2017-11-21"]["totalActual"]).toEqual(0);
          expect(data["DayTotal"]["2017-11-21"]["actualAmount"]).toEqual(0);
          expect(data["WeekTotal"]["2017-11-20"]["totalRequired"]).toEqual(14686);
          expect(data["WeekTotal"]["2017-11-20"]["requiredAmount"]).toEqual(106860);
          expect(data["WeekTotal"]["2017-11-20"]["totalActual"]).toEqual(1000);
          expect(data["WeekTotal"]["2017-11-20"]["actualAmount"]).toEqual(10000);
          expect(data["MonthTotal"]["2017-11"]["totalRequired"]).toEqual(14686);
          expect(data["MonthTotal"]["2017-11"]["requiredAmount"]).toEqual(106860);
          expect(data["MonthTotal"]["2017-11"]["totalActual"]).toEqual(1000);
          expect(data["MonthTotal"]["2017-11"]["actualAmount"]).toEqual(10000);
      
          
          done();
          
        });
        
      scope.$apply();
      
    });
  });
  
});
