"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Cutting Type Pricing Service", function () {
  var scope;
  var createdTimestamp;
  var systemTimestamp;
  

  var firebaseRoot;
  var defaultCuttingTypesPriceRef;
  var cuttingTypesPriceRef;
  var srdRef;
  
  var mockSchema;

  var cuttingTypePricingService;

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    cuttingTypesPriceRef = firebaseRoot.child("cuttingTypesPrice");
    defaultCuttingTypesPriceRef = firebaseRoot.child("defaultCuttingTypesPrice");
    srdRef = firebaseRoot.child("srd");

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });


  beforeEach(function () {
    srdRef.child("cuttingTypes").set(
      {
        "38 x 109": true,
        "38 x 114": true,
        "38 x 135": true
      }
    );
  });

  beforeEach(inject(function ($rootScope, _cuttingTypePricingService_) {
    scope = $rootScope;
    cuttingTypePricingService = _cuttingTypePricingService_;
  }));
  
  describe("collected data", function () {

    it("returns cutting type prices", function (done) {

      defaultCuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-10-20":2,
            "2017-11-25": 10
          },
          "38 x 114": {
            "2017-10-20": 2,
            "2017-11-23": 10
          },
          "38 x 135": {
            "2017-10-20": 2,
            "2017-11-23": 10
          }
        }
      );
    
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-11-22": 2
          },
          "38 x 114": {
            "2017-11-20": 2
          },
          "38 x 135": {
            "2017-11-20": 2
          }
        }
      );
      
      var data = cuttingTypePricingService.cuttingTypesPricesList;

      cuttingTypePricingService.init()
        .then(function () {

          expect(Object.keys(data).length).toEqual(5);
        
          expect(data.defaultCuttingTypesPrices["38 x 109"]).toEqual(jasmine.objectContaining({
            "2017-10-20":2,
            "2017-11-25": 10
          }));
          
          expect(data.defaultCuttingTypesPrices["38 x 114"]).toEqual(jasmine.objectContaining({
            "2017-10-20": 2,
            "2017-11-23": 10
          }));
          
          expect(data.defaultCuttingTypesPrices["38 x 135"]).toEqual(jasmine.objectContaining({
            "2017-10-20":2,
            "2017-11-23": 10
          }));
          
          expect(data.cuttingTypesPrices["38 x 109"]).toEqual(jasmine.objectContaining({
            "2017-11-22":2
          }));
          
          expect(data.cuttingTypesPrices["38 x 114"]).toEqual(jasmine.objectContaining({
            "2017-11-20": 2
          }));
          
          expect(data.cuttingTypesPrices["38 x 135"]).toEqual(jasmine.objectContaining({
            "2017-11-20":2
          }));
        
          done();
          
        });
        
      scope.$apply();
      
    });
  });
  
  
  describe("updates prices", function () {
    it("updates and adds default price", function () {
      defaultCuttingTypesPriceRef.set(
        {
          "38 x 109": {"2017-11-01": 10},
          "38 x 114": {"2017-11-02": 10},
          "38 x 135": {"2017-11-03": 10}
        }
      );

      cuttingTypePricingService.init()
        .then(function () {

          cuttingTypePricingService.updateDefaultPrice("38 x 109", "2017-11-01", 11);
          scope.$apply();
          cuttingTypePricingService.updateDefaultPrice("38 x 114", "2017-11-02", 12);
          scope.$apply();
          cuttingTypePricingService.updateDefaultPrice("38 x 135", "2017-11-05", 18);
          scope.$apply();

          expect(defaultCuttingTypesPriceRef.getData()).toEqual(
            {
              "38 x 109": {
                "2017-11-01": 11
              },
              "38 x 114": {
                "2017-11-02": 12
              },
              "38 x 135": {
                "2017-11-03": 10,
                "2017-11-05": 18
              }
            }
          );

        });

    });

    it("updates and adds price for date given", function () {
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {"2017-11-01": 10},
          "38 x 114": {"2017-11-02": 10},
          "38 x 135": {"2017-11-03": 10}
        }
      );

      cuttingTypePricingService.init()
        .then(function () {

          cuttingTypePricingService.updatePrice("38 x 109", "2017-11-01", 21);
          scope.$apply();
          cuttingTypePricingService.updatePrice("38 x 114", "2017-11-02", 22);
          scope.$apply();
          cuttingTypePricingService.updatePrice("38 x 135", "2017-11-04", 23);
          scope.$apply();

          expect(cuttingTypesPriceRef.getData()).toEqual(
            {
              "38 x 109": {
                "2017-11-01": 21
              },
              "38 x 114": {
                "2017-11-02": 22
              },
              "38 x 135": {
                "2017-11-03": 10,
                "2017-11-04": 23
              }
            }
          );

        });

    });
    
    it("updates and adds stock for date given", function () {
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {"2017-11-01": 10},
          "38 x 114": {"2017-11-02": 10},
          "38 x 135": {"2017-11-03": 10}
        }
      );

      cuttingTypePricingService.init()
        .then(function () {

          cuttingTypePricingService.updateStock("38 x 109", "2017-11-01", 21);
          scope.$apply();
          cuttingTypePricingService.updateStock("38 x 114", "2017-11-02", 22);
          scope.$apply();
          cuttingTypePricingService.updateStock("38 x 135", "2017-11-04", 23);
          scope.$apply();

          expect(cuttingTypesPriceRef.getData()).toEqual(
            {
              "38 x 109": {
                "2017-11-01": 21
              },
              "38 x 114": {
                "2017-11-02": 22
              },
              "38 x 135": {
                "2017-11-03": 10,
                "2017-11-04": 23
              }
            }
          );

        });

    });

  });
   

  
});
