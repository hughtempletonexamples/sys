"use strict";

var utils = require("../testUtils");

describe("materialanalysis", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var cuttingListRef;
  var jobsheetsRef;
  
  var scope;
  var mockLoadTimbersService;

  var updateTimbersReturnValue;

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  beforeEach(function () {
    mockLoadTimbersService = jasmine.createSpyObj("loadTimbersService", ["loadTimberSchedule", "createCuttingList", "updatePanelsUsage"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("loadTimbersService", function () {
        return mockLoadTimbersService;
      });
    });

    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
    cuttingListRef = firebaseRoot.child("cuttingList");
    cuttingListRef.set({});
    
    jobsheetsRef = firebaseRoot.child("jobsheets");
    jobsheetsRef.set({});

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope;
  }));

  beforeEach(inject(function ($q) {
    mockLoadTimbersService.loadTimberSchedule.and.callFake(function (file) {
      var result = { panels: [], errors: [] };

      switch (file) {
      case "File1":
        result.panels = [{id: "A"}];
        break;
      case "File2":
        result.panels = [{id: "B"}, {id: "C"}];
        result.errors = ["Error B", "Error C"];
        break;
      case "File3":
        result.panels = [{id: "D"}];
        result.errors = ["Error D"];
        break;
      default:
        result.errors = ["Unknown"];
      }

      return $q.when(result);
    });
    
    mockLoadTimbersService.updatePanelsUsage.and.callFake(function (panels) {
      return $q.when(updateTimbersReturnValue);
    });

  }));
  
  describe("controller", function () {

    it("initialises", inject(function ($controller) {
      var controller = $controller("LoadTimbersController");

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
    }));
    
    it("loads panels from files", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1"]);
      scope.$apply();

      expect(mockLoadTimbersService.loadTimberSchedule.calls.count()).toEqual(1);
      expect(mockLoadTimbersService.loadTimberSchedule.calls.argsFor(0)).toEqual(["File1", false]);

      expect(controller.files).toEqual(["File1"]);
      expect(controller.panels).toEqual([{id: "A"}]);
      expect(controller.errors).toEqual([]);

      controller.upload(["File2"]);
      scope.$apply();

      expect(mockLoadTimbersService.loadTimberSchedule.calls.count()).toEqual(2);
      expect(mockLoadTimbersService.loadTimberSchedule.calls.argsFor(1)).toEqual(["File2", false]);

      expect(controller.files).toEqual(["File1", "File2"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "B"}, {id: "C"}]);
      expect(controller.errors).toEqual(["Error B", "Error C"]);
    }));
    
    it("loads multiple files", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1", "File2"]);
      scope.$apply();

      expect(mockLoadTimbersService.loadTimberSchedule.calls.count()).toEqual(2);
      expect(mockLoadTimbersService.loadTimberSchedule.calls.argsFor(0)).toEqual(["File1", false]);
      expect(mockLoadTimbersService.loadTimberSchedule.calls.argsFor(1)).toEqual(["File2", false]);

      expect(controller.files).toEqual(["File1", "File2"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "B"}, {id: "C"}]);
      expect(controller.errors).toEqual(["Error B", "Error C"]);
    }));
    
    it("can remove file", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1", "File2", "File3"]);
      scope.$apply();

      expect(controller.files).toEqual(["File1", "File2", "File3"]);
      expect(controller.panels).toEqual([{id: "A"},{id: "B"}, {id: "C"}, {id: "D"}]);
      expect(controller.errors).toEqual(["Error B", "Error C", "Error D"]);

      controller.removeFile(1);

      expect(controller.files).toEqual(["File1", "File3"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "D"}]);
      expect(controller.errors).toEqual(["Error D"]);

      controller.removeFile(0);

      expect(controller.files).toEqual(["File3"]);
      expect(controller.panels).toEqual([{id: "D"}]);
      expect(controller.errors).toEqual(["Error D"]);

      controller.removeFile(0);

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
    }));
    
    it("does not save when errors", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File2"]);
      scope.$apply();

      expect(controller.errors.length).not.toEqual(0);

      controller.save();

      expect(mockLoadTimbersService.updatePanelsUsage).not.toHaveBeenCalled();
    }));
    
    it("can save uploaded panels", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1"]);
      scope.$apply();

      updateTimbersReturnValue = {
        total: 99,
        details: [
          {projectId: "123", projectName: "Name", count: 99}
        ]
      };

      controller.save();
      scope.$apply();

      expect(mockLoadTimbersService.updatePanelsUsage).toHaveBeenCalledTimes(1);
      expect(mockLoadTimbersService.updatePanelsUsage).toHaveBeenCalledWith([{id: "A"}]);

      expect(controller.updateSummary).toEqual(updateTimbersReturnValue);
    }));
    
    it("reports failure to save", inject(function ($controller, $window, $q) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1"]);
      scope.$apply();

      mockLoadTimbersService.updatePanelsUsage.and.returnValue($q.reject("Fail"));
      spyOn($window, "alert");

      controller.save();
      scope.$apply();

      expect($window.alert).toHaveBeenCalledWith("Fail");
    }));
    
    it("can reset file after save", inject(function ($controller) {

      var controller = $controller("LoadTimbersController");

      controller.upload(["File1"]);
      scope.$apply();

      controller.save();
      scope.$apply();

      expect(controller.files).not.toEqual([]);
      expect(controller.panels).not.toEqual([]);
      expect(controller.updateSummary).not.toEqual(null);

      controller.reset();

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
      expect(controller.updateSummary).toEqual(null);
    }));
   
  });
  
});
