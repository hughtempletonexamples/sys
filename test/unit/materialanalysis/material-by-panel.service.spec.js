"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Material By Panel Service", function () {
  var scope;
  var createdTimestamp;
  var systemTimestamp;
  

  var firebaseRoot;
  var projectsRef;
  var panelsRef;
  var requiredPanelUsageRef;
  var cuttingListRef;
  var jobsheetsRef;
  var defaultCuttingTypesPriceRef;
  var cuttingTypesPriceRef;
  var srdRef;
  
  var mockSchema;
  var mockAreasService;
  var mockPanelsService;
  var mockLoadTimbersService;

  var jobsheet1;
  var jobsheet2;
  
  var cuttingList1;
  var cuttingList2;
  var cuttingList3;
  
  var materialByPanelService;

  beforeEach(angular.mock.module("innView.materialanalysis", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  beforeEach(function () {
    var now = new Date("2017-11-25");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsByCompleted"]);
    mockLoadTimbersService = jasmine.createSpyObj("loadTimbersService", ["getRequiredByPanel"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");
    cuttingListRef = firebaseRoot.child("cuttingList");
    jobsheetsRef = firebaseRoot.child("jobsheets");
    cuttingTypesPriceRef = firebaseRoot.child("cuttingTypesPrice");
    defaultCuttingTypesPriceRef = firebaseRoot.child("defaultCuttingTypesPrice");
    srdRef = firebaseRoot.child("srd");
    
    utils.addQuerySupport(requiredPanelUsageRef);
    utils.addQuerySupport(cuttingListRef);
    utils.addQuerySupport(jobsheetsRef);
    utils.addQuerySupport(panelsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("loadTimbersService", function () {
        return mockLoadTimbersService;
      });
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(function () {
    projectsRef.set(
      {
        "PROJ1": {
          "id": "214"
        },
        "PROJ2": {
          "id": "314"
        }
      }
    );
  });
  
  beforeEach(function () {
    srdRef.child("cuttingTypes").set(
      {
        "38 x 109": true,
        "38 x 114": true,
        "38 x 135": true
      }
    );
  });
  
  beforeEach(function () {
    
    jobsheet1 = {
      "jobsheet": "JS-214-P2-GF-H-002",
      "projectId": "214",
      "cuttingList": {
        "cuttingList1": true,
        "cuttingList2": true
      },
      "started": new Date("2017-11-20T10:30:00+01:00").getTime(),
      "completed": new Date("2017-11-20T10:30:00+01:00").getTime()
    };
          
    jobsheet2 = {
      "jobsheet": "JS-214-P2-GF-H-003",
      "projectId": "214",
      "cuttingList": {
        "cuttingList3": true
      },
      "started": new Date("2017-11-21T10:30:00+01:00").getTime(),
      "completed": new Date("2017-11-21T10:30:00+01:00").getTime()
    };
    
    cuttingList1 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":4686,
        "actual":0
      }
    };
    
    cuttingList2 = {
      "type":"38 x 114",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
    
    cuttingList3 = {
      "type":"38 x 109",
      "lengths":{
        "fives":0,
        "fours":0,
        "threes":0},
      "cuttingDetails":{
        "required":5000,
        "actual":0
      }
    };
        
  });

  beforeEach(inject(function ($firebaseArray) {
    
    var projectAreas = {
      "PROJ1": {
        "AREA1": {
          "phase": "P2",
          "floor": "GF",
          "type": "Ext"
        },
        "AREA2": {
          "phase": "P2",
          "floor": "GF",
          "type": "Int"
        }
      },
      "PROJ2": {
        "AREA3": {
          "phase": "P2",
          "floor": "GF",
          "type": "Roof"
        }
      }
    };
   
    var requiredPanelUsage = {
      "PROJ1AREA1PANEL1": {
        "requiredPanelUsage1": {
          "id": "214-DA-P2-GF-H-100",
          "jobsheet": "JS-214-P2-GF-H-002",
          "usageDetails": {
            "amount": 1,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 2343
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA1PANEL1"
        }
      },
      "PROJ1AREA1PANEL2": {
        "requiredPanelUsage2": {
          "id": "214-DA-P2-GF-H-111",
          "jobsheet": "JS-214-P2-GF-H-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ1",
          "panel": "PROJ1AREA1PANEL2"
        }
      },
      "PROJ2AREA1PANEL1": {
        "requiredPanelUsage3": {
          "id": "314-DA-P2-GF-C-112",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL1"
        }
      },
      "PROJ2AREA1PANEL2": {
        "requiredPanelUsage4": {
          "id": "314-DA-P2-GF-C-113",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 109,
            "lenghtInMeters": 5000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL2"
        }
      },
      "PROJ2AREA1PANEL3": {
        "requiredPanelUsage5": {
          "id": "314-DA-P2-GF-C-115",
          "jobsheet": "JS-314-P2-GF-C-002",
          "usageDetails": {
            "amount": 2,
            "width": 38,
            "height": 114,
            "lenghtInMeters": 2000
          },
          "project": "PROJ2",
          "panel": "PROJ2AREA1PANEL4"
        }
      }
    };
    
    mockAreasService.getProjectAreas.and.callFake(
      function (projectKey) {
        var areasRef = new MockFirebase("MockQuery://areas");
        areasRef.autoFlush(true);
        areasRef.set(projectAreas[projectKey]);

        return $firebaseArray(areasRef);
      });
      
    mockLoadTimbersService.getRequiredByPanel.and.callFake(
      function (panelId) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);
        panelsRef.set(requiredPanelUsage[panelId]);

        return $firebaseArray(panelsRef);
      });
      
    mockPanelsService.panelsByCompleted.and.callFake(
      function (start, end) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);

        var panel1 = {
          id: "214-DA-P2-GF-H-100",
          project: "PROJ1",
          type: "Ext",
          area: "AREA1",
          dimensions: {
            length: 1164,
            height: 430,
            width: 162,
            area: 0.5,
            weight: 19
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 173086420
          }
        };

        var panel2 = {
          id: "214-DA-P2-GF-L-111",
          project: "PROJ1",
          type: "Int",
          area: "AREA1",
          dimensions: {
            length: 2700,
            height: 270,
            width: 162,
            area: 0.7,
            weight: 30
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 346172840
          }
        };
        //11/21/2017 @ 4:00pm (UTC)
        var panel3 = {
          id: "314-DA-P2-GF-C-112",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };
        
         //11/21/2017 @ 4:00pm (UTC)
        var panel4 = {
          id: "314-DA-P2-GF-C-113",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };
        
         //11/21/2017 @ 4:00pm (UTC)
        var panel5 = {
          id: "314-DA-P2-GF-C-114",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };
        
        var panel6 = {
          id: "314-DA-P2-GF-C-115",
          project: "PROJ2",
          type: "Roof",
          area: "AREA1",
          dimensions: {
            length: 2666,
            height: 3400,
            width: 207,
            area: 9,
            weight: 435
          },
          additionalInfo: {
            framingStyle: "SIP 213",
            studSize: "38x184",
            sheathing: "OSB 5980",
            components: 6,
            nailing: "150x150",
            spandrel: "No",
            doors: null,
            windows: null,
            pockets: null,
            qty: 1
          },
          qa: {
            completed: systemTimestamp - 692345680
          }
        };

        panelsRef.set({
          "PROJ1AREA1PANEL1": panel1,
          "PROJ1AREA1PANEL2": panel2,
          "PROJ2AREA1PANEL1": panel3,
          "PROJ2AREA1PANEL2": panel4,
          "PROJ2AREA1PANEL3": panel5,
          "PROJ2AREA1PANEL4": panel6
        });

        return $firebaseArray(panelsRef);
      });

  }));

  beforeEach(inject(function ($rootScope, _materialByPanelService_) {
    scope = $rootScope;
    materialByPanelService = _materialByPanelService_;
  }));
  
  describe("collected data", function () {

    it("returns material by panel data", function (done) {

      defaultCuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-10-20":2,
            "2017-11-25": 10
          },
          "38 x 114": {
            "2017-10-20": 2,
            "2017-11-23": 10
          },
          "38 x 135": {
            "2017-10-20": 2,
            "2017-11-23": 10
          }
        }
      );
    
      cuttingTypesPriceRef.set(
        {
          "38 x 109": {
            "2017-11-22": 2
          },
          "38 x 114": {
            "2017-11-20": 2
          },
          "38 x 135": {
            "2017-11-20": 2
          }
        }
      );
      
      var data = materialByPanelService.panelCuttingList;
      
      var date = "2017-11-20";

      materialByPanelService.init(date)
        .then(function () {

          expect(Object.keys(data).length).toEqual(12);
        
          expect(data["Day"]["2017-11-22"]["38 x 109"]["totalRequired"]).toEqual(2343);
          expect(data["Day"]["2017-11-22"]["38 x 109"]["totalAmount"]).toEqual(4686);
          expect(data["Day"]["2017-11-20"]["38 x 109"]["totalRequired"]).toEqual(5000);
          expect(data["Day"]["2017-11-20"]["38 x 109"]["totalAmount"]).toEqual(50000);
          expect(data["Day"]["2017-11-16"]["38 x 109"]["totalRequired"]).toEqual(10000);
          expect(data["Day"]["2017-11-16"]["38 x 109"]["totalAmount"]).toEqual(100000);
          //different size
          expect(data["Day"]["2017-11-16"]["38 x 114"]["totalRequired"]).toEqual(2000);
          expect(data["Day"]["2017-11-16"]["38 x 114"]["totalAmount"]).toEqual(20000);
          
          expect(data["Week"]["2017-11-20"]["38 x 109"]["totalRequired"]).toEqual(7343);
          expect(data["Week"]["2017-11-20"]["38 x 109"]["totalAmount"]).toEqual(54686);
          expect(data["Week"]["2017-11-13"]["38 x 109"]["totalRequired"]).toEqual(10000);
          expect(data["Week"]["2017-11-13"]["38 x 109"]["totalAmount"]).toEqual(100000);
          //different size
          expect(data["Week"]["2017-11-13"]["38 x 114"]["totalRequired"]).toEqual(2000);
          expect(data["Week"]["2017-11-13"]["38 x 114"]["totalAmount"]).toEqual(20000);
          
          expect(data["Month"]["2017-11"]["38 x 109"]["totalRequired"]).toEqual(17343);
          expect(data["Month"]["2017-11"]["38 x 109"]["totalAmount"]).toEqual(154686);
          //different size
          expect(data["Month"]["2017-11"]["38 x 114"]["totalRequired"]).toEqual(2000);
          expect(data["Month"]["2017-11"]["38 x 114"]["totalAmount"]).toEqual(20000);

          expect(data["DayTotal"]["2017-11-22"]["totalRequired"]).toEqual(2343);
          expect(data["DayTotal"]["2017-11-22"]["totalAmount"]).toEqual(4686);
          expect(data["DayTotal"]["2017-11-20"]["totalRequired"]).toEqual(5000);
          expect(data["DayTotal"]["2017-11-20"]["totalAmount"]).toEqual(50000);
          expect(data["DayTotal"]["2017-11-16"]["totalRequired"]).toEqual(12000);
          expect(data["DayTotal"]["2017-11-16"]["totalAmount"]).toEqual(120000);
          
          expect(data["WeekTotal"]["2017-11-20"]["totalRequired"]).toEqual(7343);
          expect(data["WeekTotal"]["2017-11-20"]["totalAmount"]).toEqual(54686);
          expect(data["WeekTotal"]["2017-11-13"]["totalRequired"]).toEqual(12000);
          expect(data["WeekTotal"]["2017-11-13"]["totalAmount"]).toEqual(120000);
          
          expect(data["MonthTotal"]["2017-11"]["totalRequired"]).toEqual(19343);
          expect(data["MonthTotal"]["2017-11"]["totalAmount"]).toEqual(174686);
          
          done();
          
        });
        
      scope.$apply();
      
    });
  });
  
});
