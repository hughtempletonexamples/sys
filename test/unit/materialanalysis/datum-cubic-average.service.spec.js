"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Datum cubic average service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var panelsRef;
  var datumsRef;
  var referenceDataRef;
  var areaPlansRef;
  var requiredPanelUsageRef;

  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;
  var mockLoadTimbersService;

  var scope;
  var datumCubicAverageService;

  var project1 = {
    "name": "Project 1",
    "id": "001",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV0": "2016-05-01",
      "PROJ1REV1": "2016-06-01"
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-01").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };

  var area2 = {
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-05-25"
    },
    "qa": {
      "completed": new Date("2016-05-10T14:32:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:31:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };
  
  var area3 = {
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-05-25"
    },
    "qa": {
      "completed": new Date("2016-05-10T14:32:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:31:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:34:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:33:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:36:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:35:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-21T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:38:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:37:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel4 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-002",
    "project": "PROJ1",
    "type": "Int",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:39:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T11:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel5 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-003",
    "project": "PROJ1",
    "type": "Int",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T12:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  var panel6 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-H-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T12:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel7 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-I-003",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T12:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  var requiredPanelUsage1 = {
    "id": "001-DA-P1-GF-I-001",
    "jobsheet": "JS-001-P1-GF-I-001",
    "usageDetails": {
      "amount": 1,
      "width": 38,
      "height": 109,
      "lenghtInMeters": 2343
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA1PANEL1"
  };

  var requiredPanelUsage2 = {
    "id": "001-DA-P1-GF-I-002",
    "jobsheet": "JS-001-P1-GF-I-001",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 109,
      "lenghtInMeters": 5000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA1PANEL2"
  };

  var requiredPanelUsage3 = {
    "id": "001-DA-P2-GF-I-001",
    "jobsheet": "JS-001-P2-GF-I-002",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 109,
      "lenghtInMeters": 5000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA2PANEL1"
  };

  var requiredPanelUsage4 = {
    "id": "001-DA-P2-GF-I-002",
    "jobsheet": "JS-001-P2-GF-I-002",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 109,
      "lenghtInMeters": 5000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA2PANEL2"
  };

  var requiredPanelUsage5 = {
    "id": "001-DA-P2-GF-I-003",
    "jobsheet": "JS-001-P2-GF-I-002",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 114,
      "lenghtInMeters": 2000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA2PANEL4"
  };
  
  var requiredPanelUsage6 = {
    "id": "001-DA-P3-GF-H-002",
    "jobsheet": "JS-001-P2-GF-H-002",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 109,
      "lenghtInMeters": 5000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA2PANEL1"
  };

  var requiredPanelUsage7 = {
    "id": "001-DA-P3-GF-I-003",
    "jobsheet": "JS-001-P2-GF-I-002",
    "usageDetails": {
      "amount": 2,
      "width": 38,
      "height": 114,
      "lenghtInMeters": 2000
    },
    "project": "PROJ1",
    "panel": "PROJ1AREA3PANEL2"
  };

  beforeEach(angular.mock.module("innView.admin", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    datumsRef = firebaseRoot.child("datums");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");

    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(datumsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(areaPlansRef);
    utils.addQuerySupport(requiredPanelUsageRef);

    mockLoadTimbersService = jasmine.createSpyObj("loadTimbersService", ["getRequiredByProject"]);
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForProject", "getPanelById"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getDatumTypes", "getProductionLines", "getProductTypes"]);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("loadTimbersService", function () {
        return mockLoadTimbersService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  
  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5,
      "panel6": panel6,
      "panel7": panel7
    });
    
    requiredPanelUsageRef.set({
      "requiredPanelUsage1": requiredPanelUsage1,
      "requiredPanelUsage2": requiredPanelUsage2,
      "requiredPanelUsage3": requiredPanelUsage3,
      "requiredPanelUsage4": requiredPanelUsage4,
      "requiredPanelUsage5": requiredPanelUsage5,
      "requiredPanelUsage6": requiredPanelUsage6,
      "requiredPanelUsage7": requiredPanelUsage7
    });

    referenceDataRef.child("datumTypes").set({
      "Education 1 Storey": {
        $id: "Education 1 Storey",
        "name": "Education - 1 Storey"
      },
      "Education 2 Storey": {
        $id: "Education 2 Storey",
        "name": "Education - 2 Storey"
      },
      "Education 3 Storey": {
        $id: "Education 3 Storey",
        "name": "Education - 3 Storey"
      },
      "Education Student Accommodation": {
        $id: "Education Student Accommodation",
        "name": "Education - Student Accommodation"
      }
    });
    
    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );

    referenceDataRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 35 },
        "CASS": { defaultCapacity: 6 }
      }
    );
  
  });
  
  beforeEach(inject(function ($firebaseArray) {
    mockLoadTimbersService.getRequiredByProject.and.callFake(function () {
      return $firebaseArray(requiredPanelUsageRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForProject.and.callFake(function (projectRef) {
      return $firebaseArray(panelsRef
                            .orderByChild("project")
                            .equalTo(projectRef));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getDatumTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("datumTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productionLines"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {

    var areaPanels = {
      "001-DA-P1-GF-I-001": {
        "PANEL1": panel1
      },
      "001-DA-P1-GF-I-002":{
        "PANEL2": panel2
      },
      "001-DA-P2-GF-I-001":{
        "PANEL3": panel3
      },
      "001-DA-P2-GF-I-002":{
        "PANEL4": panel4
      },
      "001-DA-P2-GF-I-003":{
        "PANEL5": panel5
      },
      "001-DA-P3-GF-H-002":{
        "PANEL6": panel6
      },
      "001-DA-P3-GF-I-003":{
        "PANEL7": panel7
      }
    };
    
    mockPanelsService.getPanelById.and.callFake(
      function (panelId) {
        var panelsRef = new MockFirebase("MockQuery://panels");
        panelsRef.autoFlush(true);
        panelsRef.set(areaPanels[panelId]);

        return $firebaseArray(panelsRef);
      });
  }));

  beforeEach(inject(function ($rootScope, _datumCubicAverageService_) {
    scope = $rootScope;
    datumCubicAverageService = _datumCubicAverageService_;
  }));

  describe("calculateUsageByDatum", function () {

    it("gets average mm of timber from each datum", function () {

      datumCubicAverageService.calculateUsageByDatum()
        .then(function (data) {
          //for all data
          expect(data["Education Student Accommodation"].average).toEqual(4484600.244897959);
          expect(data["Education Student Accommodation"].volume).toEqual(109872706);
          expect(data["Education Student Accommodation"].meters).toEqual(31392201.714285713);
          expect(data["Education Student Accommodation"].panelAreaTotal).toEqual(3.5);
          expect(data["Education Student Accommodation"].panels.length).toEqual(7);
          
          //SIP's
          expect(data["Education Student Accommodation"].productionTypes.SIP.average).toEqual(11361045.777777778);
          expect(data["Education Student Accommodation"].productionTypes.SIP.volume).toEqual(51124706);
          expect(data["Education Student Accommodation"].productionTypes.SIP.meters).toEqual(34083137.333333336);
          expect(data["Education Student Accommodation"].productionTypes.SIP.panelAreaTotal).toEqual(1.5);
          expect(data["Education Student Accommodation"].productionTypes.SIP.panels.length).toEqual(3);
          
          //HSIP's
          expect(data["Education Student Accommodation"].productionTypes.HSIP.average).toEqual(14687000);
          expect(data["Education Student Accommodation"].productionTypes.HSIP.volume).toEqual(29374000);
          expect(data["Education Student Accommodation"].productionTypes.HSIP.meters).toEqual(29374000);
          expect(data["Education Student Accommodation"].productionTypes.HSIP.panelAreaTotal).toEqual(1);
          expect(data["Education Student Accommodation"].productionTypes.HSIP.panels.length).toEqual(2);
          
          //TF's
          expect(data["Education Student Accommodation"].productionTypes.TF.average).toEqual(14687000);
          expect(data["Education Student Accommodation"].productionTypes.TF.volume).toEqual(29374000);
          expect(data["Education Student Accommodation"].productionTypes.TF.meters).toEqual(29374000);
          expect(data["Education Student Accommodation"].productionTypes.TF.panelAreaTotal).toEqual(1);
          expect(data["Education Student Accommodation"].productionTypes.TF.panels.length).toEqual(2);
         
          //CASS's
          expect(data["Education Student Accommodation"].productionTypes.CASS.average).toEqual(0);
          expect(data["Education Student Accommodation"].productionTypes.CASS.volume).toEqual(0);
          expect(data["Education Student Accommodation"].productionTypes.CASS.meters).toEqual(0);
          expect(data["Education Student Accommodation"].productionTypes.CASS.panelAreaTotal).toEqual(0);
          expect(data["Education Student Accommodation"].productionTypes.CASS.panels.length).toEqual(0);
        });

      scope.$apply();

    });
  });

 
});
