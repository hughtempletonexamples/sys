"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Datums service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var panelsRef;
  var datumsRef;
  var referenceDataRef;
  var areaPlansRef;

  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;

  var scope;
  var datumsService;

  var project1 = {
    "name": "Project 1",
    "id": "001",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV0": "2016-05-01",
      "PROJ1REV1": "2016-06-01"
    },
    "timestamps": {
      "created": new Date("2016-04-01").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };

  var area2 = {
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-05-25"
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };
  
  var area3 = {
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-05-26"
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-21T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel4 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-001",
    "project": "PROJ1",
    "type": "Int",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel5 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-I-001",
    "project": "PROJ1",
    "type": "Int",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel6 = {
    "area": "AREA3",
    "id": "001-DA-P2-GF-H-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1062,
      "height": 430,
      "length": 6100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel7 = {
    "area": "AREA3",
    "id": "001-DA-P2-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1,
      "height": 430,
      "length": 6,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel8 = {
    "area": "AREA3",
    "id": "001-DA-P2-GF-F-003",
    "project": "PROJ1",
    "type": "ExtF",
    "dimensions": {
      "area": 0.5,
      "width": 1,
      "height": 430,
      "length": 6,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.admin", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    datumsRef = firebaseRoot.child("datums");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");

    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(datumsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(areaPlansRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForProject"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getDatumTypes", "getProductionLines", "getProductTypes"]);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  
  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5,
      "panel6": panel6,
      "panel7": panel7,
      "panel8": panel8
    });

    referenceDataRef.child("datumTypes").set({
      "Education 1 Storey": {
        $id: "Education 1 Storey",
        "name": "Education - 1 Storey"
      },
      "Education 2 Storey": {
        $id: "Education 2 Storey",
        "name": "Education - 2 Storey"
      },
      "Education 3 Storey": {
        $id: "Education 3 Storey",
        "name": "Education - 3 Storey"
      },
      "Education Student Accommodation": {
        $id: "Education Student Accommodation",
        "name": "Education - Student Accommodation"
      }
    });
    
    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "ExtF": { productionLine: "IFAST" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );

    referenceDataRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "IFAST": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 35 },
        "CASS": { defaultCapacity: 6 }
      }
    );
  
  });

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForProject.and.callFake(function (projectRef) {
      return $firebaseArray(panelsRef
                            .orderByChild("project")
                            .equalTo(projectRef));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getDatumTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("datumTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productionLines"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(inject(function ($rootScope, _datumsService_) {
    scope = $rootScope;
    datumsService = _datumsService_;
  }));

  describe("averageMeterSquareOfDatum", function () {

    it("gets average m2 of panel from each datum", function () {

      datumsService.averageMeterSquareOfDatum()
        .then(function (data) {
          expect(data["Education Student Accommodation"].name).toEqual("Education Student Accommodation");
          expect(data["Education Student Accommodation"].productionLines.SIP.panelCount).toEqual(3);
          expect(data["Education Student Accommodation"].productionLines.SIP.panelAreaTotal).toEqual(2);
          expect(data["Education Student Accommodation"].productionLines.SIP.panelAvg).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.HSIP.panelCount).toEqual(2);
          expect(data["Education Student Accommodation"].productionLines.HSIP.panelAreaTotal).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.HSIP.panelAvg).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.IFAST.panelCount).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.IFAST.panelAreaTotal).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.IFAST.panelAvg).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.TF.panelCount).toEqual(2);
          expect(data["Education Student Accommodation"].productionLines.TF.panelAreaTotal).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.TF.panelAvg).toEqual(1);
          expect(data["Education Student Accommodation"].productionLines.CASS.panelCount).toEqual(0);
          expect(data["Education Student Accommodation"].productionLines.CASS.panelAreaTotal).toEqual(0);
          expect(data["Education Student Accommodation"].productionLines.CASS.panelAvg).toEqual(0);
        });

      scope.$apply();

    });
  });
  
  describe("updatePanels", function () {

    it("saves new panels", function (done) {

      var datum = {
        name: "Education 1 Storey",
        panelAreaTotal: "168",
        panelAvg: "2",
        panelAvgOverride: "4",
        panelCount: "77"
      };
      
      var datumName = "Education 1 Storey";
      
      datumsService.updateDatum(datumName, [datum])
        .then(function (result) {
          var savedDatum = utils.getRefChild(datumsRef, 0);
          expect(savedDatum[0]).toEqual(jasmine.objectContaining(datum));

          done();
        });

      scope.$apply();
    });

    it("updates existing datum", function (done) {

      datumsRef.set({
        DATUM1: {
          name: "Education 1 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        }
      });

      var datum = {
        name: "Education 1 Storey",
        panelAreaTotal: "168",
        panelAvg: "2",
        panelAvgOverride: "4",
        panelCount: "77"
      };
      
      var datumName = "Education 1 Storey";

      datumsService.updateDatum(datumName, [datum])
        .then(function () {
          var savedPanel = utils.getRefChild(datumsRef, 0);
          expect(savedPanel[0]).toEqual(jasmine.objectContaining(datum));

          done();
        });

      scope.$apply();
    });
    
  });
  
  describe("damumByName", function () {

    it("returns datum by name", function () {

      datumsRef.set({
        DATUM1: {
          name: "Education 1 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM2: {
          name: "Education 2 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM3: {
          name: "Education 3 Storey",
          panelAreaTotal: "300",
          panelAvg: "6",
          panelAvgOverride: "0",
          panelCount: "50"
        }
      });

      var datum = {
        name: "Education 1 Storey",
        panelAreaTotal: "168",
        panelAvg: "2",
        panelAvgOverride: "0",
        panelCount: "77"
      };

      var datumName = "Education 1 Storey";

      var result = datumsService.damumByName(datumName);

      scope.$apply();
      expect(result.length).toEqual(1);
      expect(result[0]).toEqual(jasmine.objectContaining(datum));
    });
  });


});
