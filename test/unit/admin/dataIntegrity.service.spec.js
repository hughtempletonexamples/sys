"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");
describe("dataIntegrity service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var referenceDataRef;
  var materialsRequestRef;
  var gatewayRef;
  var mockProjectsService;
  var mockAreasService;
  var mockMaterialsRequestService;
  var scope;
  var dataIntegrityService;

  var project1 = {
    "name": "Project1",
    "id": "001",  
    "active":true,
    "chainOfCustody":"PEFC",
    "siteAccess":"Rigid",
    "specialRequirements":true,
    "RevisedPanData(50%)":true,
    "areasAbove10Percent": false,
    "deliverySchedule": {
      "published": "PROJ1REV5",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true,
        "PROJ1REV2": true,
        "PROJ1REV3": true,
        "PROJ1REV4": true,
        "PROJ1REV5": true
      },
      "unpublished": "PROJ1REV6"
    },
    "datumType": "Education Student Accommodation"
  };
  var area1 = {
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "offloadMethod":"Crane",
    "revisions": {
      "PROJ1REV0": "2016-05-01",
      "PROJ1REV1": "2016-05-02",
      "PROJ1REV2": "2016-05-04",
      "PROJ1REV3": "2016-05-05",
      "PROJ1REV4": "2016-05-08",
      "PROJ1REV5": "2016-05-10"
    },
    "timestamps": {
      "created": new Date("2016-04-01").getTime(),
      "modified": new Date("2016-04-20").getTime()
    },
    "notifyUsers": "James",
    "estimatedAreas":{
    },  
    "line": 2
  };
  var area2 = {
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-07-25"
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    },
    "line": 2
  };
  beforeEach(angular.mock.module("innView.admin", function ($provide) {
    $provide.value("$log", console);
  }));
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    referenceDataRef = firebaseRoot.child("srd");
    materialsRequestRef = firebaseRoot.child("materialrequests");
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects","getProject"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockMaterialsRequestService = jasmine.createSpyObj("materialrequestsService", ["getMaterialRequestByProjectId"]);
    gatewayRef = jasmine.createSpyObj("referenceDataService", ["getGateways"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("materialrequestsService", function () {
        return mockMaterialsRequestService;
      });      
    });
    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  beforeEach(inject(function ($firebaseArray) {
    gatewayRef.getGateways.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("gateways"));
    });
  }));  
  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));
  beforeEach(inject(function ($q) {
    mockProjectsService.getProject.and.callFake(function () {
      return $q.when(project1);
    });
  }));  
  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));
  beforeEach(inject(function ($firebaseArray) {
    mockMaterialsRequestService.getMaterialRequestByProjectId.and.callFake(function () {
      return $firebaseArray(materialsRequestRef);
    });
  }));
  beforeEach(inject(function ($rootScope, _dataIntegrityService_) {
    scope = $rootScope;
    dataIntegrityService = _dataIntegrityService_;
  }));
  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2
    });
  });
  beforeEach(function () {
    referenceDataRef.child("gateways").set(
      {
        "G1": {
          "name": "G1 – Discover",
          "colour": "#ffff80",
          "seq": 0
        },
        "G2": {
          "name": "G2 – Consider",
          "colour": "#b30000",
          "seq": 1
        },
        "G3": {
          "name": "G3 – Decide",
          "colour": "#ff1a1a",
          "seq": 2
        },
        "G4": {
          "name": "G4 – Tender",
          "colour": "#ff8080",
          "seq": 3
        },
        "G5": {
          "name": "G5 – Pre-Order",
          "colour": "#2d5986",
          "seq": 4
        },
        "G6": {
          "name": "G6 – Delivery Strategy",
          "colour": "#538cc6",
          "seq": 5
        },
        "G7": {
          "name": "G7 - Pre-Construction",
          "colour": "#9fdf9f",
          "seq": 6
        },
        "G8": {
          "name": "G8 – Site Start",
          "colour": "#53c653",
          "seq": 7
        },
        "G9": {
          "name": "G9 – Site Handover",
          "colour": "#4f9943",
          "seq": 8
        },
        "G10": {
          "name": "G10 – Feedback",
          "colour": "#ff80df",
          "seq": 9
        }
      }
    );
  });
  describe("getData", function (){
    it("returns panels with published delivery date within specified range", function (done) {
      var from = "2016-05-01";
      var to = "2016-06-31";
      dataIntegrityService.getData(from, to)
        .then(function (data) {
          expect(data["001"]["G3"]).toEqual({ "name": true, "key": true, "Estimated Areas": false, "Site Access": true, "Datum Type": true, "Site Start": false }); 
          expect(data["001"]["G4"]).toEqual({"Chain Of Custody": true, "Offload Method": true,"Week Duration": false,"Notify Users": false,"areas": true,"revisions": true});
          expect(data["001"]["G5"]).toEqual({"Special Requirements": true,"Revised Panel Data(50%)": false, "10% Percent Of Areas Uploaded": false});
          
          expect(data["001"]["G11"]).toEqual(undefined);
          done();
        });
      scope.$apply();
    });
  });
  describe("getDataForProject", function (){
    it("returns true or false for specific project", function (done) {
      dataIntegrityService.getDataForProject("001")
        .then(function (data) {
          expect(data["001"]["G3"]).toEqual({ "name": true, "key": true, "Estimated Areas": false, "Site Access": true, "Datum Type": true, "Site Start": false }); 
          expect(data["001"]["G4"]).toEqual({"Chain Of Custody": true, "Offload Method": true,"Week Duration": false,"Notify Users": false,"areas": true,"revisions": true});
          expect(data["001"]["G5"]).toEqual({"Special Requirements": true,"Revised Panel Data(50%)": false, "10% Percent Of Areas Uploaded": false});
          
          expect(data["001"]["G11"]).toEqual(undefined);
          done();
        });
      scope.$apply();
    });
  });  
  function toLocalDateString(date) {
    return date.getFullYear() + "-" + zeroPrefix(date.getMonth() + 1) + "-" + zeroPrefix(date.getDate());
  }
  function toLocalTimeString(date) {
    return zeroPrefix(date.getHours()) + ":" + zeroPrefix(date.getMinutes()) + ":" + zeroPrefix(date.getSeconds());
  }
  function zeroPrefix(value) {
    if (value < 10) {
      return "0" + value;
    } else {
      return "" + value;
    }
  }

});