"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Export service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var panelsRef;
  var referenceDataRef;
  var areaPlansRef;
  var operativesRef;

  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;
  var mockPlannerService;

  var scope;
  var exportService;

  var project1 = {
    "name": "Project 1",
    "id": "001",
    "deliverySchedule": {
      "published": "PROJ1REV5",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true,
        "PROJ1REV2": true,
        "PROJ1REV3": true,
        "PROJ1REV4": true,
        "PROJ1REV5": true
      },
      "unpublished": "PROJ1REV6"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV0": "2016-05-01",
      "PROJ1REV1": "2016-05-02",
      "PROJ1REV2": "2016-05-04",
      "PROJ1REV3": "2016-05-05",
      "PROJ1REV4": "2016-05-08",
      "PROJ1REV5": "2016-05-10"
    },
    "timestamps": {
      "created": new Date("2016-04-01").getTime(),
      "modified": new Date("2016-04-20").getTime()
    },
    "line": 2
  };

  var area2 = {
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-07-25"
    },
    "timestamps": {
      "created": new Date("2016-04-10").getTime(),
      "modified": new Date("2016-04-20").getTime()
    },
    "line": 2
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-H-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    },
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-H-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:30:00+01:00").getTime(),
      "startedOperatives": {
        "OP1":true
      },
      "operatives": {
        "OP1":true,
        "OP3":true
      },
      "completedOperatives": {
        "OP1":true,
        "OP3":true
      }
    },
    "timestamps": {
      "created": new Date("2016-04-21T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-H-003",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-11T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel4 = {
    "area": "AREA1",
    "id": "001-DA-P2-GF-H-004",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1000,
      "height": 1000,
      "length": 1000,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 1000,
      "diagonalWidth": 1000,
      "midHeight": 1000,
      "midLength": 1000,
      "started": new Date("2016-06-10T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel5 = {
    "area": "AREA1",
    "id": "001-DA-P2-GF-H-005",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1000,
      "height": 1000,
      "length": 1000,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 1000,
      "diagonalWidth": 1000,
      "midHeight": 1000,
      "midLength": 1000,
      "started": new Date("2016-05-03T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-03-20T12:13:00").getTime()
    }
  };
  
  var panel6 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-H-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.admin", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");
    operativesRef = firebaseRoot.child("operatives");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");

    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(operativesRef);
    utils.addQuerySupport(areaPlansRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes"]);
    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProductionPlansByPlannedStart"]);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      return $firebaseArray(panelsRef
                            .orderByChild("area")
                            .equalTo(areaRef));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    areaPlansRef.set({
      "AREA1": {
        project: "PROJ1",
        plannedStart: "2016-05-03",
        plannedFinish: "2016-05-05"
      },
      "AREA2": {
        project: "PROJ1",
        plannedStart: "2016-05-06",
        plannedFinish: "2016-05-10"
      }
    });
    mockPlannerService.getProductionPlansByPlannedStart.and.callFake(function () {
      return $firebaseArray(areaPlansRef.orderByChild("plannedStart"));
    });
  }));

  beforeEach(inject(function ($rootScope, _exportService_) {
    scope = $rootScope;
    exportService = _exportService_;
  }));

  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5,
      "panel6": panel6
    });

    referenceDataRef.set({
      "productTypes": {
        "Ext": {
          "name": "External",
          "productionLine": "SIP"
        }
      }
    });
    
    operativesRef.set({
      "OP1": {
        "name": "Rita",
        "active": true
      },
      "OP2": {
        "name": "Sue",
        "active": false
      },
      "OP3": {
        "name": "Bob",
        "active": true
      }
    });
      
  });

  describe("getData", function () {

    it("returns panels with published delivery date within specified range", function (done) {

      var from = "2016-05-01";
      var to = "2016-06-31";
      exportService.getData(from, to)
        .then(function (data) {
          expect(data.length).toEqual(5);

          // panel 1
          expect(data[0].dateCreated).toEqual("2016-04-01"); //area1.timestamps.created
          expect(data[0].project).toEqual(project1.id);
          expect(data[0].panelNumber).toEqual(panel1.id);
          expect(data[0].projectName).toEqual(project1.name);
          expect(data[0].jobsheetReference).toEqual("001-P1-GF-H");
          expect(data[0].phase).toEqual(area1.phase);
          expect(data[0].productionLine).toEqual("SIP");
          expect(data[0].floor).toEqual(area1.floor);
          expect(data[0].number).toEqual("001");
          expect(data[0].designHeight).toEqual(430);
          expect(data[0].designLength).toEqual(1100);
          expect(data[0].designArea).toEqual(0.5);
          expect(data[0].designWeight).toEqual(19);
          expect(data[0].designWidth).toEqual(162);
          expect(data[0].designMP).toEqual("");
          expect(data[0].jobsheets).toEqual("P1-GF-H");
          expect(data[0].dateCompleted).toEqual("");
          expect(data[0].actualHeight).toEqual("");
          expect(data[0].actualLength).toEqual("");
          expect(data[0].actualLRDiag).toEqual("");
          expect(data[0].actualRLDiag).toEqual("");
          expect(data[0].operatorSignoff).toEqual("");
          expect(data[0].supervisorSignoff).toEqual("");
          expect(data[0].completedStatus).toEqual("Planned");
          expect(data[0].deliveryDate).toEqual("2016-05-04");
          expect(data[0].areaType).toEqual("Ext");
          expect(data[0].panelType).toEqual("H");
          expect(data[0].uploadDate).toEqual("2016-04-20");
          expect(data[0].areaPlannedStart).toEqual("2016-05-03");
          expect(data[0].areaPlannedFinish).toEqual("2016-05-05");
          expect(data[0].buildStartDate).toEqual("");
          expect(data[0].buildStartTime).toEqual("");
          expect(data[0].buildFinishDate).toEqual("");
          expect(data[0].buildFinishTime).toEqual("");
          expect(data[0].buildDuration).toEqual("");
          expect(data[0].line).toEqual(2);
          expect(data[0].startedOperatives).toEqual("");
          expect(data[0].operatives).toEqual("");
          expect(data[0].completedOperatives).toEqual("");

          // panel 2
          expect(data[1].dateCreated).toEqual("2016-04-01"); //area1.timestamps.created
          expect(data[1].project).toEqual(project1.id);
          expect(data[1].panelNumber).toEqual(panel2.id);
          expect(data[1].projectName).toEqual(project1.name);
          expect(data[1].jobsheetReference).toEqual("001-P1-GF-H");
          expect(data[1].phase).toEqual(area1.phase);
          expect(data[1].productionLine).toEqual("SIP");
          expect(data[1].floor).toEqual(area1.floor);
          expect(data[1].number).toEqual("002");
          expect(data[1].designHeight).toEqual(430);
          expect(data[1].designLength).toEqual(1100);
          expect(data[1].designArea).toEqual(0.5);
          expect(data[1].designWeight).toEqual(19);
          expect(data[1].designWidth).toEqual(162);
          expect(data[1].designMP).toEqual("");
          expect(data[1].jobsheets).toEqual("P1-GF-H");
          expect(data[1].dateCompleted).toEqual("2016-05-10");
          expect(data[1].actualLength).toEqual(1102);
          expect(data[1].actualHeight).toEqual(435);
          expect(data[1].actualLRDiag).toEqual(900);
          expect(data[1].actualRLDiag).toEqual(700);
          expect(data[1].operatorSignoff).toEqual("");
          expect(data[1].supervisorSignoff).toEqual("");
          expect(data[1].completedStatus).toEqual("Completed");
          expect(data[1].deliveryDate).toEqual("2016-05-04");
          expect(data[1].areaType).toEqual("Ext");
          expect(data[1].panelType).toEqual("H");
          expect(data[1].uploadDate).toEqual("2016-04-21");
          expect(data[1].areaPlannedStart).toEqual("2016-05-03");
          expect(data[1].areaPlannedFinish).toEqual("2016-05-05");
          expect(data[1].buildStartDate).toEqual(toLocalDateString(new Date(panel2.qa.started)));
          expect(data[1].buildStartTime).toEqual(toLocalTimeString(new Date(panel2.qa.started)));
          expect(data[1].buildFinishDate).toEqual(toLocalDateString(new Date(panel2.qa.completed)));
          expect(data[1].buildFinishTime).toEqual(toLocalTimeString(new Date(panel2.qa.completed)));
          expect(data[1].buildDuration).toEqual(14400);
          expect(data[1].projectDatum).toEqual("Education Student Accommodation");
          expect(data[1].line).toEqual(2);
          expect(data[1].startedOperatives).toEqual("Rita");
          expect(data[1].operatives).toEqual("Rita, Bob");
          expect(data[1].completedOperatives).toEqual("Rita, Bob");
          
          // panel 3
          expect(data[2].dateCreated).toEqual("2016-04-01"); //area1.timestamps.created
          expect(data[2].project).toEqual(project1.id);
          expect(data[2].panelNumber).toEqual(panel3.id);
          expect(data[2].projectName).toEqual(project1.name);
          expect(data[2].jobsheetReference).toEqual("001-P1-GF-H");
          expect(data[2].phase).toEqual(area1.phase);
          expect(data[2].productionLine).toEqual("SIP");
          expect(data[2].floor).toEqual(area1.floor);
          expect(data[2].number).toEqual("003");
          expect(data[2].designHeight).toEqual(430);
          expect(data[2].designLength).toEqual(1100);
          expect(data[2].designArea).toEqual(0.5);
          expect(data[2].designWeight).toEqual(19);
          expect(data[2].designWidth).toEqual(162);
          expect(data[2].designMP).toEqual("");
          expect(data[2].jobsheets).toEqual("P1-GF-H");
          expect(data[2].dateCompleted).toEqual("");
          expect(data[2].actualLength).toEqual(1102);
          expect(data[2].actualHeight).toEqual(435);
          expect(data[2].actualLRDiag).toEqual(900);
          expect(data[2].actualRLDiag).toEqual(700);
          expect(data[2].operatorSignoff).toEqual("");
          expect(data[2].supervisorSignoff).toEqual("");
          expect(data[2].completedStatus).toEqual("In Progress");
          expect(data[2].deliveryDate).toEqual("2016-05-04");
          expect(data[2].areaType).toEqual("Ext");
          expect(data[2].panelType).toEqual("H");
          expect(data[2].uploadDate).toEqual("2016-04-20");
          expect(data[2].areaPlannedStart).toEqual("2016-05-03");
          expect(data[2].areaPlannedFinish).toEqual("2016-05-05");
          expect(data[2].buildStartDate).toEqual(toLocalDateString(new Date(panel3.qa.started)));
          expect(data[2].buildStartTime).toEqual(toLocalTimeString(new Date(panel3.qa.started)));
          expect(data[2].buildFinishDate).toEqual("");
          expect(data[2].buildFinishTime).toEqual("");
          expect(data[2].buildDuration).toEqual("");
          expect(data[2].projectDatum).toEqual("Education Student Accommodation");
          expect(data[2].line).toEqual(2);
          expect(data[2].startedOperatives).toEqual("");
          expect(data[2].operatives).toEqual("");
          expect(data[2].completedOperatives).toEqual("");
          
          // panel 4
          expect(data[3].dateCreated).toEqual("2016-04-01"); //area1.timestamps.created
          expect(data[3].project).toEqual(project1.id);
          expect(data[3].panelNumber).toEqual(panel4.id);
          expect(data[3].projectName).toEqual(project1.name);
          expect(data[3].jobsheetReference).toEqual("001-P1-GF-H");
          expect(data[3].phase).toEqual(area1.phase);
          expect(data[3].productionLine).toEqual("SIP");
          expect(data[3].floor).toEqual(area1.floor);
          expect(data[3].number).toEqual("004");
          expect(data[3].designHeight).toEqual(1000);
          expect(data[3].designLength).toEqual(1000);
          expect(data[3].designArea).toEqual(0.5);
          expect(data[3].designWeight).toEqual(19);
          expect(data[3].designWidth).toEqual(1000);
          expect(data[3].designMP).toEqual("");
          expect(data[3].jobsheets).toEqual("P1-GF-H");
          expect(data[3].dateCompleted).toEqual("");
          expect(data[3].actualLength).toEqual(1000);
          expect(data[3].actualHeight).toEqual(1000);
          expect(data[3].actualLRDiag).toEqual(1000);
          expect(data[3].actualRLDiag).toEqual(1000);
          expect(data[3].operatorSignoff).toEqual("");
          expect(data[3].supervisorSignoff).toEqual("");
          expect(data[3].completedStatus).toEqual("In Progress");
          expect(data[3].deliveryDate).toEqual("2016-05-04");
          expect(data[3].areaType).toEqual("Ext");
          expect(data[3].panelType).toEqual("H");
          expect(data[3].uploadDate).toEqual("2016-04-20");
          expect(data[3].areaPlannedStart).toEqual("2016-05-03");
          expect(data[3].areaPlannedFinish).toEqual("2016-05-05");
          expect(data[3].buildStartDate).toEqual(toLocalDateString(new Date(panel4.qa.started)));
          expect(data[3].buildStartTime).toEqual(toLocalTimeString(new Date(panel4.qa.started)));
          expect(data[3].buildFinishDate).toEqual("");
          expect(data[3].buildFinishTime).toEqual("");
          expect(data[3].buildDuration).toEqual("");
          expect(data[3].projectDatum).toEqual("Education Student Accommodation");
          expect(data[3].line).toEqual(2);
          expect(data[3].startedOperatives).toEqual("");
          expect(data[3].operatives).toEqual("");
          expect(data[3].completedOperatives).toEqual("");
          
          // panel 5
          expect(data[4].dateCreated).toEqual("2016-04-01"); //area1.timestamps.created
          expect(data[4].project).toEqual(project1.id);
          expect(data[4].panelNumber).toEqual(panel5.id);
          expect(data[4].projectName).toEqual(project1.name);
          expect(data[4].jobsheetReference).toEqual("001-P1-GF-H");
          expect(data[4].phase).toEqual(area1.phase);
          expect(data[4].productionLine).toEqual("SIP");
          expect(data[4].floor).toEqual(area1.floor);
          expect(data[4].number).toEqual("005");
          expect(data[4].designHeight).toEqual(1000);
          expect(data[4].designLength).toEqual(1000);
          expect(data[4].designArea).toEqual(0.5);
          expect(data[4].designWeight).toEqual(19);
          expect(data[4].designWidth).toEqual(1000);
          expect(data[4].designMP).toEqual("");
          expect(data[4].jobsheets).toEqual("P1-GF-H");
          expect(data[4].dateCompleted).toEqual("");
          expect(data[4].actualLength).toEqual(1000);
          expect(data[4].actualHeight).toEqual(1000);
          expect(data[4].actualLRDiag).toEqual(1000);
          expect(data[4].actualRLDiag).toEqual(1000);
          expect(data[4].operatorSignoff).toEqual("");
          expect(data[4].supervisorSignoff).toEqual("");
          expect(data[4].completedStatus).toEqual("In Progress");
          expect(data[4].deliveryDate).toEqual("2016-05-04");
          expect(data[4].areaType).toEqual("Ext");
          expect(data[4].panelType).toEqual("H");
          expect(data[4].uploadDate).toEqual("2016-03-20");
          expect(data[4].areaPlannedStart).toEqual("2016-05-03");
          expect(data[4].areaPlannedFinish).toEqual("2016-05-05");
          expect(data[4].buildStartDate).toEqual(toLocalDateString(new Date(panel5.qa.started)));
          expect(data[4].buildStartTime).toEqual(toLocalTimeString(new Date(panel5.qa.started)));
          expect(data[4].buildFinishDate).toEqual("");
          expect(data[4].buildFinishTime).toEqual("");
          expect(data[4].buildDuration).toEqual("");
          expect(data[4].projectDatum).toEqual("Education Student Accommodation");
          expect(data[4].line).toEqual(2);
          expect(data[4].startedOperatives).toEqual("");
          expect(data[4].operatives).toEqual("");
          expect(data[4].completedOperatives).toEqual("");

          done();
        });

      scope.$apply();
    });
  });

  function toLocalDateString(date) {
    return date.getFullYear() + "-" + zeroPrefix(date.getMonth() + 1) + "-" + zeroPrefix(date.getDate());
  }

  function toLocalTimeString(date) {
    return zeroPrefix(date.getHours()) + ":" + zeroPrefix(date.getMinutes()) + ":" + zeroPrefix(date.getSeconds());
  }

  function zeroPrefix(value) {
    if (value < 10) {
      return "0" + value;
    } else {
      return "" + value;
    }
  }

});
