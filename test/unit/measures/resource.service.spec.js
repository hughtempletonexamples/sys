"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Resource Service", function () {
  var scope;
  var createdTimestamp;
  var systemTimestamp;
  

  var firebaseRoot;
  var benchmarkRef;
  var mockSchema;
  
  var resourceService;

  beforeEach(angular.mock.module("innView.measures", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    benchmarkRef = firebaseRoot.child("benchmarks");
    utils.addQuerySupport(benchmarkRef);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _resourceService_) {
    scope = $rootScope;
    resourceService = _resourceService_;
  }));
  
  describe("get benchmarks", function () {

    it("returns benchmarks", function () {

      benchmarkRef.set(
        {
          "CASS": {
            "meterSquared": 40,
            "operators": 3
          },
          "HSIP": {
            "meterSquared": 50,
            "operators": 1
          },
          "IFAST": {
            "meterSquared": 90,
            "operators": 1
          },
          "SIP": {
            "meterSquared": 100,
            "operators": 6
          },
          "TF": {
            "meterSquared": 60,
            "operators": 5
          }
        }
      );
    
      var benchmarks = resourceService.getBenchmark();
      scope.$apply();

      expect(benchmarks).toEqual(jasmine.objectContaining({
        "CASS": {
          "meterSquared": 40,
          "operators": 3
        },
        "HSIP": {
          "meterSquared": 50,
          "operators": 1
        },
        "IFAST": {
          "meterSquared": 90,
          "operators": 1
        },
        "SIP": {
          "meterSquared": 100,
          "operators": 6
        },
        "TF": {
          "meterSquared": 60,
          "operators": 5
        }
      }));

    });
  });
  
  xdescribe("set spacific benchmark", function () {

    it("returns benchmark", function (done) {

      benchmarkRef.set(
        {
          "CASS": {
            "meterSquared": 40,
            "operators": 3
          },
          "HSIP": {
            "meterSquared": 50,
            "operators": 1
          },
          "SIP": {
            "meterSquared": 100,
            "operators": 6
          },
          "TF": {
            "meterSquared": 60,
            "operators": 5
          }
        }
      );
    
      var sipBenchmarks = resourceService.getBenchmark("SIP");
      scope.$apply();
      
      sipBenchmarks.meterSquared = 20;

      resourceService.setBenchmark(sipBenchmarks)
        .then(function () {
          var savedPanel = utils.getRefChild(benchmarkRef, 2);
          expect(savedPanel).toEqual(jasmine.objectContaining({
            "meterSquared": 20,
            "operators": 6
          }));

          done();
        });

      scope.$apply();

    });
  });
  
  
});
