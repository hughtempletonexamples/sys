"use strict";

var utils = require("../testUtils");

describe("measuresService", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;
  var measuresService;
  var projectsRef;
  var areasRef;
  var panelsRef;
  var statsRef;
  var referenceDataRef;
  var capacityRef;
  var mockReferenceDataService;
  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  
  var project1 = {
    "$id": "PROJ1",
    "name": "Project 1",
    "id": "001",
    "active": true,
    "deliverySchedule": {
      "unpublished": "PROJ1REV1",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    $id: "AREA1",
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 50,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "completedArea": 95,
    "completedPanels": 14,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2016-11-20",
      "PROJ1REV1": "2016-11-20"
    },
    "timestamps": {
      "created": new Date("2016-11-20").getTime(),
      "modified": new Date("2016-11-20").getTime()
    }
  };

  var area2 = {
    $id: "AREA2",
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV1": "2016-11-20"
    },
    "timestamps": {
      "created": new Date("2016-11-20").getTime(),
      "modified": new Date("2016-11-20").getTime()
    }
  };

  var area3 = {
    $id: "AREA3",
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2016-12-11",
      "PROJ1REV1": "2016-12-11"
    },
    "timestamps": {
      "created": new Date("2016-12-11").getTime(),
      "modified": new Date("2016-12-11").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-C-001",
    "project": "PROJ1",
    "type": "Roof",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  var panel4 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-H-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  var panel5 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-I-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.measures", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.measures"));

  beforeEach(function () {
    jasmine.clock().install();
    jasmine.clock().mockDate(new Date("2016-11-21"));
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });
  
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");
    capacityRef = firebaseRoot.child("productionCapacities");
    statsRef = firebaseRoot.child("stats");
    
    utils.addQuerySupport(projectsRef);
    utils.addQuerySupport(areasRef);
    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(statsRef);
    utils.addQuerySupport(referenceDataRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes", "getProductionLines"]);

  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  
  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      return $firebaseArray(panelsRef
                            .orderByChild("area")
                            .equalTo(areaRef));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productionLines"));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));


  beforeEach(function () {
    
    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" }
      }
    );

    referenceDataRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 40 },
        "HSIP": { defaultCapacity: 20 },
        "IFAST": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 60 },
        "CASS": { defaultCapacity: 6 }
      }
    );
  
    capacityRef.set(
      {
        SIP: {"2016-11-16": 10}
      }
    );
    
    statsRef.set(
      {
        "SIP": 
        {
          "2016-11-15": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-16": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-17": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "HSIP": 
        {
          "2016-11-15": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-16": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-17": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "IFAST": {
          "2016-11-15": {
            "designedM2": 300,
            "designedCount": 3,
            "deliveryM2": 150,
            "deliveryCount":2,
            "completedM2": 250,
            "completedCount": 3
          },
          "2016-11-16": {
            "designedM2": 300,
            "designedCount": 3,
            "deliveryM2": 150,
            "deliveryCount":1,
            "completedM2": 250,
            "completedCount": 2
          }
        },
        "TF": 
        {
          "2016-11-15": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-16": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-17": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "CASS": 
        {
          "2016-11-15": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-16": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2016-11-17": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        }
      }
    );
    
  });
  
  beforeEach(function () {

    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5 
    });
    
  });
  
  beforeEach(inject(function ($rootScope, $injector) {
    scope = $rootScope;
    measuresService = $injector.get("measuresService");
  }));
  
  describe("designedIssued", function () {

    it("returns designed Issued data", function () {

      measuresService.designedIssued()
        .then(function (data){
          
          expect(data.type.SIP).toEqual({
            completedM2: 450,
            completedCount: 9,
            designedM2: 240,
            designedCount: 9,
            deliveryM2: 181.5,
            deliveryCount: 6
          });
          expect(data.type.HSIP).toEqual({
            completedM2: 450,
            completedCount: 9,
            designedM2: 240,
            designedCount: 9,
            deliveryM2: 181.5,
            deliveryCount: 6
          });
          expect(data.type.IFAST).toEqual({
            completedM2: 500,
            completedCount: 5,
            designedM2: 600,
            designedCount: 6,
            deliveryM2: 300,
            deliveryCount: 3 
          });
          expect(data.type.TF).toEqual({
            completedM2: 450,
            completedCount: 9,
            designedM2: 240,
            designedCount: 9,
            deliveryM2: 181.5,
            deliveryCount: 6
          });
          expect(data.type.CASS).toEqual({
            completedM2: 450,
            completedCount: 9,
            designedM2: 240,
            designedCount: 9,
            deliveryM2: 181.5,
            deliveryCount: 6
          });
          
          expect(data.totalCompleteM2).toEqual(2300);
          expect(data.totalCompleteCount).toEqual(41);
          expect(data.totalDesignedM2).toEqual(1560);
          expect(data.totalDesignedCount).toEqual(42);
          
        });
      
      scope.$apply();
     
    });
    
  });
  
  describe("syncStats", function () {

    it("runs through panels and counts all statistics and replaces whats there in SIPS", function () {

      measuresService.syncStats()
        .then(function () {
          var savedStats = utils.getRefChild(statsRef, 2);
          expect(savedStats).toEqual({
            "2016-11-20": { startedCount: 2, startedM2: 1, completedCount: 1, completedM2: 0.5, designedCount: 2, designedM2: 1, deliveryCount: 2, deliveryM2: 1 }
          });
        });

      scope.$apply();

    });
    
  });
  
  xdescribe("generateData", function () {

    it("returns designed chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){

          expect(data.graphData1.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData1.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData1.IFAST).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 6 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData1.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData1.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

        });
      
      scope.$apply();
     
    });
    
    it("returns design buffer chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){
          expect(data.graphData2.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -200 },{ x: "2016-12-05", y: -400 },{ x: "2016-12-12", y: -600 }]);
          expect(data.graphData2.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -100 },{ x: "2016-12-05", y: -200 },{ x: "2016-12-12", y: -300 }]);
          expect(data.graphData2.IFAST).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: 1 },{ x: "2016-11-28", y: -99 },{ x: "2016-12-05", y: -199 },{ x: "2016-12-12", y: -299 }]);
          expect(data.graphData2.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -300 },{ x: "2016-12-05", y: -600 },{ x: "2016-12-12", y: -900}]);
          expect(data.graphData2.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -30 },{ x: "2016-12-05", y: -60 },{ x: "2016-12-12", y: -90 }]);

        });
      
      scope.$apply();
     
    });
    
    it("returns manufactured chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){

          //manufactured chart
          expect(data.graphData3.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData3.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData3.IFAST).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 5 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData3.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData3.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 9 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

        });
      
      scope.$apply();
     
    });

    it("returns delivered buffer 2 chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){

          expect(data.graphData4.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 3 },{ x: "2016-11-28", y: 203 },{ x: "2016-12-05", y: 403 },{ x: "2016-12-12", y: 603 }]);
          expect(data.graphData4.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 3 },{ x: "2016-11-28", y: 103 },{ x: "2016-12-05", y: 203 },{ x: "2016-12-12", y: 303 }]);
          expect(data.graphData4.IFAST).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 2 },{ x: "2016-11-28", y: 102 },{ x: "2016-12-05", y: 202 },{ x: "2016-12-12", y: 302 }]);
          expect(data.graphData4.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 3 },{ x: "2016-11-28", y: 303 },{ x: "2016-12-05", y: 603 },{ x: "2016-12-12", y: 903 }]);
          expect(data.graphData4.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 3 },{ x: "2016-11-28", y: 33 },{ x: "2016-12-05", y: 63 },{ x: "2016-12-12", y: 93 }]);    

        });
      
      scope.$apply();

    });
    
    it("returns delivered chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){

          expect(data.graphData5.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 6 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData5.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 6 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData5.IFAST).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData5.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 6 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
          expect(data.graphData5.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 6 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

        });
      
      scope.$apply();
     
    });
    
    it("returns capacity chart", function () {

      measuresService.generateData(true, 5, 14)
        .then(function (data){

          expect(data.graphFutureData3.SIP).toEqual([{ x: "2016-11-07", y: 200 },{ x: "2016-11-14", y: 170 },{ x: "2016-11-21", y: 200 },{ x: "2016-11-28", y: 200 },{ x: "2016-12-05", y: 200 },{ x: "2016-12-12", y: 200 }]);
          expect(data.graphFutureData3.HSIP).toEqual([{ x: "2016-11-07", y: 100 },{ x: "2016-11-14", y: 100 },{ x: "2016-11-21", y: 100 },{ x: "2016-11-28", y: 100 },{ x: "2016-12-05", y: 100 },{ x: "2016-12-12", y: 100 }]);
          expect(data.graphFutureData3.IFAST).toEqual([{ x: "2016-11-07", y: 100 },{ x: "2016-11-14", y: 100 },{ x: "2016-11-21", y: 100 },{ x: "2016-11-28", y: 100 },{ x: "2016-12-05", y: 100 },{ x: "2016-12-12", y: 100 }]);
          expect(data.graphFutureData3.TF).toEqual([{ x: "2016-11-07", y: 300 },{ x: "2016-11-14", y: 300 },{ x: "2016-11-21", y: 300 },{ x: "2016-11-28", y: 300 },{ x: "2016-12-05", y: 300 },{ x: "2016-12-12", y: 300 }]);
          expect(data.graphFutureData3.CASS).toEqual([{ x: "2016-11-07", y: 30 },{ x: "2016-11-14", y: 30 },{ x: "2016-11-21", y: 30 },{ x: "2016-11-28", y: 30 },{ x: "2016-12-05", y: 30 },{ x: "2016-12-12", y: 30 }]);    

        });
      
      scope.$apply();
     
    });

    describe("generateData", function () {

      it("returns designed chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){

            expect(data.graphData1.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 3 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData1.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData1.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData1.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

          });

        scope.$apply();

      });

      it("returns design buffer chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){
            expect(data.graphData2.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: 1 },{ x: "2016-11-28", y: -199 },{ x: "2016-12-05", y: -399 },{ x: "2016-12-12", y: -599 }]);
            expect(data.graphData2.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: 1 },{ x: "2016-11-28", y: -99 },{ x: "2016-12-05", y: -199 },{ x: "2016-12-12", y: -299 }]);
            expect(data.graphData2.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -300 },{ x: "2016-12-05", y: -600 },{ x: "2016-12-12", y: -900}]);
            expect(data.graphData2.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: -30 },{ x: "2016-12-05", y: -60 },{ x: "2016-12-12", y: -90 }]);

          });

        scope.$apply();

      });

      it("returns manufactured chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){

            //manufactured chart
            expect(data.graphData3.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData3.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData3.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData3.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

          });

        scope.$apply();

      });

      it("returns delivered buffer 2 chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){

            expect(data.graphData4.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: -1 },{ x: "2016-11-28", y: 199 },{ x: "2016-12-05", y: 399 },{ x: "2016-12-12", y: 599 }]);
            expect(data.graphData4.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: -1 },{ x: "2016-11-21", y: -1 },{ x: "2016-11-28", y: 99 },{ x: "2016-12-05", y: 199 },{ x: "2016-12-12", y: 299 }]);
            expect(data.graphData4.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 300 },{ x: "2016-12-05", y: 600 },{ x: "2016-12-12", y: 900 }]);
            expect(data.graphData4.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 30 },{ x: "2016-12-05", y: 60 },{ x: "2016-12-12", y: 90 }]);    

          });

        scope.$apply();

      });

      it("returns delivered chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){

            expect(data.graphData5.SIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 1 },{ x: "2016-11-21", y: 2 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData5.HSIP).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData5.TF).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 0 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);
            expect(data.graphData5.CASS).toEqual([{ x: "2016-11-07", y: 0 },{ x: "2016-11-14", y: 2 },{ x: "2016-11-21", y: 0 },{ x: "2016-11-28", y: 0 },{ x: "2016-12-05", y: 0 },{ x: "2016-12-12", y: 0 }]);    

          });

        scope.$apply();

      });

      it("returns capacity chart", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){

            expect(data.graphFutureData3.SIP).toEqual([{ x: "2016-11-07", y: 200 },{ x: "2016-11-14", y: 170 },{ x: "2016-11-21", y: 200 },{ x: "2016-11-28", y: 200 },{ x: "2016-12-05", y: 200 },{ x: "2016-12-12", y: 200 }]);
            expect(data.graphFutureData3.HSIP).toEqual([{ x: "2016-11-07", y: 100 },{ x: "2016-11-14", y: 100 },{ x: "2016-11-21", y: 100 },{ x: "2016-11-28", y: 100 },{ x: "2016-12-05", y: 100 },{ x: "2016-12-12", y: 100 }]);
            expect(data.graphFutureData3.TF).toEqual([{ x: "2016-11-07", y: 300 },{ x: "2016-11-14", y: 300 },{ x: "2016-11-21", y: 300 },{ x: "2016-11-28", y: 300 },{ x: "2016-12-05", y: 300 },{ x: "2016-12-12", y: 300 }]);
            expect(data.graphFutureData3.CASS).toEqual([{ x: "2016-11-07", y: 30 },{ x: "2016-11-14", y: 30 },{ x: "2016-11-21", y: 30 },{ x: "2016-11-28", y: 30 },{ x: "2016-12-05", y: 30 },{ x: "2016-12-12", y: 30 }]);    

          });

        scope.$apply();

      });

      it("returns max design value", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){
            expect(data.maxDesignedBuffer).toEqual(300);
          });

        scope.$apply();

      });

      it("returns max delivered value", function () {

        measuresService.generateData(true, 5, 14)
          .then(function (data){
            expect(data.maxDesignedBuffer).toEqual(300);
          });

        scope.$apply();

      });

    });
  
  });
  
});
