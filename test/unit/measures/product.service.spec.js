"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Product service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var panelsRef;
  var referenceDataRef;
  var areaPlansRef;
  var operativesRef;
  var additionalDataRef;

  var systemTimestamp;
  var createdTimestamp;

  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;
  var mockPlannerService;

  var scope;
  var productService;

  var project1 = {
    "name": "Project 1",
    "id": "001",
    "deliverySchedule": {
      "published": "PROJ1REV5",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true,
        "PROJ1REV2": true,
        "PROJ1REV3": true,
        "PROJ1REV4": true,
        "PROJ1REV5": true
      },
      "unpublished": "PROJ1REV6"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    "$id": "AREA1",
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV0": "2016-05-01",
      "PROJ1REV1": "2016-05-02",
      "PROJ1REV2": "2016-05-04",
      "PROJ1REV3": "2016-05-05",
      "PROJ1REV4": "2016-05-08",
      "PROJ1REV5": "2016-05-10"
    },
    "timestamps": {
      "created": new Date("2016-04-01").getTime(),
      "modified": new Date("2016-04-20").getTime()
    },
    "line": 2
  };

  var area2 = {
    "$id": "AREA2",
    "floor": "GF",
    "phase": "P2",
    "type": "ExtH",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV3": "2016-05-05",
      "PROJ1REV4": "2016-05-08",
      "PROJ1REV5": "2016-05-10"
    },
    "timestamps": {
      "created": new Date("2016-03-10").getTime(),
      "modified": new Date("2016-03-20").getTime()
    },
    "line": 2
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "additionalInfo": {
      framingStyle: "SIP 213",
      studSize: "38x184",
      sheathing: "OSB 5980",
      components: 6,
      nailing: "150x150",
      spandrel: "No",
      doors: null,
      windows: null,
      pockets: null,
      qty: 1
    },
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "timestamps": {
      "created": new Date("2016-05-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "additionalInfo": {
      framingStyle: "SIP 213",
      studSize: "38x184",
      sheathing: "OSB 5980",
      components: 6,
      nailing: "150x150",
      spandrel: "No",
      doors: null,
      windows: null,
      pockets: null,
      qty: 1
    },
    "qa": {
      "completed": new Date("2016-05-10T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-10T10:30:00+01:00").getTime(),
      "startedOperatives": {
        "OP1":true
      },
      "operatives": {
        "OP1":true,
        "OP3":true
      },
      "completedOperatives": {
        "OP1":true,
        "OP3":true
      }
    },
    "timestamps": {
      "created": new Date("2016-04-21T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-003",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "additionalInfo": {
      framingStyle: "SIP 213",
      studSize: "38x184",
      sheathing: "OSB 5980",
      components: 6,
      nailing: "150x150",
      spandrel: "No",
      doors: null,
      windows: null,
      pockets: null,
      qty: 1
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-05-11T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel4 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-004",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 1000,
      "height": 1000,
      "length": 1000,
      "weight": 19
    },
    "additionalInfo": {
      framingStyle: "SIP 213",
      studSize: "38x184",
      sheathing: "OSB 5980",
      components: 6,
      nailing: "150x150",
      spandrel: "No",
      doors: null,
      windows: null,
      pockets: null,
      qty: 1
    },
    "qa": {
      "diagonalHeight": 1000,
      "diagonalWidth": 1000,
      "midHeight": 1000,
      "midLength": 1000,
      "started": new Date("2016-06-10T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  
  var panel5 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-005",
    "project": "PROJ1",
    "type": "Ext",
    "additionalInfo": {
      framingStyle: "SIP 213",
      studSize: "38x184",
      sheathing: "OSB 5980",
      components: 6,
      nailing: "150x150",
      spandrel: "No",
      doors: null,
      windows: null,
      pockets: null,
      qty: 1
    },
    "dimensions": {
      "area": 0.5,
      "width": 1000,
      "height": 1000,
      "length": 1000,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 1000,
      "diagonalWidth": 1000,
      "midHeight": 1000,
      "midLength": 1000,
      "started": new Date("2016-05-03T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-03-20T12:13:00").getTime()
    }
  };
  
  var panel6 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-H-006",
    "project": "PROJ1",
    "type": "ExtH",
    "additionalInfo" : {
      "components" : 18,
      "framingStyle" : "HSIP 206 + CP Board",
      "nailing" : "150x150",
      "qty" : 1,
      "sheathing" : "12mm MultiPurpose Board H",
      "spandrel" : "No",
      "studSize" : "38x184"
    },
    "dimensions": {
      "area": 0.9,
      "height": 243,
      "length": 3610,
      "weight": 59,
      "width": 207
    },
    "qa": {
      "completed": new Date("2016-05-03T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 60,
      "midLength": 3611,
      "started": new Date("2016-05-03T10:30:00+01:00").getTime(),
      "startedOperatives": {
        "OP1":true
      },
      "operatives": {
        "OP1":true,
        "OP3":true
      },
      "completedOperatives": {
        "OP1":true,
        "OP3":true
      }
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };
  var panel7 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-F-007",
    "project": "PROJ1",
    "type": "ExtF",
    "additionalInfo" : {
      "components" : 14,
      "framingStyle" : "IFAST 203",
      "nailing" : "150x150",
      "qty" : 1,
      "sheathing" : "12mm MultiPurpose Board H",
      "spandrel" : "No",
      "studSize" : "38x184"
    },
    "dimensions": {
      "area": 10,
      "height": 243,
      "length": 3610,
      "weight": 59,
      "width": 207
    },
    "qa": {
      "completed": new Date("2016-05-03T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 60,
      "midLength": 3611,
      "started": new Date("2016-05-03T10:30:00+01:00").getTime(),
      "startedOperatives": {
        "OP1":true
      },
      "operatives": {
        "OP1":true,
        "OP3":true
      },
      "completedOperatives": {
        "OP1":true,
        "OP3":true
      }
    },
    "timestamps": {
      "created": new Date("2016-04-20T12:13:00").getTime()
    }
  };  
  

  beforeEach(angular.mock.module("innView.measures", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");
    operativesRef = firebaseRoot.child("operatives");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    additionalDataRef = firebaseRoot.child("additionalData/areas");

    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(areasRef);
    utils.addQuerySupport(projectsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(operativesRef);
    utils.addQuerySupport(areaPlansRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes"]);
    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProductionPlansByPlannedStart"]);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ( $firebaseArray ) {
    mockAreasService.getProjectAreas.and.callFake(function ( projectRef ) {
      return $firebaseArray(areasRef
              .orderByChild("project")
              .equalTo(projectRef));
    });
  }));

  beforeEach(inject(function ( $firebaseArray ) {
    mockPanelsService.panelsForArea.and.callFake(function ( areaRef ) {
      return $firebaseArray(panelsRef
              .orderByChild("area")
              .equalTo(areaRef));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    
    additionalDataRef.set({});
    
    areaPlansRef.set({
      "AREA1": {
        project: "PROJ1",
        plannedStart: "2016-05-03",
        plannedFinish: "2016-05-05"
      },
      "AREA2": {
        project: "PROJ1",
        plannedStart: "2016-05-03",
        plannedFinish: "2016-05-03"
      }
    });
    mockPlannerService.getProductionPlansByPlannedStart.and.callFake(function () {
      return $firebaseArray(areaPlansRef.orderByChild("plannedStart"));
    });
  }));
  
  beforeEach(function () {
    var now = new Date("2016-05-01");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);
    
    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp + 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(inject(function ($rootScope, _productService_) {
    scope = $rootScope;
    productService = _productService_;
  }));

  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5,
      "panel6": panel6,
      "panel7": panel7
    });

    referenceDataRef.set({
      "productTypes": {
        "Ext": {
          "name": "External",
          "productionLine": "SIP"
        },
        "ExtH": {
          "name": "Hybrid Structural Insulated Panel",
          "productionLine": "HSIP"
        },
        "ExtF": {
          "name": "IFAST Structure",
          "productionLine": "IFAST"
        },
        "Int": {
          "name": "Internal",
          "productionLine": "TF"
        }
      }
    });
    
    operativesRef.set({
      "OP1": {
        "name": "Rita",
        "active": true
      },
      "OP2": {
        "name": "Sue",
        "active": false
      },
      "OP3": {
        "name": "Bob",
        "active": true
      }
    });
      
  });

  describe("getData", function () {

    it("returns panels with area start date grouped into type of product (taken from framing style)", function ( done ) {

      productService.getData()
              .then(function ( data ) {
                expect(data).toEqual({ 
                  SIP: [{ 
                    key: "SIP 213", 
                    values: [{ x: 1459728000000, y: 0 },{ x: 1460332800000, y: 0 },{ x: 1460937600000, y: 0 },{ x: 1461542400000, y: 0 },{ x: 1462147200000, y: 2.5 },{ x: 1462752000000, y: 0 },{ x: 1463356800000, y: 0 } ] } ], 
                  HSIP: [{ 
                    key: "HSIP 206 + CP Board", 
                    values: [{ x: 1459728000000, y: 0 },{ x: 1460332800000, y: 0 },{ x: 1460937600000, y: 0 },{ x: 1461542400000, y: 0 },{ x: 1462147200000, y: 0.9 },{ x: 1462752000000, y: 0 },{ x: 1463356800000, y: 0 } ] } ], 
                  IFAST: [{ 
                    key: "IFAST 203", 
                    values: [ { x: 1459728000000, y: 0 }, { x: 1460332800000, y: 0 }, { x: 1460937600000, y: 0 }, { x: 1461542400000, y: 0 }, { x: 1462147200000, y: 10 }, { x: 1462752000000, y: 0 }, { x: 1463356800000, y: 0 } ] } ] 
                });

                done();
              });

      scope.$apply();
    });
  });

  function toLocalDateString(date) {
    return date.getFullYear() + "-" + zeroPrefix(date.getMonth() + 1) + "-" + zeroPrefix(date.getDate());
  }

  function toLocalTimeString(date) {
    return zeroPrefix(date.getHours()) + ":" + zeroPrefix(date.getMinutes()) + ":" + zeroPrefix(date.getSeconds());
  }

  function zeroPrefix(value) {
    if (value < 10) {
      return "0" + value;
    } else {
      return "" + value;
    }
  }

});
