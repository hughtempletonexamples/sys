"use strict";

var utils = require("../testUtils");

describe("dashboard", function () {

  var MockFirebase = require("firebase-mock").MockFirebase;
  var $controller;
  var $rootScope;
  var $mdToast;
  
  var mockPanelsService;
  var sideNavMock;
  var toastMock;
  var dialogMock;
  
  var projectsRef;
  var areasRef;
  var panelsRef;
  var bookingRef;
  var systemTimestamp;
  var createdTimestamp;
  
  var project1 = {
    "$id": "PROJ1",
    "name": "Project 1",
    "id": "001",
    "active": true,
    "deliverySchedule": {
      "unpublished": "PROJ1REV1",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    $id: "AREA1",
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 50,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "completedArea": 95,
    "completedPanels": 14,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2018-11-20",
      "PROJ1REV1": "2018-11-20"
    },
    "timestamps": {
      "created": new Date("2018-11-20").getTime(),
      "modified": new Date("2018-11-20").getTime()
    }
  };

  var area2 = {
    $id: "AREA2",
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV1": "2018-11-20"
    },
    "timestamps": {
      "created": new Date("2018-11-20").getTime(),
      "modified": new Date("2018-11-20").getTime()
    }
  };

  var area3 = {
    $id: "AREA3",
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2018-12-11",
      "PROJ1REV1": "2018-12-11"
    },
    "timestamps": {
      "created": new Date("2018-12-11").getTime(),
      "modified": new Date("2018-12-11").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "booking": "booking1",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-C-001",
    "project": "PROJ1",
    "type": "Roof",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel4 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-H-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel5 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-I-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.dashboard", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
    
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    sideNavMock = jasmine.createSpy();
    toastMock = jasmine.createSpy();
    dialogMock = jasmine.createSpy();

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("$mdSidenav", function () {
        return sideNavMock;
      });
      $provide.factory("$mdToast", function () {
        return toastMock;
      });
      $provide.factory("$mdDialog", function () {
        return dialogMock;
      });
    });
    
    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");
    bookingRef = firebaseRoot.child("bookings");
    
    utils.addQuerySupport(areasRef);
    utils.addQuerySupport(bookingRef);
    utils.addQuerySupport(projectsRef);
    utils.addQuerySupport(panelsRef);
    
    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($injector, _$controller_, _$rootScope_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    
    $mdToast = $injector.get("$mdToast");
  }));

  function createController(locals) {
    return $controller("BookingController", angular.extend(
      {
        $scope: $rootScope.$new()
      },
      locals));
  }
  
  beforeEach(function () {
    
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5 
    });
    
  });
  
  
  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      if(areaRef == "AREA1"){
        return $firebaseArray(panelsRef);
      }else{
        panelsRef.set({});
        return $firebaseArray(panelsRef);
      }
    });
  }));
  
  beforeEach(function () {
    var now = new Date("2018-12-12");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);
    
    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp + 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });
  
  beforeEach(function () {
    bookingRef.set({
      "booking1": {
        "bookedTime": systemTimestamp,
        "areas": {
          "AREA2": true
        }
      }
    });
  });

  xdescribe("booking controller", function () {

    beforeEach(inject(function ($location) {
      $location.url("/dashboard/booking");
    }));

    it("adds area to booking", function () {

      $mdToast.hide = jasmine.createSpy().and.callFake(function () {
        return null;
      });
      
      var $scope = $rootScope.$new();
      var controller = createController();
      controller.addAreaToBooking("booking1", area1);
      
      $scope.$apply();
      
      expect(utils.getRefChild(bookingRef, 0)).toEqual({});
      
    });

  });

});
