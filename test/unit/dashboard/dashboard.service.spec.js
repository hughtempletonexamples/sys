"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var testUtils = require("../testUtils");
var USER_ID = "{user-guid}";
var USER_NAME = "First Last";
describe("dashboard service", function () {
  var firebaseRoot;

  var capacityRef;
  var statsRef;
  var benchmarksRef;
  var capacitySubRef;
  var projectsRef;
  var additionalDataRef;
  var areasRef;
  var panelsRef;
  var areaPlansRef;
  var mockPanelsService;
  var mockReferenceDataService;
  var srdRef;
  var componentsRef;
  var bookingRef;
  var randomNumber;
  
  var mockSchema;
  var mockSettingsService;
  var mockAuditService;
  var scope;
  var $q;
  var $timeout;
  var $log;
  var $firebaseArray;
  
  var systemTimestamp;
  var createdTimestamp;

  var dashboardService;
  var firebaseUtil;
  
  var project1 = {
    "$id": "PROJ1",
    "name": "Project 1",
    "id": "001",
    "active": true,
    "deliverySchedule": {
      "unpublished": "PROJ1REV1",
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    $id: "AREA1",
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 50,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "completedArea": 95,
    "completedPanels": 14,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2018-11-20",
      "PROJ1REV1": "2018-11-20"
    },
    "timestamps": {
      "created": new Date("2018-11-20").getTime(),
      "modified": new Date("2018-11-20").getTime()
    }
  };

  var area2 = {
    $id: "AREA2",
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV1": "2018-11-20"
    },
    "timestamps": {
      "created": new Date("2018-11-20").getTime(),
      "modified": new Date("2018-11-20").getTime()
    }
  };

  var area3 = {
    $id: "AREA3",
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "estimatedArea": 100,
    "estimatedPanels": 20,
    "actualArea": 95,
    "actualPanels": 15,
    "supplier": "Innovaré",
    "revisions": {
      "PROJ1REV0": "2018-12-11",
      "PROJ1REV1": "2018-12-11"
    },
    "timestamps": {
      "created": new Date("2018-12-11").getTime(),
      "modified": new Date("2018-12-11").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-C-001",
    "project": "PROJ1",
    "type": "Roof",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel4 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-H-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  var panel5 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-I-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2018-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2018-11-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.dashboard", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    //note productionSubCapacities and productionCapacities have been flipped over to save dev time (Going through all code just to make a little change)
    capacityRef = firebaseRoot.child("productionSubCapacities");
    capacitySubRef = firebaseRoot.child("productionCapacities");
    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    additionalDataRef = firebaseRoot.child("additionalData/areas");
    srdRef = firebaseRoot.child("srd");
    benchmarksRef = firebaseRoot.child("benchmarks");
    statsRef = firebaseRoot.child("stats");
    bookingRef = firebaseRoot.child("bookings");
    componentsRef = firebaseRoot.child("components");
    
    testUtils.addQuerySupport(areasRef);
    testUtils.addQuerySupport(areaPlansRef);
    testUtils.addQuerySupport(projectsRef);
    testUtils.addQuerySupport(panelsRef);
    testUtils.addQuerySupport(componentsRef);
    testUtils.addQuerySupport(bookingRef);
    testUtils.addQuerySupport(srdRef);

    mockSettingsService = jasmine.createSpyObj("settings", ["get"]);
    mockAuditService = jasmine.createSpyObj("audit", ["productionLine"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductionLines", "getProductTypes", "getSuppliers"]);


    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("settingsService", function () {
        return mockSettingsService;
      });
      $provide.factory("audit", function () {
        return mockAuditService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("mockReferenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    testUtils.setupMockSchema(mockSchema, firebaseRoot, USER_ID);
    mockSchema.status.userName = USER_NAME;
   


  });

  beforeEach(inject(function ($rootScope, _$q_, _$timeout_, _$log_, _$firebaseArray_, _dashboardService_,_firebaseUtil_) {
    scope = $rootScope;
    $q = _$q_;
    $timeout = _$timeout_;
    $log = _$log_;
    $firebaseArray = _$firebaseArray_;
    dashboardService = _dashboardService_;
    firebaseUtil = _firebaseUtil_;
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      if(areaRef == "AREA1"){
        return $firebaseArray(panelsRef);
      }else{
        panelsRef.set({});
        return $firebaseArray(panelsRef);
      }
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(srdRef.child("productionLines"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(srdRef.child("productTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getSuppliers.and.callFake(function () {
      return $firebaseArray(srdRef.child("suppliers"));
    });
  }));

  beforeEach(function () { 
    mockSettingsService.get.and.returnValue($q.when({designHandoverPeriod: 10}));
  });
  
   
  beforeEach(function () {

    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5 
    });
    
  });
  
  beforeEach(function () {
    componentsRef.set({
      "component1": {
        "project": "PROJ1",
        "areas": {
          "AREA1": true,
          "AREA2": true
        },
        "type": "Walls"
      },
      "component2": {
        "project": "PROJ1",
        "areas": {
          "AREA3": true
        },
        "type": "HSIPs"
      }
    });
    
    bookingRef.set({
      "booking1": {
        "bookedTime": systemTimestamp,
        "areas": {
          "AREA1": true,
          "AREA2": true
        }
      }
    });
  });

  beforeEach(function () {
    
    
    srdRef.child("components").set(
      {
        "Floor": {"name": "Floor", "productTypes": {"CrD": true, "Ext": true, "Floor": true,"Int": true},"seq": 1},
        "HSIPs": {"name": "HSIPs", "productTypes": {"ExtH": true}, "seq": 2},
        "Roof": {"name": "Roof", "productTypes": {"Ext": true, "Int": true, "Roof": true},"seq": 3},
        "Walls": {"name": "Walls", "productTypes": {"Ext": true, "ExtH": true,"Int": true},"seq": 4}
      }
    );
    
    srdRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );
  
    srdRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 35 },
        "CASS": { defaultCapacity: 6 }
      }
    );

    srdRef.child("productionSubLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "SIP2": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "HSIP2": { defaultCapacity: 0 },
        "TF": { defaultCapacity: 35 },
        "TF2": { defaultCapacity: 0 },
        "CASS": { defaultCapacity: 6 },
        "CASS2": { defaultCapacity: 0 }
      }
    );

    srdRef.child("suppliers").set(
      {
        "Innovaré": { thirdParty: false },
        "Other": { thirdParty: true }
      }
    );
    
    areaPlansRef.set({
      "AREA1": {
        "buildDays" : {
          "2018-12-06" : 1,
          "2018-12-07" : 20,
          "2018-12-10" : 20,
          "2018-12-11" : 20,
          "2018-12-12" : 6,
          "2018-12-13" : 20,
          "2018-12-14" : 20,
          "2018-12-15" : 10
        },
        "riskDate" : "2018-12-16",
        "plannedFinish" : "2018-12-15",
        "plannedStart" : "2018-12-06",
        "project" : "PROJ1"
      },
      "AREA2": {
        "buildDays" : {
          "2018-12-10" : 1,
          "2018-12-11" : 20,
          "2018-12-12" : 20,
          "2018-12-13" : 20
        },
        "plannedFinish" : "2018-12-13",
        "plannedStart" : "2018-12-10",
        "project" : "PROJ2"
      }
    });
    
    additionalDataRef.set({
      "AREA1": {
        "buildPlannedDays" : {
          "2018-12-12" : 2
        }
      },
      "AREA2": {
        "buildPlannedDays" : {
          "2018-12-12" : 2
        }
      }
    });
    
    benchmarksRef.set({
      "CASS" : {
        "2018-12-12" : {
          "meterSquared" : 60,
          "operators" : 2
        },
        "2018-12-13" : {
          "aBench" : 4,
          "aPeople" : 6
        }
      },
      "TF" : {
        "2018-12-12" : {
          "meterSquared" : 60,
          "operators" : 2
        },
        "2018-12-13" : {
          "aBench" : 4,
          "aPeople" : 6
        }
      }, 
      "SIP" : {
        "2018-12-12" : {
          "meterSquared" : 5,
          "operators" : 2,
          "aBench" : 4,
          "aPeople" : 6
        },
        "2018-12-13" : {
          "aBench" : 4,
          "aPeople" : 6
        }
      }, 
      "SIP2" : {
        "2018-12-12" : {
          "meterSquared" : 60,
          "operators" : 2
        },
        "2018-12-13" : {
          "aBench" : 4,
          "aPeople" : 6
        }
      }
    });
    
      
    capacityRef.set(
      {
        SIP: {
          "2018-12-10": 20,
          "2018-12-11": 20,
          "2018-12-12": 20
        },
        HSIP: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        TF: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        CASS: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        }
      }
    );
    
    capacitySubRef.set(
      {
        SIP: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        SIP2: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        HSIP: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        TF: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        },
        CASS: {
          "2018-12-10": 10,
          "2018-12-11": 10,
          "2018-12-12": 10
        }
      }
    );
    
    statsRef.set(
      {
        "SIP": 
        {
          "2018-12-10": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-11": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-12": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "HSIP": 
        {
          "2018-12-10": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-11": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-12": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "TF": 
        {
          "2018-12-10": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-11": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-12": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        },
        "CASS": 
        {
          "2018-12-10": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-11": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          },
          "2018-12-12": {
            "designedM2": 80,
            "designedCount": 3,
            "deliveryM2": 60.5,
            "deliveryCount":2,
            "completedM2": 150,
            "completedCount": 3
          }
        }
      }
    );
  });

  beforeEach(function () {
    var now = new Date("2018-12-12");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);
    
    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp + 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  describe("human resources", function () {

    it("allows the setting of actual benches and people for each production line and a calculated output - resource days", function () {
      // all depends on buildDays or buildPlannedDays being set if no data will display as 0
      // fBenches: 5, fPeople: 10 and output are calculated rest are inputs
      dashboardService.humanResource()
        .then(function (data) {
          expect(data.resourceDays["2018-12-12"]["SIP"]).toEqual(
            { operators: 2, meterSquared: 5, fBenches: 5, fPeople: 10, panels: 4, fm2: 23, aBench: 4, aPeople: 6 }
          );
        });
        
      scope.$apply();
    });
    
    it("testing for line benches", function () {
      // all depends on buildDays or buildPlannedDays being set if no data will display as 0
      // fBenches: 5, fPeople: 10 are calculated rest are inputs
      dashboardService.humanResource()
        .then(function (data) {
          expect(data.lineBenches["SIP"]["2018-12-12"]).toEqual(
            {
              "meterSquared": 5,
              "operators": 2,
              "aBench": 4,
              "aPeople": 6
            }
          );
        });
        
      scope.$apply();
    });

  });
  
  describe("areas for production", function () {

    it("orders areas by the start date from buildDays or buildPlannedDays", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          expect(data.areasByStart["2018-12-03"]["2018-12-06"][0]).toEqual(jasmine.objectContaining({"$id": "AREA1", "type": "Ext","_startDate":"2018-12-06","_finishDate":"2018-12-15"}));
        });
        
      scope.$apply();
    });
    
    
    it("weekly chart data for total planM2 from buildDays or buildPlannedDays ", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          expect(data.chartData.planned["SIP"]).toEqual([{ x: "2018-11-19", y: 0 },{ x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 },{ x: "2018-12-10", y: 22.7 },{ x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 },{ x: "2018-12-31", y: 0 }]);
          expect(data.chartData.manufactured["SIP"]).toEqual([{ x: "2018-11-19", y: 0 },{ x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 },{ x: "2018-12-10", y: 450 },{ x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 },{ x: "2018-12-31", y: 0 }]);
        });
        
      scope.$apply();
    });
    
    it("tests days of week are correct", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          var daysOfWeekLength = Object.keys(data.daysOfWeeks).length;
          expect(daysOfWeekLength).toEqual(6);
          expect(data.daysOfWeeks).toEqual({"2018-12-10":{},"2018-12-11":{},"2018-12-12":{},"2018-12-13":{},"2018-12-14":{},"2018-12-15":{}});
        });
        
      scope.$apply();
    });
    
    it("testing for stats", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          expect(data.productionStats["SIP"]["2018-12-12"]).toEqual({ designedM2: 80, designedCount: 3, deliveryM2: 60.5, deliveryCount: 2, completedM2: 150, completedCount: 3 });
        });
        
      scope.$apply();
    });
    
    it("testing for additional data for areas", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          expect(data.additionalData["AREA1"]).toEqual(jasmine.objectContaining({
            "buildPlannedDays": {
              "2018-12-12": 2
            }
          }));
        });
        
      scope.$apply();
    });
    
    it("tests weeks are correct - the total of planM2 should equal week on chart (2018-12-03, y: 133) rounded", function () {
      dashboardService.areasForProduction("2018-12-12")
        .then(function (data) {
          expect(data.weeks["SIP"]["2018-12-12"]).toEqual({ planM2: 22.666666666666664, planPanels: 4, manufacturedM2: 150, manufacturedPanels: 3 });
          
        });
        
      scope.$apply();
    });

  });
  
  describe("areas for design", function () {

    it("orders areas by the Design Handover Date from buildDays or buildPlannedDays", function () {
      dashboardService.areasForDesign("2018-12-12")
        .then(function (data) {
          expect(data.areasByDesignHandoverDate["2018-11-05"]["2018-11-06"][0]).toEqual(jasmine.objectContaining({"$id": "AREA1", "type": "Ext"}));
        });

      scope.$apply();
    });
    
    it("weekly chart data for total predictedPlanM2 from buildDays or buildPlannedDays", function () {
      dashboardService.areasForDesign("2018-12-12")
        .then(function (data) {
          //tests out of date select scope
          expect(data.chartData["predictedPlanM2"]).toEqual([{ x: "2018-11-19", y: 0 }, { x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 0 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
        });
        
      scope.$apply();
    });
    
    it("weekly chart data for total actualPlanM2 from buildDays or buildPlannedDays", function () {
      dashboardService.areasForDesign("2018-12-12")
        .then(function (data) {
          expect(data.chartData["actualPlanM2"]).toEqual([{ x: "2018-11-19", y: 0 }, { x: "2018-11-26", y: 95 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 0 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
        });
        
      scope.$apply();
    });
    
    it("make sure week start is brought back by Design Handover", function () {
      dashboardService.areasForDesign("2018-12-12")
        .then(function (data) {
          expect(data.weekStartReturn).toEqual("2018-12-10");
        });
        
      scope.$apply();
    });
    
    it("tests days of week are correct", function () {
      dashboardService.areasForDesign("2018-12-12")
        .then(function (data) {
          var daysOfWeekLength = Object.keys(data.daysOfWeeks).length;
          expect(daysOfWeekLength).toEqual(6);
          expect(data.daysOfWeeks).toEqual({"2018-12-10":{},"2018-12-11":{},"2018-12-12":{},"2018-12-13":{},"2018-12-14":{},"2018-12-15":{}});
        });
        
      scope.$apply();
    });

  });
  
  describe("areas for logistics", function () {
    
    it("Chart data from logistics", function () {
      dashboardService.areasForLogistics("2018-12-12")
        .then(function ( data ) {
          expect(data.chartData["booked"]).toEqual([{ x: "2018-11-19", y: 0 }, { x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 1 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
          expect(data.chartData["deliverys"]).toEqual([{ x: "2018-11-19", y: 1 }, { x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 1 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
        });
      scope.$apply();
    });
    
    it("Groups all areas by component", function () {
      dashboardService.areasForLogistics("2018-11-21")
        .then(function ( data ) {

          expect(Object.keys(data.areasByDispatch)).toEqual(["2018-11-19"]);

          expect(data.areasByDispatch["2018-11-19"]["Walls1"].areas.length).toEqual(2);
          expect(data.areasByDispatch["2018-11-19"]["Walls1"].areas[0]).toEqual(jasmine.objectContaining(area1));
          
          expect(data.areasByDispatch["2018-11-19"]["Walls1"]).toEqual(jasmine.objectContaining({
            componentId: "component1",
            comments: undefined,
            projectManagerName: "",
            projectManagerContact: "",
            siteAccess: undefined,
            siteAddress: undefined,
            estimatedAreaTotal: 150,
            estimatedCountTotal: 40,
            actualAreaTotal: 95,
            actualCountTotal: 15,
            completedAreaTotal: 95,
            riskDays: 0,
            completedCountTotal: 14
          }));


        });
      scope.$apply();
    });
    
    it("Tests days of week are correct", function () {
      dashboardService.areasForLogistics("2018-12-12")
              .then(function ( data ) {
                var daysOfWeekLength = Object.keys(data.daysOfWeeks).length;
                expect(daysOfWeekLength).toEqual(6);
                expect(data.daysOfWeeks).toEqual({
                  "2018-12-10": {
                    deliverys: 1,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-11": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-12": {
                    deliverys: 0,
                    booked: 1,
                    unplanned: 0
                  },
                  "2018-12-13": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-14": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-15": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  }
                });
              });

      scope.$apply();
    });
    
    it("Gorups all logistics by logistic date", function () {
      dashboardService.areasForLogistics("2018-12-12")
              .then(function ( data ) {
                expect(data.groupByBookingDate).toEqual({ 
                  1544572800000: { 
                    booking1: { 
                      booking: { 
                        areas: { AREA1: true, AREA2: true }, 
                        bookedTime: 1544572800000, 
                        $id: "booking1", $priority: 0 
                      }, 
                      areas: { 
                        AREA1: { 
                          area: { 
                            actualArea: 95, 
                            actualPanels: 15, 
                            completedArea: 95, 
                            completedPanels: 14, 
                            estimatedArea: 50, 
                            estimatedPanels: 20, 
                            floor: "GF", 
                            phase: "P1", 
                            project: "PROJ1", 
                            revisions: { 
                              PROJ1REV0: "2018-11-20", 
                              PROJ1REV1: "2018-11-20" 
                            }, 
                            supplier: "Innovaré", 
                            timestamps: { 
                              created: 1542672000000, 
                              modified: 1542672000000 
                            }, 
                            type: "Ext", 
                            $id: "AREA1", 
                            $priority: null, 
                            _id: "001-P1-GF-Ext", 
                            _bookings: [{ areas: { AREA1: true, AREA2: true }, bookedTime: 1544572800000, $id: "booking1", $priority: 0 }], 
                            _deliveryDate: "2018-11-20", 
                            _hasUnpublished: false, 
                            _hasUnpublishedOnly: false, 
                            _idWithoutType: "001-P1-GF", 
                            _dispatchDate: "2018-11-19", 
                            _riskDate: "2018-11-16", 
                            _designHandoverDate: "2018-11-06", 
                            _designer: "", 
                            _productionLine: "SIP", 
                            _line: 1 
                          }, 
                          bookedPanels: 0 
                        }, 
                        AREA2: { 
                          area: { 
                            estimatedArea: 100, 
                            estimatedPanels: 20, 
                            floor: "GF", 
                            phase: "P2", 
                            project: "PROJ1", 
                            revisions: { PROJ1REV1: "2018-11-20" }, 
                            supplier: "Innovaré", 
                            timestamps: { 
                              created: 1542672000000, 
                              modified: 1542672000000 
                            }, 
                            type: "Ext", 
                            $id: "AREA2", 
                            $priority: null,
                            _id: "001-P2-GF-Ext", 
                            _bookings: [{ areas: { AREA1: true, AREA2: true }, bookedTime: 1544572800000, $id: "booking1", $priority: 0 }], 
                            _deliveryDate: "2018-11-20", 
                            _hasUnpublished: true,
                            _hasUnpublishedOnly: true, 
                            _idWithoutType: "001-P2-GF", 
                            _dispatchDate: "2018-11-19", 
                            _riskDate: "2018-11-16", 
                            _designHandoverDate: "2018-11-06", 
                            _designer: "", 
                            _productionLine: "SIP", 
                            _line: 1, 
                            _unpublishedDeliveryDate: "2018-11-20", 
                            _unpublishedDispatchDate: "2018-11-19", 
                            _unpublishedRiskDate: "2018-11-16", 
                            _unpublishedDesignHandoverDate: "2018-11-06" 
                          }, 
                          bookedPanels: 0 
                        } 
                      } 
                    }
                  }

                });

              });         
      scope.$apply();
    });
    
  });
  
  describe("areas for booking", function () {
    
    it("Chart data from booking", function () {
      dashboardService.areasForBooking("2018-12-12")
        .then(function ( data ) {
          expect(data.chartData["booked"]).toEqual([{ x: "2018-11-19", y: 0 }, { x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 1 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
          expect(data.chartData["deliverys"]).toEqual([{ x: "2018-11-19", y: 1 }, { x: "2018-11-26", y: 0 }, { x: "2018-12-03", y: 0 }, { x: "2018-12-10", y: 1 }, { x: "2018-12-17", y: 0 }, { x: "2018-12-24", y: 0 }, { x: "2018-12-31", y: 0 }]);
        });
      scope.$apply();
    });
    
    it("Groups all areas by component", function () {
      dashboardService.areasForBooking("2018-12-12")
        .then(function ( data ) {

          expect(Object.keys(data.areasByDispatch)).toEqual(["2018-12-10"]);

          expect(data.areasByDispatch["2018-12-10"]["HSIPs1"].areas.length).toEqual(1);
          expect(data.areasByDispatch["2018-12-10"]["HSIPs1"].areas[0]).toEqual(jasmine.objectContaining(area3));

        });
      scope.$apply();
    });
    
    it("Tests days of week are correct", function () {
      dashboardService.areasForBooking("2018-12-12")
              .then(function ( data ) {
                var daysOfWeekLength = Object.keys(data.daysOfWeeks).length;
                expect(daysOfWeekLength).toEqual(6);
                expect(data.daysOfWeeks).toEqual({
                  "2018-12-10": {
                    deliverys: 1,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-11": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-12": {
                    deliverys: 0,
                    booked: 1,
                    unplanned: 0
                  },
                  "2018-12-13": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-14": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  },
                  "2018-12-15": {
                    deliverys: 0,
                    booked: 0,
                    unplanned: 0
                  }
                });
              });

      scope.$apply();
    });
    
    it("Gorups all bookings by booking date", function () {
      dashboardService.areasForBooking("2018-12-12")
              .then(function ( data ) {
                expect(data.groupByBookingDate).toEqual({ 
                  1544572800000: { 
                    booking1: { 
                      booking: { 
                        areas: { AREA1: true, AREA2: true }, 
                        bookedTime: 1544572800000, 
                        $id: "booking1", $priority: 0 
                      }, 
                      areas: { 
                        AREA1: { 
                          area: { 
                            actualArea: 95, 
                            actualPanels: 15, 
                            completedArea: 95, 
                            completedPanels: 14, 
                            estimatedArea: 50, 
                            estimatedPanels: 20, 
                            floor: "GF", 
                            phase: "P1", 
                            project: "PROJ1", 
                            revisions: { 
                              PROJ1REV0: "2018-11-20", 
                              PROJ1REV1: "2018-11-20" 
                            }, 
                            supplier: "Innovaré", 
                            timestamps: { 
                              created: 1542672000000, 
                              modified: 1542672000000 
                            }, 
                            type: "Ext", 
                            $id: "AREA1", 
                            $priority: null, 
                            _id: "001-P1-GF-Ext", 
                            _bookings: [{ areas: { AREA1: true, AREA2: true }, bookedTime: 1544572800000, $id: "booking1", $priority: 0 }], 
                            _deliveryDate: "2018-11-20", 
                            _hasUnpublished: false, 
                            _hasUnpublishedOnly: false, 
                            _idWithoutType: "001-P1-GF", 
                            _dispatchDate: "2018-11-19", 
                            _riskDate: "2018-11-16", 
                            _designHandoverDate: "2018-11-06", 
                            _designer: "", 
                            _productionLine: "SIP", 
                            _line: 1 
                          }, 
                          bookedPanels: 0 
                        }, 
                        AREA2: { 
                          area: { 
                            estimatedArea: 100, 
                            estimatedPanels: 20, 
                            floor: "GF", 
                            phase: "P2", 
                            project: "PROJ1", 
                            revisions: { PROJ1REV1: "2018-11-20" }, 
                            supplier: "Innovaré", 
                            timestamps: { 
                              created: 1542672000000, 
                              modified: 1542672000000 
                            }, 
                            type: "Ext", 
                            $id: "AREA2", 
                            $priority: null,
                            _id: "001-P2-GF-Ext", 
                            _bookings: [{ areas: { AREA1: true, AREA2: true }, bookedTime: 1544572800000, $id: "booking1", $priority: 0 }], 
                            _deliveryDate: "2018-11-20", 
                            _hasUnpublished: true,
                            _hasUnpublishedOnly: true, 
                            _idWithoutType: "001-P2-GF", 
                            _dispatchDate: "2018-11-19", 
                            _riskDate: "2018-11-16", 
                            _designHandoverDate: "2018-11-06", 
                            _designer: "", 
                            _productionLine: "SIP", 
                            _line: 1, 
                            _unpublishedDeliveryDate: "2018-11-20", 
                            _unpublishedDispatchDate: "2018-11-19", 
                            _unpublishedRiskDate: "2018-11-16", 
                            _unpublishedDesignHandoverDate: "2018-11-06" 
                          }, 
                          bookedPanels: 0 
                        } 
                      } 
                    }
                  }

                });

              });         
      scope.$apply();
    });
    
  });
  
  describe("save additional Data labels", function () {
    it("saves labels in additional data",function () {
      dashboardService.updateAddData("AREA1", "haulierType", "HiAb")
              .then(function (data) {
                testUtils.expectRefChildToContain(additionalDataRef,0,{ "buildPlannedDays": { "2018-12-12": 2 }, "haulierType": "HiAb" });
              });
      scope.$apply();
    });    
  });//update add area
  
  describe("update booking updates booking information, or saves new ",function (){
    it("update booking updates booking information :",function (){
      dashboardService.updateBookingData("booking1", "haulierType", "HiAb")
              .then(function (data){
                testUtils.expectRefChildToContain(bookingRef,0,{ "areas": {"AREA1":true, "AREA2":true}, "haulierType": "HiAb" });
              });
      scope.$apply(); 
    });
    
  });
  
  describe("set panels actually set panels", function () {
    
    it("set panels actually", function () {
      dashboardService.setPanel("panel1", "booking", "booking1")
              .then(function (data) {
                testUtils.expectRefChildToContain(panelsRef, 0, { "booking": "booking1" });
              });
      scope.$apply();
    });

  });
  
  describe("addAreaToBooking", function () {

    it("set panels bookings reference", function () {
      dashboardService.addAreaToBooking("booking1",area1)
              .then(function (data) {
                testUtils.expectRefChildToContain(panelsRef, 0, { "area": "AREA1", "booking": "booking1"});
                testUtils.expectRefChildToContain(panelsRef, 1, { "area": "AREA1", "booking": "booking1"});
                testUtils.expectRefChildToContain(panelsRef, 2, { "area": "AREA2", "booking": "booking1"});
                testUtils.expectRefChildToContain(panelsRef, 3, { "area": "AREA3"});
                testUtils.expectRefChildToContain(panelsRef, 4, { "area": "AREA3"});
              });
      scope.$apply();
    });
    it("sets area in booking", function () {
      
      bookingRef.set({
        "booking1": {
          "bookedTime": systemTimestamp,
          "areas": {
            "AREA2": true
          }
        }
      });
    
      dashboardService.addAreaToBooking("booking1",area1)
              .then(function (data) {
                testUtils.expectRefChildToContain(bookingRef, 0, {"bookedTime": systemTimestamp, "areas":{"AREA1":true,"AREA2":true}});
              });
      scope.$apply();
    }); 
    it("Booking doesnt exist", function () {
      dashboardService.addAreaToBooking("booking767868",area1)
              .then(function (data) {
                expect(bookingRef.booking767868).toEqual(undefined);
              });
      scope.$apply();
    });
    it("Additional data gets updated", function () {
      dashboardService.addAreaToBooking("booking1",area1)
              .then(function (data) {
                testUtils.expectRefChildToContain(additionalDataRef, 0 ,{ "bookings": { "booking1": true }});
              });
      scope.$apply();
    }); 

  });

  describe("save additional Data", function () {
    it("saves objects for data its given :type:", function () {
      dashboardService.updateAddCheckData("AREA1", "checkedDFM", true)
              .then(function ( data ) {
                testUtils.expectRefChildToContain(additionalDataRef,0,{checkedDFM: { "status": true, "timeDate": systemTimestamp, "user": "FL" } });
              });
      scope.$apply();
    });    
  });  
  
  describe("Update Booking Add Check Data Delivered", function () {

    it("updates delivered checkbox in a specified booking", function () {
      dashboardService.updateBookingAddCheckDataDelivered("booking1", true)
              .then(function () {
                testUtils.expectRefChildToContain(bookingRef, 0, {delivered: {"status": true, "timeDate": systemTimestamp, "user": "FL"}});
              });
      scope.$apply();
    });
    
    it("if all areas panels area in bookings delivered, area delivered should be set to true", function () {
      
      additionalDataRef.set({
        "AREA1": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        },  
        "AREA2": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        }
      });
    
      dashboardService.updateBookingAddCheckDataDelivered("booking1", true)
              .then(function () {
                var savedArea = testUtils.getRefChild(areasRef, 0);
                expect(savedArea).toEqual(jasmine.objectContaining({delivered: true}));
              });
              
      scope.$apply();
    });
    
    it("if all areas panels area in bookings delivered, area delivered should be set to false", function () {
      
      additionalDataRef.set({
        "AREA1": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        },  
        "AREA2": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        }
      });
    
      dashboardService.updateBookingAddCheckDataDelivered("booking1", false)
              .then(function () {
                var savedArea = testUtils.getRefChild(areasRef, 0);
                expect(savedArea).toEqual(jasmine.objectContaining({delivered: false}));
              });
              
      scope.$apply();
    });

  });
  
  describe("Remove area from booking", function () {
    
    beforeEach(function () {

      var project1 = {
        "$id": "PROJ1",
        "name": "Project 1",
        "id": "001",
        "active": true,
        "deliverySchedule": {
          "unpublished": "PROJ1REV1",
          "revisions": {
            "PROJ1REV0": true,
            "PROJ1REV1": true
          },
          "published": "PROJ1REV0"
        },
        "datumType": "Education Student Accommodation"
      };

      var area1 = {
        $id: "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 95,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        },
        "delivered": true
      };

      var area2 = {
        $id: "AREA2",
        "floor": "GF",
        "phase": "P2",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 100,
        "estimatedPanels": 20,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        },
        "delivered": true
      };

      var area3 = {
        $id: "AREA3",
        "floor": "GF",
        "phase": "P3",
        "type": "ExtH",
        "project": "PROJ1",
        "estimatedArea": 100,
        "estimatedPanels": 20,
        "actualArea": 95,
        "actualPanels": 15,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-12-11",
          "PROJ1REV1": "2018-12-11"
        },
        "timestamps": {
          "created": new Date("2018-12-11").getTime(),
          "modified": new Date("2018-12-11").getTime()
        }
      };

      var panel1 = {
        "area": "AREA1",
        "id": "001-DA-P1-GF-I-001",
        "project": "PROJ1",
        "type": "Ext",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        },
        "booking": "booking1"
      };

      var panel2 = {
        "area": "AREA1",
        "id": "001-DA-P1-GF-I-002",
        "project": "PROJ1",
        "type": "Ext",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:15:00+01:00").getTime()
        },
        "booking": "booking1"
      };

      var panel3 = {
        "area": "AREA2",
        "id": "001-DA-P2-GF-C-001",
        "project": "PROJ1",
        "type": "Roof",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        },
        "booking": "booking1"
      };

      var panel4 = {
        "area": "AREA3",
        "id": "001-DA-P3-GF-H-001",
        "project": "PROJ1",
        "type": "ExtH",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        }
      };

      var panel5 = {
        "area": "AREA3",
        "id": "001-DA-P3-GF-I-001",
        "project": "PROJ1",
        "type": "ExtH",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 1100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        }
      };

      projectsRef.set({
        "PROJ1": project1
      });

      areasRef.set({
        "AREA1": area1,
        "AREA2": area2,
        "AREA3": area3
      });

      panelsRef.set({
        "panel1": panel1,
        "panel2": panel2,
        "panel3": panel3,
        "panel4": panel4,
        "panel5": panel5
      });
      
      additionalDataRef.set({
        "AREA1": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        },
        "AREA2": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        }
      });
      
      bookingRef.set({
        "booking1": {
          "bookedTime": systemTimestamp,
          "areas": {
            "AREA1": true,
            "AREA2": true
          },
          "delivered": {"status": true, "timeDate": systemTimestamp, "user": "FL"}
        }
      });

    });

    it("panels delivered removed", function () {

      dashboardService.removeAreaFromBooking(area1, "booking1")
              .then(function () {
                var savedPanel1 = testUtils.getRefChild(panelsRef, 0);
                expect(savedPanel1).toEqual({
                  "area": "AREA1",
                  "id": "001-DA-P1-GF-I-001",
                  "project": "PROJ1",
                  "type": "Ext",
                  "dimensions": {
                    "area": 0.5,
                    "width": 162,
                    "height": 430,
                    "length": 3100,
                    "weight": 19
                  },
                  "qa": {
                    "diagonalHeight": 700,
                    "diagonalWidth": 900,
                    "midHeight": 435,
                    "midLength": 1102,
                    "started": new Date("2018-11-20T10:30:00+01:00").getTime()
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20T12:13:00").getTime()
                  }
                });
                var savedPanel2 = testUtils.getRefChild(panelsRef, 1);
                expect(savedPanel2).toEqual({
                  "area": "AREA1",
                  "id": "001-DA-P1-GF-I-002",
                  "project": "PROJ1",
                  "type": "Ext",
                  "dimensions": {
                    "area": 0.5,
                    "width": 162,
                    "height": 430,
                    "length": 3100,
                    "weight": 19
                  },
                  "qa": {
                    "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
                    "diagonalHeight": 700,
                    "diagonalWidth": 900,
                    "midHeight": 435,
                    "midLength": 1102,
                    "started": new Date("2018-11-20T10:30:00+01:00").getTime()
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20T12:15:00+01:00").getTime()
                  }
                });
              });
      scope.$apply();
    });
    
    it("Remove Delivered From Area", function () {

      dashboardService.removeAreaFromBooking(area1, "booking1")
              .then(function () {
                var savedArea1 = testUtils.getRefChild(areasRef, 0);
        
                expect(savedArea1).toEqual({
                  "floor": "GF",
                  "phase": "P1",
                  "type": "Ext",
                  "project": "PROJ1",
                  "estimatedArea": 50,
                  "estimatedPanels": 20,
                  "actualArea": 95,
                  "actualPanels": 15,
                  "completedArea": 95,
                  "completedPanels": 14,
                  "supplier": "Innovaré",
                  "revisions": {
                    "PROJ1REV0": "2018-11-20",
                    "PROJ1REV1": "2018-11-20"
                  },
                  "timestamps": {
                    "created": 1542672000000,
                    "modified": 1542672000000
                  }
                });

              });
      scope.$apply();
    });
    
    it("Remove Delivered From Area (testing it doesn't remove from other areas)", function () {

      dashboardService.removeAreaFromBooking(area1, "booking1")
              .then(function () {
                var savedArea1 = testUtils.getRefChild(areasRef, 0);

                expect(savedArea1).toEqual({
                  "floor": "GF",
                  "phase": "P1",
                  "type": "Ext",
                  "project": "PROJ1",
                  "estimatedArea": 50,
                  "estimatedPanels": 20,
                  "actualArea": 95,
                  "actualPanels": 15,
                  "completedArea": 95,
                  "completedPanels": 14,
                  "supplier": "Innovaré",
                  "revisions": {
                    "PROJ1REV0": "2018-11-20",
                    "PROJ1REV1": "2018-11-20"
                  },
                  "timestamps": {
                    "created": 1542672000000,
                    "modified": 1542672000000
                  }
                });

                var savedArea2 = testUtils.getRefChild(areasRef, 1);

                expect(savedArea2).toEqual({
                  $id: "AREA2",
                  "floor": "GF",
                  "phase": "P2",
                  "type": "Ext",
                  "project": "PROJ1",
                  "estimatedArea": 100,
                  "estimatedPanels": 20,
                  "supplier": "Innovaré",
                  "revisions": {
                    "PROJ1REV1": "2018-11-20"
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20").getTime(),
                    "modified": new Date("2018-11-20").getTime()
                  },
                  "delivered": true
                });

              });
      scope.$apply();
    });
    
    it("Remove Booking From Area Additional Data", function () {

      dashboardService.removeAreaFromBooking(area1, "booking1")
              .then(function () {
                var savedAadditionalData1 = testUtils.getRefChild(additionalDataRef, 0);
        
                expect(savedAadditionalData1).toEqual({
                  "buildPlannedDays": {
                    "2018-12-12": 2
                  }
                });

              });
      scope.$apply();
    });
    
    
  });
  
  describe("Remove booking", function () {
    
    beforeEach(function () {

      var project1 = {
        "$id": "PROJ1",
        "name": "Project 1",
        "id": "001",
        "active": true,
        "deliverySchedule": {
          "unpublished": "PROJ1REV1",
          "revisions": {
            "PROJ1REV0": true,
            "PROJ1REV1": true
          },
          "published": "PROJ1REV0"
        },
        "datumType": "Education Student Accommodation"
      };

      var area1 = {
        $id: "AREA1",
        "floor": "GF",
        "phase": "P1",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 50,
        "estimatedPanels": 20,
        "actualArea": 95,
        "actualPanels": 15,
        "completedArea": 95,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-11-20",
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        },
        "delivered": true
      };

      var area2 = {
        $id: "AREA2",
        "floor": "GF",
        "phase": "P2",
        "type": "Ext",
        "project": "PROJ1",
        "estimatedArea": 100,
        "estimatedPanels": 20,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2018-11-20"
        },
        "timestamps": {
          "created": new Date("2018-11-20").getTime(),
          "modified": new Date("2018-11-20").getTime()
        },
        "delivered": true
      };

      var area3 = {
        $id: "AREA3",
        "floor": "GF",
        "phase": "P3",
        "type": "ExtH",
        "project": "PROJ1",
        "estimatedArea": 100,
        "estimatedPanels": 20,
        "actualArea": 95,
        "actualPanels": 15,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV0": "2018-12-11",
          "PROJ1REV1": "2018-12-11"
        },
        "timestamps": {
          "created": new Date("2018-12-11").getTime(),
          "modified": new Date("2018-12-11").getTime()
        }
      };

      var panel1 = {
        "area": "AREA1",
        "id": "001-DA-P1-GF-I-001",
        "project": "PROJ1",
        "type": "Ext",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        },
        "booking": "booking1"
      };

      var panel2 = {
        "area": "AREA1",
        "id": "001-DA-P1-GF-I-002",
        "project": "PROJ1",
        "type": "Ext",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:15:00+01:00").getTime()
        },
        "booking": "booking1"
      };

      var panel3 = {
        "area": "AREA2",
        "id": "001-DA-P2-GF-C-001",
        "project": "PROJ1",
        "type": "Roof",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        },
        "booking": "booking1"
      };

      var panel4 = {
        "area": "AREA3",
        "id": "001-DA-P3-GF-H-001",
        "project": "PROJ1",
        "type": "ExtH",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 3100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        }
      };

      var panel5 = {
        "area": "AREA3",
        "id": "001-DA-P3-GF-I-001",
        "project": "PROJ1",
        "type": "ExtH",
        "dimensions": {
          "area": 0.5,
          "width": 162,
          "height": 430,
          "length": 1100,
          "weight": 19
        },
        "qa": {
          "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
          "diagonalHeight": 700,
          "diagonalWidth": 900,
          "midHeight": 435,
          "midLength": 1102,
          "started": new Date("2018-11-20T10:30:00+01:00").getTime()
        },
        "timestamps": {
          "created": new Date("2018-11-20T12:13:00").getTime()
        }
      };

      projectsRef.set({
        "PROJ1": project1
      });

      areasRef.set({
        "AREA1": area1,
        "AREA2": area2,
        "AREA3": area3
      });

      panelsRef.set({
        "panel1": panel1,
        "panel2": panel2,
        "panel3": panel3,
        "panel4": panel4,
        "panel5": panel5
      });
      
      additionalDataRef.set({
        "AREA1": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        },
        "AREA2": {
          "bookings": {
            "booking1": true
          },
          "buildPlannedDays": {
            "2018-12-12": 2
          }
        }
      });
      
      bookingRef.set({
        "booking1": {
          "bookedTime": systemTimestamp,
          "areas": {
            "AREA1": true,
            "AREA2": true
          },
          "delivered": {"status": true, "timeDate": systemTimestamp, "user": "FL"}
        }
      });

    });

    it("panels delivered removed", function () {

      var areas = {
        "AREA1": true,
        "AREA2": true
      };

      dashboardService.removeBooking("booking1", areas)
              .then(function () {
                var savedPanel1 = testUtils.getRefChild(panelsRef, 0);
                expect(savedPanel1).toEqual({
                  "area": "AREA1",
                  "id": "001-DA-P1-GF-I-001",
                  "project": "PROJ1",
                  "type": "Ext",
                  "dimensions": {
                    "area": 0.5,
                    "width": 162,
                    "height": 430,
                    "length": 3100,
                    "weight": 19
                  },
                  "qa": {
                    "diagonalHeight": 700,
                    "diagonalWidth": 900,
                    "midHeight": 435,
                    "midLength": 1102,
                    "started": new Date("2018-11-20T10:30:00+01:00").getTime()
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20T12:13:00").getTime()
                  }
                });
                var savedPanel2 = testUtils.getRefChild(panelsRef, 1);
                expect(savedPanel2).toEqual({
                  "area": "AREA1",
                  "id": "001-DA-P1-GF-I-002",
                  "project": "PROJ1",
                  "type": "Ext",
                  "dimensions": {
                    "area": 0.5,
                    "width": 162,
                    "height": 430,
                    "length": 3100,
                    "weight": 19
                  },
                  "qa": {
                    "completed": new Date("2018-11-20T14:30:00+01:00").getTime(),
                    "diagonalHeight": 700,
                    "diagonalWidth": 900,
                    "midHeight": 435,
                    "midLength": 1102,
                    "started": new Date("2018-11-20T10:30:00+01:00").getTime()
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20T12:15:00+01:00").getTime()
                  }
                });
              });
      scope.$apply();
    });
    
    it("Remove Delivered From Area", function () {

      var areas = {
        "AREA1": true,
        "AREA2": true
      };

      dashboardService.removeBooking("booking1", areas)
              .then(function () {
                var savedArea1 = testUtils.getRefChild(areasRef, 0);
        
                expect(savedArea1).toEqual({
                  "floor": "GF",
                  "phase": "P1",
                  "type": "Ext",
                  "project": "PROJ1",
                  "estimatedArea": 50,
                  "estimatedPanels": 20,
                  "actualArea": 95,
                  "actualPanels": 15,
                  "completedArea": 95,
                  "completedPanels": 14,
                  "supplier": "Innovaré",
                  "revisions": {
                    "PROJ1REV0": "2018-11-20",
                    "PROJ1REV1": "2018-11-20"
                  },
                  "timestamps": {
                    "created": 1542672000000,
                    "modified": 1542672000000
                  }
                });

                var savedArea2 = testUtils.getRefChild(areasRef, 1);
                expect(savedArea2).toEqual({
                  "floor": "GF",
                  "phase": "P2",
                  "type": "Ext",
                  "project": "PROJ1",
                  "estimatedArea": 100,
                  "estimatedPanels": 20,
                  "supplier": "Innovaré",
                  "revisions": {
                    "PROJ1REV1": "2018-11-20"
                  },
                  "timestamps": {
                    "created": new Date("2018-11-20").getTime(),
                    "modified": new Date("2018-11-20").getTime()
                  }
                });
              });
      scope.$apply();
    });
    
    it("Remove Booking From Area Additional Data", function () {

      var areas = {
        "AREA1": true,
        "AREA2": true
      };

      dashboardService.removeBooking("booking1", areas)
              .then(function () {
                var savedAadditionalData1 = testUtils.getRefChild(additionalDataRef, 0);
        
                expect(savedAadditionalData1).toEqual({
                  "buildPlannedDays": {
                    "2018-12-12": 2
                  }
                });

                var savedAadditionalData2 = testUtils.getRefChild(additionalDataRef, 1);
        
                expect(savedAadditionalData2).toEqual({
                  "buildPlannedDays": {
                    "2018-12-12": 2
                  }
                });
              });
      scope.$apply();
    });
    
    it("Removes bookings", function () {

      var areas = {
        "AREA1": true,
        "AREA2": true
      };

      dashboardService.removeBooking("booking1", areas)
              .then(function () {
                var savedBooking = testUtils.getRefChild(bookingRef);
                expect(savedBooking).toEqual(undefined);
              });
      scope.$apply();
    });
    
  });
  
});