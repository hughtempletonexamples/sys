"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("Materialrequests Service", function () {
  var scope;
  var createdTimestamp;
  var systemTimestamp;


  var firebaseRoot;
  var materialrequestsRef;
  var mockSchema;

  var materialrequestsService;
  
  var request1 = {
    "dateRequired": "2018-07-23",
    "email": "Hugh.Templeton@innovaresystems.co.uk",
    "materialRequired": "chocolate buttons",
    "person": "Hugh",
    "projectId": "876",
    "projectName": "The Test 876"
  };
  
  var request2 = {
    "dateRequired": "2018-07-24",
    "email": "Hugh.Templeton@innovaresystems.co.uk",
    "materialRequired": "snickers bar",
    "person": "Hugh",
    "projectId": "333",
    "projectName": "The Test 333"
  };
  
  var request3 = {
    "dateRequired": "2018-07-25",
    "email": "Hugh.Templeton@innovaresystems.co.uk",
    "materialRequired": "mars bar",
    "person": "Hugh",
    "projectId": "359",
    "projectName": "The Test 359"
  };

  beforeEach(angular.mock.module("innView.materialrequests", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    materialrequestsRef = firebaseRoot.child("materialrequests");
    utils.addQuerySupport(materialrequestsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _materialrequestsService_) {
    scope = $rootScope;
    materialrequestsService = _materialrequestsService_;
  }));

  describe("retrieve all material requests", function () {

    it("returns all material requests", function () {

      materialrequestsRef.set({
        "request1":request1,
        "request2":request2,
        "request3":request3
      });

      var materialrequests = materialrequestsService.getMaterialRequests();
      scope.$apply();

      expect(materialrequests.length).toEqual(3);
      expect(materialrequests[0]).toEqual(jasmine.objectContaining({
        "person": "Hugh",
        "projectId": "876"
      }));
      expect(materialrequests[1]).toEqual(jasmine.objectContaining({
        "person": "Hugh",
        "projectId": "333"
      }));
      expect(materialrequests[2]).toEqual(jasmine.objectContaining({
        "person": "Hugh",
        "projectId": "359"
      }));

    });
  });

  describe("update material request", function () {

    it("add new material request", function (done) {

      materialrequestsRef.set({
        "request1": request1,
        "request2": request2,
        "request3": request3
      });

      var newRequest = {
        "dateRequired": "2018-07-26",
        "email": "Hugh.Templeton@innovaresystems.co.uk",
        "materialRequired": "maltesers",
        "person": "Hugh",
        "projectId": "445",
        "projectName": "The Test 445"
      };

      materialrequestsService.updateMaterialRequest(newRequest)
        .then(function () {
          utils.expectRefChildToContain(materialrequestsRef, 0, {projectId: "445", materialRequired: "maltesers"});
          done();
        });

      scope.$apply();

    });


    it("updates existing material request", function (done) {
      
      request1["$id"] = "request1";
      request1["projectId"] = "876";
      request1["person"] = "Hugh Templeton";

      materialrequestsRef.set({
        "request1": request1,
        "request2": request2,
        "request3": request3
      });

      materialrequestsService.updateMaterialRequest(request1)
        .then(function () {
          utils.expectRefChildToContain(materialrequestsRef, 0, {projectId: "876", person: "Hugh Templeton"});
          done();
        });

      scope.$apply();

    });

  });
  
  describe("getPanelById", function () {

    beforeEach(function () {
      materialrequestsRef.set({
        "request1": request1,
        "request2": request2,
        "request3": request3
      });
    });

    it("returns the specified panel", function (done) {
      materialrequestsService.getMaterialRequestByProjectId("876")
              .$loaded()
              .then(function (panels) {
                expect(panels[0]).toEqual(jasmine.objectContaining(request1));
                done();
              });

      scope.$apply();
    });
  });


});