"use strict";

var utils = require("../testUtils");

describe("productionperformanceService", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;
  var productionperformanceService;
  var projectsRef;
  var areasRef;
  var panelsRef;
  var referenceDataRef;
  var mockProjectsService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;
  
  var project1 = {
    "name": "Project 1",
    "id": "001",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV0"
    },
    "datumType": "Education Student Accommodation"
  };

  var area1 = {
    "floor": "GF",
    "phase": "P1",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV0": "2016-11-20",
      "PROJ1REV1": "2016-11-20"
    },
    "timestamps": {
      "created": new Date("2016-11-20").getTime(),
      "modified": new Date("2016-11-20").getTime()
    }
  };

  var area2 = {
    "floor": "GF",
    "phase": "P2",
    "type": "Ext",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-11-20"
    },
    "timestamps": {
      "created": new Date("2016-11-20").getTime(),
      "modified": new Date("2016-11-20").getTime()
    }
  };
  
  var area3 = {
    "floor": "GF",
    "phase": "P3",
    "type": "ExtH",
    "project": "PROJ1",
    "revisions": {
      "PROJ1REV1": "2016-11-20"
    },
    "timestamps": {
      "created": new Date("2016-11-20").getTime(),
      "modified": new Date("2016-11-20").getTime()
    }
  };

  var panel1 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-001",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  var panel2 = {
    "area": "AREA1",
    "id": "001-DA-P1-GF-I-002",
    "project": "PROJ1",
    "type": "Ext",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:15:00+01:00").getTime()
    }
  };

  var panel3 = {
    "area": "AREA2",
    "id": "001-DA-P2-GF-C-001",
    "project": "PROJ1",
    "type": "Roof",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };
  
  var panel4 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-H-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 3100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };
  
  var panel5 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-I-001",
    "project": "PROJ1",
    "type": "ExtH",
    "dimensions": {
      "area": 0.5,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };
  var panel6 = {
    "area": "AREA3",
    "id": "001-DA-P3-GF-F-001",
    "project": "PROJ1",
    "type": "ExtF",
    "dimensions": {
      "area": 3,
      "width": 162,
      "height": 430,
      "length": 1100,
      "weight": 19
    },
    "qa": {
      "completed": new Date("2016-11-20T14:30:00+01:00").getTime(),
      "diagonalHeight": 700,
      "diagonalWidth": 900,
      "midHeight": 435,
      "midLength": 1102,
      "started": new Date("2016-11-20T10:30:00+01:00").getTime()
    },
    "timestamps": {
      "created": new Date("2016-11-20T12:13:00").getTime()
    }
  };

  beforeEach(angular.mock.module("innView.productionperformance", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));
  
  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.productionperformance"));

  beforeEach(function () {
    jasmine.clock().install();
    jasmine.clock().mockDate(new Date("2016-11-21"));
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });
  
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");

    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(referenceDataRef);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes", "getProductionLines"]);

  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockAreasService.getProjectAreas.and.callFake(function () {
      return $firebaseArray(areasRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      return $firebaseArray(panelsRef
                            .orderByChild("area")
                            .equalTo(areaRef));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductionLines.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productionLines"));
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  
  
  beforeEach(function () {
    projectsRef.set({
      "PROJ1": project1
    });

    areasRef.set({
      "AREA1": area1,
      "AREA2": area2,
      "AREA3": area3
    });

    panelsRef.set({
      "panel1": panel1,
      "panel2": panel2,
      "panel3": panel3,
      "panel4": panel4,
      "panel5": panel5,
      "panel6": panel6
    });

    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "ExtF": { productionLine: "IFAST" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" }
      }
    );

    referenceDataRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 40 },
        "HSIP": { defaultCapacity: 20 },
        "IFAST": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 60 },
        "CASS": { defaultCapacity: 6 }
      }
    );
    
  });
  
  beforeEach(inject(function ($rootScope, $injector) {
    scope = $rootScope;
    productionperformanceService = $injector.get("productionperformanceService");
  }));
  
  describe("processPanelsForPerformance", function () {

    it("returns performance data for set day", function () {

      var data = new productionperformanceService(panelsRef);
      
      var selectDate = "2016-11-20";
      
      data.setDate(selectDate);
      
      scope.$apply();
     
      expect(data._calculatedTotal.completed.SIP).toEqual(1);
      expect(data._calculatedTotal.started.SIP).toEqual(1);
      expect(data._calculatedTotal.area.SIP).toEqual(0.5);
      
      expect(data._calculatedTotal.completed.HSIP).toEqual(2);
      expect(data._calculatedTotal.started.HSIP).toEqual(0);
      expect(data._calculatedTotal.area.HSIP).toEqual(1);
      
      expect(data._calculatedTotal.completed.IFAST).toEqual(1);
      expect(data._calculatedTotal.started.IFAST).toEqual(0);
      expect(data._calculatedTotal.area.IFAST).toEqual(3);
      
      expect(data._calculatedTotal.completed.TF).toEqual(0);
      expect(data._calculatedTotal.started.TF).toEqual(0);
      expect(data._calculatedTotal.area.TF).toEqual(0);
      
      expect(data._calculatedTotal.completed.CASS).toEqual(1);
      expect(data._calculatedTotal.started.CASS).toEqual(0);
      expect(data._calculatedTotal.area.CASS).toEqual(0.5);


    });
  });
  
});
