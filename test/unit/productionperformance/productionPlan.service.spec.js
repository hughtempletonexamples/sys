"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var testUtils = require("../testUtils");

describe("productionPlan service", function () {

  var productionPlanService;

  var mockPlannerService;
  var mockAreasService;
  var mockProjectsService;
  var mockReferenceDataService;

  var areaPlansRef;
  var areasRef;
  var projectsRef;
  var referenceDataRef;

  var systemTimestamp;

  var scope;

  // Angular mock our qa module
  beforeEach(angular.mock.module("innView.productionperformance", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    areasRef = firebaseRoot.child("areas");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    projectsRef = firebaseRoot.child("projects");
    referenceDataRef = firebaseRoot.child("srd");

    testUtils.addQuerySupport(areaPlansRef);
    testUtils.addQuerySupport(areasRef);
    testUtils.addQuerySupport(projectsRef);
    testUtils.addQuerySupport(referenceDataRef);

    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProductionPlansByPlannedStart"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectArea"]);
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getSuppliers","getProductTypes"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockProjectsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    testUtils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseObject, $firebaseArray) {
    mockAreasService.getProjectArea.and.callFake(function (areaKey) {
      return $firebaseObject(areasRef.child(areaKey));
    });

    mockPlannerService.getProductionPlansByPlannedStart.and.callFake(function () {
      return $firebaseArray(areaPlansRef);
    });

    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });

    mockReferenceDataService.getSuppliers.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("suppliers"));
    });
    
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(inject(function ($rootScope, _productionPlanService_) {
    scope = $rootScope;
    productionPlanService = _productionPlanService_;
  }));

  beforeEach(function () {
    var now = new Date("2015-08-08");
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });


  describe("getIncompleteAreas", function () {

    beforeEach(function () {
      
      referenceDataRef.child("productTypes").set(
        {
          "Ext": {productionLine: "SIP"},
          "ExtH": {productionLine: "HSIP"},
          "Int": {productionLine: "TF"},
          "Roof": {productionLine: "CASS"},
          "P&B": {},
          "Anc": {}
        }
      );

      referenceDataRef.child("suppliers").set(
        {
          "Innovaré": {thirdParty: false},
          "Other": {thirdParty: true}
        }
      );
     
      projectsRef.set(
        {
          "PROJ1": {
            "active": true,
            "name": "Project 1",
            "id": "001",
            "deliverySchedule": {
              "unpublished": "PROJ2REV1",
              "revisions": {
                "PROJ2REV0": true,
                "PROJ2REV1": true
              },
              "published": "PROJ2REV0"
            },
            "datumType": "Education Student Accommodation"
          }
        }
      );
    
      areaPlansRef.set(
        {
          "AREA1": {
            "plannedFinish" : "2016-05-24",
            "plannedStart" : "2016-05-24",
            "project" : "PROJ1",
            "riskDate" : "2016-05-14"
          },
          "AREA2": {
            "plannedFinish" : "2016-05-27",
            "plannedStart" : "2016-05-27",
            "project" : "PROJ1",
            "riskDate" : "2016-05-20"
          },
          "AREA3": {
            "plannedFinish" : "2016-05-17",
            "plannedStart" : "2016-05-16",
            "project" : "PROJ1",
            "riskDate" : "2016-05-10"
          }
        }
      );

      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "Ext",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          },
          "AREA2": {
            phase: "1",
            floor: "2",
            type: "Ext",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 10
          },
          "AREA3": {
            phase: "1",
            floor: "2",
            type: "Roof",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 8
          }
        }
      );
    });

    it("returns all areas with incomplete panels by production planned start date", function () {
      productionPlanService.getGreatestRisks()
        .then(function (incompleteAreas) {
          expect(incompleteAreas).toEqual({ SIP: -7, HSIP: 0, TF: 0, CASS: -5 });
        });
      scope.$apply();
    });
  });
});