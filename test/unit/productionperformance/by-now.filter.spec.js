"use strict";

xdescribe("buynow filter before", function () {
  var byNowFilter;
  var systemTimestamp;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.productionperformance"));
  
  beforeEach(function () {
    var now = new Date(Date.UTC(2018, 11, 24, 15, 30, 0, 0));
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });
  
  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(inject(function (_byNowFilter_) {
    byNowFilter = _byNowFilter_;
  }));

  it("should be stateless", function () {
    expect(byNowFilter.$stateful).not.toEqual(true);
  });

  it("should calculate how many panels should be made by 15:30", function () {
    
    //use this manually it is designed for realtime by getting current date
    expect(byNowFilter(systemTimestamp,30,40)).toBe("38");
  });
  

});

xdescribe("buynow filter after", function () {
  var byNowFilter;
  var systemTimestamp;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.productionperformance"));
  
  beforeEach(function () {
    var now = new Date(Date.UTC(2018, 11, 24, 16, 30, 0, 0));
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });
  
  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(inject(function (_byNowFilter_) {
    byNowFilter = _byNowFilter_;
  }));

  it("should be stateless", function () {
    expect(byNowFilter.$stateful).not.toEqual(true);
  });

  it("should calculate how many panels should be made by 16:30", function () {
    
    //use this manually it is designed for realtime by getting current date
    expect(byNowFilter(systemTimestamp,30,40)).toBe("38");
  });
  

});
