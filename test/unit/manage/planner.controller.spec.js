"use strict";

var utils = require("../testUtils");

describe("manage", function () {

  var MockFirebase = require("firebase-mock").MockFirebase;
  var $controller;
  var $rootScope;

  var mockPlannerService;

  beforeEach(angular.mock.module("innView.manage", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    mockPlannerService = jasmine.createSpyObj("plannerService", ["getForwardPlan", "setCapacity"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
    });

    var firebaseRoot = new MockFirebase("Mock://");
    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function (_$controller_, _$rootScope_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
  }));

  function createController(locals) {
    return $controller("ForwardPlannerController", angular.extend(
      {
        $scope: $rootScope.$new()
      },
      locals));
  }

  describe("planner controller", function () {

    beforeEach(inject(function ($location) {
      $location.url("/manage/plan");
    }));

    it("defaults to null for plan start date", function () {
      var controller = createController();
      var mondayOfCurrentWeek = function () {
        var date = new Date();
        date.setDate(date.getDate() - (date.getDay() || 7) + 1);
        return date.toISOString().substring(0, 10);
      };
      

      expect(mockPlannerService.getForwardPlan.calls.mostRecent().args).toEqual([null, {}]);
    });

    it("uses parameter for plan start date", function () {
      var controller = createController({$routeParams: {start: "2016-04-04"}});

      expect(mockPlannerService.getForwardPlan.calls.mostRecent().args).toEqual(["2016-04-04", {}]);
    });

    it("updates location with start date parameter when date selected", inject(function ($controller, $location) {
      var controller = createController();

      controller.loadPlan("2016-06-06");

      expect($location.url()).toEqual("/manage/plan?start=2016-06-06");
      expect(mockPlannerService.getForwardPlan.calls.mostRecent().args).toEqual(["2016-06-06", {}]);

      controller.loadPlan(null);
      
      var mondayOfCurrentWeek = function () {
        var date = new Date();
        date.setDate(date.getDate() - (date.getDay() || 7) + 1);
        return date.toISOString().substring(0, 10);
      };

      expect($location.url()).toEqual("/manage/plan");
      expect(mockPlannerService.getForwardPlan.calls.mostRecent().args).toEqual([null, {}]);
    }));

    it("start date is monday of week of selected date", inject(function ($controller, $location) {
      var controller = createController();

      var today = new Date().toISOString().substring(0, 10);
      var mondayOfCurrentWeek = function () {
        var date = new Date();
        date.setDate(date.getDate() - (date.getDay() || 7) + 1);
        return date.toISOString().substring(0, 10);
      };

      controller.loadPlan(today);

      expect($location.url()).toEqual("/manage/plan?start=" + mondayOfCurrentWeek());
    }));

    it("loads plan", function () {
      mockPlannerService.getForwardPlan.and.returnValue(
        {
          detailed: "Information"
        }
      );

      var controller = createController();

      expect(controller.plan).toEqual({detailed: "Information"});

      expect(mockPlannerService.getForwardPlan.calls.count()).toEqual(1);
    });

    it("calculates date for weeks", function () {
      mockPlannerService.getForwardPlan.and.returnValue(
        {
          viewStart: "2016-03-28"
        }
      );

      var controller = createController();

      expect(controller.weekStart(0)).toEqual("2016-03-28");
      expect(controller.weekStart(1)).toEqual("2016-04-04");
      expect(controller.weekStart(2)).toEqual("2016-04-11");

      expect(controller.weekFinish(0)).toEqual("2016-04-03");
      expect(controller.weekFinish(1)).toEqual("2016-04-10");
      expect(controller.weekFinish(2)).toEqual("2016-04-17");
    });

    it("updates capacity", function () {
      var controller = createController();

      controller.plan = {
        viewStart: "2016-08-01"
      };

      controller.updateCapacity("ABC", 3, 21);

      expect(mockPlannerService.setCapacity.calls.count()).toEqual(1);
      expect(mockPlannerService.setCapacity.calls.mostRecent().args).toEqual(["ABC", "2016-08-04", 21]);
    });

    it("destroys previous plan when loading next", function () {
      var destroyed = [];

      var plan = {
        $destroy: function () {
          destroyed.push(this);
        }
      };

      mockPlannerService.getForwardPlan.and.returnValue(plan);

      var controller = createController();

      controller.loadPlan("2016-08-16");

      expect(destroyed.length).toEqual(1);
      expect(destroyed[0]).toEqual(plan);
    });

    it("destroys plan when scope destroyed", function () {
      var destroyed = [];

      var plan = {
        $destroy: function () {
          destroyed.push(this);
        }
      };

      mockPlannerService.getForwardPlan.and.returnValue(plan);

      var $scope = $rootScope.$new();
      var controller = createController({$scope: $scope});

      $scope.$destroy();

      expect(destroyed.length).toEqual(1);
      expect(destroyed[0]).toEqual(plan);
    });

  });

  describe("controller helpers", function () {

    var areaWithoutEstimatedPanelCount = {
    };

    var areaWithEstimatedPanelCount = {
      estimatedPanels: 17
    };
    
    var areaWithEstimatedPanelAndUserGuessCount = {
      estimatedPanels: 17,
      numberOfPanels: 6
    };
    
    var areaWithEstimatedPanelAndUserGuessCountNoMatch = {
      actualPanels: 6,
      numberOfPanels: 17
    };
    
    var areaWithEstimatedPanelAndUserGuessCountMatch = {
      actualPanels: 17,
      numberOfPanels: 17
    };

    var areaWithActualPanelCount = {
      estimatedPanels: 17,
      actualPanels: 20
    };

    var areaWithCompletedPanelCount = {
      estimatedPanels: 17,
      actualPanels: 24,
      completedPanels: 11
    };

    it("returns is estimated", function () {
      var controller = createController();

      expect(controller.panelsCount(areaWithoutEstimatedPanelCount).type)
        .toEqual("noData");

      expect(controller.panelsCount(areaWithEstimatedPanelCount).type)
        .toEqual("notfinal");
      
      expect(controller.panelsCount(areaWithEstimatedPanelAndUserGuessCount).type)
        .toEqual("userGuess");

      expect(controller.panelsCount(areaWithEstimatedPanelAndUserGuessCountNoMatch).type)
        .toEqual("noMatch");

      expect(controller.panelsCount(areaWithEstimatedPanelAndUserGuessCountMatch).type)
        .toEqual("actual");

      expect(controller.panelsCount(areaWithActualPanelCount).type)
        .toEqual("actual");
    });

    it("returns panel quantity", function () {
      var controller = createController();

      expect(controller.panelsCount(areaWithoutEstimatedPanelCount).count)
        .toEqual(0);

      expect(controller.panelsCount(areaWithEstimatedPanelCount).count)
        .toEqual(areaWithEstimatedPanelCount.estimatedPanels);

      expect(controller.panelsCount(areaWithActualPanelCount).count)
        .toEqual(areaWithActualPanelCount.actualPanels);
    });

    it("returns panels completed count", function () {
      var controller = createController();

      expect(controller.completedPanelsCount(areaWithoutEstimatedPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsCount(areaWithEstimatedPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsCount(areaWithActualPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsCount(areaWithCompletedPanelCount))
        .toEqual(areaWithCompletedPanelCount.completedPanels);

    });

    it("returns panels completed percent", function () {
      var controller = createController();

      expect(controller.completedPanelsPercent(areaWithoutEstimatedPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsPercent(areaWithEstimatedPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsPercent(areaWithActualPanelCount))
        .toEqual(0);

      expect(controller.completedPanelsPercent(areaWithCompletedPanelCount))
        .toEqual(45);

    });

    it("returns risk days", function () {
      var area;

      var controller = createController();

      area = {
        _productionEndDate: "2016-08-01",
        _riskDate: "2016-08-01"
      };
      expect(controller.atRisk(area)).toEqual(false);
      expect(controller.riskDays(area)).toEqual(0);

      area = {
        _productionEndDate: "2016-08-01",
        _riskDate: "2016-08-08"
      };
      expect(controller.atRisk(area)).toEqual(false);
      expect(controller.riskDays(area)).toEqual(5);

      area = {
        _productionEndDate: "2016-08-08",
        _riskDate: "2016-08-01"
      };
      expect(controller.atRisk(area)).toEqual(true);
      expect(controller.riskDays(area)).toEqual(-5);

      area = {
        _productionEndDate: null
      };
      expect(controller.atRisk(area)).toEqual(true);
      expect(controller.riskDays(area)).toEqual(undefined);

      area = {
        _productionEndDate: "2016-03-25",
        _riskDate: "2016-03-28"
      };
      expect(controller.atRisk(area)).toEqual(false);
      expect(controller.riskDays(area)).toEqual(1, "DST begin");

      area = {
        _productionEndDate: "2016-10-28",
        _riskDate: "2016-10-31"
      };
      expect(controller.atRisk(area)).toEqual(false);
      expect(controller.riskDays(area)).toEqual(1, "DST end");

    });

  });
});
