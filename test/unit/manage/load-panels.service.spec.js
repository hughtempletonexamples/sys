"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("load panels service", function () {
  var scope;

  var mockXLSX;
  var worksheet;

  var firebaseRoot;
  var projectsRef;
  var panelsRef;
  var statsRef;
  var componentsRef;
  var mockSchema;
  var mockAreasService;
  var mockPanelsService;
  var mockComponentsService;
  var mockFileReader;
  var referenceDataRef;
  var referenceDataService;

  var loadPanelsService;

  beforeEach(angular.mock.module("innView.manage", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {

    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["getPanelById"]);
    mockComponentsService = jasmine.createSpyObj("componentsService", ["getComponentsForProject"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    componentsRef = firebaseRoot.child("components");
    statsRef = firebaseRoot.child("stats");
    referenceDataRef = firebaseRoot.child("srd");
    utils.addQuerySupport(componentsRef);
    referenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes"]);
    utils.addQuerySupport(panelsRef);
    
    mockXLSX = {
      read: jasmine.createSpy("read"),
      utils: {
        decode_range: decode_range,
        encode_cell: encode_cell
      }
    };

    worksheet = {
      Sheets: []
    };

    mockXLSX.read.and.callFake(function () {
      return worksheet;
    });

    angular.mock.module(function ($provide) {
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("componentsService", function () {
        return mockComponentsService;
      });
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.constant("FileReader", function () {
        return mockFileReader;
      });
      $provide.constant("XLSX", mockXLSX);
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });
  
  beforeEach(inject(function ($firebaseArray) {
    referenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));  

  beforeEach(inject(function (schema) {
    schema.getRoot().child("srd").set({
      "phases": {
        "P1": {},
        "P2": {}
      },
      "floors": {
        "GF": {},
        "1F": {}
      },
      "panelTypes": {
        "F": {},
        "H": {},
        "L": {},
        "C": {},
        "I": {}
      },
      "productTypes": {
        "Ext": { "name": "External", "productionLine" : "SIP" },
        "ExtH": { "name": "Hybrid SIP", "productionLine" : "HSIP" },
        "ExtF": { "name": "Innovare Fire Acoustic Structual Thermal", "productionLine" : "IFAST" },
        "Int": { "name": "Internal", "productionLine" : "TF" },
        "Roof": { "name": "Roof", "productionLine" : "CASS" }
      }
    });
  }));

  beforeEach(function () {
    projectsRef.set(
      {
        "PROJ1": {
          "id": "214"
        },
        "PROJ2": {
          "id": "314"
        }
      }
    );
  });
  
  beforeEach(function () {
    componentsRef.set({
      "component1": {
        "project": "PROJ1",
        "areas": {
          "AREA1": true,
          "AREA2": true
        },
        "type": "Walls"
      },
      "component2": {
        "project": "PROJ2",
        "areas": {
          "AREA3": true
        },
        "type": "Roof"
      }
    });
  });

  beforeEach(inject(function ($rootScope, _loadPanelsService_) {
    scope = $rootScope;
    loadPanelsService = _loadPanelsService_;
  }));

  describe("Incriments Design Value", function () {
    var panelsArray = {
      "PANEL1": {
        id: "214-DA-P2-GF-I-100",
        project: "PROJ1",
        type: "Ext",
        area: "AREA1",
        dimensions: {
          length: 3164,
          height: 430,
          width: 162,
          area: 2.5,
          weight: 19
        },
        additionalInfo: {
          framingStyle: "SIP 213",
          studSize: "38x184",
          sheathing: "OSB 5980",
          components: 6,
          nailing: "150x150",
          spandrel: "No",
          doors: null,
          windows: null,
          pockets: null,
          qty: 1
        }
      },
      "PANEL2": {
        id: "214-DA-P2-GF-L-111",
        project: "PROJ1",
        type: "Int",
        area: "AREA2",
        dimensions: {
          length: 3700,
          height: 270,
          width: 162,
          area: 1.7,
          weight: 30
        },
        additionalInfo: {
          framingStyle: "SIP 213",
          studSize: "38x184",
          sheathing: "OSB 5980",
          components: 6,
          nailing: "150x150",
          spandrel: "No",
          doors: null,
          windows: null,
          pockets: null,
          qty: 1
        }
      }



    };
    it("it incriments design incrimented value for date", function (done) {
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-034,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-L-036,Internal,SIP 213,3700,270,0.7,162,38x184,OSB 5980,6,150x150,No,0,0,0,30kg,1",
        "314-DA-P2-GF-C-037,Roof,SIP 213,2666,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-H-038,External,HSIP 213,2640,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-I-039,External,HSIP 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-F-040,External,IFAST 213,2640,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-F-041,External,IFAST 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1"
      ]);

      loadPanelsService.incrimentDesignCount(panelsArray)
              .then(function (newData){
                expect(newData).toEqual({"designedM2":4,"designedCount":5});
              });
              

      
      
      done();
    });
  });

  describe("loadPanelSchedule with XLSX", function () {
    
    beforeEach(inject(function ($firebaseArray) {

      var projectComponents = {
        "PROJ1": {
          "component1": {
            "project": "PROJ1",
            "areas": {
              "AREA1": true,
              "AREA2": true,
              "AREA4": true
            },
            "type": "Walls"
          },
          "component2": {
            "project": "PROJ1",
            "areas": {
              "AREA5": true
            },
            "type": "IFAST"
          }
        },
        "PROJ2": {
          "component2": {
            "project": "PROJ2",
            "areas": {
              "AREA3": true
            },
            "type": "Roof"
          }
        }
      };

      var projectAreas = {
        "PROJ1": {
          "AREA1": {
            "phase": "P2",
            "floor": "GF",
            "type": "Ext"
          },
          "AREA2": {
            "phase": "P2",
            "floor": "GF",
            "type": "Int"
          },
          "AREA4": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtH"
          },
          "AREA5": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtF"
          },
          "AREA6": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtF"
          }
        },
        "PROJ2": {
          "AREA3": {
            "phase": "P2",
            "floor": "GF",
            "type": "Roof"
          }
        }
      };

      var areaPanels = {
        "214-DA-P2-GF-I-100": {
          "PANEL1": {
            id: "214-DA-P2-GF-I-100",
            project: "PROJ1",
            type: "Ext",
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-L-111": {
          "PANEL2": {
            id: "214-DA-P2-GF-L-111",
            project: "PROJ1",
            type: "Int",
            area: "AREA2",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "314-DA-P2-GF-C-112": {
          "PANEL3": {
            id: "314-DA-P2-GF-C-112",
            project: "PROJ2",
            type: "Roof",
            area: "AREA3",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-H-113": {
          "PANEL4": {
            id: "214-DA-P2-GF-H-113",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA4",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-I-114": {
          "PANEL5": {
            id: "214-DA-P2-GF-I-114",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA4",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-F-115": {
          "PANEL6": {
            id: "214-DA-P2-GF-F-115",
            project: "PROJ1",
            type: "ExtF",
            area: "AREA6",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-F-116": {
          "PANEL7": {
            id: "214-DA-P2-GF-F-116",
            project: "PROJ1",
            type: "ExtF",
            area: "AREA6",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        }
      };

      mockComponentsService.getComponentsForProject.and.callFake(
        function (projectKey) {
          var componentsRef = new MockFirebase("MockQuery://components");
          componentsRef.autoFlush(true);
          componentsRef.set(projectComponents[projectKey]);

          return $firebaseArray(componentsRef);
        });

      mockAreasService.getProjectAreas.and.callFake(
        function (projectKey) {
          var areasRef = new MockFirebase("MockQuery://areas");
          areasRef.autoFlush(true);
          areasRef.set(projectAreas[projectKey]);

          return $firebaseArray(areasRef);
        });

      mockPanelsService.getPanelById.and.callFake(
        function (panelId) {
          var panelsRef = new MockFirebase("MockQuery://panels");
          panelsRef.autoFlush(true);
          panelsRef.set(areaPanels[panelId]);

          return $firebaseArray(panelsRef);
        });

    }));
    
    beforeEach(function () {
      mockFileReader = {
        onload: undefined,
        result: undefined,
        readAsBinaryString: jasmine.createSpy("readAsBinaryString")
      };

      mockFileReader.readAsBinaryString.and.callFake(function (blob) {
        mockFileReader.onload({target: {result: "DATA"}});
      });

    });

    it("exception reading XLXS reported as rejected promise", function (done) {
      mockXLSX.read.and.callFake(function () {
        throw new Error("Unsupported file");
      });

      loadPanelsService.loadPanelSchedule(new Blob())
        .catch(function (error) {
          expect(error).toEqual("Unsupported file");

          done();
        });

      scope.$apply();
    });

    it("validates panel reference format with XLSX", function (done) {
      addSheet([
        "Panel Ref",
        "214-P2-GF-I-034",
        "ABC-DA-P2-GF-I-034",
        "214-DA-P2-GF-I-XYZ"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-P2-GF-I-034 Invalid panel reference",
            "ABC-DA-P2-GF-I-034 Invalid panel reference",
            "214-DA-P2-GF-I-XYZ Invalid panel reference"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown project in panel reference with XLSX", function (done) {
      addSheet([
        "Panel Ref",
        "241-DA-P2-GF-I-034"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "241-DA-P2-GF-I-034 Unknown project '241'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown phase/floor/panel type in panel reference with XLSX", function (done) {
      addSheet([
        "Panel Ref",
        "214-DA-XX-GF-I-034",
        "214-DA-P2-YY-I-034",
        "214-DA-P2-GF-Z-034"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-XX-GF-I-034 Unknown phase 'XX'",
            "214-DA-P2-YY-I-034 Unknown floor 'YY'",
            "214-DA-P2-GF-Z-034 Unknown panel type 'Z'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown type with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type",
        "214-DA-P2-GF-I-034,Floor"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-P2-GF-I-034 Unknown type 'Floor'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown area with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type",
        "214-DA-P2-1F-I-034,External"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-P2-1F-I-034 Unknown area 'P2-1F-Ext'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports missing dimensions with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type,Length,Height,Area,Depth,Weight",
        "214-DA-P2-GF-I-001,External,0,430,0.5,162,19kg",
        "214-DA-P2-GF-I-002,External,3164,0,0.5,162,19kg",
        "214-DA-P2-GF-I-003,External,3164,430,0,162,19kg",
        "214-DA-P2-GF-I-004,External,3164,430,0.5,0,19kg",
        "214-DA-P2-GF-I-005,External,3164,430,0.5,162,0kg"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.errors.length).toEqual(5);
          expect(result.errors).toEqual([
            "214-DA-P2-GF-I-001 Missing dimension information",
            "214-DA-P2-GF-I-002 Missing dimension information",
            "214-DA-P2-GF-I-003 Missing dimension information",
            "214-DA-P2-GF-I-004 Missing dimension information",
            "214-DA-P2-GF-I-005 Missing dimension information"
          ]);

          done();
        });

      scope.$apply();
    });

    it("loads panel details from sheet with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-034,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-L-036,Internal,SIP 213,3700,270,0.7,162,38x184,OSB 5980,6,150x150,No,0,0,0,30kg,1",
        "314-DA-P2-GF-C-037,Roof,SIP 213,2666,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-H-038,External,HSIP 213,2640,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-I-039,External,HSIP 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-F-040,External,IFAST 213,2640,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1",
        "214-DA-P2-GF-F-041,External,IFAST 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(7);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-034",
            project: "PROJ1",
            area: "AREA1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({
            id: "214-DA-P2-GF-L-036",
            project: "PROJ1",
            area: "AREA2",
            type: "Int",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[2]).toEqual({
            id: "214-DA-P2-GF-H-038",
            project: "PROJ1",
            area: "AREA4",
            type: "ExtH",
            dimensions: {
              length: 2640,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[3]).toEqual({
            id: "214-DA-P2-GF-I-039",
            project: "PROJ1",
            area: "AREA1",
            type: "Ext",
            dimensions: {
              length: 2,
              height: 3,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[4]).toEqual({ 
            id: "214-DA-P2-GF-F-040", 
            project: "PROJ1", 
            type: "ExtF", 
            dimensions: { 
              length: 2640, 
              height: 3400, 
              width: 207, 
              area: 9, 
              weight: 435 
            }, 
            additionalInfo:{ 
              framingStyle: "IFAST 213", 
              studSize: "38x184", 
              sheathing: "OSB 5980", 
              components: 6, 
              nailing: "150x150", 
              spandrel: "No", 
              doors: null, 
              windows: null, 
              pockets: null, 
              qty: 1 
            }
          });
          
          expect(result.errors).toEqual([]);

          done();
        });
        

      scope.$apply();
    });
    
    it("loads panel job sheet details from sheet with XLSX", function (done) {
      addSheet([
        "Panel Ref,Jobsheet No.",
        "214-DA-P2-GF-I-100,JS-214-P2-GF-I-020",
        "214-DA-P2-GF-L-111,JS-214-P2-GF-L-020",
        "314-DA-P2-GF-C-112,JS-314-P2-GF-C-020",
        "214-DA-P2-GF-H-113,JS-214-P2-GF-H-020",
        "214-DA-P2-GF-I-114,JS-214-P2-GF-I-020",
        "214-DA-P2-GF-F-115,JS-214-P2-GF-F-020",
        "214-DA-P2-GF-F-116,JS-214-P2-GF-F-020"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(7);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-100",
            project: "PROJ1",
            type: "Ext",
            jobSheet: 20,
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({
            id: "214-DA-P2-GF-L-111",
            project: "PROJ1",
            type: "Int",
            jobSheet: 20,
            area: "AREA2",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[2]).toEqual({
            id: "214-DA-P2-GF-H-113",
            project: "PROJ1",
            type: "ExtH",
            jobSheet: 20,
            area: "AREA4",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[3]).toEqual({
            id: "214-DA-P2-GF-I-114",
            project: "PROJ1",
            type: "ExtH",
            jobSheet: 20,
            area: "AREA4",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[4]).toEqual({
            "id": "214-DA-P2-GF-F-115", 
            "project": "PROJ1", 
            "type": "ExtF", 
            "jobSheet": 20, 
            dimensions: { 
              "length": 22, 
              "height": 20, 
              "width": 207, 
              "area": 9, 
              "weight": 435 
            }, 
            additionalInfo:{ 
              "framingStyle": "SIP 213", 
              "studSize": "38x184", 
              "sheathing": "OSB 5980", 
              "components": 6, 
              "nailing": "150x150", 
              "spandrel": "No", 
              "doors": null, 
              "windows": null, 
              "pockets": null, 
              "qty": 1 
            }
          });
          expect(result.panels[5]).toEqual({
            "id": "214-DA-P2-GF-F-116", 
            "project": "PROJ1", 
            "type": "ExtF", 
            "jobSheet": 20, 
            dimensions: { 
              "length": 22, 
              "height": 20, 
              "width": 207, 
              "area": 9, 
              "weight": 435 
            }, 
            additionalInfo:{ 
              "framingStyle": "SIP 213", 
              "studSize": "38x184", 
              "sheathing": "OSB 5980", 
              "components": 6, 
              "nailing": "150x150", 
              "spandrel": "No", 
              "doors": null, 
              "windows": null, 
              "pockets": null, 
              "qty": 1 
            }
          });
          expect(result.panels[6]).toEqual({
            id: "314-DA-P2-GF-C-112",
            project: "PROJ2",
            type: "Roof",
            jobSheet: 20,
            area: "AREA3",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.errors).toEqual([]);

          done();
        });

      scope.$apply();
    });

    it("skips header rows with XLSX", function (done) {
      addSheet([
        "Job Reference,08621B",
        "House Type",
        "Description",
        "Cutting Set,206mm iSIP",
        "",
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-012,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(1);
          expect(result.panels[0]).toEqual(jasmine.objectContaining({id: "214-DA-P2-GF-I-012"}));

          done();
        });

      scope.$apply();
    });

    it("loads panel details from multiple sheets with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-034,External,SIP,1000,1000,1000,1000,38x184,OSB 5980,1,150x150,No,1,1,1,19kg,1"
      ]);
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "314-DA-P2-GF-C-001,Roof,SIP,1000,1000,1000,1000,38x184,OSB 5980,1,150x150,No,1,1,1,19kg,1"
      ]);
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-H-022,External,SIP,1000,1000,1000,1000,38x184,OSB 5980,1,150x150,No,1,1,1,19kg,1"
      ]);
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-F-041,External,IFAST 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(4);
          expect(result.panels[0]).toEqual(jasmine.objectContaining({id: "214-DA-P2-GF-I-034"}));
          expect(result.panels[1]).toEqual(jasmine.objectContaining({id: "214-DA-P2-GF-H-022"}));
          expect(result.panels[3]).toEqual(jasmine.objectContaining({id: "314-DA-P2-GF-C-001"}));
          expect(result.panels[2]).toEqual(jasmine.objectContaining({id: "214-DA-P2-GF-F-041"}));
          done();
        });

      scope.$apply();
    });

    it("loads panel details using column headings with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty,Fake Column",
        "214-DA-P2-GF-I-034,External,HSIP 162,3164,430,0.5,162,38x140,OSB SF,9,150x150,No,0,0,0,19kg,1,6",
        "214-DA-P2-GF-F-041,External,IFAST 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,3"
        
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(2);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-034",
            project: "PROJ1",
            type: "Ext",
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "HSIP 162",
              studSize: "38x140",
              sheathing: "OSB SF",
              components: 9,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({ 
            id: "214-DA-P2-GF-F-041", 
            project: "PROJ1", 
            type: "ExtF", 
            dimensions:{ 
              length: 2, 
              height: 3,
              width: 207, 
              area: 9, 
              weight: 435 
            }, 
            additionalInfo:{ 
              framingStyle: "IFAST 213", 
              studSize: "38x184", 
              sheathing: "OSB 5980", 
              components: 6, 
              nailing: "150x150", 
              spandrel: "No", 
              doors: null, 
              windows: null, 
              pockets: null, 
              qty: 3 
            } 
          });
          expect(result.errors).toEqual([]);

          done();
        });

      scope.$apply();
    });

    it("caches area search result with XLSX", function (done) {
      addSheet([
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "314-DA-P2-GF-C-034,Roof,SIP,1000,1000,1000,1000,38x184,OSB 5980,1,150x150,No,1,1,1,19kg,1",
        "314-DA-P2-GF-C-035,Roof,SIP,1000,1000,1000,1000,38x184,OSB 5980,1,150x150,No,1,1,1,19kg,1",
        "214-DA-P2-GF-F-041,External,IFAST 213,2,3,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,3"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(3);

          expect(mockAreasService.getProjectAreas.calls.count()).toEqual(2);

          done();
        });

      scope.$apply();
    });

  });
  
  describe("loadPanelSchedule with CSV", function () {
    
    beforeEach(inject(function ($firebaseArray) {

      var projectComponents = {
        "PROJ1": {
          "component1": {
            "project": "PROJ1",
            "areas": {
              "AREA1": true,
              "AREA2": true,
              "AREA4": true
            },
            "type": "Walls"
          },
          "component2": {
            "project": "PROJ1",
            "areas": {
              "AREA5": true
            },
            "type": "IFAST"
          }
        },
        "PROJ2": {
          "component2": {
            "project": "PROJ2",
            "areas": {
              "AREA3": true
            },
            "type": "Roof"
          }
        }
      };

      var projectAreas = {
        "PROJ1": {
          "AREA1": {
            "phase": "P2",
            "floor": "GF",
            "type": "Ext"
          },
          "AREA2": {
            "phase": "P2",
            "floor": "GF",
            "type": "Int"
          },
          "AREA4": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtH"
          },
          "AREA5": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtF"
          }
        },
        "PROJ2": {
          "AREA3": {
            "phase": "P2",
            "floor": "GF",
            "type": "Roof"
          }
        }
      };
      
      var areaPanels = {
        "214-DA-P2-GF-I-100": {
          "PANEL1": {
            id: "214-DA-P2-GF-I-100",
            project: "PROJ1",
            type: "Ext",
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-L-111": {
          "PANEL2": {
            id: "214-DA-P2-GF-L-111",
            project: "PROJ1",
            type: "Int",
            area: "AREA2",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "314-DA-P2-GF-C-112": {
          "PANEL3": {
            id: "314-DA-P2-GF-C-112",
            project: "PROJ2",
            type: "Roof",
            area: "AREA3",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-H-113": {
          "PANEL4": {
            id: "214-DA-P2-GF-H-113",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA4",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-I-114": {
          "PANEL5": {
            id: "214-DA-P2-GF-I-114",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA4",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        }
      };

      mockComponentsService.getComponentsForProject.and.callFake(
        function (projectKey) {
          var componentsRef = new MockFirebase("MockQuery://components");
          componentsRef.autoFlush(true);
          componentsRef.set(projectComponents[projectKey]);

          return $firebaseArray(componentsRef);
        });

      mockAreasService.getProjectAreas.and.callFake(
        function (projectKey) {
          var areasRef = new MockFirebase("MockQuery://areas");
          areasRef.autoFlush(true);
          areasRef.set(projectAreas[projectKey]);

          return $firebaseArray(areasRef);
        });

      mockPanelsService.getPanelById.and.callFake(
        function (panelId) {
          var panelsRef = new MockFirebase("MockQuery://panels");
          panelsRef.autoFlush(true);
          panelsRef.set(areaPanels[panelId]);

          return $firebaseArray(panelsRef);
        });

    }));

    beforeEach(function () {

      mockFileReader = {
        onload: undefined,
        result: undefined,
        readAsBinaryString: jasmine.createSpy("readAsBinaryString")
      };

      mockFileReader.readAsBinaryString.and.callFake(function (blob) {
        mockFileReader.onload({target: {result: worksheet}});
      });

    });

    it("validates panel reference format with CSV", function (done) {
      
      addCSV([["Panel Ref"],
        ["214-P2-GF-I-034"],
        ["ABC-DA-P2-GF-I-034"],
        ["214-DA-P2-GF-I-XYZ"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-P2-GF-I-034 Invalid panel reference",
            "ABC-DA-P2-GF-I-034 Invalid panel reference",
            "214-DA-P2-GF-I-XYZ Invalid panel reference"
          ]);

          done();
        });

      scope.$apply();
    });
    
    it("reports unknown project in panel reference with CVS", function (done) {
      
      addCSV([["Panel Ref"],
        ["241-DA-P2-GF-I-034"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "241-DA-P2-GF-I-034 Unknown project '241'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown phase/floor/panel type in panel reference with CSV", function (done) {

      addCSV([["Panel Ref"],
        ["214-DA-XX-GF-I-034"],
        ["214-DA-P2-YY-I-034"],
        ["214-DA-P2-GF-Z-034"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});

      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-XX-GF-I-034 Unknown phase 'XX'",
            "214-DA-P2-YY-I-034 Unknown floor 'YY'",
            "214-DA-P2-GF-Z-034 Unknown panel type 'Z'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown type with CSV", function (done) {

      addCSV([["Panel Ref", "Type"],
        ["214-DA-P2-GF-I-034","Floor"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});

      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-P2-GF-I-034 Unknown type 'Floor'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports unknown area with CSV", function (done) {

      addCSV([["Panel Ref", "Type"],
        ["214-DA-P2-1F-I-034", "External"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});

      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels).toEqual([]);
          expect(result.errors).toEqual([
            "214-DA-P2-1F-I-034 Unknown area 'P2-1F-Ext'"
          ]);

          done();
        });

      scope.$apply();
    });

    it("reports missing dimensions with CSV", function (done) {
      
      addCSV([["Panel Ref","Type","Length","Height","Area","Depth","Weight"],
        ["214-DA-P2-GF-I-001","External","0","430","0.5","162","19kg"],
        ["214-DA-P2-GF-I-002","External","3164","0","0.5","162","19kg"],
        ["214-DA-P2-GF-I-003","External","3164","430","0","162","19kg"],
        ["214-DA-P2-GF-I-004","External","3164","430","0.5","0","19kg"],
        ["214-DA-P2-GF-I-005","External","3164","430","0.5","162","0kg"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.errors.length).toEqual(5);
          expect(result.errors).toEqual([
            "214-DA-P2-GF-I-001 Missing dimension information",
            "214-DA-P2-GF-I-002 Missing dimension information",
            "214-DA-P2-GF-I-003 Missing dimension information",
            "214-DA-P2-GF-I-004 Missing dimension information",
            "214-DA-P2-GF-I-005 Missing dimension information"
          ]);

          done();
        });

      scope.$apply();
    });
    
    it("loads panel details from sheet with CSV", function (done) {
      addCSV([
        ["Panel Ref","Type","Framing Style","Length","Height","Area","Depth","Stud Size","Sheathing","Components","Nailing","Spandrel","Doors","Windows","Pockets","Weight","Qty"],
        ["214-DA-P2-GF-I-034","External","SIP 213","3164","430","0.5","162","38x184","OSB 5980","6","150x150","No","0","0","0","19kg","1"],
        ["214-DA-P2-GF-L-036","Internal","SIP 213","3700","270","0.7","162","38x184","OSB 5980","6","150x150","No","0","0","0","30kg","1"],
        ["314-DA-P2-GF-C-037","Roof","SIP 213","2666","3400","9","207","38x184","OSB 5980","6","150x150","No","0","0","0","435kg","1"],
        ["214-DA-P2-GF-H-038","External","HSIP 213","2640","3400","9","207","38x184","OSB 5980","6","150x150","No","0","0","0","435kg","1"],
        ["214-DA-P2-GF-I-039","External","HSIP 213","2","3","9","207","38x184","OSB 5980","6","150x150","No","0","0","0","435kg","1"],
        ["214-DA-P2-GF-F-040","External","IFAST 213","2640","3400","9","207","38x184","OSB 5980","6","150x150","No","0","0","0","435kg","1"],
        ["214-DA-P2-GF-F-041","External","IFAST 213","2","3","9","207","38x184","OSB 5980","6","150x150","No","0","0","0","435kg","1"]
      ]);
 
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels.length).toEqual(7);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-034",
            project: "PROJ1",
            area: "AREA1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({
            id: "214-DA-P2-GF-L-036",
            project: "PROJ1",
            area: "AREA2",
            type: "Int",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[2]).toEqual({
            id: "214-DA-P2-GF-H-038",
            project: "PROJ1",
            area: "AREA4",
            type: "ExtH",
            dimensions: {
              length: 2640,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[3]).toEqual({
            id: "214-DA-P2-GF-I-039",
            project: "PROJ1",
            area: "AREA1",
            type: "Ext",
            dimensions: {
              length: 2,
              height: 3,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[4]).toEqual({ 
            id: "214-DA-P2-GF-F-040", 
            project: "PROJ1", 
            type: "ExtF", 
            area: "AREA5", 
            dimensions: { length: 2640, height: 3400, width: 207, area: 9, weight: 435 }, 
            additionalInfo: { 
              framingStyle: "IFAST 213", 
              studSize: "38x184", 
              sheathing: "OSB 5980", 
              components: 6, 
              nailing: "150x150", 
              spandrel: "No", 
              doors: null, 
              windows: null, 
              pockets: null, 
              qty: 1 
            } 
          });
          expect(result.errors).toEqual([]);

          done();
        });

      scope.$apply();
    });
    
    it("loads panel job sheet details from sheet with CSV", function (done) {
      addCSV([
        ["Panel Ref","Jobsheet No."],
        ["214-DA-P2-GF-I-100","JS-214-P2-GF-I-020"],
        ["214-DA-P2-GF-L-111","JS-214-P2-GF-L-020"],
        ["314-DA-P2-GF-C-112","JS-314-P2-GF-C-020"],
        ["214-DA-P2-GF-H-113","JS-214-P2-GF-H-020"],
        ["214-DA-P2-GF-I-114","JS-214-P2-GF-I-020"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels.length).toEqual(5);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-100",
            project: "PROJ1",
            type: "Ext",
            jobSheet: 20,
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({
            id: "214-DA-P2-GF-L-111",
            project: "PROJ1",
            type: "Int",
            jobSheet: 20,
            area: "AREA2",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[2]).toEqual({
            id: "214-DA-P2-GF-H-113",
            project: "PROJ1",
            type: "ExtH",
            jobSheet: 20,
            area: "AREA4",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[3]).toEqual({
            id: "214-DA-P2-GF-I-114",
            project: "PROJ1",
            type: "ExtH",
            jobSheet: 20,
            area: "AREA4",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[4]).toEqual({
            id: "314-DA-P2-GF-C-112",
            project: "PROJ2",
            type: "Roof",
            jobSheet: 20,
            area: "AREA3",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.errors).toEqual([]);

          done();
        });

      scope.$apply();
    });

    it("skips header rows with CSV", function (done) {
      addCSV([
        ["Job Reference","08621B"],
        ["House Type"],
        ["Description"],
        ["Cutting Set","206mm iSIP"],
        [""],
        ["Panel Ref","Type","Framing Style","Length","Height","Area","Depth","Stud Size","Sheathing","Components","Nailing","Spandrel","Doors","Windows","Pockets","Weight","Qty"],
        ["214-DA-P2-GF-I-012","External","SIP 213","3164","430","0.5","162","38x184","OSB 5980","6","150x150","No","0","0","0","19kg","1"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});
      
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels.length).toEqual(1);
          expect(result.panels[0]).toEqual(jasmine.objectContaining({id: "214-DA-P2-GF-I-012"}));

          done();
        });

      scope.$apply();
    });

    it("caches area search result with CSV", function (done) {
      addCSV([
        ["Panel Ref","Type","Framing Style","Length","Height","Area","Depth","Stud Size","Sheathing","Components","Nailing","Spandrel","Doors","Windows","Pockets","Weight","Qty"],
        ["314-DA-P2-GF-C-034","Roof","SIP","1000","1000","1000","1000","38x184","OSB 5980","1","150x150","No","1","1","1","19kg","1"],
        ["314-DA-P2-GF-C-035","Roof","SIP","1000","1000","1000","1000","38x184","OSB 5980","1","150x150","No","1","1","1","19kg","1"]
      ]);
      
      var file = new Blob([worksheet],{"type":"text/csv"});

      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          expect(result.panels.length).toEqual(2);

          expect(mockAreasService.getProjectAreas.calls.count()).toEqual(1);

          done();
        });

      scope.$apply();
    });

  });
  
  describe("Multiple areas with same id", function () {

    beforeEach(inject(function ($firebaseArray) {

      var projectComponents = {
        "PROJ1": {
          "component1": {
            "project": "PROJ1",
            "areas": {
              "AREA1": true,
              "AREA2": true,
              "AREA5": true,
              "AREA7": true
            },
            "type": "Walls"
          },
          "component4": {
            "project": "PROJ1",
            "areas": {
              "AREA4": true
            },
            "type": "Bay Window"
          }
        },
        "PROJ2": {
          "component2": {
            "project": "PROJ2",
            "areas": {
              "AREA3": true
            },
            "type": "Roof"
          },
          "component3": {
            "project": "PROJ2",
            "areas": {
              "AREA6": true
            },
            "type": "Roof"
          }
        }
      };

      var projectAreas = {
        "PROJ1": {
          "AREA1": {
            "phase": "P2",
            "floor": "GF",
            "type": "Ext"
          },
          "AREA2": {
            "phase": "P2",
            "floor": "GF",
            "type": "Int"
          },
          "AREA4": {
            "phase": "P2",
            "floor": "GF",
            "type": "Ext"
          },
          "AREA5": {
            "phase": "P2",
            "floor": "GF",
            "type": "Ext"
          },
          "AREA7": {
            "phase": "P2",
            "floor": "GF",
            "type": "ExtH"
          }
        },
        "PROJ2": {
          "AREA3": {
            "phase": "P2",
            "floor": "GF",
            "type": "Roof"
          },
          "AREA6": {
            "phase": "P2",
            "floor": "GF",
            "type": "Roof"
          }
        }
      };

      var areaPanels = {
        "214-DA-P2-GF-I-100": {
          "PANEL1": {
            id: "214-DA-P2-GF-I-100",
            project: "PROJ1",
            type: "Ext",
            area: "AREA1",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-L-111": {
          "PANEL2": {
            id: "214-DA-P2-GF-L-111",
            project: "PROJ1",
            type: "Int",
            area: "AREA2",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "314-DA-P2-GF-C-112": {
          "PANEL3": {
            id: "314-DA-P2-GF-C-112",
            project: "PROJ2",
            type: "Roof",
            area: "AREA3",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-H-113": {
          "PANEL4": {
            id: "214-DA-P2-GF-H-113",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA7",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        },
        "214-DA-P2-GF-I-114": {
          "PANEL5": {
            id: "214-DA-P2-GF-I-114",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA7",
            dimensions: {
              length: 22,
              height: 20,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          }
        }
      };

      mockComponentsService.getComponentsForProject.and.callFake(
        function (projectKey) {
          var componentsRef = new MockFirebase("MockQuery://components");
          componentsRef.autoFlush(true);
          componentsRef.set(projectComponents[projectKey]);

          return $firebaseArray(componentsRef);
        });

      mockAreasService.getProjectAreas.and.callFake(
        function (projectKey) {
          var areasRef = new MockFirebase("MockQuery://areas");
          areasRef.autoFlush(true);
          areasRef.set(projectAreas[projectKey]);

          return $firebaseArray(areasRef);
        });

      mockPanelsService.getPanelById.and.callFake(
        function (panelId) {
          var panelsRef = new MockFirebase("MockQuery://panels");
          panelsRef.autoFlush(true);
          panelsRef.set(areaPanels[panelId]);

          return $firebaseArray(panelsRef);
        });

    }));

    beforeEach(function () {

      mockFileReader = {
        onload: undefined,
        result: undefined,
        readAsBinaryString: jasmine.createSpy("readAsBinaryString")
      };

      mockFileReader.readAsBinaryString.and.callFake(function (blob) {
        mockFileReader.onload({target: {result: worksheet}});
      });

    });

    it("Tests component dropdown if more than one area with same id exists", function (done) {

      addSheet([
        "Job Reference,08621B",
        "House Type",
        "Description",
        "Cutting Set,206mm iSIP",
        "",
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-030,External,HSIP 213,3,4,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-H-031,External,HSIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-I-032,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-I-033,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1"
      ]);
      
      addSheet([
        "Job Reference,08621C",
        "House Type",
        "Description",
        "Cutting Set,206mm iSIP",
        "",
        "Panel Ref,Type,Framing Style,Length,Height,Area,Depth,Stud Size,Sheathing,Components,Nailing,Spandrel,Doors,Windows,Pockets,Weight,Qty",
        "214-DA-P2-GF-I-034,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-I-035,External,SIP 213,3164,430,0.5,162,38x184,OSB 5980,6,150x150,No,0,0,0,19kg,1",
        "214-DA-P2-GF-L-036,Internal,SIP 213,3700,270,0.7,162,38x184,OSB 5980,6,150x150,No,0,0,0,30kg,1",
        "314-DA-P2-GF-C-037,Roof,SIP 213,2666,3400,9,207,38x184,OSB 5980,6,150x150,No,0,0,0,435kg,1"
      ]);

      loadPanelsService.loadPanelSchedule(new Blob())
        .then(function (result) {
          expect(result.panels.length).toEqual(8);
          expect(result.panels[0]).toEqual({
            id: "214-DA-P2-GF-I-030",
            project: "PROJ1",
            type: "Ext",
            dimensions: {
              length: 3,
              height: 4,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[1]).toEqual({
            id: "214-DA-P2-GF-H-031",
            project: "PROJ1",
            type: "ExtH",
            area: "AREA7",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "HSIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[2]).toEqual({
            id: "214-DA-P2-GF-I-032",
            project: "PROJ1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[3]).toEqual({
            id: "214-DA-P2-GF-I-033",
            project: "PROJ1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[4]).toEqual({
            id: "214-DA-P2-GF-I-034",
            project: "PROJ1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[5]).toEqual({
            id: "214-DA-P2-GF-I-035",
            project: "PROJ1",
            type: "Ext",
            dimensions: {
              length: 3164,
              height: 430,
              width: 162,
              area: 0.5,
              weight: 19
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[6]).toEqual({
            id: "214-DA-P2-GF-L-036",
            project: "PROJ1",
            area: "AREA2",
            type: "Int",
            dimensions: {
              length: 3700,
              height: 270,
              width: 162,
              area: 0.7,
              weight: 30
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.panels[7]).toEqual({
            id: "314-DA-P2-GF-C-037",
            project: "PROJ2",
            type: "Roof",
            dimensions: {
              length: 2666,
              height: 3400,
              width: 207,
              area: 9,
              weight: 435
            },
            additionalInfo: {
              framingStyle: "SIP 213",
              studSize: "38x184",
              sheathing: "OSB 5980",
              components: 6,
              nailing: "150x150",
              spandrel: "No",
              doors: null,
              windows: null,
              pockets: null,
              qty: 1
            }
          });
          expect(result.components).toEqual({
            "214-P2-GF-Ext": {
              "selected": false,
              "type": {
                "Walls": {
                  "AREA1": {
                    "areaObj": {floor: "GF", phase: "P2", type: "Ext", $id: "AREA1", $priority: null}
                  },
                  "AREA5": {
                    "areaObj": {floor: "GF", phase: "P2", type: "Ext", $id: "AREA5", $priority: null}
                  }
                },
                "Bay Window": {
                  "AREA4": {
                    "areaObj": {floor: "GF", phase: "P2", type: "Ext", $id: "AREA4", $priority: null}
                  }
                }
              }
            },
            "314-P2-GF-Roof": {
              "selected": false,
              "type": {
                "Roof": {
                  "AREA3": {
                    "areaObj": {floor: "GF", phase: "P2", type: "Roof", $id: "AREA3", $priority: null}
                  },
                  "AREA6": {
                    "areaObj": {floor: "GF", phase: "P2", type: "Roof", $id: "AREA6", $priority: null}
                  }
                }
              }
            }
          });
          expect(result.errors).toEqual([]);

          done();
        });

      scope.$apply();
    });

  });
  
  function addCSV(data) {
    var lineArray = [];
    data.forEach(function (infoArray, index) {
      var line = infoArray.join(",");
      lineArray.push(line);
    });
    worksheet = lineArray.join("\n");
  }

  function addSheet(lines) {
    var sheet = {};

    var cols = 0;
    angular.forEach(lines, function (line, row) {
      var values = line.split(",");
      angular.forEach(values, function (value, col) {
        sheet[encode_cell({c: col, r: row})] = {v: value};
      });
      cols = Math.max(cols, values.length);
    });

    // size of sheet
    sheet["!ref"] = encode_range({s: {c: 0, r: 0}, e: {c: cols, r: lines.length}});

    worksheet.Sheets.push(sheet);
  }

  function encode_cell(cell) {
    return String.fromCharCode(65 + cell.c) + (cell.r + 1);
  }

  function decode_cell(cell_address) {
    var match = /^([A-Z])(\d+)$/.exec(cell_address);
    return {c: match[1].charCodeAt(0) - 65, r: match[2] - 1};
  }

  function encode_range(range) {
    return encode_cell(range.s) + ":" + encode_cell(range.e);
  }

  function decode_range(range) {
    var parts = range.split(":", 2);
    return {s: decode_cell(parts[0]), e: decode_cell(parts[1])};
  }
});
