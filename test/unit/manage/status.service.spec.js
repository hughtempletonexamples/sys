"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("status service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var componentsRef;
  var panelsRef;
  var referenceDataRef;
  var panelsImagesRef;
  var areaProductionPlansRef;
  var mockSettingsService;
  var mockAreasService;
  var mockProjectsService;
  var mockPanelsService;
  var mockPlannerService;
  var mockReferenceDataService;
  var mockComponentsService;
  var statsRef;

  var scope;

  var statusService;
  var areas;
  var projects;
  var revisions;
  var panel1;
  var project1 = {
    $id: "PROJ1",
    "name": "PROJ1 Name",
    "active": true,
    "id": "001",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV1"
    }
  };
  var Image01 = {
    "id": "panel1",
    "name": "Image01",
    "project": "PROJ2"    
  };

  beforeEach(angular.mock.module("innView.manage", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    areaProductionPlansRef = firebaseRoot.child("productionPlans/areas");
    componentsRef = firebaseRoot.child("components");
    referenceDataRef = firebaseRoot.child("srd");
    panelsImagesRef = firebaseRoot.child("images").child("panels");
    statsRef = firebaseRoot.child("stats");
    
    utils.addQuerySupport(statsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(componentsRef);
    utils.addQuerySupport(panelsRef);
    utils.addQuerySupport(panelsImagesRef);

    mockSettingsService = jasmine.createSpyObj("settingsService", ["get"]);
    mockComponentsService = jasmine.createSpyObj("componentsService", ["getComponentsForProject"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas", "getProjectArea", "updateProjectArea", "modifyCompletedPanels"]);
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProject"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea", "markPanelsComplete", "deletePanels","getPanelImage", "markPanelIncomplete", "updatePanels"]);
    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProjectAreaProductionPlans"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getDesignHandoverValues","getProductTypes", "getSuppliers"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("settingsService", function () {
        return mockSettingsService;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("componentsService", function () {
        return mockComponentsService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
      
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($q) {
    mockSettingsService.get.and.returnValue(
      $q.when({
        designHandoverPeriod: 10,
        designHandoverBuffer: 3
      })
    );
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));
  
  beforeEach(inject(function ($q) {
    mockPanelsService.getPanelImage.and.callFake(function (data) {
      return $q.when(data);
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getSuppliers.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("suppliers"));
    });
  }));
  beforeEach(inject(function ($q) {
    mockProjectsService.getProject.and.callFake(function () {
      return $q.when(project1);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPlannerService.getProjectAreaProductionPlans.and.callFake(function () {
      return $firebaseArray(areaProductionPlansRef);
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {

    var projectComponents = {
      "PROJ1": {
        "component1": {
          "project": "PROJ1",
          "areas": {
            "PROJ1AREA1": true,
            "PROJ1AREA2": true
          },
          "type": "Walls"
        }
      },
      "PROJ2": {
        "component2": {
          "project": "PROJ2",
          "areas": {
            "PROJ2AREA1": true
          },
          "type": "Roof"
        }
      }
    };

    mockComponentsService.getComponentsForProject.and.callFake(
      function (projectKey) {
        var componentsRef = new MockFirebase("MockQuery://components");
        componentsRef.autoFlush(true);
        componentsRef.set(projectComponents[projectKey]);

        return $firebaseArray(componentsRef);
      });
  }));
  
  beforeEach(inject(function ($firebaseArray) {

    var stats = {
      "SIP": {
        "2018-09-12": {
          "designedCount": 5,
          "designedM2": 20
        }
      },
      "HSIP": {
        "2018-09-12": {
          "designedCount": 4,
          "designedM2": 16
        }
      }
    };

    setStats();
    function setStats(projectKey) {
      var statsRef = new MockFirebase("MockQuery://stats");
      statsRef.autoFlush(true);
      statsRef.set(stats);

      return $firebaseArray(statsRef);
    }
    

  }));
  beforeEach(function () {
    panel1 = {
      "area": "AREA1",
      "id": "001-DA-P1-GF-I-001",
      "project": "PROJ1",
      "type": "Ext",
      "dimensions": {
        "area": 0.5,
        "width": 1062,
        "height": 430,
        "length": 6100,
        "weight": 19
      },
      "timestamps": {
        "created": new Date("2018-09-12T12:13:00").getTime()
      }
    };
  });  


  beforeEach(inject(function ($rootScope, _statusService_) {
    scope = $rootScope;
    statusService = _statusService_;
  }));

  beforeEach(inject(function ($firebaseArray, $firebaseObject) {
    mockAreasService.getProjectAreas.and.callFake(function (y) {
      return $firebaseArray(areasRef);
    });
    mockAreasService.getProjectArea.and.callFake(function (areaKey) {
      return $firebaseObject(areasRef.child(areaKey));
    });
  }));

  beforeEach(function () {
    
    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );
  
    referenceDataRef.child("suppliers").set(
      {
        "Innovaré": {thirdParty: false},
        "Other": {thirdParty: true}
      }
    );
    referenceDataRef.child("images").child("panels").set(
      {
        "Image01":{
          "id":"panel1",
          "name":"Image01",
          "project":"PROJ2"
        },
        "Image02":{
          "id":"panel2",
          "name":"Image02",
          "project":"PROJ2"
        }        
      }
    );    
  
    projects = {
      "PROJ1": project1,
      "PROJ2": {
        "name": "PROJ2",
        "active": true,
        "id": "002",
        "deliverySchedule": {
          "unpublished": "PROJ2REV1"
        }
      }
    };
    projectsRef.set(projects);

    areas = {
      "PROJ1AREA1": {
        "phase": "P1",
        "floor": "GF",
        "type": "Ext",
        "estimatedArea": 100.1,
        "estimatedPanels": 20,
        "actualArea": 95.1,
        "actualPanels": 15,
        "completedArea": 95.1,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2016-04-10"
        }
      },
      "PROJ1AREA2": {
        "phase": "P1",
        "floor": "GF",
        "type": "Int",
        "estimatedArea": 50.1,
        "estimatedPanels": 10,
        "actualArea": 45.1,
        "actualPanels": 9,
        "completedArea": 20.1,
        "completedPanels": 4,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2016-04-05"
        }
      }
    };
    areasRef.set(areas);

    revisions = {
      "PROJ1REV1": {
        "createdDate": "2016-04-01"
      }
    };
    revisionsRef.set(revisions);
  });

  describe("getProjectsStatus", function () {
    describe("decrement design count",function (){
      it("it incriments design incrimented value for date", function (done) {

        var panels = [panel1];

        statusService.decrementDesignCount(panels)
                .then(function (newData) {
                  expect(newData).toEqual({"designedM2": 19.5, "designedCount": 4});
                });




        done();
      });
    });
    
    describe("results", function () {
      var result;

      beforeEach(function () {
        result = statusService.getProjectsStatus();
        scope.$apply();
      });

      it("contains an array equal in length to the number of projects with published revisions", function () {
        expect(result.length).toEqual(1);
      });

      it("contains the project id", function () {
        expect(result[0].projectId).toEqual("001");
      });
      it("concatenates the project name and id for the project label", function () {
        expect(result[0].label).toEqual("001 - PROJ1 Name");
      });

      it("sums the estimated panels for all child areas", function () {
        expect(result[0].estimated.count).toEqual(30);
      });

      it("sums the estimated areas for all child areas", function () {
        expect(result[0].estimated.area).toEqual(150.2);
      });

      it("contains earliest delivery date for a project", function () {
        expect(result[0].delivery).toEqual("2016-04-05");
      });

      it("sums the actual area for all child areas", function () {
        expect(result[0].actual.area).toEqual(140.2);
      });

      it("calculates the actual area percentage", function () {
        expect(result[0].actual.areaPercentage).toEqual(93);
      });

      it("sums the actual actual panels for all child areas", function () {
        expect(result[0].actual.count).toEqual(24);
      });

      it("calculates actual count percentage", function () {
        expect(result[0].actual.countPercentage).toEqual(80);
      });

      it("calculates completed area for all child areas", function () {
        expect(result[0].completed.area).toEqual(115.2);
      });

      it("calculates completed area percentage", function () {
        expect(result[0].completed.areaPercentage).toEqual(82);
      });

      it("calculates completed count for all child areas", function () {
        expect(result[0].completed.count).toEqual(18);
      });

      it("calculates completed count percentage", function () {
        expect(result[0].completed.countPercentage).toEqual(75);
      });

      it("contains earliest delivery date for a project", function () {
        expect(result[0].delivery).toEqual("2016-04-05");
      });

      it("contains the revision id, equal to the number of published revisions", function () {
        expect(result[0].revision).toEqual(2);
      });

      describe("aggregates risk status and", function () {

        it("shows medium risk for project if one areas is at medium risk", function () {
          areasRef.child("PROJ1AREA1/actualPanels").set("0");

          //PROJ1AREA1 delivery date is 2016-04-10
          jasmine.clock().mockDate(new Date("2016-03-23"));
          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.medium).toEqual(true);
          expect(result[0].risk.high).toEqual(false);
        });

        it("shows high risk for project if one areas isat high risk", function () {
          areasRef.child("PROJ1AREA1/actualPanels").set("0");

          //PROJ1AREA1 delivery date is 2016-04-10
          jasmine.clock().mockDate(new Date("2016-03-28"));
          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.medium).toEqual(false);
          expect(result[0].risk.high).toEqual(true);
        });

        it("shows high risk for project if one area is at high risk and another is medium risk", function () {
          areasRef.child("PROJ1AREA1/actualPanels").set("0");
          areasRef.child("PROJ1AREA2/actualPanels").set("0");

          //PROJ1AREA1 delivery date is 2016-04-10
          //PROJ1AREA2 delivery date is 2016-04-05
          jasmine.clock().mockDate(new Date("2016-03-23"));
          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.medium).toEqual(false);
          expect(result[0].risk.high).toEqual(true);
        });

      });

      describe("aggregates area plan", function () {

        it("to include worst risk value", function () {
          areaProductionPlansRef.child("PROJ1AREA1").set({
            plannedStart: "2016-08-01",
            plannedFinish: "2016-08-04",
            riskDate: "2016-08-01"
          });

          areaProductionPlansRef.child("PROJ1AREA2").set({
            plannedStart: "2016-09-01",
            plannedFinish: "2016-09-03",
            riskDate: "2016-09-01"
          });

          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.days).toBe(-3);
        });

        it("to include largest available risk value", function () {
          areaProductionPlansRef.child("PROJ1AREA1").set({
            plannedStart: "2016-08-01",
            plannedFinish: "2016-08-01",
            riskDate: "2016-08-01"
          });

          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.days).toBe(0);
        });

        it("to have undefined risk value if none available", function () {
          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].risk.days).toEqual(undefined);
        });

      });
      
      describe("All projects risks for production types", function () {

        it("to include worst risk value for each production type", function () {
          areaProductionPlansRef.child("PROJ1AREA1").set({
            plannedStart: "2016-08-01",
            plannedFinish: "2016-08-04",
            riskDate: "2016-08-01"
          });

          areaProductionPlansRef.child("PROJ1AREA2").set({
            plannedStart: "2016-09-01",
            plannedFinish: "2016-09-03",
            riskDate: "2016-09-01"
          });

          result = statusService.getProjectsStatus();
          scope.$apply();

          expect(result[0].allProjectRisks).toEqual({ SIP: -3, TF: -1 });
        });


      });

    });

    describe("results sorts", function () {

      it("projects by earliest delivery date", function () {
        projects["PROJ2"] = {
          "name": "PROJ2 Name",
          "id": "002",
          "active": true,
          "deliverySchedule": {
            "revisions": {
              "PROJ2REV1": true
            },
            "published": "PROJ2REV1"
          }
        };
        projectsRef.set(projects);

        areas["PROJ2AREA1"] = {
          "estimatedArea": 50,
          "estimatedPanels": 10,
          "revisions": {
            "PROJ2REV1": "2016-04-04"
          }
        };
        areasRef.set(areas);

        revisions["PROJ2REV1"] = {
          "createdDate": "2016-04-01"
        };
        revisionsRef.set(revisions);

        var result = statusService.getProjectsStatus();
        scope.$apply();

        expect(result.length).toEqual(2);
        expect(result[0].label).toEqual("002 - PROJ2 Name");
        expect(result[1].label).toEqual("001 - PROJ1 Name");
      });
    });

    describe("results limited to next 4 weeks", function () {
      beforeEach(function () {
        jasmine.clock().install();
      });

      afterEach(function () {
        jasmine.clock().uninstall();
      });

      it("if invoked with flag", function () {
        jasmine.clock().mockDate(new Date("2016-04-01"));

        projects["PROJ2"] = {
          "name": "PROJ2 Name",
          "id": "002",
          "active": true,
          "deliverySchedule": {
            "revisions": {
              "PROJ2REV1": true
            },
            "published": "PROJ2REV1"
          }
        };
        projectsRef.set(projects);

        areas["PROJ2AREA1"] = {
          "estimatedArea": "50",
          "estimatedPanels": "10",
          "revisions": {
            "PROJ2REV1": "2016-04-29"
          }
        };
        areasRef.set(areas);

        revisions["PROJ2REV1"] = {
          "createdDate": "2016-04-01"
        };
        revisionsRef.set(revisions);

        var result = statusService.getProjectsStatus(true);

        scope.$apply();

        expect(result.length).toEqual(1);
        expect(result[0].label).toEqual("001 - PROJ1 Name");
      });

      it("excludes areas from totals if delivery date greater than 4 weeks", function () {
        // Set date to between the delivery dates of PROJ1AREA1 and
        // PROJ1AREA2, so that only PROJ1AREA2 is included in results.
        jasmine.clock().mockDate(new Date("2016-03-11"));

        var result = statusService.getProjectsStatus(true);

        scope.$apply();

        expect(result[0].estimated.count).toEqual(10);
        expect(result[0].estimated.area).toEqual(50.1);
        expect(result[0].actual.count).toEqual(9);
        expect(result[0].actual.area).toEqual(45.1);
        expect(result[0].completed.count).toEqual(4);
        expect(result[0].completed.area).toEqual(20.1);
      });
    });
  });

  describe("getStatusForProject response", function () {
    var result;

    beforeEach(inject(function ($q) {
      mockProjectsService.getProject.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    beforeEach(function () {
      result = statusService.getStatusForProject("001");
      scope.$apply();
    });

    it("contains the project id", function () {
      expect(result.id).toEqual("001");
    });

    it("contains the project name", function () {
      expect(result.name).toEqual("PROJ1 Name");
    });

    it("contains the project revision number", function () {
      expect(result.revision).toEqual(2);
    });

    it("contains an array equal in length to the number of areas in the project", function () {
      expect(result.areas.length).toEqual(2);
    });

    it("contains expected area values", function () {
      var area = result.areas[1];
      expect(area.label).toEqual("P1-GF-Ext");
      expect(area.component).toEqual("Walls");
      expect(area.delivery).toEqual("2016-04-10");
      expect(area.estimated.count).toEqual(20);
      expect(area.estimated.area).toEqual(100.1);
      expect(area.actual.count).toEqual(15);
      expect(area.actual.countPercentage).toEqual(75);
      expect(area.actual.area).toEqual(95.1);
      expect(area.actual.areaPercentage).toEqual(95);
      expect(area.completed.count).toEqual(14);
      expect(area.completed.countPercentage).toEqual(93);
      expect(area.completed.area).toEqual(95.1);
      expect(area.completed.areaPercentage).toEqual(100);

    });

    it("contains expected production plan values", function () {
      areaProductionPlansRef.child("PROJ1AREA2").set({
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-02",
        riskDate: "2016-08-03"
      });

      result = statusService.getStatusForProject("001");
      scope.$apply();

      expect(result.areas[0].label).toBe("P1-GF-Int");
      expect(result.areas[0].plan).toEqual({
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-02",
        riskDate: "2016-08-03"
      });
      expect(result.areas[1].label).toBe("P1-GF-Ext");
      expect(result.areas[1].plan).toBe(undefined);
    });

    it("contains medium risk for area with no panels loaded before design handover period but during buffer", function () {
      areasRef.child("PROJ1AREA1/actualPanels").set("0");

      //PROJ1AREA1 delivery date is 2016-04-10
      jasmine.clock().mockDate(new Date("2016-03-23"));
      result = statusService.getStatusForProject("001");
      scope.$apply();

      var area = result.areas[1];
      expect(area.risk.medium).toEqual(true);
      expect(area.risk.high).toEqual(false);
    });

    it("contains high risk for area with no panels loaded during design handover period", function () {
      areasRef.child("PROJ1AREA1/actualPanels").set("0");

      //PROJ1AREA1 delivery date is 2016-04-10
      jasmine.clock().mockDate(new Date("2016-03-28"));
      result = statusService.getStatusForProject("001");
      scope.$apply();

      var area = result.areas[1];
      expect(area.risk.medium).toEqual(false);
      expect(area.risk.high).toEqual(true);
    });

    it("orders the areas by delivery date", function () {
      var area1 = result.areas[0];
      var area2 = result.areas[1];

      expect(area1.label).toEqual("P1-GF-Int");
      expect(area1.delivery).toEqual("2016-04-05");
      expect(area2.label).toEqual("P1-GF-Ext");
      expect(area2.delivery).toEqual("2016-04-10");
    });

    it("excludes unpublished areas", inject(function ($q) {
      var project2 = {
        "name": "PROJ2",
        "id": "002",
        "active": true,
        "deliverySchedule": {
          "revisions": {
            "PROJ2REV1": true
          },
          "published": "PROJ2REV1",
          "unpublished": "PROJ2REV2"
        }
      };

      mockProjectsService.getProject.and.callFake(function () {
        return $q.when(project2);
      });

      projects["PROJ2"] = project2;
      projectsRef.set(projects);

      areas = {
        "PROJ2AREA1": {
          "phase": "P2",
          "floor": "1F",
          "type": "Ext",
          "estimatedArea": 10,
          "estimatedPanels": 2,
          "actualArea": 10,
          "actualPanels": 2,
          "completedArea": 5,
          "completedPanels": 1,
          "revisions": {
            "PROJ2REV1": "2016-04-10",
            "PROJ2REV2": "2016-04-05"
          }
        },
        "PROJ2AREA2": {
          "phase": "P2",
          "floor": "1F",
          "type": "Int",
          "estimatedArea": 5,
          "estimatedPanels": 1,
          "actualArea": 5,
          "actualPanels": 1,
          "completedArea": 0,
          "completedPanels": 0,
          "revisions": {
            "PROJ2REV2": "2016-04-05"
          }
        }
      };
      areasRef.set(areas);

      revisions["PROJ2REV1"] = {
        "createdDate": "2016-04-01"
      };
      revisions["PROJ2REV2"] = {
        "createdDate": "2016-04-01"
      };
      revisionsRef.set(revisions);
      scope.$apply();

      result = statusService.getStatusForProject("002", false);

      scope.$apply();
      expect(result.areas.length).toEqual(1);
      expect(result.areas[0].label).toEqual("P2-1F-Ext");
    }));

    describe("limits areas", function () {
      beforeEach(function () {
        jasmine.clock().install();
      });

      afterEach(function () {
        jasmine.clock().uninstall();
      });

      it("to next 4 weeks if invoked with flag", function () {
        jasmine.clock().mockDate(new Date("2016-03-12")); // 4 weeks before delivery date of PROJ1AREA1

        result = statusService.getStatusForProject("001", true);
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0].label).toEqual("P1-GF-Int");
      });
    });
  });

  describe("getAreaStatus response", function () {

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1"
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1"
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA1"
    };

    var panels;

    beforeEach(function () {
      panels = {
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA1PANEL3": panel3
      };

      panelsRef.set(panels);
    });

    beforeEach(inject(function ($q) {
      mockProjectsService.getProject.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    beforeEach(inject(function ($firebaseArray) {
      mockPanelsService.panelsForArea.and.callFake(function () {
        return $firebaseArray(panelsRef);
      });
    }));

    it("contains the panels for the area", function () {
      var result = statusService.getAreaStatus("PROJ1", "PROJ1AREA1");

      scope.$apply();
      expect(result.panels[0]).toEqual(jasmine.objectContaining(panel1));
      expect(result.panels[1]).toEqual(jasmine.objectContaining(panel2));
      expect(result.panels[2]).toEqual(jasmine.objectContaining(panel3));
    });

    it("contains the project details", function () {
      var result = statusService.getAreaStatus("PROJ1", "PROJ1AREA1");

      scope.$apply();
      expect(result.project).toEqual(jasmine.objectContaining(project1));
    });

    it("contains the area details", function () {
      var result = statusService.getAreaStatus("PROJ1", "PROJ1AREA1");

      scope.$apply();
      expect(result.area).toEqual(jasmine.objectContaining({
        "phase": "P1",
        "floor": "GF",
        "type": "Ext",
        "estimatedArea": 100.1,
        "estimatedPanels": 20,
        "actualArea": 95.1,
        "actualPanels": 15,
        "completedArea": 95.1,
        "completedPanels": 14,
        "revisions": {
          "PROJ1REV1": "2016-04-10"
        }
      }));
    });
  });

  describe("markPanelsIncomplete", function () {

    var createdTimestamp = 1458732564115;
    var completedTimestamp = 1458432000000;

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp,
        modified: completedTimestamp,
        completed: completedTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panelsFirebaseArray;

    beforeEach(inject(function ($firebaseArray) {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1
      });

      utils.addQuerySupport(panelsRef);
      panelsFirebaseArray = $firebaseArray(panelsRef);

      scope.$apply();
    }));

    it("invokes panelsService", function () {
      statusService.markPanelIncomplete(panelsFirebaseArray, "PROJ1AREA1PANEL1");

      scope.$apply();
      expect(mockPanelsService.markPanelIncomplete).toHaveBeenCalledWith(panelsFirebaseArray, "PROJ1AREA1PANEL1");
    });
  });

  describe("markPanelsComplete", function () {

    var createdTimestamp = 1458732564115;

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panelsFirebaseArray;

    beforeEach(inject(function ($firebaseArray) {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA1PANEL3": panel3
      });

      panelsFirebaseArray = $firebaseArray(panelsRef);
      scope.$apply();
    }));

    beforeEach(inject(function ($q) {
      mockProjectsService.getProject.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    beforeEach(inject(function ($q) {
      mockPanelsService.markPanelsComplete.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    beforeEach(function () {
      jasmine.clock().install();
    });

    afterEach(function () {
      jasmine.clock().uninstall();
    });

    it("invokes panelsService according to selection status", function () {
      var selection = {
        panel1: false,
        panel2: true,
        panel3: true
      };

      var completedDate = "2016-03-20";
      var modifiedTimestamp = 1458732705315;

      jasmine.clock().mockDate(new Date(modifiedTimestamp));

      statusService.markPanelsComplete(panelsFirebaseArray, selection, completedDate);

      scope.$apply();

      expect(mockPanelsService.markPanelsComplete)
        .toHaveBeenCalledWith("PROJ1AREA1", ["PROJ1AREA1PANEL2", "PROJ1AREA1PANEL3"], completedDate);

    });
  });
  
  describe("deletePanels", function () {

    var createdTimestamp = 1458732564115;

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panelsFirebaseArray;

    beforeEach(inject(function ($firebaseArray) {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA1PANEL3": panel3
      });

      panelsFirebaseArray = $firebaseArray(panelsRef);
      scope.$apply();
    }));

    beforeEach(inject(function ($q) {
      mockProjectsService.getProject.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    beforeEach(inject(function ($q) {
      mockPanelsService.deletePanels.and.callFake(function () {
        return $q.when(project1);
      });
    }));

    it("invokes panelsService according to selection status", function () {
      var selection = {
        panel1: false,
        panel2: true,
        panel3: true
      };

      statusService.deletePanels(panelsFirebaseArray, selection);

      scope.$apply();

      expect(mockPanelsService.deletePanels)
        .toHaveBeenCalledWith(panelsFirebaseArray, selection);

    });
  });
  
  describe("get fire storage", function () {
    
    it("gets image when given panel reference", function () {
      scope.$apply();
      
      return mockPanelsService.getPanelImage("panel1").then(function (data){
        expect(data).toEqual(jasmine.objectContaining({"id":"panel1","name":"Image01","project":"PROJ2"}));
      });
      return mockPanelsService.getPanelImage("panel2").then(function (data) {
        expect(data).toEqual(jasmine.objectContaining({"id": "panel2", "name": "Image02", "project": "PROJ2"}));
      });

    });


  });
});
