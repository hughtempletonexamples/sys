"use strict";

var utils = require("../testUtils");

describe("manage", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var scope;
  var mockLoadPanelsService;
  var mockPanelsService;
  var mockDatumsService;

  var updatePanelsReturnValue;

  beforeEach(angular.mock.module("innView.manage", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    mockLoadPanelsService = jasmine.createSpyObj("loadPanelsService", ["loadPanelSchedule","incrimentDesignCount"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["updatePanels"]);
    mockDatumsService = jasmine.createSpyObj("datumsService", ["averageMeterSquareOfDatum"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("loadPanelsService", function () {
        return mockLoadPanelsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("datumsService", function () {
        return mockDatumsService;
      });
    });

    var firebaseRoot = new MockFirebase("Mock://");
    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope;
  }));

  beforeEach(inject(function ($q) {
    mockLoadPanelsService.loadPanelSchedule.and.callFake(function (file) {
      var result = { panels: [], errors: [] };

      switch (file) {
      case "File1":
        result.panels = [{id: "A"}];
        break;
      case "File2":
        result.panels = [{id: "B"}, {id: "C"}];
        result.errors = ["Error B", "Error C"];
        break;
      case "File3":
        result.panels = [{id: "D"}];
        result.errors = ["Error D"];
        break;
      default:
        result.errors = ["Unknown"];
      }

      return $q.when(result);
    });
    
    mockPanelsService.updatePanels.and.callFake(function (panels) {
      return $q.when(updatePanelsReturnValue);
    });
    
    mockDatumsService.averageMeterSquareOfDatum.and.callFake(function () {
      
      var datumHolder = [];
      
      var datum1 = {
        name: "Education 1 Storey",
        panelAreaTotal: "168",
        panelAvg: "2",
        panelAvgOverride: "0",
        panelCount: "77"
      };
      
      datumHolder["Education 1 Storey"] = datum1;

      return $q.when(datumHolder);
    });

  }));

  describe("controller", function () {

    it("initialises", inject(function ($controller) {
      var controller = $controller("LoadPanelsController");

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
    }));

    it("loads panels from files", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1"]);
      scope.$apply();

      expect(mockLoadPanelsService.loadPanelSchedule.calls.count()).toEqual(1);
      expect(mockLoadPanelsService.loadPanelSchedule.calls.argsFor(0)).toEqual(["File1"]);

      expect(controller.files).toEqual(["File1"]);
      expect(controller.panels).toEqual([{id: "A"}]);
      expect(controller.errors).toEqual([]);

      controller.upload(["File2"]);
      scope.$apply();

      expect(mockLoadPanelsService.loadPanelSchedule.calls.count()).toEqual(2);
      expect(mockLoadPanelsService.loadPanelSchedule.calls.argsFor(1)).toEqual(["File2"]);

      expect(controller.files).toEqual(["File1", "File2"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "B"}, {id: "C"}]);
      expect(controller.errors).toEqual(["Error B", "Error C"]);
    }));

    it("loads multiple files", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1", "File2"]);
      scope.$apply();

      expect(mockLoadPanelsService.loadPanelSchedule.calls.count()).toEqual(2);
      expect(mockLoadPanelsService.loadPanelSchedule.calls.argsFor(0)).toEqual(["File1"]);
      expect(mockLoadPanelsService.loadPanelSchedule.calls.argsFor(1)).toEqual(["File2"]);

      expect(controller.files).toEqual(["File1", "File2"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "B"}, {id: "C"}]);
      expect(controller.errors).toEqual(["Error B", "Error C"]);
    }));

    it("can remove file", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1", "File2", "File3"]);
      scope.$apply();

      expect(controller.files).toEqual(["File1", "File2", "File3"]);
      expect(controller.panels).toEqual([{id: "A"},{id: "B"}, {id: "C"}, {id: "D"}]);
      expect(controller.errors).toEqual(["Error B", "Error C", "Error D"]);

      controller.removeFile(1);

      expect(controller.files).toEqual(["File1", "File3"]);
      expect(controller.panels).toEqual([{id: "A"}, {id: "D"}]);
      expect(controller.errors).toEqual(["Error D"]);

      controller.removeFile(0);

      expect(controller.files).toEqual(["File3"]);
      expect(controller.panels).toEqual([{id: "D"}]);
      expect(controller.errors).toEqual(["Error D"]);

      controller.removeFile(0);

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
    }));

    it("does not save when errors", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File2"]);
      scope.$apply();

      expect(controller.errors.length).not.toEqual(0);

      controller.save();

      expect(mockPanelsService.updatePanels).not.toHaveBeenCalled();
    }));

    it("can save uploaded panels", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1"]);
      scope.$apply();

      updatePanelsReturnValue = {
        total: 99,
        details: [
          {projectId: "123", projectName: "Name", count: 99}
        ]
      };

      controller.save();
      scope.$apply();

      expect(mockPanelsService.updatePanels).toHaveBeenCalledTimes(1);
      expect(mockPanelsService.updatePanels).toHaveBeenCalledWith([{id: "A"}]);
      
      expect(mockDatumsService.averageMeterSquareOfDatum).toHaveBeenCalledTimes(1);

      expect(controller.updateSummary).toEqual(updatePanelsReturnValue);
    }));

    it("reports failure to save", inject(function ($controller, $window, $q) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1"]);
      scope.$apply();

      mockPanelsService.updatePanels.and.returnValue($q.reject("Fail"));
      spyOn($window, "alert");

      controller.save();
      scope.$apply();

      expect($window.alert).toHaveBeenCalledWith("Fail");
    }));

    it("can reset file loading", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1", "File2", "File3"]);
      scope.$apply();

      expect(controller.files).toEqual(["File1", "File2", "File3"]);
      expect(controller.panels).toEqual([{id: "A"},{id: "B"}, {id: "C"}, {id: "D"}]);
      expect(controller.errors).toEqual(["Error B", "Error C", "Error D"]);

      controller.reset();

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
    }));

    it("can reset file after save", inject(function ($controller) {

      var controller = $controller("LoadPanelsController");

      controller.upload(["File1"]);
      scope.$apply();

      controller.save();
      scope.$apply();

      expect(controller.files).not.toEqual([]);
      expect(controller.panels).not.toEqual([]);
      expect(controller.updateSummary).not.toEqual(null);

      controller.reset();

      expect(controller.files).toEqual([]);
      expect(controller.panels).toEqual([]);
      expect(controller.errors).toEqual([]);
      expect(controller.updateSummary).toEqual(null);
    }));

  });

});
