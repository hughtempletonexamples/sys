"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var testUtils = require("../testUtils");

describe("areas service", function () {
  var firebaseRoot;

  var capacityRef;
  var capacitySubRef;
  var projectsRef;
  var areasRef;
  var panelsRef;
  var areaPlansRef;
  var mockPanelsService;
  var srdRef;
  var randomNumber;

  var mockSettingsService;
  var mockAuditService;
  var scope;
  var $q;
  var $timeout;
  var $log;
  var $firebaseArray;

  var plannerService;

  beforeEach(angular.mock.module("innView.manage", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    //note productionSubCapacities and productionCapacities have been flipped over to save dev time (Going through all code just to make a little change)
    capacityRef = firebaseRoot.child("productionSubCapacities");
    capacitySubRef = firebaseRoot.child("productionCapacities");
    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    srdRef = firebaseRoot.child("srd");

    testUtils.addQuerySupport(areaPlansRef);
    testUtils.addQuerySupport(panelsRef);

    mockSettingsService = jasmine.createSpyObj("settings", ["get"]);
    mockAuditService = jasmine.createSpyObj("audit", ["productionLine"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("settingsService", function () {
        return mockSettingsService;
      });
      $provide.factory("audit", function () {
        return mockAuditService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
    });

    testUtils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _$q_, _$timeout_, _$log_, _$firebaseArray_, _plannerService_) {
    scope = $rootScope;
    $q = _$q_;
    $timeout = _$timeout_;
    $log = _$log_;
    $firebaseArray = _$firebaseArray_;
    plannerService = _plannerService_;
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      if(areaRef == "AREA1"){
        return $firebaseArray(panelsRef);
      }else{
        panelsRef.set({});
        return $firebaseArray(panelsRef);
      }
    });
  }));

  beforeEach(function () {
    mockSettingsService.get.and.returnValue($q.when({designHandoverPeriod: 10}));
  });

  beforeEach(function () {
    srdRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "ExtF": { productionLine: "IFAST" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );
  
    srdRef.child("productionLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "IFAST": { defaultCapacity: 20 },
        "TF": { defaultCapacity: 35 },
        "CASS": { defaultCapacity: 6 }
      }
    );

    srdRef.child("productionSubLines").set(
      {
        "SIP": { defaultCapacity: 20 },
        "SIP2": { defaultCapacity: 20 },
        "HSIP": { defaultCapacity: 20 },
        "HSIP2": { defaultCapacity: 0 },
        "IFAST": { defaultCapacity: 20 },
        "IFAST2": { defaultCapacity: 0 },
        "TF": { defaultCapacity: 25 },
        "TF2": { defaultCapacity: 25 },
        "CASS": { defaultCapacity: 6 },
        "CASS2": { defaultCapacity: 0 }
      }
    );

    srdRef.child("suppliers").set(
      {
        "Innovaré": { thirdParty: false },
        "Other": { thirdParty: true }
      }
    );
  });

  beforeEach(function () {
    jasmine.clock().install();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  describe("setCapacity", function () {

    it("sets production capacity", function () {

      plannerService.setCapacity("SIP", "2016-04-01", 11);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-04-02", 22);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-04-03", 33);
      scope.$apply();

      expect(capacityRef.getData()).toEqual(
        {
          SIP: {"2016-04-01": 11},
          TF: {"2016-04-02": 22},
          CASS: {"2016-04-03": 33}
        }
      );
    });
    
    it("sets production capacity and total", function () {

      plannerService.setCapacity("SIP", "2016-04-01", 11);
      scope.$apply();
      plannerService.setCapacity("SIP2", "2016-04-01", 11);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-04-02", 22);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-04-03", 33);
      scope.$apply();
      plannerService.setCapacity("IFAST", "2016-04-03", 13);
      scope.$apply();

      expect(capacityRef.getData()).toEqual(
        {
          SIP: {"2016-04-01": 11},
          IFAST: {"2016-04-03": 13},
          SIP2: {"2016-04-01": 11},
          TF: {"2016-04-02": 22},
          CASS: {"2016-04-03": 33}
        }
      );
    
      expect(capacitySubRef.getData()).toEqual(
        {
          SIP: {"2016-04-01": 22},
          IFAST: {"2016-04-03": 13},
          TF: {"2016-04-02": 22},
          CASS: {"2016-04-03": 33}
        }
      );
    });

    it("updates production capacity", function () {
      capacityRef.set(
        {
          SIP: {"2016-04-01": 10},
          TF: {"2016-04-02": 10},
          CASS: {"2016-04-03": 10},
          IFAST: {"2016-04-03": 10}
        }
      );

      plannerService.setCapacity("SIP", "2016-04-01", 11);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-04-02", 12);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-04-03", 13);
      scope.$apply();
      plannerService.setCapacity("IFAST", "2016-04-03", 14);
      scope.$apply();

      expect(capacityRef.getData()).toEqual(
        {
          SIP: {"2016-04-01": 11},
          TF: {"2016-04-02": 12},
          CASS: {"2016-04-03": 13},
          IFAST: {"2016-04-03": 14}
        }
      );
    
    });

    it("removes production capacity entry if null value passed", function () {
      capacityRef.set(
        {
          SIP: {
            "2016-07-01": 10,
            "2016-07-02": 10
          },
          TF: {
            "2016-07-01": 10,
            "2016-07-02": 10
          },
          CASS: {
            "2016-07-01": 10,
            "2016-07-02": 10
          },
          IFAST: {
            "2016-07-01": 10,
            "2016-07-02": 10
          }
        }
      );

      plannerService.setCapacity("SIP", "2016-07-01", null);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-07-01", null);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-07-01", null);
      scope.$apply();
      plannerService.setCapacity("IFAST", "2016-07-01", null);
      scope.$apply();
      expect(capacityRef.getData()).toEqual(
        {
          SIP: {
            "2016-07-02": 10
          },
          TF: {
            "2016-07-02": 10
          },
          CASS: {
            "2016-07-02": 10
          },
          IFAST: {
            "2016-07-02": 10
          }
        }
      );
    });

    it("removes production capacity entry if same as default", function () {
    
      capacitySubRef.set(
        {
          SIP: {
            "2016-07-01": 10,
            "2016-07-02": 10,
            "2016-07-03": 10
          },
          TF: {
            "2016-07-01": 10,
            "2016-07-02": 10,
            "2016-07-03": 10
          },
          CASS: {
            "2016-07-01": 10,
            "2016-07-02": 10,
            "2016-07-03": 10
          }
        }
      );

      plannerService.setCapacity("SIP", "2016-07-01", 20);
      scope.$apply();
      plannerService.setCapacity("SIP", "2016-07-02", 0);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-07-01", 35);
      scope.$apply();
      plannerService.setCapacity("TF", "2016-07-02", 0);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-07-01", 6);
      scope.$apply();
      plannerService.setCapacity("CASS", "2016-07-02", 0);
      scope.$apply();

      expect(capacitySubRef.getData()).toEqual(
        {
          SIP: {
            "2016-07-03": 10
          },
          TF: {
            "2016-07-03": 10
          },
          CASS: {
            "2016-07-03": 10
          }
        }
      );
    });

    it("audits production capacity change", function () {

      plannerService.setCapacity("SIP", "2016-07-01", 10);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-01 set to 10", "SIP"]);

      plannerService.setCapacity("SIP", "2016-07-01", 0);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-01 set to 0", "SIP"]);


      plannerService.setCapacity("IFAST", "2016-07-01", 7);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-01 set to 7", "IFAST"]);

      plannerService.setCapacity("SIP", "2016-07-01", 20);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-01 reset to default", "SIP"]);


      plannerService.setCapacity("TF", "2016-07-02", 20);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-02 set to 20", "TF"]);

      plannerService.setCapacity("TF", "2016-07-02", 0);
      scope.$apply();

      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-02 reset to default", "TF"]);

      plannerService.setCapacity("TF", "2016-07-02", null);
      scope.$apply();
      expect(mockAuditService.productionLine.calls.mostRecent().args).toEqual(["Capacity on 2016-07-02 reset to default", "TF"]);

      expect(mockAuditService.productionLine.calls.count()).toEqual(7);
    });

  });

  describe("getForwardPlan", function () {

    beforeEach(function () {
      projectsRef.set(
        {
          "PROJ1": {
            "deliverySchedule": {
              "published": "PROJ1REV1",
              "unpublished": "PROJ1REV2"
            },
            "active": true
          },
          "PROJ2": {
            "deliverySchedule": {
              "published": "PROJ2REV1",
              "unpublished": "PROJ2REV2"
            },
            "active": true
          }
        }
      );
    });

    it("defaults to starting on Monday of current week", function () {

      var dates = {
        "2016-05-30": 0,
        "2016-05-31": 1,
        "2016-06-01": 2,
        "2016-06-02": 3,
        "2016-06-03": 4,
        "2016-06-04": 5,
        "2016-06-05": 6
      };
      angular.forEach(dates, function (expectedCurrent, date) {
        jasmine.clock().mockDate(new Date(date));
        var result = plannerService.getForwardPlan(null);

        expect(result.current).toEqual(expectedCurrent);
        expect(result.viewStart).toEqual("2016-05-30");
      });

    });

    it("starts on Monday of week for specified date", function () {

      var dates = [
        "2016-05-30",
        "2016-05-31",
        "2016-06-01",
        "2016-06-02",
        "2016-06-03",
        "2016-06-04",
        "2016-06-05"
      ];
      jasmine.clock().mockDate(new Date("2016-05-29"));

      angular.forEach(dates, function (date) {
        var result = plannerService.getForwardPlan(date);

        expect(result.current).toEqual(-1);
        expect(result.viewStart).toEqual("2016-05-30");
      });

    });

    it("starts on Monday of current week if specified date in past", function () {

      jasmine.clock().mockDate(new Date("2016-05-30"));

      var result = plannerService.getForwardPlan("2016-05-29");

      expect(result.current).toEqual(0);
      expect(result.viewStart).toEqual("2016-05-30");

    });

    it("has default capacity", function () {
      var result = plannerService.getForwardPlan(null);
      scope.$apply();

      expect(result.capacity).toEqual(
        {
          "SIP" : [ 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null ],
          "SIP2" : [ 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null ],
          "HSIP" : [ 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null ],
          "IFAST" : [ 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null ],
          "IFAST2" : [ 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null ],
          "HSIP2" : [ 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null ],
          "TF" : [ 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null ],
          "TF2" : [ 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null ],
          "CASS" : [ 6, 6, 6, 6, 6, null, null, 6, 6, 6, 6, 6, null, null, 6, 6, 6, 6, 6, null, null ],
          "CASS2" : [ 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null ]
        }
      );
    });

    it("uses capacity set for specific days", function () {

      jasmine.clock().mockDate(new Date("2016-05-09"));

      plannerService.setCapacity("SIP", "2016-05-09", 11);
      plannerService.setCapacity("SIP2", "2016-05-09", 11);
      plannerService.setCapacity("TF", "2016-05-10", 22);
      plannerService.setCapacity("CASS", "2016-05-11", 33);
      plannerService.setCapacity("IFAST2", "2016-05-09", 11);
      plannerService.setCapacity("SIP", "2016-05-29", 2);
      plannerService.setCapacity("SIP2", "2016-05-29", 2);
      plannerService.setCapacity("TF", "2016-05-29", 3);
      plannerService.setCapacity("CASS", "2016-05-29", 4);

      var result = plannerService.getForwardPlan(null);
      scope.$apply();

      expect(result.capacity).toEqual(
        {
          "CASS": [6, 6, 33, 6, 6, null, null, 6, 6, 6, 6, 6, null, null, 6, 6, 6, 6, 6, null, 4],
          "CASS2": [0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null],
          "HSIP": [20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null],
          "HSIP2": [0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null],
          "IFAST" : [ 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null ],
          "IFAST2" : [ 11, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null, 0, 0, 0, 0, 0, null, null ],
          "SIP": [11, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, 2],
          "SIP2": [11, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, 2],
          "TF": [25, 22, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, 3],
          "TF2": [25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null]
        }
      );
    });

    describe("includes areas", function () {

      it("orders areas by delivery date", function () {

        jasmine.clock().mockDate(new Date("2016-08-01"));

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-13"
              },
              "supplier": "Innovaré"
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-15"
              },
              "supplier": "Innovaré"
            },
            "AREA3": {
              "project": "PROJ2",
              "type": "Ext",
              "revisions": {
                "PROJ2REV1": "2016-08-14"
              },
              "supplier": "Innovaré"
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1", "AREA3", "AREA2"]);
      });

      it("orders areas by unpublished delivery date if requested", function () {
        jasmine.clock().mockDate(new Date("2016-08-01"));

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-13",
                "PROJ1REV2": "2016-08-12"
              },
              "supplier": "Innovaré"
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-15",
                "PROJ1REV2": "2016-08-10"
              },
              "supplier": "Innovaré"
            },
            "AREA3": {
              "project": "PROJ2",
              "type": "Ext",
              "revisions": {
                "PROJ2REV1": "2016-08-14",
                "PROJ2REV2": "2016-08-09"
              },
              "supplier": "Innovaré"
            }
          }
        );

        var includeUnpublishedAreas = {
          "AREA1": true,
          "AREA2": true,
          "AREA3": false
        };

        var result = plannerService.getForwardPlan(null, includeUnpublishedAreas);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA2", "AREA1", "AREA3"]);
      });

      it("includes areas that only have unpublished revision but with no entries in plan", function () {
        jasmine.clock().mockDate(new Date("2016-08-01"));

        projectsRef.child("PROJ1/id").set("837");
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "phase": "P2",
              "floor": "GF",
              "type": "Ext",
              "revisions": {
                "PROJ1REV2": "2016-08-13"
              },
              "supplier": "Innovaré",
              "estimatedPanels": 20
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0]._id).toEqual("837-P2-GF-Ext");
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([null]), "AREA1 plan");
        expect(result.areas[0]._productionStartDate).toEqual(undefined, "AREA1._production start date");
        expect(result.areas[0]._productionEndDate).toEqual(undefined, "AREA1._production end date");
      });

      it("includes areas that only have unpublished revision in plan when requested", function () {
        jasmine.clock().mockDate(new Date("2016-08-01"));

        projectsRef.child("PROJ1/id").set("837");
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "phase": "P2",
              "floor": "GF",
              "type": "Ext",
              "revisions": {
                "PROJ1REV2": "2016-08-13"
              },
              "supplier": "Innovaré",
              "estimatedPanels": 20
            }
          }
        );

        var result = plannerService.getForwardPlan(null, {"AREA1": true});
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0]._id).toEqual("837-P2-GF-Ext");
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20]), "AREA1 plan");
        expect(result.areas[0]._productionStartDate).toEqual("2016-08-01", "AREA1._production start date");
        expect(result.areas[0]._productionEndDate).toEqual("2016-08-01", "AREA1._production end date");
      });

      it("excludes completed areas", function () {
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "actualPanels": 800,
              "completedPanels": 800,
              "revisions": {
                "PROJ1REV1": "2016-08-13"
              },
              "supplier": "Innovaré"
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas).toEqual([]);
      });

      it("excludes third party areas", function () {

        jasmine.clock().mockDate(new Date("2016-08-01"));

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-13"
              },
              "supplier": "Innovaré"
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-08-15"
              },
              "supplier": "Other"
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1"]);
      });
    });

    describe("includes areas", function () {
      var now = new Date("2016-02-01");
      beforeEach(function () {
        jasmine.clock().mockDate(now);

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "revisions": {
                "PROJ1REV1": "2016-02-10"
              }
            },
            "AREA2": {
              "project": "PROJ1",
              "revisions": {
                "PROJ1REV1": "2016-02-11"
              }
            },
            "AREA3": {
              "project": "PROJ1",
              "revisions": {
                "PROJ1REV1": "2016-02-12"
              }
            }
          }
        );
      });

      it("has no production plan for areas without panel count", function () {
        areasRef.child("AREA1").update({"type": "Ext"});
        areasRef.child("AREA2").update({"type": "Int"});
        areasRef.child("AREA3").update({"type": "Roof"});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        angular.forEach(result.areas, function (area) {
          expect(area._productionPlan).toEqual(expectedPlan([0]), area.$id);
          expect(area._productionStartDate).toEqual("2016-02-01", area.$id);
          expect(area._productionEndDate).toEqual(null, area.$id);
        });

      });

      it("derives production plan based on capacity", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 35});
        areasRef.child("AREA2").update({"type": "Ext", "actualPanels": 45});
        areasRef.child("AREA3").update({"type": "Ext", "actualPanels": 20});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20, 15]), "AREA1 plan");
        expect(result.areas[0]._productionStartDate).toEqual("2016-02-01", "AREA1._production start date");
        expect(result.areas[0]._productionEndDate).toEqual("2016-02-02", "AREA1._production end date");

        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([null, 5, 20, 20]), "AREA2 plan");
        expect(result.areas[1]._productionStartDate).toEqual("2016-02-02", "AREA2._production start date");
        expect(result.areas[1]._productionEndDate).toEqual("2016-02-04", "AREA2._production end date");

        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([null, null, null, null, 20]), "AREA3 plan");
        expect(result.areas[2]._productionStartDate).toEqual("2016-02-05", "AREA3._production start date");
        expect(result.areas[2]._productionEndDate).toEqual("2016-02-05", "AREA3._production end date");
      });

      it("derives production plan starting from design handover date", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 1});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 1});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 1});

        jasmine.clock().mockDate(new Date("2016-01-01"));
        var result = plannerService.getForwardPlan("2016-01-11");
        scope.$apply();

        expect(result.areas.length).toBe(3);
        expect(result.areas[0]._productionStartDate).toEqual("2016-01-27", "AREA1._production start date");
        expect(result.areas[1]._productionStartDate).toEqual("2016-01-28", "AREA2._production start date");
        expect(result.areas[2]._productionStartDate).toEqual("2016-01-29", "AREA3._production start date");
      });

      it("assigns production in order of delivery date", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-02-15"}});
        areasRef.child("AREA2").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-02-13"}});
        areasRef.child("AREA3").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-02-14"}});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA2", "AREA3", "AREA1"]);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20]), "AREA2 plan");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([null, 20]), "AREA3 plan");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([null, null, 20]), "AREA1 plan");
      });

      it("assigns production in order of unpublished delivery date if requested", function () {
        jasmine.clock().mockDate(new Date("2016-08-01"));

        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-08-15", "PROJ1REV2": "2016-08-10"}});
        areasRef.child("AREA2").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-08-13"}});
        areasRef.child("AREA3").update({"type": "Ext", "actualPanels": 20, "revisions": { "PROJ1REV1": "2016-08-14", "PROJ1REV2": "2016-08-12"}});

        // initial rendering of forward plan, to mimic initial page load, prior to selection of unpublished areas
        plannerService.getForwardPlan(null);
        scope.$apply();

        var includeUnpublishedAreas = {
          "AREA1": true,
          "AREA3": true
        };

        var result = plannerService.getForwardPlan(null, includeUnpublishedAreas);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1", "AREA3", "AREA2"]);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20]), "AREA1 plan");
        expect(result.areas[0]._productionStartDate).toEqual("2016-08-01", "AREA1 productionStartDate");

        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([null, 20]), "AREA3 plan");
        expect(result.areas[1]._productionStartDate).toEqual("2016-08-02", "AREA3 productionStartDate");

        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([null, null, 20]), "AREA2 plan");
        expect(result.areas[2]._productionStartDate).toEqual("2016-08-03", "AREA2 productionStartDate");
      });

      it("does not schedule production plan at weekend", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 120});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 210});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 36});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20, 20, 20, 20, 20, null, null, 20]), "AREA1 plan");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 25, 25, 25, 25, null, null, 25, 25, 25, 10]), "AREA2 plan");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([6, 6, 6, 6, 6, null, null, 6]), "AREA3 plan");
      });

      it("derives production plan across panel types", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 40});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 40});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 20});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20, 20]));
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 15]));
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([6, 6, 6, 2]));
      });

      it("deducts panels complete from today's capacity", function () {
        
        var systemTimestamp = now.getTime();
        var createdTimestamp = systemTimestamp - 86543210;

        var startedTimestamp = systemTimestamp;
        var completedTimestamp = systemTimestamp;
          
        var panel1 = {
          id: "panel1",
          area: "AREA1",
          qa: {
            started: startedTimestamp,
            completed: completedTimestamp
          },
          additionalInfo: {
            framingStyle: "SIP 214",
            sheathing: "OSB 5980"
          },
          timestamps: {
            created: createdTimestamp,
            modified: completedTimestamp
          }
        };

        var panel2 = {
          id: "panel2",
          area: "AREA1",
          qa: {
            started: startedTimestamp,
            completed: completedTimestamp
          },
          additionalInfo: {
            framingStyle: "SIP 214",
            sheathing: "OSB 5980"
          },
          timestamps: {
            created: createdTimestamp,
            modified: completedTimestamp
          }
        };

        var panel3 = {
          id: "panel3",
          area: "AREA1",
          additionalInfo: {
            framingStyle: "SIP 214",
            sheathing: "OSB 5980"
          },
          timestamps: {
            created: createdTimestamp,
            modified: completedTimestamp
          }
        };

        panelsRef.set({
          "PROJ1AREA1PANEL1": panel1,
          "PROJ1AREA1PANEL2": panel2,
          "PROJ1AREA1PANEL3": panel3
        });
        
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 23, "estimatedPanels": 5, "completedPanels": 2});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 40, "estimatedPanels": 5, "completedPanels": 0});

        var result = plannerService.getForwardPlan(null);

        scope.$apply();
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([18, 3]));
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 15]));
      });

      
      it("uses updated capacity for plan production plan at weekend", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 120});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 210});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 36});

        jasmine.clock().mockDate(new Date("2016-05-02"));

        plannerService.setCapacity("SIP", "2016-05-02", 0);
        plannerService.setCapacity("SIP", "2016-05-06", 15);
        plannerService.setCapacity("SIP", "2016-05-07", 6);
        plannerService.setCapacity("TF", "2016-05-02", 0);
        plannerService.setCapacity("TF", "2016-05-06", 30);
        plannerService.setCapacity("TF", "2016-05-07", 7);
        plannerService.setCapacity("CASS", "2016-05-02", 0);
        plannerService.setCapacity("CASS", "2016-05-06", 3);
        plannerService.setCapacity("CASS", "2016-05-07", 2);
        scope.$apply();

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([null, 20, 20, 20, 15, 6, null, 20, 19]), "AREA1 plan");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([null, 25, 25, 25, 30, 7, null, 25, 25, 25, 23]), "AREA2 plan");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([null, 6, 6, 6, 3, 2, null, 6, 6, 1]), "AREA3 plan");

        expect(result.areas[0]._productionStartDate).toEqual("2016-05-03");
        expect(result.areas[1]._productionStartDate).toEqual("2016-05-03");
        expect(result.areas[2]._productionStartDate).toEqual("2016-05-03");
      });

      it("uses estimated panel count", function () {
        areasRef.child("AREA1").update({"type": "Ext", "estimatedPanels": 1});
        areasRef.child("AREA2").update({"type": "Int", "estimatedPanels": 1});
        areasRef.child("AREA3").update({"type": "Roof", "estimatedPanels": 1});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        angular.forEach(result.areas, function (area) {
          expect(area._productionPlan).toEqual(expectedPlan([1]), area.$id);
          expect(area._productionStartDate).toEqual("2016-02-01", area.$id);
          expect(area._productionEndDate).toEqual("2016-02-01", area.$id);
        });

      });

      it("derives production end beyond plan display", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 301});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 526});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 91});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null, 20, 20, 20, 20, 20, null, null]));
        expect(result.areas[0]._productionEndDate).toEqual("2016-02-22");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null]));
        expect(result.areas[1]._productionEndDate).toEqual("2016-03-01");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([6, 6, 6, 6, 6, null, null, 6, 6, 6, 6, 6, null, null, 6, 6, 6, 6, 6, null, null]));
        expect(result.areas[2]._productionEndDate).toEqual("2016-02-22");
      });

      it("excludes areas for types without production line", function () {
        areasRef.child("AREA1").update({"type": "Anc", "estimatedPanels": 1});
        areasRef.child("AREA2").update({"type": "P&B", "estimatedPanels": 1});
        areasRef.child("AREA3").update({"type": "Garbage", "estimatedPanels": 1});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(0);
      });

      it("excludes areas starting beyond plan display", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 300});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 525});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 95});
        areasRef.update(
          {
            "AREA4": {
              "project": "PROJ2",
              "type": "Ext",
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA5": {
              "project": "PROJ2",
              "type": "Int",
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA6": {
              "project": "PROJ2",
              "type": "Roof",
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        expect(result.areas[0].$id).toEqual("AREA1");
        expect(result.areas[1].$id).toEqual("AREA2");
        expect(result.areas[2].$id).toEqual("AREA3");
      });

      it("excludes areas with design handover period starting beyond plan display", function () {
        jasmine.clock().mockDate(new Date("2016-01-01"));

        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 10});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 10});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 10});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(0);
      });

      it("derives production plan for future date", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 301});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 526});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 91});

        var result = plannerService.getForwardPlan("2016-02-15");
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1", "AREA2", "AREA3"]);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20, 20, 20, 20, 20, null, null, 1]));
        expect(result.areas[0]._productionStartDate).toEqual("2016-02-01");
        expect(result.areas[0]._productionEndDate).toEqual("2016-02-22");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 25, 25, 25, 25, null, null, 25, 25, 25, 25, 25, null, null, 25, 1]));
        expect(result.areas[1]._productionStartDate).toEqual("2016-02-01");
        expect(result.areas[1]._productionEndDate).toEqual("2016-03-01");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([6, 6, 6, 6, 6, null, null, 1]));
        expect(result.areas[2]._productionStartDate).toEqual("2016-02-01");
        expect(result.areas[2]._productionEndDate).toEqual("2016-02-22");
      });

      it("excludes areas where production end before future date", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 300});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 525});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 90});
        areasRef.update(
          {
            "AREA4": {
              "project": "PROJ2",
              "type": "Ext",
              "actualPanels": 20,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA5": {
              "project": "PROJ2",
              "type": "Int",
              "actualPanels": 35,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA6": {
              "project": "PROJ2",
              "type": "Roof",
              "actualPanels": 6,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan("2016-02-22");
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA2", "AREA4", "AREA5", "AREA6"]);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([25, 25, 25, 25, 25, null, null, 25]));
        expect(result.areas[0]._productionStartDate).toEqual("2016-02-01");
        expect(result.areas[0]._productionEndDate).toEqual("2016-02-29");
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([20]));
        expect(result.areas[1]._productionStartDate).toEqual("2016-02-22");
        expect(result.areas[1]._productionEndDate).toEqual("2016-02-22");
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([null, null, null, null, null, null, null, null, 25, 10]));
        expect(result.areas[2]._productionStartDate).toEqual("2016-03-01");
        expect(result.areas[2]._productionEndDate).toEqual("2016-03-02");
      });

      it("excludes areas where no production end, and production start before future date", function () {
        areasRef.child("AREA1").update({"type": "Ext"});
        areasRef.child("AREA2").update({"type": "Int"});
        areasRef.child("AREA3").update({"type": "Roof"});
        areasRef.update(
          {
            "AREA4": {
              "project": "PROJ2",
              "type": "Ext",
              "actualPanels": 200,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA5": {
              "project": "PROJ2",
              "type": "Int",
              "actualPanels": 350,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            },
            "AREA6": {
              "project": "PROJ2",
              "type": "Roof",
              "actualPanels": 60,
              "revisions": {
                "PROJ2REV1": "2016-02-13"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan("2016-02-08");
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA4", "AREA5", "AREA6"]);
      });

      it("subtracts completed panel count", function () {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 3, "completedPanels": 2});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 3, "completedPanels": 2});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 3, "completedPanels": 2});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        angular.forEach(result.areas, function (area) {
          expect(area._productionPlan).toEqual(expectedPlan([1]), area.$id);
          expect(area._productionStartDate).toEqual("2016-02-01", area.$id);
          expect(area._productionEndDate).toEqual("2016-02-01", area.$id);
        });

      });

      it("starts plan from current day", function () {
        jasmine.clock().mockDate(new Date("2016-02-04"));
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 1});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 1});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 1});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        angular.forEach(result.areas, function (area) {
          expect(area._productionPlan).toEqual(expectedPlan([null, null, null, 1]), area.$id);
          expect(area._productionStartDate).toEqual("2016-02-04", area.$id);
          expect(area._productionEndDate).toEqual("2016-02-04", area.$id);
        });

      });

      it("updates schedule dates dynamically when capacity changed", inject(function ($timeout) {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20});
        areasRef.child("AREA2").update({"type": "Int", "actualPanels": 35});
        areasRef.child("AREA3").update({"type": "Roof", "actualPanels": 6});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(3);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([20]));
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([25, 10]));
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([6]));

        plannerService.setCapacity("SIP", "2016-02-01", 19);
        plannerService.setCapacity("TF", "2016-02-01", 34);
        plannerService.setCapacity("CASS", "2016-02-01", 5);
        scope.$apply();
        $timeout.flush();

        expect(result.areas.length).toEqual(3);
        expect(result.areas[0]._productionPlan).toEqual(expectedPlan([19, 1]));
        expect(result.areas[1]._productionPlan).toEqual(expectedPlan([34, 1]));
        expect(result.areas[2]._productionPlan).toEqual(expectedPlan([5, 1]));
      }));

      it("dynamic updates cease when plan destroyed", inject(function ($timeout) {
        areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20});

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(1);

        result.$destroy();
        scope.$apply();

        expect(result).toEqual({});

        plannerService.setCapacity("SIP", "2016-02-01", 0);
        scope.$apply();
        $timeout.flush();

        expect(result).toEqual({});
      }));

    });

    describe("derives schedule dates for areas", function () {

      it("allowing for weekends", function () {
        var handover_fri = "2016-03-04";
        var handover_mon = "2016-03-07";
        var handover_tue = "2016-03-08";
        var wed = "2016-03-16";
        var thu = "2016-03-17";
        var fri = "2016-03-18";
        var mon = "2016-03-21";
        var tue = "2016-03-22";

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": fri
              }
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": mon
              }
            },
            "AREA3": {
              "project": "PROJ2",
              "type": "Ext",
              "revisions": {
                "PROJ2REV1": tue
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1", "AREA2", "AREA3"]);
        expect(result.areas[0])
          .toEqual(jasmine.objectContaining({
            $id: "AREA1",
            _deliveryDate: fri,
            _dispatchDate: thu,
            _riskDate: wed,
            _designHandoverDate: handover_fri
          }));
        expect(result.areas[1])
          .toEqual(jasmine.objectContaining({
            $id: "AREA2",
            _deliveryDate: mon,
            _dispatchDate: fri,
            _riskDate: thu,
            _designHandoverDate: handover_mon
          }));
        expect(result.areas[2])
          .toEqual(jasmine.objectContaining({
            $id: "AREA3",
            _deliveryDate: tue,
            _dispatchDate: mon,
            _riskDate: fri,
            _designHandoverDate: handover_tue
          }));
      });

      it("Derives schedule dates across month boundaries", function () {
        var handover_tue = "2016-02-16";
        var fri = "2016-02-26";
        var mon = "2016-02-29";
        var tue = "2016-03-01";

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": tue
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0])
          .toEqual(jasmine.objectContaining({
            $id: "AREA1",
            _deliveryDate: tue,
            _dispatchDate: mon,
            _riskDate: fri,
            _designHandoverDate: handover_tue
          }));
      });

      it("Derives schedule dates across year boundaries", function () {
        jasmine.clock().mockDate(new Date("2016-12-20"));

        var handover_tue = "2016-12-20";
        var fri = "2016-12-30";
        var mon = "2017-01-02";
        var tue = "2017-01-03";

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": tue
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0])
          .toEqual(jasmine.objectContaining({
            $id: "AREA1",
            _deliveryDate: tue,
            _dispatchDate: mon,
            _riskDate: fri,
            _designHandoverDate: handover_tue
          }));
      });

      it("Used settings design handover period", function () {

        mockSettingsService.get.and.returnValue($q.when({
          designHandoverPeriod: 15
        }));

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-05-25",
                "PROJ1REV2": "2016-05-31"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(1);
        expect(result.areas[0])
          .toEqual(jasmine.objectContaining({
            $id: "AREA1",
            _designHandoverDate: "2016-05-04",
            _unpublishedDesignHandoverDate: "2016-05-10"
          }));
      });

      it("updates schedule dates dynamically when projects updated", inject(function ($timeout) {
        var revisionDeliveryDate1 = "2016-02-01";
        var revisionDeliveryDate2 = "2016-03-01";

        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": revisionDeliveryDate1,
                "PROJ2REV2": revisionDeliveryDate2
              }
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": revisionDeliveryDate2,
                "PROJ1REV2": revisionDeliveryDate1
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA1", "AREA2"]);
        expect(result.areas[0]._deliveryDate).toEqual(revisionDeliveryDate1);
        expect(result.areas[1]._deliveryDate).toEqual(revisionDeliveryDate2);

        // Publish new revision with updated dates
        areasRef.child("AREA1/revisions").update(
          {
            "PROJ1REV2": revisionDeliveryDate2
          }
        );
        areasRef.child("AREA2/revisions").update(
          {
            "PROJ1REV2": revisionDeliveryDate1
          }
        );
        projectsRef.child("PROJ1/deliverySchedule/published").set("PROJ1REV2");

        scope.$apply();
        $timeout.flush();

        expect(result.areas.map(function (area) { return area.$id; })).toEqual(["AREA2", "AREA1"]);
        expect(result.areas[0]._deliveryDate).toEqual(revisionDeliveryDate1);
        expect(result.areas[1]._deliveryDate).toEqual(revisionDeliveryDate2);
      }));

    });

    describe("decorates areas", function () {

      it("with id", function () {
        projectsRef.child("PROJ1/id").set("837");
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "phase": "P2",
              "floor": "GF",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-02-13"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { return area._id; })).toEqual(["837-P2-GF-Ext"]);
      });

      it("with unpublished dates", function () {
        projectsRef.child("PROJ1/deliverySchedule/unpublished").set("PROJ1REV2");
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-02-13",
                "PROJ1REV2": "2016-02-18"
              }
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Int",
              "revisions": {
                "PROJ1REV1": "2016-02-13",
                "PROJ1REV2": "2016-02-19"
              }
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.map(function (area) { var obj = {}; obj[area.$id] = area._unpublishedDeliveryDate; return obj; }))
          .toEqual([
            {"AREA1": "2016-02-18"},
            {"AREA2": "2016-02-19"}
          ]);
        expect(result.areas.map(function (area) { var obj = {}; obj[area.$id] = area._unpublishedDesignHandoverDate; return obj; }))
          .toEqual([
            {"AREA1": "2016-02-04"},
            {"AREA2": "2016-02-05"}
          ]);
      });

      it("with panel type", function () {
        projectsRef.child("PROJ1/id").set("837");
        areasRef.set(
          {
            "AREA1": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-02-13"
              }
            },
            "AREA2": {
              "project": "PROJ1",
              "type": "Int",
              "revisions": {
                "PROJ1REV1": "2016-02-13"
              }
            },
            "AREA3": {
              "project": "PROJ1",
              "type": "Roof",
              "revisions": {
                "PROJ1REV1": "2016-02-13"
              }
            },
            "AREA4": {
              "project": "PROJ1",
              "type": "Ext",
              "revisions": {
                "PROJ1REV1": "2016-02-13"
              },
              "line": 2
            }
          }
        );

        var result = plannerService.getForwardPlan(null);
        scope.$apply();

        expect(result.areas.length).toEqual(4);
        expect(result.areas.map(function (area) { var obj = {}; obj[area.$id] = area._productionLine; return obj; }))
          .toEqual([
            { AREA1: "SIP" }, 
            { AREA4: "SIP2" }, 
            { AREA2: "TF" }, 
            { AREA3: "CASS" } 
          ]);
      });
    });
  });

  describe("updates production plans", function () {
    var now = new Date("2016-08-01T12:34:56");

    beforeEach(function () {
      jasmine.clock().mockDate(now);

      projectsRef.set(
        {
          "PROJ1": {
            "deliverySchedule": {
              "published": "PROJ1REV1"
            },
            "active": true
          },
          "PROJ2": {
            "deliverySchedule": {
              "published": "PROJ2REV1"
            },
            "active": true
          }
        }
      );

      areasRef.set(
        {
          "AREA1": {
            "project": "PROJ1",
            "revisions": {
              "PROJ1REV1": "2016-08-08"
            }
          },
          "AREA2": {
            "project": "PROJ2",
            "revisions": {
              "PROJ2REV1": "2016-08-09"
            }
          }
        }
      );
    });

    it("stores production plan data", function () {
      areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 35});

      plannerService.refreshProductionPlan();
      scope.$apply();
      $timeout.flush();

      expect(areaPlansRef.child("AREA1").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: now.getTime(), modified: now.getTime()},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-02",
        riskDate: "2016-08-04"
      }));

    });

    it("stores production plan beyond 3 weeks", function () {
      areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 300});
      areasRef.child("AREA2").update({"type": "Ext", "actualPanels": 20});

      plannerService.refreshProductionPlan();
      scope.$apply();
      $timeout.flush();

      expect(areaPlansRef.child("AREA1").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: now.getTime(), modified: now.getTime()},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-19",
        riskDate: "2016-08-04"
      }));

      expect(areaPlansRef.child("AREA2").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: now.getTime(), modified: now.getTime()},
        project: "PROJ2",
        plannedStart: "2016-08-22",
        plannedFinish: "2016-08-22",
        riskDate: "2016-08-05"
      }));

    });

    it("updates timestamp if values change", function () {
      areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20});

      areaPlansRef.child("AREA1").set({
        timestamps: {created: 1234567890000, modified: 1234567890000},
        project: "PROJ1",
        plannedStart: "2016-07-01",
        plannedFinish: "2016-07-01",
        riskDate: "2016-08-04"
      });

      plannerService.refreshProductionPlan();
      scope.$apply();
      $timeout.flush();

      expect(areaPlansRef.child("AREA1").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: 1234567890000, modified: now.getTime()},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-01",
        riskDate: "2016-08-04"
      }));

    });

    it("leaves timestamp if no values change", function () {
      areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 20});

      areaPlansRef.child("AREA1").set({
        timestamps: {created: 1234567890000, modified: 1234567890000},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-01",
        riskDate: "2016-08-04"
      });

      plannerService.refreshProductionPlan();
      scope.$apply();
      $timeout.flush();

      expect(areaPlansRef.child("AREA1").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: 1234567890000, modified: 1234567890000},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-01",
        riskDate: "2016-08-04"
      }));

    });

    it("trigger production plan update after setCapacity", function () {
      areasRef.child("AREA1").update({"type": "Ext", "actualPanels": 35});

      plannerService.setCapacity("SIP", "2016-08-01", 35);
      scope.$apply();
      $timeout.flush();

      expect(areaPlansRef.child("AREA1").getData()).toEqual(jasmine.objectContaining({
        timestamps: {created: now.getTime(), modified: now.getTime()},
        project: "PROJ1",
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-01",
        riskDate: "2016-08-04"
      }));

    });
  });

  describe("gets production plans", function () {

    beforeEach(function () {

      areaPlansRef.set({
        "AREA1": {
          project: "PROJ1",
          plannedStart: "2016-08-01"
        },
        "AREA2": {
          project: "PROJ2",
          plannedStart: "2016-08-01"
        }
      });
    });

    it("for project areas", function () {
      var plans = plannerService.getProjectAreaProductionPlans("PROJ1");
      scope.$apply();

      expect(plans.length).toBe(1);
      expect(plans[0]).toEqual(jasmine.objectContaining({
        $id: "AREA1",
        project: "PROJ1",
        plannedStart: "2016-08-01"
      }));
    });

  });

  describe("getProductionPlansByPlannedStart", function () {
    beforeEach(function () {

      areaPlansRef.set({
        "AREA1": {
          project: "PROJ1",
          plannedStart: "2016-08-03"
        },
        "AREA2": {
          project: "PROJ2",
          plannedStart: "2016-08-01"
        }
      });
    });

    it("returns production plans ordered by planned start date", function () {
      var plans = plannerService.getProductionPlansByPlannedStart();
      scope.$apply();

      expect(plans.length).toBe(2);
      expect(plans[0]).toEqual(jasmine.objectContaining({
        $id: "AREA2",
        project: "PROJ2",
        plannedStart: "2016-08-01"
      }));
      expect(plans[1]).toEqual(jasmine.objectContaining({
        $id: "AREA1",
        project: "PROJ1",
        plannedStart: "2016-08-03"
      }));
    });
  });
  
  describe("setPlannedStart", function () {
      
    beforeEach(function () {

      areaPlansRef.set({
        "AREA1": {
          project: "PROJ1",
          plannedStart: "2016-08-03"
        },
        "AREA2": {
          project: "PROJ2",
          plannedStart: "2016-08-01"
        }
      });
    });

    it("set the planned start date to date set by user", function () {
      
      var area1 = {
        "$id": "AREA1",
        "project": "PROJ1",
        "type": "Ext",
        "revisions": {
          "PROJ1REV1": "2016-08-13"
        },
        "supplier": "Innovaré"
      };
        
      plannerService.setPlannedStart(area1, "2016-08-04", true);
      scope.$apply();

      var plans = plannerService.getProductionPlansByPlannedStart();
      scope.$apply();

      expect(plans.length).toBe(2);
      expect(plans[0]).toEqual(jasmine.objectContaining({
        $id: "AREA2",
        project: "PROJ2",
        plannedStart: "2016-08-01"
      }));
      expect(plans[1]).toEqual(jasmine.objectContaining({
        $id: "AREA1",
        project: "PROJ1",
        plannedStart: "2016-08-04",
        userChange: true
      }));
    });
    
    it("set the planned start back to default", function () {

      var area1 = {
        "$id": "AREA1",
        "project": "PROJ1",
        "type": "Ext",
        "revisions": {
          "PROJ1REV1": "2016-08-13"
        },
        "supplier": "Innovaré"
      };

      plannerService.setPlannedStart(area1, "2016-08-04", false);
      scope.$apply();

      var plans = plannerService.getProductionPlansByPlannedStart();
      scope.$apply();

      expect(plans.length).toBe(2);
      expect(plans[0]).toEqual(jasmine.objectContaining({
        $id: "AREA2",
        project: "PROJ2",
        plannedStart: "2016-08-01"
      }));
      expect(plans[1]).toEqual(jasmine.objectContaining({
        $id: "AREA1",
        project: "PROJ1",
        plannedStart: "2016-08-04",
        userChange: false
      }));
    });
   
  });

  describe("sortDataForChart", function () {
    
        
    var staticAverages = {};
    staticAverages["SIP"] = 0;
    staticAverages["SIP2"] = 0;
    staticAverages["HSIP"] = 0;
    staticAverages["IFAST"] = 0;
    staticAverages["TF"] = 0;
    staticAverages["TF2"] = 0;
    staticAverages["CASS"] = 0;
    
    var now = new Date("2016-02-01");
    beforeEach(function () {
      jasmine.clock().mockDate(now);

      areasRef.set(
        {
          "AREA1": {
            "project": "PROJ1",
            "phase": "P1",
            "floor": "1F",
            "revisions": {
              "PROJ1REV1": "2016-02-10"
            }
          },
          "AREA2": {
            "project": "PROJ1",
            "phase": "P1",
            "floor": "1F",
            "revisions": {
              "PROJ1REV1": "2016-02-11"
            }
          },
          "AREA3": {
            "project": "PROJ1",
            "phase": "P1",
            "floor": "1F",
            "revisions": {
              "PROJ1REV1": "2016-02-12"
            }
          },
          "AREA4": {
            "project": "PROJ1",
            "phase": "P1",
            "floor": "1F",
            "revisions": {
              "PROJ1REV1": "2016-02-12"
            }
          },
          "AREA5": {
            "project": "PROJ1",
            "phase": "P1",
            "floor": "1F",
            "revisions": {
              "PROJ1REV1": "2016-02-12"
            }
          }
        }
      );
    });

    beforeEach(function () {
      projectsRef.set(
        {
          "PROJ1": {
            "id": "100",
            "deliverySchedule": {
              "published": "PROJ1REV1",
              "unpublished": "PROJ1REV2"
            },
            "active": true
          },
          "PROJ2": {
            "id": "101",
            "deliverySchedule": {
              "published": "PROJ2REV1",
              "unpublished": "PROJ2REV2"
            },
            "active": true
          }
        }
      );
    });

    it("derives production plan across panel types", function () {
      areasRef.child("AREA1").update({"type": "Ext", "supplier":"Innovaré", "actualArea": 400, "actualPanels": 40});
      areasRef.child("AREA2").update({"type": "Int", "supplier":"Innovaré", "estimatedArea": 400, "estimatedPanels": 40});
      areasRef.child("AREA3").update({"type": "Roof", "supplier":"Innovaré", "actualArea": 200, "actualPanels": 20});
      areasRef.child("AREA4").update({"type": "ExtH", "supplier":"Innovaré", "actualArea": 400, "actualPanels": 40});
      areasRef.child("AREA5").update({"type": "ExtF", "supplier":"Innovaré", "actualArea": 400, "actualPanels": 40});

      var result = plannerService.getForwardPlan(null);
      scope.$apply();
      
      var data = plannerService.sortDataForChart(result, staticAverages);
      
      scope.$apply();
      
      expect(data["maxData"]["SIP"]).toEqual([ 200, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
      expect(data["maxData"]["HSIP"]).toEqual([ 200, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
      expect(data["maxData"]["TF"]).toEqual([ 250, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
      expect(data["maxData"]["IFAST"]).toEqual([ 200, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  ]);
      expect(data["maxData"]["CASS"]).toEqual([ 60, 60, 60, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ]);
      
      //tests main data to make charts and get dates
      expect(data["mainData"]["SIP"]).toEqual({
        "1": { 
          "100-P1-1F-Ext-axis1": {
            "2016-02-01": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        },
        "2": { 
          "100-P1-1F-Ext-axis2": {
            "2016-02-01": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 400, "type": "actualAmount", "amount": 40, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        }
      });
      
      expect(data["mainData"]["HSIP"]).toEqual({
        "1": {
          "100-P1-1F-ExtH-axis1": {
            "2016-02-01": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        },
        "2": {
          "100-P1-1F-ExtH-axis2": {
            "2016-02-01": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 400, "type": "actualAmount", "amount": 40, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        }
      });

      expect(data["mainData"]["IFAST"]).toEqual({
        "1": {
          "100-P1-1F-ExtF-axis1": {
            "2016-02-01": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        },
        "2": {
          "100-P1-1F-ExtF-axis2": {
            "2016-02-01": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 400, "type": "actualAmount", "amount": 40, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        }
      });
      
      expect(data["mainData"]["TF"]).toEqual({
        "1": {
          "100-P1-1F-Int-axis1": {
            "2016-02-01": {"meterSquared": 250, "type": "estimatedAmount", "amount": 25, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 150, "type": "estimatedAmount", "amount": 15, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        },
        "2": {
          "100-P1-1F-Int-axis2": {
            "2016-02-01": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 400, "type": "estimatedAmount", "amount": 40, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        }
      });
      
      expect(data["mainData"]["CASS"]).toEqual({
        "1": {
          "100-P1-1F-Roof-axis1": {
            "2016-02-01": {"meterSquared": 60, "type": "actualAmount", "amount": 6, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 60, "type": "actualAmount", "amount": 6, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 60, "type": "actualAmount", "amount": 6, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 20, "type": "actualAmount", "amount": 2, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        },
        "2": {
          "100-P1-1F-Roof-axis2": {
            "2016-02-01": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-02": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-03": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-04": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-05": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-06": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-07": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-08": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-09": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-10": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-11": {"meterSquared": 200, "type": "actualAmount", "amount": 20, "framingStyle": ""},
            "2016-02-12": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-13": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-14": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-15": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-16": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-17": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-18": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-19": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-20": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""},
            "2016-02-21": {"meterSquared": 0, "type": "neither", "amount": 0, "framingStyle": ""}
          }
        }
      });
      
      expect(data["chartData"]["SIP"]).toEqual(
        [
          { 
            "type": "bar", 
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "1", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 },
              { x: 1454544000000, y: 0 },
              { x: 1454630400000, y: 0 },
              { x: 1454716800000, y: 0 },
              { x: 1454803200000, y: 0 },
              { x: 1454889600000, y: 0 },
              { x: 1454976000000, y: 0 },
              { x: 1455062400000, y: 0 },
              { x: 1455148800000, y: 0 },
              { x: 1455235200000, y: 0 },
              { x: 1455321600000, y: 0 },
              { x: 1455408000000, y: 0 },
              { x: 1455494400000, y: 0 },
              { x: 1455580800000, y: 0 },
              { x: 1455667200000, y: 0 },
              { x: 1455753600000, y: 0 },
              { x: 1455840000000, y: 0 },
              { x: 1455926400000, y: 0 },
              { x: 1456012800000, y: 0 } 
            ]
          },
          { 
            "type": "bar",
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "2", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 } ,
              { x: 1454544000000, y: 0 } ,
              { x: 1454630400000, y: 0 } ,
              { x: 1454716800000, y: 0 } ,
              { x: 1454803200000, y: 0 } ,
              { x: 1454889600000, y: 0 } ,
              { x: 1454976000000, y: 0 } ,
              { x: 1455062400000, y: 0 } ,
              { x: 1455148800000, y: 0 } ,
              { x: 1455235200000, y: 0 } ,
              { x: 1455321600000, y: 0 } ,
              { x: 1455408000000, y: 0 } ,
              { x: 1455494400000, y: 0 } ,
              { x: 1455580800000, y: 0 } ,
              { x: 1455667200000, y: 0 } ,
              { x: 1455753600000, y: 0 } ,
              { x: 1455840000000, y: 0 } ,
              { x: 1455926400000, y: 0 } ,
              { x: 1456012800000, y: 0 } 
            ] 
          },
          {
            "key":"100-P1-1F-Ext-axis1",
            "type": "bar",
            "yAxis": "1",
            "color": "rgb(0,128,0)",
            "values":[
              {"x":1454284800000,"y":200},
              {"x":1454371200000,"y":200},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          },
          {
            "key":"100-P1-1F-Ext-axis2",
            "type": "bar",
            "color": "rgb(128,128,128)",
            "yAxis": "2",
            "values":[
              {"x":1454284800000,"y":0},
              {"x":1454371200000,"y":0},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":400},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          }
        ]);
          
      
      expect(data["chartData"]["HSIP"]).toEqual(
        [
          { 
            "type": "bar", 
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "1", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 },
              { x: 1454544000000, y: 0 },
              { x: 1454630400000, y: 0 },
              { x: 1454716800000, y: 0 },
              { x: 1454803200000, y: 0 },
              { x: 1454889600000, y: 0 },
              { x: 1454976000000, y: 0 },
              { x: 1455062400000, y: 0 },
              { x: 1455148800000, y: 0 },
              { x: 1455235200000, y: 0 },
              { x: 1455321600000, y: 0 },
              { x: 1455408000000, y: 0 },
              { x: 1455494400000, y: 0 },
              { x: 1455580800000, y: 0 },
              { x: 1455667200000, y: 0 },
              { x: 1455753600000, y: 0 },
              { x: 1455840000000, y: 0 },
              { x: 1455926400000, y: 0 },
              { x: 1456012800000, y: 0 } 
            ]
          },
          { 
            "type": "bar",
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "2", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 } ,
              { x: 1454544000000, y: 0 } ,
              { x: 1454630400000, y: 0 } ,
              { x: 1454716800000, y: 0 } ,
              { x: 1454803200000, y: 0 } ,
              { x: 1454889600000, y: 0 } ,
              { x: 1454976000000, y: 0 } ,
              { x: 1455062400000, y: 0 } ,
              { x: 1455148800000, y: 0 } ,
              { x: 1455235200000, y: 0 } ,
              { x: 1455321600000, y: 0 } ,
              { x: 1455408000000, y: 0 } ,
              { x: 1455494400000, y: 0 } ,
              { x: 1455580800000, y: 0 } ,
              { x: 1455667200000, y: 0 } ,
              { x: 1455753600000, y: 0 } ,
              { x: 1455840000000, y: 0 } ,
              { x: 1455926400000, y: 0 } ,
              { x: 1456012800000, y: 0 } 
            ] 
          },
          {
            "key":"100-P1-1F-ExtH-axis1",
            "color": "rgb(0,128,0)",
            "type": "bar",
            "yAxis": "1",
            "values":[
              {"x":1454284800000,"y":200},
              {"x":1454371200000,"y":200},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          },
          {
            "key":"100-P1-1F-ExtH-axis2",
            "color": "rgb(128,128,128)",
            "type": "bar",
            "yAxis": "2",
            "values":[
              {"x":1454284800000,"y":0},
              {"x":1454371200000,"y":0},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":400},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          }
        ]);
      
      expect(data["chartData"]["TF"]).toEqual(
        [
          { 
            "type": "bar", 
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "1", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 },
              { x: 1454544000000, y: 0 },
              { x: 1454630400000, y: 0 },
              { x: 1454716800000, y: 0 },
              { x: 1454803200000, y: 0 },
              { x: 1454889600000, y: 0 },
              { x: 1454976000000, y: 0 },
              { x: 1455062400000, y: 0 },
              { x: 1455148800000, y: 0 },
              { x: 1455235200000, y: 0 },
              { x: 1455321600000, y: 0 },
              { x: 1455408000000, y: 0 },
              { x: 1455494400000, y: 0 },
              { x: 1455580800000, y: 0 },
              { x: 1455667200000, y: 0 },
              { x: 1455753600000, y: 0 },
              { x: 1455840000000, y: 0 },
              { x: 1455926400000, y: 0 },
              { x: 1456012800000, y: 0 } 
            ]
          },
          { 
            "type": "bar",
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "2", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 } ,
              { x: 1454544000000, y: 0 } ,
              { x: 1454630400000, y: 0 } ,
              { x: 1454716800000, y: 0 } ,
              { x: 1454803200000, y: 0 } ,
              { x: 1454889600000, y: 0 } ,
              { x: 1454976000000, y: 0 } ,
              { x: 1455062400000, y: 0 } ,
              { x: 1455148800000, y: 0 } ,
              { x: 1455235200000, y: 0 } ,
              { x: 1455321600000, y: 0 } ,
              { x: 1455408000000, y: 0 } ,
              { x: 1455494400000, y: 0 } ,
              { x: 1455580800000, y: 0 } ,
              { x: 1455667200000, y: 0 } ,
              { x: 1455753600000, y: 0 } ,
              { x: 1455840000000, y: 0 } ,
              { x: 1455926400000, y: 0 } ,
              { x: 1456012800000, y: 0 } 
            ] 
          },
          {
            "key":"100-P1-1F-Int-axis1",
            "color": "rgb(255,165,0)",
            "type": "bar",
            "yAxis": "1",
            "values":[
              {"x":1454284800000,"y":250},
              {"x":1454371200000,"y":150},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          },
          {
            "key":"100-P1-1F-Int-axis2",
            "color": "rgb(192,192,192)",
            "type": "bar",
            "yAxis": "2",
            "values":[
              {"x":1454284800000,"y":0},
              {"x":1454371200000,"y":0},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":400},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          }
        ]);
      
      expect(data["chartData"]["CASS"]).toEqual(
        [
          { 
            "type": "bar", 
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "1", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 },
              { x: 1454544000000, y: 0 },
              { x: 1454630400000, y: 0 },
              { x: 1454716800000, y: 0 },
              { x: 1454803200000, y: 0 },
              { x: 1454889600000, y: 0 },
              { x: 1454976000000, y: 0 },
              { x: 1455062400000, y: 0 },
              { x: 1455148800000, y: 0 },
              { x: 1455235200000, y: 0 },
              { x: 1455321600000, y: 0 },
              { x: 1455408000000, y: 0 },
              { x: 1455494400000, y: 0 },
              { x: 1455580800000, y: 0 },
              { x: 1455667200000, y: 0 },
              { x: 1455753600000, y: 0 },
              { x: 1455840000000, y: 0 },
              { x: 1455926400000, y: 0 },
              { x: 1456012800000, y: 0 } 
            ]
          },
          { 
            "type": "bar",
            "key": "area", 
            "color": "rgb(139,0,0)", 
            "yAxis": "2", 
            "values": [
              { x: 1454284800000, y: 0 },
              { x: 1454371200000, y: 0 },
              { x: 1454457600000, y: 0 } ,
              { x: 1454544000000, y: 0 } ,
              { x: 1454630400000, y: 0 } ,
              { x: 1454716800000, y: 0 } ,
              { x: 1454803200000, y: 0 } ,
              { x: 1454889600000, y: 0 } ,
              { x: 1454976000000, y: 0 } ,
              { x: 1455062400000, y: 0 } ,
              { x: 1455148800000, y: 0 } ,
              { x: 1455235200000, y: 0 } ,
              { x: 1455321600000, y: 0 } ,
              { x: 1455408000000, y: 0 } ,
              { x: 1455494400000, y: 0 } ,
              { x: 1455580800000, y: 0 } ,
              { x: 1455667200000, y: 0 } ,
              { x: 1455753600000, y: 0 } ,
              { x: 1455840000000, y: 0 } ,
              { x: 1455926400000, y: 0 } ,
              { x: 1456012800000, y: 0 } 
            ] 
          },
          {
            "key":"100-P1-1F-Roof-axis1",
            "color": "rgb(0,128,0)",
            "type": "bar",
            "yAxis": "1",
            "values":[
              {"x":1454284800000,"y":60},
              {"x":1454371200000,"y":60},
              {"x":1454457600000,"y":60},
              {"x":1454544000000,"y":20},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":0},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          },
          {
            "key":"100-P1-1F-Roof-axis2",
            "color": "rgb(128,128,128)",
            "type": "bar",
            "yAxis": "2",
            "values":[
              {"x":1454284800000,"y":0},
              {"x":1454371200000,"y":0},
              {"x":1454457600000,"y":0},
              {"x":1454544000000,"y":0},
              {"x":1454630400000,"y":0},
              {"x":1454716800000,"y":0},
              {"x":1454803200000,"y":0},
              {"x":1454889600000,"y":0},
              {"x":1454976000000,"y":0},
              {"x":1455062400000,"y":0},
              {"x":1455148800000,"y":200},
              {"x":1455235200000,"y":0},
              {"x":1455321600000,"y":0},
              {"x":1455408000000,"y":0},
              {"x":1455494400000,"y":0},
              {"x":1455580800000,"y":0},
              {"x":1455667200000,"y":0},
              {"x":1455753600000,"y":0},
              {"x":1455840000000,"y":0},
              {"x":1455926400000,"y":0},
              {"x":1456012800000,"y":0}
            ]
          }
        ]);
      
    });
  
  });
 

  function expectedPlan(panelCountList) {
    var plan = new Array(21);
    for (var index = 0; index < plan.length; index++) {
      plan[index] = index < panelCountList.length ? panelCountList[index] : null;
    }
    return plan;
  }

});
