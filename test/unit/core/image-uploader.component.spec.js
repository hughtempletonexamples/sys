"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

xdescribe("image uploading component", function () {
  var additionalDataRef;
  var $componentController;
  var firebaseRoot;
  var storageRoot;
  
  
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    storageRoot = new MockFirebase("Mock://");
    additionalDataRef = firebaseRoot.child("additionalData");
    utils.addQuerySupport(additionalDataRef);
    

  }); 
  
  var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);
  var mockStorage = jasmine.createSpyObj("schema", ["getRoot","getStorage"]);

  
  angular.mock.module(function ($provide) {
    $provide.factory("schema", function () {
      return mockSchema;
    });
  });
  
  utils.setupMockSchema(mockSchema, firebaseRoot);
  utils.setupMockStorage(mockStorage, storageRoot);

  beforeEach(function () {
    
    mockSchema.status.userId = "tom";

    
    additionalDataRef.set({
      "panels":{
        "PROJ1AREA1PANEL1":{
          images:{"panel1image":true},
          errors:{"0":"error1"}
        }
      }
    });
    

  });





  beforeEach(angular.mock.module("innView.admin", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(inject(function (_$componentController_) {
    $componentController = _$componentController_;
  }));












  it("uploaded to firebase", function ($document) {
    var ImageData = {
      "size": 564828848,
      "name": "image1",
      "type": "image/jpeg",
      "lastModified": 1556281235645
    };

    var bindings = {data: {
      "$id": "PROJ1AREA1PANEL1",
      "size": 564828848,
      "name": "image1",
      "type": "image/jpeg",
      "lastModified": 1556281235645
    }};
  
    additionalDataRef.set({
      "panels":{
        "PROJ1AREA1PANEL1":{
          images:{"panel1image":true},
          errors:{"0":"error1"}
        }
      }
    });


    var ctrl = $componentController("innImageUploader", {$document: $document, schema: mockSchema}, bindings);

    var result = ctrl.uploadImage(ImageData,false);
    expect(result).toEqual({
      "size": 564828848,
      "name": "image1",
      "type": "image/jpeg",
      "lastModified": 1556281235645
    });

  });
});

