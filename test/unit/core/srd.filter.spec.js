"use strict";

var utils = require("../testUtils");

describe("srd filter", function () {

  var srdFilter;
  var scope;

  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var srdRef;
  var mockReferenceDataService;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    srdRef = firebaseRoot.child("srd");

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getItem"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _srdNameFilter_) {
    scope = $rootScope;
    srdFilter = _srdNameFilter_;
  }));

  beforeEach(inject(function ($firebaseObject) {
    mockReferenceDataService.getItem.and.callFake(function (variant, ref){
      return $firebaseObject(srdRef.child(variant).child(ref));
    });
  }));

  beforeEach(function () {
    srdRef.set({
      "variant1": {
        "item1": {
          "name": "Item One"
        },
        "item2": {
          "name": "Variant One Item Two"
        }
      },
      "variant2": {
        "item2": {
          "name": "Kevin Bacon"
        }
      }
    });
  });

  it("should be stateful to allow for async lookup", function () {
    expect(srdFilter.$stateful).toEqual(true);
  });

  it("should lookup reference data name", function () {
    // Value before data loaded
    expect(srdFilter("item2", "variant1")).toEqual("item2");
    expect(srdFilter("item2", "variant2")).toEqual("item2");

    scope.$apply();

    // Expected value
    expect(srdFilter("item2", "variant1")).toEqual("Variant One Item Two");
    expect(srdFilter("item2", "variant2")).toEqual("Kevin Bacon");
  });
});
