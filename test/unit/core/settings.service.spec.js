"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("settings service", function () {
  var defaultSettingsRef;
  var settingsRef;
  var scope;

  var firebaseRoot;

  var settingsService;

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    defaultSettingsRef = firebaseRoot.child("srd/defaultSettings");
    settingsRef = firebaseRoot.child("settings");

    angular.mock.module("innView.core");

    angular.mock.module(function ($provide) {
      $provide.value("$log", console);
    });
  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function (_$rootScope_, _settingsService_) {
    scope = _$rootScope_;
    settingsService = _settingsService_;
  }));

  beforeEach(function () {
    defaultSettingsRef.set({
      designHandoverPeriod: 10,
      designHandoverBuffer: 3
    });
  });

  it("returns default settings", function (done) {

    settingsService.default()
      .then(function (result) {
        expect(result).toEqual({
          designHandoverPeriod: 10,
          designHandoverBuffer: 3
        });

        done();
      });

    scope.$apply();
  });

  it("returns settings populated with defaults if no overrides", function (done) {

    settingsService.get()
      .then(function (result) {
        expect(result).toEqual({
          designHandoverPeriod: 10,
          designHandoverBuffer: 3
        });

        done();
      });

    scope.$apply();
  });

  it("returns settings overriding defaults", function (done) {

    settingsRef.set({
      designHandoverPeriod: 8,
      designHandoverBuffer: 5
    });

    settingsService.get()
      .then(function (result) {
        expect(result).toEqual({
          designHandoverPeriod: 8,
          designHandoverBuffer: 5
        });

        done();
      });

    scope.$apply();
  });

  it("updates settings", function (done) {

    settingsService.set({designHandoverPeriod: 9, designHandoverBuffer: 2})
      .then(function () {
        expect(settingsRef.getData()).toEqual({
          designHandoverPeriod: 9,
          designHandoverBuffer: 2
        });

        done();
      });

    scope.$apply();
  });

  it("updates only specified settings", function (done) {

    settingsRef.set({
      designHandoverPeriod: 8,
      designHandoverBuffer: 5
    });

    settingsService.set({designHandoverPeriod: 9})
      .then(function () {
        expect(settingsRef.getData()).toEqual({
          designHandoverPeriod: 9,
          designHandoverBuffer: 5
        });

        done();
      });

    scope.$apply();
  });

  it("updates specified settings removing value same as default", function (done) {

    settingsRef.set({
      designHandoverPeriod: 8,
      designHandoverBuffer: 5
    });

    settingsService.set({designHandoverPeriod: 10})
      .then(function () {
        expect(settingsRef.getData()).toEqual({
          designHandoverBuffer: 5
        });

        done();
      });

    scope.$apply();
  });

  it("updates specified settings removing value if null specified", function (done) {

    settingsRef.set({
      designHandoverPeriod: 8,
      designHandoverBuffer: 5
    });

    settingsService.set({designHandoverPeriod: null})
      .then(function () {
        expect(settingsRef.getData()).toEqual({
          designHandoverBuffer: 5
        });

        done();
      });

    scope.$apply();
  });

  it("updates specified settings with value not present in defaults", function (done) {

    settingsService.set({animal: "unicorn"})
      .then(function () {
        expect(settingsRef.getData()).toEqual({
          animal: "unicorn"
        });

        done();
      });

    scope.$apply();
  });

});
