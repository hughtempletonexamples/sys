"use strict";

xdescribe("core", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;

  beforeEach(angular.mock.module("innView.core"));

  beforeEach(inject(function ($window) {
    $window.Firebase = MockFirebase;
    $window.firebase = MockFirebase;
  }));

  describe("schema service", function () {

    beforeEach(inject(function ($window) {
      $window.PRODUCTION = false;
      $window.FIREBASE = "monumental-edifice-42";
      //firebase.initializeApp();
    }));

    it("dev envs should use FIREBASE value", inject(function (schema) {
      var root = schema.getRoot();
      expect(root.toString()).toEqual("https://monumental-edifice-42.firebaseio.com");
    }));
  });

  describe("schema service", function () {

    beforeEach(inject(function ($window, $location) {
      $window.PRODUCTION = true;
      spyOn($location, "host").and.returnValue("wobbling-weeble-1234.firebaseapp.com");
      //firebase.initializeApp();
    }));

    it("deployed envs should use value derived from deployed location", inject(function (schema) {
      var root = schema.getRoot();
      expect(root.toString()).toEqual("https://wobbling-weeble-1234.firebaseio.com");
    }));
  });

  describe("schema status", function () {

    it("should update connection status", inject(function ($rootScope, schema) {
      var info = schema.getRoot().ref.child(".info");
      info.autoFlush(true);

      var status = schema.status;

      info.set({connected: false});
      $rootScope.$apply();

      expect(status.connected).toEqual(false);

      info.set({connected: true});
      $rootScope.$apply();

      expect(status.connected).toEqual(true);

    }));
  });

});
