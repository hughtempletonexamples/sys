"use strict";

describe("googleFormsService", function () {
  var utils = require("../testUtils");

  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope;
  }));

  it("returns requested form subtree", inject(function (googleFormsService) {

    firebaseRoot.child("googleforms").set({
      "form1": {
        "1": {
          "name": "row 1"
        },
        "2": {
          "name": "row 2"
        }
      },
      "form2": {
        "2": {
          "column": "value1"
        },
        "3": {
          "column": "value2"
        }
      }
    });

    var result = googleFormsService.getFormData("form2");
    scope.$apply();

    utils.assertListEquals(result,
      [
        {
          "column": "value1"
        },
        {
          "column": "value2"
        }
      ]);
  }));
});
