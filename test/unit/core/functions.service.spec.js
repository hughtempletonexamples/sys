"use strict";

var utils = require("../testUtils");

describe("functionsService", function () {

  var scope;
  var functionsService;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(inject(function ($rootScope, _functionsService_) {
    scope = $rootScope;
    functionsService = _functionsService_;
  }));

  describe("logicHSIP", function () {

    it("calculates if HSIP by panelType ", function () {
      var foundProductType = "Ext";
      var panelType = "H";
      var height = 3000;
      var length = 3000;

      var result = functionsService.logicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtH");
    });
    
    it("calculates if HSIP by dimension ", function () {
      var foundProductType = "Ext";
      var panelType = "I";
      var height = 2650;
      var length = 600;

      var result = functionsService.logicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtH");
    });
    
    it("calculates if HSIP by dimension reverse ", function () {
      var foundProductType = "Ext";
      var panelType = "I";
      var height = 600;
      var length = 2650;

      var result = functionsService.logicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtH");
    });
    
    it("calculates if HSIP by foundProductType ", function () {
      var foundProductType = "ExtH";
      var panelType = "I";
      var height = 3000;
      var length = 3000;

      var result = functionsService.logicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtH");
    });

  });
  
  describe("uploaderLogicHSIP", function () {

    it("calculates if HSIP by panelType on uploader", function () {
      var foundProductType = "Ext";
      var panelType = "H";
      var height = 3000;
      var length = 3000;

      var result = functionsService.uploaderLogicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtH");
    });
    
    it("calculates if not HSIP or IFAST", function () {
      var foundProductType = "Ext";
      var panelType = "I";
      var height = 2650;
      var length = 600;

      var result = functionsService.uploaderLogicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("Ext");
    });

    it("calculates if IFAST by panelType on uploader", function () {
      var foundProductType = "Ext";
      var panelType = "F";
      var height = 3000;
      var length = 3000;

      var result = functionsService.uploaderLogicHSIP(foundProductType, panelType, height, length);
      scope.$apply();
      
      expect(result).toEqual("ExtF");
    });

  });

});
