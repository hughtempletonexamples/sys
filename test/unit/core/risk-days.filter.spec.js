"use strict";

describe("riskdays filter", function () {
  var riskDaysFilter;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(inject(function (_riskDaysFilter_) {
    riskDaysFilter = _riskDaysFilter_;
  }));

  it("should be stateless", function () {
    expect(riskDaysFilter.$stateful).not.toEqual(true);
  });

  it("should convert risk days", function () {

    expect(riskDaysFilter(undefined)).toBe("!");
    expect(riskDaysFilter(null)).toBe("");

    expect(riskDaysFilter(-1)).toBe("-1");
    expect(riskDaysFilter(0)).toBe("0");
    expect(riskDaysFilter(+1)).toBe("+1");
  });

});
