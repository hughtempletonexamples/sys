"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

var USER_ID = "{user-guid}";
var USER_NAME = "First Last";

describe("audit service", function () {
  var systemTimestamp;
  var firebaseRoot;
  var auditEntries;
  var scope;
  var audit;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    auditEntries = firebaseRoot.child("auditEntries");

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });
    utils.setupMockSchema(mockSchema, firebaseRoot, USER_ID);
    mockSchema.status.userName = USER_NAME;
  });

  beforeEach(inject(function (_$rootScope_, _audit_) {
    scope = _$rootScope_;
    audit = _audit_;
  }));

  it("records log entry", function (done) {

    audit.log("Log Detail")
      .then(function () {
        expect(utils.getRefChild(auditEntries, 0)).toEqual(
          {
            log: "Log Detail",
            timestamp: systemTimestamp,
            user: USER_ID,
            userName: USER_NAME
          }
        );

        done();
      });

    scope.$apply();
  });

  it("records project entry", function (done) {

    audit.project("Log Detail", "PROJ1", "123")
      .then(function () {
        expect(utils.getRefChild(auditEntries, 0)).toEqual(
          {
            log: "Log Detail",
            link:"",
            timestamp: systemTimestamp,
            user: USER_ID,
            userName: USER_NAME,
            project: "PROJ1",
            projectId: "123"
          }
        );

        done();
      });

    scope.$apply();
  });

  it("records project entry with project name", function (done) {
    firebaseRoot.child("projects").update({"PROJ1": {"id": "123"}});

    audit.project("Log Detail", "PROJ1")
      .then(function () {
        expect(utils.getRefChild(auditEntries, 0)).toEqual(
          {
            log: "Log Detail",
            link:"",
            timestamp: systemTimestamp,
            user: USER_ID,
            userName: USER_NAME,
            project: "PROJ1",
            projectId: "123"
          }
        );

        done();
      });

    scope.$apply();
  });

  it("records production line entry", function (done) {

    audit.productionLine("Log Detail", "SIP")
      .then(function () {
        expect(utils.getRefChild(auditEntries, 0)).toEqual(
          {
            log: "Log Detail",
            timestamp: systemTimestamp,
            user: USER_ID,
            userName: USER_NAME,
            productionLine: "SIP"
          }
        );

        done();
      });

    scope.$apply();
  });

});
