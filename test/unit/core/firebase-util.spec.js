"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;

describe("firebase utility functions", function () {
  var $firebaseObject;
  var $firebaseArray;
  var scope;

  var firebaseUtil;

  beforeEach(angular.mock.module("innView.core"));

  beforeEach(inject(function (_firebaseUtil_, $rootScope, _$firebaseObject_, _$firebaseArray_) {
    firebaseUtil = _firebaseUtil_;
    scope = $rootScope;
    $firebaseObject = _$firebaseObject_;
    $firebaseArray = _$firebaseArray_;
  }));

  describe("loaded", function () {

    it("resolves promise when firebaseObject loaded", function () {
      var ref = new MockFirebase("Mock://ref");

      var resolved;

      firebaseUtil.loaded($firebaseObject(ref))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      scope.$apply();
      expect(resolved).toBeUndefined();

      ref.flush();
      scope.$apply();
      expect(resolved).toBe(true);
    });

    it("resolves promise when firebaseArray loaded", function () {
      var ref = new MockFirebase("Mock://ref");

      var resolved;

      firebaseUtil.loaded($firebaseArray(ref))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      scope.$apply();
      expect(resolved).toBeUndefined();

      ref.flush();
      scope.$apply();
      expect(resolved).toBe(true);
    });

    it("resolves promise for multiple angularfire objects loaded", function () {
      var ref1 = new MockFirebase("Mock://ref1");
      var ref2 = new MockFirebase("Mock://ref2");

      var resolved;

      firebaseUtil.loaded($firebaseObject(ref1), $firebaseArray(ref2))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      scope.$apply();
      expect(resolved).toBeUndefined();

      ref1.flush();
      scope.$apply();
      expect(resolved).toBeUndefined();

      ref2.flush();
      scope.$apply();
      expect(resolved).toBe(true);
    });

    it("rejects promise for firebaseObject load failed", function () {
      var ref = new MockFirebase("Mock://ref");
      ref.failNext("on", new Error("EIO"));

      var resolved;

      firebaseUtil.loaded($firebaseObject(ref))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      scope.$apply();
      expect(resolved).toBeUndefined();

      ref.flush();
      scope.$apply();
      expect(resolved).toBe(false);
    });

    it("rejects promise for firebaseArray load failed", function () {
      var ref = new MockFirebase("Mock://ref");
      ref.failNext("on", new Error("EIO"));

      var resolved;

      firebaseUtil.loaded($firebaseArray(ref))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      ref.flush();
      scope.$apply();
      expect(resolved).toBe(false);
    });

    it("rejects promise for multiple angularfire objects load failed", function () {
      var ref1 = new MockFirebase("Mock://ref1");
      ref1.failNext("on", new Error("EIO"));
      var ref2 = new MockFirebase("Mock://ref2");

      var resolved;

      firebaseUtil.loaded($firebaseObject(ref1), $firebaseObject(ref2))
        .then(function () {
          resolved = true;
        })
        .catch(function () {
          resolved = false;
        });

      scope.$apply();
      expect(resolved).toBeUndefined();

      ref1.flush();
      scope.$apply();
      expect(resolved).toBe(false);
    });

  });

});
