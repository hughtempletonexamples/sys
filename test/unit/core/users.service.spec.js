"use strict";

var utils = require("../testUtils");

describe("usersService", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;
  var usersService;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _usersService_) {
    scope = $rootScope;
    usersService = _usersService_;
  }));

  describe("getUserById", function () {

    it("gets a specific user", function () {

      firebaseRoot.child("users").set({
        "A": {
          "$id": "A",
          "email": "aa@aa.com",
          "name": "Alpha",
          "roles": {
            "delivery-manager": true
          },
          "phone": "07795358899"
        },
        "B": {
          "$id": "B",
          "email": "bb@bb.com",
          "name": "Bravo",
          "roles": {
            "operations": true,
            "production": true,
          },
          "phone": "07965358899"
        }
      });

      var result = usersService.getUserById("A");
      scope.$apply();
      
      expect(result).toEqual(jasmine.objectContaining({
        "$id": "A",
        "name": "Alpha",
        "email": "aa@aa.com",
        "roles": {
          "delivery-manager": true
        },
        "phone": "07795358899"
      }));

    })
  });

  describe("getAllUsers", function () {

    it("gets all users ", function () {

      firebaseRoot.child("users").set({
        "A": {
          "$id": "A",
          "email": "aa@aa.com",
          "name": "Alpha",
          "roles": {
            "operations": true,
            "production": true,
            "delivery-manager": true
          },
          "phone": "07795358899"
        },
        "B": {
          "$id": "B",
          "email": "bb@bb.com",
          "name": "Bravo",
          "roles": {
            "operations": true,
            "production": true,
          },
          "phone": "07965358899"
        }
      });

      var result = usersService.getAllUsers();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "email": "aa@aa.com",
            "name": "Alpha",
            "roles": {
              "operations": true,
              "production": true,
              "delivery-manager": true
            },
            "phone": "07795358899"
          },
          {
            "$id": "B",
            "email": "bb@bb.com",
            "name": "Bravo",
            "roles": {
              "operations": true,
              "production": true,
            },
            "phone": "07965358899"
          }
        ]);
    });

  });

});
