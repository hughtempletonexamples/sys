"use strict";

var USER1_GUID = "user1-guid";
var USER2_GUID = "user2-guid";

var utils = require("../testUtils");

describe("user filter", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;

  var userFilter;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _userFilter_) {
    scope = $rootScope;
    userFilter = _userFilter_;
  }));

  beforeEach(function () {
    firebaseRoot.child("users").child(USER1_GUID).set(
      {
        name: "User One"
      });
    firebaseRoot.child("users").child(USER2_GUID).set(
      {
        name: "User Two"
      });
  });

  it("should be stateful to allow for async lookup", function () {
    expect(userFilter.$stateful).toEqual(true);
  });

  it("should look up user name", function () {
    // Value before data loaded
    expect(userFilter(USER1_GUID)).toEqual("...");
    expect(userFilter(USER2_GUID)).toEqual("...");

    scope.$apply();

    // Expected value
    expect(userFilter(USER1_GUID)).toEqual("User One");
    expect(userFilter(USER2_GUID)).toEqual("User Two");
  });

});
