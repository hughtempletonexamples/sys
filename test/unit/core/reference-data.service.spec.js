"use strict";

var utils = require("../testUtils");

describe("referenceDataService", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
  });

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope;
  }));

  describe("static data", function () {

    it("gets datum types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/datumTypes").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getDatumTypes();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));
    
    it("gets components types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/components").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getComponents();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets vehicle types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/vehicleTypes").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getVehicleTypes();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets chains of custody types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/chainOfCustodyTypes").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getChainOfCustodyTypes();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));
    it("gets phases", inject(function (referenceDataService) {

      firebaseRoot.child("srd/phases").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getPhases();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets floors", inject(function (referenceDataService) {

      firebaseRoot.child("srd/floors").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getFloors();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets production lines", inject(function (referenceDataService) {

      firebaseRoot.child("srd/productionLines").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getProductionLines();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets product types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/productTypes").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getProductTypes();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets panel types", inject(function (referenceDataService) {

      firebaseRoot.child("srd/panelTypes").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getPanelTypes();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets suppliers", inject(function (referenceDataService) {

      firebaseRoot.child("srd/suppliers").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getSuppliers();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets offload methods", inject(function (referenceDataService) {

      firebaseRoot.child("srd/offloadMethods").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getOffloadMethods();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));
    
    it("gets gateways", inject(function (referenceDataService) {

      firebaseRoot.child("srd/gateways").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getGateways();
      scope.$apply();

      utils.assertListEquals(result,
        [
          {
            "$id": "A",
            "name": "Alpha"
          },
          {
            "$id": "B",
            "name": "Bravo"
          }
        ]);
    }));

    it("gets requested item", inject(function (referenceDataService) {
      firebaseRoot.child("srd/offloadMethods").set({
        "A": {
          "name": "Alpha"
        },
        "B": {
          "name": "Bravo"
        }
      });

      var result = referenceDataService.getItem("offloadMethods", "B");
      scope.$apply();

      expect(result).toEqual(jasmine.objectContaining({
        "name": "Bravo"
      }));
    }));
  });

});
