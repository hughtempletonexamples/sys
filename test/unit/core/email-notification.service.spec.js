"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("email notification service", function () {
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var revisionsRef;
  var componentsRef;
  var panelsRef;
  var referenceDataRef;
  var areaProductionPlansRef;
  var mockSettingsService;
  var mockAreasService;
  var mockProjectsService;
  var mockPanelsService;
  var mockPlannerService;
  var mockReferenceDataService;
  var mockComponentsService;

  var scope;

  var emailNotificationService;
  var areas;
  var projects;
  var revisions;
  var project1 = {
    $id: "PROJ1",
    "name": "PROJ1 Name",
    "active": true,
    "id": "001",
    "deliverySchedule": {
      "revisions": {
        "PROJ1REV0": true,
        "PROJ1REV1": true
      },
      "published": "PROJ1REV1"
    }
  };

  beforeEach(angular.mock.module("innView.core", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    revisionsRef = firebaseRoot.child("revisions");
    panelsRef = firebaseRoot.child("panels");
    areaProductionPlansRef = firebaseRoot.child("productionPlans/areas");
    componentsRef = firebaseRoot.child("components");
    referenceDataRef = firebaseRoot.child("srd");
    
    utils.addQuerySupport(projectsRef);
    utils.addQuerySupport(referenceDataRef);
    utils.addQuerySupport(componentsRef);
    utils.addQuerySupport(panelsRef);

    mockSettingsService = jasmine.createSpyObj("settingsService", ["get"]);
    mockComponentsService = jasmine.createSpyObj("componentsService", ["getComponentsForProject"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas", "getProjectArea", "updateProjectArea", "modifyCompletedPanels"]);
    mockProjectsService = jasmine.createSpyObj("projectsService", ["getProjects"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea", "markPanelsComplete", "deletePanels", "markPanelIncomplete", "updatePanels"]);
    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProjectAreaProductionPlans"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getDesignHandoverValues","getProductTypes", "getSuppliers"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("settingsService", function () {
        return mockSettingsService;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("componentsService", function () {
        return mockComponentsService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($q) {
    mockSettingsService.get.and.returnValue(
      $q.when({
        designHandoverPeriod: 10,
        designHandoverBuffer: 3
      })
    );
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockReferenceDataService.getSuppliers.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("suppliers"));
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {
    mockProjectsService.getProjects.and.callFake(function () {
      return $firebaseArray(projectsRef);
    });
  }));

  beforeEach(inject(function ($firebaseArray) {
    mockPlannerService.getProjectAreaProductionPlans.and.callFake(function () {
      return $firebaseArray(areaProductionPlansRef);
    });
  }));
  
  beforeEach(inject(function ($firebaseArray) {

    var projectComponents = {
      "PROJ1": {
        "component1": {
          "project": "PROJ1",
          "areas": {
            "PROJ1AREA1": true,
            "PROJ1AREA2": true
          },
          "type": "Walls"
        }
      },
      "PROJ2": {
        "component2": {
          "project": "PROJ2",
          "areas": {
            "PROJ2AREA1": true
          },
          "type": "Roof"
        }
      }
    };

    mockComponentsService.getComponentsForProject.and.callFake(
      function (projectKey) {
        var componentsRef = new MockFirebase("MockQuery://components");
        componentsRef.autoFlush(true);
        componentsRef.set(projectComponents[projectKey]);

        return $firebaseArray(componentsRef);
      });
  }));

  beforeEach(inject(function ($rootScope, _emailNotificationService_) {
    scope = $rootScope;
    emailNotificationService = _emailNotificationService_;
  }));

  beforeEach(inject(function ($firebaseArray, $firebaseObject) {
    mockAreasService.getProjectAreas.and.callFake(function (y) {
      return $firebaseArray(areasRef);
    });
    mockAreasService.getProjectArea.and.callFake(function (areaKey) {
      return $firebaseObject(areasRef.child(areaKey));
    });
  }));

  beforeEach(function () {
    
    referenceDataRef.child("productTypes").set(
      {
        "Ext": { productionLine: "SIP" },
        "ExtH": { productionLine: "HSIP" },
        "Int": { productionLine: "TF" },
        "Roof": { productionLine: "CASS" },
        "P&B": { },
        "Anc": { }
      }
    );
  
    referenceDataRef.child("suppliers").set(
      {
        "Innovaré": {thirdParty: false},
        "Other": {thirdParty: true}
      }
    );
  
    projects = {
      "PROJ1": project1,
      "PROJ2": {
        "name": "PROJ2",
        "active": true,
        "id": "002",
        "deliverySchedule": {
          "unpublished": "PROJ2REV1"
        }
      }
    };
    projectsRef.set(projects);

    areas = {
      "PROJ1AREA1": {
        "phase": "P1",
        "floor": "GF",
        "type": "Ext",
        "estimatedArea": 100.1,
        "estimatedPanels": 20,
        "actualArea": 95.1,
        "actualPanels": 15,
        "completedArea": 95.1,
        "completedPanels": 14,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2016-04-10"
        }
      },
      "PROJ1AREA2": {
        "phase": "P1",
        "floor": "GF",
        "type": "Int",
        "estimatedArea": 50.1,
        "estimatedPanels": 10,
        "actualArea": 45.1,
        "actualPanels": 9,
        "completedArea": 20.1,
        "completedPanels": 4,
        "supplier": "Innovaré",
        "revisions": {
          "PROJ1REV1": "2016-04-05"
        }
      }
    };
    areasRef.set(areas);

    revisions = {
      "PROJ1REV1": {
        "createdDate": "2016-04-01"
      }
    };
    revisionsRef.set(revisions);
  });

  xdescribe("innviewWeeklyDispatch", function () {

    it("to include worst risk value", function () {
      areaProductionPlansRef.child("PROJ1AREA1").set({
        plannedStart: "2016-08-01",
        plannedFinish: "2016-08-04",
        riskDate: "2016-08-01"
      });

      areaProductionPlansRef.child("PROJ1AREA2").set({
        plannedStart: "2016-09-01",
        plannedFinish: "2016-09-03",
        riskDate: "2016-09-01"
      });

      emailNotificationService.innviewWeeklyDispatch()
        .then(function (data){
          scope.$apply();
          expect(data).toBe();
        });
    });

  });
 
});
