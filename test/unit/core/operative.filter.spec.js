"use strict";

var OP1_KEY = "OPKEYA";
var OP2_KEY = "OPKEYB";

var utils = require("../testUtils");

describe("operative filter", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;

  var operativeFilter;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.core"));

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _operativeFilter_) {
    scope = $rootScope;
    operativeFilter = _operativeFilter_;
  }));

  beforeEach(function () {
    firebaseRoot.child("operatives").child(OP1_KEY).set(
      {
        name: "Neil Naylor"
      });
    firebaseRoot.child("operatives").child(OP2_KEY).set(
      {
        name: "Ruth Trusse"
      });
  });

  it("should be stateful to allow for async lookup", function () {
    expect(operativeFilter.$stateful).toEqual(true);
  });

  it("should look up operative name", function () {
    // Value before data loaded
    expect(operativeFilter(OP1_KEY)).toEqual("...");
    expect(operativeFilter(OP2_KEY)).toEqual("...");

    scope.$apply();

    // Expected value
    expect(operativeFilter(OP1_KEY)).toEqual("Neil Naylor");
    expect(operativeFilter(OP2_KEY)).toEqual("Ruth Trusse");
  });

});
