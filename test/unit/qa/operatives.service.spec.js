"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var utils = require("../testUtils");

describe("operatives service", function () {
  var firebaseRoot;
  var operativesRef;

  var scope;
  var operativesService;

  beforeEach(function () {
    angular.mock.module("innView.qa");
  });

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    operativesRef = firebaseRoot.child("operatives");
    utils.addQuerySupport(operativesRef);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _operativesService_) {
    scope = $rootScope;
    operativesService = _operativesService_;
  }));

  describe("available operatives", function () {

    it("returns active operatives", function () {

      operativesRef.set({
        "OP1": {
          "name": "Rita",
          "active": true
        },
        "OP2": {
          "name": "Sue",
          "active": false
        },
        "OP3": {
          "name": "Bob",
          "active": true
        }
      });

      var result = operativesService.list();
      scope.$apply();

      expect(result.length).toBe(2);
      expect(result[0]).toEqual(jasmine.objectContaining({$id: "OP1", name: "Rita"}));
      expect(result[1]).toEqual(jasmine.objectContaining({$id: "OP3", name: "Bob"}));
    });

  });

  describe("selected operatives", function () {

    it("defaults to none selected", function () {

      var result = operativesService.selected();

      expect(result).toEqual([]);
    });

  });

  describe("select operatives", function () {

    it("can select operatives", function () {

      operativesService.select(["OP1", "OP2"]);
      expect(operativesService.selected()).toEqual(["OP1", "OP2"]);

      operativesService.select(["OP2", "OP3"]);
      expect(operativesService.selected()).toEqual(["OP2", "OP3"]);
    });

  });


});
