"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var testUtils = require("../testUtils");

describe("prekit service", function () {

  var prekitService;

  var mockOperativesService;

  var projectsRef;
  var panelsRef;
  var requiredPanelUsageRef;
  var cuttingListRef;
  var jobsheetsRef;

  var operatives = [];

  var systemTimestamp;

  var scope;

  // Angular mock our qa module
  beforeEach(angular.mock.module("innView.qa", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    panelsRef = firebaseRoot.child("panels");
    requiredPanelUsageRef = firebaseRoot.child("requiredPanelUsage");
    cuttingListRef = firebaseRoot.child("cuttingList");
    jobsheetsRef = firebaseRoot.child("jobsheets");
    
    testUtils.addQuerySupport(requiredPanelUsageRef);
    testUtils.addQuerySupport(cuttingListRef);
    testUtils.addQuerySupport(jobsheetsRef);
    testUtils.addQuerySupport(panelsRef);

    mockOperativesService = jasmine.createSpyObj("operativesService", ["selected"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("operativesService", function () {
        return mockOperativesService;
      });
    });

    testUtils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _prekitService_) {
    scope = $rootScope;
    prekitService = _prekitService_;
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {
    mockOperativesService.selected.and.callFake(function () {
      return operatives;
    });
  });

  describe("startPanel", function () {

    beforeEach(function () {
      jobsheetsRef.set(
        {
          "JOBSHEET1": {
            jobsheet: "jobsheet1"
          }
        }
      );

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
    });

    it("sets the prekit start time and currently selected operatives on a jobsheet", function () {
      prekitService.startPanel("jobsheet1");

      scope.$apply();

      testUtils.expectRefChildToContain(jobsheetsRef, 0, {
        jobsheet: "jobsheet1",
        started: systemTimestamp,
        operatives: {"operative1Ref": true, "operative2Ref": true},
        startedOperatives: {"operative1Ref": true, "operative2Ref": true}
      });
    });

    it("does not set new start time on subsequent calls", function () {
      var historicalTimestamp = systemTimestamp - 10000;
      jobsheetsRef.set(
        {
          "JOBSHEET1": {
            jobsheet: "jobsheet1",
            started: historicalTimestamp
          }
        }
      );

      prekitService.startPanel("jobsheet1");

      scope.$apply();

      testUtils.expectRefChildToContain(jobsheetsRef, 0, {
        jobsheet: "jobsheet1",
        started: historicalTimestamp
      });
    });
  });

  describe("completePanel", function () {
    var jobsheet1;

    beforeEach(function () {

      jobsheetsRef.set(
        {
          "JOBSHEET1": {
            jobsheet: "jobsheet1"
          }
        }
      );

      jobsheet1 = {
        $id: "JOBSHEET1",
        jobsheet: "jobsheet1",
        operatives: {"operative1Ref": true},
        startedOperatives: {"operative1Ref": true}
      };

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
    });

    it("merges selected operatives", function () {
      prekitService.completePanel(jobsheet1);

      scope.$apply();

      testUtils.expectRefChildToContain(jobsheetsRef, 0, {
        jobsheet: "jobsheet1",
        operatives: {"operative1Ref": true, "operative2Ref": true},
        completedOperatives: {"operative1Ref": true, "operative2Ref": true}
      });
    });

  });
  
  describe("jobsheetsForProject", function () {

    var jobsheet1 = {
      jobsheet: "jobsheet1",
      projectId: "PROJ1"
    };

    var jobsheet2 = {
      jobsheet: "jobsheet2",
      projectId: "PROJ1"
    };

    var jobsheet3 = {
      jobsheet: "jobsheet3",
      projectId: "PROJ2"
    };

    beforeEach(function () {
      jobsheetsRef.set({
        "PROJ1JOBSHEET1": jobsheet1,
        "PROJ1JOBSHEET2": jobsheet2,
        "PROJ2JOBSHEET1": jobsheet3
      });
    });

    it("returns all jobsheets for a given project", function () {
      var result = prekitService.jobsheetsForProject("PROJ1");

      scope.$apply();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(jasmine.objectContaining(jobsheet1));
      expect(result[1]).toEqual(jasmine.objectContaining(jobsheet2));
    });
  });
  
  describe("getJobsheets", function () {

    var jobsheet1 = {
      jobsheet: "jobsheet1",
      projectId: "PROJ1"
    };

    var jobsheet2 = {
      jobsheet: "jobsheet2",
      projectId: "PROJ1"
    };

    var jobsheet3 = {
      jobsheet: "jobsheet3",
      projectId: "PROJ2"
    };

    beforeEach(function () {
      jobsheetsRef.set({
        "PROJ1JOBSHEET1": jobsheet1,
        "PROJ1JOBSHEET2": jobsheet2,
        "PROJ2JOBSHEET1": jobsheet3
      });
    });

    it("returns all jobsheets for a given project", function () {
      var result = prekitService.getJobsheets("jobsheet3");

      scope.$apply();
      expect(result.length).toEqual(1);
      expect(result[0]).toEqual(jasmine.objectContaining(jobsheet3));
    });
  });
  
  describe("getExistingCuttingList", function () {
    beforeEach(function () {
      
      var jobsheet1 = {
        jobsheet: "jobsheet1",
        projectId: "PROJ1",
        cuttingList: {
          "a":true,
          "b":true,
          "c":true
        }
      };

      var jobsheet2 = {
        jobsheet: "jobsheet2",
        projectId: "PROJ1",
        cuttingList: {
          "d":true,
          "e":true,
          "f":true
        }
      };

      beforeEach(function () {
        jobsheetsRef.set({
          "PROJ1JOBSHEET1": jobsheet1,
          "PROJ1JOBSHEET2": jobsheet2
        });
      });

      cuttingListRef.set({
        "a": {
          "type": "38 x 114"
        },
        "b": {
          "type": "38 x 130"
        },
        "c": {
          "type": "38 x 135"
        }
      });
    });

    it("returns all known cutting lists", function () {
      
      var jobsheet1 = {
        jobsheet: "jobsheet1",
        projectId: "PROJ1",
        cuttingList: {
          "a":true,
          "b":true,
          "c":true
        }
      };

      prekitService.getExistingCuttingList(jobsheet1)
      .then(function (cutting) {

        expect(cutting.length).toEqual(3);
        expect(cutting[0]).toEqual(jasmine.objectContaining({
          "type": "38 x 114"
        }));
        expect(cutting[1]).toEqual(jasmine.objectContaining({
          "type": "38 x 130"
        }));
        expect(cutting[2]).toEqual(jasmine.objectContaining({
          "type": "38 x 135"
        }));
        
      });
      scope.$apply();

    });
  });
});
