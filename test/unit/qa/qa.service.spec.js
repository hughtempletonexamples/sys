"use strict";

var MockFirebase = require("firebase-mock").MockFirebase;
var testUtils = require("../testUtils");

describe("qa service", function () {

  var qaService;

  var mockPlannerService;
  var mockAreasService;
  var mockPanelsService;
  var mockReferenceDataService;
  var mockOperativesService;

  var areaPlansRef;
  var areasRef;
  var panelsRef;
  var referenceDataRef;

  var operatives = [];

  var systemTimestamp;

  var scope;

  // Angular mock our qa module
  beforeEach(angular.mock.module("innView.qa", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    areasRef = firebaseRoot.child("areas");
    areaPlansRef = firebaseRoot.child("productionPlans/areas");
    panelsRef = firebaseRoot.child("panels");
    referenceDataRef = firebaseRoot.child("srd");

    testUtils.addQuerySupport(areaPlansRef);
    testUtils.addQuerySupport(areasRef);
    testUtils.addQuerySupport(panelsRef);
    testUtils.addQuerySupport(referenceDataRef);

    mockPlannerService = jasmine.createSpyObj("plannerService", ["getProductionPlansByPlannedStart"]);
    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectArea"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["getPanelById", "markPanelsComplete"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getProductTypes"]);
    mockOperativesService = jasmine.createSpyObj("operativesService", ["selected"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
      $provide.factory("operativesService", function () {
        return mockOperativesService;
      });
    });

    testUtils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($firebaseObject, $firebaseArray) {
    mockAreasService.getProjectArea.and.callFake(function (areaKey) {
      return $firebaseObject(areasRef.child(areaKey));
    });

    mockPlannerService.getProductionPlansByPlannedStart.and.callFake(function () {
      return $firebaseArray(areaPlansRef);
    });

    mockPanelsService.getPanelById.and.callFake(function () {
      return $firebaseArray(panelsRef);
    });

    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(inject(function ($rootScope, _qaService_) {
    scope = $rootScope;
    qaService = _qaService_;
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {
    referenceDataRef.set({
      "productTypes": {
        "type1": {
          "name": "Type 1",
          "productionLine": "PL1"
        },
        "type2": {
          "name": "Type 2",
          "productionLine": "PL2"
        }
      }
    });

    scope.$apply();
  });

  beforeEach(function () {
    mockOperativesService.selected.and.callFake(function () {
      return operatives;
    });
  });

  describe("getIncompleteAreasByProductionLine", function () {

    beforeEach(function () {
      areaPlansRef.set(
        {
          "AREA1": {
            project: "PROJ1",
            plannedStart: "2016-08-03"
          },
          "AREA2": {
            project: "PROJ2",
            plannedStart: "2016-08-01"
          },
          "AREA3": {
            project: "PROJ1",
            plannedStart: "2016-08-01"
          }
        }
      );

      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "type2",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          },
          "AREA2": {
            phase: "1",
            floor: "2",
            type: "type2",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 10
          },
          "AREA3": {
            phase: "1",
            floor: "2",
            type: "type1",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 8
          }
        }
      );
    });

    it("returns all areas with incomplete panels for a given production line ordered by production planned start date", function () {
      qaService.getIncompleteAreasForProductionLine("PL2")
        .then(function (incompleteAreas) {
          expect(incompleteAreas.length).toBe(1);

          expect(incompleteAreas[0]).toEqual(jasmine.objectContaining({
            phase: "1",
            floor: "1",
            type: "type2",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          }));
        });
      scope.$apply();
    });
  });

  describe("startPanel", function () {

    beforeEach(function () {
      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "type1",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          }
        }
       );

      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1"
          }
        }
      );

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
    });

    it("sets the qa start time and currently selected operatives on a panel", function () {
      qaService.startPanel("panel1");

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        id: "panel1",
        area: "AREA1",
        qa: {
          started: systemTimestamp,
          operatives: {"operative1Ref": true, "operative2Ref": true},
          startedOperatives: {"operative1Ref": true, "operative2Ref": true}
        }
      });
    });

    it("does not set new start time on subsequent calls", function () {
      var historicalTimestamp = systemTimestamp - 10000;
      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1",
            qa: {
              started: historicalTimestamp
            }
          }
        }
      );

      qaService.startPanel("panel1");

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        id: "panel1",
        area: "AREA1",
        qa: {
          started: historicalTimestamp
        }
      });
    });
  });
  
  describe("finishAssembly", function () {

    beforeEach(function () {
      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "type1",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          }
        }
       );

      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1",
            qa: {
              started: systemTimestamp - 776543210,
              operatives: {"operative1Ref": true},
              startedOperatives: {"operative1Ref": true}
            },
          }
        }
      );

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
    });

    it("sets the qa finish assembly time and currently selected operatives on a panel", function () {
      qaService.finishAssembly("panel1");

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        id: "panel1",
        area: "AREA1",
        qa: {
          started: systemTimestamp - 776543210,
          startedOperatives: {"operative1Ref": true},
          finishAssembly: systemTimestamp,
          operatives: {"operative1Ref": true, "operative2Ref": true},
          finishAssemblyOperatives: {"operative1Ref": true, "operative2Ref": true}
        }
      });
    });

    it("does not set new finish assembly time on subsequent calls", function () {
      var historicalTimestamp = systemTimestamp - 10000;
      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1",
            qa: {
              finishAssembly: historicalTimestamp
            }
          }
        }
      );

      qaService.finishAssembly("panel1");

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        id: "panel1",
        area: "AREA1",
        qa: {
          finishAssembly: historicalTimestamp
        }
      });
    });
  });
  
  describe("undoPrevious", function () {


    it("goes back a stage from current state, but not if complete or start. this tests taking finish assembly back to start", function () {
      
      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "type1",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          }
        }
      );

      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1",
            qa: {
              started: systemTimestamp - 776543210,
              operatives: {"operative1Ref": true},
              startedOperatives: {"operative1Ref": true},
              finishAssembly: systemTimestamp,
              finishAssemblyOperatives: {"operative1Ref": true}
            }
          }
        }
      );

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
      
      qaService.undoPrevious("panel1");

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        id: "panel1",
        area: "AREA1",
        qa: {
          started: systemTimestamp - 776543210,
          startedOperatives: {"operative1Ref": true},
          operatives: {"operative1Ref": true},
        }
      });
    });
  });

  describe("completePanel", function () {
    var panel1;
    var startedTimestamp = systemTimestamp - 10000;

    beforeEach(function () {
      areasRef.set(
        {
          "AREA1": {
            phase: "1",
            floor: "1",
            type: "type1",
            description: "An area",
            supplier: "Innovaré",
            project: "a",
            timestamps: {
              created: systemTimestamp - 876543210,
              modified: systemTimestamp - 876543210
            },
            actualPanels: 10,
            completedPanels: 9
          }
        }
       );

      panelsRef.set(
        {
          "AREA1PANEL11": {
            id: "panel1",
            area: "AREA1",
            qa: {
              started: startedTimestamp
            }
          }
        }
      );

      panel1 = {
        $id: "AREA1PANEL1",
        id: "panel1",
        area: "AREA1",
        qa: {
          started: startedTimestamp,
          operatives: {"operative1Ref": true},
          startedOperatives: {"operative1Ref": true}
        }
      };

      operatives = [
        "operative1Ref",
        "operative2Ref"
      ];
    });

    it("merges selected operatives", function () {
      qaService.completePanel(panel1);

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        qa: {
          started: startedTimestamp,
          operatives: {"operative1Ref": true, "operative2Ref": true},
          startedOperatives: {"operative1Ref": true},
          completedOperatives: {"operative1Ref": true, "operative2Ref": true}
        }
      });
    });

    it("persists qa values for panel", function () {
      panel1.qa["midLength"] = 1;
      panel1.qa["midHeight"] = 2;
      panel1.qa["diagonalWidth"] = 3;
      panel1.qa["diagonalHeight"] = 4;

      qaService.completePanel(panel1);

      scope.$apply();

      testUtils.expectRefChildToContain(panelsRef, 0, {
        qa: {
          started: startedTimestamp,
          midLength: 1,
          midHeight: 2,
          diagonalWidth: 3,
          diagonalHeight: 4,
          operatives: {"operative1Ref": true, "operative2Ref": true},
          startedOperatives: {"operative1Ref": true},
          completedOperatives: {"operative1Ref": true, "operative2Ref": true}
        }
      });
    });

    it("marks panel as complete", function () {
      qaService.completePanel(panel1);

      scope.$apply();

      expect(mockPanelsService.markPanelsComplete).toHaveBeenCalledWith("AREA1", ["AREA1PANEL1"]);
    });
  });
});