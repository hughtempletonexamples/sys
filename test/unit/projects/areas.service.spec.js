"use strict";

var utils = require("../testUtils");

describe("areas service", function () {
  var systemTimestamp;
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var panelsRef;
  var datumsRef;
  var referenceDataRef;

  var scope;

  var mockAuditService;
  var mockPanelsService;
  var mockDatumsService;
  var mockReferenceDataService;
  var areasService;

  var projectA;
  var area1;
  var area2;
  var area3;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.projects", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    mockAuditService = jasmine.createSpyObj("auditsService", ["project"]);
    mockPanelsService = jasmine.createSpyObj("panelsService", ["panelsForArea", "deletePanelsForArea"]);
    mockDatumsService = jasmine.createSpyObj("datumsService", ["damumByName"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getComponents", "getProductTypes"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");

    areasRef = firebaseRoot.child("areas");
    utils.addQuerySupport(areasRef);

    panelsRef = firebaseRoot.child("panels");
    utils.addQuerySupport(panelsRef);
    
    datumsRef = firebaseRoot.child("datums");
    utils.addQuerySupport(datumsRef);
    
    referenceDataRef = firebaseRoot.child("srd");
    utils.addQuerySupport(referenceDataRef);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("audit", function () {
        return mockAuditService;
      });
      $provide.factory("panelsService", function () {
        return mockPanelsService;
      });
      $provide.factory("datumsService", function () {
        return mockDatumsService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function  ($rootScope, _areasService_) {
    scope = $rootScope;
    areasService = _areasService_;
  }));

  beforeEach(inject(function ($q, $firebaseArray) {
    mockDatumsService.damumByName.and.callFake(function (datumName) {
      return $firebaseArray(datumsRef.orderByChild("name").equalTo(datumName));
    });
    mockPanelsService.panelsForArea.and.callFake(function (areaRef) {
      return $firebaseArray(panelsRef.orderByChild("area").equalTo(areaRef));
    });
    mockPanelsService.deletePanelsForArea.and.callFake(function (areaRef) {
      return $q.when();
    });
    mockReferenceDataService.getComponents.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("components"));
    });
    mockReferenceDataService.getProductTypes.and.callFake(function () {
      return $firebaseArray(referenceDataRef.child("productTypes"));
    });
  }));

  beforeEach(function () {
    projectA = {
      "$id": "a",
      "id": "123",
      "name": "Some Project",
      "deliverySchedule": {
        "unpublished": "unpublishedRevisionRef"
      }
    };

    projectsRef.set({
      "a": projectA
    });

    area1 = {
      phase: "1",
      floor: "1",
      type: "type",
      description: "An area",
      supplier: "Innovaré",
      project: "a",
      timestamps: {
        created: systemTimestamp - 876543210,
        modified: systemTimestamp - 876543210
      },
      revisions: {
        "unpublishedRevisionRef": "2016-09-05"
      },
      active: true
    };

    area2 = {
      phase: "2",
      floor: "1",
      type: "type",
      description: "An area",
      supplier: "Innovaré",
      project: "a",
      timestamps: {
        created: systemTimestamp - 876543210,
        modified: systemTimestamp - 876543210
      },
      revisions: {
        "unpublishedRevisionRef": "2016-10-03"
      },
      active: true
    };

    area3 = {
      phase: "1",
      floor: "2",
      type: "type",
      description: "An area",
      supplier: "Innovaré",
      project: "a",
      timestamps: {
        created: systemTimestamp - 876543210,
        modified: systemTimestamp - 876543210
      },
      revisions: {
        "unpublishedRevisionRef": "2016-09-06"
      },
      active: true
    };
    
    referenceDataRef.child("productTypes").set({
      "Anc": {
        $id: "Ancilliaries",
        "name": "Ancilliaries",
        "seq": 5
      },
      "Ext": {
        $id: "External",
        "name": "External",
        "productionLine": "SIP",
        "seq": 0
      },
      "Floor": {
        $id: "Floor",
        "name": "Floor",
        "productionLine": "CASS",
        "seq": 3
      },
      "Int": {
        $id: "Internal",
        "name": "Internal",
        "productionLine": "TF",
        "seq": 1
      },
      "P&B": {
        $id: "Posts and Beams",
        "name": "Posts and Beams",
        "seq": 6
      },
      "Roof": {
        $id: "Roof",
        "name": "Roof",
        "productionLine": "CASS",
        "seq": 4
      },
      "Steel": {
        $id: "Steel",
        "name": "Steel",
        "seq": 7
      },
      "Truss": {
        $id: "Trusses",
        "name": "Trusses",
        "seq": 8
      }
    });
    
    referenceDataRef.child("components").set({
      "Ancilliaries": {
        $id: "Ancilliaries",
        "name": "Ancilliaries",
        "productTypes": {
          "Anc": true
        },
        "seq": 0
      },
      "Floor": {
        $id: "Floor",
        "name": "Floor",
        "productTypes": {
          "Ext": true,
          "Floor": true,
          "Int": true
        },
        "seq": 2
      },
      "Glulams": {
        $id: "Glulams",
        "name": "Glulams",
        "productTypes": {
          "P&B": true
        },
        "seq": 6
      },
      "Parapets": {
        $id: "Parapets",
        "name": "Parapets",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 4
      },
      "Roof": {
        $id: "Roof",
        "name": "Roof",
        "productTypes": {
          "Ext": true,
          "Int": true,
          "Roof": true
        },
        "seq": 3
      },
      "Spandrels": {
        $id: "Spandrels",
        "name": "Spandrels",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 5
      },
      "Steels": {
        $id: "Steels",
        "name": "Steels",
        "productTypes": {
          "Steel": true
        },
        "seq": 7
      },
      "Trusses": {
        "name": "Trusses",
        "productTypes": {
          "Truss": true
        },
        "seq": 8
      },
      "Walls": {
        $id: "Walls",
        "name": "Walls",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 1
      }
    });

  });

  describe("getProjectAreas", function () {

    beforeEach(function () {
      areasRef.set({
        "area1": area1
      });
    });

    it("Returns areas for requested project", function () {
      var result;

      result = areasService.getProjectAreas("a");

      scope.$apply();
      expect(result.length).toEqual(1);
      expect(result[0])
        .toEqual(jasmine.objectContaining({
          description: "An area",
          floor: "1",
          phase: "1",
          project: "a",
          supplier: "Innovaré",
          type: "type"
        }));
    });
  });

  describe("getProjectAreasByPhase", function () {

    beforeEach(function () {
      areasRef.set({
        "area1": area1,
        "area2": area2,
        "area3": area3
      });
    });
    
    it("calculates the percentage of each phase", function (done) {
      areasService.getProjectAreasByPhase(projectA)
        .then(function (result) {

          expect(Object.keys(result.groups)).toEqual(["1", "2"]);

          expect(result.percentage).toEqual(50);

          done();
        });

      scope.$apply();
    });

    it("calculates if phase is delivered", function (done) {
      var components = [
        {
          "project": "a",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "a",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];

      areasService.getProjectAreasByPhase(projectA, components)
        .then(function (result) {
          expect(result.groups["1"].delivered).toEqual(false);
          expect(result.groups["1"].components["Walls1"].delivered).toEqual(false);

          done();
        });


      scope.$apply();
    });

    it("returns areas grouped by phase and component and sorted by unpublished delivery date", function (done) {

      var components = [
        {
          "project": "a",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "a",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];

      areasService.getProjectAreasByPhase(projectA, components)
        .then(function (result) {
          expect(Object.keys(result.groups)).toEqual(["1", "2"]);

          expect(result.groups["1"].components["Walls1"].areas.length).toEqual(2);
          expect(result.groups["1"].components["Walls1"].areas[0]).toEqual(jasmine.objectContaining(area1));
          expect(result.groups["1"].components["Walls1"].areas[1]).toEqual(jasmine.objectContaining(area3));

          expect(result.groups["2"].components["Floor1"].areas.length).toEqual(1);
          expect(result.groups["2"].components["Floor1"].areas[0]).toEqual(jasmine.objectContaining(area2));

          done();
        });

      scope.$apply();
    });
  });

  describe("getProjectAreasByWeek", function () {

    beforeEach(function () {
      areasRef.set({
        "area1": area1,
        "area2": area2,
        "area3": area3
      });
    });

    it("calculates the percentage of each week", function (done) {
      areasService.getProjectAreasByWeek(projectA)
        .then(function (result) {

          expect(result.percentage).toEqual(50);

          done();
        });

      scope.$apply();
    });
    
    it("calculates if week is delivered", function (done) {
      var components = [
        {
          "project": "a",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "a",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];

      areasService.getProjectAreasByWeek(projectA, components)
        .then(function (result) {
          expect(result.groups["2016-09-05"].delivered).toEqual(false);
          expect(result.groups["2016-09-05"].components["Walls1"].delivered).toEqual(false);

          done();
        });


      scope.$apply();
    });

    it("returns areas grouped by week and sorted by unpublished delivery date", function (done) {

      var components = [
        {
          "project": "a",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "a",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];

      areasService.getProjectAreasByWeek(projectA, components)
        .then(function (result) {

          expect(Object.keys(result.groups)).toEqual(["2016-09-05", "2016-10-03"]);

          expect(result.groups["2016-09-05"].components["Walls1"].areas.length).toEqual(2);
          expect(result.groups["2016-09-05"].components["Walls1"].areas[0]).toEqual(jasmine.objectContaining(area1));
          expect(result.groups["2016-09-05"].components["Walls1"].areas[1]).toEqual(jasmine.objectContaining(area3));

          expect(result.groups["2016-10-03"].components["Floor1"].areas.length).toEqual(1);
          expect(result.groups["2016-10-03"].components["Floor1"].areas[0]).toEqual(jasmine.objectContaining(area2));

          done();
        });

      scope.$apply();
    });
  });

  xdescribe("getProjectAreasByPhase", function () {

    /*
     Given a project with the following estimated areas:
     Int: 100

     And 2 phases with the following areas:

     P1-GF-Int
     P1-1F-Int

     P2-GF-Int

     Then the estimated areas will be:

     P1-GF-Int:   25m2
     P1-1F-Int:   25m2

     P2-GF-Int:   50m2

     */

    beforeEach(function () {
      projectA.estimatedAreas = {
        "type": 100
      };
      
      projectA.datumType = "Education 1 Storey";

      areasRef.set({
        "area1": area1,
        "area2": area2,
        "area3": area3
      });
      
      datumsRef.set({
        DATUM1: {
          name: "Education 1 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM2: {
          name: "Education 2 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM3: {
          name: "Education 3 Storey",
          panelAreaTotal: "300",
          panelAvg: "6",
          panelAvgOverride: "0",
          panelCount: "50"
        }
      });
    });

    var components = [
      {
        "project": "a",
        "areas": {
          "area1": true,
          "area3": true
        },
        "type": "Walls"
      },
      {
        "project": "a",
        "areas": {
          "area2": true
        },
        "type": "Floor"
      }
    ];

    it("divides the projects estimated areas among the areas according to group", function (done) {
      areasService.getProjectAreasByPhase(projectA, components)
        .then(function (result) {
          expect(Object.keys(result.groups)).toEqual(["1", "2"]);

          expect(result.groups["1"].components["Walls1"].areas.length).toEqual(2);
          expect(result.groups["1"].components["Walls1"].areas[0].estimatedArea).toEqual(25);
          expect(result.groups["1"].components["Walls1"].areas[1].estimatedArea).toEqual(25);

          expect(result.groups["2"].components["Floor1"].areas.length).toEqual(1);
          expect(result.groups["2"].components["Floor1"].areas[0].estimatedArea).toEqual(50);

          done();
        });

      scope.$apply();
    });
  });

  xdescribe("getProjectAreasByWeek", function () {

    /*
     Given a project with the following estimated areas:
     Int: 100

     And 2 phases with the following areas:

     P1-GF-Int
     P1-1F-Int

     P2-GF-Int

     Then the estimated areas will be:

     P1-GF-Int:   25m2
     P1-1F-Int:   25m2

     P2-GF-Int:   50m2

     */

    beforeEach(function () {
      projectA.estimatedAreas = {
        "type": 100
      };
      
      projectA.datumType = "Education 1 Storey";
      
      areasRef.set({
        "area1": area1,
        "area2": area2,
        "area3": area3
      });
      
      datumsRef.set({
        DATUM1: {
          name: "Education 1 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM2: {
          name: "Education 2 Storey",
          panelAreaTotal: "168",
          panelAvg: "2",
          panelAvgOverride: "0",
          panelCount: "77"
        },
        DATUM3: {
          name: "Education 3 Storey",
          panelAreaTotal: "300",
          panelAvg: "6",
          panelAvgOverride: "0",
          panelCount: "50"
        }
      });
    });

    var components = [
      {
        "project": "a",
        "areas": {
          "area1": true,
          "area3": true
        },
        "type": "Walls"
      },
      {
        "project": "a",
        "areas": {
          "area2": true
        },
        "type": "Floor"
      }
    ];

    it("divides the projects estimated areas among the areas according to group", function (done) {
      areasService.getProjectAreasByWeek(projectA, components)
        .then(function (result) {
          expect(Object.keys(result.groups)).toEqual(["2016-09-05", "2016-10-03"]);

          expect(result.groups["2016-09-05"].components["Walls1"].areas[0].estimatedArea).toEqual(25);
          expect(result.groups["2016-09-05"].components["Walls1"].areas[1].estimatedArea).toEqual(25);

          expect(result.groups["2016-10-03"].components["Floor1"].areas[0].estimatedArea).toEqual(50);

          done();
        });

      scope.$apply();
    });
  });
  
  describe("createProjectArea", function () {
    it("adds new project area and sets active flag to true", function () {

      areasService
        .createProjectArea(
          projectA,
        {description: "An area",
          floor: "1",
          phase: "1",
          project: "a",
          supplier: "Innovaré",
          type: "type"},
        "2016-03-01"
        );

      scope.$apply();
      var createdArea = utils.getRefChild(areasRef, 0);
      expect(createdArea.active).toBe(true);
    });

    it("adds new project area and sets timestamps", function (done) {

      areasService
        .createProjectArea(
          projectA,
        {description: "An area",
          floor: "1",
          phase: "1",
          project: "a",
          supplier: "Innovaré",
          type: "type"},
        "2016-03-01"
        )
        .then(function () {
          var createdArea = utils.getRefChild(areasRef, 0);
          expect(createdArea).toEqual(jasmine.objectContaining({
            timestamps: {
              created: systemTimestamp,
              modified: systemTimestamp
            }
          }));

          done();
        });

      scope.$apply();
    });

    it("adds project's unpublished revision ref to new area", function (done) {
      areasService
        .createProjectArea(
          projectA,
        {description: "An area",
          floor: "1",
          phase: "1",
          project: "a",
          supplier: "Innovaré",
          type: "type"},
        "2016-03-01"
        )
        .then(function () {
          var createdArea = utils.getRefChild(areasRef, 0);
          expect(createdArea.revisions)
            .toEqual(jasmine.objectContaining({
              "unpublishedRevisionRef": "2016-03-01"
            }));

          done();
        });

      scope.$apply();
    });

    it("audits add area", function (done) {
      areasService
        .createProjectArea(
          projectA,
        {description: "An area",
          floor: "1",
          phase: "1",
          project: "a",
          supplier: "Innovaré",
          type: "type"},
        "2016-03-01"
        )
        .then(function () {
          expect(mockAuditService.project.calls.count()).toBe(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Added area '1-1-type'", "a"]);

          done();
        });

      scope.$apply();
    });
  });

  describe("getProjectArea", function () {
    beforeEach(function () {
      area1.$id = "area1FirebaseKey";

      areasRef.set({
        "area1FirebaseKey": area1
      });
    });

    it("returns the requested existing project area", function () {
      var area = areasService.getProjectArea("area1FirebaseKey");

      scope.$apply();
      expect(area.$id).toEqual("area1FirebaseKey");
      expect(area).toEqual(jasmine.objectContaining(area1));
    });
  });

  describe("updateProjectArea", function () {

    beforeEach(function () {
      area1.$id = "area1";

      areasRef.set({
        "area1": area1
      });
    });

    it("updates existing project area", function (done) {
      var area1 = angular.copy(utils.getRefChild(areasRef, 0));

      area1.description = "new description";
      area1.supplier = "new supplier";
      area1.estimatedPanels = "30";
      area1.estimatedArea = "120";

      areasService
        .updateProjectArea(area1)
        .then(function () {
          var updatedArea = utils.getRefChild(areasRef, 0);

          expect(updatedArea.description).toEqual("new description");
          expect(updatedArea.supplier).toEqual("new supplier");
          expect(updatedArea.estimatedPanels).toEqual("30");
          expect(updatedArea.estimatedArea).toEqual("120");

          done();
        });

      scope.$apply();
    });

    it("updates timestamps on existing project area", function (done) {
      var area1 = angular.copy(utils.getRefChild(areasRef, 0));

      area1.description = "new description";

      areasService
        .updateProjectArea(area1)
        .then(function () {
          var updatedArea = utils.getRefChild(areasRef, 0);

          expect(updatedArea.timestamps).toEqual({
            created: area1.timestamps.created,
            modified: systemTimestamp
          });

          done();
        });

      scope.$apply();
    });

    it("audits area updates", function (done) {
      var area1 = angular.copy(utils.getRefChild(areasRef, 0));

      area1.description = "new description";

      areasService
        .updateProjectArea(area1)
        .then(function () {
          expect(mockAuditService.project.calls.count()).toBe(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Updated area '1-1-type'", "a"]);

          done();
        });

      scope.$apply();
    });
  });

  describe("addRevisionToProjectAreas", function () {
    var area2;

    beforeEach(function () {
      area1.$id = "area1";
      area1.revisions = {
        "revision1": "2016-02-01",
        "revision2": "2016-02-05"
      };

      area2 = {
        $id: "area2",
        phase: "1",
        floor: "1",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "revision2": "2016-02-01"
        }
      };

      areasRef.set({
        "area1": area1,
        "area2": area2
      });
    });

    it("Adds new revision to all areas for a project and copies latest date", function (done) {
      areasService.addRevisionToProjectAreas("a", "revision3")
        .then(function () {
          var area1 = utils.getRefChild(areasRef, 0);
          var area2 = utils.getRefChild(areasRef, 1);
          expect(area1.revisions).toEqual(jasmine.objectContaining({
            "revision1": "2016-02-01",
            "revision2": "2016-02-05",
            "revision3": "2016-02-05"
          }));

          expect(area2.revisions).toEqual(jasmine.objectContaining({
            "revision2": "2016-02-01",
            "revision3": "2016-02-01"
          }));

          done();
        });

      scope.$apply();
    });
  });

  describe("modifyCompletedPanels", function () {

    beforeEach(function () {
      area1.$id = "area1";
      area1.completedPanels = 0;
      area1.completedArea = 0;
    });

    beforeEach(function () {
      areasRef.set({
        "area1": area1
      });
    });

    it("Changes the completed panel count by the specified amount", function () {
      areasService.modifyCompletedPanels("area1", 2, 0);

      scope.$apply();
      var area1 = utils.getRefChild(areasRef, 0);
      expect(area1.completedPanels).toEqual(2);

      areasService.modifyCompletedPanels("area1", 1, 0);

      scope.$apply();
      area1 = utils.getRefChild(areasRef, 0);
      expect(area1.completedPanels).toEqual(1);
    });

    it("Changes the completed panel area by the specified amount", function () {
      areasService.modifyCompletedPanels("area1", 0, 10);

      scope.$apply();
      var area1 = utils.getRefChild(areasRef, 0);
      expect(area1.completedArea).toEqual(10);

      areasService.modifyCompletedPanels("area1", 0, 6);

      scope.$apply();
      area1 = utils.getRefChild(areasRef, 0);
      expect(area1.completedArea).toEqual(6);
    });

    it("Updates the area's modified timestamp", function () {
      areasService.modifyCompletedPanels("area1", 0, 6);

      scope.$apply();
      var area1 = utils.getRefChild(areasRef, 0);
      expect(area1.timestamps["modified"]).toEqual(systemTimestamp);
    });
  });

  describe("delete", function () {

    beforeEach(function () {
      areasRef.set({
        "area1": area1
      });
    });

    it("removes area with no panels", function (done) {
      panelsRef.set({});

      areasService.delete("area1")
        .then(function (response) {
          expect(response.status).toEqual("OK");
          done();
        });

      scope.$apply();
    });

    it("prompts for confirmation when deleting an area with panels", function (done) {
      panelsRef.set({
        "panel1": {
          "area": "area1",
          "id": "panel1"
        }
      });

      areasService.delete("area1")
        .then(function (response) {
          expect(response.status).toEqual("PANELS_CONFIRM");
          expect(angular.isDefined(response.token)).toBe(true);
          done();
        });

      scope.$apply();
    });

    it("removes area and associated panels when provided with confirmation", function (done) {
      panelsRef.set({
        "panel1": {
          "area": "area1",
          "id": "panel1"
        }
      });

      areasService.delete("area1")
        .then(function (response) {
          areasService.delete("area1", response.token)
            .then(function (response) {
              expect(response.status).toEqual("OK");
              done();
            });
        });

      scope.$apply();
    });

    it("refuses to delete area with panels that are inprogress", function (done) {
      panelsRef.set({
        "panel1": {
          "area": "area1",
          "id": "panel1",
          "qa": {
            "started": 1462875089729
          }
        }
      });

      areasService.delete("area1")
        .then(function (response) {
          expect(response.status).toEqual("PANELS_STARTED");
          done();
        });

      scope.$apply();
    });

    it("refuses to delete area with panels that are complete", function (done) {
      panelsRef.set({
        "panel1": {
          "area": "area1",
          "id": "panel1",
          "qa": {
            "started": 1462875089729,
            "completed": 1462880881186
          }
        }
      });

      areasService.delete("area1")
        .then(function (response) {
          expect(response.status).toEqual("PANELS_STARTED");
          done();
        });

      scope.$apply();
    });
  });

  describe("getEarliestDeliveryDate", function () {

    var area2;

    beforeEach(function () {
      area1.revisions = {
        "revision1": "2016-02-01",
        "revision2": "2016-02-05"
      };

      area2 = {
        phase: "1",
        floor: "1",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "revision2": "2016-02-02"
        }
      };

      areasRef.set({
        "area1": area1,
        "area2": area2
      });
    });

    it("returns the earliest delivery date of single area in a revision", function (done) {

      areasService.getEarliestDeliveryDate("a", "revision1")
        .then(function (earliestDate) {
          expect(earliestDate).toEqual("2016-02-01");

          done();
        });

      scope.$apply();
    });

    it("returns the earliest delivery date of all areaa in a revision", function (done) {

      areasService.getEarliestDeliveryDate("a", "revision2")
        .then(function (earliestDate) {
          expect(earliestDate).toEqual("2016-02-02");

          done();
        });

      scope.$apply();
    });
  });
  
  describe("getEarliestDeliveryDateAfterToday", function () {

    var area2;

    beforeEach(function () {
      area1.revisions = {
        "revision1": "2022-02-01",
        "revision2": "2022-02-05",
        "revision3": "2022-08-05"
      };

      area2 = {
        phase: "1",
        floor: "1",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "revision2": "2022-08-05"
        }
      };

      areasRef.set({
        "area1": area1,
        "area2": area2
      });
    });

    it("returns the earliest delivery date after today of single area in a revision", function (done) {

      areasService.getEarliestDeliveryDateAfterToday("a", "revision1")
        .then(function (earliestDate) {
          expect(earliestDate).toEqual("2022-02-01");

          done();
        });

      scope.$apply();
    });

    it("returns the earliest delivery date after today of all areaa in a revision", function (done) {

      areasService.getEarliestDeliveryDateAfterToday("a", "revision2")
        .then(function (earliestDate) {
          expect(earliestDate).toEqual("2022-02-05");

          done();
        });

      scope.$apply();
    });
  });

});
