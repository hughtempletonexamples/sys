"use strict";

var PROJ1_KEY = "PROJKEYA";
var PROJ2_KEY = "PROJKEYB";

var utils = require("../testUtils");

describe("project filter", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;

  var projectFilter;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.projects"));

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _projectFilter_) {
    scope = $rootScope;
    projectFilter = _projectFilter_;
  }));

  beforeEach(function () {
    firebaseRoot.child("projects").child(PROJ1_KEY).set(
      {
        id: "001"
      }
    );
    firebaseRoot.child("projects").child(PROJ2_KEY).set(
      {
        id: "002"
      }
    );
  });

  it("should be stateful to allow for async lookup", function () {
    expect(projectFilter.$stateful).toEqual(true);
  });

  it("should look up project id", function () {
    // Value before data loaded
    expect(projectFilter(PROJ1_KEY)).toEqual("...");
    expect(projectFilter(PROJ2_KEY)).toEqual("...");

    scope.$apply();

    // Expected value
    expect(projectFilter(PROJ1_KEY)).toEqual("001");
    expect(projectFilter(PROJ2_KEY)).toEqual("002");
  });
});
