"use strict";

var utils = require("../testUtils");
describe("components service", function () {

  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;
  var componentsRef;

  var scope;

  var componentsService;

  beforeEach(function () {
    angular.mock.module("innView.projects", function ($provide) {
      $provide.value("$log", console);
    });

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    componentsRef = firebaseRoot.child("components");
    utils.addQuerySupport(componentsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);

    inject(function ($rootScope, _componentsService_) {
      scope = $rootScope;
      componentsService = _componentsService_;
    });
  });

  describe("create", function () {

    it("adds new component", function (done) {
      var createdAreaIds = ["area1", "area2", "area3"];
      
      componentsService.create("project1", createdAreaIds, "Walls")
        .then(function (component) {
          utils.expectRefChildToContain(componentsRef, 0, {
            project: "project1",
            areas: {
              area1: true,
              area2: true,
              area3: true
            },
            type: "Walls"
          });
          done();
        });

      scope.$apply();
    });
  });
  
  describe("updateComponentNote", function () {

    it("updates existing datum", function (done) {

      componentsRef.set({
        "component1": {
          "project": "projectA",
          "areas": {
            "area1": true,
            "area2": true
          },
          "type": "type1"
        }
      });

      var component = {
        "project": "projectA",
        "areas": {
          "area1": true,
          "area2": true
        },
        "type": "type1",
        "comments": "This is component 3"
      };

      var notes = "This is component 3";
      var componentId = "component1";

      componentsService.updateComponentNote(componentId, notes)
        .then(function () {
          var existingComponent = utils.getRefChild(componentsRef, 0);
          expect(existingComponent).toEqual(jasmine.objectContaining(component));
          done();
        });

      scope.$apply();
    });
  });

  describe("getComponentsForProject", function () {
    beforeEach(function () {
      componentsRef.set({
        "component1": {
          "project": "projectA",
          "areas": {
            "area1": true,
            "area2": true
          },
          "type": "type1"
        },
        "component2": {
          "project": "projectB",
          "areas": {
            "area3": true
          },
          "type": "type1"
        },
        "component3": {
          "project": "projectA",
          "areas": {
            "area4": true,
            "area5": true
          },
          "type": "type2"
        }
      });
    });

    it("returns all components for the specified project", function (done) {
      componentsService.getComponentsForProject("projectA")
        .$loaded()
        .then(function (components) {
          expect(components.length).toBe(2);

          expect(components[0]).toEqual(jasmine.objectContaining({
            "areas": {
              "area1": true,
              "area2": true
            },
            "project": "projectA",
            "type": "type1"
          }));

          expect(components[1]).toEqual(jasmine.objectContaining({
            "areas": {
              "area4": true,
              "area5": true
            },
            "project": "projectA",
            "type": "type2"
          }));

          done();
        });

      scope.$apply();
    });
  });
});
