"use strict";

var utils = require("../testUtils");

describe("panels service", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var createdTimestamp;
  var systemTimestamp;

  var firebaseRoot;
  var projectsRef;
  var areasRef;
  var panelsRef;
  var mockAreasService;
  var mockProjectsService;
  var mockAuditService;

  var scope;

  var panelsService;

  beforeEach(angular.mock.module("innView.projects", function ($provide) {
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
    createdTimestamp = systemTimestamp - 86543210;
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    projectsRef = firebaseRoot.child("projects");
    areasRef = firebaseRoot.child("areas");
    panelsRef = firebaseRoot.child("panels");

    utils.addQuerySupport(panelsRef);

    mockAreasService = jasmine.createSpyObj("areasService", ["getProjectAreas", "getProjectArea", "updateProjectArea", "modifyCompletedPanels"]);

    mockProjectsService = jasmine.createSpyObj("projectsService", ["touch"]);

    mockAuditService = jasmine.createSpyObj("auditsService", ["project"]);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("areasService", function () {
        return mockAreasService;
      });
      $provide.factory("projectsService", function () {
        return mockProjectsService;
      });
      $provide.factory("audit", function () {
        return mockAuditService;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _panelsService_) {
    scope = $rootScope;
    panelsService = _panelsService_;
  }));

  beforeEach(inject(function ($q, $firebaseArray) {

    projectsRef.set({
      "PROJ1": {
        "id": "214",
        "name": "Project One",
        "timestamps": {
          "created": createdTimestamp,
          "modified": createdTimestamp
        }
      }
    });

    var area1 = {
      "project": "PROJ1",
      "timestamps": {
        "created": createdTimestamp,
        "modified": createdTimestamp
      }
    };

    areasRef.set({
      "PROJ1AREA1": area1
    });

    mockAreasService.modifyCompletedPanels.and.returnValue($q.when(area1));

    mockAreasService.getProjectAreas.and.callFake(function (y) {
      return $firebaseArray(areasRef);
    });
  }));
  
  xdescribe("panelsByCompleted", function () {

    var startedTimestamp = 1509494400;
    var completedTimestamp = 1512086340;

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1",
      qa: {
        completed: 1510380000
      }
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1",
      qa: {
        completed: 1511164800
      }
    };
    //11/21/2017 @ 4:00pm (UTC)
    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA2",
      qa: {
        completed: 1511280000
      }
    };

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA2PANEL1": panel3
      });
    });

    it("returns all panels completed in date range", function () {
      var result = panelsService.panelsByCompleted(startedTimestamp, completedTimestamp);

      scope.$apply();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(jasmine.objectContaining(panel1));
      expect(result[1]).toEqual(jasmine.objectContaining(panel2));
      expect(result[2]).toEqual(jasmine.objectContaining(panel3));
    });
  });

  describe("panelsForArea", function () {

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1"
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1"
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA2"
    };

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA2PANEL1": panel3
      });
    });

    it("returns all panels for a given area", function () {
      var result = panelsService.panelsForArea("PROJ1AREA1");

      scope.$apply();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(jasmine.objectContaining(panel1));
      expect(result[1]).toEqual(jasmine.objectContaining(panel2));
    });
  });
  
  describe("panelsForProject", function () {

    var panel1 = {
      id: "panel1",
      project: "PROJ1"
    };

    var panel2 = {
      id: "panel2",
      project: "PROJ1"
    };

    var panel3 = {
      id: "panel3",
      project: "PROJ2"
    };

    beforeEach(function () {
      panelsRef.set({
        "PROJ1PANEL1": panel1,
        "PROJ1PANEL2": panel2,
        "PROJ2PANEL1": panel3
      });
    });

    it("returns all panels for a given project", function () {
      var result = panelsService.panelsForProject("PROJ1");

      scope.$apply();
      expect(result.length).toEqual(2);
      expect(result[0]).toEqual(jasmine.objectContaining(panel1));
      expect(result[1]).toEqual(jasmine.objectContaining(panel2));
    });
  });

  describe("markPanelIncomplete", function () {
    var startedTimestamp = 1458422000000;
    var completedTimestamp = 1458432000000;

    var panel1;
    var areaRef;
    var panelsFirebaseArray;

    beforeEach(function () {
      panel1 = {
        id: "panel1",
        project: "PROJ1",
        area: "PROJ1AREA1",
        qa: {
          started: startedTimestamp,
          completed: completedTimestamp
        },
        timestamps: {
          created: createdTimestamp,
          modified: completedTimestamp
        },
        dimensions: {
          length: 4,
          height: 2,
          width: 1,
          area: 8,
          weight: 1
        }
      };
    });

    beforeEach(inject(function ($q, $firebaseObject) {
      areaRef = firebaseRoot.child("areas/PROJ1AREA1");
      mockAreasService.getProjectArea.and.returnValue(
        $firebaseObject(areaRef)
      );
      areaRef.set({
        "phase": "P1",
        "floor": "GF",
        "type": "Ext",
        "estimatedArea": 10,
        "estimatedPanels": 1,
        "actualArea": 8,
        "actualPanels": 1,
        "completedArea": 8,
        "completedPanels": 1,
        "revisions": {
          "PROJ1REV1": "2016-04-10"
        }
      });
    }));

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1
      });
    });

    beforeEach(inject(function ($firebaseArray) {
      panelsFirebaseArray = $firebaseArray(panelsRef);

      scope.$apply();

      spyOn(panelsFirebaseArray, "$save");
    }));

    it("removes completed timestamp and updates modified timestamp on the specified panel", function () {

      panelsService
        .markPanelIncomplete(panelsFirebaseArray, "PROJ1AREA1PANEL1");

      scope.$apply();

      expect(panelsFirebaseArray.$save).toHaveBeenCalledWith(jasmine.objectContaining({
        id: "panel1",
        timestamps: {
          created: createdTimestamp,
          modified: firebase.database.ServerValue.TIMESTAMP
        }
      }));
    });

    it("invokes area service to update panel totals", function () {
      panelsService
        .markPanelIncomplete(panelsFirebaseArray, "PROJ1AREA1PANEL1");

      scope.$apply();

      expect(mockAreasService.modifyCompletedPanels).toHaveBeenCalled();
    });

    it("audits", function () {
      projectsRef.set({
        "PROJ1": {
          "id": "214"
        }
      });

      panelsService
        .markPanelIncomplete(panelsFirebaseArray, "PROJ1AREA1PANEL1");

      scope.$apply();

      expect(mockAuditService.project.calls.count()).toEqual(1);
      expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Marked 1 panel incomplete", "PROJ1"]);
    });
  });

  describe("markPanelsComplete", function () {

    var panel1;
    var panel2;

    beforeEach(function () {
      panel1 = {
        id: "panel1",
        area: "PROJ1AREA1",
        project: "PROJ1",
        timestamps: {
          created: createdTimestamp
        },
        dimensions: {
          length: 4,
          height: 2,
          width: 1,
          area: 8,
          weight: 1
        }
      };

      panel2 = {
        id: "panel2",
        complete: false,
        area: "PROJ1AREA1",
        project: "PROJ1",
        timestamps: {
          created: createdTimestamp
        },
        dimensions: {
          length: 3,
          height: 2,
          width: 1,
          area: 6,
          weight: 1
        }
      };
    });

    var areaRef;

    beforeEach(inject(function ($firebaseObject) {
      areaRef = firebaseRoot.child("areas/PROJ1AREA1");
      mockAreasService.getProjectArea.and.returnValue(
        $firebaseObject(areaRef)
      );
      areaRef.set({
        "phase": "P1",
        "floor": "GF",
        "type": "Ext",
        "estimatedArea": 100,
        "estimatedPanels": 20,
        "actualArea": 95,
        "actualPanels": 15,
        "completedArea": 0,
        "completedPanels": 0,
        "revisions": {
          "PROJ1REV1": "2016-04-10"
        }
      });
    }));

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2
      });


      var panelRef = firebaseRoot.child("panels/PROJ1AREA1PANEL1");
      panelRef.set(panel1);
    });

    it("sets completed and modified timestamps on the specified panels", function (done) {
      var completedDate = "2016-03-20";
      var completedTimestamp = 1458432000000;

      var savedObjects = [];

      panelsService.markPanelsComplete(
        "PROJ1AREA1",
        ["PROJ1AREA1PANEL1", "PROJ1AREA1PANEL2"],
        completedDate)
        .then(function () {
          utils.expectRefChildToContain(panelsRef, 0, {
            qa: {
              started: completedTimestamp,
              completed: completedTimestamp
            },
            timestamps: {
              created: createdTimestamp,
              modified: systemTimestamp
            }
          });

          utils.expectRefChildToContain(panelsRef, 1, {
            qa: {
              started: completedTimestamp,
              completed: completedTimestamp
            },
            timestamps: {
              created: createdTimestamp,
              modified: systemTimestamp
            }
          });

          done();
        });

      scope.$apply();
    });

    it("invokes modifyCompletedPanels on areasService", function (done) {
      var completedDate = "2016-03-20";
      var completedTimestamp = 1458432000000;

      panelsService.markPanelsComplete("PROJ1AREA1", ["PROJ1AREA1PANEL1"], completedDate)
        .then(function () {
          expect(mockAreasService.modifyCompletedPanels).toHaveBeenCalled();

          done();
        });

      scope.$apply();
    });

    it("audits", function (done) {
      var completedDate = "2016-03-20";
      var completedTimestamp = 1458432000000;

      panelsService.markPanelsComplete("PROJ1AREA1", ["PROJ1AREA1PANEL1", "PROJ1AREA1PANEL2"], completedDate)
        .then(function () {
          expect(mockAuditService.project.calls.count()).toEqual(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Marked 2 panels complete", "PROJ1"]);

          done();
        });

      scope.$apply();
    });

  });

  describe("updatePanels", function () {

    it("saves new panels", function (done) {

      var panel = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          length: 11, height: 22, width: 3, area: 0.4, weight: 5
        }
      };
      
      var updatePanelsSheet = false;

      panelsService.updatePanels([panel], updatePanelsSheet)
        .then(function (result) {
          var savedPanel = utils.getRefChild(panelsRef, 0);
          expect(savedPanel).toEqual(jasmine.objectContaining(panel));
          expect(savedPanel.timestamps).toEqual({created: systemTimestamp, modified: systemTimestamp});

          expect(result.total).toEqual(1);
          expect(result.details).toEqual(
            [
              {projectId: "214", projectName: "Project One", count: 1}
            ]
          );

          done();
        });

      scope.$apply();
    });

    it("updates existing panels", function (done) {

      panelsRef.set({
        PANEL1 : {
          id: "A",
          project: "PROJ1",
          area: "PROJ1AREA1",
          timestamps: {
            created: createdTimestamp,
            modified: createdTimestamp
          }
        }
      });

      var panel = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          length: 11, height: 22, width: 3, area: 0.4, weight: 5
        }
      };
      
      var updatePanelsSheet = false;

      panelsService.updatePanels([panel], updatePanelsSheet)
        .then(function (result) {
          var savedPanel = utils.getRefChild(panelsRef, 0);
          expect(savedPanel).toEqual(jasmine.objectContaining(panel));
          expect(savedPanel.timestamps).toEqual({created: createdTimestamp, modified: systemTimestamp});

          expect(result.total).toEqual(1);
          expect(result.details).toEqual(
            [
              {projectId: "214", projectName: "Project One", count: 1}
            ]
          );

          done();
        });

      scope.$apply();
    });

    it("updates modified timestamp on project and area", function (done) {

      var panel = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          length: 11, height: 22, width: 3, area: 0.4, weight: 5
        }
      };

      var updatePanelsSheet = false;

      panelsService.updatePanels([panel], updatePanelsSheet)
        .then(function (result) {
          var updatedProject = projectsRef.getData()["PROJ1"];
          var updatedArea = areasRef.getData()["PROJ1AREA1"];

          expect(updatedProject.timestamps).toEqual({
            created: createdTimestamp,
            modified: systemTimestamp
          });

          expect(updatedArea.timestamps).toEqual({
            created: createdTimestamp,
            modified: systemTimestamp
          });

          done();
        });

      scope.$apply();
    });

    it("audits panel updates", function (done) {

      var panel1 = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {area: 1}
      };
      var panel2 = {
        id: "B",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {area: 1}
      };
      
      var updatePanelsSheet = false;

      panelsService.updatePanels([panel1, panel2], updatePanelsSheet)
        .then(function (result) {
          expect(mockAuditService.project.calls.count()).toEqual(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Loaded 2 panels", "PROJ1", "214"]);

          done();
        });

      scope.$apply();
    });

    it("updates actual values for panel count and area", function (done) {

      panelsRef.set({
        PANEL1 : {
          id: "A",
          project: "PROJ1",
          area: "PROJ1AREA1",
          timestamps: {
            created: createdTimestamp
          },
          dimensions: {
            area: 0.5
          }
        }
      });
      areasRef.child("PROJ1AREA1").update({
        "actualPanels": 1,
        "actualArea": 0.5
      });
      
      var panel1 = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          area: 0.4
        }
      };

      var panel2 = {
        id: "D",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          area: 0.7
        }
      };
      
      var updatePanelsSheet = false;

      panelsService.updatePanels([panel1, panel2], updatePanelsSheet)
        .then(function (result) {
          var updatedArea = areasRef.getData()["PROJ1AREA1"];

          expect(updatedArea).toEqual(jasmine.objectContaining(
            {
              actualPanels: 2,
              actualArea: 1.1
            }
          ));

          done();
        });

      scope.$apply();
    });
    
    it("updates actual values for panel count and area, checks if other areas are updated after panel moves area", function (done) {

      panelsRef.set({
        PANEL1 : {
          id: "A",
          project: "PROJ1",
          area: "PROJ1AREA1",
          timestamps: {
            created: createdTimestamp
          },
          dimensions: {
            area: 0.5
          }
        },
        PANEL2 : {
          id: "B",
          project: "PROJ1",
          area: "PROJ1AREA2",
          timestamps: {
            created: createdTimestamp
          },
          dimensions: {
            area: 5
          }
        },
        PANEL3 : {
          id: "C",
          project: "PROJ1",
          area: "PROJ1AREA2",
          timestamps: {
            created: createdTimestamp
          },
          dimensions: {
            area: 5
          }
        }
      });
      
      areasRef.child("PROJ1AREA1").update({
        "actualPanels": 1,
        "actualArea": 0.5
      });
      
      areasRef.child("PROJ1AREA2").update({
        "actualPanels": 2,
        "actualArea": 10
      });

      var panel1 = {
        id: "A",
        project: "PROJ1",
        area: "PROJ1AREA2",
        dimensions: {
          area: 0.4
        }
      };

      var panel2 = {
        id: "D",
        project: "PROJ1",
        area: "PROJ1AREA1",
        dimensions: {
          area: 0.7
        }
      };
      
      var updatePanelsSheet = false;

      panelsService.updatePanels([panel1, panel2], updatePanelsSheet)
        .then(function (result) {
          var updatedArea = areasRef.getData()["PROJ1AREA1"];
          var updatedExistingArea = areasRef.getData()["PROJ1AREA2"];

          expect(updatedArea).toEqual(jasmine.objectContaining(
            {
              actualPanels: 1,
              actualArea: 0.7
            }
          ));
        
          expect(updatedExistingArea).toEqual(jasmine.objectContaining(
            {
              actualPanels: 3,
              actualArea: 10.4
            }
          ));

          done();
        });

      scope.$apply();
    });

  });

  describe("getPanelById", function () {

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1"
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1"
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA2"
    };

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA2PANEL1": panel3
      });
    });

    it("returns the specified panel", function (done) {
      panelsService.getPanelById("panel2")
        .$loaded()
        .then(function (panels) {
          expect(panels[0]).toEqual(jasmine.objectContaining(panel2));
          done();
        });

      scope.$apply();
    });
  });

  describe("deletePanelsForArea", function () {

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1"
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1"
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA2"
    };

    beforeEach(function () {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA2PANEL1": panel3
      });
    });

    it("deletes panels for specified area ref", function (done) {
      panelsService.deletePanelsForArea("PROJ1AREA1")
        .then(function () {
          utils.expectRefChildToContain(panelsRef, 0, {
            id: "panel3",
            area: "PROJ1AREA2"
          });
          done();
        });

      scope.$apply();
    });
  });
  
  describe("deletePanels", function () {

    var createdTimestamp = 1458732564115;

    var panel1 = {
      id: "panel1",
      area: "PROJ1AREA1",
      project: "PROJ1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel2 = {
      id: "panel2",
      area: "PROJ1AREA1",
      project: "PROJ1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panel3 = {
      id: "panel3",
      area: "PROJ1AREA1",
      project: "PROJ1",
      timestamps: {
        created: createdTimestamp
      },
      dimensions: {
        length: 1,
        height: 2,
        width: 1,
        area: 2,
        weight: 1
      }
    };

    var panelsFirebaseArray;

    beforeEach(inject(function ($firebaseArray) {
      panelsRef.set({
        "PROJ1AREA1PANEL1": panel1,
        "PROJ1AREA1PANEL2": panel2,
        "PROJ1AREA1PANEL3": panel3
      });

      panelsFirebaseArray = $firebaseArray(panelsRef);
      scope.$apply();
    }));

    it("deletes panels that were selected", function (done) {
      var selection = {
        panel1: false,
        panel2: true,
        panel3: true
      };

      panelsService.deletePanels(panelsFirebaseArray, selection)
        .then(function () {
          utils.expectRefChildToContain(panelsRef, 0, {
            id: "panel1",
            area: "PROJ1AREA1"
          });
          done();
        });

      scope.$apply();
      
    });

    it("uploads images", function () {

      var errorDate = "2016-03-20";
      
      jasmine.clock().uninstall();
      var now = new Date("2016-03-20");
      jasmine.clock().install();
      jasmine.clock().mockDate(now);

      systemTimestamp = now.getTime();

      
       
      panelsService.uploadToDb("panel1","cihwukcgkg","error1Name")
        .then(function (result) {
          expect(result["error1Name"]).toEqual({"panel":"panel1","error":"cihwukcgkg","date":errorDate});
        });

      scope.$apply();
      
    });
    
    it("audits", function (done) {
      var completedDate = "2016-03-20";
      var completedTimestamp = 1458432000000;
      
      var selection = {
        panel1: false,
        panel2: true,
        panel3: true
      };

      panelsService.deletePanels(panelsFirebaseArray, selection)
        .then(function () {
          expect(mockAuditService.project.calls.count()).toEqual(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Deleted 2 panels", "PROJ1"]);

          done();
        });

      scope.$apply();
    });
  });
});
