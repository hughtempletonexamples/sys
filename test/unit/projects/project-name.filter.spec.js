"use strict";

var PROJ1_KEY = "PROJKEYA";
var PROJ2_KEY = "PROJKEYB";

var utils = require("../testUtils");

describe("projectName filter", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var firebaseRoot;

  var projectNameFilter;
  var scope;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.projects"));

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);

    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot);
  });

  beforeEach(inject(function ($rootScope, _projectNameFilter_) {
    scope = $rootScope;
    projectNameFilter = _projectNameFilter_;
  }));

  beforeEach(function () {
    firebaseRoot.child("projects").child(PROJ1_KEY).set(
      {
        id: "001",
        name: "Project 1"
      }
    );
    firebaseRoot.child("projects").child(PROJ2_KEY).set(
      {
        id: "002",
        name: "Project 2"
      }
    );
  });

  it("should be stateful to allow for async lookup", function () {
    expect(projectNameFilter.$stateful).toEqual(true);
  });

  it("should look up project id", function () {
    // Value before data loaded
    expect(projectNameFilter(PROJ1_KEY)).toEqual("...");
    expect(projectNameFilter(PROJ2_KEY)).toEqual("...");

    scope.$apply();

    // Expected value
    expect(projectNameFilter(PROJ1_KEY)).toEqual("Project 1");
    expect(projectNameFilter(PROJ2_KEY)).toEqual("Project 2");
  });
});
