"use strict";

var utils = require("../testUtils");

describe("projects", function () {

  var MockFirebase = require("firebase-mock").MockFirebase;
  var areasRef;
  var componentsRef;

  var mockProjectService;
  var mockAreaService;
  var mockSchema;
  var mockComponentsService;

  var scope;

  beforeEach(angular.mock.module("innView.projects", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    mockProjectService = jasmine.createSpyObj("projectsService", ["createProject", "getProject", "promoteUnpublishedRevision"]);
    mockAreaService = jasmine.createSpyObj("areasService", ["getProjectAreas", "createProjectArea", "updateProjectArea", "getProjectAreasByPhase", "getProjectAreasByWeek"]);
    mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);
    mockComponentsService = jasmine.createSpyObj("componentsService", ["getComponentsForProject"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("projectsService", function () {
        return mockProjectService;
      });
      $provide.factory("areasService", function () {
        return mockAreaService;
      });
      $provide.factory("componentsService", function () {
        return mockComponentsService;
      });
      $provide.factory("$scope", function () {
        return scope;
      });
      $provide.factory("$route", function () {
        //Need to provide a value to avoid karma errors
        return null;
      });
      $provide.factory("$confirm", function () {
        return function () {
          return $q.when();
        };
      });
    });
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  beforeEach(inject(function ($q, $firebaseArray) {
    var firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
    areasRef = firebaseRoot.child("areas");
    areasRef.set({});

    var areasFirebaseArray = $firebaseArray(areasRef);

    mockAreaService.getProjectAreas.and.returnValue(areasFirebaseArray);
    mockAreaService.getProjectAreasByPhase.and.returnValue($q.when({}));
    mockAreaService.getProjectAreasByWeek.and.returnValue($q.when({}));

    componentsRef = firebaseRoot.child("components");

    mockComponentsService.getComponentsForProject.and.returnValue($firebaseArray(componentsRef));

    utils.setupMockSchema(mockSchema, firebaseRoot);
  }));

  afterEach(function () {
    angular.forEach(mockProjectService, function (spyMethod) {
      spyMethod.calls.reset();
    });
  });

  describe("controller", function () {

    var projectAbc456 = {
      $id: "abc456",
      id: "123",
      name: "A project",
      deliverySchedule: {
        published: "revision1",
        unpublished: "revision2",
        revisions: {
          "revision1": true
        }
      }
    };

    var area1 = {
      $id: "area1",
      phase: "1",
      floor: "1",
      type: "type",
      description: "An area",
      supplier: "Innovaré",
      project: "abc456",
      revisions: {
        "revision1": "2016-04-01"
      }
    };

    beforeEach(inject(function ($q) {
      mockProjectService.createProject.and.returnValue(
        $q.when("123")
      );
    }));

    it("has no project by default", inject(function ($controller) {
      var controller = $controller("ProjectsController");
      expect(controller.project).toEqual(null);
    }));

    it("has no areas by default", inject(function ($controller) {
      var controller = $controller("ProjectsController");
      expect(controller.areas).toEqual(undefined);
    }));

    it("has no area by default", inject(function ($controller) {
      var controller = $controller("ProjectsController");
      expect(controller.area).toEqual(undefined);
    }));

    it("adds create param to url when newProject invoked", inject(function ($controller, $location) {
      var controller = $controller("ProjectsController");

      controller.newProject();

      expect($location.url()).toEqual("/projects?create");
    }));

    it("has default project when invoked with create param", inject(function ($controller) {
      var controller = $controller("ProjectsController", {
        $routeParams: {create: true}
      });

      expect(controller.project).toEqual({});
    }));

    it("loads project when invoked with projectId param", inject(function ($controller, $q, $rootScope) {

      mockProjectService.getProject.and.returnValue(
        $q.when({
          $id: "abc456",
          id: "123",
          name: "A project",
          deliverySchedule: {
            published: "revision1",
            unpublished: "revision2",
            revisions: {
              "revision1": true
            }
          }
        })
      );

      var controller = $controller("ProjectsController", {
        $routeParams: {projectId: "123"}
      });

      $rootScope.$apply();
      expect(controller.project).toEqual({
        $id: "abc456",
        id: "123",
        name: "A project",
        deliverySchedule: {
          published: "revision1",
          unpublished: "revision2",
          revisions: {
            "revision1": true
          }
        }
      });
    }));

    describe("save", function () {
      // Disabling this test until the create form is moved to the new ux as we currently have to do a full page reload
      xit("creates new project when form is valid", inject(function ($controller, $location, $rootScope) {
        var controller = $controller("ProjectsController", {
          $routeParams: {create: true}
        });

        controller.project.id = "123";
        controller.project.name = "New Office";
        controller.project.client = "New Office";
        controller.project.siteAccess = "Site Access";
        controller.project.datumType = "Datum Type";
        controller.project.specialRequirements = "Special Requirements";
        controller.project.siteAddress = "Site Address";
        controller.project.postCode = "Post Code";
        controller.project.siteStart = "2017-02-02";
        controller.project.estimatedAreas = {};
        controller.project.estimatedAreas.Ext = "1";
        controller.project.estimatedAreas.Int = "1";
        controller.project.estimatedAreas.Floor = "1";
        controller.project.estimatedAreas.Roof = "1";

        controller.save(true);

        $rootScope.$apply();
        expect(mockProjectService.createProject).toHaveBeenCalledWith({
          id: "123",
          name: "New Office",
          client: "New Office",
          siteAccess: "Site Access",
          datumType: "Datum Type",
          specialRequirements: "Special Requirements",
          siteAddress: "Site Address",
          postCode: "Post Code",
          siteStart: "2017-02-02",
          estimatedAreas: {
            Ext: "1",
            Int: "1",
            Floor: "1",
            Roof: "1"
          }
        });
        expect($location.url()).toEqual("/projects/123");
      }));

      it("does not create new project when form is invalid", inject(function ($controller) {
        var controller = $controller("ProjectsController", {
          $routeParams: {create: true}
        });

        controller.save(false);
        expect(mockProjectService.createProject.calls.any()).toEqual(false);
      }));

      xit("displays error message if service returns error", inject(function ($controller) {
        //TODO: write test
        // Defer to e2e scenario test?
      }));
    });

    describe("saveArea", function () {
      var controller;

      beforeEach(inject(function ($q) {
        mockProjectService.getProject.and.returnValue(
          $q.when(projectAbc456)
        );
      }));

      describe("for new area", function () {
        beforeEach(inject(function ($q, $controller, $rootScope) {
          mockAreaService.createProjectArea.and.returnValue(
            $q.when("blah")
          );

          controller = $controller("ProjectsController", {
            $routeParams: {
              projectId: projectAbc456.id,
              createArea: true
            }
          });

          controller.area = {};

          $rootScope.$apply();
        }));

        it("creates new area when form is valid", inject(function ($rootScope) {
          controller.area.phase = "1";
          controller.area.floor = "GF";
          controller.area.type = "Ext";
          controller.area.description = "Description";
          controller.area.supplier = "Innovaré";
          controller.deliveryDate = "2016-04-01";

          controller.saveArea(true);

          expect(mockAreaService.createProjectArea)
            .toHaveBeenCalledWith(
              projectAbc456,
            {
              phase: "1",
              floor: "GF",
              type: "Ext",
              description: "Description",
              supplier: "Innovaré",
              project: "abc456"
            },
              "2016-04-01");
        }));

        it("does not create new area when form is invalid", inject(function ($rootScope) {
          controller.area.phase = "1";
          controller.area.floor = "GF";
          controller.area.type = "Ext";
          controller.area.description = "Description";
          controller.area.supplier = "Innovaré";

          controller.saveArea(false);

          expect(mockAreaService.createProjectArea.calls.any()).toEqual(false);
        }));

      });

      describe("for an existing area", function () {

        beforeEach(inject(function ($q, $controller, $rootScope) {
          areasRef.set({
            area1: area1
          });

          projectAbc456.deliverySchedule = {
            unpublished: "revision1"
          };

          controller = $controller("ProjectsController", {
            $routeParams: {
              projectId: projectAbc456.id,
              areaRef: "area1",
              createArea: true
            }
          });

          controller.area = area1;

          $rootScope.$apply();
        }));

        it("updates when form is valid", inject(function ($q, $rootScope) {
          mockAreaService.updateProjectArea.and.returnValue(
            $q.when("area1")
          );

          controller.area.phase = "1";
          controller.area.floor = "1";
          controller.area.type = "type";
          controller.area.description = "New Description";
          controller.area.supplier = "Innovaré";
          controller.deliveryDate = "2016-04-02";

          controller.saveArea(true);

          expect(mockAreaService.createProjectArea.calls.any()).toEqual(false);
          expect(mockAreaService.updateProjectArea)
            .toHaveBeenCalledWith(
            {
              $id: "area1",
              phase: "1",
              floor: "1",
              type: "type",
              description: "New Description",
              supplier: "Innovaré",
              project: "abc456",
              revisions: {
                "revision1": "2016-04-02"
              }
            });
        }));
      });
    });

    describe("publishRevision", function () {

      it("invokes service", inject(function ($q, $controller, $rootScope) {
        projectAbc456.deliverySchedule = {
          unpublished: "revision1"
        };

        mockProjectService.getProject.and.returnValue(
          $q.when(projectAbc456)
        );

        var controller = $controller("ProjectsController", {
          $routeParams: {
            projectId: projectAbc456.id
          }
        });

        mockProjectService.promoteUnpublishedRevision.and.returnValue(
          $q.when(projectAbc456)
        );

        $rootScope.$apply();

        controller.publishRevision();

        $rootScope.$apply();

        expect(mockProjectService.promoteUnpublishedRevision)
          .toHaveBeenCalledWith(projectAbc456.$id);
      }));

    });

    describe("components", function () {
      var components;

      beforeEach(inject(function ($q) {
        mockProjectService.getProject.and.returnValue(
          $q.when({
            $id: "abc456",
            id: "123",
            name: "A project",
            deliverySchedule: {
              unpublished: "revision1"
            }
          })
        );

        componentsRef.set({
          "component1": {
            "project": "projectA",
            "areas": {
              "area4": true,
              "area5": true
            },
            "type": "type2"
          }
        });
      }));

      it("are loaded when projectId param present in url", function ()  {
        inject(function ($controller, $rootScope) {
          var controller = $controller("ProjectsController", {
            $routeParams: {projectId: "123"}
          });

          $rootScope.$apply();
          controller
            .components
            .$loaded()
            .then(function (controllerComponents) {
              expect(controllerComponents.length).toBe(1);
              expect(controllerComponents[0]).toEqual(jasmine.objectContaining({
                "project": "projectA",
                "areas": {
                  "area4": true,
                  "area5": true
                },
                "type": "type2"
              }));
            });
        });
      });
    });

    describe("areas", function () {

      var areas;

      beforeEach(inject(function ($q) {
        mockProjectService.getProject.and.returnValue(
          $q.when({
            $id: "abc456",
            id: "123",
            name: "A project",
            deliverySchedule: {
              unpublished: "revision1"
            }
          })
        );

        areas = {
          area1: {
            phase: "1",
            floor: "1",
            type: "type",
            description: "An area",
            supplier: "Innovaré",
            project: "abc456",
            revisions: {
              "revision1": "2016-04-01"
            }
          },
          area2: {
            phase: "1",
            floor: "2",
            type: "type",
            description: "Another area",
            supplier: "Innovaré",
            project: "abc456",
            revisions: {
              "revision1": "2016-04-10"
            }
          }
        };

        areasRef.set(areas);
      }));

      it("are loaded when projectId param present in url", inject(function ($controller, $rootScope) {
        var controller = $controller("ProjectsController", {
          $routeParams: {projectId: "123"}
        });

        $rootScope.$apply();
        controller
          .areas
          .$loaded()
          .then(function (controllerAreas) {
            expect(controllerAreas[0])
              .toEqual(jasmine.objectContaining(
                {
                  $id: "area1",
                  phase: "1",
                  floor: "1",
                  type: "type",
                  description: "An area",
                  supplier: "Innovaré",
                  project: "abc456",
                  revisions: {
                    "revision1": "2016-04-01"
                  }
                }));
            expect(controllerAreas[1])
              .toEqual(jasmine.objectContaining(
                {
                  $id: "area2",
                  phase: "1",
                  floor: "2",
                  type: "type",
                  description: "Another area",
                  supplier: "Innovaré",
                  project: "abc456",
                  revisions: {
                    "revision1": "2016-04-10"
                  }
                }));
          });

        $rootScope.$apply();
      }));

      it("adds specified area to view model when selectArea invoked", inject(function ($controller, $q, $rootScope) {
        var controller = $controller("ProjectsController", {
          $routeParams: {
            projectId: "123"
          }
        });

        // simulate loading of areas
        areas.area1.$id = "area1";
        areas.area2.$id = "area2";
        controller.areas = areas;

        $rootScope.$apply();

        controller.selectArea("area2");

        $rootScope.$apply();
        expect(controller.area).toEqual(jasmine.objectContaining({
          $id: "area2",
          phase: "1",
          floor: "2",
          type: "type",
          description: "Another area",
          supplier: "Innovaré",
          project: "abc456",
          revisions: {
            "revision1": "2016-04-10"
          }
        }));

        //$rootScope.$apply();
      }));

      it("has empty area when newArea invoked", inject(function ($controller, $rootScope) {
        var controller = $controller("ProjectsController", {
          $routeParams: {
            projectId: "123"
          }
        });

        $rootScope.$apply();

        controller.newArea();

        expect(controller.area).toEqual({});
      }));
    });
  });
});
