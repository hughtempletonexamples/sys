"use strict";

var utils = require("../testUtils");

var USER_ID = "{user-guid}";
var USER_NAME = "First Last";

describe("revisions service", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var utils = require("../testUtils");
  var firebaseRoot;
  var scope;
  var revisionsRef;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.projects", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    firebaseRoot = new MockFirebase("Mock://");

    revisionsRef = firebaseRoot.child("revisions");
    revisionsRef.autoFlush(true);
    
    utils.addQuerySupport(revisionsRef);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
    });

    utils.setupMockSchema(mockSchema, firebaseRoot, USER_ID);
    mockSchema.status.userName = USER_NAME;
  });

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope;
  }));

  describe("createRevision", function () {
    
    beforeEach(function () {
      jasmine.clock().install();
    });

    afterEach(function () {
      jasmine.clock().uninstall();
    });

    it("creates a new revision with current date and specified projectKey", inject(function (revisionsService) {
      var date = new Date("2016-03-01");
      jasmine.clock().mockDate(date);

      revisionsService.createRevision("projectKey");

      scope.$apply();
      utils.expectRefChildToEqual(revisionsRef, 0, {
        createdDate: "2016-03-01",
        project: "projectKey",
        deliveryManager: USER_NAME
      });
    }));
  });
  
  describe("getRevisionsByProjectId", function () {

    beforeEach(function () {
      revisionsRef.set({
        "revision1": {
          createdDate: "2016-03-01",
          project: "projectA"
        },
        "revision2": {
          createdDate: "2016-03-03",
          project: "projectA"
        },
        "revision3": {
          createdDate: "2016-03-05",
          project: "projectA"
        },
        "revision4": {
          createdDate: "2016-03-10",
          project: "projectB"
        }
      });
    });

    it("returns all revisions for given project", inject(function (revisionsService) {
      revisionsService.getRevisionsByProjectId("projectA")
        .$loaded()
        .then(function (revisions) {
          expect(revisions.length).toBe(3);

          expect(revisions[0]).toEqual(jasmine.objectContaining({
            createdDate: "2016-03-01",
            project: "projectA"
          }));

          expect(revisions[1]).toEqual(jasmine.objectContaining({
            createdDate: "2016-03-03",
            project: "projectA"
          }));

          expect(revisions[2]).toEqual(jasmine.objectContaining({
            createdDate: "2016-03-05",
            project: "projectA"
          }));

        });

      scope.$apply();
    }));
  });
  
  describe("removeRevisions", function () {

    it("removes projects revisions", inject(function (revisionsService) {
 
      revisionsRef.set({
        "revision1": {
          createdDate: "2016-03-01",
          project: "projectA"
        },
        "revision2": {
          createdDate: "2016-03-03",
          project: "projectA"
        },
        "revision3": {
          createdDate: "2016-03-05",
          project: "projectA"
        },
        "revision4": {
          createdDate: "2016-03-10",
          project: "projectB"
        }
      });
      
      revisionsService.removeRevisions("projectA")
        .then(function (revisions) {
          expect(revisions.length).toBe(3);
        
          utils.expectRefChildToContain(revisionsRef, 0, {
            createdDate: "2016-03-10",
            project: "projectB"
          });
          
        });

      scope.$apply();
    }));
  });
  
  describe("deleteRevision", function () {

    it("removes revision", inject(function (revisionsService) {
 
      revisionsRef.set({
        "revision1": {
          createdDate: "2016-03-01",
          project: "projectA"
        },
        "revision2": {
          createdDate: "2016-03-03",
          project: "projectB"
        }
      });
      
      revisionsService.deleteRevision("revision1")
        .then(function () {      
          utils.expectRefChildToContain(revisionsRef, 0, {
            createdDate: "2016-03-03",
            project: "projectB"
          });
        });

      scope.$apply();
    }));
  });
  
});
