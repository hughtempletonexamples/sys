"use strict";

var utils = require("../testUtils");

var USER_ID = "{user-guid}";

describe("projects service", function () {
  var MockFirebase = require("firebase-mock").MockFirebase;
  var systemTimestamp;
  var mockRevisionService;
  var mockAreaService;
  var mockAuditService;
  var mockPlannerService;
  var mockReferenceDataService;
  
  var referenceDataRef;
  var firebaseRoot;
  var revisionsRef;
  var projectsRef;
  var areasRef;
  var $q;
  var scope;
  var log;

  var projectsService;

  // Angular mock our projects module
  beforeEach(angular.mock.module("innView.projects", function ($provide) {
    // Output messages
    $provide.value("$log", console);
  }));

  beforeEach(function () {
    var now = new Date();
    jasmine.clock().install();
    jasmine.clock().mockDate(now);

    systemTimestamp = now.getTime();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  // Create mock schema object that will be injected into our service under test
  beforeEach(function () {
    mockRevisionService = jasmine.createSpyObj("revisionsService", ["createRevision", "deleteRevision"]);
    mockAreaService = jasmine.createSpyObj("areasService", ["addRevisionToProjectAreas", "getEarliestDeliveryDate", "getEarliestDeliveryDateAfterToday", "getProjectAreas", "updateProjectArea"]);
    mockAuditService = jasmine.createSpyObj("auditsService", ["project"]);
    mockPlannerService = jasmine.createSpyObj("plannerService", ["refreshProductionPlan"]);
    mockReferenceDataService = jasmine.createSpyObj("referenceDataService", ["getComponents"]);
    var mockSchema = jasmine.createSpyObj("schema", ["getRoot", "getObject", "getArray"]);

    angular.mock.module(function ($provide) {
      $provide.factory("schema", function () {
        return mockSchema;
      });
      $provide.factory("revisionsService", function () {
        return mockRevisionService;
      });
      $provide.factory("areasService", function () {
        return mockAreaService;
      });
      $provide.factory("plannerService", function () {
        return mockPlannerService;
      });
      $provide.factory("audit", function () {
        return mockAuditService;
      });
      $provide.factory("referenceDataService", function () {
        return mockReferenceDataService;
      });
    });

    firebaseRoot = new MockFirebase("Mock://");
    firebaseRoot.autoFlush(true);
    
    revisionsRef = firebaseRoot.child("revisions");
    revisionsRef.autoFlush(true);
    
    utils.addQuerySupport(revisionsRef);

    projectsRef = firebaseRoot.child("projects");
    utils.addQuerySupport(projectsRef);

    areasRef = firebaseRoot.child("areas");
    utils.addQuerySupport(areasRef);
    
    referenceDataRef = firebaseRoot.child("srd");
    utils.addQuerySupport(referenceDataRef);

    mockRevisionService.createRevision.and.callFake(function () {
      return $q.when();
    });
    
    mockRevisionService.deleteRevision.and.callFake(function () {
      return $q.when();
    });
    
    mockAuditService.project.and.callFake(function () {
      return $q.when();
    });

    utils.setupMockSchema(mockSchema, firebaseRoot, USER_ID);
  });

  beforeEach(inject(function ($rootScope, _$q_, _projectsService_) {
    scope = $rootScope;
    $q = _$q_;

    projectsService = _projectsService_;
    
    referenceDataRef.child("components").set({
      "Ancilliaries": {
        "$id": "Ancilliaries",
        "name": "Ancilliaries",
        "productTypes": {
          "Anc": true
        },
        "seq": 0
      },
      "Steels": {
        "$id": "Steels",
        "name": "Steels",
        "productTypes": {
          "Steel": true
        },
        "seq": 7
      },
      "Glulams": {
        "$id": "Glulams",
        "name": "Glulams",
        "productTypes": {
          "P&B": true
        },
        "seq": 6
      },
      "Walls": {
        "$id": "Walls",
        "name": "Walls",
        "productTypes": {
          "Ext": true,
          "Int": true,
          "ExtH": true
        },
        "seq": 1
      },
      "Floor": {
        "$id": "Floor",
        "name": "Floor",
        "productTypes": {
          "Floor": true,
          "Ext": true,
          "Int": true,
          "CrD": true
        },
        "seq": 2
      },
      "Spandrels": {
        "$id": "Spandrels",
        "name": "Spandrels",
        "productTypes": {
          "Ext": true,
          "Int": true
        }
        ,
        "seq": 5
      },
      "Roof": {
        "$id": "Roof",
        "name": "Roof",
        "productTypes": {
          "Roof": true,
          "Ext": true,
          "Int": true
        },
        "seq": 3
      },
      "Trusses": {
        "$id": "Trusses",
        "name": "Trusses",
        "productTypes": {
          "Truss": true
        },
        "seq": 8
      },
      "Parapets": {
        "$id": "Parapets",
        "name": "Parapets",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 4
      },
      "TF Walls": {
        "$id": "TF Walls",
        "name": "TF Walls",
        "productTypes": {
          "Int": true
        },
        "seq": 9
      },
      "SIP Walls": {
        "$id": "SIP Walls",
        "name": "SIP Walls",
        "productTypes": {
          "Ext": true
        },
        "seq": 10
      },
      "uCASS": {
        "$id": "uCASS",
        "name": "uCASS",
        "productTypes": {
          "Roof": true,
          "CrD": true
        },
        "seq": 11
      },
      "iCASS": {
        "$id": "iCASS",
        "name": "iCASS",
        "productTypes": {
          "Roof": true,
          "CrD": true
        },
        "seq": 12
      },
      "ClerestoryWallsNorthLight": {
        "$id": "Clerestory Walls/North Light",
        "name": "Clerestory Walls/North Light",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 13
      },
      "Upstands": {
        "$id": "Upstands",
        "name": "Upstands",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 14
      },
      "Bay Window": {
        "$id": "Bay Window",
        "name": "Bay Window",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 15
      },
      "Verge Panels": {
        "$id": "Verge Panels",
        "name": "Verge Panels",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 16
      },
      "Balcony Panels": {
        "$id": "Balcony Panels",
        "name": "Balcony Panels",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 17
      },
      "HSIPs": {
        "$id": "HSIPs",
        "name": "HSIPs",
        "productTypes": {
          "ExtH": true
        },
        "seq": 18
      },
      "Gable Ladder": {
        "$id": "Gable Ladder",
        "name": "Gable Ladder",
        "productTypes": {
          "Ext": true,
          "Int": true
        },
        "seq": 19
      }
    });
  }));

  describe("createProject", function () {

    it("adds new project", function (done) {
      projectsService.createProject({id: "123", name: "School"})
        .then(function () {
          utils.expectRefChildToContain(projectsRef, 0, {id: "123", name: "School"});

          done();
        });

      scope.$apply();
    });

    it("sets created timestamp", function (done) {
      projectsService.createProject({id: "123", name: "School"})
        .then(function () {
          utils.expectRefChildToContain(projectsRef, 0, {
            timestamps: {
              created: systemTimestamp,
              createdBy: USER_ID,
              modified: systemTimestamp,
              modifiedBy: USER_ID
            }
          });

          done();
        });

      scope.$apply();

    });

    it("creates new (empty) unpublished revision", function (done) {
      mockRevisionService.createRevision.and.callFake(function () {
        return $q.when("revisionReference");
      });

      projectsService.createProject({id: "321", name: "School"})
        .then(function () {
          expect(mockRevisionService.createRevision.calls.count()).toEqual(1);

          utils.expectRefChildToContain(projectsRef, 0, {
            deliverySchedule: {
              unpublished: "revisionReference"
            }
          });

          done();
        });

      scope.$apply();
    });

    it("does not add project with duplicate id", function (done) {
      //create preexisting project in projectsRef
      projectsRef.set({
        "a": {
          "id": "234"
        }
      });

      projectsService.createProject({id: "234", name: "Hospital"})
        .catch(function () {
          expect(projectsRef.getKeys().length).toEqual(1);

          done();
        });

      scope.$apply();
    });

    it("returns a promise on success", function (done) {
      projectsService.createProject({id: "345", name: "House"})
        .then(function (projectId) {
          expect(projectId).toBe("345");

          done();
        });

      scope.$apply();
    });

    it("breaks a promise for duplicate project id", function (done) {
      projectsRef.set({
        "a": {
          "id": "345"
        }
      });

      projectsService.createProject({id: "345", name: "House"})
        .catch(function (error) {
          expect(error).toBe("Duplicate id");

          done();
        });

      scope.$apply();
    });

    it("breaks a promise for failure to save", function (done) {
      var projectQueryRef = projectsRef.orderByChild("id");
      projectQueryRef.failNext("push", new Error("help"));

      projectsService.createProject({id: "345", name: "House"})
        .catch(function (error) {
          expect(error).toBe("Save failed");

          done();
        });

      scope.$apply();
    });

    it("audits creation", function (done) {
      projectsService.createProject({id: "345", name: "House"})
        .then(function () {
          var projectKey = projectsRef.getKeys()[0];

          expect(mockAuditService.project.calls.count()).toBe(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Created project", projectKey, "345"]);

          done();
        });

      scope.$apply();
    });

  });

  describe("removeNextDeliveryDateProject", function () {

    var projectA;

    beforeEach(inject(function ($q, $firebaseArray) {
      projectA = {
        "id": "123",
        "name": "project a",
        "client": "Client A",
        "siteAccess": "Site access A",
        "datumType": "Datum A",
        "chainOfCustody": "CoC A",
        "specialRequirements": "No Dairy",
        "siteAddress": "Address A",
        "postCode": "AB1 23C",
        "siteStart": "2017-02-02",
        "nextDeliveryDate": "2017-03-02",
        "timestamps": {
          "created": systemTimestamp - 2000,
          "modified": systemTimestamp - 1000
        }
      };

      projectsRef.set({
        "a": projectA
      });

    }));

    it("removes revisions", function (done) {
      var projectRef = "a";

      projectsService.removeNextDeliveryDateProject(projectRef)
        .then(function () {
          var modifiedProject = utils.getRefChild(projectsRef, 0);
          expect(modifiedProject).toEqual({
            "id": "123",
            "name": "project a",
            "client": "Client A",
            "siteAccess": "Site access A",
            "datumType": "Datum A",
            "chainOfCustody": "CoC A",
            "specialRequirements": "No Dairy",
            "siteAddress": "Address A",
            "postCode": "AB1 23C",
            "siteStart": "2017-02-02",
            "timestamps": {
              "created": systemTimestamp - 2000,
              "modified": systemTimestamp - 1000
            }
          });

          done();
        });

      scope.$apply();
    });

  });
  
  describe("resetRevisionsProject", function () {

    var projectA;

    beforeEach(inject(function ($q, $firebaseArray) {
      
      projectA = {
        "id": "123",
        "name": "project a",
        "client": "Client A",
        "siteAccess": "Site access A",
        "datumType": "Datum A",
        "chainOfCustody": "CoC A",
        "specialRequirements": "No Dairy",
        "siteAddress": "Address A",
        "postCode": "AB1 23C",
        "siteStart": "2017-02-02",
        "deliverySchedule": {
          "published": "revision1",
          "revisions": {
            "revision1": true
          },
          "unpublished": "revision1"
        },
        "timestamps": {
          "created": systemTimestamp - 2000,
          "modified": systemTimestamp - 1000
        }
      };

      projectsRef.set({
        "a": projectA
      });
      
      revisionsRef.set({
        "revision1": {
          createdDate: "2016-03-01",
          project: "projectA"
        },
        "revision2": {
          createdDate: "2016-03-03",
          project: "projectA"
        },
        "revision3": {
          createdDate: "2016-03-05",
          project: "projectA"
        },
        "revision4": {
          createdDate: "2016-03-10",
          project: "projectB"
        }
      });
      
    }));

    it("removes revisions", function (done) {
      
      mockRevisionService.createRevision.and.callFake(function () {
        return $q.when("revision2");
      });
      
      var projectRef = "a";

      projectsService.resetRevisionsProject(projectRef)
        .then(function () {
          var modifiedProject = utils.getRefChild(projectsRef, 0);
          expect(modifiedProject).toEqual({
            "id": "123",
            "name": "project a",
            "client": "Client A",
            "siteAccess": "Site access A",
            "datumType": "Datum A",
            "chainOfCustody": "CoC A",
            "specialRequirements": "No Dairy",
            "siteAddress": "Address A",
            "postCode": "AB1 23C",
            "siteStart": "2017-02-02",
            "deliverySchedule": {
              "unpublished": "revision2"
            },
            "timestamps": {
              "created": systemTimestamp - 2000,
              "modified": systemTimestamp - 1000
            }
          });

          done();
        });

      scope.$apply();
    });

  });

  describe("updateProject", function () {

    var projectA;

    beforeEach(inject(function ($q, $firebaseArray) {
      projectA = {
        "id": "123",
        "name": "project a",
        "client": "Client A",
        "siteAccess": "Site access A",
        "datumType": "Datum A",
        "chainOfCustody": "CoC A",
        "specialRequirements": "No Dairy",
        "siteAddress": "Address A",
        "postCode": "AB1 23C",
        "siteStart": "2017-02-02",
        "deliverySchedule": {
          "unpublished": "unpublishedRevisionRef"
        },
        "timestamps": {
          "created": systemTimestamp - 2000,
          "modified": systemTimestamp - 1000
        }
      };

      projectsRef.set({
        "a": projectA
      });

    }));

    it("updates all detail fields except id", function (done) {
      projectA["$id"] = "a";
      projectA["id"] = "234";
      projectA["name"] = "Project B";
      projectA["client"] = "Client B";
      projectA["siteAccess"] = "Site Access B";
      projectA["datumType"] = "Datum B";
      projectA["chainOfCustody"] = "CoC B";
      projectA["specialRequirements"] = "Gluten Free";
      projectA["siteAddress"] = "Address B";
      projectA["postCode"] = "CV32 4PG";
      projectA["siteStart"] = "2017-02-12";

      projectsService.updateProject(projectA)
        .then(function () {
          utils.expectRefChildToContain(projectsRef, 0, {
            "id": "123",
            "name": "Project B",
            "client": "Client B",
            "siteAccess": "Site Access B",
            "datumType": "Datum B",
            "chainOfCustody": "CoC B",
            "specialRequirements": "Gluten Free",
            "siteAddress": "Address B",
            "postCode": "CV32 4PG",
            "siteStart": "2017-02-12"
          });

          done();
        });

      scope.$apply();
    });

    it("updates the project's modified timestamp", function (done) {
      projectA.$id = "a";
      projectA.id = "123";

      projectsService.updateProject(projectA)
        .then(function () {
          var modifiedProject = utils.getRefChild(projectsRef, 0);
          expect(modifiedProject.timestamps.modified).toEqual(systemTimestamp);
          expect(modifiedProject.timestamps.modifiedBy).toEqual(USER_ID);

          done();
        });

      scope.$apply();
    });

    it("audits update", function (done) {
      projectA.$id = "a";
      projectA.id = "123";

      projectsService.updateProject(projectA)
        .then(function () {
          expect(mockAuditService.project.calls.count()).toBe(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Updated project", "a", "123"]);

          done();
        });

      scope.$apply();
    });
  });
  
  describe("checkDateDiffWithDeliveryDateUpdate", function () {
    
    var projectA;
    var area1;
    var area2;
    var area3;

    beforeEach(inject(function ($q, $firebaseArray) {
      projectA = {
        "id": "123",
        "name": "project a",
        "client": "Client A",
        "siteAccess": "Site access A",
        "datumType": "Datum A",
        "chainOfCustody": "CoC A",
        "specialRequirements": "No Dairy",
        "siteAddress": "Address A",
        "postCode": "AB1 23C",
        "siteStart": "2017-02-02",
        "deliverySchedule": {
          "unpublished": "unpublishedRevisionRef"
        },
        "timestamps": {
          "created": systemTimestamp - 2000,
          "modified": systemTimestamp - 1000
        }
      };

      projectsRef.set({
        "a": projectA
      });
      
      area1 = {
        $id: "area1",
        phase: "1",
        floor: "1",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "unpublishedRevisionRef": "2016-09-05"
        }
      };

      area2 = {
        $id: "area2",
        phase: "2",
        floor: "1",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "unpublishedRevisionRef": "2016-10-03"
        }
      };

      area3 = {
        $id: "area3",
        phase: "1",
        floor: "2",
        type: "type",
        description: "An area",
        supplier: "Innovaré",
        project: "a",
        revisions: {
          "unpublishedRevisionRef": "2016-09-06"
        }
      };

      areasRef.set({
        "area1": area1,
        "area2": area2,
        "area3": area3
      });
      
      mockReferenceDataService.getComponents.and.callFake(function () {
        return $firebaseArray(referenceDataRef.child("components"));
      });

      mockAreaService.getProjectAreas.and.callFake(function (y) {
        return $firebaseArray(areasRef);
      });

      mockAreaService.updateProjectArea.and.returnValue(
        $q.when("area1")
      );
    

    }));
    
    
    it("Project started with next delivery date", function (done) {
      projectA["$id"] = "a";
      projectA["id"] = "123";
      projectA["siteStart"] = "2017-02-12";
      projectA["nextDeliveryDate"] = "2017-08-12";
      
      var originalSiteStart = "2017-02-12";
      var nextDeliveryDate = "2017-08-12";
      
      projectsService.checkDateDiffWithDeliveryDateUpdate(projectA, originalSiteStart, nextDeliveryDate, true, false)
        .then(function (dates) {
          expect(dates.currentDate).toEqual("2017-08-12");
          expect(dates.changedDate).toEqual("2017-08-12");
          expect(dates.diffDays).toEqual(0);

          done();
        });

      scope.$apply();
    });

    it("checks existing site start date with newly change site start date, giving a return object of the two dates and difference, option to update all areas in the projects delivery date (option set to false)", function (done) {
      projectA["$id"] = "a";
      projectA["id"] = "123";
      projectA["siteStart"] = "2017-02-13";
      
      var originalSiteStart = "2017-02-02";
      var nextDeliveryDate = "2017-02-13";
      
      projectsService.checkDateDiffWithDeliveryDateUpdate(projectA, originalSiteStart, nextDeliveryDate, false, false)
        .then(function (dates) {
          expect(dates.currentDate).toEqual("2017-02-02");
          expect(dates.changedDate).toEqual("2017-02-13");
          expect(dates.diffDays).toEqual(7);

          done();
        });

      scope.$apply();
    });
    
    it("checks existing site start date with newly change site start date, giving a return object of the two dates and difference, option to update all areas in the projects delivery date (option set to false)", function (done) {
      projectA["$id"] = "a";
      projectA["id"] = "123";
      projectA["siteStart"] = "2017-02-13";
      
      var originalSiteStart = "2017-02-02";
      var nextDeliveryDate = "2017-02-13";
      
      var calledData = [
        [{$id: "area1", phase: "1", floor: "1", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-09-14"}, $priority: null}],
        [{$id: "area2", phase: "2", floor: "1", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-10-12"}, $priority: null}],
        [{$id: "area3", phase: "1", floor: "2", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-09-15"}, $priority: null}]
      ];
      
      var components = [
        {
          "project": "projectA",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "projectA",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];
      
      projectsService.checkDateDiffWithDeliveryDateUpdate(projectA, originalSiteStart, nextDeliveryDate, false, true, components)
        .then(function (dates) {
          expect(dates.currentDate).toEqual("2017-02-02");
          expect(dates.changedDate).toEqual("2017-02-13");
          expect(dates.diffDays).toEqual(7);
          
          expect(mockAreaService.updateProjectArea.calls.allArgs()).toEqual(calledData);
          
          done();
        });

      scope.$apply();
    });
    
    it("checks existing site start date with newly change site start date, giving a return object of the two dates and difference, option to update all areas in the projects delivery date (option set to true) including Saturdays", function (done) {
      projectA["$id"] = "a";
      projectA["id"] = "123";
      projectA["siteStart"] = "2017-02-13";
      projectA["includeSaturday"] = true;
      
      var originalSiteStart = "2017-02-02";
      var nextDeliveryDate = "2017-02-13";
      
      var calledData = [
        [{$id: "area1", phase: "1", floor: "1", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-09-15"}, $priority: null}],
        [{$id: "area2", phase: "2", floor: "1", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-10-13"}, $priority: null}],
        [{$id: "area3", phase: "1", floor: "2", type: "type", description: "An area", supplier: "Innovaré", project: "a", revisions: {"unpublishedRevisionRef": "2016-09-16"}, $priority: null}]
      ];
      
      var components = [
        {
          "project": "projectA",
          "areas": {
            "area1": true,
            "area3": true
          },
          "type": "Walls"
        },
        {
          "project": "projectA",
          "areas": {
            "area2": true
          },
          "type": "Floor"
        }
      ];
      
      projectsService.checkDateDiffWithDeliveryDateUpdate(projectA, originalSiteStart, nextDeliveryDate, false, true, components)
        .then(function (dates) {
          expect(dates.currentDate).toEqual("2017-02-02");
          expect(dates.changedDate).toEqual("2017-02-13");
          expect(dates.diffDays).toEqual(9);
          
          expect(mockAreaService.updateProjectArea.calls.allArgs()).toEqual(calledData);
          
          done();
        });

      scope.$apply();
    });
    
  });
  

  describe("getProjects", function () {
    beforeEach(function () {
      projectsRef.set({
        "a": {
          "id": "123",
          "name": "Project 1"
        },
        "b": {
          "id": "345",
          "name": "Project 3"
        },
        "c": {
          "id": "234",
          "name": "Project 2"
        }
      });
    });

    it("returns all known projects in numeric order of project id", function () {
      var projects = projectsService.getProjects();
      scope.$apply();

      expect(projects.length).toEqual(3);
      expect(projects[0]).toEqual(jasmine.objectContaining({
        "id": "123",
        "name": "Project 1"
      }));
      expect(projects[1]).toEqual(jasmine.objectContaining({
        "id": "234",
        "name": "Project 2"
      }));
      expect(projects[2]).toEqual(jasmine.objectContaining({
        "id": "345",
        "name": "Project 3"
      }));
    });
  });

  describe("getProject", function () {

    it("promises to return known project", function (done) {
      projectsRef.set({
        "a": {
          "id": "123",
          "name": "Some Project"
        }
      });

      projectsService.getProject("123")
        .then(function (result) {
          expect(result)
            .toEqual(jasmine.objectContaining({
              "$id": "a",
              "id": "123",
              "name": "Some Project"
            }));

          done();
        });

      scope.$apply();
    });

    it("breaks a promise for unknown project", function (done) {
      projectsService.getProject("123")
        .catch(function (error) {
          expect(error).toBe("Unknown project id");

          done();
        });

      scope.$apply();
    });
  });
  
  describe("getProjectArray", function () {

    it("promises to return known project", function (done) {
      projectsRef.set({
        "a": {
          "id": "123",
          "name": "Some Project"
        }
      });

      projectsService.getProjectArray("123")
        .then(function (result) {
          expect(result[0])
            .toEqual(jasmine.objectContaining({
              "$id": "a",
              "id": "123",
              "name": "Some Project"
            }));

          done();
        });

      scope.$apply();
    });

    it("breaks a promise for unknown project", function (done) {
      projectsService.getProject("123")
        .catch(function (error) {
          expect(error).toBe("Unknown project id");

          done();
        });

      scope.$apply();
    });
  });

  describe("promoteUnpublishedRevision", function () {
    var projectA;

    beforeEach(function () {
      projectA = {
        $id: "projectA",
        id: "123",
        name: "Project A",
        deliverySchedule: {
          unpublished: "revision1"
        }
      };

      projectsRef.set({
        "projectA": projectA
      });

      var projectARef = firebaseRoot.child("projects").child(projectA.$id);

      mockRevisionService.createRevision.and.callFake(function () {
        return $q.when("revision2");
      });

      mockAreaService.getEarliestDeliveryDate.and.callFake(function () {
        return $q.when("");
      });
      
      mockAreaService.getEarliestDeliveryDateAfterToday.and.callFake(function () {
        return $q.when("");
      });
    });

    it("sets published revision", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          var updatedProject = utils.getRefChild(projectsRef, 0);
          expect(updatedProject.deliverySchedule)
            .toEqual(jasmine.objectContaining({
              published: "revision1"
            }));

          done();
        });

      scope.$apply();
    });

    it("adds published revision to revisions list", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {

          var updatedProject = utils.getRefChild(projectsRef, 0);
          expect(updatedProject.deliverySchedule)
            .toEqual(jasmine.objectContaining({
              revisions: {
                revision1: true
              }
            }));

          done();
        });

      scope.$apply();

    });

    it("creates new unpublished revision", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          var updatedProject = utils.getRefChild(projectsRef, 0);
          expect(updatedProject.deliverySchedule)
            .toEqual(jasmine.objectContaining({
              unpublished: "revision2"
            }));

          done();
        });

      scope.$apply();
    });

    it("invokes area service to update revisions on linked area", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          expect(mockAreaService.addRevisionToProjectAreas.calls.count()).toEqual(1);

          done();
        });

      scope.$apply();
    });

    it("audits publishing revision", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          expect(mockAuditService.project.calls.count()).toBe(1);
          expect(mockAuditService.project.calls.mostRecent().args).toEqual(["Published delivery schedule", "projectA", "123"]);

          done();
        });

      scope.$apply();
    });

    it("triggers production plan refresh", function (done) {
      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          expect(mockPlannerService.refreshProductionPlan.calls.count()).toBe(1);

          done();
        });

      scope.$apply();
    });

    it("sets project next delivery date", function (done) {
      mockAreaService.getEarliestDeliveryDate.and.callFake(function () {
        return $q.when("2016-06-12");
      });
      
      mockAreaService.getEarliestDeliveryDateAfterToday.and.callFake(function () {
        return $q.when("2016-06-12");
      });

      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          var updatedProject = utils.getRefChild(projectsRef, 0);
          expect(updatedProject.nextDeliveryDate)
            .toEqual("2016-06-12");

          done();
        });

      scope.$apply();
    });

    it("updates project next delivery date", function (done) {
      projectA.nextDeliveryDate = "2016-06-12";

      mockAreaService.getEarliestDeliveryDate.and.callFake(function (revisionRef) {
        return $q.when("2016-06-15");
      });
      
      mockAreaService.getEarliestDeliveryDateAfterToday.and.callFake(function () {
        return $q.when("2016-06-15");
      });

      projectsService.promoteUnpublishedRevision("projectA")
        .then(function () {
          var updatedProject = utils.getRefChild(projectsRef, 0);
          expect(updatedProject.nextDeliveryDate)
            .toEqual("2016-06-15");

          done();
        });

      scope.$apply();
    });
  });

  describe("touch", function () {

    it("updates the project's modified timestamp", function (done) {
      projectsRef.set({
        "a": {
          "id": "123",
          "name": "Some Project",
          "timestamps": {
            "created": systemTimestamp - 10000,
            "createdBy": "{another-user-guid}",
            "modified": systemTimestamp - 1000,
            "modifiedBy": "{another-user-guid}"
          }
        }
      });

      projectsService.touch("a")
        .then(function () {
          var modifiedProject = utils.getRefChild(projectsRef, 0);
          expect(modifiedProject.timestamps.modified).toEqual(systemTimestamp);
          expect(modifiedProject.timestamps.modifiedBy).toEqual(USER_ID);

          done();
        });

      scope.$apply();
    });
  });
});
