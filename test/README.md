Testing
-------

There are two sets of tests:

* Unit Tests
* End to End / Acceptance Tests

Unit Tests
----------

These are run using karma. The karma configuration is in test/karma.conf.js

To run the unit tests, execute the following command in the root directory:
```
npm test
```

Junit compatible output will be placed in test/unit/output

Acceptance Tests
----------------

These are run using protractor and selenium.

Currently, you need to manually start the application server by running the following command in the root directory:
```
npm start
```

Then to run the acceptance tests, in another terminal run this command:
```
npm run acceptance
```
This will check that you have selenium installed, and downlaod it if required.

The tests will run and the results displayed in your terminal.