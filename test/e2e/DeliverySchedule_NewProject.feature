Feature:

  @ignore
  Scenario:  A new project using all fields can be added without areas
    Given the new project page
    When I choose to add 1 project with values in all fields
    Then the delivery schedule shows the project details
    And the project fields are read-only

  @ignore
  Scenario:  A new project using only the mandatory fields can be added without areas
    Given the new project page
    When I choose to add 1 project with (P.Name, P.Number, C.Name)
    Then the delivery schedule shows the project details
    And the project fields are read-only

  @ignore
  Scenario: An new project can not be added without mandatory field Project Name
    Given the new project page
    When I choose to add 1 project with no value in the field "Project Name"
    Then the I cannot store the new project
    And the field "Project Name" is shown as invalid

  @ignore
  Scenario: An new project can not be added without mandatory field Project Number
    Given the new project page
    When I choose to add 1 project with no value in the field "Project No"
    Then the I cannot store the new project
    And the field "Project No" is shown as invalid

  @ignore
  Scenario: An new project can not be added without mandatory field Client
    Given the new project page
    When I choose to add 1 project with no value in the field "Client"
    Then the I cannot store the new project
    And the field "Client" is shown as invalid

  @ignore
  Scenario: Empty new project mandatory fields are shown as invalid after they lose focus
    Given the add area page
    When I move the cursor of the empty "Name" field
    And I move the cursor of the empty "Project No" field
    And I move the cursor of the empty "Client" field
    Then the "Name" field is shown as invalid
    And the "Project No" field is shown as invalid
    And the "Client" field is shown as invalid