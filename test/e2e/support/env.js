//
// Setup / override Cucumber config
//

var configure = function () {
  this.setDefaultTimeout(10 * 1000);
};

module.exports = configure;
