"use strict";

var World = function World() {

  this.createdProjectsList = [];

  this.recordProject = function (project) {
    console.log("Record project " + project);
    return this.createdProjectsList.push(project);
  };

  this.getCreatedProjects = function () {
    return this.createdProjectsList;
  };

  return;
};

module.exports = function () {
  this.World = World;
};
