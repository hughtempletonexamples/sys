"use strict";

var myHooks = function () {

  this.After(function (scenario) {
    // Again, "this" is set to the World instance the scenario just finished
    // playing with.
    // Intention is to tear down stuff (projects) created by the scenario.
    console.log("myHooks: After scenario " + this.getCreatedProjects());
  });

  this.Before({tags: ["@foo"]}, function (scenario) {
    console.log("myHooks: Before " + JSON.stringify(scenario));
  });

};

module.exports = myHooks;
