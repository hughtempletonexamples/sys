
Feature: User Authentication

  Authentication allows valid users to access the system
  while preventing access for unauthorised users.

  Scenario: Login page is displayed initially
    When I first view the application
    Then I am shown the login page

  Scenario: A User with valid credentials can login
    Given I am on the login page
    When I login with "operations@example.com" and "password"
    Then I can see the current projects

  Scenario: A User with invalid credentials cannot login
    Given I am on the login page
    When I login with "joe.bloggs@example.com" and "password"
    Then my login has failed
    And I am shown the login page
