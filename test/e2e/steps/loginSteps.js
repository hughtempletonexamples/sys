"use strict";

var LoginPageDriver = require("../pages/loginPage.js");
var HomePageDriver = require("../pages/homePage.js");

module.exports = function () {

  var loginPage = new LoginPageDriver();
  var homePage = new HomePageDriver();

  this.When("I first view the application", function () {
    return browser.get("/");
  });

  this.Then("I am shown the login page", function () {
    return loginPage.isShowing();
  });

  this.Given("I am on the login page", function () {
    return loginPage.get();
  });

  this.When("I login with \"$email\" and \"$password\"", function (email, password) {
    return loginPage.login(email, password);
  });

  this.Then("I can see the current projects", function () {
    return homePage.showsCurrentProjects();
  });

  this.Then("my login has failed", function () {
    return loginPage.isShowingLoginError();
  });
};
