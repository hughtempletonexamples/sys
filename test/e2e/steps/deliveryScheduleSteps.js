"use strict";

var HomePageDriver = require("../pages/homePage.js");
var DeliverySchedulePageDriver = require("../pages/deliverySchedulePage.js");

module.exports = function () {

  var deliverySchedulePage = new DeliverySchedulePageDriver();
  var homePage = new HomePageDriver();

  this.Given("I am on the home page", function () {
    return homePage.loginToHomePage();
  });

  this.When("I create a new project \"$id\" called \"$name\" for \"$client\"", function (id, name, client) {
    homePage.clickNewProjectButton();
    deliverySchedulePage.showsNewProjectForm();
    this.recordProject(id);
    return deliverySchedulePage.createProject(name, id, client);
  });

  this.Then("a delivery schedule with zero Areas is available", function () {
    // TODO: Assert Zero Areas somehow?
    return deliverySchedulePage.showsProjectAreasList();
  });

  this.Then("it is possible to add a new area", function () {
  });

};
