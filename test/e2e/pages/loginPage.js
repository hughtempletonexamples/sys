"use strict";

// LoginPage

module.exports = function () {
  var url = "index.html#/login";
  var absUrl = "/login";
  var emailElem = element(by.model("vm.email"));
  var passwordElem = element(by.model("vm.password"));
  var submitElem = element(by.name("submit"));
  var loginErrorElem = element(by.binding("vm.errorMessage"));
  var loginErrorXPath = by.xpath("//span[@class='ng-binding']");
  var loginErrorText = "Unknown email or incorrect password";
  var validEmail = "operations@example.com";
  var validPassword = "password";

  this.get = function () {
    return browser.get(url);
  };

  this.isShowing = function () {
    return expect(browser.getLocationAbsUrl()).to.eventually.equal(absUrl);
  };

  /*
   * See https://github.com/angular/protractor/issues/2358
   * After a failed login, we need to wait for the root to update.
   */
  this.loginFailureDone = function () {
    return browser.wait(function () {
      return browser.driver.isElementPresent(loginErrorXPath);
    });
  };

  this.isShowingLoginError = function () {
    this.loginFailureDone();
    return expect(loginErrorElem.getText()).to.eventually.equal(loginErrorText);
  };

  this.loginWithValidCredentials = function () {
    this.get();
    this.isShowing();
    return this.login(validEmail, validPassword);
  };

  this.login = function (email, password) {
    emailElem.sendKeys(email);
    passwordElem.sendKeys(password);
    return submitElem.click();
  };

};
