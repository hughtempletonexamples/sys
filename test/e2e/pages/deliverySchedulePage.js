"use strict";

// DeliverySchedule

module.exports = function () {

  var createUrl = "index.html#/projects?create";
  var headingByXpath = by.xpath("//span[@class='ng-scope']");
  var headingElem = element(headingByXpath);
  var headingText = "Project detail"

  var nameElem = element(by.model("vm.project.name"));
  var idElem = element(by.model("vm.project.id"));
  var clientElem = element(by.model("vm.project.client"));
  var saveButtonElem = element(by.css("button[type=submit]"));

  var areasListElem = element(headingByXpath);
  var areasListText = "Project areas list";


  this.get = function () {
    return browser.get(url);
  };

  /*
   * See https://github.com/angular/protractor/issues/2358
   * After login, we need to wait for the home page to load.
   */
  this.isLoaded = function () {
    return browser.wait(function () {
      return browser.driver.isElementPresent(headingByXpath);
    });
  };

  this.showsNewProjectForm = function () {
    this.isLoaded();
    browser.ignoreSynchronization = true; // Because of the $timeout for logout
    return expect(headingElem.getText()).to.eventually.equal(headingText);
  };

  this.showsProjectAreasList = function () {
    return expect(areasListElem.getText()).to.eventually.equal(areasListText);
  };

  this.createProject = function (name, id, client) {
    nameElem.sendKeys(name);
    idElem.sendKeys(id);
    clientElem.sendKeys(client);
    saveButtonElem.click();
    return this.showsProjectAreasList();
  };


};
