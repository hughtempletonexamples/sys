"use strict";

var LoginPageDriver = require("./loginPage.js");

// HomePage

module.exports = function () {

  var loginPage = new LoginPageDriver();

  var url = "index.html#/home";
  var headingText = "Current projects";
  var headingElem = element(by.tagName("h1"));
  var headingByXpath = by.xpath("//h1");
   /* <a href="#/projects?create">Create a new project</a> */
  var newProjectButtonElem = element(by.css("a[href='#/projects?create']"));

  this.get = function () {
    return browser.get(url);
  };

  /*
   * See https://github.com/angular/protractor/issues/2358
   * After login, we need to wait for the home page to load.
   */
  this.isLoaded = function () {
    return browser.wait(function () {
      return browser.driver.isElementPresent(headingByXpath);
    });
  };

  this.showsCurrentProjects = function () {
    this.isLoaded();
    browser.ignoreSynchronization = true; // Because of the $timeout for logout
    return expect(headingElem.getText()).to.eventually.equal(headingText);
  };

  this.loginToHomePage = function () {
    this.get();
    loginPage.loginWithValidCredentials();
    return this.showsCurrentProjects();
  };

  this.clickNewProjectButton = function () {
    return newProjectButtonElem.click();
  };
};
