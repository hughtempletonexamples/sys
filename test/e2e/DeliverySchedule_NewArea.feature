Feature:

  @ignore
  Scenario: An new area can be added to a project using all fields with no existing areas
    Given a project with 0 existing areas
    And I choose to add 1 area
    And the area details are entered (Phase, Floor, Type, Desc, Est.m2/Est.panels, Supplier, Additional.Comments, Del.Date)
    When I store the area details
    Then the delivery schedule shows the project details
    And the delivery schedule shows the area details (except the Supplier field)
      | Area ID | Phase | Floor | Type | Description | Delivery Date |
      | Row1    |       |       |      | New Area    |               |

  @ignore
  Scenario: An new area can be added to a project using only the mandatory fields with an existing area
    Given a project with 1 existing areas
    And I choose to add 1 area
    And the area details are entered (Phase, Floor, Type, Desc, Supplier, Del.Date)
    When I store the area details
    Then the delivery schedule shows the project details
    And the delivery schedule shows the area details (except the Supplier field)
      | Area ID | Phase | Floor | Type | Description   | Delivery Date |
      | Row1    |       |       |      | Existing Area |               |
      | Row2    |       |       |      | New Area      |               |

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Phase
    Given an existing project
    When I choose to add 1 area with no value in the "Phase" field
    Then I cannot store the new area
    And the field "Phase" is shown as invalid

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Floor
    Given an existing project
    When I choose to add 1 area with no value in the "Floor" field
    Then I cannot store the new area
    And the field "Floor" is shown as invalid

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Type
    Given an existing project
    When I choose to add 1 area with no value in the "Type" field
    Then I cannot store the new area
    And the field "Type" is shown as invalid

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Description
    Given an existing project
    When I choose to add 1 area with no value in the "Description" field
    Then I cannot store the new area
    And the field "Description" is shown as invalid

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Supplier
    Given an existing project
    When I choose to add 1 area with no value in the "Supplier" field
    Then I cannot store the new area
    And the field "Supplier" is shown as invalid

  @ignore
  Scenario: An new area can not be added to a project without mandatory field Delivery Date
    Given an existing project
    When I choose to add 1 area with no value in the "Delivery Date" field
    Then I cannot store the new area
    And the field "Delivery Date" is shown as invalid

  @ignore
  Scenario: Empty new area mandatory fields are shown as invalid after they lose focus
    Given the add area page
    When I move the cursor of the empty "Focus" field
    And I move the cursor of the empty "Floor" field
    And I move the cursor of the empty "Phase" field
    And I move the cursor of the empty "Description" field
    And I move the cursor of the empty "Supplier" field
    And I move the cursor of the empty "Delivery Date" field
    Then the "Focus" field is shown as invalid
    And the "Floor" field is shown as invalid
    And the "Phase" field is shown as invalid
    And the "Description" field is shown as invalid
    And the "Supplier" field is shown as invalid
    And the "Delivery Date" field is shown as invalid
