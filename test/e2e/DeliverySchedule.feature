
Feature: Create New Project with Areas

  @ignore
  # TODO Project tear down not yet immplemented.
  # waiting for a Delete button in the UI
  Scenario: I can create a new project
    Given I am on the home page
    When I create a new project "200" called "test project name" for "test client"
    Then a delivery schedule with zero Areas is available

  @ignore
  # TODO Test steps not yet immplemented.
  Scenario: I can add an Area to a project
    Given I am viewing project "200"
    When I add an area "P1-GF-Ext" with supplier "Innovaré" for the "20th" of next month
    Then the delivery schedule contains:
    | name   | email              | twitter         |
    | Aslak  | aslak@cucumber.io  | @aslak_hellesoy |
