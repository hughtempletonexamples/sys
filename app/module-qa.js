(function () {
  "use strict";

  angular.module("innView", [
    "ngRoute",
    "ngAnimate",
    "ngTouch",
    "ngMessages",
    "ui.bootstrap",
    "angular-confirm",
    "innView.core",
    "innView.login",
    "innView.projects",
    "innView.manage",
    "innView.qa"
  ]);
})();
