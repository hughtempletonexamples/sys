(function () {
  "use strict";

  angular
    .module("innView.projects")
    .controller("ProjectRevisionsController", ProjectRevisionsController);

  /**
   * @ngInject
   */
  function ProjectRevisionsController($routeParams, $mdDialog, $location, $window, projectsService, areasService, revisionsService, componentsService) {
    var vm = this;

    vm.project = null;
    vm.selectedTab = "revisions";
    vm.printModel = {};
    vm.hidden = {};
    vm.goto = goto;
    vm.toggleVisibility = toggleVisibility;
    vm.toggleDelivered = toggleDelivered;
    vm.isToggleDelivered = false;
    vm.groupedBy = "phase";
    vm.groupBy = groupBy;
    vm.publishRevision = publishRevision;
    vm.togglePublishButton = false;
    vm.revMore = revMore;
    vm.revLess = revLess;
    vm.revisionShow = [4,3,2,1,0];
    

    init();

    ////////////

    function init() {
      if ($routeParams.projectId) {
        loadProject($routeParams.projectId)
          .then(loadRevisions)
          .then(loadAreas)
          .then(loadAreasForPrinting)
          .catch(showError);
      }
    }
    
    function revMore(){
      if(vm.totalRevisionsMarker < vm.totalRevisions){
        vm.totalRevisionsMarker++;
      }
    }
    
    function revLess(){
      if (vm.totalRevisionsMarker > 4){
        vm.totalRevisionsMarker--;
      }
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page
      }else{
        $location.url("/projects/" + vm.project.id + "/" + page);
      }
    }

    function loadProject(id) {
      return projectsService
        .getProject(id)
        .then(function (project) {
          vm.project = project;
          vm.hidden = {};
          return project.$id;
        });
    }

    function loadRevisions() {
      if (angular.isDefined(vm.project.deliverySchedule.revisions)) {
        revisionsService.getRevisionsByProjectId(vm.project.$id)
          .$loaded()
          .then(function (revisions) {
            var revisionsOb = [];
            var projRevisions = [];

            angular.forEach(revisions, function (revision, key) {

              if (key > 0) {
                revisionsOb.push({
                  revisionRef: revision.$id,
                  createdDate: revision.createdDate,
                  deliveryManager: revision.deliveryManager
                });
              }

              if (revision.$id !== vm.project.deliverySchedule.unpublished) {
                projRevisions.push(revision.$id);
              }

            });
            vm.projRevisions = projRevisions;
            vm.revisions = revisionsOb;
            vm.totalRevisions = revisions.length;
            vm.totalRevisionsMarker = revisions.length;
          });
      }else{
        vm.totalRevisions = 0;
        vm.totalRevisionsMarker = 0;
      }
    }

    function loadProjectAreas(getAreasForProject) {
      vm.components = componentsService.getComponentsForProject(vm.project.$id);
      vm.components
        .$loaded(function () {
          processAreas();
          vm.components.$watch(processAreas);
        });

      function processAreas() {
        getAreasForProject(vm.project, vm.components)
          .then(function (groupedAreas) {
            vm.groupedAreas = groupedAreas;
            angular.forEach(groupedAreas.groups, function (phase, phaseName) {
              if (angular.isUndefined(vm.hidden[phaseName])) {
                vm.hidden[phaseName] = false;
              }
              angular.forEach(phase.components, function (component, componentName) {
                if (angular.isUndefined(vm.hidden[phaseName + "-" + componentName])) {
                  vm.hidden[phaseName + "-" + componentName] = true;
                }
              });
            });
          });
      }
    }

    function loadAreasForPrinting() {
      areasService.getProjectAreas(vm.project.$id)
        .$loaded()
        .then(function (areas) {
          
          vm.togglePublishButton = false;
        
          if (angular.isDefined(vm.project.deliverySchedule.revisions)) {
            angular.forEach(areas, function (area) {
              if (area.revisions[vm.project.deliverySchedule.published] !== area.revisions[vm.project.deliverySchedule.unpublished]){
                vm.togglePublishButton = true;
              }
            });
              
            var revisions = Object.keys(vm.project.deliverySchedule.revisions);
            vm.printModel.latestFourRevisions = revisions.slice(revisions.length - 4);

            vm.printModel.areas = areas.sort(function (a, b) {
              if (a.revisions[vm.project.deliverySchedule.published] < b.revisions[vm.project.deliverySchedule.published]) {
                return -1;
              } else if (a.revisions[vm.project.deliverySchedule.published] > b.revisions[vm.project.deliverySchedule.published]) {
                return 1;
              } else {
                return 0;
              }
            });
          }else{
            if (areas.length > 0){
              vm.togglePublishButton = true;
            }
          }
        });
    }
    
    function toggleDelivered(){
      vm.isToggleDelivered = !vm.isToggleDelivered;
    }

    function toggleVisibility(key) {
      vm.hidden[key] = !vm.hidden[key];
    }

    function groupBy(grouping) {
      vm.groupedBy = grouping;

      loadAreas();
    }

    function loadAreas() {
      switch(vm.groupedBy) {
      case "week":
        loadProjectAreas(areasService.getProjectAreasByWeek);
        break;
      default:
      case "phase":
        loadProjectAreas(areasService.getProjectAreasByPhase);
        break;
      }
    }

    function publishRevision() {
      vm.togglePublishButton = false;
      projectsService
        .promoteUnpublishedRevision(vm.project.$id)
        .then(loadRevisions)
        .then(loadAreas)
        .then(loadAreasForPrinting)
        .catch(function (error) {
          showError(error);
          return;
        });
    }

    function showError(error) {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .title("There was a problem")
          .textContent(error)
          .ok("Ok")
      );
    }
  }
})();
