/* global PRODUCTION:false */
(function () {
  "use strict";

  angular
    .module("innView.projects")
    .controller("ProjectsController", ProjectsController);

  /**
   * @ngInject
   */
  function ProjectsController($scope, $log, $filter, $confirm, projectsService, usersService, areasService, componentsService, $routeParams, $location, $route, $window) {
    var vm = this;

    vm.project = null;

    vm.printModel = {};

    vm.newProject = newProject;
    vm.save = save;
    vm.newArea = newArea;
    vm.saveArea = saveArea;
    vm.resetProjectDetailsForm = resetProjectDetailsForm;
    vm.resetAreaForm = resetAreaForm;
    vm.selectArea = selectArea;
    vm.publishRevision = publishRevision;
    vm.deleteSelectedArea = deleteSelectedArea;
    vm.updateFilter = updateFilter;
    vm.selectSuppliersByDefault = selectSuppliersByDefault;
    vm.updateDeliveryManager = updateDeliveryManager;

    vm.expand = {
      project: true,
      area: true,
      schedule: true
    };

    vm.original = {};

    vm.areasGridOptions = {
      rowHeight: 61,
      enableSorting: false,
      enableColumnMenus: false,
      enableRowHeaderSelection: false,
      multiSelect: false,
      enableSelectAll: false,
      enableSelectionBatchEvent: false,
      rowTemplate:
      "<div " +
        "ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.uid\" " +
        "ui-grid-one-bind-id-grid=\"rowRenderIndex + '-' + col.uid + '-cell'\" " +
        "class=\"ui-grid-cell\" " +
        "ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader, 'thirdparty': row.entity.supplier != \'Innovaré\' }\" " +
        "role=\"{{col.isRowHeader ? 'rowheader' : 'gridcell'}}\" " +
        "ui-grid-cell> "+
      "</div>"
    };

    //
    //
    vm.uniqueSuppliers = {};
    vm.selectedSuppliers = {};

    init();

    ////////////

    function init() {
      
      vm.areasGridOptions.onRegisterApi = function (gridApi){
        //set gridApi on scope
        vm.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row){
          if (row.isSelected) {
            vm.selectArea(row.entity["ref"]);
          }
        });
      };

      if ($routeParams.projectId) {
        vm.selectedTab = "detail";
        loadProject($routeParams.projectId)
          .then(loadProjectComponents)
          .then(loadProjectAreas)
          .then(loadProjectAreasByPhase)
          .then(loadProjectAreasByWeek)
          .then(populateAreasGrid)
          .catch(showError);

      }
      else if ($routeParams.create) {
        vm.project = {};
        vm.printModel = {};
      }
    }

    function initGridColumns() {
      vm.areasGridOptions.columnDefs = [
        { displayName: "Phase - Floor - Type", field: "pft", width: 180, pinnedLeft: true},
        { displayName: "Description", field: "description", width: 251, pinnedLeft: true},
        { displayName: "Supplier", field: "supplier", width: 120, pinnedLeft: true}
      ];
    }

    function selectSuppliersByDefault(areas) {
      areas.forEach(function (area) {
        if (angular.isUndefined(vm.selectedSuppliers[area.supplier])) {
          vm.selectedSuppliers[area.supplier] = {"selected" : true};
        }
      });
      return areas;
    }

    function updateFilter() {
      populateAreasGrid(vm.areas);
    }

    function newProject() {
      $location.url("/projects?create");
    }

    function newArea() {
      if (vm.project !== null && vm.project.id !== null) {
        vm.area = {};
        vm.original["area"] = {};
        vm.original["deliveryDate"] = ""; // needs to be an invalid
                                          // value rather than
                                          // undefined, to allow the
                                          // date input component to
                                          // handle discard changes
                                          // properly
        if (vm.areaForm) {
          resetAreaForm();
        }
      }
    }

    function save(isValid) {
      if (isValid) {

        if (angular.isDefined(vm.project.$id)) {
          projectsService
            .updateProject(vm.project)
            .then(function (projectId) {
              $route.reload();
            })
            .catch(showError);
        } else {
          projectsService
            .createProject(vm.project)
            .then(function (projectId) {
              $window.location.href = "/newux.html#/projects/" + vm.project.id + "/details";
              //$location.url("newux.html#/projects/" + vm.project.id + "/details");
            })
            .catch(showError);
        }
      }
    }

    function saveArea(isValid) {
      if (isValid) {
        var projectArea = vm.area;

        if (angular.isDefined(projectArea.$id)) {
          projectArea.revisions[vm.project.deliverySchedule.unpublished] = vm.deliveryDate;
          areasService
            .updateProjectArea(projectArea)
            .then(stopEditingArea)
            .catch(showError);
        } else {
          projectArea.project = vm.project.$id;

          areasService
            .createProjectArea(vm.project, projectArea, vm.deliveryDate)
            .then(function (areaRef) {
              vm.area.$id = areaRef;
            })
            .then(stopEditingArea)
            .catch(showError);
        }
      }
    }

    function publishRevision() {
      projectsService
        .promoteUnpublishedRevision(vm.project.$id)
        .catch(function (error) {
          showError(error);
          return;
        });
    }

    function stopEditingArea() {
      if (vm.areaForm) {
        vm.areaForm.$setPristine();
      }
    }

    function showError(error) {
      alert("Error: " + error);
    }

    function updateDeliveryManager(selectedUserId) {
      vm.userLoading = true;
      vm.userLoaded = false;
      //retrieve full user details from user service
      usersService.getUserById(selectedUserId)
        .$loaded()
        .then(function (user) {
          if(angular.isUndefined(vm.project.deliveryManager)) {
            vm.project.deliveryManager = {};
          }
          vm.project.deliveryManager.name = user.name;
          vm.project.deliveryManager.email = user.email;
          vm.project.deliveryManager.mobile = user.phone;
          vm.userLoaded = true;
          vm.userLoading = false;
        });
    }

    function loadProject(id) {
      return projectsService
        .getProject(id)
        .then(function (project) {
          vm.project = project;
          vm.printModel.project = project;
          vm.original["project"] = angular.copy(project);
          return project.$id;
        });
    }

    function loadProjectComponents(key) {
      vm.components = componentsService.getComponentsForProject(key);
      return key;
    }

    function loadProjectAreas(key) {
      vm.areas = areasService.getProjectAreas(key);
      $scope.$watch("vm.areas", populateAreasGrid, true);
      return vm.areas;
    }

    function loadProjectAreasByPhase(areas) {

      if (enableDev()) {
        areasService.getProjectAreasByPhase(vm.project)
          .then(function (areasByPhase) {
            vm.areasByPhase = areasByPhase;
          });
      }
      return areas;
    }

    function loadProjectAreasByWeek(areas) {

      if (enableDev()) {
        areasService.getProjectAreasByWeek(vm.project)
          .then(function (areasByWeek) {
            vm.areasByWeek = areasByWeek;
          });
      }
      return areas;
    }

    function populateAreasGrid(areas) {
      if (angular.isDefined(areas)) {
        initGridColumns();

        areas
          .$loaded()
          .then(selectSuppliersByDefault)
          .then(function (areas) {
            var revisionCount = 1;
            angular.forEach(vm.project.deliverySchedule.revisions, function (value, revision) {
              // $log.log("Revision: " + revision);
              vm.areasGridOptions.columnDefs.push(
                { displayName: "Revision " + $filter("revisionCount")(revisionCount++), field: revision, name: revision, width: 105}
              );
            });

            vm.areasGridOptions.columnDefs.push(
              { displayName: "Unpublished", field: "unpublished", name: "unpublished", width: 105}
            );

            vm.areasGridOptions.data = [];
            vm.areasGridOptions.primaryKey = "ref";

            angular.forEach(areas, function (area) {
              var areaData = {};
              areaData["pft"] = area.phase + "-" + area.floor + "-" + area.type;
              areaData["description"] = area.description;
              areaData["supplier"] = area.supplier;

              angular.forEach(vm.project.deliverySchedule.revisions, function (value, revision) {
                var revisionValue = area.revisions[revision];
                areaData[revision] = angular.isUndefined(revisionValue) ? "-" : $filter("date")(revisionValue, "EEE dd-MMM");
              });

              areaData["unpublished"] = $filter("date")(area.revisions[vm.project.deliverySchedule.unpublished], "EEE dd-MMM");
              areaData["unpublishedDate"] = area.revisions[vm.project.deliverySchedule.unpublished];
              areaData["ref"] = area.$id;
              if (vm.selectedSuppliers[area.supplier].selected == true) {
                vm.areasGridOptions.data.push(areaData);
              }
            });

            // Sort Areas by delivery date
            vm.areasGridOptions.data = sortByDeliveryDate(vm.areasGridOptions.data);
            findUniqueSuppliers();

            extractPrintData(revisionCount);

            return vm.areasGridOptions;
          });
      }
    }

    function findUniqueSuppliers() {
      vm.uniqueSuppliers = {};
      angular.forEach(vm.areas, function (area) {
        if (angular.isUndefined(vm.uniqueSuppliers[area.supplier])) {
          vm.uniqueSuppliers[area.supplier] = {"name" : area.supplier, "count" : 1};
        } else {
          vm.uniqueSuppliers[area.supplier].count++;
        }
      });
    }

    function extractPrintData(revisionCount) {
      vm.printModel.columnDefs = vm.areasGridOptions.columnDefs.slice(0, 3);
      vm.printModel.data = vm.areasGridOptions.data;

      var startIndex = 3;
      var endIndex = startIndex + revisionCount;
      if (revisionCount >4) {
        //get last 4 published revisions
        startIndex = -5;
        endIndex = -1;
      }

      vm.printModel.columnDefs = vm.printModel.columnDefs.concat(vm.areasGridOptions.columnDefs.slice(startIndex, endIndex));
    }

    function selectArea(areaId) {
      stopEditingArea();
      angular.forEach(vm.areas, function (area) {
        if (areaId === area.$id) {
          vm.area = angular.copy(area);
          vm.original["area"] = angular.copy(area);
          vm.deliveryDate = area.revisions[vm.project.deliverySchedule.unpublished];
          vm.original["deliveryDate"] = angular.copy(vm.deliveryDate);
          return;
        }
      });
    }

    function resetProjectDetailsForm(projectDetailsForm) {
      vm.project = angular.copy(vm.original["project"]);
      projectDetailsForm.$setPristine();
      return;
    }

    function sortByDeliveryDate(items) {
      return items.sort(function (a, b) {
        if (a.unpublishedDate < b.unpublishedDate) {
          return -1;
        } else if (a.unpublishedDate > b.unpublishedDate) {
          return 1;
        } else {
          return 0;
        }
      });
    }

    function resetAreaForm() {
      vm.area = angular.copy(vm.original["area"]);
      vm.deliveryDate = angular.copy(vm.original["deliveryDate"]);
      vm.areaForm.$setPristine();
      return;
    }

    function deleteSelectedArea(token) {

      areasService.delete(vm.area.$id, token)
        .then(function (response) {
          switch(response.status) {
          case "PANELS_CONFIRM":
            $confirm({
              text: "This area has panels associated with it. They will be deleted along with the area."
            },{
              templateUrl: require("../core/confirm-dialog.html")
            }).then(function () {
              deleteSelectedArea(response.token);
            });

            break;

          case "PANELS_STARTED":
            $confirm({
              text: "This area cannot be deleted because work has been recorded against its panels.",
              cancel: "Ok"
            },{
              templateUrl: require("../core/info-modal.html")
            });

            break;

          case "OK":
          default:
            delete vm.area;
            delete vm.deliveryDate;
            break;
          }
        });

    }

    function enableDev() {
      function includes(string, search) {
        return string.indexOf(search, 0) !== -1;
      }
      return !includes($location.host(), "innview.firebaseapp.com");
    }
  }
})();
