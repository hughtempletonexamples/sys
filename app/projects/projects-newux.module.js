(function () {
  "use strict";

  angular
    .module("innView.projects", [
      "ngRoute",
      "ngMaterial",
      "firebase",
      "innView.core"
    ]);
})();
