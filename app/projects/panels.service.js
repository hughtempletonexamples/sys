(function () {
  "use strict";

  var FIREBASE_ROOT = "panels";

  angular
    .module("innView.projects")
    .factory("panelsService", panelsService);

  /**
   * @ngInject
   */
  function panelsService($log, $q, schema, areasService, projectsService, audit,$filter, firebaseUtil) {

    var service = {
      panelsByCompleted: panelsByCompleted,
      panelsForArea: panelsForArea,
      panelsForProject: panelsForProject,
      markPanelsComplete: markPanelsComplete,
      deletePanels: deletePanels,
      markPanelIncomplete: markPanelIncomplete,
      updatePanels: updatePanels,
      getPanelById: getPanelById,
      comparator: compareById,
      deletePanelsForArea: deletePanelsForArea,
      uploadToDb:saveError
    };

    return service;

    //////////////
    
    
    function saveError(panelId,error,imageId){
      var panelsErrors = schema.getObject(schema.getRoot().child("errors"));

      return panelsErrors.$loaded()
          .then(function (errorsList){
            var data = {};
            var date = $filter("date")(new Date(), "yyyy-MM-dd");
            
            if (angular.isUndefined(errorsList)) {
              errorsList = {};
            }
            var data = {
              "date": date,
              "panel": panelId,
              "error": error
            };
            panelsErrors[imageId] = data;
            return panelsErrors;
          }).then(function (panelsErrors){
            panelsErrors.$save();
            return panelsErrors;
          });
      
    }
    
    function panelsByCompleted(firstDay, lastDay){
      var panels = schema.getRoot().child("panels").orderByChild("qa/completed").startAt(firstDay).endAt(lastDay); 
      return schema.getArray(panels);
    }

    function panelsForArea(areaRef) {
      return schema.getArray(schema
                            .getRoot()
                            .child(FIREBASE_ROOT)
                            .orderByChild("area")
                            .equalTo(areaRef));
    }
    
    function panelsForProject(projectRef) {
      return schema.getArray(schema
                            .getRoot()
                            .child(FIREBASE_ROOT)
                            .orderByChild("project")
                            .equalTo(projectRef));
    }
    
    function deletePanelsForArea(areaRef) {
      var promises = [];

      return panelsForArea(areaRef)
        .$loaded()
        .then(function (panels) {
          angular.forEach(panels, function (panel) {
            promises.push(deletePanel(panel.$id));
          });

        })
        .then(function () {
          return $q.all(promises);
        });
    }

    function deletePanel(panelRef) {
      return schema.getObject(schema.getRoot().child(FIREBASE_ROOT).child(panelRef)).$remove();
    }
    
    function updatePanelCompletion(panels) {
      var completed = {};
      return calculatePanelCompletion(panels);

      var panelsSelection = {};
      angular.forEach(panels, function (panel) {
        panelsSelection[panel.id] = false;
      });

      function calculatePanelCompletion(panels) {
        var numberCompleted = countCompletedPanels(panels);
        var totalAreaComplete = countTotalAreaComplete(panels);
        var numberPanels = angular.isDefined(panels) ? panels.length : 0;
        completed = {
          number: numberCompleted,
          percentage: numberPanels > 0 ? Math.round((numberCompleted / numberPanels) * 100) : 0,
          total: numberPanels,
          area: totalAreaComplete
        };
        return completed;
      }
      
      function countTotalAreaComplete(panels) {
        var areaCompleted = 0;
        angular.forEach(panels, function (panel) {
          if (panelCompleted(panel)) {
            areaCompleted += panel.dimensions.area;
          }
        });
        return areaCompleted;
      }
      
      function countCompletedPanels(panels) {
        var numberCompleted = 0;
        angular.forEach(panels, function (panel) {
          if (panelCompleted(panel)) {
            numberCompleted++;
          }
        });
        return numberCompleted;
      }
    }
    
    function panelCompleted(panel) {
      return panelQAStatusDefined(panel, "completed");
    }
    
    function panelQAStatusDefined(panel, status) {
      return (angular.isDefined(panel.qa) && angular.isDefined(panel.qa[status]));
    }

    function deletePanels(panels, panelSelections) {

      var panelDeletes = [];
      var projectUpdates = [];
      var panelsDeletedCount = 0;
      var projectRef = panels[0].project;
      var areaRef = panels[0].area;

      angular.forEach(panels, function (panel) {
        if (panelSelections[panel.id]) {
          panelDeletes.push(deletePanel(panel.$id));
          panelsDeletedCount++;
        }
      });

      return $q.all(panelDeletes)
        .then(function () {
          projectUpdates.push(updateProjectPanelsForDelete(projectRef, areaRef));
          return $q.all(projectUpdates);
        })
        .then(function () {
          return audit.project("Deleted " + panelsDeletedCount + " panels", projectRef);
        })
        .catch(function (error) {
          $log.log("Error in deletePanels: " + angular.toJson(error));
        });

    }

    function markPanelsComplete(areaRef, panelRefs, completedDate) {
      var completedTimestamp = angular.isDefined(completedDate) ? Date.parse(completedDate) : firebaseUtil.serverTime();

      var panelSaves = [];
      var panelAreas = [];
      var panelProject;
      angular.forEach(panelRefs, function (panelRef) {
        var promise = completePanel(panelRef, panelAreas);
        if (promise !== null) {
          panelSaves.push(promise);
        }
      });

      return $q.all(panelSaves)
        .then(function () {
          return panelsForArea(areaRef).$loaded();
        })
        .then(function (panels) {
          var completed = updatePanelCompletion(panels);
          var totalPanelArea = panelAreas.reduce(function (pv, cv) { return pv + cv; }, 0);
          return areasService.modifyCompletedPanels(areaRef, completed.number, completed.area);
        })
        .then(function () {
          if (angular.isDefined(panelProject)) {
            return projectsService.touch(panelProject);
          } else {
            return $q.when();
          }
        })
        .then(function () {
          if (angular.isDefined(panelProject)) {
            return audit.project("Marked " + panelRefs.length + " panels complete", panelProject);
          } else {
            return $q.when();
          }
        })
        .catch(function (error) {
          $log.log("Error in markPanelsComplete: " + angular.toJson(error));
          $q.reject(error);
        });

      function completePanel(panelRef, panelAreas) {
        schema.getObject(schema.getRoot().child(FIREBASE_ROOT).child(panelRef))
          .$loaded()
          .then(function (panel) {
            if (angular.isUndefined(panel.qa)) {
              panel.qa = {};
            }
            if (angular.isUndefined(panel.qa["completed"])) {
              panel.qa["started"] = completedTimestamp;
              panel.qa["completed"] = completedTimestamp;
              setModifiedTimestamp(panel);
              panelAreas.push(panel.dimensions.area);
              panelProject = panel.project;
              return panel.$save();
            } else {
              return null;
            }
          })
          .catch(function (error) {
            $q.reject("Error in markPanelsComplete.completePanel(" + panelRef + "): " + error);
          });
      }
    }

    function markPanelIncomplete(panels, panelRef) {
      var panel;
      angular.forEach(panels, function (p) {
        if (panelRef === p.$id) {
          panel = p;
          return;
        }
      });

      if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa["completed"])) {
        delete panel.qa;
        var completed = updatePanelCompletion(panels);
        areasService
          .modifyCompletedPanels(panel.area, completed.number, completed.area)
          .then(function () {
            return projectsService.touch(panel.project);
          })
          .then(function () {
            return audit.project("Marked 1 panel incomplete", panel.project);
          });
      }

      panel.timestamps["modified"] = firebaseUtil.serverTime();

      return panels.$save(panel);
    }

    function updatePanels(panels) {
      var projectUpdates = [];

      angular.forEach(partitonPanelsByProject(panels), function (panelList, projectKey) {
        projectUpdates.push(updateProjectPanels(projectKey, panelList));
      });

      return $q.all(projectUpdates)
        .then(buildSummary);

      function buildSummary(updates) {
        var summary = {
          total: 0,
          details: []
        };
        angular.forEach(updates, function (value) {
          summary.details.push(value);
          summary.total += value.count;
        });
        summary.details.sort(function (a, b) {
          return a.projectId.localeCompare(b.projectId);
        });

        return $q.when(summary);
      }
    }

    function getPanelById(panelId) {
      return schema.getArray(schema
                            .getRoot()
                            .child(FIREBASE_ROOT)
                            .orderByChild("id")
                            .equalTo(panelId));
    }

    function updateProjectPanelsForDelete(projectKey, areaKey) {
      var updateDetail = {};

      var project = schema.getObject(schema.getRoot()
        .child("projects")
        .child(projectKey));
      var areas = areasService.getProjectAreas(projectKey);
      var existingPanels = schema.getArray(schema.getRoot()
        .child("panels")
        .orderByChild("project")
        .equalTo(projectKey));

      return existingPanels
        .$loaded()
        .then(function () {
          return doAreaUpdate(areaKey, areas, existingPanels);
        })
        .then(function () {
          return doProjectUpdate(project, updateDetail);
        });
    }

    function updateProjectPanels(projectKey, panels) {
      var updateDetail = {};
      var project = schema.getObject(schema.getRoot()
                                     .child("projects")
                                     .child(projectKey));
      var areas = areasService.getProjectAreas(projectKey);
      var existingPanels = schema.getArray(schema.getRoot()
        .child("panels")
        .orderByChild("project")
        .equalTo(projectKey));

      return existingPanels
        .$loaded()
        .then(doPanelUpdates)
        .then(recordUpdateCount)
        .then(function () {
          return doAreaUpdates(areas, existingPanels, panels);
        })
        .then(function () {
          return doProjectUpdate(project, updateDetail);
        })
        .then(recordAudit)
        .then(returnDetail);

      function doPanelUpdates(existingPanels) {
        var x = panels.map(upsertPanel);
        return $q.all(x);

        function upsertPanel(panel) {
          var existing;
          angular.forEach(existingPanels, function (p) {
            if (p.id === panel.id) {
              existing = p;
            }
          });
          
          if (existing) {
            angular.extend(existing, panel);
            setModifiedTimestamp(existing);
            return existingPanels.$save(existing);
          }
          else {
            var newPanel = angular.extend({}, panel);
            setModifiedTimestamp(newPanel);
            return existingPanels.$add(newPanel);
          }
        }
      }

      function recordUpdateCount(panelUpdates) {
        updateDetail.count = panelUpdates.length;

        return $q.when();
      }

      function recordAudit() {
        audit.project("Loaded " + updateDetail.count + " panels", projectKey, updateDetail.projectId);
        return $q.when();
      }

      function returnDetail() {
        return $q.when(updateDetail);
      }
    }

    function doAreaUpdates(areas, existingPanels) {
      return areas
        .$loaded()
        .then(function () {
          var areaUpdates = [];
          angular.forEach(areas, function (area) {
            angular.extend(area, calculateActuals(area.$id));
            setModifiedTimestamp(area);
            areaUpdates.push(areas.$save(area));
          });

          return $q.all(areaUpdates);
        });

      function calculateActuals(areaKey) {
        var actuals = {
          actualPanels: 0,
          actualArea: 0
        };

        angular.forEach(existingPanels, function (panel) {
          if (panel.area === areaKey) {
            actuals.actualPanels += 1;
            actuals.actualArea += panel.dimensions.area;
          }
        });

        return actuals;
      }
    }
    
    function doAreaUpdate(areaKey, areas, existingPanels) {

      var area = areas.$getRecord(areaKey);
      angular.extend(area, calculateActuals(areaKey));
      setModifiedTimestamp(area);
      return areas.$save(area);

      function calculateActuals(areaKey) {
        var actuals = {
          actualPanels: 0,
          actualArea: 0
        };

        angular.forEach(existingPanels, function (panel) {
          if (panel.area === areaKey) {
            actuals.actualPanels += 1;
            actuals.actualArea += panel.dimensions.area;
          }
        });

        return actuals;
      }
    }

    function doProjectUpdate(project, updateDetail) {
      var deferred = $q.defer();
      project
        .$loaded()
        .then(function () {
          updateDetail.projectId = project.id;
          updateDetail.projectName = project.name;

          setModifiedTimestamp(project);
          project.$save()
            .then(function () {
              deferred.resolve(updateDetail);
            });
        });
      return deferred.promise;
    }

    function partitonPanelsByProject(panels) {
      var panelsByProject = {};

      angular.forEach(panels, function (panel) {
        var project = panel.project;
        if (!panelsByProject[project]) {
          panelsByProject[project] = [];
        }
        panelsByProject[project].push(panel);
      });

      return panelsByProject;
    }

    function setModifiedTimestamp(item) {
      // Ensure existing data has a created timestamp
      if (!item.timestamps) {
        item.timestamps = {
          created: firebaseUtil.serverTime()
        };
      }

      item.timestamps.modified = firebaseUtil.serverTime();
    }

    function compareById(a, b) {
      if (a.id < b.id) {
        return -1;
      } else if (a.id > b.id) {
        return 1;
      } else {
        return 0;
      }
    }
  }
})();
