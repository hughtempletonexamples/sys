(function () {
  "use strict";

  angular
    .module("innView.projects")
    .filter("numbersFromString", filter);

  function filter() {

    return numbersFromStringFilter;

    function numbersFromStringFilter(string) {
      return string.replace(/[0-9]/g, "");
    }
  }

})();
