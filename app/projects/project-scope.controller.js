(function () {
  "use strict";

  angular
    .module("innView.projects")
    .controller("ProjectScopeController", ProjectScopeController);

  /**
   * @ngInject
   */
  function ProjectScopeController(schema, $log, $routeParams, $route, $window, $location, projectsService, dateUtil) {
    var vm = this;
    
    vm.save = save;
    vm.goto = goto;
    vm.searchTermSIP = "";
    vm.searchTermHSIP = "";
    vm.searchTermIFAST = "";
    vm.searchTermTF = "";
    vm.searchTermCASS = "";
    vm.clearSearchTerm = clearSearchTerm;
    
    loadProject();

    function loadProject() {
      
      if ($routeParams.projectId) {

        return projectsService.getProject($routeParams.projectId)
                .then(function ( project ) {
                  vm.project = project;
                  return schema.getObject(schema.getRoot().child("additionalData/projects").child(project.$id)).$loaded();
                })
                .then(function ( additionalData ) {
                  additionalData.framingStyles = orderFramingStyle(additionalData.framingStyles, false);
                  vm.projectAdditionalData = additionalData;
                  return schema.getArray(schema.getRoot().child("config/framingStyles")).$loaded();
                })
                .then(function ( framingStyle ) {
                  vm.configFramingStyle = orderFramingStyle(framingStyle, true);
                });

      }

    }
    
    function orderFramingStyle(framingStyle, useKey){
      var configFramingStyle = {};

      angular.forEach(framingStyle, function ( value, key ) {
        angular.forEach(value, function ( value2, key2 ) {
          var keyOption = key;
          if(useKey){
            keyOption = value.$id;
          }
          if (angular.isUndefined(configFramingStyle[keyOption])) {
            configFramingStyle[keyOption] = [];
          }
          if (key2.indexOf("$") === -1) {
            configFramingStyle[keyOption].push(key2);
          }
        });
      });
      return configFramingStyle;
    }
    
    function save() {
      var newFramingStyle = {};
      if (angular.isDefined(vm.projectAdditionalData.$id)) {
        angular.forEach(vm.projectAdditionalData.framingStyles, function (framingStyles, productionLine) {
          angular.forEach(framingStyles, function (framingStyle) {
            if(angular.isUndefined(newFramingStyle[productionLine])){
              newFramingStyle[productionLine] = {};
            }
            if(angular.isUndefined(newFramingStyle[productionLine][framingStyle])){
              newFramingStyle[productionLine][framingStyle] = true;
            }
          });
        });
        vm.projectAdditionalData.framingStyles = newFramingStyle;
        $log.log(vm.projectAdditionalData);
        return vm.projectAdditionalData.$save();
      } 
      
    }
    
    function clearSearchTerm() {
      vm.searchTermSIP = "";
      vm.searchTermHSIP = "";
      vm.searchTermTF = "";
      vm.searchTermCASS = "";
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/projects/" + vm.project.id + "/" + page);
      }
    }

  }
})();