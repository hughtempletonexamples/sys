(function () {
  "use strict";

  require("angular-route");
  require("angular-file-saver");

  require("./projects.module");
  require("./projects.controller");
  require("./projects.service");
  require("./areas.service");
  require("./panels.service");
  require("./revisions.service");
  require("./config.route");
  require("./project.filter");
  require("./project-name.filter");
  require("./numbers-from-string.filter");
  require("./projects-print.component");
  require("./components.service");
  
})();
