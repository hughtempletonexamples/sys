(function () {
  "use strict";

  angular
    .module("innView.projects")
    .component("innPrintSchedule", {
      templateUrl: require("./project-print.html"),
      controller: printController,
      controllerAs: "vm",
      bindings: {
        printModel: "=ngModel"
      }
    });

  /**
   * @ngInject
   */
  function printController() {
    var vm = this;

    ////////////
  }

})();
