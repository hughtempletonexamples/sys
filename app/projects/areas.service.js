(function () {
  "use strict";

  angular
    .module("innView.projects")
    .factory("areasService", areasService);

  /**
   * @ngInject
   */
  function areasService($log, $filter, $injector, $q, dateUtil, schema, audit, firebaseUtil, referenceDataService) {
    var service = {
      getProjectAreas: getAreas,
      getProjectAreasByPhase: getProjectAreasByPhase,
      getProjectAreasByWeek: getProjectAreasByWeek,
      createProjectArea: createArea,
      getProjectArea: getArea,
      updateProjectArea: updateArea,
      addRevisionToProjectAreas: addRevisionToAreas,
      modifyCompletedPanels: modifyCompletedPanels,
      delete: deleteArea,
      getEarliestDeliveryDate: getEarliestDeliveryDate,
      getEarliestDeliveryDateAfterToday: getEarliestDeliveryDateAfterToday
    };

    return service;

    function createArea(project, area, deliveryDate) {
      //Add project's unpublished revision to area revisions
      var revisions = {};
      revisions[project.deliverySchedule.unpublished] = deliveryDate;

      var newArea = angular.extend(
        {},
        area,
        {
          active: true,
          delivered: false,
          revisions: revisions,
          timestamps: {
            created: firebaseUtil.serverTime(),
            modified: firebaseUtil.serverTime()
          }
        });

      var newAreaRef;

      return getAreas(area.project)
        .$add(newArea)
        .then(function (ref) {
          newAreaRef = ref.key;
        })
        .then(auditCreateArea)
        .then(function () {
          return newAreaRef;
        })
        .catch(function (error) {
          $log.log("Unable to save area: " + error);
        });

      function auditCreateArea(areaId) {
        var id = [newArea.phase, newArea.floor, newArea.type].join("-");
        return audit.project("Added area '" + id + "'", area.project);
      }
    }

    function getAreas(projectKey) {
      return schema.getArray(schema.getRoot()
                             .child("areas")
                             .orderByChild("project")
                             .equalTo(projectKey));
    }
    
    var datumsService;
    
    function calculateEstimatedAreasAndPanels(project) {
      
      if (!datumsService) {
        datumsService = $injector.get("datumsService");
      }
      
      var productTypes = referenceDataService.getProductTypes();
      var datumData = datumsService.damumByName(project.datumType);

      return datumData
        .$loaded()
        .then(function () {
          return getAreas(project.$id);
        })
        .then(function (areas) {
          
          var promises = [];
          var productTypeProductionLine = buildMap(productTypes, "productionLine");
          var estimatesPerGroup = {};
          var areaCount = {};

          angular.forEach(project.estimatedAreas, function (totalArea, group) {
            estimatesPerGroup[group] = totalArea;
          });

          angular.forEach(areas, function (area) {
            if (angular.isUndefined(areaCount[area.type])) {
              areaCount[area.type] = 0;
            }
            areaCount[area.type]++;
          });
          
          angular.forEach(areas, function (area) {
            var productionLineKey = productTypeProductionLine[area.type];
            if (angular.isDefined(estimatesPerGroup[area.type])) {
              area.estimatedArea = Math.round(estimatesPerGroup[area.type] / areaCount[area.type]);
              if (angular.isDefined(productionLineKey) && angular.isDefined(datumData[0]) && angular.isDefined(datumData[0].productionLines[productionLineKey])) {
                var panelAvgOverride = datumData[0].productionLines[productionLineKey].panelAvgOverride;
                var panelAvg = datumData[0].productionLines[productionLineKey].panelAvg;
                if (panelAvgOverride > 0) {
                  area.estimatedPanels = Math.round(area.estimatedArea / panelAvgOverride) | 0;
                } else {
                  area.estimatedPanels = Math.round(area.estimatedArea / panelAvg) | 0;
                }
              }
              promises.push(updateArea(area));
            }
          });
          
          return $q.all(promises);
          
        });

      
    } 

//    function calculateEstimatedAreasAndPanels(project, getAreasByGroup, components, areaSort) {
//      
//      if (!datumsService) {
//        datumsService = $injector.get("datumsService");
//      }
//      
//      var deferred = $q.defer();
//      
//      var productTypes = referenceDataService.getProductTypes();
//      var datumData = datumsService.damumByName(project.datumType);
//
//      datumData
//        .$loaded()
//        .then(function () {
//          return getAreasByGroup(project, components, areaSort);
//        })
//        .then(function (areasGrouped) {
//          
//          var productTypeProductionLine = buildMap(productTypes, "productionLine");
//        
//          var estimatesPerGroup = {};
//
//          angular.forEach(project.estimatedAreas, function (totalArea, group) {
//            estimatesPerGroup[group] = (totalArea / 100) * areasGrouped.percentage;
//          });
//
//          // TODO: Remove this once the getProjectAreasByWeek has been refactored to match ...ByPhase
//          var groups = angular.isDefined(areasGrouped.groups) ? areasGrouped.groups : areasGrouped.group;
//
//          angular.forEach(groups, function (group) {
//            var areaCount = {};
//            var components = angular.isDefined(group.components) ? group.components : group;
//            angular.forEach(components, function (component) {
//              var areas = angular.isDefined(component.areas) ? component.areas : component;
//              angular.forEach(areas, function (area) {
//                if (angular.isUndefined(areaCount[area.type])) {
//                  areaCount[area.type] = 0;
//                }
//
//                areaCount[area.type]++;
//              });
//            });
//
//            angular.forEach(components, function (component) {
//              var areas = angular.isDefined(component.areas) ? component.areas : component;
//              angular.forEach(areas, function (area) {
//                var productionLineKey = productTypeProductionLine[area.type];
//                if (angular.isDefined(estimatesPerGroup[area.type])) {
//                  area.estimatedArea = Math.round(estimatesPerGroup[area.type] / areaCount[area.type]);
//                  if (angular.isDefined(productionLineKey) && angular.isDefined(datumData[0]) && angular.isDefined(datumData[0].productionLines[productionLineKey])) {
//                    var panelAvgOverride = datumData[0].productionLines[productionLineKey].panelAvgOverride;
//                    var panelAvg = datumData[0].productionLines[productionLineKey].panelAvg;
//                    if (panelAvgOverride > 0) {
//                      area.estimatedPanels = Math.round(area.estimatedArea / panelAvgOverride) | 0;
//                    } else {
//                      area.estimatedPanels = Math.round(area.estimatedArea / panelAvg) | 0;
//                    }
//                  }
//                  updateArea(area);
//                }
//              });
//            });
//          });
//          deferred.resolve(areasGrouped);
//        });
//
//      return deferred.promise;
//    } 

    function createAreasMap( components, srdComponents ) {
      var areasMap = {};
      var areasMapCounter = {};

      angular.forEach(components, function ( component ) {
        if (angular.isUndefined(areasMapCounter[component.type])) {
          areasMapCounter[component.type] = 0;
        }
        areasMapCounter[component.type]++;
        angular.forEach(component.areas, function ( value, area ) {
          var componentName = component.type + areasMapCounter[component.type];
          areasMap[area] = {
            id: component.$id,
            name: componentName,
            comments: component.comments
          };
        });
      });

      return areasMap;
    }
   
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }

    function getAreasByPhase(project, components, areaSort) {

      var deferred = $q.defer();
      
      var areas = {};

      getAreas(project.$id)
        .$loaded()
        .then(function (areasData) {
          areas = areasData;
        })
        .then(function () {
          return referenceDataService.getComponents().$loaded();
        })
        .then(function (srdComponents) { 
          
          if(areaSort){
            sortByDeliveryDate(areas);
          }else{
            sortByFirstRevision(areas);
          }
          
          var areasGroupedByPhase = {
            percentage: 100,
            groups: {}
          };
          
          var componentsMap = createAreasMap(components, srdComponents);

          angular.forEach(areas, function (area) {
            var phase = areasGroupedByPhase.groups[area.phase];

            if(angular.isUndefined(phase)) {
              phase = areasGroupedByPhase.groups[area.phase] = {
                delivered: true,
                active: true,
                totalGroupSpan: 1,
                components: {}
              };
            }

            var componentKey = componentsMap[area.$id];
            if(angular.isUndefined(componentKey)) {
              var componentKey = {};
              componentKey.name = "NONE";
              componentKey.id = 0;
              componentKey.comments = 0;
            }

            var component = phase.components[componentKey.name];
            if(angular.isUndefined(component)) {
              component = phase.components[componentKey.name] = {
                componentId: componentKey.id,
                comments: componentKey.comments,
                areas: [],
                areasDeliveryDate: [],
                areasLatestRevision: [],
                areasDelivered: [],
                areasActive: [],
                delivered: true,
                active: true,
                same: false,
                isSame: [],
                deliveryDate: new Date("2100-12-30"),
                latestRevision: new Date("2100-12-30")
              };
            }
            
            var areaDate = new Date(area.revisions[project.deliverySchedule.unpublished]);
            var revision = new Date(area.revisions[project.deliverySchedule.unpublished]);
            
            if(angular.isDefined(project.deliverySchedule.published)){
              revision = new Date(area.revisions[project.deliverySchedule.published]);
            }
           
            var isSame = component.isSame[area.$id];
            var deliveryDate = component.areasDeliveryDate[area.$id];
            var latestRevision = component.areasLatestRevision[area.$id];
            var areaDelivered = component.areasDelivered[area.$id];
            var areaActive = component.areasActive[area.$id];
            
            if (angular.isUndefined(deliveryDate)) {
              deliveryDate = component.areasDeliveryDate[area.$id] = areaDate;
            }
            
            if (angular.isUndefined(latestRevision)) {
              latestRevision = component.areasLatestRevision[area.$id] = revision;
            }
            
            if (angular.isUndefined(isSame)) {
              if(dateUtil.toIsoDate(deliveryDate) === dateUtil.toIsoDate(latestRevision)){
                isSame = component.isSame[area.$id] = true;
              }else{
                isSame = component.isSame[area.$id] = false;
              }
            }

            if (angular.isUndefined(areaDelivered)) {
              if (angular.isDefined(area.delivered) && area.delivered) {
                areaDelivered = component.areasDelivered[area.$id] = true;
              } else {
                areaDelivered = component.areasDelivered[area.$id] = false;
              }
            }
            
            if (angular.isUndefined(areaActive)) {
              if (area.active) {
                areaActive = component.areasActive[area.$id] = true;
              } else {
                areaActive = component.areasActive[area.$id] = false;
              }
            }
            
            if (!areaDelivered){
              component.delivered = false;
              phase.delivered = false;
            }
            
            if (!areaActive){
              component.active = false;
              phase.active = false;
            }

            component.areas.push(area);
            if (areaDate < component.deliveryDate) {
              component.deliveryDate = areaDate;
            }
            if (latestRevision < component.latestRevision) {
              component.latestRevision = latestRevision;
            }
            if(dateUtil.toIsoDate(component.deliveryDate) === dateUtil.toIsoDate(component.latestRevision)){
              component.same = true;
            }
          });

          angular.forEach(areasGroupedByPhase.groups, function (phase) {
            var totalComponents = Object.keys(phase.components).length;
            phase.percentage = (1/totalComponents)*100;
          });

          var totalPhases = Object.keys(areasGroupedByPhase.groups).length;
          areasGroupedByPhase.percentage = (1/totalPhases)*100;

          return areasGroupedByPhase;
        })
        .then(function (groupedAreas) {
          deferred.resolve(groupedAreas);
        })
        .catch(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
      
      function sortByDeliveryDate(areas) {
        return areas.sort(function (a, b) {
          if (a.revisions[project.deliverySchedule.unpublished] < b.revisions[project.deliverySchedule.unpublished]) {
            return -1;
          } else if (a.revisions[project.deliverySchedule.unpublished] > b.revisions[project.deliverySchedule.unpublished]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
      
      function sortByFirstRevision(areas) {
        return areas.sort(function (a, b) {
          
          var revisionA = Object.keys(a.revisions);
          var revisionB = Object.keys(b.revisions);
          var firstRevisionA = revisionA[0];
          var firstRevisionB = revisionB[0];
          
          if (a.revisions[firstRevisionA] < b.revisions[firstRevisionB]) {
            return -1;
          } else if (a.revisions[firstRevisionA] > b.revisions[firstRevisionB]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
    }

    function getAreasByWeek(project, components, areaSort) {

      var deferred = $q.defer();
      
      var areas = {};
      
      getAreas(project.$id)
        .$loaded()
        .then(function (areasData) {
          areas = areasData;
        })
        .then(function () {
          return referenceDataService.getComponents().$loaded();
        })
        .then(function (srdComponents) {
          
          if(areaSort){
            sortByDeliveryDate(areas);
          }else{
            sortByFirstRevision(areas);
          }
          
          var areasGroupedByWeek = {
            percentage: 100,
            groups: {}
          };
          
          var componentsMap = createAreasMap(components, srdComponents);

          angular.forEach(areas, function (area) {

            var weekStart = dateUtil.weekStart(area.revisions[project.deliverySchedule.unpublished]);
            var week = areasGroupedByWeek.groups[weekStart];

            if(angular.isUndefined(week)) {
              week = areasGroupedByWeek.groups[weekStart] = {
                components: {},
                totalGroupSpan: 1,
                delivered: true,
                active: true
              };
            }

            var componentKey = componentsMap[area.$id];
            if(angular.isUndefined(componentKey)) {
              var componentKey = {};
              componentKey.name = "NONE";
              componentKey.id = 0;
              componentKey.comments = 0;
            }

            var component = week.components[componentKey.name];
            if(angular.isUndefined(component)) {
              component = week.components[componentKey.name] = {
                componentId: componentKey.id,
                comments: componentKey.comments,
                areas: [],
                areasDeliveryDate: [],
                areasLatestRevision: [],
                areasDelivered: [],
                delivered: true,
                areasActive: [],
                active: true,
                same: false,
                isSame: [],
                deliveryDate: new Date("2100-12-30"),
                latestRevision: new Date("2100-12-30")
              };
            }
            
            var areaDate = new Date(area.revisions[project.deliverySchedule.unpublished]);
            var revision = new Date(area.revisions[project.deliverySchedule.unpublished]);
            
            if(angular.isDefined(project.deliverySchedule.published)){
              revision = new Date(area.revisions[project.deliverySchedule.published]);
            }
           
            var isSame = component.isSame[area.$id];
            var deliveryDate = component.areasDeliveryDate[area.$id];
            var latestRevision = component.areasLatestRevision[area.$id];
            var areaDelivered = component.areasDelivered[area.$id];
            var areaActive = component.areasActive[area.$id];
            
            if (angular.isUndefined(deliveryDate)) {
              deliveryDate = component.areasDeliveryDate[area.$id] = areaDate;
            }
            
            if (angular.isUndefined(latestRevision)) {
              latestRevision = component.areasLatestRevision[area.$id] = revision;
            }
            
            if (angular.isUndefined(isSame)) {
              if(dateUtil.toIsoDate(deliveryDate) === dateUtil.toIsoDate(latestRevision)){
                isSame = component.isSame[area.$id] = true;
              }else{
                isSame = component.isSame[area.$id] = false;
              }
            }
            
            if (angular.isUndefined(areaDelivered)) {
              if (angular.isDefined(area.delivered) && area.delivered) {
                areaDelivered = component.areasDelivered[area.$id] = true;
              } else {
                areaDelivered = component.areasDelivered[area.$id] = false;
              }
            }
            
            if (angular.isUndefined(areaActive)) {
              if (area.active) {
                areaActive = component.areasActive[area.$id] = true;
              } else {
                areaActive = component.areasActive[area.$id] = false;
              }
            }
            
            if (!areaDelivered){
              component.delivered = false;
              week.delivered = false;
            }
            
            if (!areaActive){
              component.active = false;
              week.active = false;
            }
            
            component.areas.push(area);
            if (areaDate < component.deliveryDate) {
              component.deliveryDate = areaDate;
            }
            if (latestRevision < component.latestRevision) {
              component.latestRevision = latestRevision;
            }
            if(dateUtil.toIsoDate(component.deliveryDate) === dateUtil.toIsoDate(component.latestRevision)){
              component.same = true;
            }
          });

          angular.forEach(areasGroupedByWeek.groups, function (week) {
            var totalComponents = Object.keys(week.components).length;
            week.percentage = (1/totalComponents)*100;
          });

          var totalWeeks = Object.keys(areasGroupedByWeek.groups).length;
          areasGroupedByWeek.percentage = (1 / totalWeeks) * 100;

          return areasGroupedByWeek;
        })
        .then(function (groupedAreas) {
          deferred.resolve(groupedAreas);
        })
        .catch(function (error) {
          deferred.reject(error);
        });

      return deferred.promise;

      function sortByDeliveryDate(areas) {
        return areas.sort(function (a, b) {
          if (a.revisions[project.deliverySchedule.unpublished] < b.revisions[project.deliverySchedule.unpublished]) {
            return -1;
          } else if (a.revisions[project.deliverySchedule.unpublished] > b.revisions[project.deliverySchedule.unpublished]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
      
      function sortByFirstRevision(areas) {
        return areas.sort(function (a, b) {
          
          var revisionA = Object.keys(a.revisions);
          var revisionB = Object.keys(b.revisions);
          var firstRevisionA = revisionA[0];
          var firstRevisionB = revisionB[0];
          
          if (a.revisions[firstRevisionA] < b.revisions[firstRevisionB]) {
            return -1;
          } else if (a.revisions[firstRevisionA] > b.revisions[firstRevisionB]) {
            return 1;
          } else {
            return 0;
          }
        });
      }
    }

    function getProjectAreasByPhase(project, components, areaSort) {
      var deferred = $q.defer();
      getAreasByPhase(project, components, areaSort)
        .then(function (areasByPhase) {
          deferred.resolve(areasByPhase);
        });
      return deferred.promise;
    }

    function getProjectAreasByWeek(project, components, areaSort) {
      var deferred = $q.defer();
      getAreasByWeek(project, components, areaSort)
        .then(function (areasByWeek) {
          deferred.resolve(areasByWeek);
        });
      return deferred.promise;
    }

    function getArea(areaKey) {
      return schema.getObject(schema.getRoot()
                              .child("areas")
                              .child(areaKey));
    }

    function updateArea(updatedArea) {
      var firebaseArea = getArea(updatedArea.$id);

      return firebaseArea.$loaded()
        .then(doUpdate)
        .then(auditUpdateArea)
        .then(function () {
          return updatedArea.$id;
        })
        .catch(function (error) {
          $log.log("Unable to updateArea: " + error);
        });

      function doUpdate() {
        // copy changes from updatedArea to firebaseobject
        angular.extend(firebaseArea, updatedArea);
        // Handle data migration...
        if (!firebaseArea.timestamps) {
          firebaseArea.timestamps = {
            created: firebaseUtil.serverTime()
          };
        }
        firebaseArea.timestamps.modified = firebaseUtil.serverTime();

        return firebaseArea.$save();
      }

      function auditUpdateArea(areaId) {
        var id = [firebaseArea.phase, firebaseArea.floor, firebaseArea.type].join("-");
        return audit.project("Updated area '" + id + "'", firebaseArea.project);
      }
    }

    function addRevisionToAreas(projectKey, revision) {
      var areas = getAreas(projectKey);

      return areas.$loaded()
        .then(function () {
          angular.forEach(areas, function (area) {

            var revisions = Object.keys(area.revisions);
            var lastRevisionDate = area.revisions[revisions[revisions.length -1]];

            area.revisions[revision] =  lastRevisionDate;
            areas.$save(area);
          });
        });
    }

    function modifyCompletedPanels(areaRef, totalPanelCount, totalPanelArea) {
      return this.getProjectArea(areaRef)
        .$loaded()
        .then(function (area) {
          
          var completedPanels = getOptionalNumber(totalPanelCount);
          var completedArea = getOptionalNumber(totalPanelArea);
          
          if(completedPanels < 0){
            completedPanels = 0;
            completedArea = 0;
          }
          
          area.completedPanels = completedPanels;
          area.completedArea = completedArea;

          area.timestamps.modified = firebaseUtil.serverTime();
          return area.$save();
        })
        .catch(function (error) {
          $log.log("Error in areasService.modifyCompletedPanels: " + angular.toJson(error));
          return $q.reject("Error in areasService.modifyCompletedPanels(" + areaRef + "): " + error);
        });

      function getOptionalNumber(optionalValue) {
        return angular.isDefined(optionalValue) ? parseFloat(optionalValue) : 0;
      }
    }

    var panelsService;

    function deleteArea(areaRef, token) {
      // Lazily inject panelsService, to avoid circular dependency if
      // done at construction time.
      if (!panelsService) {
        panelsService = $injector.get("panelsService");
      }

      var response = {
        "status": "NOT_IMPLEMENTED"
      };

      return panelsService.panelsForArea(areaRef)
        .$loaded()
        .then(function (panels) {
          if (angular.isUndefined(panels) || panels.length < 1) {
            //delete area
            return deleteArea(areaRef);
          } else {
            if(panelsStarted(panels)) {
              response.status = "PANELS_STARTED";
            } else if(angular.isDefined(token) && token === areaRef) {
              //this is the confirmed invocation.
              return panelsService.deletePanelsForArea(areaRef)
                .then(function () {
                  return deleteArea(areaRef);
                });
            } else {
              response.status = "PANELS_CONFIRM";

              // Just use the areaRef as the confirmation token.
              // Since this is all client side, there's no point using
              // a hash or similar to prevent spoofing. We just want
              // to ensure there's a two step invocation.
              response.token = areaRef;
            }
            return undefined;
          }
        })
        .then(function (removedAreaRef) {
          if (angular.isDefined(removedAreaRef)) {
            response.status = "OK";
          }
        })
        .then(function () {
          return response;
        })
        .catch(function (error) {
          response.status = "ERROR";
          return response;
        });

      function deleteArea(areaRef) {
        return getArea(areaRef).$remove();
      }

      function panelsStarted(panels) {
        var panelsStarted = false;
        angular.forEach(panels, function (panel) {
          if (angular.isDefined(panel.qa) &&
              (angular.isDefined(panel.qa["started"]) || angular.isDefined(panel.qa["completed"]))) {
            panelsStarted = true;
          }
        });
        return panelsStarted;
      }
    }

    function getEarliestDeliveryDate(projectRef, revisionRef) {
      return getAreas(projectRef)
        .$loaded()
        .then(function (areas) {
          var earliestDeliveryDate = "9999-99-99";

          angular.forEach(areas, function (area) {
            if (angular.isDefined(area.revisions[revisionRef]) && area.revisions[revisionRef] < earliestDeliveryDate) {
              earliestDeliveryDate = area.revisions[revisionRef];
            }
          });

          return earliestDeliveryDate;
        });
    }
    
    function getEarliestDeliveryDateAfterToday(projectRef, revisionRef) {
      return getAreas(projectRef)
        .$loaded()
        .then(function (areas) {
          var earliestDeliveryDate = "9999-99-99";
          var todaysDate = dateUtil.todayAsIso();

          angular.forEach(areas, function (area) {
            if (angular.isDefined(area.revisions[revisionRef]) && area.revisions[revisionRef] < earliestDeliveryDate && area.revisions[revisionRef] > todaysDate) {
              earliestDeliveryDate = area.revisions[revisionRef];
            }
          });

          return earliestDeliveryDate;
        });
    }
  }
})();
