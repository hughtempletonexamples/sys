(function () {
  "use strict";

  angular
    .module("innView.projects")
    .filter("nameInitials", filter);

  function filter() {

    return nameInitialsFilter;

    function nameInitialsFilter(input) {
      if (!input) {
        return "";
      }

      var name = input;
      var initials = name.match(/\b\w/g) || [];
      initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();
      return initials;
    }
  }

})();
