(function () {
  "use strict";

  angular
    .module("innView.projects")
    .factory("componentsService", componentsService);

  /**
   * @ngInject
   */
  function componentsService($log, $q, schema) {

    var components;
    
    var service = {
      getComponentsForProject: getComponentsForProject,
      create: create,
      updateComponentNote: updateComponentNote
    };

    init();
    
    return service;

    ////////////

    function init() {
      components = schema.getRoot().child("components");
    }
    
    function getComponentsForProject(projectKey) {
      return schema.getArray(components.orderByChild("project").equalTo(projectKey));
    }
    
    function getComponent(componentKey) {
      return schema.getObject(components.child(componentKey));
    }
    
    function updateComponentNote(componentId, notes) {
      var deferred = $q.defer();
      getComponent(componentId)
        .$loaded()
        .then(function (component) {
          component.comments = notes;
          component.$save()
            .then(function () {
              deferred.resolve(componentId);
            });
        });
      return deferred.promise;
    }

    function create(projectKey, areas, componentType) {
      
      var newComponent = {
        project: projectKey,
        areas: createSet(areas),
        type: componentType
      };

      var deferred = $q.defer();
      components.push(newComponent, callback);

      return deferred.promise;

      // Mockfirebase.push() is not thenable :-(
      function callback(error) {
        if (!error) {
          deferred.resolve();
        }
        else {
          $log.error(error);
          deferred.reject(error);
        }
      }

      function createSet(arrayOfItems) {
        var set = {};
        angular.forEach(arrayOfItems, function (item) {
          set[item] = true;
        });
        return set;
      }
    }
  }
})();
