(function () {
  "use strict";
  
  var FIREBASE_ROOT = "revisions";

  angular
    .module("innView.projects")
    .factory("revisionsService", revisionsService);

  /**
   * @ngInject
   */
  function revisionsService($q, $filter, dateUtil, schema) {

    var service = {
      createRevision: createRevision,
      getRevisionsByProjectId: getRevisionsByProjectId,
      deleteRevision: deleteRevision,
      removeRevisions: removeRevisions
    };
    
    return service;

    ///////////
    
    function createRevision(projectKey) {
      var response = $q.defer();
      var revision = {
        createdDate: dateUtil.toIsoDate(new Date()),
        project: projectKey
      };

      if (angular.isDefined(schema.status.userName)){
        revision.deliveryManager = schema.status.userName;
      }

      var revisionRef = schema.getRoot().child("revisions").push(revision);

      response.resolve(revisionRef.key);

      return response.promise;
    }
    
    function removeRevisions(projectRef) {
      var promises = [];

      return getRevisionsByProjectId(projectRef)
        .$loaded()
        .then(function (revisions) {
          angular.forEach(revisions, function (revision) {
            promises.push(deleteRevision(revision.$id));
          });

        })
        .then(function () {
          return $q.all(promises);
        });
    }

    function deleteRevision(revisionRef) {
      return schema.getObject(schema.getRoot().child(FIREBASE_ROOT).child(revisionRef)).$remove();
    }
    
    function getRevisionsByProjectId(projectKey) {
      return schema.getArray(schema
        .getRoot()
        .child("revisions")
        .orderByChild("project")
        .equalTo(projectKey));
    }
      
  }
})();
