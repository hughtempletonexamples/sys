(function () {
  "use strict";

  angular
    .module("innView.projects")
    .filter("keyLength", filter);

  function filter() {

    return keyLengthFilter;

    function keyLengthFilter(input, plus) {
      if (!angular.isObject(input)) {
        throw Error("Usage of non-objects with keylength filter!!");
      }
      if (angular.isUndefined(plus)){
        plus = 0;
      }
      return Object.keys(input).length + plus;
    }
  }

})();
