(function () {
  "use strict";

  angular
    .module("innView.projects")
    .factory("projectsService", projectsService);

  /**
   * @ngInject
   */
  function projectsService($log, $q, schema, audit, revisionsService, areasService, plannerService, firebaseUtil, dateUtil, referenceDataService) {
    var service = {
      createProject: createProject,
      updateProject: updateProject,
      getProject: getProject,
      getProjectArray: getProjectArray,
      getProjects: getProjects,
      checkDateDiffWithDeliveryDateUpdate: checkDateDiffWithDeliveryDateUpdate,
      promoteUnpublishedRevision: promoteUnpublishedRevision,
      resetRevisionsProject: resetRevisionsProject,
      removeNextDeliveryDateProject: removeNextDeliveryDateProject,
      touch: touch
    };

    return service;

    ////////////

    function createProject(project) {
      return queryByProjectId(project.id)
        .then(checkDuplicate)
        .then(addNewProject);

      function checkDuplicate(projects) {
        if (projects.length !== 0) {
          return $q.reject("Duplicate id");
        }

        return projects;
      }

      function addNewProject(projects) {
        
        project.active = true;

        var newProject = angular.extend(
          {},
          project);

        updateModified(newProject);
        
        if(angular.isDefined(newProject.estimatedAreas) && angular.isDefined(newProject.estimatedAreas.Ext)){
          newProject.estimatedAreas.ExtH = Math.floor((10/ 100) * newProject.estimatedAreas.Ext);
        }
        
        if(angular.isDefined(newProject.siteStart)){
          newProject.siteStart = dateUtil.toIsoDate(newProject.siteStart);
        }

        return projects
          .$add(newProject)
          .then(createRevisionForProject)
          .then(setUnpublishedRevisionOnProject)
          .then(auditCreation)
          .then(function () {
            return $q.when(project.id);
          })
          .catch(function () {
            return $q.reject("Save failed");
          });
      }

      function auditCreation(projectRef) {
        return audit.project("Created project", projectRef.key, project.id);
      }
    }

    function checkDateDiffWithDeliveryDateUpdate(updatedProject, originalSiteStart, nextDeliveryDate, siteStarted, updateDelivery, components) {

      var dateData = {};
      var diffDays;

      if (!siteStarted){
        if (angular.isDefined(originalSiteStart)) {
          var originalProjectDate = dateUtil.toIsoDate(originalSiteStart);
          var updatedProjectDate = dateUtil.toIsoDate(updatedProject.siteStart);
        }
      }else{
        if (angular.isDefined(nextDeliveryDate)) {
          var originalProjectDate = dateUtil.toIsoDate(nextDeliveryDate);
          var updatedProjectDate = dateUtil.toIsoDate(updatedProject.nextDeliveryDate);
        }
      }

      if (dateUtil.validDate(originalProjectDate) && dateUtil.validDate(updatedProjectDate)){

        if (updatedProject.includeSaturday) {
          diffDays = dateUtil.daysBetweenWeekDaysAndSat(originalProjectDate, updatedProjectDate);
        }else{
          diffDays = dateUtil.daysBetweenWeekDaysOnly(originalProjectDate, updatedProjectDate);
        }

        dateData.diffDays = diffDays;
        dateData.currentDate = originalProjectDate;
        dateData.changedDate = updatedProjectDate;
        dateData.includeSaturday = updatedProject.includeSaturday;
      }
      
      if(updateDelivery){
        return updateAreaDeliveryDate()
                .then(function () {
                  return $q.resolve(dateData);
                });
      }else{
        return $q.resolve(dateData);
      }

      function updateAreaDeliveryDate() {
        var areas = [];
        var todaysDate = dateUtil.todayAsIso();
        if (Object.keys(dateData).length !== 0 && (dateData.diffDays > 0 || dateData.diffDays < 0)) {
          return areasService.getProjectAreas(updatedProject.$id)
            .$loaded()
            .then(function (areasData) {
              if (!siteStarted){
                areas = areasData;
              }else{
                angular.forEach(areasData, function (area) {
                  if (area.revisions[updatedProject.deliverySchedule.unpublished] > todaysDate){
                    areas.push(area);
                  }
                });
              }
              return referenceDataService.getComponents().$loaded();
            })
            .then(function (srdComponents) {
              var componentsMap = createAreasMap(components, srdComponents);
              return updateArea(areas, componentsMap);
            });       
        } else {
          return $q.when();
        }
      }
      
      function updateArea(areas, componentsMap) {
        var promises = [];
        angular.forEach(areas, function (area) {
          var componentKey = componentsMap[area.$id];
          if (angular.isDefined(componentKey)) {
            var plusDate;
            if(dateData.diffDays > 0){
              if (updatedProject.includeSaturday) {
                plusDate = dateUtil.addWorkingDaysAndSat(area.revisions[updatedProject.deliverySchedule.unpublished], dateData.diffDays);
              } else {
                plusDate = dateUtil.addWorkingDays(area.revisions[updatedProject.deliverySchedule.unpublished], dateData.diffDays);
              }
            }else{
              var diffDays = Math.abs(dateData.diffDays);
              if (updatedProject.includeSaturday) {
                plusDate = dateUtil.subtractWorkingDaysAndSat(area.revisions[updatedProject.deliverySchedule.unpublished], diffDays);
              } else {
                plusDate = dateUtil.subtractWorkingDays(area.revisions[updatedProject.deliverySchedule.unpublished], diffDays);
              }
            }
            area.revisions[updatedProject.deliverySchedule.unpublished] = plusDate;
            promises.push(
              areasService.updateProjectArea(area)
              );
          }
        });
        return $q.all(promises);
      }
      
      function createAreasMap( components, srdComponents ) {
        var areasMap = {};
        var areasMapCounter = {};

        angular.forEach(components, function ( component ) {
          if (angular.isUndefined(areasMapCounter[component.type])) {
            areasMapCounter[component.type] = 0;
          }
          areasMapCounter[component.type]++;
          angular.forEach(component.areas, function ( value, area ) { 
            var componentName = component.type + areasMapCounter[component.type];
            areasMap[area] = {
              id: component.$id,
              name: componentName,
              comments: component.comments
            };
          });
        });

        return areasMap;
      }

    }
    
    function resetRevisionsProject(projectRef) {
      
      var promises = [];

      return schema.getObject(schema.getRoot().child("projects").child(projectRef).child("deliverySchedule"))
        .$loaded()
        .then(function (deliverySchedule) {
          return deliverySchedule.$remove();
        })
        .then(function () {
          var projectHolder = {
            key: projectRef
          };
          return createRevisionForProject(projectHolder);
        })
        .then(setUnpublishedRevisionOnProject)
        .then(function () {
          return $q.all(promises);
        });
    }
    
    function removeNextDeliveryDateProject(projectRef) {

      return schema.getObject(schema.getRoot().child("projects").child(projectRef).child("nextDeliveryDate"))
        .$loaded()
        .then(removeNextDeliveryDate);
      
      function removeNextDeliveryDate(nextDeliveryDate) {
        return nextDeliveryDate.$remove();
      }
    }

    function updateProject(updatedProject) {
      var projectId;

      return schema.getObject(schema.getRoot().child("projects").child(updatedProject.$id))
        .$loaded()
        .then(saveUpdate)
        .then(auditUpdate);
      
      function saveUpdate(originalProject) {
        projectId = originalProject.id;
        angular.extend(originalProject, updatedProject);
        originalProject.id = projectId;
        if (angular.isDefined(originalProject.estimatedAreas) && angular.isDefined(originalProject.estimatedAreas.Ext)){
          originalProject.estimatedAreas.ExtH = Math.floor((10/ 100) * originalProject.estimatedAreas.Ext);
        }
        if(angular.isDefined(originalProject.siteStart)){
          originalProject.siteStart = dateUtil.toIsoDate(originalProject.siteStart);
        }
        if(angular.isDefined(originalProject.nextDeliveryDate)){
          originalProject.nextDeliveryDate = dateUtil.toIsoDate(originalProject.nextDeliveryDate);
        }
        updateModified(originalProject);
        return originalProject.$save();
      }

      function auditUpdate(projectRef) {
        return audit.project("Updated project", updatedProject.$id, projectId);
      }
    }

    function createRevisionForProject(projectRef) {
      return revisionsService.createRevision(projectRef.key)
        .then(function (revisionKey) {
          return {
            project: projectRef.key,
            revision: revisionKey
          };
        });
    }

    function setUnpublishedRevisionOnProject(keys) {
      var projectRef = schema.getRoot().child("projects").child(keys.project);
      return schema.getObject(projectRef)
        .$loaded()
        .then(addUnpublishedRevision);

      function addUnpublishedRevision(createdProject) {
        if (angular.isUndefined(createdProject.deliverySchedule)) {
          createdProject.deliverySchedule = {};
        }
        createdProject.deliverySchedule["unpublished"] = keys.revision;
        return createdProject.$save();
      }
    }

    function getProject(projectId) {
      return queryByProjectId(projectId)
        .then(function (projects) {
          if (projects.length) {
            return $q.when(projects[0]);
          } else {
            return $q.reject("Unknown project id");
          }
        });
    }
    
    function getProjectArray(projectId) {
      return queryByProjectId(projectId)
        .then(function (projects) {
          if (projects.length) {
            return $q.when(projects);
          } else {
            return $q.reject("Unknown project id");
          }
        });
    }
    
    function queryByProjectId(projectId) {
      var query = schema.getRoot().child("projects").orderByChild("id").equalTo(projectId);
      return schema.getArray(query)
        .$loaded();
    }

    function getProjects() {
      return schema.getArray(schema.getRoot().child("projects").orderByChild("id"));
    }

    function promoteUnpublishedRevision(projectId) {
      var projectRef= schema.getRoot().child("projects").child(projectId);

      var project = schema.getObject(projectRef);

      return project
        .$loaded()
        .then(publishDeliverySchedule)
        .then(createRevisionForProject)
        .then(setUnpublishedRevisionOnProject)
        .then(addRevisionToAreas)
        .then(updateNextDeliveryDateOrSiteStart)
        .then(auditPublishSchedule)
        .then(plannerService.refreshProductionPlan)
        .then(function () {
          return $q.when(project);
        });

      function publishDeliverySchedule() {
        project.deliverySchedule["published"] = angular.copy(project.deliverySchedule["unpublished"]);
        ensureDeliveryScheduleHasRevisions(project.deliverySchedule);
        project.deliverySchedule["revisions"][angular.copy(project.deliverySchedule["unpublished"])] = true;
        return project.$save();
      }

      function ensureDeliveryScheduleHasRevisions(deliverySchedule) {
        if (angular.isUndefined(deliverySchedule.revisions)) {
          deliverySchedule.revisions = {};
        }
      }

      function addRevisionToAreas(projectRef) {
        var project = schema.getObject(projectRef);
        project.$loaded()
          .then(function () {
            areasService.addRevisionToProjectAreas(project.$id, project.deliverySchedule["unpublished"]);
          });
      }
      
      function updateNextDeliveryDateOrSiteStart() {
        return areasService.getEarliestDeliveryDate(project.$id, project.deliverySchedule["published"])
          .then(function (earliestDeliveryDate) {
            if (angular.isUndefined(project.siteStart)) {
              project.siteStart = earliestDeliveryDate;
            }
            return project;
          })
          .then(function (project) {
            if (project.siteStart > dateUtil.todayAsIso()) {
              return areasService.getEarliestDeliveryDate(project.$id, project.deliverySchedule["published"])
                .then(function (earliestDeliveryDate) {
                  if (earliestDeliveryDate !== "9999-99-99") {
                    project.siteStart = earliestDeliveryDate;
                  }
                  return project.$save();
                });
            } else {
              return areasService.getEarliestDeliveryDateAfterToday(project.$id, project.deliverySchedule["published"])
                .then(function (AfterToday) {
                  if (AfterToday !== "9999-99-99") {
                    project.nextDeliveryDate = AfterToday;
                  }
                  return project.$save();
                });
            }
          });
      }

      function auditPublishSchedule() {
        return audit.project("Published delivery schedule", projectId, project.id);
      }
    }

    function touch(projectRef) {
      if (angular.isDefined(projectRef)) {
        return schema.getObject(schema.getRoot().child("projects").child(projectRef))
          .$loaded()
          .then(function (project) {
            updateModified(project);
            return project.$save();
          })
          .catch(function (error) {
            $q.reject("Error in projectsService.touch(" + projectRef + "): " + error);
          });
      } else {
        return $q.when();
      }
    }

    function updateModified(item) {
      var userId = schema.status.userId;

      // Set "created" if it is not present
      if (!item.timestamps) {
        item.timestamps = {
          created: firebaseUtil.serverTime(),
          createdBy: userId
        };
      }

      item.timestamps.modified = firebaseUtil.serverTime();
      item.timestamps.modifiedBy = userId;
    }
  }
})();
