(function () {
  "use strict";

  angular
    .module("innView.projects", [
      "ngRoute",
      "firebase",
      "innView.core",
      "ui.grid",
      "ui.grid.pinning",
      "ui.grid.selection"
    ]);
})();
