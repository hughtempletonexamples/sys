(function () {
  "use strict";

  angular
    .module("innView.projects")
    .controller("ProjectDeliveryScheduleController", ProjectDeliveryScheduleController);

  /**
   * @ngInject
   */
  function ProjectDeliveryScheduleController($log, $q, $routeParams, $mdDialog, $document, $location, $window, dateUtil, referenceDataService, revisionsService, projectsService, areasService, componentsService) {
    var vm = this;

    vm.project = null;
    vm.selectedTab = "delivery-schedule";
    vm.hidden = {};
    vm.toggleVisibility = toggleVisibility;
    vm.toggleDelivered = toggleDelivered;
    vm.toggleShowHide = toggleShowHide;
    vm.isToggleDelivered = false;
    vm.isToggleShowHide = false;
    vm.groupedBy = "phase";
    vm.viewedBy = "simple";
    vm.areaSort = false;
    vm.viewBy = viewBy;
    vm.sortBy = sortBy;
    vm.goto = goto;
    vm.groupBy = groupBy;
    vm.changeShowHideForComponent = changeShowHideForComponent;
    vm.changeShowHideForArea = changeShowHideForArea;
    vm.changeDeliveryDateForComponent = changeDeliveryDateForComponent;
    vm.changeDeliveryDateForArea = changeDeliveryDateForArea;
    vm.deleteSelectedArea = deleteSelectedArea;
    vm.showAddArea = showAddArea;
    vm.editArea = editArea;
    vm.editComponentNote = editComponentNote;
    vm.todayDate = new Date();
    vm.dateFilter = function (date){
      date = dateUtil.asDate(date);
      var day = date.getDay();
      if (date > vm.todayDate) {
        if (angular.isDefined(vm.project) && angular.isDefined(vm.project.includeSaturday)) {
          return day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6;
        } else {
          return day === 1 || day === 2 || day === 3 || day === 4 || day === 5;
        }
      }else{
        return day === 0 || day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6 || day === 7;
      }
    };
    
    init();

    ////////////

    function init() {
      if ($routeParams.projectId) {
        loadProject($routeParams.projectId)
          .then(loadProjectStatus)
          .then(loadRevisions)
          .then(loadAreas)
          .catch(showError);
      }
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/projects/" + vm.project.id + "/" + page);
      }
    }

    function loadProject(id) {
      return projectsService
        .getProject(id)
        .then(function (project) {
          vm.project = project;
          vm.hidden = {};
          return project.$id;
        });
    }

    function loadProjectStatus() {
      if (angular.isDefined(vm.project.deliverySchedule.revisions)) {
        vm.totalRevisions = Object.keys(vm.project.deliverySchedule.revisions).length - 1;
      }else{
        delete vm.totalRevisions;
      }
    }
    
    function loadRevisions() {
      if (angular.isDefined(vm.project.deliverySchedule.revisions)) {
        revisionsService.getRevisionsByProjectId(vm.project.$id)
          .$loaded()
          .then(function (revisions) {
            vm.latestRevision = revisions[revisions.length-1];
          });
      }else{
        vm.latestRevision = 0;
      }
    }

    function loadProjectAreas(getAreasForProject) {
      vm.components = componentsService.getComponentsForProject(vm.project.$id);
      vm.components
        .$loaded(function () {
          processAreas();
          vm.components.$watch(processAreas);
        });

      function processAreas() {
        getAreasForProject(vm.project, vm.components, vm.areaSort)
          .then(function (groupedAreas) {
            vm.groupedAreas = groupedAreas;
            angular.forEach(groupedAreas.groups, function (group, groupName) {
              if (angular.isUndefined(vm.hidden[groupName])) {
                vm.hidden[groupName] = false;
              }
              angular.forEach(group.components, function (component, componentName) {
                if (angular.isUndefined(vm.hidden[groupName + "_"+ componentName])) {
                  vm.hidden[groupName + "_"+ componentName] = true;
                }
              });
            });
            angular.forEach(groupedAreas.groups, function (group, groupName) {  
              angular.forEach(group.components, function (component, componentName) {
                if(!vm.isToggleDelivered && !vm.isToggleShowHide){
                  group.totalGroupSpan++;
                  if (!vm.hidden[groupName + "_"+ componentName]) {
                    vm.groupedAreas.groups[groupName].totalGroupSpan += vm.groupedAreas.groups[groupName].components[componentName].areas.length;
                  }
                }else{
                  if(vm.isToggleDelivered){
                    if(!component.delivered){
                      group.totalGroupSpan++;
                      if (!vm.hidden[groupName + "_" + componentName]) {
                        var notDelivered = 0;
                        angular.forEach(component.areas, function (area) {
                          if (!component.areasDelivered[area.$id]){
                            notDelivered++;
                          }
                        });
                        vm.groupedAreas.groups[groupName].totalGroupSpan += notDelivered;
                      }
                    }
                  }
                  if(vm.isToggleShowHide){
                    if(!component.active){
                      group.totalGroupSpan++;
                      if (!vm.hidden[groupName + "_" + componentName]) {
                        var notActive = 0;
                        angular.forEach(component.areas, function (area) {
                          if (!component.areasActive[area.$id]){
                            notActive++;
                          }
                        });
                        vm.groupedAreas.groups[groupName].totalGroupSpan += notActive;
                      }
                    }
                  }
                }
              });
            });
          });
      }
    }

    function toggleVisibility(key) {

      vm.hidden[key] = !vm.hidden[key];
      
      var groupNameAndcomponentName = key.split("_");
      var groupName = groupNameAndcomponentName[0];
      var componentName = groupNameAndcomponentName[1];
      
      if (!vm.isToggleDelivered && !vm.isToggleShowHide) {
        if(vm.hidden[key]){
          vm.groupedAreas.groups[groupName].totalGroupSpan -= vm.groupedAreas.groups[groupName].components[componentName].areas.length;
        }else{
          vm.groupedAreas.groups[groupName].totalGroupSpan += vm.groupedAreas.groups[groupName].components[componentName].areas.length;
        }
      }else{
        if (vm.isToggleDelivered){
          var notDelivered = 0;
          angular.forEach(vm.groupedAreas.groups[groupName].components[componentName].areas, function (area) {
            if (!vm.groupedAreas.groups[groupName].components[componentName].areasDelivered[area.$id]) {
              notDelivered++;
            }
          });
          if (vm.hidden[key]) {
            vm.groupedAreas.groups[groupName].totalGroupSpan -= notDelivered;
          } else {
            vm.groupedAreas.groups[groupName].totalGroupSpan += notDelivered;
          }
        }
        if (vm.isToggleShowHide){
          var notActive = 0;
          angular.forEach(vm.groupedAreas.groups[groupName].components[componentName].areas, function (area) {
            if (!vm.groupedAreas.groups[groupName].components[componentName].areasActive[area.$id]) {
              notActive++;
            }
          });
          if (vm.hidden[key]) {
            vm.groupedAreas.groups[groupName].totalGroupSpan -= notActive;
          } else {
            vm.groupedAreas.groups[groupName].totalGroupSpan += notActive;
          }
        }
      }
    
    }
    
    function toggleDelivered(){
      vm.isToggleDelivered = !vm.isToggleDelivered;
      
      loadAreas();
    }
    
    function toggleShowHide(){
      vm.isToggleShowHide = !vm.isToggleShowHide;
      
      loadAreas();
    }
    

    function groupBy(grouping) {
      vm.groupedBy = grouping;

      loadAreas();
    }
    
    function viewBy(viewing) {
      vm.viewedBy = viewing;

      loadAreas();
    }
    
    function sortBy() {
      vm.areaSort = !vm.areaSort;
      
      loadAreas();
    }

    function loadAreas() {
      switch(vm.groupedBy) {
      case "week":
        loadProjectAreas(areasService.getProjectAreasByWeek);
        break;
      default:
      case "phase":
        loadProjectAreas(areasService.getProjectAreasByPhase);
        break;
      }
    }

    function changeDeliveryDateForComponent(component, componentName) {
      if (componentName !== "NONE") {
        angular.forEach(component.areas, function (area) {
          updateArea(area, component.deliveryDate);
        });
        loadAreas();
      }
    }

    function updateArea(area, deliveryDate) {
      if (angular.isDefined(deliveryDate)) {
        area.revisions[vm.project.deliverySchedule.unpublished] = dateUtil.toIsoDate(deliveryDate);
        areasService.updateProjectArea(area);
      }
    }

    function changeDeliveryDateForArea(area, deliveryDate) {
      updateArea(area, deliveryDate);
      loadAreas();
    }
    
    function changeShowHideForArea(area) {
      area.active = !area.active;
      areasService.updateProjectArea(area);
      loadAreas();
    }
    
    function changeShowHideForComponent(component) {
      var saves = [];
      component.active = !component.active;
      angular.forEach(component.areas, function (area) {
        area.active = component.active;
        saves.push(areasService.updateProjectArea(area));
      });
      $q.all(saves).then(function () {
        loadAreas();
      });

    }

    function deleteSelectedArea(areaId, token) {

      return areasService.delete(areaId, token)
        .then(function (response) {
          switch(response.status) {
          case "PANELS_CONFIRM":
            $mdDialog.show(
              $mdDialog.confirm()
                .title("Delete Area?")
                .textContent("This area has panels associated with it. They will be deleted along with the area.")
                .ok("Ok")
                .cancel("Cancel")
            ).then(function () {
              deleteSelectedArea(areaId, response.token);
            });

            break;

          case "PANELS_STARTED":
            $mdDialog.show(
              $mdDialog.alert()
                .clickOutsideToClose(true)
                .title("Panels Built")
                .textContent("This area cannot be deleted because work has been recorded against its panels.")
                .ok("Ok")
            );
            break;

          case "OK":
            areasService.getProjectAreas(vm.project.$id).$loaded()
                    .then(function ( areas ) {
                      if (areas.length === 0) {
                        return revisionsService.removeRevisions(vm.project.$id)
                                .then(function () {
                                  return projectsService.removeNextDeliveryDateProject(vm.project.$id);
                                })
                                .then(function () {
                                  return projectsService.resetRevisionsProject(vm.project.$id);
                                })
                                .then(loadProjectStatus)
                                .then(loadRevisions)
                                .then(loadAreas)
                                .catch(showError);
                      } else {
                        loadAreas();
                      }
                    });
            break;
          default:
            break;
          }
        });

    }

    function parentElement() {
      return angular.element($document.body);
    }

    function showAddArea(event) {
      $mdDialog.show({
        parent: parentElement(),
        targetEvent: event,
        templateUrl: require("./addArea.html"),
        locals: {
          project: vm.project,
          area: undefined,
          dateFilter: vm.dateFilter,
          todayDate: vm.todayDate 
        },
        controller: areaDialogController,
        controllerAs: "vm"
      });
    }

    function editArea(area) {
      $mdDialog.show({
        parent: parentElement(),
        templateUrl: require("./editArea.html"),
        locals: {
          project: vm.project,
          area: area,
          dateFilter: vm.dateFilter,
          todayDate: vm.todayDate 
        },
        controller: areaDialogController,
        controllerAs: "vm"
      });
    }
    
    function editComponentNote(component) {
      $mdDialog.show({
        parent: parentElement(),
        templateUrl: require("./editComponentNote.html"),
        locals: {
          component: component
        },
        controller: componentDialogController,
        controllerAs: "vm"
      });
    }
    
    function componentDialogController($mdDialog, component) {
      var vm = this;

      vm.componentTemplate = {};

      init();

      ///////////

      function init() {
        if (angular.isDefined(component)) {
          vm.componentTemplate.$id = component.componentId;
          vm.componentTemplate.notes = component.comments;
        }
      }

      vm.closeDialog = function () {
        $mdDialog.hide();
      };

      vm.saveComponentNotes = function (isValid) {
        if (isValid) {
          if (angular.isDefined(vm.componentTemplate.$id)) {
            componentsService.updateComponentNote(component.componentId, vm.componentTemplate.notes)
              .then(vm.closeDialog)
              .catch(showError);
          }
        }
      };

      function showError(error) {
        $mdDialog.show(
          $mdDialog.alert()
          .clickOutsideToClose(true)
          .title("There was a problem")
          .textContent(error)
          .ok("Ok")
          .parent(parentElement())
          );
      }
    }
    
    function areaDialogController($mdDialog, project, area, dateFilter, todayDate) {
      var vm = this;

      vm.areaTemplate = {};
      vm.todayDate = todayDate;
      vm.dateFilter = dateFilter;
      
      init();

      ////////////

      function init() {
        if (angular.isDefined(area)) {
          vm.areaTemplate.$id = area.$id;
          vm.areaTemplate.phase = area.phase;
          vm.areaTemplate.floor = area.floor;
          vm.areaTemplate.supplier = area.supplier;
          vm.areaTemplate.deliveryDate = dateUtil.asDate(area.revisions[project.deliverySchedule.unpublished]);
          vm.areaTemplate.offloadMethod = area.offloadMethod;
          vm.areaTemplate.supplier = area.supplier;
          vm.areaTemplate.description = area.description;
          vm.areaTemplate.notes = area.comments;
          if (angular.isDefined(area.numberOfPanels)) {
            vm.areaTemplate.numberOfPanels = area.numberOfPanels;
          }
          if (angular.isDefined(area.areaOfPanels)) {
            vm.areaTemplate.areaOfPanels = area.areaOfPanels;
          }
          if (angular.isDefined(area.estimatedArea) &&
              area.estimatedArea > 0 &&
              angular.isDefined(project.estimatedArea) &&
              angular.isDefined(project.estimatedAreas[area.type]) &&
              project.estimatedAreas[area.type] > 0) {
            vm.areaTemplate.percentageOfBuild = Math.round((area.estimatedArea / project.estimatedAreas[area.type]) * 100);
          }
        }
      }
        
      vm.closeDialog = function () {
        $mdDialog.hide();
      };

      vm.saveArea = function (isValid) {
        if(isValid) {
          if (angular.isDefined(vm.areaTemplate.$id)) {
            var modifiedArea = updateAreaFromTemplate(area, vm.areaTemplate, project.$id);
            modifiedArea.revisions[project.deliverySchedule.unpublished] = dateUtil.toIsoDate(vm.areaTemplate.deliveryDate);
            areasService
              .updateProjectArea(modifiedArea)
              .then(loadAreas)
              .then(vm.closeDialog)
              .catch(showError);
          } else {
            
            referenceDataService.getItem("components", vm.areaTemplate.component)
              .$loaded(function (component) {
                var saves = [];
                // for each child area, create using the details from the area template.
                var area;
                var isoDeliveryDate = dateUtil.toIsoDate(vm.areaTemplate.deliveryDate);
                
                angular.forEach(component.productTypes, function (_, productType) {
                  area = updateAreaFromTemplate({}, vm.areaTemplate, project.$id, productType);
                  saves.push(areasService.createProjectArea(project, area, isoDeliveryDate));
                });
                return $q.all(saves);
              })
              .then(function (createdAreas) {
                return componentsService.create(project.$id, createdAreas, vm.areaTemplate.component);
              })
              .then(vm.closeDialog)
              .catch(showError);
          }
        }
      };

      function updateAreaFromTemplate(area, template, projectId, productType) {
        area.project = projectId;
        area.phase = template.phase;
        area.floor = template.floor;
        area.description = template.description;
        area.supplier = template.supplier;
        
        if (angular.isDefined(template.numberOfPanels)) {
          area.numberOfPanels = template.numberOfPanels;
        }
        
        if (angular.isDefined(template.areaOfPanels)) {
          area.areaOfPanels = template.areaOfPanels;
        }

        if (angular.isDefined(template.offloadMethod)) {
          area.offloadMethod = template.offloadMethod;
        }

        if (angular.isDefined(productType)) {
          area.type = productType;
        }

        if (angular.isDefined(vm.areaTemplate.notes)) {
          area.comments = vm.areaTemplate.notes;
        }

        if (angular.isDefined(vm.areaTemplate.percentageOfBuild) && vm.areaTemplate.percentageOfBuild > 0) {
          if (angular.isDefined(project.estimatedAreas) &&
              angular.isDefined(project.estimatedAreas[productType]) &&
              project.estimatedAreas[productType] > 0) {
            area.estimatedArea =
              Math.round((vm.areaTemplate.percentageOfBuild / 100) * project.estimatedAreas[productType]);
          }
        }

        return area;
      }
    }

    function showError(error) {
      $mdDialog.show(
        $mdDialog.alert()
        .clickOutsideToClose(true)
        .title("There was a problem")
        .textContent(error)
        .ok("Ok")
        .parent(parentElement())
        );
    }
  }
})();
