(function () {
  "use strict";

  angular
    .module("innView.projects")
    .filter("project", filter);

  function filter(schema) {

    var projectIdCache = {};

    projectIdFilter.$stateful = true;
    return projectIdFilter;

    function projectIdFilter(projectRef) {
      if (!projectRef) {
        return "";
      }

      return projectIdCache[projectRef] || lookupId(projectRef);

      function lookupId(projectRef) {
        schema.getObject(schema.getRoot().child("projects").child(projectRef))
          .$loaded()
          .then(function (project) {
            projectIdCache[projectRef] = project.id;
            project.$destroy();
          });

        return projectIdCache[projectRef] = "...";
      }
    }
  }

})();
