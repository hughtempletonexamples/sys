(function () {
  "use strict";

  angular
    .module("innView.projects")
    .controller("ProjectDetailsController", ProjectDetailsController);

  /**
   * @ngInject
   */
  function ProjectDetailsController($route,$log, $location, $window, $routeParams,$mdToast, $mdDialog, projectsService, usersService, areasService, dateUtil, componentsService, dataIntegrityService) {
    var vm = this;
    
    var selectedUsers = new Map();
    vm.clearSearchTerm = clearSearchTerm;
    vm.project = null;
    vm.selectedTab = "detail";
    vm.updateDeliveryManager = updateDeliveryManager;
    vm.save = save;
    vm.checkIfTrue = checkIfTrue;    
    vm.goto = goto;
    vm.siteStarted = false;
    vm.nextDeliveryDateAfterToday = false;
    vm.todayDate = new Date();
    vm.getData = getData;
    vm.searchTerm = "";
    vm.selectedUsers = null;  
    vm.getDesigners = findDesigners;
    vm.designers = [];
    vm.gatwayNames =["G1 - Marketing", "G2 - Sales", "G3 - Bid", "G4 - Tender", "G5 - Pre-Order", "G6 - Delivery Strategy", "G7 - Pre-Construction", "G8 - Site Start", "G9 - Site Handover", "G10 - Feedback"];    
    vm.dateFilter = function (date){
      date = dateUtil.asDate(date);
      var day = date.getDay();
      if (date > vm.todayDate) {
        if (angular.isDefined(vm.project) && angular.isDefined(vm.project.includeSaturday)) {
          return day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6;
        } else {
          return day === 1 || day === 2 || day === 3 || day === 4 || day === 5;
        }
      }else{
        return day === 0 || day === 1 || day === 2 || day === 3 || day === 4 || day === 5 || day === 6 || day === 7;
      }
    };
    
    init();

    ////////////

    function init() {
      if ($routeParams.projectId) {
        loadProject($routeParams.projectId)
          .then(function (){
            return usersService.getAllUsers().$loaded();
          })
          .then(function (users){
            var selectedUsers = [];
            if (angular.isDefined(vm.project) && angular.isDefined(vm.project.notifyUsers)){
              var notifyUsers = vm.project.notifyUsers;
              angular.forEach(users, function (user) {
                if(angular.isDefined(notifyUsers[user.$id])) {
                  selectedUsers.push(user);
                }
              });
              vm.selectedUsers = selectedUsers;
            }
            vm.users = users;
            return vm.users;
          })
          .then(function (data){
            return vm.getData(vm.project.id);
          })
          .then(function (data) {
            vm.tableData = data;
          })
          .then(findDesigners)
          .catch(showError);
      }else{
        vm.users = usersService.getAllUsers()
                .$loaded()
                .then(findDesigners);

      }
    }
    function checkIfTrue(values) {
      var counter;
      if (angular.isDefined(values)) {
        if (!(Object.keys(values).length === 0)) {
          angular.forEach(values, function (data) {
            if (data === false) {
              counter = 1;
            }
          });
          if (counter === 1) {
            return false;
          } else {
            return true;
          }
        } else {
          return false;
        }
      }
    }    
    
    
    function findDesigners(){
      return usersService.getAllUsers()
              .$loaded()
              .then(function (users){
                angular.forEach(users, function (user) {
                  if(angular.isDefined(user.roles.designer)){
                    vm.designers.push(user);
                  }
                  else{
                    user.roles.designer = false;
                  }
                });
                
              });
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/projects/" + vm.project.id + "/" + page);
      }
    }
    
    function gotoGlobal(page){
      $location.url("/index.html#/" + page);
    }
    function getData(projectId) {
      return dataIntegrityService.getDataForProject(projectId);
    }

    function loadProject(id) {
      var project;
      return projectsService.getProjectArray(id)
        .then(function (proj) {
          project = proj;
          return areasService.getProjectAreas(project[0].$id).$loaded();
        })
        .then(function (areas) {
          
          vm.project = project[0];
  
          vm.originalSiteStart = vm.project.siteStart;
          vm.nextDeliveryDate = vm.project.nextDeliveryDate;
          
          var nextDeliveryDate = "9999-99-99";
          var todaysDate = dateUtil.todayAsIso();
          var earliestDeliveryDate = "9999-99-99";
          var revisionRef = vm.project.deliverySchedule.published;

          angular.forEach(areas, function (area) {
            if (angular.isDefined(area.revisions[revisionRef]) && area.revisions[revisionRef] < earliestDeliveryDate) {
              earliestDeliveryDate = area.revisions[revisionRef];
            }
            if (angular.isDefined(area.revisions[revisionRef]) && area.revisions[revisionRef] < nextDeliveryDate && area.revisions[revisionRef] > todaysDate) {
              nextDeliveryDate = area.revisions[revisionRef];
            }
          });

          if(earliestDeliveryDate !== "9999-99-99"){
            vm.project.siteStart = earliestDeliveryDate;
          }
          
          if(nextDeliveryDate !== "9999-99-99"){
            vm.project.nextDeliveryDate = nextDeliveryDate;
          }

          formatDates();
          
          function formatDates() {   
            
            if (angular.toJson(vm.project.siteStart)) {
              if (vm.project.siteStart < dateUtil.todayAsIso()) {
                vm.siteStarted = true;
              }
              if (vm.siteStarted) {
                vm.siteStart = dateUtil.toUserDateNew(vm.project.siteStart);
              }
              vm.project.siteStart = new Date(vm.project.siteStart);
              vm.siteStartDateIsValid = dateUtil.validDate(vm.project.siteStart);
            }
            
            if (angular.toJson(vm.project.nextDeliveryDate)) {
              if (vm.project.nextDeliveryDate > dateUtil.todayAsIso()) {
                vm.nextDeliveryDateAfterToday = true;
              }
              vm.project.nextDeliveryDate = new Date(vm.project.nextDeliveryDate);
              vm.nextDeliveryDateIsValid = dateUtil.validDate(vm.project.nextDeliveryDate);
            }
        
          }
          
          project.$watch(formatDates);
          
          //vm.printModel.project = project;
          //vm.original["project"] = angular.copy(project);
          
          return project.$id;
        });
    }
    
    function clearSearchTerm() {
      vm.searchTerm = "";
    }

    function save(isValid) {
      if (isValid) {
        if (angular.isDefined(vm.project.$id)) {
          setNotifyUsers();
          projectsService.checkDateDiffWithDeliveryDateUpdate(vm.project, vm.originalSiteStart, vm.nextDeliveryDate, vm.siteStarted, false)
            .then(function (dates) {
              if (Object.keys(dates).length !== 0 && dates.diffDays !== 0 && vm.project.deliverySchedule.published) {
                showRevisionPrompt(dates);
              } else {
                projectsService
                  .updateProject(vm.project)
                  .then(function () {
                    $route.reload();
                  })
                  .catch(showError);
              }
            })
            .catch(showError);
        } else {
          setNotifyUsers();
          projectsService
            .createProject(vm.project)
            .then(function () {
              $location.url("/projects/" + vm.project.id);
            })
            .catch(showError);
        }
      }
    }
    
    function setNotifyUsers(){
      selectedUsers.clear();
      angular.forEach(vm.selectedUsers, function (selectedUser) {
        selectedUsers.set(selectedUser.$id, true);
      });
      vm.project.notifyUsers = selectedUsers;
    }

    function updateDeliveryManager(selectedUserId) {
      vm.userLoading = true;
      //retrieve full user details from user service
      usersService.getUserById(selectedUserId)
        .$loaded()
        .then(function (user) {
          if(angular.isUndefined(vm.project.deliveryManager)) {
            vm.project.deliveryManager = {};
          }
          vm.project.deliveryManager.name = user.name;
          vm.project.deliveryManager.email = user.email;
          vm.project.deliveryManager.mobile = user.phone;
          vm.userLoading = false;
        })
        .catch(showError);
    }
    
    function updateProjectAndPublishRevision() {
      componentsService.getComponentsForProject(vm.project.$id).$loaded()
        .then(function (components) {
          return projectsService.checkDateDiffWithDeliveryDateUpdate(vm.project, vm.originalSiteStart, vm.nextDeliveryDate, vm.siteStarted, true, components);
        })
        .then(function () {
          return projectsService.updateProject(vm.project);
        })
        .then(function () {
          return projectsService.promoteUnpublishedRevision(vm.project.$id);
        })
        .then(function (projectId) {
          $route.reload();
        })
        .catch(showError);
    }

    function showRevisionPrompt(dates) {
      var whichDate;
      if (!vm.siteStarted){
        whichDate = "site start date";
      }else{
        whichDate = "next delivery date";
      }
      if(dates.includeSaturday){
        var text = "The " + whichDate + " has changed from " + dateUtil.toUserDate(dates.currentDate) + " to " + dateUtil.toUserDate(dates.changedDate) + " all dates have changed by " + dates.diffDays + " working days, including Saturday. Please select an option below:";
      }else{
        var text = "The " + whichDate + " has changed from " + dateUtil.toUserDate(dates.currentDate) + " to " + dateUtil.toUserDate(dates.changedDate) + " all dates have changed by " + dates.diffDays + " working days. Please select an option below:";
      }
      $mdDialog.show(
        $mdDialog.confirm()
        .title("Add Revision?")
        .textContent(text)
        .ok("Publish a new revision")
        .cancel("Cancel")
        )
        .then(updateProjectAndPublishRevision, function () {
          setToPreviousDate(dates.currentDate);
        });
    }
    
    function setToPreviousDate(currentDate) {
      if (!vm.siteStarted) {
        vm.project.siteStart = new Date(currentDate);
      } else {
        vm.project.nextDeliveryDate = new Date(currentDate);
      }
    }

    function showError(error) {
      $mdDialog.show(
        $mdDialog.alert()
          .clickOutsideToClose(true)
          .title("There was a problem")
          .textContent(error)
          .ok("Ok")
      );
    }   
    
    
    
  }
})();