(function () {
  "use strict";

  angular
    .module("innView.projects")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider.when("/projects/create", {
      templateUrl: require("./projectCreate.html"),
      controller: "ProjectDetailsController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
    $routeProvider.when("/projects/:projectId/details", {
      templateUrl: require("./projectDetails.html"),
      controller: "ProjectDetailsController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
    $routeProvider.when("/projects/:projectId", {
      redirectTo: function (routeParams, path, search) {
        return "/projects/" + routeParams.projectId + "/details";
      }
    });
    $routeProvider.when("/projects/:projectId/scope", {
      templateUrl: require("./projectScope.html"),
      controller: "ProjectScopeController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
    $routeProvider.when("/projects/:projectId/schedule", {
      templateUrl: require("./projectDeliverySchedule.html"),
      controller: "ProjectDeliveryScheduleController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
    $routeProvider.when("/projects/:projectId/revisions", {
      templateUrl: require("./projectRevisions.html"),
      controller: "ProjectRevisionsController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
