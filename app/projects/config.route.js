(function () {
  "use strict";

  angular
    .module("innView.projects")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider.when("/projects", {
      templateUrl: require("./projects.html"),
      controller: "ProjectsController",
      controllerAs: "vm",
      resolve: {
        auth: requireAuth
      }
    });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
