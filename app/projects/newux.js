(function () {
  "use strict";

  require("angular-route");
  require("angular-material");
  require("angular-file-saver");

  require("./projects-newux.module");
  require("./project-details.controller");
  require("./project-scope.controller");
  require("./project-deliverySchedule.controller");
  require("./project-revisions.controller");
  require("./projects.service");
  require("./areas.service");
  require("./panels.service");
  require("./revisions.service");
  require("./config-newux.route");
  require("./key-length.filter");
  require("./object-key.filter");
  require("./project.filter");
  require("./project-name.filter");
  require("./name-initials.filter");
  require("./numbers-from-string.filter");
  require("./components.service");
})();
