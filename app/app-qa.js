(function () {
  "use strict";

  require("./stylesheets/override.scss");

  require("angular");
  require("angular-route");
  require("angular-animate");
  require("angular-touch");
  require("angular-messages");
  require("angular-ui-bootstrap");
  require("angular-ui-grid/ui-grid");
  require("angular-confirm");

  require("./module-qa");

  require("./core");
  require("./login");
  require("./projects");
  require("./manage");
  require("./qa");

})();
