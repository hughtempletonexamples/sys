(function () {
  "use strict";

  angular
    .module("innView.manage")
    .factory("loadPanelsService", loadPanelsService);

  /**
   * @ngInject
   */
  function loadPanelsService($log, $q,$filter, FileReader, XLSX, schema, referenceDataService, areasService, panelsService, componentsService, functionsService) {
    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
    
    var framingStyles = schema.getArray(schema.getRoot().child("config/framingStyles"));
    var projects = schema.getArray(schema.getRoot().child("projects"));
    var phases = referenceDataService.getPhases();
    var floors = referenceDataService.getFloors();
    var panelTypes = referenceDataService.getPanelTypes();
    var productTypes = referenceDataService.getProductTypes();
    var productTypeProductionLine;
    var areasCache = {};
    var componentsCache = {};
    var componentsHolder = {};
    var areasHolder = {};
    
    var updatePanelsSheet = false;
    var fileType = null;

    var service = {
      loadPanelSchedule: loadPanelSchedule,
      incrimentDesignCount:incrimentDesignCount
    };

    return service;

    ////////////


    function incrimentDesignCount(result){
      
      var panels = result;
      var date = $filter("date")(new Date(), "yyyy-MM-dd");
      var promises = [];

      angular.forEach(panels,function (panel,key){
        promises.push(incriment(panel,date));
      });
      return $q.all(promises).then(function (data){
        return data;
      });
    }
     
    function incriment(panel, date) {
      
      return referenceDataService.getProductTypes().$loaded()
              .then(function (productTypestt) {
               
                var productionLines = buildMap(productTypestt, "productionLine");
                var countRef = schema.getRoot().child("stats").child(productionLines[panel.type]).child(date).child("designedCount");
                var m2Ref = schema.getRoot().child("stats").child(productionLines[panel.type]).child(date).child("designedM2");

                var m2 = Math.round(panel.dimensions.area * 10) / 10;
                return countRef.transaction(function (current) {
                  return (current || 0) + 1;
                }).then(function () {
                  return m2Ref.transaction(function (current) {
                    return (current || 0) + m2;
                  });
                });
              });


    }
       
       
    function loadPanelSchedule(file) {

      return loadFile(file)
        .then(parseWorkbook)
        .then(loadFromWorkbook)
        .finally(clearCache);
        

      
      function loadFile(file) {

        var deferred = $q.defer();
        
        fileType = file.type.toString();

        var reader = new FileReader();
        reader.onload = function (e) {
          deferred.resolve(decode(e.target.result));
        };
        if (angular.isDefined(reader.readAsBinaryString)) {
          reader.readAsBinaryString(file);
        }
        else {
          reader.readAsArrayBuffer(file);
        }

        return deferred.promise;

        function decode(result) {
          if (angular.isString(result)) {
            return result;
          }

          var data = "";
          var chunkSize = 10240;
          for (var offset = 0; offset < result.byteLength; offset += chunkSize) {
            data += String.fromCharCode.apply(null, new Uint8Array(result.slice(offset, offset+chunkSize)));
          }
          return data;
        }
      }

      function parseWorkbook(data) {
        try {
          
          var workbook;
          
          if (fileType !== "text/csv"){
            workbook = XLSX.read(data, {type: "binary"});
          }else{
            workbook = data;
          }
          
          return $q.resolve(workbook);
        }
        catch (e) {
          return $q.reject(e.message);
        }
      }

      function loadFromWorkbook(workbook) {
        var response = {
          panels: [],
          errors: []
        };

        updatePanelsSheet = false;

        if (fileType == "text/csv") {

          var allRows = workbook.split(/\r?\n|\r/);
          
          for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
            var rowCells = allRows[singleRow].split(",");
            if (rowCells[1] === "Jobsheet No.") {
              updatePanelsSheet = true;
              break;
            }
          }

        } else {

          angular.forEach(workbook.Sheets, function (worksheet) {
            var range = XLSX.utils.decode_range(worksheet["!ref"]);
            var row = -1;

            while (++row <= range.e.r) {
              if (cellValue(worksheet, 1, row) === "Jobsheet No.") {
                updatePanelsSheet = true;
                break;
              }
            }

          });

        }

        if (!updatePanelsSheet) {
          return loadFromWorksheetsPanels(response, workbook, fileType)
            .then(function () {
              response.components = componentsHolder;
              response.areas = areasHolder;
              return response;
            });
        } else {
          return loadFromWorksheetsPanelUpdate(response, workbook, fileType)
            .then(function () {
              return response;
            });
        }

      }

      function clearCache(result) {
        componentsCache = {};
        areasCache = {};
        componentsHolder = {};
        areasHolder = {};
        return result;
      }
    }

    function loadFromWorksheetsPanels(response, workbook, fileType) {
      var deferredList = [];
      var content = [];

      if (fileType == "text/csv") {

        var row = -1;
        var jobSheet = null;

        var allRows = workbook.split(/\r?\n|\r/);

        for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[0] === "Jobsheet No.") {
            jobSheet = rowCells[1];
            break;
          }
        }

        for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[0] != "") {
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
              var value = rowCells[rowCell];
              if (rowCells[0] === "Panel Ref") {
                row = singleRow + 1;
                content.push(value);
              }
            }
          }
        }

        for (var singleRow = row; singleRow < allRows.length; singleRow++) {
          
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[0] != "") {
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
              var value = rowCells[rowCell];
              if (value) {
                switch (content[rowCell]) {
                case "Panel Ref":
                  var id = value;
                  break;
                case "Type":
                  var type = value;
                  break;
                case "Length":
                  var length = value;
                  break;
                case "Height":
                  var height = value;
                  break;
                case "Depth":
                  var width = value;
                  break;
                case "Area":
                  var area = value;
                  break;
                case "Weight":
                  var weight = value;
                  break;
                case "Framing Style":
                  var framingStyle = value;
                  break;
                case "Stud Size":
                  var studSize = value;
                  break;
                case "Sheathing":
                  var sheathing = value;
                  break;
                case "Components":
                  var components = value;
                  break;
                case "Nailing":
                  var nailing = value;
                  break;
                case "Spandrel":
                  var spandrel = value;
                  break;
                case "Doors":
                  var doors = value;
                  break;
                case "Windows":
                  var windows = value;
                  break;
                case "Pockets":
                  var pockets = value;
                  break;
                case "Qty":
                  var qty = value;
                  break;
                }
              }
            }
            if (id) {
              deferredList.push(
                validatePanel(id, type, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty)
                .then(function (panel) {
                  response.panels.push(panel);
                })
                .catch(function (error) {
                  response.errors.push(error);
                })
                );
            }
          }
        }


      } else {

        angular.forEach(workbook.Sheets, function (worksheet) {
          var range = XLSX.utils.decode_range(worksheet["!ref"]);

          var columnMap = {};
          var row = -1;
          var jobSheet = null;

          while (++row <= range.e.r) {
            if (cellValue(worksheet, 0, row) === "Job Sheet No") {
              jobSheet = cellValue(worksheet, 1, row);
              break;
            }
          }
          
          row = -1;

          while (++row <= range.e.r) {
            if (cellValue(worksheet, 0, row) === "Panel Ref") {
              for (var col = 1; col <= range.e.c; col++) {
                var heading = cellValue(worksheet, col, row);
                if (heading) {
                  columnMap[heading] = col;
                }
              }
              break;
            }
          }

          while (++row <= range.e.r) {
            var id = cellValue(worksheet, 0, row);
            if (id) {
              var type = cellValue(worksheet, columnMap["Type"], row);
              var length = cellValue(worksheet, columnMap["Length"], row);
              var height = cellValue(worksheet, columnMap["Height"], row);
              // INN-88 rationalised panel terms. Depth became width,
              // but the imported spreadsheet has a Depth column...
              var width = cellValue(worksheet, columnMap["Depth"], row);
              var area = cellValue(worksheet, columnMap["Area"], row);
              var weight = cellValue(worksheet, columnMap["Weight"], row);
              
              var framingStyle = cellValue(worksheet, columnMap["Framing Style"], row);
              var studSize = cellValue(worksheet, columnMap["Stud Size"], row);
              var sheathing = cellValue(worksheet, columnMap["Sheathing"], row);
              var components = cellValue(worksheet, columnMap["Components"], row);
              var nailing = cellValue(worksheet, columnMap["Nailing"], row);
              var spandrel = cellValue(worksheet, columnMap["Spandrel"], row);
              var doors = cellValue(worksheet, columnMap["Doors"], row);
              var windows = cellValue(worksheet, columnMap["Windows"], row);
              var pockets = cellValue(worksheet, columnMap["Pockets"], row);
              var qty = cellValue(worksheet, columnMap["Qty"], row);

              deferredList.push(
                validatePanel(id, type, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty)
                .then(function (panel) {
                  response.panels.push(panel);
                })
                .catch(function (error) {
                  response.errors.push(error);
                })
                );
            }
          }

        });

      }

      return $q.all(deferredList);
    }
    
    function loadFromWorksheetsPanelUpdate(response, workbook, fileType) {
      var deferredList = [];

      if (fileType == "text/csv") {

        var allRows = workbook.split(/\r?\n|\r/);
        var content = [];
        var row = -1;
        
        for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[0] != "") {
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
              var value = rowCells[rowCell];
              if (rowCells[0] === "Panel Ref") {
                row = singleRow + 1;
                content.push(value);
              }
            }
          }
        }
        
        for (var singleRow = row; singleRow < allRows.length; singleRow++) {
          var rowCells = allRows[singleRow].split(",");
          if (rowCells[0] !== "") {
            for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
              var value = rowCells[rowCell];
              if (value) {
                switch (content[rowCell]) {
                case "Panel Ref":
                  var id = value;
                  break;
                case "Jobsheet No.":
                  var jobSheet = value;
                  break;
                }
              }
            }
            if (id) {
              var jobSheetParts = jobSheet.split("-");
              var jobSheetString = jobSheetParts[5];
              var jobSheetNumber = parseInt(jobSheetString);
              deferredList.push(
                panelExists(id, jobSheetNumber)
                .then(getProductTypeName)
                .then(function (panel) {
                  return validatePanel(panel.id, panel.productType, panel.length, panel.height, panel.width, panel.area, panel.weight, panel.jobSheetNumber, panel.framingStyle, panel.studSize, panel.sheathing, panel.components, panel.nailing, panel.spandrel, panel.doors, panel.windows, panel.pockets, panel.qty);
                })
                .then(function (panel) {
                  response.panels.push(panel);
                })
                .catch(function (error) {
                  response.errors.push(error);
                })
                );
            }
          }
        }
        

      } else {

        angular.forEach(workbook.Sheets, function (worksheet) {
          var range = XLSX.utils.decode_range(worksheet["!ref"]);

          var columnMap = {};

          var row = -1;

          while (++row <= range.e.r) {
            if (cellValue(worksheet, 0, row) === "Panel Ref") {
              for (var col = 1; col <= range.e.c; col++) {
                var heading = cellValue(worksheet, col, row);
                if (heading) {
                  columnMap[heading] = col;
                }
              }
              break;
            }
          }

          while (++row <= range.e.r) {
            var id = cellValue(worksheet, 0, row);
            if (id) {
              var jobSheet = cellValue(worksheet, columnMap["Jobsheet No."], row);
              var jobSheetParts = jobSheet.split("-");
              var jobSheetString = jobSheetParts[5];
              var jobSheetNumber = parseInt(jobSheetString);
              deferredList.push(
                panelExists(id, jobSheetNumber)
                .then(getProductTypeName)
                .then(function (panel) {
                  return validatePanel(panel.id, panel.productType, panel.length, panel.height, panel.width, panel.area, panel.weight, panel.jobSheetNumber, panel.framingStyle, panel.studSize, panel.sheathing, panel.components, panel.nailing, panel.spandrel, panel.doors, panel.windows, panel.pockets, panel.qty);
                })
                .then(function (panel) {
                  response.panels.push(panel);
                })
                .catch(function (error) {
                  response.errors.push(error);
                })
                );
            }
          }

        });

      }

      return $q.all(deferredList);
    }
    
    function panelExists(id, jobSheetNumber) {
      var panel = {id: id};
      var deferred = $q.defer();

      panelsService.getPanelById(panel.id)
        .$loaded()
        .then(function (matchPanel) {
          if (matchPanel.length > 0) {
            panel.productType = matchPanel[0].type;
            panel.length = matchPanel[0].dimensions.length;
            panel.height = matchPanel[0].dimensions.height;
            panel.width = matchPanel[0].dimensions.width;
            panel.area = matchPanel[0].dimensions.area;
            panel.weight = matchPanel[0].dimensions.weight;
            panel.framingStyle = matchPanel[0].additionalInfo.framingStyle;
            panel.studSize = matchPanel[0].additionalInfo.studSize;
            panel.sheathing = matchPanel[0].additionalInfo.sheathing;
            panel.components = matchPanel[0].additionalInfo.components;
            panel.nailing = matchPanel[0].additionalInfo.nailing;
            panel.spandrel = matchPanel[0].additionalInfo.spandrel;
            panel.doors = matchPanel[0].additionalInfo.doors;
            panel.windows = matchPanel[0].additionalInfo.windows;
            panel.pockets = matchPanel[0].additionalInfo.pockets;
            panel.qty = matchPanel[0].additionalInfo.qty;
            panel.jobSheetNumber = jobSheetNumber;
            deferred.resolve(panel);
          } else {
            deferred.reject("Unknown panel '" + panel.id + "'");
          }
        })
        .catch(function (error) {
          deferred.reject(id + " " + error);
        });

      return deferred.promise;
    }

    function getProductTypeName(panel) {
      var deferred = $q.defer();
      productTypes.$loaded()
        .then(function () {
          var found;
          angular.forEach(productTypes, function (object) {
            if (panel.productType === object.$id) {
              found = object;
            }
          });
          if (found) {
            panel.productType = found.name;
            deferred.resolve(panel);
          } else {
            deferred.reject("Unknown type '" + panel.type + "'");
          }
        });

      return deferred.promise;
    }

    function cellValue(worksheet, column, row) {
      var cell_address = XLSX.utils.encode_cell({c: column, r: row});

      var cell = worksheet[cell_address];

      return cell ? cell.v : undefined;
    }
    
    function validatePanel(id, productType, length, height, width, area, weight, jobSheet, framingStyle, studSize, sheathing, components, nailing, spandrel, doors, windows, pockets, qty) {
      
      // Temporary values during processing
      var projectId;
      var phase;
      var floor;
      var panelType;
      var panel = {id: id};
      var areaByRef = {};
      var areas = [];
      var deferred = $q.defer();

      validatePanelReference()
        .then(resolveProject)
        .then(validatePhase)
        .then(validateFloor)
        .then(validatePanelType)
        .then(validateProductType)
        .then(validateJobSheet)
        .then(resolveAreas)
        .then(resolveComponent)
        .then(resolveDimensions)
        .then(resolveAdditionalInfo)
        .then(configFramingStyles)
        .then(function () {
          deferred.resolve(panel);
        })
        .catch(function (error) {
          deferred.reject(id + " " + error);
        });

      return deferred.promise;

      function validatePanelReference() {
        var idParts = PANEL_ID_REGEX.exec(panel.id);
        if (idParts) {
          projectId = idParts[1];
          phase = idParts[3];
          floor = idParts[4];
          panelType = idParts[5];
          
          return $q.resolve();
        }
        else {
          return $q.reject("Invalid panel reference");
        }
      }

      function resolveDimensions() {
        var dimensions = {
          length: parseInt(length) || null,
          height: parseInt(height) || null,
          width: parseInt(width) || null,
          area: parseFloat(area) || null,
          weight: parseInt(weight) || null
        };
        if (!dimensions.length || !dimensions.height || !dimensions.width || !dimensions.area || !dimensions.weight) {
          return $q.reject("Missing dimension information");
        }
        else {
          panel.dimensions = dimensions;
          return $q.when();
        }
      }
      
      function resolveAdditionalInfo() {
        var additionalInfo = {
          framingStyle: String(framingStyle) || null,
          studSize: String(studSize) || null,
          sheathing: String(sheathing) || null,
          components: parseInt(components) || null,
          nailing: String(nailing) || null,
          spandrel: String(spandrel) || null,
          doors: parseInt(doors) || null,
          windows: parseInt(windows) || null,
          pockets: parseInt(pockets) || null,
          qty: parseInt(qty) || null
        };
        if (!additionalInfo.framingStyle, !additionalInfo.studSize, !additionalInfo.sheathing, !additionalInfo.components, !additionalInfo.nailing, !additionalInfo.spandrel, !additionalInfo.doors, !additionalInfo.windows, !additionalInfo.pockets, !additionalInfo.qty) {
          return $q.reject("Missing additional information");
        }
        else {
          panel.additionalInfo = additionalInfo;
          return $q.when();
        }
      }

      function resolveProject() {
        var deferred = $q.defer();

        projects.$loaded()
          .then(function () {
            var found;
            angular.forEach(projects, function (project) {
              if (projectId === project.id) {
                found = project;
              }
            });
            if (found) {
              panel.project = found.$id;
              deferred.resolve();
            }
            else {
              deferred.reject("Unknown project '" + projectId + "'");
            }
          });

        return deferred.promise;
      }

      function validatePhase() {
        return validateReferenceDataKey(phases, phase, "phase");
      }

      function validateFloor() {
        return validateReferenceDataKey(floors, floor, "floor");
      }

      function validatePanelType() {
        return validateReferenceDataKey(panelTypes, panelType, "panel type");
      }

      function validateReferenceDataKey(referenceData, key, description) {
        var deferred = $q.defer();

        referenceData.$loaded()
          .then(function () {
            if (referenceData.$indexFor(key) >= 0) {
              deferred.resolve();
            }
            else {
              deferred.reject("Unknown " + description + " '" + key + "'");
            }
          });

        return deferred.promise;
      }

      function validateProductType() {
        var deferred = $q.defer();

        productTypes.$loaded()
          .then(function () {
            var found;
            if(angular.isUndefined(productTypeProductionLine)){
              productTypeProductionLine = buildMap(productTypes, "productionLine");
            }
            angular.forEach(productTypes, function (object) {
              var find = productType.search(object.name);
              if (find >= 0) {
                found = object;
              }
            });
            if (found) {
              panel.type = functionsService.uploaderLogicHSIP(found.$id, panelType, height, length);

              deferred.resolve();
            }
            else {
              deferred.reject("Unknown type '" + productType + "'");
            }
          });

        return deferred.promise;
      }
      
      function configFramingStyles() {
        var deferred = $q.defer();
        
        var productionLine;
        var configFramingStyles = {};
        
        framingStyles.$loaded()
          .then(function () {
            productionLine = productTypeProductionLine[panel.type];
            if(angular.isUndefined(configFramingStyles[panel.additionalInfo.framingStyle])){
              configFramingStyles[panel.additionalInfo.framingStyle] = true;
            }
            if(angular.isDefined(framingStyles[productionLine]) && angular.isDefined(framingStyles[productionLine][panel.additionalInfo.framingStyle])){
              deferred.resolve();
            }else{
              deferred.resolve(schema.getRoot().child("config/framingStyles/" + productionLine).update(configFramingStyles));
            }
          });
        return deferred.promise;
      }
      
      function validateJobSheet() {
        if (jobSheet === null) {
          return $q.resolve();
        } else {
          if (angular.isNumber(jobSheet)) {
            panel.jobSheet = jobSheet;
            return $q.resolve();
          } else {
            return $q.reject("Job Sheet No is not a number '" + jobSheet + "'");
          }
        }
      }
      
      function resolveAreas() {
        var deferred = $q.defer();
        var projectAreas = getCachedProjectAreas(panel.project);

        projectAreas.$loaded()
          .then(function () {
            angular.forEach(projectAreas, function (object) {
              if(angular.isUndefined(areaByRef[object.$id])){
                areaByRef[object.$id] = object;
              }
              if(angular.isUndefined(areasHolder[object.$id])){
                areasHolder[object.$id] = object;
              }

              if (phase === object.phase
                && floor === object.floor
                && panel.type === object.type) {
                areas.push(object.$id);
              }
            });
            if (areas.length) {
              if (areas.length === 1) {
                panel.area = areas[0];
              }
              deferred.resolve();
            }
            else {
              deferred.reject("Unknown area '" + phase + "-" + floor + "-" + panel.type + "'");
            }
          });

        return deferred.promise;
      }
      
      function resolveComponent() {
        var deferred = $q.defer();
        var projectComponents = getCachedProjectComponent(panel.project);
        var found = 0;
        if (!updatePanelsSheet) {
          projectComponents.$loaded()
            .then(function () {
              angular.forEach(projectComponents, function (object) {
                angular.forEach(object.areas, function (area, areaKey) {
                  found++;
                  if (areas.length > 1) {
                    for (var i = 0; i < areas.length; i++) {
                      var area = areas[i];
                      if (area === areaKey) {
                        var areaObj = areaByRef[areaKey];
                        var nameHolder = projectId + "-" + areaObj.phase + "-" + areaObj.floor + "-" + areaObj.type;
                        if (angular.isUndefined(componentsHolder[nameHolder])) {
                          componentsHolder[nameHolder] = {
                            "selected": false,
                            "type": {}
                          };
                        }
                        if (angular.isUndefined(componentsHolder[nameHolder]["type"][object.type])) {
                          componentsHolder[nameHolder]["type"][object.type] = {};
                        }
                        if (angular.isUndefined(componentsHolder[nameHolder]["type"][object.type][areaKey])) {
                          componentsHolder[nameHolder]["type"][object.type][areaKey] = {
                            areaObj: {}
                          };
                        }
                        componentsHolder[nameHolder]["type"][object.type][areaKey].areaObj = areaObj;
                      }
                    }
                  }
                });
              });
              areas = [];
              deferred.resolve();
            });
        } else {
          deferred.resolve();
        }

        return deferred.promise;
      }

      function getCachedProjectAreas(project) {
        if (!areasCache[project]) {
          areasCache[project] = areasService.getProjectAreas(project);
        }
        return areasCache[project];
      }
      
      function getCachedProjectComponent(project) {
        if (!componentsCache[project]) {
          componentsCache[project] = componentsService.getComponentsForProject(project);
        }
        return componentsCache[project];
      }
    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }
    
  }

})();
