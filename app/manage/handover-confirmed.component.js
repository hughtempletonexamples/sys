(function () {
  "use strict";

  angular
    .module("innView.manage")
    .component("handoverConfirmed", {
      templateUrl: function () {
        return require("./areaHandoverConfirmed.html");
      },
      controller: handoverConfirmedController,
      controllerAs: "vm",
      bindings: {
        area: "="
      }
    });

  /**
   * @ngInject
   */
  function handoverConfirmedController($log, $timeout, schema, plannerService) {
    var vm = this;
    
    vm.isChecked = false;
    vm.timeout;
    
    vm.ref = schema.getRoot().child("additionalData/areas").child(vm.area);
    
    vm.ref.on("value", getData);
    
    function getData(snapshot){
      var val = snapshot.val();
      if(val !== null){
        if(angular.isDefined(val.handoverConfirmed) && angular.isDefined(val.handoverConfirmed.status)){
          vm.timeout = $timeout(function (){ 
            vm.isChecked = val.handoverConfirmed.status;
          },5);
        }
      }
    }
    
    vm.$onDestroy = function () {
      vm.ref.off("value", getData);
      if(angular.isDefined(vm.timeout)){
        $timeout.cancel(vm.timeout);
      }
    };

  }

})();
