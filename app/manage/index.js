(function () {
  "use strict";

  require("angular-route");
  require("ng-file-upload");

  require("xlsx/dist/jszip");
  require("xlsx/dist/xlsx.min");

  require("../floatthead");

  require("./manage.module");
  require("./constants");
  require("./config.route");

  require("./planner.controller");
  require("./planner.service");

  require("./status.controller");
  require("./status.service");

  require("./load-panels.controller");
  require("./load-panels.service");

  require("./capacity-input.component");
  require("./handover-confirmed.component");
  require("./panel-images.component");
})();
