(function () {
  "use strict";

  angular
    .module("innView.manage")
    .component("innPanelImages", {
      templateUrl: require("./panelImages.html"),
      controller: ("panelImageController",panelImageController),
      controllerAs: "vm",
      bindings: {
        panel:"<"
      }
    });

  /**
   * @ngInject
   */
  function panelImageController(statusService,$uibModal) {
    var vm = this;
    vm.openPopup = openPopup;
    getPanelImage(vm.panel);
    
    
    
    


    
    function getPanelImage(panel) {

      if (angular.isDefined(panel)) {
        return statusService.getPanelImage(panel.$id)
                .then(function (data) {
                  if (data === false) {
                    vm.imageUrl = false;
                    vm.exists = false;
                    return false;
                  } else {
                    vm.imageUrl = data;
                    if (vm.imageUrl.length >= 1) {
                      vm.exists = true;
                    }
                    return true;
                  }

                });
      }

    }
    
    function openPopup(ev) {


      vm.images = vm.imageUrl;
      var modalInstance = $uibModal.open({
        templateUrl: "imgModal.html",
        controller: ["$uibModalInstance",  "images", imageModalController],
        controllerAs: "vm",
        size: "500px",
        resolve: {
          images: function () {
            return vm.images;
          }
        }
      });
       
      function imageModalController( $uibModalInstance, images ) {
        var vm = this;

        vm.images = images;
      
      }
    } 
     
  }

})();


