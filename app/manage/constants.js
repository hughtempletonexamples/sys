(function () {
  "use strict";

  angular
    .module("innView.manage")
    .constant("FileReader", FileReader)
    .constant("XLSX", XLSX);

})();