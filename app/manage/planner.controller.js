(function () {
  "use strict";

  angular
    .module("innView.manage")
    .controller("ForwardPlannerController", ForwardPlannerController);

  /**
   * @ngInject
   */
  function ForwardPlannerController($log, $scope, $location, $routeParams, dateUtil, schema, plannerService, areasService) {
    var vm = this;
    
    var plannerSort = {};
    
    vm.productionLineFilter = { SIP: true, SIP2: true, HSIP: true, IFAST: true, TF: true, TF2: true, CASS: true };
    vm.showActualPanels = false;
    vm.showM2 = false;
    
    vm.loadPlan = loadPlan;
    vm.updateCapacity = updateCapacity;
    vm.toggleActual = toggleActual;
    
    vm.dayHeadings = getDayHeadings();

    vm.weekStart = getWeekStart;
    vm.weekFinish = getWeekFinish;

    vm.panelsCount = getPanelsCount;
    vm.completedPanelsCount = getCompletedPanelsCount;
    vm.completedPanelsPercent = getCompletedPanelsPercent;
    vm.atRisk = atRisk;
    vm.riskDays = getRiskDays;
    vm.reloadPlan = reloadPlan;
    vm.useUnpublished = useUnpublished;
    vm.useUnpublishedOnly = useUnpublishedOnly;
    vm.updateFilter = updateFilter;
    vm.userChange = userChange;
    vm.areasSelection = {};
    vm.updateAreaLine = updateAreaLine;
    vm.fieldFilters = [];
    vm.makeUpdates = makeUpdates;

    vm.floatTheadOptions = floatTheadOptions();

    loadPlan($routeParams.start || null);

    $scope.$on("$destroy", destroyPlan);

    ////////////
    function makeUpdates(){
      vm.loadPlan(vm.plan.viewStart); 
    }

    function updateAreaLine(areaKey, value) {
      
      if (angular.isDefined(areaKey)) {
        return areasService.getProjectArea(areaKey)
          .$loaded()
          .then(function (area) {
            area.line = value;
            return areasService.updateProjectArea(area);
          });
      }
    }

    function loadPlan(date) {

      var startDate = date != null ? dateUtil.weekStart(date) : null;
      destroyPlan();
      vm.plan = plannerService.getForwardPlan(startDate, vm.areasSelection);
      plannerSort = schema.getRoot().child("config/plannerSort");
      
      plannerSort.once("value", function (snapshot) {
        $log.log(vm.plan);
        var returnPlannerSort = snapshot.val();
        var arr = getSortData(returnPlannerSort);
        if(angular.isDefined(arr) && arr.length > 0){
          vm.fieldFilters = getSortData(returnPlannerSort);
        } 
      });
      $location.search("start", startDate);
    }
    
    function getSortData(plannerSort){
      if (angular.isDefined(plannerSort)) {
        var fieldFilters = [];
        angular.forEach(plannerSort, function ( order, dataType ) {
          fieldFilters[order] = dataType;
        });
        vm.fieldFilters = fieldFilters;
      }
    }

    function destroyPlan() {
      if (vm.plan && vm.plan.$destroy) {
        vm.plan.$destroy();
        delete vm.plan;
      }
    }

    function reloadPlan() {
      var startDate = $routeParams.start != null ? dateUtil.weekStart($routeParams.start) : null;
      destroyPlan();
      vm.plan = plannerService.getForwardPlan(startDate, vm.areasSelection);
      plannerSort = schema.getRoot().child("config/plannerSort");
      plannerSort.once("value", function (snapshot) {
        var returnPlannerSort = snapshot.val();
        var arr = getSortData(returnPlannerSort);
        if(angular.isDefined(arr) && arr.length > 0){
          vm.fieldFilters = getSortData(returnPlannerSort);
        } 
      });
    }

    function updateCapacity(productionLine, index, value) {
      var date = dateUtil.plusDays(vm.plan.viewStart, index);
      plannerService.setCapacity(productionLine, date, value);
    }
    
    function userChange(area, plannedStart, change) {
      plannerService.setPlannedStart(area, plannedStart, change)
        .then(plannerService.refreshProductionPlan)
        .then(reloadPlan);
    }
    
    function toggleActual() {
      $log.log(angular.toJson(vm.plan));
      plannerService.filterActualPanels(vm.showActualPanels);
    }
    
    function getDayHeadings() {
      var week = ["M", "T", "W", "T", "F", "S", "S" ];
      return week.concat(week, week);
    }

    function getWeekStart(n) {
      return dateUtil.plusDays(vm.plan.viewStart, 7 * n);
    }

    function getWeekFinish(n) {
      return dateUtil.plusDays(vm.plan.viewStart, 7 * n + 6);
    }

    function getPanelsCount( area ) {
      var stats = {};
      if (angular.isDefined(area.actualPanels) && angular.isDefined(area.numberOfPanels) && area.actualPanels > 0) {
        if (area.actualPanels !== area.numberOfPanels) {
          stats.type = "noMatch";
          stats.count = area.actualPanels;
          stats.display = area.numberOfPanels + " / " + area.actualPanels;
        } else {
          stats.type = "actual";
          stats.count = area.actualPanels;
          stats.display = area.actualPanels;
        }
      } else {
        if (angular.isDefined(area.actualPanels) && area.actualPanels > 0) {
          stats.type = "actual";
          stats.count = area.actualPanels;
          stats.display = area.actualPanels;
        } else if (angular.isDefined(area.numberOfPanels)) {
          stats.type = "userGuess";
          stats.count = area.numberOfPanels;
          stats.display = area.numberOfPanels;
        } else if (angular.isDefined(area.estimatedPanels)) {
          stats.type = "notfinal";
          stats.count = area.estimatedPanels;
          stats.display = area.estimatedPanels;
        } else {
          stats.type = "noData";
          stats.count = 0;
          stats.display = 0;
        }
      }
      return stats;
    }

    function getCompletedPanelsCount(area) {
      return area.completedPanels || 0;
    }

    function getCompletedPanelsPercent(area) {
      var count = getPanelsCount(area).count;
      return count == 0 ? 0 : Math.floor(100 * getCompletedPanelsCount(area) / count);
    }

    function atRisk(area) {
      var riskDays = getRiskDays(area);
      return angular.isUndefined(riskDays) || riskDays < 0;
    }
    
    function removeDefaultSorts(plannerSort) {
      var sortId = ["_deliveryDate", "_projId", "phase", "floor", "timestamps.modified"];
      angular.forEach(sortId, function ( dataType ) {
        var index = plannerSort.indexOf(dataType);
        if(index > -1){
          plannerSort.splice( index, 1 );
        }
      });
      return plannerSort;
    }
    
    function updateFilter(field){
      var arr = vm.fieldFilters;
      var exists = arr.indexOf(field);
      if (exists > -1){
        arr.splice(exists, 1);
      }else{
        arr.push(field);
      }
      plannerService.filterAreas(arr);
      vm.fieldFilters = arr;
      var newData = {};
      angular.forEach(arr, function (filt, key){
        if (angular.isUndefined(newData[filt])){
          newData[filt] = key;
        }
      });
      if(Object.keys(newData).length > 0){
        plannerSort.set(newData);
      }else{
        plannerSort.remove();
      }
      
    }

    function getRiskDays(area) {
      if (useUnpublishedOnly(area)) {
        return null;
      }

      if (!area._productionEndDate || !area._riskDate) {
        return undefined;
      }

      var riskDate = useUnpublished(area) ? area._unpublishedRiskDate : area._riskDate;

      return dateUtil.daysBetweenWeekDaysOnly(area._productionEndDate, riskDate);
    }

    function useUnpublishedOnly(area) {
      return area._hasUnpublishedOnly && !useUnpublished(area);
    }

    function useUnpublished(area) {
      return area._hasUnpublished && vm.areasSelection[area.$id];
    }

    function floatTheadOptions() {
      return {
        responsiveContainer: function ($table) {
          return $table.closest(".table-responsive");
        },
        zIndex: 999
      };
    }
  }
})();
