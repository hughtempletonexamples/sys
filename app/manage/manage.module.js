(function () {
  "use strict";

  angular
    .module("innView.manage", [
      "ngRoute",
      "firebase",
      "floatThead",
      "innView.core",
      "innView.projects",
      "ngFileUpload"
    ]);
})();
