(function () {
  "use strict";

  angular
    .module("innView.manage")
    .factory("statusService", statusService);

  var MILLIS_IN_WEEK = (1000*60*60*24*7);
  var LABEL_SEPARATOR = "-";

  /**
   * @ngInject
   */
  function statusService($log, dateUtil, firebaseUtil,$q,$filter, schema, settingsService, projectsService, areasService, panelsService, componentsService, referenceDataService, plannerService) {

    var service = {
      getProjectsStatus: getProjectsStatus,
      getStatusForProject: getStatusForProject,
      getAreaStatus: getAreaStatus,
      markPanelsComplete: markPanelsComplete,
      deletePanels: deletePanels,
      getPanelImage:getPanelImage,
      markPanelIncomplete: markPanelIncomplete,
      decrementDesignCount: decrementDesignCount
    };

    return service;



    function decrementDesignCount(result){
      
      var panels = result;
      var promises = [];
      angular.forEach(panels,function (panel,key){
        var date = $filter("date")(new Date(panel.timestamps.created), "yyyy-MM-dd");
        promises.push(decrement(panel,date));
      });

      return $q.all(promises).then(function (data) {
        return data;
      });
    }
     
    function decrement(panel, date) {
      
      return referenceDataService.getProductTypes().$loaded()
              .then(function (productTypestt) {
               
                var productionLines = buildMap(productTypestt, "productionLine");
                var countRef = schema.getRoot().child("stats").child(productionLines[panel.type]).child(date).child("designedCount");
                var m2Ref = schema.getRoot().child("stats").child(productionLines[panel.type]).child(date).child("designedM2");

                var m2 = Math.round(panel.dimensions.area * 10) / 10;
                
                return countRef.transaction(function (current) {
                  return (current || 0) - 1;
                }).then(function () {
                  return m2Ref.transaction(function (current) {
                    return (current || 0) - m2;
                  });
                });
              });
      function buildMap( items, property ) {
        var map = {};
        angular.forEach(items, function ( item ) {
          map[item.$id] = item[property];
        });
        return map;
      }

    }
    
    ////////////


    
    
    function getPanelImage(id){
      var images = schema.getObject(schema.getRoot().child("errors").orderByChild("panel").equalTo(id));
      var promises = [];
      return images.$loaded()
              .then(function (data) {
                
                angular.forEach(data,function (panelInfo,key){
                  
                  promises.push(getImageByPanelKey(panelInfo.panel,key));
                });

                return $q.all(promises);
              });

    }
    function getImageByPanelKey(panelId,key) {

      var image = schema.getStorageRoot().child("images").child("panels").child(panelId).child(key);

      return getDownloadUrl(image);

    }
    
    function getDownloadUrl(image){
      return image.getDownloadURL()
              .then(function (url) {
                return url;
              }).catch(function (error) {
                return false;
              });
    }    

    function getProjectsStatus(limitToFourWeeks) {
      var status = [];

      loadProjectsStatus(status, getFilter(limitToFourWeeks));

      return status;
    }

    function getStatusForProject(projectId, limitToFourWeeks) {
      var status = {};
      
      loadStatusForProject(projectId, status, getFilter(limitToFourWeeks));

      return status;
    }

    function getAreaStatus(projectId, areaRef) {
      var status = {};

      loadAreaStatus(status, projectId, areaRef);

      return status;
    }

    function markPanelsComplete(panels, panelSelections, completedDate) {
      var areaRef = panels[0].area;

      var panelRefs = [];
      angular.forEach(panels, function (panel) {
        if (panelSelections[panel.id]) {
          panelRefs.push(panel.$id);
        }
      });

      panelsService
        .markPanelsComplete(areaRef, panelRefs, completedDate)
    }
    
    
    function deletePanels(panels, panelSelections) {
      return panelsService.deletePanels(panels, panelSelections);
    }

    function markPanelIncomplete(panels, panelRef) {
      panelsService.markPanelIncomplete(panels, panelRef);
    }

    function getFilter(limitToFourWeeks) {
      if (limitToFourWeeks) {
        return dueInLessThanFourWeeks;
      } else {
        return alwaysAllow;
      }
    }

    function dueInLessThanFourWeeks(deliveryDate) {
      var now = new Date();
      var deliveryDateAsDate = new Date(deliveryDate);
      var difference = ((deliveryDateAsDate - now) / MILLIS_IN_WEEK);

      return difference < 4;
    }

    function alwaysAllow(deliveryDate) {
      return true;
    }

    function loadProjectsStatus(status, projectFilter) {
      var suppliers = referenceDataService.getSuppliers();
      var productTypes = referenceDataService.getProductTypes();
      var projects = schema.getArray(schema.getRoot().child("projects"));
      firebaseUtil.loaded(projects, productTypes, suppliers)
        .then(function () {

          load(projectFilter);
          projects.$watch(function () {
            status = [];
            load(projectFilter);
          });
        });

      function load(projectFilter) {
        populateStatus(status, projects, productTypes, suppliers, projectFilter);
      }
    }

    function parseAreaLabel(label) {
      var parts = label.split(LABEL_SEPARATOR);
      return {
        "phase": parts[0],
        "floor": parts[1],
        "type": parts[2]
      };
    }

    function loadAreaStatus(status, projectId, areaRef) {
      projectsService
        .getProject(projectId)
        .then(function (project) {
          status.project = project;
          areasService.getProjectArea(areaRef)
            .$loaded()
            .then(function (area) {
              status.area = area;
              return status.area;
            })
            .then(function (area) {
              loadPanelsForArea(area, status);
            });
        });

      function loadPanelsForArea(area, status) {
        panelsService
          .panelsForArea(area.$id)
          .$loaded()
          .then(function (panels) {
            status.panels = panels.sort(panelsService.comparator);
          });
      }
    }

    function loadStatusForProject(projectId, status, includeArea) {
      var project = projectsService.getProject(projectId);
      project
        .then(function (project) {
          
          var revisions = 0;

          if (angular.isDefined(project.deliverySchedule.revisions)) {
            revisions = Object.keys(project.deliverySchedule.revisions).length;
          }
          
          status.id = project.id;
          status.name = project.name;
          status.revision = revisions;
          status.areas = [];
          var areas = areasService.getProjectAreas(project.$id);
          var plans = plannerService.getProjectAreaProductionPlans(project.$id);
          var components = componentsService.getComponentsForProject(project.$id);
          var designHandoverPeriod;
          var designHandoverBuffer;

          settingsService.get()
            .then(function (settings) {
              designHandoverPeriod = settings.designHandoverPeriod;
              designHandoverBuffer = settings.designHandoverBuffer;
              return firebaseUtil.loaded(areas, plans, components);
            })
            .then(function () {
              angular.forEach(areas, function (area) {
                var deliveryDate = area.revisions[project.deliverySchedule.published];

                if (angular.isDefined(deliveryDate) && includeArea(deliveryDate)) {
                  var estimatedPanels = getOptionalNumber(area.estimatedPanels);
                  var estimatedArea = getOptionalNumber(area.estimatedArea);
                  var actualPanels = getOptionalNumber(area.actualPanels);
                  var actualArea = getOptionalNumber(area.actualArea);
                  var completedPanels = getOptionalNumber(area.completedPanels);
                  var completedArea = getOptionalNumber(area.completedArea);

                  var highRisk = isAtRisk(actualPanels, deliveryDate, designHandoverPeriod);
                  var mediumRisk = !highRisk && isAtRisk(actualPanels, deliveryDate, designHandoverPeriod + designHandoverBuffer);
                  
                  var component = getComponent(components, area.$id);

                  var areaStatus = {
                    areaRef: area.$id,
                    label: area.phase + LABEL_SEPARATOR + area.floor + LABEL_SEPARATOR + area.type,
                    component: component,
                    delivery: deliveryDate,
                    estimated: {
                      count: estimatedPanels,
                      area: round(estimatedArea)
                    },
                    actual: {
                      count: actualPanels,
                      countPercentage: calculatePercentage(actualPanels, estimatedPanels),
                      area: round(actualArea),
                      areaPercentage: calculatePercentage(actualArea, estimatedArea)
                    },
                    completed: {
                      count: completedPanels,
                      countPercentage: calculatePercentage(completedPanels, actualPanels),
                      area: round(completedArea),
                      areaPercentage: calculatePercentage(completedArea, actualArea)
                    },
                    risk: {
                      high: highRisk,
                      medium: mediumRisk
                    }
                  };
                  
                  var areaPlan = plans.$getRecord(area.$id);
                  if (areaPlan) {
                    var past = riskInPast(areaPlan.plannedFinish);
                    areaStatus.risk.past = past;
                    areaStatus.plan = {
                      plannedStart: areaPlan.plannedStart,
                      plannedFinish: areaPlan.plannedFinish,
                      riskDate: areaPlan.riskDate
                    };
                  }

                  status.areas.push(areaStatus);
                }
              });
              areas.$destroy();
              plans.$destroy();
              sortByDeliveryDate(status.areas);
            });
        });
    }
    
    function getComponent(components, areaKey){
      var componentType = "";
      angular.forEach(components, function (component) {
        angular.forEach(component.areas, function (area, key) {
          if(areaKey === key){
            componentType = component.type;
          }
        });
      });
      return componentType;
    }

    function populateStatus(status, projects, productTypes, suppliers, include) {
      
      var productTypeProductionLine = buildMap(productTypes, "productionLine");
      var thirdPartySuppliers = buildMap(suppliers, "thirdParty");

      angular.forEach(projects, function (project) {
        if (project.active && angular.isDefined(project.deliverySchedule) &&
            angular.isDefined(project.deliverySchedule.published)) {
          var allProjectRisks = {};
          var areas = areasService.getProjectAreas(project.$id);
          var plans = plannerService.getProjectAreaProductionPlans(project.$id);
          var designHandoverPeriod;
          var designHandoverBuffer;

          settingsService.get()
            .then(function (settings) {
              designHandoverPeriod = settings.designHandoverPeriod;
              designHandoverBuffer = settings.designHandoverBuffer;
              return firebaseUtil.loaded(areas, plans, productTypes);
            })
            .then(function () {
              var deliveryDate = "9999-99-99";
              var estimatedAreaTotal = 0;
              var estimatedCountTotal = 0;
              var actualAreaTotal = 0;
              var actualCountTotal = 0;
              var completedAreaTotal = 0;
              var completedCountTotal = 0;
              var mediumRisk = false;
              var highRisk = false;
              var past = false;
              var riskDays = undefined;
              var allAreaRisks = {};
              angular.forEach(areas, function (area) {
                deliveryDate = calculateDeliveryDate(deliveryDate, area, project);
                if (include(area.revisions[project.deliverySchedule.published])) {
                  estimatedAreaTotal += getOptionalNumber(area.estimatedArea);
                  estimatedCountTotal += getOptionalNumber(area.estimatedPanels);
                  actualAreaTotal += getOptionalNumber(area.actualArea);
                  var areaActualCount = getOptionalNumber(area.actualPanels);
                  actualCountTotal += areaActualCount;
                  completedAreaTotal += getOptionalNumber(area.completedArea);
                  completedCountTotal += getOptionalNumber(area.completedPanels);
                  highRisk = highRisk || isAtRisk(areaActualCount, deliveryDate, designHandoverPeriod);
                  mediumRisk = mediumRisk || isAtRisk(areaActualCount, deliveryDate, designHandoverPeriod + designHandoverBuffer);
                  past = riskInPast(deliveryDate);
                  var areaPlan = plans.$getRecord(area.$id);
                  if (areaPlan && areaPlan.plannedFinish && areaPlan.riskDate) {
                    var areaRiskDays = dateUtil.daysBetweenWeekDaysOnly(areaPlan.plannedFinish, areaPlan.riskDate);
                    if (!thirdPartySuppliers[area.supplier] && area.actualPanels && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
                      if(project.active){
                        var productionLine = productTypeProductionLine[area.type];
                        if (angular.isDefined(productionLine) && angular.isUndefined(allAreaRisks[productionLine])){
                          allAreaRisks[productionLine] = [];
                        }
                        allAreaRisks[productionLine].push(areaRiskDays);
                      }
                    }
                    riskDays = angular.isDefined(riskDays) ? Math.min(riskDays, areaRiskDays) : areaRiskDays;
                  }
                }
              });
              
              allProjectRisks = lowestRisk(allAreaRisks);
              
              var revisions = 0;
              
              if (angular.isDefined(project.deliverySchedule.revisions)){
                revisions = Object.keys(project.deliverySchedule.revisions).length;
              }

              if (include(deliveryDate)) {
                status.push({
                  projectId: project.id,
                  label: createProjectLabel(project),
                  delivery: deliveryDate,
                  revision: revisions,
                  estimated: {
                    area: round(estimatedAreaTotal),
                    count: estimatedCountTotal
                  },
                  actual: {
                    area: round(actualAreaTotal),
                    areaPercentage: calculatePercentage(actualAreaTotal, estimatedAreaTotal),
                    count: actualCountTotal,
                    countPercentage: calculatePercentage(actualCountTotal, estimatedCountTotal)
                  },
                  completed: {
                    area: round(completedAreaTotal),
                    areaPercentage: calculatePercentage(completedAreaTotal, actualAreaTotal),
                    count: completedCountTotal,
                    countPercentage: calculatePercentage(completedCountTotal, actualCountTotal)
                  },
                  risk: {
                    past: past,
                    days: riskDays,
                    medium: highRisk ? false : mediumRisk,
                    high: highRisk
                  },
                  allProjectRisks : allProjectRisks,
                });
              }
              areas.$destroy();
              plans.$destroy();

              sortByDeliveryDate(status);
            });
        }

      });
        
      function lowestRisk(entryOb) {
        var toFillOb = {};

        angular.forEach(entryOb, function (risks, productionLine) {
          var lowest = Number.POSITIVE_INFINITY;
          var tmp;
          for (var i = risks.length - 1; i >= 0; i--) {
            tmp = risks[i];
            if (tmp < lowest) {
              lowest = tmp;
            }
          }
          if (angular.isUndefined(toFillOb[productionLine])) {
            toFillOb[productionLine] = lowest;
          }
        });
        return toFillOb;
      }
      
      function buildMap(items, property) {
        var map = {};
        angular.forEach(items, function (item) {
          map[item.$id] = item[property];
        });
        return map;
      }

      function createProjectLabel(project) {
        return project.id + " - " + project.name;
      }

      function calculateDeliveryDate(earliestDeliveryDate, area, project) {
        var deliveryDate = area.revisions[project.deliverySchedule.published];
        if (deliveryDate < earliestDeliveryDate) {
          earliestDeliveryDate = deliveryDate;
        }
        return earliestDeliveryDate;
      }

    }
    
    function riskInPast(deliveryDate){
      var today = new Date();
      deliveryDate = new Date(deliveryDate);
      return today < deliveryDate;
    }

    function isAtRisk(panelsCount, deliveryDate, riskPeriod) {
      if (panelsCount > 0) {
        return false;
      }
      var today = new Date();
      var startOfRiskPeriod = new Date(dateUtil.subtractWorkingDays(new Date(deliveryDate), riskPeriod));
      return today >= startOfRiskPeriod;
    }

    function getOptionalNumber(value) {
      if (angular.isDefined(value)) {
        return parseFloat(value);
      }
      return 0;
    }

    function sortByDeliveryDate(items) {
      items.sort(function (a, b) {
        if (a.delivery < b.delivery) {
          return -1;
        } else if (a.delivery > b.delivery) {
          return 1;
        } else {
          return 0;
        }
      });
    }

    function calculatePercentage(a, b) {
      if (b === 0) {
        return 0;
      }
      return Math.round((a/b)*100);
    }

    function round(value) {
      return Math.round(value * 100) / 100;
    }
  }
})();
