(function () {
  "use strict";

  angular
    .module("innView.manage")
    .controller("ProjectStatusController", ProjectStatusController);

  /**
   * @ngInject
   */
  function ProjectStatusController($confirm,$document, $scope, $location, $routeParams, $confirmModalDefaults, statusService, dateUtil) {

    var vm = this;

    vm.selectProject = selectProject;
    vm.selectArea = selectArea;
    vm.markSelectedPanelsComplete = markSelectedPanelsComplete;
    vm.deleteSelectedPanels = deleteSelectedPanels;
    vm.toggleAllPanels = toggleAllPanels;
    vm.panelToggled = panelToggled;
    vm.panelCompleted = panelCompleted;
    vm.panelStatus = panelStatus;
    vm.markPanelIncomplete = markPanelIncomplete;
    vm.atRisk = atRisk;
    vm.riskDays = riskDays;
    vm.riskText = riskText;

    vm.limitView = false;
    vm.numberPanels = 0;
    vm.completed = {
      number: 0,
      percentage: 0
    };
    vm.panelsSelected = false;
    vm.panelsAllSelected = false;
    vm.completedDate = dateUtil.todayAsIso();

    init();

    ////////////

    function init() {
      $confirmModalDefaults.templateUrl = require("../core/confirm-dialog.html");
      $scope.$watch("vm.limitView", load);
      load();
    }

    function load() {
      if ($routeParams.projectId) {
        if ($routeParams.areaRef) {
          vm.status = statusService.getAreaStatus($routeParams.projectId, $routeParams.areaRef);
          $scope.$watch("vm.status.panels", updatePanelCompletion, true);

          
        } else {
          vm.project = statusService.getStatusForProject($routeParams.projectId, vm.limitView);
        }
      } else {
        vm.projects = statusService.getProjectsStatus(vm.limitView);
      }
    }

    function updatePanelCompletion() {
      calculatePanelCompletion(vm.status.panels);

      vm.panelsSelection = {};
      angular.forEach(vm.status.panels, function (panel) {
        vm.panelsSelection[panel.id] = false;
      });

      function calculatePanelCompletion(panels) {
        var numberCompleted = countCompletedPanels(vm.status.panels);
        var numberPanels = angular.isDefined(vm.status.panels) ? vm.status.panels.length : 0;
        vm.numberPanels = numberPanels;
        vm.completed = {
          number: numberCompleted,
          percentage: numberPanels > 0 ? Math.round((numberCompleted / numberPanels) * 100) : 0
        };
      }
      function countCompletedPanels(panels) {
        var numberCompleted = 0;
        angular.forEach(panels, function (panel) {
          if (panelCompleted(panel)) {
            numberCompleted++;
          }
        });
        return numberCompleted;
      }
    }

    function selectProject(projectId) {
      $location.url("/manage/status/" + projectId);
    }

    function selectArea(areaRef) {
      var currentUrl = $location.url();
      $location.url(currentUrl + "/" + areaRef);
    }

    function markSelectedPanelsComplete() {
      statusService.markPanelsComplete(vm.status.panels, vm.panelsSelection, vm.completedDate);
    }
    
    function deleteSelectedPanels() {
      
      var anyPanelsSelectedInProgress = false;
      
      angular.forEach(vm.status.panels, function (panel) {
        if (vm.panelsSelection[panel.id]) {
          var status = panelStatus(panel);
          if (status === "In progress") {
            anyPanelsSelectedInProgress = true;
          }
        }
      });
      
      if (anyPanelsSelectedInProgress) {
        $confirm({
          text: "Some panels selected are in progress, you cannot delete panels that are in progress!",
          cancel: "Ok"
        }, {
          templateUrl: require("../core/info-modal.html")
        });
      } else {
        $confirm({
          text: "Are you sure you want to delete the selected panels?"
        }, {
          templateUrl: require("../core/confirm-dialog.html")
        }).then(function () {
          return statusService.decrementDesignCount(getSelectedPanels(vm.panelsSelection))
              .then(function (){
                statusService.deletePanels(vm.status.panels, vm.panelsSelection);
              });
          if (vm.panelsAllSelected) {
            vm.panelsSelected = false;
          }
        });
      }

    }
    function getSelectedPanels(panelArr) {
      var markedPanels = {};
      var retunObj = {};
      angular.forEach(panelArr,function (value,key){
        if(value === true){
          markedPanels[key] = value;
        }
      });
      
      angular.forEach(vm.status.panels, function (panelData){
        angular.forEach(markedPanels, function (val,key){
          if(panelData.id === key){
            retunObj[panelData.$id] = panelData;
          }
        });
      });
      return retunObj;
    }

    function markPanelIncomplete(panel) {
      statusService.markPanelIncomplete(vm.status.panels, panel.$id);
    }

    function toggleAllPanels() {
      var toggleStatus = vm.panelsAllSelected;
      angular.forEach(vm.status.panels, function (panel){
        if (!panelCompleted(panel)) {
          vm.panelsSelection[panel.id] = toggleStatus;
          vm.panelsSelected = toggleStatus;
        }
      });
    }

    function atRisk(area) {
      var days = riskDays(area);
      return angular.isUndefined(days) || days < 0;
    }

    function riskDays(area) {
      if (!area.plan) {
        return null;
      }

      if (!area.plan.plannedFinish || !area.plan.riskDate) {
        return undefined;
      }

      return dateUtil.daysBetweenWeekDaysOnly(area.plan.plannedFinish, area.plan.riskDate);
    }

    function riskText(anArea) {
      var riskText = "";
      if (anArea.risk.high) {
        riskText = "Design handover is late";
      }
      if (anArea.risk.medium) {
        riskText = "Design handover due soon";
      }
      return riskText;
    }

    function panelToggled() {
      var allSelected = true;
      var anySelected = false;
      angular.forEach(vm.panelsSelection, function (value, key) {
        allSelected = allSelected && vm.panelsSelection[key];
        anySelected = vm.panelsSelection[key] ? true : anySelected;
      });
      vm.panelsAllSelected = allSelected;
      vm.panelsSelected = anySelected;
    }

    function panelCompleted(panel) {
      return panelQAStatusDefined(panel, "completed");
    }

    function panelStatus(panel) {
      if (panelQAStatusDefined(panel, "started")) {
        return "In progress";
      } else {
        return "...";
      }
    }

    function panelQAStatusDefined(panel, status) {
      return (angular.isDefined(panel.qa) && angular.isDefined(panel.qa[status]));
    }
  }
})();
