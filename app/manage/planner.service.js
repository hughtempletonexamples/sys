(function () {
  "use strict";

  angular
    .module("innView.manage")
    .factory("plannerService", plannerService);

  /**
   * @ngInject
   */
  function plannerService($log, $filter, $injector, $timeout, $q, dateUtil, firebaseUtil, schema, settingsService, audit, referenceDataService) {
    var WEEKS_IN_PLAN = 3;
    var WEEKS_FOR_REFRESH = 104;

    var initialised = false;

    var lineCapacities = {};
    var lineTotalCapacities = {};
    var areaPlans = {};

    var defaultBenchOperators = {};
    var defaultLineCapacity = {};
    var lineBenches = {};
    var defaultBench = {};
    var mainDefaultLineCapacity = {};
    var thirdPartySuppliers = {};
    var productTypeProductionLine = {};
    var areaPanels = {};
    var plannerSort = [];
    var designHandoverPeriod;
    var toggleActualPanels = false;

    var watches = [];
    var projects = null;
    var areas = null;
    var buildAreas = [];
    var buildAreasPanels = [];

    var activePlans = [];

    var planRefreshTimer = null;

    var service = {
      setCapacity: setCapacity,
      getForwardPlan: getForwardPlan,
      refreshProductionPlan: refreshProductionPlan,
      getProjectAreaProductionPlans: getProjectAreaProductionPlans,
      getProductionPlansByPlannedStart: getProductionPlansByPlannedStart,
      filterActualPanels: filterActualPanels,
      setPlannedStart: setPlannedStart,
      sortDataForChart: sortDataForChart,
      filterAreas: filterAreas
    };

    return service;

    ////////////

    function initialise() {
      if (initialised) {
        return $q.resolve();
      }
      else {
        return init();
      }

      function init() {
        var productTypes = referenceDataService.getProductTypes();
        var productionLines = referenceDataService.getProductionSubLines();
        var mainProductionLines = referenceDataService.getProductionLines();
        var suppliers = referenceDataService.getSuppliers();

        return settingsService.get()
          .then(function (settings) {
            designHandoverPeriod = settings.designHandoverPeriod;
            return firebaseUtil.loaded(productTypes, productionLines, suppliers);
          })
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
            defaultLineCapacity = buildMap(productionLines, "defaultCapacity");
            defaultBench = buildMap(productionLines, "defaultBench");
            defaultBenchOperators = buildMap(productionLines, "defaultBenchOperators");
            mainDefaultLineCapacity = buildMap(mainProductionLines, "defaultCapacity");
            thirdPartySuppliers = buildMap(suppliers, "thirdParty");
          })
          .then(function () {
            var deferredList = [];
            var productionCapacities = schema.getRoot().child("productionSubCapacities");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var lineCapacity = schema.getObject(productionCapacities.child(productionLineKey));
              lineCapacities[productionLineKey] = lineCapacity;

              deferredList.push(lineCapacity.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            var deferredList = [];
            var benchmarks = schema.getRoot().child("benchmarks");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var benchmark = schema.getObject(benchmarks.child(productionLineKey));
              lineBenches[productionLineKey] = benchmark;

              deferredList.push(benchmark.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            return schema.getRoot().child("config/plannerSort")
              .once("value", function ( snapshot ) {
                var returnPlannerSort = snapshot.val();
                var arr = getSortData(returnPlannerSort);
                if (angular.isDefined(arr) && arr.length > 0) {
                  plannerSort = getSortData(returnPlannerSort);
                }
              });
          })
          .then(function () {
            var deferredList = [];
            var productionTotalCapacities = schema.getRoot().child("productionCapacities");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var lineTotalCapacity = schema.getObject(productionTotalCapacities.child(productionLineKey));
              lineTotalCapacities[productionLineKey] = lineTotalCapacity;

              deferredList.push(lineTotalCapacity.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            var planRef = schema.getRoot().child("productionPlans/areas");
            return planRef.once("value", function (snapshot) {
              snapshot.forEach(function (snapPlan) {
                var plan = snapPlan.val();
                var planKey = snapPlan.key;
                if (angular.isUndefined(areaPlans[planKey])) {
                  areaPlans[planKey] = plan;
                }
              });
            });
          })
          .then(function () {
            initialised = true;
          });

        function buildMap(items, property) {
          var map = {};
          angular.forEach(items, function (item) {
            map[item.$id] = item[property];
          });
          return map;
        }
      }
    }
    
    function filterAreas(toSort){
      plannerSort = toSort;
      projects === null;
      areas === null;
      return initialise()
        .then(updateBuildAreas);
    }
    
    function filterActualPanels(showActualPanels){
      toggleActualPanels = showActualPanels;
      projects === null;
      areas === null;
      return initialise()
        .then(updateBuildAreas);
    }
    
    function getSortData(plannerSort){
      var fieldFilters = [];
      if (angular.isDefined(plannerSort)) {
        angular.forEach(plannerSort, function ( order, dataType ) {
          fieldFilters[order] = dataType;
        });
      }
      return fieldFilters;
    }


    function setCapacity(productionLine, date, panelCount) {
      
      var totalPanelCount = 0;
      var productionLineFiltered = $filter("numbersFromString")(productionLine);
      
      return initialise()
        .then(updateCapacity)
        .then(updateTotalCapacity)
        .then(auditUpdate)
        .then(refreshProductionPlan);

      function updateCapacity() {
        var lineCapacity = lineCapacities[productionLine];
        lineCapacities[productionLine][date] = panelCount;
        lineCapacity[date] = panelCount;
        
        totalPanelCount = panelCount;

        return lineCapacity.$save();
      }
      
      function updateTotalCapacity() {
        var lineTotalCapacity = lineTotalCapacities[productionLineFiltered];
        var defaultCapacity = dateUtil.isWeekDay(date) ? mainDefaultLineCapacity[productionLineFiltered] : 0;

        var line1 = productionLineFiltered;
        var line2 = productionLineFiltered+"2";
        var gotData = [];
        
        if (productionLineFiltered === "SIP" || productionLineFiltered === "TF"){
          if (angular.isDefined(lineCapacities[line1][date]) && angular.isDefined(lineCapacities[line2][date])){
            totalPanelCount = lineCapacities[line1][date] + lineCapacities[line2][date];
          }else{
            if (angular.isDefined(lineCapacities[line1][date])) {
              var line1 = lineCapacities[line1][date];
            }else{
              gotData.push(true);
              var line1 = 0;
            }
            if (angular.isDefined(lineCapacities[line2][date])) {
              var line2 = lineCapacities[line2][date];
            }else{
              gotData.push(true);
              var line2 = 0;
            }
            totalPanelCount = line1 + line2;
            
            if (gotData.length === 2){
              totalPanelCount = null;
            }
          }
        }else{
          if (angular.isDefined(lineCapacities[line1][date])){
            totalPanelCount = lineCapacities[line1][date];
          }
        }
        
        if (defaultCapacity === totalPanelCount) {
          totalPanelCount = null;
        }
        
        if (!angular.isNumber(totalPanelCount)){
          totalPanelCount = null;
        }

        lineTotalCapacity[date] = totalPanelCount;

        return lineTotalCapacity.$save();
      }

      function auditUpdate() {
        var log = "Capacity on " + date + (totalPanelCount != null ? " set to " + totalPanelCount : " reset to default");
        audit.productionLine(log, productionLineFiltered);
      }
    }

    function getForwardPlan(startDate, includeUnpublishedAreas) {
      if (angular.isUndefined(includeUnpublishedAreas)) {
        includeUnpublishedAreas = {};
      }

      var today = new Date();
      var start = new Date(startDate || today);
      if (start < today) {
        start = today;
      }

      // Plan displays from Monday of selected start date
      var viewStart = dateUtil.weekStart(start);
      var skipWeeks = Math.ceil(dateUtil.daysBetween(today, viewStart) / 7);

      var plan = {
        viewStart: viewStart,
        current: skipWeeks == 0 ? dateUtil.dayOfWeek(today) : -1,
        capacity: {},
        plannerSort: plannerSort,
        areas: [],
        dailyM2Totals: {},
        lineM2Totals: {},
        $destroy: destroyPlan
      };

      loadPlan(plan, skipWeeks, includeUnpublishedAreas);

      return plan;

      function destroyPlan() {
        removeActivePlan(this);
      }
    }

    function refreshProductionPlan() {
      $timeout.cancel(planRefreshTimer);
      planRefreshTimer = $timeout(performProductionPlanRefresh, 2000);
    }

    function performProductionPlanRefresh() {
      var plan = {
        viewStart: dateUtil.weekStart(new Date()),
        capacity: {},
        plannerSort: plannerSort,
        dailyM2Totals: {},
        lineM2Totals: {},
        areas: []
      };

      return initialise()
        .then(ensureDataLoaded)
        .then(populateCompletedPanels)
        .then(function () {
          populatePlan(plan, 0, WEEKS_FOR_REFRESH, {});

          refreshAreaProductionPlans(plan.areas)
            .then(triggerDataUnload);

          function refreshAreaProductionPlans(areas) {
            return $q.all(areas.filter(hasPlan).map(refreshAreaProductionPlan));
          }

          function hasPlan(area) {
            return !area._hasUnpublishedOnly;
          }

          function refreshAreaProductionPlan(area) {
            
            var today = dateUtil.todayAsIso();
            
            var buildDays = {};
            angular.forEach(area._productionPlan, function (panels, key) {
              var date = dateUtil.plusDays(plan.viewStart, key);
              //date greater than today because plan is effected now by panels complete on day
              if(panels !== null && date > today){
                buildDays[date] = panels;
              }
            });
            
            var areaPlan = {
              timestamps: {
                created: firebaseUtil.serverTime(),
                modified: firebaseUtil.serverTime()
              },
              project: area.project,
              plannedStart: area._productionStartDate,
              plannedFinish: area._productionEndDate,
              riskDate: area._riskDate,
              buildDays: buildDays
            };
            
            var planRef = schema.getRoot().child("productionPlans/areas").child(area.$id);
            
            var daysChanged = false;

            return planRef.once("value", function (snapshot) {
              var existing = snapshot.val();
              if (existing != null) {
                areaPlan.timestamps.created = existing.timestamps.created;
                angular.forEach(existing.buildDays, function ( amount, buildDate ) {
                  if (buildDays[buildDate] !== amount) {
                    daysChanged = true;
                  }
                });
              }
              if (existing == null
                || existing.plannedStart !== areaPlan.plannedStart
                || existing.plannedFinish !== areaPlan.plannedFinish
                || existing.riskDate !== areaPlan.riskDate
                || daysChanged) {
                return schema.getObject(planRef.child("buildDays")).$remove()
                        .then(function () {
                          return planRef.update(areaPlan);
                        });
              }else{
                return schema.getObject(planRef.child("buildDays")).$remove()
                        .then(function () {
                          return planRef.child("buildDays").update(buildDays);
                        });
              }
            });
          }

          function triggerDataUnload() {
            $timeout(unloadData, 5000);
          }
        });
    }
    
    function setPlannedStart(area, plannedStart, change) {
      
      var planRef = schema.getRoot().child("productionPlans/areas").child(area.$id);
     
      return planRef.once("value", function (snapshot) {
        var existing = snapshot.val();
        var existingKey = snapshot.key;
        existing.plannedStart = plannedStart;
        existing.userChange = change;
        areaPlans[existingKey] = existing;
        return planRef.update(existing);
      });
      
    }
    
    function sortDataForChart(plannerData, staticAverages) {
      var start = plannerData.viewStart;
      var allData = {};
      var mainData = {};
      var chartData = {};
      var maxData = {};
      var maxDataEach = {};
      var maxBench = {};
      var colours = [];
      var axises = ["1","2"];
      var areaColour = {};
      var projectsPublished = {};
      var days = WEEKS_IN_PLAN * 7;
      
      angular.forEach(projects, function ( project ) {
        if(angular.isUndefined(projectsPublished[project.$id]) && angular.isDefined(project.deliverySchedule.published)){
          projectsPublished[project.$id] = project;
        }
      });

      //This is in to make sure there is alway minimal data in the chart
      angular.forEach(staticAverages, function ( averages, productionLine ) {
        
        if (angular.isUndefined(chartData[productionLine])) {
          chartData[productionLine] = [];
        }
        
        if (angular.isUndefined(maxData[productionLine])) {
          maxData[productionLine] = {};
        }
        if (angular.isUndefined(mainData[productionLine])) {
          mainData[productionLine] = {};
        }

        angular.forEach(axises, function ( axis ) {
          
          if (angular.isUndefined(maxData[productionLine][axis])) {
            maxData[productionLine][axis] = {};
          }
          if (angular.isUndefined(mainData[productionLine][axis])) {
            mainData[productionLine][axis] = {};
          }

          if (axis) {
            var dataOb = {
              type: "bar",
              key: "area",
              color: "rgb(139,0,0)",
              yAxis: axis,
              values: []
            };
          } else {
            var dataOb = {
              key: "area",
              type: "bar",
              color: "rgb(139,0,0)",
              yAxis: axis,
              values: []
            };
          }
          for (var i = 0; i < days; i++) { 
            var allDate = dateUtil.plusDays(start, i); 
            if (angular.isUndefined(maxData[productionLine][axis][allDate])) {
              maxData[productionLine][axis][allDate] = 0;
            }
            var monthDate = new Date(allDate);
            var value;
            var value = {
              "x": monthDate.getTime(),
              "y": 0
            };
            dataOb.values[i] = value;

          }
          
          chartData[productionLine].push(dataOb);
          
        });
        
      });
      
      var axis = "1";
      angular.forEach(plannerData.areas, function ( area ) {
        if (area.supplier === "Innovaré") {
          //now not filtered to just SIP anymore
          var productionLineFiltered = area._productionLine;
          if(angular.isDefined(productionLineFiltered) && productionLineFiltered !== null){
            if (angular.isUndefined(maxData[productionLineFiltered])) {
              maxData[productionLineFiltered] = {};
            }
            if (angular.isUndefined(mainData[productionLineFiltered])) {
              mainData[productionLineFiltered] = {};
            }
            var areaId = area._id + "-axis" + axis;
            //build objects
            if (angular.isUndefined(areaColour[areaId])) {
              areaColour[areaId] = {
                "color": "rgb(139,0,0)",
                "type": "unset"
              };
            }

            if (angular.isUndefined(mainData[productionLineFiltered][axis][areaId])) {
              mainData[productionLineFiltered][axis][areaId] = {};
            }
            for (var i = 0; i < days; i++) {
              var allDate = dateUtil.plusDays(start, i);
              if (angular.isUndefined(maxData[productionLineFiltered][axis][allDate])) {
                maxData[productionLineFiltered][axis][allDate] = 0;
              }
              if (angular.isUndefined(mainData[productionLineFiltered][axis][areaId][allDate])) {
                mainData[productionLineFiltered][axis][areaId][allDate] = {
                  "meterSquared": 0,
                  "type": "neither",
                  "amount": 0,
                  "framingStyle": ""
                };
              }
            }
            angular.forEach(area._productionPlan, function ( buildOnDay, key ) {
              if (key <= 20 && buildOnDay !== null && buildOnDay > 0) {
                var date = dateUtil.plusDays(start, key);
                var type = "neither";
                var amount = 0;
                if (buildOnDay !== null && buildOnDay > 0) {
                  if (area.actualArea > 0 && area.actualArea > 0) {
                    var avg = area.actualArea / area.actualPanels;
                    var amount = avg * buildOnDay;
                    type = "actualAmount";
                  } else if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                    var avg = area.estimatedArea / area.estimatedPanels;
                    var amount = avg * buildOnDay;
                    type = "estimatedAmount";
                  } else {
                    type = "neither";
                    var amount = 0;
                  }
                }
                var niceData = mainData[productionLineFiltered][axis][areaId][date];
                if (angular.isDefined(niceData)) {              
                  niceData.amount += buildOnDay;
                  niceData.meterSquared += amount;
                  areaColour[areaId].color = getColor(type, axis);
                  areaColour[areaId].type = type;
                  maxData[productionLineFiltered][axis][date] += amount;
                  niceData.type = type;
                  if (area._framingStyleList && area._framingStyleList.length > 1) {
                    niceData.framingStyle = area._framingStyleList[0];
                  }
                }
              }
            });
          }
        }
      });
      


      
      var axis = "2";
      angular.forEach(areas, function ( area ) {
        if (angular.isDefined(projectsPublished[area.project]) && angular.isDefined(area.revisions[projectsPublished[area.project].deliverySchedule.published])) {
          var published = area.revisions[projectsPublished[area.project].deliverySchedule.published];
          var dispatchDate = dateUtil.subtractWorkingDays(published, 1);
          var nextTwentyOneDays = dateUtil.plusDays(start, 20);
          if (area.supplier === "Innovaré" && (dispatchDate >= start && dispatchDate <= nextTwentyOneDays)) {
            //now not filtered to just SIP anymore
            var areaIdBeforeAxis = getAreaId(area, projectsPublished[area.project]);
            var areaId = areaIdBeforeAxis + "-axis" + axis;
            var productionLineFiltered = area._productionLine;
            if(angular.isDefined(productionLineFiltered) && productionLineFiltered !== null){
              if (angular.isUndefined(maxData[productionLineFiltered])) {
                maxData[productionLineFiltered] = {};
              }
              if (angular.isUndefined(mainData[productionLineFiltered])) {
                mainData[productionLineFiltered] = {};
              }
              if (angular.isUndefined(areaColour[areaId])) {
                areaColour[areaId] = {
                  "color": "rgb(139,0,0)",
                  "type": "unset"
                };
              }
              if (angular.isUndefined(maxData[productionLineFiltered][axis])) {
                maxData[productionLineFiltered][axis] = {};
              }
              if (angular.isUndefined(mainData[productionLineFiltered][axis])) {
                mainData[productionLineFiltered][axis] = {};
              }
              if (angular.isUndefined(mainData[productionLineFiltered][axis][areaId])) {
                mainData[productionLineFiltered][axis][areaId] = {};
              }
              for (var i = 0; i < days; i++) {
                var allDate = dateUtil.plusDays(start, i);
                if (angular.isUndefined(maxData[productionLineFiltered][axis][allDate])) {
                  maxData[productionLineFiltered][axis][allDate] = 0;
                }
                if (angular.isUndefined(mainData[productionLineFiltered][axis][areaId][allDate])) {
                  mainData[productionLineFiltered][axis][areaId][allDate] = {
                    "meterSquared": 0,
                    "type": "neither",
                    "amount": 0,
                    "framingStyle": ""
                  };
                }
              }
              var type = "neither";
              var amount = 0;
              var count = 0;
              if (area.actualArea > 0 && area.actualArea > 0) {
                var avg = area.actualArea / area.actualPanels;
                amount = area.actualArea;
                count = area.actualPanels;
                type = "actualAmount";
              } else if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                var avg = area.estimatedArea / area.estimatedPanels;
                amount = area.estimatedArea;
                count = area.estimatedPanels;
                type = "estimatedAmount";
              } else {
                amount = 0;
                count = 0;
                type = "neither";
              }
              var niceData = mainData[productionLineFiltered][axis][areaId][dispatchDate];
              if (angular.isDefined(niceData)) {
                niceData.amount += count;
                niceData.meterSquared += amount;
                maxData[productionLineFiltered][axis][dispatchDate] += amount;
                areaColour[areaId].color = getColor(type, axis);
                areaColour[areaId].type = type;
                niceData.type = type;
                if (area._framingStyleList && area._framingStyleList.length > 1) {
                  niceData.framingStyle = area._framingStyleList[0];
                }
              }
            }
          }
        }
      });
      
      angular.forEach(mainData, function (data, productionLine) {
        if (angular.isUndefined(chartData[productionLine])) {
          chartData[productionLine] = [];
        }
        if (angular.isUndefined(maxDataEach[productionLine])) {
          maxDataEach[productionLine] = [];
        }
        if (angular.isUndefined(maxBench[productionLine])) {
          maxBench[productionLine] = [];
        }
        angular.forEach(data, function (theData, axis) {
          var counter = 0;
          angular.forEach(theData, function (forecastDatums, area) {
            if (axis) {
              var dataOb = {
                type: "bar",
                key: area,
                color: areaColour[area].color,
                yAxis: axis,
                values: []
              };
            } else {
              var dataOb = {
                key: area,
                type: "bar",
                color: areaColour[area].color,
                yAxis: axis,
                values: []
              };
            }
            var weekCounter = 0;
            angular.forEach(forecastDatums, function (forecastData, week) {        
              var monthDate = new Date(week);
              var value;
              var value = {
                "x": monthDate.getTime(),
                "y": forecastData.meterSquared
              };
              if(angular.isDefined(dataOb.values[weekCounter])){
                dataOb.values[weekCounter] = value;
              }else{
                dataOb.values.push(value);
              }
              weekCounter++;
              maxDataEach[productionLine].push(maxData[productionLine][axis][week]);
              if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][week]) && angular.isDefined(lineBenches[productionLine][week].meterSquared)) {
                maxBench[productionLine].push(lineBenches[productionLine][week].meterSquared);
              } else {
                maxBench[productionLine].push(defaultBench[productionLine]);
              }
            });
            if (angular.isDefined(dataOb.values[weekCounter])) {
              chartData[productionLine][counter] = dataOb;
            } else {
              chartData[productionLine].push(dataOb);
            }
            
            counter++;
          });
        });
      });
//      angular.forEach(benches, function (dateData, productionLine) {
//        var lineOb = {
//          key: "bench",
//          type: "line",
//          color: "rgb(2, 0, 0)",
//          yAxis: 1,
//          values: []
//        };
//        angular.forEach(dateData, function ( benchData, date ) {
//          var monthDate = new Date(date);
//          var value;
//          var value = {
//            "x": monthDate.getTime(),
//            "y": benchData.benchM2
//          };
//          lineOb.values.push(value);
//        });
//        chartData[productionLine].push(lineOb);
//      });
      allData["maxBench"] = maxBench;
      allData["defaultBenchOperators"] = defaultBenchOperators;
      allData["defaultBench"] = defaultBench;
      allData["defaultLineCapacity"] = defaultLineCapacity;
      allData["lineCapacities"] = lineCapacities;
      allData["lineBenches"] = lineBenches;
      allData["maxData"] = maxDataEach;
      allData["maxDataDate"] = maxData;
      allData["mainData"] = mainData;
      allData["chartData"] = chartData;
      allData["areaColour"] = areaColour;
      return allData;
      
      function getColor( type, axis ) {
        var color;
        if (axis === "1") {
          if (type === "actualAmount") {
            color = "rgb(0,128,0)";
          } else {
            color = "rgb(255,165,0)";
          }
        } else {
          if (type === "actualAmount") {
            color = "rgb(128,128,128)";
          } else {
            color = "rgb(192,192,192)";
          }
        }
        return color;
      }

    }

    function getProjectAreaProductionPlans(projectKey) {
      var planRef = schema.getRoot().child("productionPlans/areas").orderByChild("project").equalTo(projectKey);
      return schema.getArray(planRef);
    }

    function getProductionPlansByPlannedStart() {
      var plansRef = schema.getRoot().child("productionPlans/areas").orderByChild("plannedStart");
      return schema.getArray(plansRef);
    }

    function loadPlan(plan, skipWeeks, includeUnpublishedAreas) {
      var includeUnpublished = includeUnpublishedAreas;
      return initialise()
        .then(ensureDataLoaded)
        .then(populateCompletedPanels)
        .then(function () {
          populatePlan(plan, skipWeeks, WEEKS_IN_PLAN, includeUnpublished);

          addActivePlan(plan, skipWeeks, WEEKS_IN_PLAN, includeUnpublished);
        });
    }
    
    var panelsService;
 
    function populateCompletedPanels() {

      // Lazily inject panelsService, to avoid circular dependency if
      // done at construction time.
      if (!panelsService) {
        panelsService = $injector.get("panelsService");
      }

      var deferredList = [];
      angular.forEach(areas, function (area) {
        if (angular.isDefined(area._productionLine) && area._productionLine !== null) {   
          if (angular.isUndefined(buildAreasPanels[area._productionLine])) {
            buildAreasPanels[area._productionLine] = [];
          }
          if (angular.isUndefined(areaPanels[area.$id])) {
            areaPanels[area.$id] = [];
          }
          deferredList.push(
            panelsService.panelsForArea(area.$id)
            .$loaded()
            .then(function (panels) {
              var totalM2 = 0;
              angular.forEach(panels, function (panel) {
                if (angular.isDefined(panel.dimensions) && angular.isDefined(panel.dimensions.area)){
                  totalM2 += panel.dimensions.area;
                }
                
                var framingStyle = writeFramingStyle(panel.additionalInfo.framingStyle, panel.additionalInfo.sheathing);
                if(areaPanels[area.$id].indexOf(framingStyle) === -1) {
                  areaPanels[area.$id].push(framingStyle);
                }
              
                if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa["completed"])) {
                  buildAreasPanels[area._productionLine].push(panel);
                }
              });
              areaPanels[area.$id].push("Total m2 : " + $filter("number")(totalM2, 0));
            })
            );
        }
      });
      return $q.all(deferredList);
    }
    
    function writeFramingStyle(framingStyle, sheathing){
      if (angular.isDefined(framingStyle) && angular.isDefined(sheathing)){
        return framingStyle + " || " + sheathing;
      }
    }
    
    function ensureDataLoaded() {
      var reloadBuildAreaTimer;
      var reloadPlansTimer;

      if (projects == null && areas == null) {
        projects = schema.getArray(schema.getRoot().child("projects"));
        areas = schema.getArray(schema.getRoot().child("areas"));

        watches.push(projects.$watch(reloadBuildAreas));
        watches.push(areas.$watch(reloadBuildAreas));

        angular.forEach(lineCapacities, function (lineCapacity) {
          watches.push(lineCapacity.$watch(reloadPlans));
        });
        
        angular.forEach(lineTotalCapacities, function (lineTotalCapacity) {
          watches.push(lineTotalCapacity.$watch(reloadPlans));
        });

        return firebaseUtil.loaded(projects, areas)
          .then(updateBuildAreas);
      }

      return $q.resolve();

      function reloadBuildAreas() {
        $timeout.cancel(reloadBuildAreaTimer);
        reloadBuildAreaTimer = $timeout(updateBuildAreas, 250);
      }

      function reloadPlans() {
        $timeout.cancel(reloadPlansTimer);
        reloadPlansTimer = $timeout(updateActivePlans, 100);
      }
    }

    function unloadData() {
      if (activePlans.length == 0 && projects != null && areas != null) {
        angular.forEach(watches, function (watch) {
          watch();
        });
        watches = [];

        projects.$destroy();
        projects = null;
        areas.$destroy();
        areas = null;
      }
    }

    function updateBuildAreas() {
      buildAreas = selectAreasForProduction(areas, projects);
      updateActivePlans();
    }

    function updateActivePlans() {
      angular.forEach(activePlans, function (active) {
        populatePlan(active.plan, active.skipWeeks, active.planWeeks, active.includeUnpublishedAreas);
      });
    }

    function addActivePlan(plan, skipWeeks, planWeeks, includeUnpublishedAreas) {
      activePlans.push(
        {
          plan: plan,
          skipWeeks: skipWeeks,
          planWeeks: planWeeks,
          includeUnpublishedAreas: includeUnpublishedAreas
        }
      );
    }

    function removeActivePlan(plan) {
      angular.forEach(activePlans, function (active, index) {
        if (plan == active.plan) {
          activePlans.splice(index, 1);
          angular.forEach(plan, function (value, key) {
            delete plan[key];
          });
        }
      });

      $timeout(unloadData, 5000);
    }

    function populatePlan(plan, skipWeeks, planWeeks, includeUnpublishedAreas) {
      var areas = angular.copy(buildAreas);
      var blotter = createBlotter();

      calculatePlan(blotter, areas, includeUnpublishedAreas);
      plan.capacity = getCapacityFromBlotter();
      var blotterAreas = getAreasFromBlotter(includeUnpublishedAreas);
      plan.areas = $filter("orderBy")(blotterAreas, plannerSort);
      var calculateM2 = calculateM2( plan.areas );
      plan.lineM2Totals = calculateM2.line;
      plan.dailyM2Totals = calculateM2.total;
      
      function createBlotter() {
        var today = new Date();
        var planStart = dateUtil.weekStart(today);
        var planDays = 7 * (skipWeeks + planWeeks);

        var blotter = {
          planStart: planStart,
          planDays: planDays,
          viewStart: plan.viewStart,
          current: plan.current,
          dailyM2Totals: {},
          lineM2Totals: {},
          productionLines: {},
          areas: []
        };

        var nextStart = dateUtil.dayOfWeek(today);
        angular.forEach(defaultLineCapacity, function (_, productionLine) {
          var lineBlotter = {
            capacity: [],
            available: [],
            nextStart: nextStart
          };
 
          for (var i = 0; i < planDays; i++) {
            var buildDate = dateUtil.plusDays(planStart, i);
            var capacity = getCapacity(productionLine, buildDate);
            var available = getAvailable(productionLine, buildDate, capacity, today);
            
            lineBlotter.capacity.push(capacity);
            lineBlotter.available.push(available);
          }

          blotter.productionLines[productionLine] = lineBlotter;
        });

        return blotter;
      }
      
      function calculateM2( areas ) {

        var calculatedM2 = {
          "line": {},
          "total": {}
        };

        angular.forEach(areas, function ( area ) {
          var productionLine = productTypeProductionLine[area.type];
          angular.forEach(area._productionPlanM2, function ( m2, buildDays ) {
            if (angular.isUndefined(calculatedM2.line[productionLine])) {
              calculatedM2.line[productionLine] = {};
            }
            if (angular.isUndefined(calculatedM2.line[productionLine][buildDays])) {
              calculatedM2.line[productionLine][buildDays] = 0;
            }
            if (angular.isUndefined(calculatedM2.total[buildDays])) {
              calculatedM2.total[buildDays] = 0;
            }
            calculatedM2.line[productionLine][buildDays] += m2;
            calculatedM2.total[buildDays] += m2;
          });
        });

        return calculatedM2;
      }

      function getAreasFromBlotter() {
        angular.forEach(blotter.areas, function (area) {
          area._productionPlan.splice(0, 7 * skipWeeks);
          area._productionPlanM2.splice(0, 7 * skipWeeks);
        });

        return blotter.areas;
      }

      function getCapacityFromBlotter() {
        var capacity = {};
        angular.forEach(blotter.productionLines, function (lineBlotter, productionLine) {
          lineBlotter.capacity.splice(0, 7 * skipWeeks);
          capacity[productionLine] = lineBlotter.capacity;
        });
        return capacity;
      }
    }

    function selectAreasForProduction(areas, projects) {
      var buildAreas = [];
      var sortKey = 0;

      angular.forEach(areas, function (area, key) {
        area._deliveryDate = undefined;
        if (toggleActualPanels) {
          if (!thirdPartySuppliers[area.supplier] && area.actualPanels && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
            var project = projects.$getRecord(area.project);
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if(project.active){
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                area._sortKey = sortKey;
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._framingStyleList = areaPanels[area.$id];
                area._id = getAreaId(area, project);
                area._projId = project.id;
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);
                if (area.line > 1){
                  var productionLine = productTypeProductionLine[area.type] || null;
                  if (productionLine === "SIP"){
                    area._productionLine =  productionLine + area.line;
                    area._line = area.line;
                  }else if (productionLine === "TF"){
                    area._productionLine =  productionLine + area.line;
                    area._line = area.line;
                  }else{
                    area._productionLine =  productionLine;
                    area._line = 1;
                  }           
                }else{
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }
                sortKey++;
                buildAreas.push(area);
              }
            }
          }
        } else {
          if (!thirdPartySuppliers[area.supplier] && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
            var project = projects.$getRecord(area.project);
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if(project.active){
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                area._sortKey = sortKey;
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._framingStyleList = areaPanels[area.$id];
                area._id = getAreaId(area, project);
                area._projId = project.id;
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);
                if (area.line > 1){
                  var productionLine = productTypeProductionLine[area.type] || null;
                  if (productionLine === "SIP"){
                    area._productionLine =  productionLine + area.line;
                    area._line = area.line;
                  }else if (productionLine === "TF"){
                    area._productionLine =  productionLine + area.line;
                    area._line = area.line;
                  }else{
                    area._productionLine =  productionLine;
                    area._line = 1;
                  }           
                }else{
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }
                sortKey++;
                buildAreas.push(area);
              }
            }
          }
        }

      });

      return buildAreas;
    }

    function calculatePlan(blotter, areas, includeUnpublishedAreas) {
      
      areas.sort(
        function (areaA, areaB) {
          var a = includeUnpublishedAreas[areaA.$id] ? areaA._unpublishedDeliveryDate : areaA._deliveryDate;
          var b = includeUnpublishedAreas[areaB.$id] ? areaB._unpublishedDeliveryDate : areaB._deliveryDate;
          var r = a.localeCompare(b);
          if (r === 0) {
            a = areaA._id;
            b = areaB._id;
            r = a.localeCompare(b);
            return r;
          }
          if (r === 0) {
            r = (angular.isDefined(areaA._sortKey) && angular.isDefined(areaA._sortKey)) ? areaA._sortKey - areaB._sortKey : 0;
          }
          return r;
        }
      );

      angular.forEach(areas, function (area) {
        if (angular.isDefined(area._deliveryDate)) {
          if (calculateAreaPlan(area, includeUnpublishedAreas[area.$id])) {
            blotter.areas.push(area);
          }
        }
      });

      function calculateAreaPlan(area, useUnpublishedDate) {
        delete area._productionStartDate;
        var productionLine = area._productionLine;
        var lineBlotter = blotter.productionLines[productionLine];
        if (!lineBlotter) {
          // Just drop area
          return false;
        }

        if (lineBlotter.nextStart < 0) {
          // No available capacity
          return false;
        }

        var productionPlan = [];
        var productionPlanM2 = [];
        for (var i = 0; i < blotter.planDays; i++) {
          productionPlan[i] = null;
          productionPlanM2[i] = null;
        }

        var designHandoverDate = useUnpublishedDate ? area._unpublishedDesignHandoverDate : area._designHandoverDate;

              
        var buildDay = lineBlotter.nextStart;
        var buildDate = dateUtil.plusDays(blotter.planStart, buildDay);  
        area._userChange = false;
        
        //if there is a change of plannedStart by user
        if (angular.isDefined(areaPlans[area.$id]) && areaPlans[area.$id].userChange) {
          var difference = dateUtil.daysBetween(blotter.planStart, areaPlans[area.$id].plannedStart);
          if (difference > -1){
            buildDay = difference;
            var buildDate = areaPlans[area.$id].plannedStart;
            area._userChange = true;
          }
        }
        
        if (!area._userChange){
          // Skip forward to design handover date
          while (buildDate < designHandoverDate) {
            buildDate = dateUtil.plusDays(blotter.planStart, ++buildDay);
          }
        }

        if (buildDay > blotter.planDays) {
          return false;
        }

        area._productionPlan = productionPlan;
        area._productionPlanM2 = productionPlanM2;

        if (area._hasUnpublishedOnly && !useUnpublishedDate) {
          // Don't consume capacity, but potentially include in area list
          return buildDate >= blotter.viewStart;
        }

        var panels = (area.actualPanels || area.numberOfPanels || area.estimatedPanels || 0) - (area.completedPanels || 0);
        if (panels == 0) {
          productionPlan[buildDay] = 0;
          productionPlanM2[buildDay] = 0;
          area._productionStartDate = buildDate;
          area._productionEndDate = null;
        }
        while (panels > 0) {
          // Extend planning capacity
          if (lineBlotter.available.length <= buildDay) {
            var capacity = getCapacity(productionLine, buildDate);
            lineBlotter.available[buildDay] = capacity;
          }

          var available = lineBlotter.available[buildDay];
          if (available) {
            if (!area._productionStartDate) {
              area._productionStartDate = buildDate;
            }
            var buildCount = Math.min(panels, available);

            panels -= buildCount;
            available -= buildCount;

            lineBlotter.available[buildDay] = available;
            if (buildDay < productionPlan.length) {
              //get meter squared
              var avg = 0;
              var amount = 0;

              if (angular.isDefined(area.actualPanels) && area.actualPanels > 0) {
                if (angular.isDefined(area.actualArea) && area.actualArea > 0) {
                  avg = area.actualArea / area.actualPanels;
                  amount = avg * buildCount;
                }
              } else if (angular.isDefined(area.numberOfPanels) && area.numberOfPanels > 0) {
                if (angular.isDefined(area.areaOfPanels) && area.areaOfPanels > 0) {
                  avg = area.areaOfPanels / area.numberOfPanels;
                  amount = avg * buildCount;
                }
              } else if (angular.isDefined(area.estimatedPanels) && area.estimatedPanels > 0) {
                if (angular.isDefined(area.estimatedArea) && area.estimatedArea > 0) {
                  avg = area.estimatedArea / area.estimatedPanels;
                  amount = avg * buildCount;
                }
              }
              
              productionPlan[buildDay] = buildCount;
              productionPlanM2[buildDay] = amount; 
              
            }

            if (panels == 0) {
              area._productionEndDate = buildDate;
            }
          }

          if (!available) {
            while (buildDay < lineBlotter.available.length && !lineBlotter.available[buildDay]) {
              buildDay++;
            }
            buildDate = dateUtil.plusDays(blotter.planStart, buildDay);
          }
        }

        lineBlotter.nextStart = buildDay < blotter.planDays ? buildDay : -1;

        if (area._productionEndDate) {
          return area._productionEndDate >= blotter.viewStart;
        }
        else {
          return area._productionStartDate >= blotter.viewStart;
        }
      
      }
    }
         
    function getAreaId(area, project) {
      return [project.id, area.phase, area.floor, area.type].join("-");
    }

    function getCapacity(productionLine, date) {
      var capacity = lineCapacities[productionLine][date];
      if (angular.isDefined(capacity)) {
        return capacity;
      }

      if (dateUtil.isWeekDay(date)) {
        return defaultLineCapacity[productionLine];
      }
      else {
        return null;
      }
    }
    
    function getAvailable(productionLine, buildDate, capacity, today) {
      
      var available;
      var sum = 0;
      var panelsCompleteOnDay;
        
      panelsCompleteOnDay  = getPanelsCompleteOnDay(productionLine, buildDate);
      
      if(angular.isUndefined(panelsCompleteOnDay) || isNaN(panelsCompleteOnDay)){
        panelsCompleteOnDay = 0;
      }

      if (panelsCompleteOnDay > capacity) {
        sum = capacity;
      } else {
        sum = panelsCompleteOnDay;
      }

      if (dateUtil.isWeekDay(buildDate) && buildDate === dateUtil.toIsoDate(today)) {
        available = capacity - sum;
      } else {
        available = capacity;
      }

      return available;
    }

    function getPanelsCompleteOnDay(productionLine, date) {
      var counter = 0;
      var completeDate;
      var formattedDate;

      angular.forEach(buildAreasPanels[productionLine], function (panel) {
        completeDate = new Date(panel.qa["completed"]);
        formattedDate = dateUtil.toIsoDate(completeDate);
        if (formattedDate === date) {
          counter++;
        }
      });
      return counter;
    }

  }

})();
