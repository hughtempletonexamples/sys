(function () {
  "use strict";

  angular
    .module("innView.manage")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/manage/status", {
        templateUrl: require("./allProjectsStatus.html"),
        controller: "ProjectStatusController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/manage/status/:projectId", {
        templateUrl: require("./singleProjectStatus.html"),
        controller: "ProjectStatusController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/manage/status/:projectId/:areaRef", {
        templateUrl: require("./areaStatus.html"),
        controller: "ProjectStatusController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/manage/plan", {
        templateUrl: require("./forwardPlanner.html"),
        controller: "ForwardPlannerController",
        controllerAs: "vm",
        reloadOnSearch: false,
        resolve: {
          auth: requireAuth
        }
      })
      .when("/manage/loadPanels", {
        templateUrl: require("./loadPanels.html"),
        controller: "LoadPanelsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
