(function () {
  "use strict";

  angular
    .module("innView.manage")
    .component("innCapacityEdit", {
      templateUrl: require("./capacityEdit.html"),
      controller: capacityInputController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        _capacity: "=ngModel",
        ngChange: "&?ngChange"
      }
    });

  /**
   * @ngInject
   */
  function capacityInputController($timeout) {
    var vm = this;

    vm.max = 999;
    vm.capacity = capacity;

    ////////////

    function capacity(value) {
      if (arguments.length) {
        if (angular.isDefined(value) && value >= 0 && value <= vm.max) {
          var newCapacity = value != null ? Math.floor(value) : null;
          if (vm._capacity !== newCapacity) {
            vm._capacity = newCapacity;
            if (vm.ngChange) {
              $timeout(vm.ngChange);
            }
          }
        }
      }

      return vm._capacity;
    }

  }

})();
