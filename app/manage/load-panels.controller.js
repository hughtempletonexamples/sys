(function () {
  "use strict";

  angular
    .module("innView.manage")
    .controller("LoadPanelsController", LoadPanelsController);

  /**
   * @ngInject
   */
  function LoadPanelsController($log, $window, loadPanelsService, panelsService, datumsService) {
    var vm = this;

    var filePanels = [];
    var fileComponents = {};
    var fileAreas = {};
    var fileErrors = [];
    
    vm.saveInProgress = 0;
    
    vm.selectedComponet = selectedComponet;
    vm.upload = upload;
    vm.removeFile = removeFile;
    vm.reset = reset;
    vm.save = save;

    init();

    ////////////

    function init() {
      vm.files = [];
      vm.updateSummary = null;
      filePanels = [];
      fileComponents = {};
      fileAreas = {};
      fileErrors = [];
      vm.saveInProgress = 0;

      updateLists();
    }

    function upload(files) {
      angular.forEach(files, parseFile);
    }

    function removeFile(index) {
      vm.files.splice(index, 1);
      filePanels.splice(index, 1);
      fileComponents.delete;
      fileAreas.delete;
      fileErrors.splice(index, 1);

      updateLists();
    }

    function reset() {
      init();
    }
    
    function allSelected(){
      var selected = true;
      if (angular.isDefined(vm.components)){
        angular.forEach(vm.components, function (components) {
          if (!components.selected){
            selected = false;
          }
        });
      }
      return selected;
    }

    function save() {
      var selected = allSelected();
      if (selected) {
        vm.saveInProgress = 1;
        if (vm.panels.length > 0 && vm.errors.length === 0) {
          panelsService.updatePanels(vm.panels)
            .then(function (updateSummary) {
              vm.updateSummary = updateSummary;
              return loadPanelsService.incrimentDesignCount(vm.panels);
            })
            .then(function () {
              return datumsService.averageMeterSquareOfDatum();
            })
            .catch(function (error) {
              $window.alert(error);
            });
        }
      }
    }
    
    function jobSheetCount(panels){
      var keys = [];
      angular.forEach(panels, function (item) {
        var key = item["jobSheet"];
        if (keys.indexOf(key) === -1 && angular.isDefined(key)) {
          keys.push(key);
        }
      });
      return keys.length;
    }

    function parseFile(file) {
      var index = vm.files.push(file) -1;
      loadPanelsService.loadPanelSchedule(file)
        .then(function (result) {
          filePanels[index] = result.panels || [];
          fileComponents = result.components || {};
          fileAreas = result.areas || {};
          fileErrors[index] = result.errors || [];
          updateLists();
        })
        .catch(function (error) {
          filePanels[index] = [];
          fileComponents = {};
          fileAreas = {};
          fileErrors[index] = [ "Failed to parse '" + file.name + "'"];
          updateLists();
        });

    }
    
    function selectedComponet(area, components, index) {
      var selectData = vm.selectData[index];
      if (angular.isDefined(selectData) && selectData !== ""){
        components.selected = true;
        angular.forEach(vm.panels, function (panel) {
          var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
          var idParts = PANEL_ID_REGEX.exec(panel.id);
          if (idParts) {
            var id = idParts[1] + "-" + idParts[3] + "-" + idParts[4] + "-" + panel.type;
            if(id === area){
              panel.area = selectData;
            }
          }
        });
        vm.saveInProgress = false;
        vm.actualEqualNumberOfPanels = actualEqualNumberOfPanels(fileAreas, vm.panels);
      }else{
        components.selected = false;
        angular.forEach(vm.panels, function (panel) {
          var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
          var idParts = PANEL_ID_REGEX.exec(panel.id);
          if (idParts) {
            var id = idParts[1] + "-" + idParts[3] + "-" + idParts[4] + "-" + panel.type;
            if(id === area){
              panel.area = "";
            }
          }
        });
        vm.saveInProgress = true;
        vm.actualEqualNumberOfPanels = actualEqualNumberOfPanels(fileAreas, vm.panels);
      }
    } 
   
    function updateLists() {
      vm.panels  = [].concat.apply([], filePanels);
      vm.components = fileComponents;
      vm.actualEqualNumberOfPanels = actualEqualNumberOfPanels(fileAreas, vm.panels);
      vm.errors = [].concat.apply([], fileErrors);
      vm.jobSheetCount = jobSheetCount(vm.panels);
    }
    
    function actualEqualNumberOfPanels(areas, panels){
      var areasHolder = {};
      angular.forEach(panels, function (item) {
        var key = item["area"];
        if (angular.isDefined(areas[key])) {
          var area = areas[key];
          var areaId = area.phase + "-" + area.floor + "-" + area.type;
          if (angular.isUndefined(areasHolder[areaId])) {
            areasHolder[areaId] = {
              "numberOfPanels": 0,
              "actual": 0,
              "numberOfPanelsExist": false
            };
          }
          if (angular.isDefined(area.numberOfPanels)) {
            areasHolder[areaId].numberOfPanels = area.numberOfPanels;
            areasHolder[areaId].numberOfPanelsExist = true;
          }
          areasHolder[areaId].actual++;
        }else{
          vm.saveInProgress = true;
        }
      });
      return areasHolder;
    }


  }
})();
