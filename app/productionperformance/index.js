(function () {
  "use strict";

  require("./productionperformance.module");

  require("./productionperformance.controller");
  
  require("./productionperformance.service");
  
  require("./productionPlan.service");
  
  require("./maxRisks.filter");
  
  require("./by-now.filter");

  require("./config.route");

})();
