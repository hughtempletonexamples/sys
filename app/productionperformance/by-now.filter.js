(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .filter("byNow", filter);

  function filter($filter) {

    return byNowFilter;

    function byNowFilter(now, target, standard) {
      
      
      if (angular.isUndefined(target)){
        target = standard;
      }
      
      target = parseInt(target, 0);
      
      var byNow = 0;
      var start = new Date();
      var halfThree = new Date();
      halfThree = new Date(halfThree.setHours(15,30,0,0));
      start = new Date (start.setHours(8,0,0,0));
      
      if(now > halfThree){
        byNow = target;
      }
      
      if(now >= start && now <= halfThree){
        var portionOfDay = now - start;
        var portionOfDayFormatted = new Date(portionOfDay);
        var portionOfDayMinutes = (portionOfDayFormatted.getHours() * 60);
        var minutes = portionOfDayFormatted.getMinutes();
        var cal = (portionOfDayMinutes + minutes) / 510;
        byNow = target * cal;
      }
      
      if (byNow > target){
        byNow = target;
      }
      
      return $filter("number")(byNow, 0);
      
    }
  }

})();
