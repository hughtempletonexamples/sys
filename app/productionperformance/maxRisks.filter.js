(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .filter("maxRisks", filter);

  function filter() {

    return maxRisksFilter;

    function maxRisksFilter(projects, productionLine) {
      
      if (angular.isUndefined(projects) || projects.length === 0){
        return null;
      }
      
      var maxRisksArrays = {};
      var maxRisks = {};
      
      angular.forEach(projects, function (project){
        angular.forEach(project.allProjectRisks, function (risk, productionLine){
          if (angular.isUndefined(maxRisksArrays[productionLine])){
            maxRisksArrays[productionLine] = [];
          }
          maxRisksArrays[productionLine].push(risk);
        });
      });
      
      maxRisks = lowestRisk(maxRisksArrays);
      
      return maxRisks[productionLine];
      
      
      function lowestRisk(entryOb) {
        var toFillOb = {};

        angular.forEach(entryOb, function (risks, productionLine) {
          var lowest = Number.POSITIVE_INFINITY;
          var tmp;
          for (var i = risks.length - 1; i >= 0; i--) {
            tmp = risks[i];
            if (tmp < lowest) {
              lowest = tmp;
            }
          }
          if (angular.isUndefined(toFillOb[productionLine])) {
            toFillOb[productionLine] = lowest;
          }
        });
        return toFillOb;
      }

    }
    
    
  }

})();
