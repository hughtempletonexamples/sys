(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/productionperformance", {
        templateUrl: require("./productionperformance.html"),
        controller: "ProductionPerformanceController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("performance");
  }

})();
