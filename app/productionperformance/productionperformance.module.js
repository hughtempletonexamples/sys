(function () {
  "use strict";

  angular
    .module("innView.productionperformance", [
      "ngRoute",
      "firebase",
      "floatThead",
      "innView.core",
      "innView.projects"
    ]);

})();
