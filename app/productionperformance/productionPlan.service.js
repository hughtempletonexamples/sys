(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .factory("productionPlanService", productionPlanService);

  /**
   * @ngInject
   */
  function productionPlanService($log, $q, dateUtil, referenceDataService, plannerService, areasService, projectsService) {

    var service = {
      getGreatestRisks: getGreatestRisks
    };
    
    var suppliers = referenceDataService.getSuppliers();
    var productTypes = referenceDataService.getProductTypes();
    
    var thirdPartySuppliers;
    var productTypeProductionLine;

    var projects = {};
   
    return service;

    ////////////

    function getGreatestRisks() {
      
      var risks = {};
      var today = new Date();

      return projectsService.getProjects()  
        .$loaded()
        .then(function (proj) {
          thirdPartySuppliers = buildMap(suppliers, "thirdParty");
          productTypeProductionLine = buildMap(productTypes, "productionLine");
          projects = buildActiveProjectsMap(proj);
          var promises = [];
          angular.forEach(productTypeProductionLine, function (productionLine) {
            if (angular.isDefined(productionLine) && angular.isUndefined(risks[productionLine])){
              promises.push(
                getGreatestRiskForProductionLine(productionLine, today)
                .then(function (risk){
                  risks[productionLine] = risk;
                })
              );
            }
          });
          return $q.all(promises);
        })
        .then(function () {
          return risks;
        })
        .catch(function (error) {
          $log.log("Unable to process max risks: " + angular.toJson(error));
        });

      function buildMap(items, property) {
        var map = {};
        angular.forEach(items, function (item) {
          map[item.$id] = item[property];
        });
        return map;
      }

      function buildActiveProjectsMap(items, property) {
        var map = {};
        angular.forEach(items, function (item) {
          if(item.active){
            map[item.$id] = item;
          }
        });
        return map;
      }
    }

    function getGreatestRiskForProductionLine(productionLine, today) {

      var risks = [];
      return plannerService.getProductionPlansByPlannedStart()
        .$loaded()
        .then(function (plans) {
          var promises = [];
          angular.forEach(plans, function (plan) {
            var project = projects[plan.project];
            promises.push(
              areasService.getProjectArea(plan.$id)
              .$loaded()
              .then(function (area) {
                if (angular.isDefined(project) && productTypeProductionLine[area.type] === productionLine) {
                  var plannedFinish = new Date(plan.plannedFinish);
                  if (plannedFinish >= today && !thirdPartySuppliers[area.supplier] && (!area.completedPanels || area.completedPanels < area.actualPanels)) {
                    var areaRiskDays = dateUtil.daysBetweenWeekDaysOnly(plan.plannedFinish, plan.riskDate);
                    risks.push(areaRiskDays);
                  }
                }
              })
              );
          });
          return $q.all(promises);
        })
        .then(function () {
          var maxRisks = lowestRisk(risks);
          return maxRisks;
        })
        .catch(function (error) {
          $log.log("Unable to process max risks for " + productionLine + ": " + angular.toJson(error));
        });

      function lowestRisk(entryArr) {
        if(entryArr.length === 0){
          return 0;
        }
        var lowest = Number.POSITIVE_INFINITY;
        var tmp;
        for (var i = entryArr.length - 1; i >= 0; i--) {
          tmp = entryArr[i];
          if (tmp < lowest) {
            lowest = tmp;
          }
        }
        return lowest;
      }
    }
  }
})();