(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .factory("productionperformanceService", productionperformanceService);

  /**
   * @ngInject
   */
  function productionperformanceService($log, $firebaseObject, schema, functionsService) {
    
    var map = {};
    var date = new Date();
    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;

    var productTypes = schema.getObject(schema.getRoot().child("srd").child("productTypes"));
    
    return $firebaseObject.$extend({
      
      setDate: function (d) {
        date = d;
        this._calculatedTotal = calculate(this, date);
      },

      $$updated: function () {
        var changed = $firebaseObject.prototype.$$updated.apply(this, arguments);
        this._calculatedTotal = calculate(this, date);
        return changed;
      }
    });

    function calculate(panels, date) {

      angular.forEach(productTypes, function (item, key) {
        map[key] = item["productionLine"];
      });

      date = new Date(date);
      
      var calculatedTotal = createDataHolder();

      var start = date.setHours(0, 0, 0, 0);
      var end = date.setHours(23, 59, 59, 999);

      angular.forEach(panels, function (panel) {
        var type = panel.type;
        var panelType = null;
        var idParts = PANEL_ID_REGEX.exec(panel.id);
        if (idParts) {
          panelType = idParts[5];
        }
        var line = map[type];
        if (angular.isDefined(panel.qa)) {
          if (angular.isDefined(panel.qa["completed"])) {
            if (panel.qa["completed"] >= start && panel.qa["completed"] <= end) {
              calculatedTotal.area[line] += panel.dimensions.area;
              calculatedTotal.completed[line]++;
            }
          } else if (angular.isDefined(panel.qa["started"])) {
            if (panel.qa["started"] < end) {
              calculatedTotal.started[line]++;
            }
          }
        }
      });
      return calculatedTotal;
    }
    
    function createDataHolder(){
      var calculatedTotal = {
        completed : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 },
        started : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 },
        area : { "SIP" : 0, "HSIP" : 0, "IFAST" : 0, "TF" : 0, "CASS" : 0 }
      };
      return calculatedTotal;
    }
    
  }
})();
