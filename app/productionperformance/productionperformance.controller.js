(function () {
  "use strict";

  angular
    .module("innView.productionperformance")
    .controller("ProductionPerformanceController", ProductionPerformanceController);
  
  /**
   * @ngInject
   */
  function ProductionPerformanceController(schema, $filter, $interval, $scope, dateUtil, productionperformanceService, referenceDataService, productionPlanService) {
    var vm = this;

    var panelsRef = {};
    var productionCapacitiesRef = {};

    vm.colourIndicator = colourIndicator;
    vm.performanceIndicator = performanceIndicator;
    vm.changeDate = changeDate;
    vm.now = new Date();
    vm.selectDate = dateUtil.toIsoDate(new Date());
    vm.limitView = false;
    
    $scope.$on("$destroy", destroyPerformanceData);
    
    var nowDate = $interval(function () {
      vm.now = new Date();
    }, 60000);

    init();

    function init(){
      getProLines();
      panelsRef = schema.getRoot().child("panels");
      productionCapacitiesRef = schema.getRoot().child("productionCapacities");
      vm.productionCapacities = schema.getObject(productionCapacitiesRef);
      vm.panels = new productionperformanceService(panelsRef);
      
      productionCapacitiesRef.on("value", function () {
        getRisks();
      });

    }

    function changeDate(){
      vm.panels.setDate(vm.selectDate);
    }
    
    function getProLines() {
      referenceDataService.getProductionLines()
        .$loaded()
        .then(function (productionLines) {
          vm.defaultCapacities = buildMap(productionLines, "defaultCapacity");
        });
    }
    
    function getRisks(){
      productionPlanService.getGreatestRisks()
        .then(function (areaPlans) {
          vm.areasPlans = areaPlans;
        });
    }
    
    function performanceIndicator(value1, byNow, capacity, defaultCapacity){
      
      var colours = [];
      colours.push({"red-indicator": true});
      colours.push({"amber-indicator": true});
      colours.push({"green-indicator": true});
      colours.push({"purple-indicator": true});
      colours.push({"white-indicator": true});
      

      if (angular.isDefined(value1) && angular.isDefined(byNow)) {
        
        byNow = $filter("byNow")(byNow, capacity, defaultCapacity);

        var percent = (value1 * 100) / parseInt(byNow);

        if (percent > 1 && percent < 90) {
          return "red-indicator";
        } else if (percent >= 90 && percent < 100) {
          return "amber-indicator";
        } else if (percent >= 100 && percent < 105) {
          return "green-indicator";
        } else if (percent >= 105) {
          return "purple-indicator";
        }
        
      }
      
    }
  
    
    function colourIndicator(value, entry1, entry2, entry3){
      
      var colours = [];
      colours.push({"red-indicator": true});
      colours.push({"amber-indicator": true});
      colours.push({"green-indicator": true});
      colours.push({"white-indicator": true});
      
      var now = new Date();
      var three = new Date();
      three.setHours(15,0,0,0);
      three = new Date(three);
      
      if(now > three){
        if(angular.isDefined(value)){
          if (value >= entry3) {
            return "green-indicator";   
          } else if (value >= entry2) {
            return "amber-indicator";
          } else if (value <= entry1) {
            return "red-indicator";
          }
        }
      }else{
        return colours[3];
      }
      
    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }

    function destroyPerformanceData() {
      if (vm.panels && vm.panels.$destroy) {
        vm.panels.$destroy();
        delete vm.panels;
      }
      if (vm.productionCapacities && vm.productionCapacities.$destroy) {
        vm.productionCapacities.$destroy();
        delete vm.productionCapacities;
      }
      productionCapacitiesRef.off();
      $interval.cancel(nowDate);
    }
  }

})();
