(function () {
  "use strict";

  angular.module("innView", [
    "ngRoute",
    "ngAnimate",
    "ngMaterial",
    "ngMessages",
    "firebase",
    "nvd3",
    "ngMaterialDatePicker",
    "innView.core",
    "innView.login",
    "innView.home",
    "innView.projects",
    "innView.dashboard",
    "innView.manage",
    "innView.admin",
    "innView.productionperformance",
    "innView.materialrequests",
    "innView.materialanalysis",
    "innView.measures"
  ]).config(function ($mdThemingProvider, $mdDateLocaleProvider, $mdAriaProvider) {
    $mdThemingProvider.theme("default")
      .primaryPalette("purple", {
        "default": "500",
        "hue-1": "100",
        "hue-2": "600",
        "hue-3": "A100"
      })
      .accentPalette("teal", {
        "default": "500"
      });
    $mdAriaProvider.disableWarnings();
    $mdDateLocaleProvider.firstDayOfWeek = 1;
    $mdDateLocaleProvider.formatDate = function (date) {
      if (angular.isDefined(date)) {
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return (day < 10 ? "0" + day : day) + "/" + (monthIndex + 1) + "/" + year;
      }
      return "";
    };
  })
    .filter("toDate", function () {
      function toDateFilter(dateString) {
        return new Date(dateString);
      }

      return toDateFilter;
    })
    .filter("sameDateAsDatepicker", function () {
      function toDateFilter(dateString) {
        if (angular.isDefined(dateString)) {
          var date = new Date(dateString);
          var day = date.getDate();
          var monthIndex = date.getMonth();
          var year = date.getFullYear();

          return (day < 10 ? "0" + day : day) + "/" + (monthIndex + 1) + "/" + year;
        }
        return "";
      }

      return toDateFilter;
    })
    .filter("ordinalDate", function ($filter) {
      //See http://www.michaelbromley.co.uk/blog/13/an-ordinal-date-filter-for-angularjs for details
      var getOrdinalSuffix = function (number) {
        var suffixes = ["'th'", "'st'", "'nd'", "'rd'"];
        var relevantDigits = (number < 30) ? number % 20 : number % 30;
        return "d" + ((relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0]);
      };

      return function (timestamp, format) {
        var regex = /d+((?!\w*(?=")))|d$/g;
        var date = new Date(timestamp);
        var dayOfMonth = date.getDate();
        var suffix = getOrdinalSuffix(dayOfMonth);

        format = format.replace(regex, function (match) {
          return match === "d" ? suffix : match;
        });
        return $filter("date")(date, format);
      };
    });
})();
