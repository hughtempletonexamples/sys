(function () {
  "use strict";

  angular
    .module("innView.home")
    .controller("HomeController", HomeController);

  /**
   * @ngInject
   */
  function HomeController($location, projectsService) {
    var vm = this;

    vm.selectProject = selectProject;

    vm.projects = [];

    init();

    ////////////

    function init() {
      vm.projects = projectsService.getProjects();
    }

    function selectProject(projectId) {
      $location.url("/projects/" + projectId);
    }
  }
})();
