(function () {
  "use strict";

  angular
    .module("innView.measures")
    .factory("measuresService", measuresService);

  /**
   * @ngInject
   */
  function measuresService($log, $q, schema, firebaseUtil, dateUtil, referenceDataService, functionsService) {
    
    var lineCapacities = {};
    var productTypeProductionLine = {};
    var defaultLineCapacity = {};
    var capacityAccumulation = {};
    var statsMap = {};

    var projects = getProjects();
    var areas = getAreas();
    var panels = getPanels();
    
    var graphData = {};
    
    var initialised = false;
    
    var thisMonday = null;
    var firstDay = null;
    var lastMonday = null;
    var lastSaturday = null;
    var lastDay = null;
    
    initialise();


    var weeks = [];

    var service = {
      generateData : generateData,
      designedIssued: designedIssued,
      syncStats: syncStats
    };

    return service;
    
    ////////////
    
    function initialise() {
      if (initialised) {
        return $q.resolve();
      }
      else {
        return init();
      }

      function init() {
        var productTypes = referenceDataService.getProductTypes();
        var productionLines = referenceDataService.getProductionLines();

        return firebaseUtil.loaded(productTypes, productionLines)
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
            defaultLineCapacity = buildMap(productionLines, "defaultCapacity");
          })
          .then(function () {
            var deferredList = [];
            var productionTotalCapacities = schema.getRoot().child("productionCapacities");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var lineTotalCapacity = schema.getObject(productionTotalCapacities.child(productionLineKey));
              lineCapacities[productionLineKey] = lineTotalCapacity;

              deferredList.push(lineTotalCapacity.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            initialised = true;
          });

        function buildMap(items, property) {
          var map = {};
          angular.forEach(items, function (item) {
            map[item.$id] = item[property];
          });
          return map;
        }
      }
    }
    
    function syncStats() {
      
      var firstDay = new Date(new Date("2015-12-12").setHours(0, 0, 0, 0));
      var lastDay = new Date(new Date().setHours(23, 59, 59, 999));   
      
      var matchingAreas = {};
      var areaUnpublishedDate = [];
      var productTypeProductionLine = {};

      var panData = {};
      
      var matchingPanels = {};

      var areaDelivery = {};
      var projectPublished = {};
      var projectUnpublished = {};
      
      var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;

      var complete = {
        "SIP": 0,
        "HSIP": 0,
        "IFAST": 0,
        "TF": 0,
        "CASS": 0
      };
      var designed = {
        "SIP": 0,
        "HSIP": 0,
        "IFAST": 0,
        "TF": 0,
        "CASS": 0
      };

      var deferred = $q.defer();

      var projects = schema.getArray(schema.getRoot().child("projects"));
      projects.$loaded()
        .then(processProjects)
        .then(getProductTypes)
        .then(aggregatePanels)
        .then(processPanels)
        .then(buildAndSave)
        .then(function (allPanelsData) {
          deferred.resolve(allPanelsData);
        });

      return deferred.promise;

      function processProjects(projects) {
        var promises = [];
        angular.forEach(projects, function (project) {
          if (angular.isUndefined(projectPublished[project.$id])) {
            projectPublished[project.$id] = project.deliverySchedule.published;
          }
          if (angular.isUndefined(projectUnpublished[project.$id])) {
            projectUnpublished[project.$id] = project.deliverySchedule.unpublished;
          }
          promises.push(findAreasWithMatchingDeliveryDate(project));
        });
        return $q.all(promises);
      }

      function findAreasWithMatchingDeliveryDate(project) {
        return schema.getArray(schema.getRoot().child("areas").orderByChild("project").equalTo(project.$id))
          .$loaded()
          .then(function (projectAreas) {
            angular.forEach(projectAreas, function (area) {
              if (angular.isUndefined(areaDelivery[area.$id])) {
                areaDelivery[area.$id] = area.revisions[projectPublished[area.project]] || area.revisions[projectUnpublished[area.project]];
              }
              matchingAreas[area.$id] = area;
              areaUnpublishedDate[area.$id] = project.deliverySchedule.published;
            });
          });
      }
      
      function getProductTypes(){
        var productTypes = referenceDataService.getProductTypes();
        productTypes
          .$loaded()
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
          });
      }

      function aggregatePanels() {
        var promises = [];
        angular.forEach(matchingAreas, function (area, areaRef) {
          promises.push(
            schema.getArray(schema.getRoot().child("panels").orderByChild("area").equalTo(area.$id))
              .$loaded()
              .then(function (panels) {
                angular.forEach(panels, function (panel) {
                  if(angular.isUndefined(matchingPanels[panel.$id])){
                    matchingPanels[panel.$id] = panel;
                  }                 
                });
              })
          );
        
        });
        return $q.all(promises);
      }
    
      function processPanels() {
        angular.forEach(matchingPanels, function ( panel ) {
          var delivery = areaDelivery[panel.area];
          var type = panel.type;
          var panelType = null;
          var idParts = PANEL_ID_REGEX.exec(panel.id);
          if (idParts) {
            panelType = idParts[5];
          }
          type = functionsService.uploaderLogicHSIP(type, panelType);
          var line = productTypeProductionLine[type];
          if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa.started)) {
            var panDate = new Date(panel.qa.started);
            var formatDate = dateUtil.toIsoDate(panel.qa.started);
            if (angular.isUndefined(panData[line])) {
              panData[line] = {};
            }
            if (angular.isUndefined(panData[line][formatDate])) {
              panData[line][formatDate] = {
                "startedCount": 0,
                "startedM2": 0,
                "completedCount": 0,
                "completedM2": 0,
                "designedCount": 0,
                "designedM2": 0,
                "deliveryCount": 0,
                "deliveryM2": 0
              };
            }
            if (panDate > firstDay && panDate < lastDay) {
              panData[line][formatDate].startedM2 += panel.dimensions.area;
              panData[line][formatDate].startedCount++;
            }
          }
          if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa.completed)) {
            var panDate = new Date(panel.qa.completed);
            var formatDate = dateUtil.toIsoDate(panel.qa.completed);
            if (angular.isUndefined(panData[line])) {
              panData[line] = {};
            }
            if (angular.isUndefined(panData[line][formatDate])) {
              panData[line][formatDate] = {
                "startedCount": 0,
                "startedM2": 0,
                "completedCount": 0,
                "completedM2": 0,
                "designedCount": 0,
                "designedM2": 0,
                "deliveryCount": 0,
                "deliveryM2": 0
              };
            }
            if(panDate > firstDay && panDate < lastDay){
              panData[line][formatDate].completedM2 += panel.dimensions.area;
              panData[line][formatDate].completedCount++;
              complete[line]++;
              if (angular.isDefined(panel.qa) && angular.isUndefined(panel.qa.started)) {
                panData[line][formatDate].startedM2 += panel.dimensions.area;
                panData[line][formatDate].startedCount++;
              }
            }
          }
          if (angular.isDefined(panel.timestamps.created)) {
            var panDate = new Date(panel.timestamps.created);
            var formatDate = dateUtil.toIsoDate(panel.timestamps.created);
            if (angular.isUndefined(panData[line])) {
              panData[line] = {};
            }
            if (angular.isUndefined(panData[line][formatDate])) {
              panData[line][formatDate] = {
                "startedCount": 0,
                "startedM2": 0,
                "completedCount": 0,
                "completedM2": 0,
                "designedCount": 0,
                "designedM2": 0,
                "deliveryCount": 0,
                "deliveryM2": 0
              };
            }
            if (panDate > firstDay && panDate < lastDay) {
              panData[line][formatDate].designedM2 += panel.dimensions.area;
              panData[line][formatDate].designedCount++;
              designed[line]++;
            }
          }
          if (angular.isDefined(delivery)) {
            var panDate = new Date(delivery);
            var formatDate = dateUtil.toIsoDate(panDate);
            if (angular.isUndefined(panData[line])) {
              panData[line] = {};
            }
            if (angular.isUndefined(panData[line][formatDate])) {
              panData[line][formatDate] = {
                "startedCount": 0,
                "startedM2": 0,
                "completedCount": 0,
                "completedM2": 0,
                "designedCount": 0,
                "designedM2": 0,
                "deliveryCount": 0,
                "deliveryM2": 0
              };
            }
            if (panDate > firstDay && panDate < lastDay) {
              panData[line][formatDate].deliveryM2 += panel.dimensions.area;
              panData[line][formatDate].deliveryCount++;
            }
          }
        });
      }
      
      function buildAndSave(){
        angular.forEach(panData, function ( theData, productionLine ) {
          angular.forEach(theData, function ( data, date ) {
            data.startedM2 = Math.round(data.startedM2 * 10) / 10;
            data.completedM2 = Math.round(data.completedM2 * 10) / 10;
            data.designedM2 = Math.round(data.designedM2 * 10) / 10;
            data.deliveryM2 = Math.round(data.deliveryM2 * 10) / 10;
          });
        });

        return schema.getRoot().child("stats").set(panData);
      }
      
    }
    
    
    function designedIssued() {
      
      var deferred = $q.defer();
      
      var statsMap = {};
      var designIssued = {
        "type": {},
        "totalCompleteCount": 0,
        "totalDesignedCount": 0,
        "totalCompleteM2": 0,
        "totalDesignedM2": 0
      };
      
      var productionLines = referenceDataService.getProductionLines();

      productionLines
        .$loaded()
        .then(function () {
          var deferredList = [];
          var statsRef = schema.getRoot().child("stats");
          angular.forEach(productionLines, function ( productionLine ) {
            var productionLineKey = productionLine.$id;
            var statsData = schema.getObject(statsRef.child(productionLineKey));
            statsMap[productionLineKey] = statsData;
            deferredList.push(statsData.$loaded());
          });
          return $q.all(deferredList);
        })
        .then(function () {
          angular.forEach(statsMap, function (data, productionLine){
            angular.forEach(data, function (stats, date){
              if (angular.isUndefined(designIssued.type[productionLine])) {
                designIssued.type[productionLine] = {
                  "designedM2": 0,
                  "designedCount": 0,
                  "deliveryM2": 0,
                  "deliveryCount": 0,
                  "completedM2": 0,
                  "completedCount": 0
                };
              }
              designIssued.type[productionLine].completedM2 += stats.completedM2 || 0;
              designIssued.type[productionLine].completedCount += stats.completedCount || 0;
              designIssued.type[productionLine].designedM2 += stats.designedM2 || 0;
              designIssued.type[productionLine].designedCount += stats.designedCount || 0;
              designIssued.type[productionLine].deliveryM2 += stats.deliveryM2 || 0;
              designIssued.type[productionLine].deliveryCount += stats.deliveryCount || 0;
              
              designIssued.totalCompleteM2 += stats.completedM2 || 0;
              designIssued.totalCompleteCount += stats.completedCount || 0;
              designIssued.totalDesignedM2 += stats.designedM2 || 0;
              designIssued.totalDesignedCount += stats.designedCount || 0;
            });

          });
          deferred.resolve(designIssued);
        });
        
      return deferred.promise;

    }


    function generateData(isTest, weeksAmount, dayzGone) {
      
      var deferred = $q.defer();

      var panData = {};

      var areaDelivery = {};
      var projectPublished = {};
      var projectUnpublished = {};
      var deliveryBufferAllCounts = [];
      var designedBufferAllCounts = [];
      
      weeks = getWeeks(weeksAmount, dayzGone);
      thisMonday = dateUtil.weekStart();
      firstDay = new Date(weeks[0]);
      lastMonday = weeks[weeks.length - 1];
      lastSaturday = dateUtil.plusDays(lastMonday, 5);
      lastDay = new Date(lastSaturday);

      firstDay.setHours(0, 0, 0, 0);
      lastDay.setHours(23, 59, 59, 999);

      initialise()
        .then(function () {
          
          capacityAccumulation = buildDesignIssued();
          graphData = refreshGraphData();
          panData = buildPanData();
          
          return firebaseUtil.loaded(projects, areas, panels);
        })
        .then(function () {

          angular.forEach(projects, function (project) {
            if (angular.isUndefined(projectPublished[project.$id])) {
              projectPublished[project.$id] = project.deliverySchedule.published;
            }
            if (angular.isUndefined(projectUnpublished[project.$id])) {
              projectUnpublished[project.$id] = project.deliverySchedule.unpublished;
            }
          });

          angular.forEach(areas, function (area) {
            if (angular.isUndefined(areaDelivery[area.$id])) {
              areaDelivery[area.$id] = area.revisions[projectPublished[area.project]] || area.revisions[projectUnpublished[area.project]];
            }
          });

          angular.forEach(panels, function (panel) {
            var delivery = areaDelivery[panel.area];
            var type = productTypeProductionLine[panel.type];
            if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa.completed)) {
              var panDate = new Date(panel.qa.completed);
              var formatDate = dateUtil.toIsoDate(panel.qa.completed, "dd-mm-yyyy");
              var mondayOfWeek = dateUtil.weekStart(formatDate);

              if (panDate > firstDay && panDate < lastDay) {
                panData[type][mondayOfWeek].manufacturedAreaM2 += panel.dimensions.area;
                panData[type][mondayOfWeek].manufacturedCount++;
              }
            }
            if (angular.isDefined(panel.timestamps.created)) {
              var panDate = new Date(panel.timestamps.created);
              var formatDate = dateUtil.toIsoDate(panel.timestamps.created, "dd-mm-yyyy");
              var mondayOfWeek = dateUtil.weekStart(formatDate);

              if (panDate > firstDay && panDate < lastDay) {
                panData[type][mondayOfWeek].designedAreaM2 += panel.dimensions.area;
                panData[type][mondayOfWeek].designedCount++;
              }
            }
            if (angular.isDefined(delivery)) {
              var panDate = new Date(delivery);
              var mondayOfWeek = dateUtil.weekStart(delivery);

              if (panDate > firstDay && panDate < lastDay) {
                panData[type][mondayOfWeek].deliveryAreaM2 += panel.dimensions.area;
                panData[type][mondayOfWeek].deliveryCount++;
              }
            }
          });

          angular.forEach(panData, function (datedData, type) {
            angular.forEach(datedData, function (datedData, week) {
              angular.forEach(panels, function (panel) {
                var delivery = areaDelivery[panel.area];
                var mondayOfWeek = new Date(week);
                
                var panType = productTypeProductionLine[panel.type];
                if (angular.isDefined(panel.qa) && angular.isDefined(panel.qa.completed)) {
                  var panDate = new Date(panel.qa.completed);
                  if (panDate < mondayOfWeek && panType === type) {
                    panData[type][week].beforeManufacturedAreaM2 += panel.dimensions.area;
                    panData[type][week].beforeManufacturedCount++;
                  }
                }
                if (angular.isDefined(panel.timestamps.created)) {
                  var panDate = new Date(panel.timestamps.created);
                  if (panDate < mondayOfWeek && panType === type) {
                    panData[type][week].beforeDesignedAreaM2 += panel.dimensions.area;
                    panData[type][week].beforeDesignedCount++;
                  }
                }
                if (angular.isDefined(delivery)) {
                  var panDate = new Date(delivery);
                  if (panDate < mondayOfWeek && panType === type) {
                    panData[type][week].beforeDeliveryAreaM2 += panel.dimensions.area;
                    panData[type][week].beforeDeliveryCount++;
                  }
                }
              });
            });
          });

          angular.forEach(panData, function (datedDat, type) {
            angular.forEach(datedDat, function (datedData, week) {
              var weekDate = Date.parse(week);
              if (isTest){
                weekDate = dateUtil.toIsoDate(week);
              }
              if (angular.isDefined(weekDate)) {

                var weekFromatted = new Date(week);
                var thisMondayFromatted = new Date(thisMonday);

                if (weekFromatted > thisMondayFromatted) {
                  var weeklySum = datedData.designedCount - datedData.capacityAcc;
                  var weeklyDeliverySum = datedData.capacityAcc - datedData.deliveryCount;
                } else {
                  var weeklySum = datedData.designedCount - datedData.manufacturedCount;
                  var weeklyDeliverySum = datedData.manufacturedCount - datedData.deliveryCount;
                }

                designedBufferAllCounts.push(datedData.designedCount);
                designedBufferAllCounts.push(datedData.capacity);
                designedBufferAllCounts.push(datedData.manufacturedCount);
                deliveryBufferAllCounts.push(datedData.deliveryCount);
                deliveryBufferAllCounts.push(datedData.capacity);
                deliveryBufferAllCounts.push(datedData.manufacturedCount);

                var weeklySumBefore = datedData.beforeDesignedCount - datedData.beforeManufacturedCount;
                var weeklyDeliveryBefore = datedData.beforeManufacturedCount - datedData.beforeDeliveryCount;

                var buffer = weeklySumBefore + weeklySum;
                var buffer2 = weeklyDeliveryBefore + weeklyDeliverySum;
                
                if (angular.isDefined(datedData.designedCount)) {
                  graphData.graphData1[type].push({x: weekDate, y: datedData.designedCount});
                }
                if (angular.isDefined(buffer)) {
                  graphData.graphData2[type].push({x: weekDate, y: buffer});
                }
                if (angular.isDefined(datedData.manufacturedCount)) {
                  graphData.graphData3[type].push({x: weekDate, y: datedData.manufacturedCount});
                }
                if (angular.isDefined(datedData.capacity)) {
                  graphData.graphFutureData3[type].push({x: weekDate, y: datedData.capacity});
                }
                if (angular.isDefined(buffer2)) {
                  graphData.graphData4[type].push({x: weekDate, y: buffer2});
                }
                if (angular.isDefined(datedData.deliveryCount)) {
                  graphData.graphData5[type].push({x: weekDate, y: datedData.deliveryCount});
                }
              }
            });
          });

          graphData.maxDesignedBuffer = designedBufferAllCounts.reduce(function (a, b) {
            return Math.max(a, b);
          });

          graphData.maxDeliveryBuffer = deliveryBufferAllCounts.reduce(function (a, b) {
            return Math.max(a, b);
          });
          
          return graphData;
          
        })
        .then(function (graphData) {
          deferred.resolve(graphData);
        });
        
      return deferred.promise;

    }

    function refreshGraphData(){
      var graphData = {};
      graphData.graphData1 = buildGraphData();
      graphData.graphData2 = buildGraphData();
      graphData.graphData3 = buildGraphData();
      graphData.graphFutureData3 = buildGraphData();
      graphData.graphData4 = buildGraphData();
      graphData.graphData5 = buildGraphData();
      graphData.maxDesignedBuffer = 0;
      graphData.maxDeliveryBuffer = 0;
      return graphData;
    }
    
    function buildPanData() {
      var dataHolder = {};
      angular.forEach(defaultLineCapacity, function (dafaultCapacity, type) {
        capacityAccumulation[type] = 0;
        angular.forEach(weeks, function (week) {
          if (angular.isUndefined(dataHolder[type])) {
            dataHolder[type] = {};
          }
          if (angular.isUndefined(dataHolder[type][week])) {
            dataHolder[type][week] = {
              "designedAreaM2": 0,
              "designedCount": 0,
              "deliveryAreaM2": 0,
              "deliveryCount": 0,
              "manufacturedAreaM2": 0,
              "manufacturedCount": 0,
              "beforeDeliveryAreaM2": 0,
              "beforeDeliveryCount": 0,
              "beforeDesignedAreaM2": 0,
              "beforeDesignedCount": 0,
              "beforeManufacturedAreaM2": 0,
              "beforeManufacturedCount": 0,
              "capacity": 0,
              "capacityAcc": 0
            };
          }
          for (var i = 0; i <= 5; i++) {
            var dayOfWeek = dateUtil.plusDays(week, i);
            if (angular.isDefined(lineCapacities[type][dayOfWeek])) {
              var capacity = lineCapacities[type][dayOfWeek];
            } else {
              if(dateUtil.isWeekDay(dayOfWeek)){
                var capacity = dafaultCapacity;
              }else{
                var capacity = 0;
              }
            }
            dataHolder[type][week].capacity += capacity;
            var weekFromatted = new Date(week);
            var thisMondayFromatted = new Date(thisMonday);
            if (weekFromatted > thisMondayFromatted) {
              capacityAccumulation[type] += capacity;
            }
          }
          dataHolder[type][week].capacityAcc = capacityAccumulation[type];
        });
      });
      return dataHolder;
    }
    
    function buildGraphData(){
      var dataHolder = {};
      angular.forEach(defaultLineCapacity, function (dafaultCapacity, type) {  
        if(angular.isUndefined(dataHolder[type])){
          dataHolder[type] = [];
        }
      });
      return dataHolder;
    }
    
    function buildDesignIssued(){
      var dataHolder = {};
      angular.forEach(defaultLineCapacity, function (dafaultCapacity, type) {  
        if(angular.isUndefined(dataHolder[type])){
          dataHolder[type] = 0;
        }
      });
      return dataHolder;
    }
    
    function getWeeks(weeksAmount, dayzGone) {
      var weekHolder = [];
      var monday = dateUtil.weekStart();
      var firstMonday = dateUtil.minusDays(monday, dayzGone);
      weekHolder.push(firstMonday);
      for (var j = 1; j <= weeksAmount; j++) {
        firstMonday = dateUtil.plusDays(firstMonday, 7);
        weekHolder.push(firstMonday);
      }
      return weekHolder;
    }
    
    function getProjects(){
      var projects = schema.getRoot().child("projects");
      return schema.getArray(projects);
    }

    function getPanels(){
      var panels = schema.getRoot().child("panels");
      return schema.getArray(panels);
    }

    function getAreas(){
      var areas = schema.getRoot().child("areas");
      return schema.getArray(areas);
    }


  }
  
  function buildMap( items, property ) {
    var map = {};
    angular.forEach(items, function ( item ) {
      map[item.$id] = item[property];
    });
    return map;
  }
  
})();
