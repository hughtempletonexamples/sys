(function () {
  "use strict";

  require("./measures.module");

  require("./measures.controller");
  
  require("./designIssued.controller");
  
  require("./measures.service");
  
  require("./forecast.controller");
  
  require("./forecast.service");
  
  require("./forecast-bar.controller");
  
  require("./forecast-bar.service");
  
  require("./resource.controller");
  
  require("./resource.service");
  
  require("./product.controller");
  
  require("./product.service");
    
  require("./config.route");

})();
