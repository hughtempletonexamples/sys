(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("ResourceController", ResourceController);

  /**
   * @ngInject
   */
  function ResourceController($log, $q, $scope, $routeParams, $uibModal, $timeout, $confirm, $window, dateUtil, plannerService, productionPlanService) {

    var vm = this;
    var listener = null;
    var modifyChartTimeout;
    vm.areasSelection = {};
    vm.lineBenches = {};
    vm.loadPlan = loadPlan;
    vm.hideHolder = false;
    vm.includeCapacity = true;

    $scope.$on("$viewContentLoaded", function (){
      loadPlan($routeParams.start || null);
    });

    $scope.$on("$destroy", destroyPlan);
    
    var staticAverages = {};
    staticAverages["SIP"] = 0;
    staticAverages["SIP2"] = 0;
    staticAverages["HSIP"] = 0;
    staticAverages["IFAST"] = 0;
    staticAverages["TF"] = 0;
    staticAverages["TF2"] = 0;
    staticAverages["CASS"] = 0;
    
    vm.resourceChartAPI = {};
    vm.options = {};
    
    angular.forEach(staticAverages, function (averages, productionLine) {
      vm.options[productionLine] = {
        chart:{
          type: "multiChart",
          height: 500,
          margin: {
            top: 20,
            right: 20,
            bottom: 30,
            left: 60
          },
          transitionDuration: 5,
          useInteractiveGuideline: false,
          interactive: true,
          tooltips: true,
          showControls: false,
          showLegend: false,
          bars1: {
            stacked: true,
            dispatch: {   
              elementMouseover: function (e) {
                barMouseOver(e, productionLine, 1);
              },
              elementClick: function (d) {
                setWeekCapacity("md", d, productionLine);
              }
            }
          },
          bars2: {
            stacked: true,
            dispatch: {   
              elementMouseover: function (e) {
                barMouseOver(e, productionLine, 2);
              },
              elementClick: function (d) {
                setWeekCapacity("md", d, productionLine);
              },
              renderEnd: function (e) {
                modifyChart(productionLine);
              }
            }
          },
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format("%b-%d")(new Date(d));
            }
          },
          yAxis1: {
            axisLabel: "m2",
            tickFormat: function (d) {
              return d3.format(",.1f")(d);
            }
          },
          yAxis2: {
            tickFormat: function (d) {
              return d3.format(",.1f")(d);
            }
          }
        }
      };
    });
    
    angular.element($window).on("resize", function () {
      angular.forEach(staticAverages, function (averages, productionLine) {
        if (angular.isDefined(vm.resourceChartAPI[productionLine])) {
          vm.resourceChartAPI[productionLine].refreshWithTimeout(100);
        }
      });
    });
    
    function modifyChart(productionLineHolder) {

      modifyChartTimeout = $timeout(function () {
        
        var bar = d3.select(".bars2Wrap .nv-bar");
        
        if (bar[0][0] !== null) {
          var w2 = bar.attr("width") / 2;
          d3.selectAll(".bars1Wrap .nv-bar").style("width", w2);
          d3.selectAll(".bars2Wrap .nv-bar").style("width", w2);
          d3.selectAll(".bars2Wrap .nv-bar")[0].forEach(function ( d ) {
            d3.select(d).attr("x", w2);
          });
        }

        var productionLine = productionLineHolder;
        var theClass = "." + productionLine;

        var allRects = d3.select(theClass).selectAll("g.nv-group").selectAll("rect");
        var parent = d3.select(theClass).select(".nv-x.nv-axis");

        var weekPos = [];
        var areaColours = {};
 
        if (angular.isUndefined(areaColours[productionLine])){
          areaColours[productionLine] = [];
        }

        // drawing the line
        var svg = d3.select(theClass).select("svg");

        var width = $window.innerWidth;

        //remove all previous placements
        parent.selectAll("text").remove();
        
        // more space at the bottom
        svg.style("height", "550px");
        
        angular.forEach(allRects, function (rect) {
          angular.forEach(rect, function (path, key) {
            //sets class to estimated or actual
            var rectHolder = d3.select(path);
            rectHolder.attr("class", "rectLine");
          });
        });

        angular.forEach(allRects[0], function (path, key) {
          var rectHolder = d3.select(path);
          var transform = rectHolder.attr("transform");
          var rectWidth = rectHolder.attr("width");
          var halfWidth = rectWidth / 2;
          var splitted = transform.split(",");
          var transformX = ~~splitted [0].split("(")[1];
          var transformY = 0;      
          var dates = vm.chartData[productionLine][0].values;
          weekPos.push({
            "transformY": transformY,
            "transformX": transformX + halfWidth,
            "transformXBefore": transformX,
            "width": rectHolder.attr("width"),
            "date": dates[key].x,
            "style": "text-anchor: left;",
            "dy": ".71em",
            "y": 7
          });
        });
        
        var benchesForDay = [];
        var benchOutputTotal = [];
        var benchOutputs = [];
        var benchToSave = [];
        angular.forEach(weekPos, function (week) {
          var day = d3.time.format("%Y-%m-%d")(new Date(week.date));
          var bench = 0;
          var m2;
          benchOutputs[day] = [];
          var maxAxis1 = vm.maxDataDate[productionLine]["1"][day];
          //var maxAxis2 = vm.maxDataDate[productionLine]["2"][day];
          //var greatest = Math.max(maxAxis1, maxAxis2);
          if(angular.isDefined(vm.lineBenches[productionLine][day])){
            m2 = vm.lineBenches[productionLine][day].meterSquared;
          }else{
            m2 = vm.defaultBench[productionLine];
          }
          benchOutputTotal[day] = m2;
          var maxY = vm.maxData[productionLine] + benchOutputTotal[day] + 1;
          while (benchOutputTotal[day] <= maxY) {
            bench++;
            benchOutputTotal[day] += m2;
            if(benchOutputTotal[day] > maxAxis1){
              benchOutputs[day].push(benchOutputTotal[day]);
            }
          }
          benchesForDay.push(bench);
          benchToSave[day] = benchOutputs[day][0];
        });
        
        var benchCount = benchesForDay.reduce(function (a, b) {
          return Math.max(a, b);
        });
           
        //getLineCoords
        var cords = {};
        var benchOutputTotal = [];
        if(angular.isUndefined(cords[productionLine])){
          cords[productionLine] = [];
        }
        angular.forEach(weekPos, function (week) {
          var day = d3.time.format("%Y-%m-%d")(new Date(week.date));
          var bench = 0;
          var m2;
          if(angular.isDefined(vm.lineBenches[productionLine][day])){
            m2 = vm.lineBenches[productionLine][day].meterSquared;
          }else{
            m2 = vm.defaultBench[productionLine];
          }
          benchOutputTotal[day] = m2;
          for(var i = 0; i < benchCount; i++){
            bench++;
            if(benchOutputTotal[day] === benchToSave[day]){           
              var lineCords = {
                "x1": 0,
                "x2": 0,
                "y1": 0,
                "y2": 0
              };
              lineCords.x2 = Math.round(week.transformXBefore) + Math.round(week.width);
              lineCords.x1 = week.transformXBefore;
              lineCords.y1 = benchOutputTotal[day];
              lineCords.y2 = benchOutputTotal[day];
              drawLine("line", 20, lineCords.x1, lineCords.x2, lineCords.y1, lineCords.y2, "black", bench, theClass, productionLine);
              cords[productionLine].push(lineCords);  
            }
            benchOutputTotal[day] += m2;
          }
        });

        var connectors = [];      
        angular.forEach(cords[productionLine], function (xYamount, key) {
          if(key !== cords[productionLine].length -1){
            var lineCords = {
              "x1":0,
              "x2":0,
              "y1":0,
              "y2":0
            };
            var current = cords[productionLine][key];
            var next = cords[productionLine][key+1];
            lineCords.x1 = current.x2;
            lineCords.y1 = current.y2;
            lineCords.x2 = next.x1;
            lineCords.y2 = next.y1;
            connectors.push(lineCords);
          }
        });
        
        angular.forEach(connectors, function (xYamount, key) {
          drawLine("connector", 20, xYamount.x1, xYamount.x2, xYamount.y1, xYamount.y2, "black", "connector", theClass, productionLine);
        });  

        //append weeks
        if (width > 900) {
          angular.forEach(weekPos, function (week) {
            parent.append("text")
              .attr("transform", "translate(" + [week.transformX, week.transformY] + ")rotate(45)")
              .attr("style", week.style)
              .attr("dy", week.dy)
              .attr("y", week.y)
              .text(function () {
                return d3.time.format("%d-%b")(new Date(week.date));
              })
              .style("fill", "black");
          });
        }


      }, 500);
    }
    
    function buildInteractiveInfo(event, productionLine, axis) {
      var axisText = {
        "1": "-axis1",
        "2": "-axis2"
      };
      var day = d3.time.format("%Y-%m-%d")(new Date(event.value));
      var dayHuman = d3.time.format("%d-%b-%y")(new Date(event.value));
      var textEntry = "";
      var totalM2 = 0;
      var totalCount = 0;
      var name;
      var selectedArea;
      if(axis === 1){
        name = "planned";
        selectedArea = event.data.key.replace(axisText["1"], "");
      }else{
        name = "dispatch";
        selectedArea = event.data.key.replace(axisText["2"], "");
      }
      textEntry += "<table style='padding: 2px;margin: 2px;text-align: center'><thead><tr><th>Colour</th><th>Area</th><th>Type</th><th>m2</th><th>Amount</th></tr></thead><tbody><caption style='background-color: " + event.series[0].color + ";font-weight: bold;color:black;text-align:center;'>" + selectedArea + "</caption><caption style='font-weight: bold;color:black;text-align:center;'>" + dayHuman + " ( "+name+" )</caption>";
      angular.forEach(vm.chartData[productionLine], function (data) {
        var yAxis = parseInt(data.yAxis);
        var originalKey = data.key;
        if(originalKey !== "bench"){
          angular.forEach(data.values, function (barValues) {
            if(barValues.x === event.value && barValues.y > 0 && axis === yAxis){
              var mainData = vm.mainData[productionLine][yAxis][originalKey][day];
              totalM2 += mainData.meterSquared;
              totalCount += mainData.amount;
              textEntry += "<tr><td class='legend-color-guide'><div style='background-color: " + data.color + ";'></div></td><td class='key'>" + originalKey.replace(axisText[yAxis], "") + "</td><td class='value'>" + mainData.type + "</td><td class='value'>" + Math.round(mainData.meterSquared) + " m2</td><td class='value'>" + Math.round(mainData.amount) + " panels</td><td class='value'>" + mainData.framingStyle + "</td></tr>";
            }
          });
        }
      });
      textEntry += "<tr><td>Totals:</td><td></td><td></td><td>"+Math.round(totalM2)+" m2</td><td>"+Math.round(totalCount)+" panels</td></tr>";
      textEntry += "</tbody>";
      textEntry += "</table>";
      return textEntry;
    }
    
    function barMouseOver(e, productionLine, axis){
      if (vm.resourceChartAPI[productionLine] && angular.isDefined(e)) {
        var chart = vm.resourceChartAPI[productionLine].getScope().chart;
        var tooltip = chart.tooltip;
        tooltip.gravity("s");
        tooltip.contentGenerator(function (d) {
          var textEntry = buildInteractiveInfo(e, productionLine, axis);
          return textEntry;
        });
      }
    }
    
    function drawLine(type, lineId, x1, x2, y1, y2, color, text, theClass, productionLine) {
      
      var chart = vm.resourceChartAPI[productionLine].getScope().chart;
      var svg = d3.select(theClass).select("svg");
      var yScale = chart.yAxis1.scale();
      var xScale = chart.xAxis.scale();
      var margin = chart.margin();
      var height = chart.height();

      svg.append("line")
        .style("stroke", color)
        .style("stroke-width", "0.5px")
        //.style("stroke-dasharray", "5,10")
        .attr("class", lineId)
        .attr("x1", x1 + margin.left)
        .attr("y1", yScale(y1) + margin.top)
        .attr("x2", x2 + margin.left)
        .attr("y2", yScale(y2) + margin.top);

      if (type === "line") {
        svg.append("text")
                .style("stroke", color)
                .attr("class", lineId + "-text")
                .attr("x", x1 + margin.left)
                .attr("y", yScale(y1) + margin.top)
                .text(text);
      }
    }
 
    function loadPlan(date) {
      var startDate = date != null ? dateUtil.weekStart(date) : null;
      destroyPlan();

      vm.plan = plannerService.getForwardPlan(startDate, vm.areasSelection);
      
      
      $timeout(function () {
        productionPlanService.getGreatestRisks()
        .then(function ( risks ) {
          vm.areasPlans = risks;
        });
      }, 1000);
   
      //$location.search("start", startDate);
      
      listener = $scope.$watch("vm.plan", function (newVal, oldVal) {
        if(newVal.areas.length > 0){
          vm.hideHolder = true;
          var data = plannerService.sortDataForChart(newVal, staticAverages);
          vm.chartData = {};
          vm.mainData = {};
          vm.chartData = data["chartData"];
          vm.mainData = data["mainData"];
          vm.lineBenches = data["lineBenches"];
          vm.areaColour = data["areaColour"];
          vm.maxData = data["maxData"];
          vm.maxBench = data["maxBench"];
          vm.lineCapacities = data["lineCapacities"];
          vm.defaultLineCapacity = data["defaultLineCapacity"];
          vm.defaultBench = data["defaultBench"];
          vm.defaultBenchOperators = data["defaultBenchOperators"];
          vm.maxDataDate = data["maxDataDate"];
          setYDomains(data["maxData"], data["maxBench"]);
        }
      }, true);
    }
    
    function setYDomains(maxData, maxBench) {
      angular.forEach(staticAverages, function (averages, productionLine) {
        if (angular.isDefined(maxBench[productionLine]) && angular.isDefined(maxData[productionLine])) {
          maxData[productionLine] = maxData[productionLine].reduce(function (a, b) {
            return Math.max(a, b);
          });
          maxBench[productionLine] = maxBench[productionLine].reduce(function (a, b) {
            return Math.max(a, b);
          });
          var toSet = maxData[productionLine] + maxBench[productionLine] + 1;
          vm.options[productionLine].chart.yDomain1 = [0, parseInt(toSet)];
          vm.options[productionLine].chart.yDomain2 = [0, parseInt(toSet)];
          if(angular.isDefined(vm.resourceChartAPI[productionLine])){
            vm.resourceChartAPI[productionLine].refreshWithTimeout(100);
          }
        }
      });
    }
    
    function destroyPlan() {
      if (vm.plan && vm.plan.$destroy) {
        vm.plan.$destroy();
        delete vm.plan;
      }
      if (listener) {
        listener();
      }
      if (modifyChartTimeout) {
        $timeout.cancel(modifyChartTimeout);
      }
      angular.element($window).off("resize");
    }
    
    function setWeekCapacity(size, d, productionLine) {
      var day = d3.time.format("%d-%b-%y")(new Date(d.data.x));
      var dayISO = dateUtil.toIsoDate(d.data.x);
      vm.day = day;
      vm.days = "1";
      var capacity = vm.lineCapacities[productionLine][dayISO];
      if(angular.isUndefined(capacity)){
        capacity = vm.defaultLineCapacity[productionLine];
      }
      vm.capacity = capacity;
      if(angular.isDefined(vm.lineBenches[productionLine][dayISO])){
        vm.benchMeterSquared = vm.lineBenches[productionLine][dayISO].meterSquared;
        vm.benchOperators = vm.lineBenches[productionLine][dayISO].operators;
      }else{
        vm.benchMeterSquared = vm.defaultBench[productionLine];
        vm.benchOperators = vm.defaultBenchOperators[productionLine];
      }
      vm.productionLine = productionLine;
      var modalInstance = $uibModal.open({
        templateUrl: "capacityModal.html",
        controller: ["$uibModalInstance", "day", "productionLine", "capacity", "benchMeterSquared", "benchOperators", "days", "weekend", "plan", "defaultLineCapacity", "defaultBench", "defaultBenchOperators", "benchmarks", capacityController],
        controllerAs: "vm",
        size: size,
        resolve: {
          day: function () {
            return vm.day;
          },
          productionLine: function () {
            return productionLine;
          },
          capacity: function () {
            return vm.capacity;
          },
          benchMeterSquared: function () {
            return vm.benchMeterSquared;
          },
          benchOperators: function () {
            return vm.benchOperators;
          },
          days: function () {
            return vm.days;
          },
          weekend: function () {
            return vm.weekend;
          },
          plan: function () {
            return vm.plan;
          },
          defaultLineCapacity: function () {
            return vm.defaultLineCapacity;
          },
          defaultBench: function () {
            return vm.defaultBench;
          },
          defaultBenchOperators: function () {
            return vm.defaultBenchOperators;
          },
          benchmarks: function () {
            return vm.lineBenches;
          }
        }
      });
      modalInstance.result.then(function ( resolvedResponse ) {
        vm.resourceChartAPI[productionLine].refreshWithTimeout(100);
        $confirm({
          text: "Update Success: Week: " + vm.day + " set to " + resolvedResponse,
          cancel: "Ok"
        }, {
          templateUrl: require("../core/info-modal.html")
        });
      }, function ( rejectionResponse ) {
        vm.resourceChartAPI[productionLine].refreshWithTimeout(100);
        $confirm({
          text: "Update Failure: " + rejectionResponse,
          cancel: "Ok"
        }, {
          templateUrl: require("../core/info-modal.html")
        });
      });
    }
    
    function capacityController( $uibModalInstance, day, productionLine, capacity, benchMeterSquared, benchMOperators, days, weekend, plan, defaultLineCapacity, defaultBench, defaultBenchOperators, benchmarks ) {
      var vm = this;
      
      angular.element('[data-target="#tab1"]').tab("show");
      
      vm.day = day;
      vm.days = days;
      vm.productionLine = productionLine;
      vm.capacity = capacity;
      vm.weekend = weekend;
      vm.lineBenches = benchmarks;
      vm.defaultBenchOperators = defaultBenchOperators;
      vm.defaultLineCapacity = defaultLineCapacity;
      vm.defaultBench = defaultBench;
      vm.benchMeterSquared = benchMeterSquared;
      vm.benchMOperators = benchMOperators;
      
      vm.daysChanged = daysChanged;
      vm.updateBenches = updateBenches;
      vm.updateCapacity = updateCapacity;
      vm.cancel = cancel;

      function daysChanged(type){
        if(type === "capacity"){
          if(parseInt(vm.days) !== 1){
            vm.capacity = vm.defaultLineCapacity[productionLine];
          }else{
            vm.capacity = capacity;
          }
        }else{
          if(parseInt(vm.days) !== 1){
            vm.benchMeterSquared = vm.defaultBench[productionLine];
            vm.benchMOperators = vm.defaultBenchOperators[productionLine];
          }else{
            vm.benchMeterSquared = benchMeterSquared;
            vm.benchMOperators = benchMOperators;
          }
        }
      }
      
      function updateCapacity() {
        if (parseInt(vm.days) === 7){
          vm.day = dateUtil.weekStart(vm.day);
        }else if (parseInt(vm.days) === 21){
          vm.day = plan.viewStart;
        }
        var promises = [];
        for(var i = 0; i < parseInt(vm.days); i++){
          var date = dateUtil.plusDays(vm.day, i);
          var dayOfWeek = dateUtil.dayOfWeek(date);
          if(vm.weekend){
            if (dayOfWeek !== 6){
              if(vm.capacity < 1){
                vm.capacity = vm.defaultLineCapacity[productionLine];
              }
              promises.push(plannerService.setCapacity(vm.productionLine, date, vm.capacity));
            }
          }else{
            if (dayOfWeek !== 5 && dayOfWeek !== 6){
              if(vm.capacity < 1){
                vm.capacity = vm.defaultLineCapacity[productionLine];
              }
              promises.push(plannerService.setCapacity(vm.productionLine, date, vm.capacity));
            }
          }
        }
        $q.all(promises)
        .then(function () {
          $uibModalInstance.close(vm.capacity);
        });

      }
      function updateBenches() {
        if (parseInt(vm.days) === 7){
          vm.day = dateUtil.weekStart(vm.day);
        }else if (parseInt(vm.days) === 21){
          vm.day = plan.viewStart;
        }
        var promises = [];
        for(var i = 0; i < parseInt(vm.days); i++){
          var date = dateUtil.plusDays(vm.day, i);
          var dayOfWeek = dateUtil.dayOfWeek(date);
          if(vm.weekend){
            if (dayOfWeek !== 6){
              if(vm.benchMeterSquared < 1 || vm.benchMOperators < 1){
                vm.benchMeterSquared = vm.defaultBench[vm.productionLine];
                vm.benchMOperators = vm.defaultBenchOperators[vm.productionLine];
              }
              if (angular.isUndefined(vm.lineBenches[vm.productionLine][date])){
                vm.lineBenches[vm.productionLine][date] = {
                  "meterSquared": vm.benchMeterSquared,
                  "operators": vm.benchMOperators
                };
              }else{
                vm.lineBenches[vm.productionLine][date].meterSquared = vm.benchMeterSquared;
                vm.lineBenches[vm.productionLine][date].operators = vm.benchMOperators;
              }
              promises.push(vm.lineBenches[vm.productionLine].$save());
            }
          }else{
            if (dayOfWeek !== 5 && dayOfWeek !== 6){
              if(vm.benchMeterSquared < 1 || vm.benchMOperators < 1){
                vm.benchMeterSquared = vm.defaultBench[vm.productionLine];
                vm.benchMOperators = vm.defaultBenchOperators[vm.productionLine];
              }
              if (angular.isUndefined(vm.lineBenches[vm.productionLine][date])){
                vm.lineBenches[vm.productionLine][date] = {
                  "meterSquared": vm.benchMeterSquared,
                  "operators": vm.benchMOperators
                };
              }else{
                vm.lineBenches[vm.productionLine][date].meterSquared = vm.benchMeterSquared;
                vm.lineBenches[vm.productionLine][date].operators = vm.benchMOperators;
              }
              promises.push(vm.lineBenches[vm.productionLine].$save());
            }
          }
        }
        $q.all(promises)
        .then(function () {
          $uibModalInstance.close(vm.benchMeterSquared);
        });

      }
      function cancel() {
        $uibModalInstance.dismiss("cancel");
      }
    }
    
  }

})();
