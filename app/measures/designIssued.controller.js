(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("DesignIssuedController", DesignIssuedController);

  /**
   * @ngInject
   */
  function DesignIssuedController($log, $scope, $timeout, schema, measuresService) {

    var vm = this;
    
    vm.designIssuedCount = {};
    vm.designIssuedM2 = {};
    vm.syncStats = syncStats;
    vm.searchSyncStatsText = "Sync Stats";
    vm.showSpinner = false;
    
    $scope.$on("$destroy", destroyStats);
    
    var stats = schema.getRoot().child("stats");
    stats.on("value", init);
    
    function init(){
    
      measuresService.designedIssued()
        .then(function (stats){
          vm.designIssuedCount["SIP"] = stats.type["SIP"].designedCount - stats.type["SIP"].completedCount;
          vm.designIssuedCount["HSIP"] = stats.type["HSIP"].designedCount - stats.type["HSIP"].completedCount;
          vm.designIssuedCount["IFAST"] = stats.type["IFAST"].designedCount - stats.type["IFAST"].completedCount;
          vm.designIssuedCount["TF"] = stats.type["TF"].designedCount - stats.type["TF"].completedCount;
          vm.designIssuedCount["CASS"] = stats.type["CASS"].designedCount - stats.type["CASS"].completedCount;

          vm.designIssuedM2["SIP"] = stats.type["SIP"].designedM2 - stats.type["SIP"].completedM2;
          vm.designIssuedM2["HSIP"] = stats.type["HSIP"].designedM2 - stats.type["HSIP"].completedM2;
          vm.designIssuedM2["IFAST"] = stats.type["IFAST"].designedM2 - stats.type["IFAST"].completedM2;
          vm.designIssuedM2["TF"] = stats.type["TF"].designedM2 - stats.type["TF"].completedM2;
          vm.designIssuedM2["CASS"] = stats.type["CASS"].designedM2 - stats.type["CASS"].completedM2;

          vm.totalDesignCount = stats.totalDesignedCount - stats.totalCompleteCount;
          vm.totalDesignM2 = stats.totalDesignedM2 - stats.totalCompleteM2;
        });
      
    }
    
    function destroyStats() {
      stats.off("value", init);
    }
    
    function syncStats(){
      vm.searchSyncStatsText = "Syncing";
      vm.showSpinner = true;
      measuresService.syncStats()
        .then(function (){
          vm.showSpinner = false;
          vm.searchSyncStatsText = "Sync Complete";
          $timeout(function (){
            vm.searchSyncStatsText = "Sync Stats";
          }, 2000);
        });
    }

  }

})();
