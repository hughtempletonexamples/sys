(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("ProductController", ProductController);

  /**
   * @ngInject
   */
  function ProductController(productService, $uibModal, $scope, $log, $window, $confirm, $timeout, dateUtil, plannerService, productionPlanService, $q) {

    var vm = this;
    
    vm.chartAPI = {};
    vm.options = {};
    
    vm.capacity = 0;
    vm.weekend = false;
    
    var modifyChartTimeout;
    
    var staticAverages = {};
    staticAverages["SIP"] = 3875 / 4;
    staticAverages["HSIP"] = 3875 / 4;
    staticAverages["IFAST"] = 3875 / 4;
    staticAverages["TF"] = 3798 / 4;
    staticAverages["CASS"] = 1171 / 4;

    var staticMaxAverages = {};
    staticMaxAverages["SIP"] = 5067 / 4;
    staticMaxAverages["HSIP"] = 5067 / 4;
    staticMaxAverages["IFAST"] = 5067 / 4;
    staticMaxAverages["TF"] = 4805 / 4;
    staticMaxAverages["CASS"] = 1740 / 4;
    
    $scope.$on("$destroy", destroyChart);
    
    angular.forEach(staticAverages, function (averages, productionLine) {
      
      vm.options[productionLine] = {
        chart: {
          hasClick: false,
          type: "multiBarChart",
          height: 300,
          margin: {
            top: 20,
            right: 20,
            bottom: 30,
            left: 60
          },
          clipEdge: true,
          duration: 500,
          useVoronoi: false,
          useInteractiveGuideline: false,
          showXAxis: "true",
          showYAxis: "true",
          stacked: true,
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format("%b-%d")(new Date(d));
            }
          },
          yAxis: {
            axisLabelDistance: -20,
            tickFormat: function (d) {
              return d3.format(",.1f")(d);
            }
          }
        }
      };

    });

    productService.getData()
        .then(function (data) {
          vm.chartData = data;
        });
  
    function destroyChart() {

      
    }
    
 
  }

})();
