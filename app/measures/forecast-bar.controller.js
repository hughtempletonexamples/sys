(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("ForecastBarController", ForecastBarController);

  /**
   * @ngInject
   */
  function ForecastBarController(forecastBarService, $uibModal, $scope, $log, $window, $confirm, $timeout, dateUtil, plannerService, productionPlanService, $q) {

    var vm = this;
    
    vm.chartAPI = {};
    vm.options = {};
    
    vm.capacity = 0;
    vm.weekend = false;
    
    var modifyChartTimeout;
    
    var staticAverages = {};
    staticAverages["SIP"] = 3875 / 4;
    staticAverages["TF"] = 3798 / 4;
    staticAverages["CASS"] = 1171 / 4;

    var staticMaxAverages = {};
    staticMaxAverages["SIP"] = 5067 / 4;
    staticMaxAverages["TF"] = 4805 / 4;
    staticMaxAverages["CASS"] = 1740 / 4;
    
    $scope.$on("$destroy", destroyChart);
    
    angular.forEach(staticAverages, function (averages, productionLine) {
      
      vm.options[productionLine] = {
        chart: {
          callback: function (e) {
            theCallBack(e, productionLine);
          },
          hasClick: false,
          type: "multiBarChart",
          height: 270,
          margin: {
            top: 20,
            right: 20,
            bottom: 80,
            left: 85
          },
          clipEdge: true,
          duration: 500,
          useVoronoi: false,
          useInteractiveGuideline: true,
          showXAxis: "true",
          showYAxis: "true",
          stacked: true,
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format("%b-%d")(new Date(d));
            }
          },
          yAxis: {
            axisLabelDistance: -20,
            tickFormat: function (d) {
              return d3.format(",.1f")(d);
            }
          },
          multibar: {   
            dispatch: {   
              elementClick: function (d) {
                setWeekCapacity("md", d, productionLine);
              }
            }
          }
        }
      };

    });
    
    angular.element($window).on("resize", function () {
      angular.forEach(staticAverages, function (averages, productionLine) {
        if (vm.chartAPI) {
          vm.chartAPI[productionLine].refreshWithTimeout(100);
        }
      });
    });
    
    productionPlanService.getGreatestRisks()
        .then(function (areaPlans) {
          vm.areasPlans = areaPlans;
          return forecastBarService.getData(false, 48, 0);
        })
        .then(function (data) {
          vm.data = data;
          vm.chartData = data.chartData;
          vm.monthData = data.monthData;
          vm.monthTotals = data.monthTotals;
          vm.weekTotals = data.weekTotals;
          vm.yearTotals = data.yearTotals;
          vm.sipYear = Math.round(vm.data.yearTotals["SIP"]);
          vm.tfYear = Math.round(vm.data.yearTotals["TF"]);
          vm.cassYear = Math.round(vm.data.yearTotals["CASS"]);
          vm.projectsByGateway = data.projectsByGateway;
          vm.monthProjectsByGateway = data.monthProjectsByGateway;
        });
      
    function modifyChart(productionLineHolder) {

      modifyChartTimeout = $timeout(function () {
        
        var productionLine = productionLineHolder;
        var theClass = "." + productionLine;
        
        var allRects = d3.select(theClass).selectAll("g.nv-group").selectAll("rect");
        var parent = d3.select(theClass).select(".nv-x.nv-axis");

        var weekPos = []; 
        var monthPos = [];
        
        //last text is max y axis
        var maxY = 0;
        var allYAxis = d3.select(theClass).select(".nv-y.nv-axis > g").selectAll("text");
        if (angular.isDefined(allYAxis[0])){
          maxY = allYAxis[0][allYAxis[0].length-1].innerHTML;
          maxY = maxY.replace(",","");
        } 

        // drawing the line
        var svg = d3.select(theClass).select("svg");    
        
        //remove all previous placements
        svg.select(".average").remove();
        svg.select(".maxAverage").remove();
        parent.selectAll("text").remove();
        parent.selectAll(".monthRect").remove();
        
        var textStaticAverages = staticAverages[productionLine] + " m2";
        var textStaticMaxAverages = staticMaxAverages[productionLine] + " m2";
        
        if (staticAverages[productionLine] <= maxY) {  
          drawLine("average", staticAverages[productionLine], "black", textStaticAverages, theClass, productionLine, false);
        }
        
        if (staticMaxAverages[productionLine] <= maxY) {          
          drawLine("maxAverage", staticMaxAverages[productionLine], "black", textStaticMaxAverages, theClass, productionLine, true);
        }          
        // more space at the bottom
        svg.style("height", "350px");          
        angular.forEach(allRects[0], function (path, key) {
          var transform = d3.select(path).attr("transform");
          var splitted = transform.split(",");
          var transformX = ~~splitted [0].split("(")[1];
          var transformY = 0;
          
          var dates = vm.chartData[productionLine][0].values;
          
          var month = d3.time.format("%b-%y")(new Date(dates[key].x));
          var monthName = d3.time.format("%b")(new Date(dates[key].x));
          
          var results = monthPos.find(function (item) {
            return item.month === month;
          });
          
          if (angular.isUndefined(results)){

            monthPos.push({
              "transformY": 50,
              "transformX": transformX,
              "height": "25px",
              "month": month,
              "monthName": monthName,
              "date": dates[key].x
            });
            
          }

          weekPos.push({
            "transformY": transformY,
            "transformX": transformX,
            "date": dates[key].x,
            "style":"text-anchor: left;",
            "dy":".71em",
            "y":7
          });

        });
        //append weeks
        if ($window.innerWidth > 900){
          angular.forEach(weekPos, function (week) {
            parent.append("text")
              .attr("transform", "translate(" + [week.transformX, week.transformY] + ")rotate(45)")
              .attr("style", week.style)
              .attr("dy", week.dy)
              .attr("y", week.y)
              .text(function () {
                return d3.time.format("%d-%b")(new Date(week.date));
              })
              .style("fill", "black");
          });
        }
        //append rects
        angular.forEach(monthPos, function (month, i) {
          
          var svgWidth = svg.style("width").split("px")[0];
          var chart = vm.chartAPI[productionLine].getScope().chart;
          var margin = chart.margin();
          var yearDate = new Date().getFullYear();
         
          
          var width = 0;
          var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

          if (i === monthPos.length-1){
            width = svgWidthMinusMargin - month.transformX;
          }else{
            width = monthPos[i+1].transformX - month.transformX;
          }
          
          parent.append("rect")
            .attr("class", "monthRect")
            .attr("transform", "translate(" + [month.transformX, month.transformY] + ")")
            .attr("x", 0)
            .attr("y", 34)
            .attr("width", width + "px")
            .attr("height", month.height)
            .style("fill", "rgb(255,255,255)")
            .style("stroke-width", "2")
            .style("opacity", "0.6")
            .style("stroke", "rgb(0,0,0)");
            
        });
        //append months
        if ($window.innerWidth > 900) {
          angular.forEach(monthPos, function (month, i) {
            
            var svgWidth = svg.style("width").split("px")[0];
            var chart = vm.chartAPI[productionLine].getScope().chart;
            var margin = chart.margin();

            var width = 0;
            var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

            if (i === monthPos.length - 1) {
              width = svgWidthMinusMargin - month.transformX;
            } else {
              width = monthPos[i + 1].transformX - month.transformX - 25;
            }

            var textTransformX = month.transformX + (width / 2);

            if (width > 40) {
              parent.append("text")
                .attr("class", "monthText")
                .attr("transform", "translate(" + [textTransformX, month.transformY] + ")")
                .attr("x", -25)
                .attr("y", 54)
                .attr("width", width + "px")
                .attr("height", month.height)
                .style("text-anchor", "center")
                .style("text-size", "12px")
                .text(function () {
                  return month.monthName ;
                })
                .style("fill", "black")
                .style("font-size", "1.3em");

            }
          });
        }        
        //append rects
        angular.forEach(monthPos, function (month, i) {
          
          var svgWidth = svg.style("width").split("px")[0];
          var chart = vm.chartAPI[productionLine].getScope().chart;
          var margin = chart.margin();
          
          var width = 0;
          var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

          if (i === monthPos.length-1){
            width = svgWidthMinusMargin - month.transformX;
          }else{
            width = monthPos[i+1].transformX - month.transformX;
          }
          
          parent.append("rect")
            .attr("class", "monthRect")
            .attr("transform", "translate(" + [month.transformX, month.transformY] + ")")
            .attr("x", 0)
            .attr("y", 59)
            .attr("width", width + "px")
            .attr("height", month.height)
            .style("fill", "rgb(255,255,255)")
            .style("stroke-width", "2")
            .style("opacity", "0.6")
            .style("stroke", "rgb(0,0,0)");
            
        });        
        //append months
        if ($window.innerWidth > 900) {
          angular.forEach(monthPos, function (month, i) {
            var fmonth = dateUtil.dateMonth(month.date);
            var svgWidth = svg.style("width").split("px")[0];
            var chart = vm.chartAPI[productionLine].getScope().chart;
            var margin = chart.margin();

            var width = 0;
            var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

            if (i === monthPos.length - 1) {
              width = svgWidthMinusMargin - month.transformX;
            } else {
              width = monthPos[i + 1].transformX - month.transformX - 25;
            }

            var textTransformX = month.transformX + (width / 2);

            if (width > 40) {
              parent.append("text")
                .attr("class", "monthText")
                .attr("transform", "translate(" + [textTransformX, month.transformY] + ")")
                .attr("x", -25)
                .attr("y", 82)
                .attr("width", width + "px")
                .attr("height", month.height)
                .style("text-anchor", "center")
                .style("text-size", "12px")
                .html(function () {
                  if(angular.isDefined(vm.monthTotals[productionLine][fmonth])){
                    return Math.round(vm.monthTotals[productionLine][fmonth]) +"m2";
                  }else{
                    return Math.round(vm.monthTotals[productionLine][fmonth]) +"m2";
                  }
                  
                })
                .style("fill", "black")
                .style("font-size", "1.3em");

            }
          });
        }                           
        //append week rects
        angular.forEach(weekPos, function (week, i) {
          
          var svgWidth = svg.style("width").split("px")[0];
          var chart = vm.chartAPI[productionLine].getScope().chart;
          var margin = chart.margin();
          
          var width = 0;
          var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

          if (i === weekPos.length-1){
            width = svgWidthMinusMargin - week.transformX;
          }else{
            width = weekPos[i+1].transformX - week.transformX;
          }
          
          parent.append("rect")
            .attr("class", "weekRect")
            .attr("transform", "translate(" + [week.transformX, week.transformY] + ")")
            .attr("x", 0)
            .attr("y", 42)
            .attr("width", width + "px")
            .attr("height", "42px")
            .style("fill", "rgb(255,255,255)")
            .style("stroke-width", "2")
            .style("opacity", "0.6")
            .style("stroke", "rgb(0,0,0)");
            
        });        
        //append week data
        if ($window.innerWidth > 900) {
          angular.forEach(weekPos, function (week, i) {
            
            var svgWidth = svg.style("width").split("px")[0];
            var chart = vm.chartAPI[productionLine].getScope().chart;
            var margin = chart.margin();
            var weekDate =  d3.time.format("%Y-%m-%d")(new Date(week.date));

            var width = 0;
            var svgWidthMinusMargin = svgWidth - (margin.left + margin.right);

            if (i === weekPos.length - 1) {
              width = svgWidthMinusMargin - week.transformX;
            } else {
              width = weekPos[i + 1].transformX - week.transformX;
            }

            var textTransformX = week.transformX + (width / 2);

            if (width > 20) {
              parent.append("text")
                .attr("class", "weekText")
                .attr("transform", "translate(" + [textTransformX, week.transformY]  + ")rotate(90)")
                .attr("x", 42)
                .attr("y", 10)
                .attr("width", width + "px")
                .attr("height", "42px")
                .style("text-anchor", "center")
                .style("text-size", "2px")
                .text(function () {
                  if(angular.isDefined(vm.weekTotals[productionLine][weekDate])){
                    return Math.round(vm.weekTotals[productionLine][weekDate]); 
                  }
                  else{
                    return 0;
                  }
                  
                })
                .style("fill", "black")
                .style("font-size", "1.3em");

            }
          });
        }                      
      }, 100);
    }
    
    function drawLine(lineId, value, color, text, theClass, productionLine, dash) {
      
      var chart = vm.chartAPI[productionLine].getScope().chart;
      var svg = d3.select(theClass).select("svg");
      var yScale = chart.yAxis.scale();
      var margin = chart.margin();
      var height = chart.height();
      var width = svg.style("width").split("px")[0];
      
      if (dash) {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-dasharray", "10, 5")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      } else {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-width", "1px")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      }
 
      svg.append("text")
        .style("stroke", color)
        .attr("class", lineId + "-text")
        .attr("x", +width - margin.right / 2)
        .attr("y", yScale(value) + margin.top)
        .attr("text-anchor", "end")
        .text(text);
    }
    
    function theCallBack(e, productionLine){
      if(angular.isDefined(e)){
        var tooltip = e.interactiveLayer.tooltip;
        tooltip.contentGenerator(function (event) {
          var textEntry = buildInteractiveInfo(event, productionLine);
          vm.options[productionLine].chart.textEntry = textEntry;
          return textEntry;
        });
        modifyChart(productionLine);
      }
    }
    
    function areaClick(e, productionLine){
      if (vm.chartAPI[productionLine] && angular.isDefined(e)) {
        if (!vm.options[productionLine].chart.hasClick) {
          var chart = vm.chartAPI[productionLine].getScope().chart;
          var tooltip = chart.interactiveLayer.tooltip;
          tooltip.contentGenerator(function (d) {
            var textEntry = buildAreaClickInfo(d, e, productionLine);
            return textEntry;
          });
          vm.options[productionLine].chart.hasClick = true;
        } else {
          vm.chartAPI[productionLine].refreshWithTimeout(100);
          vm.options[productionLine].chart.hasClick = false;
        }
      }
    }
    
    function buildInteractiveInfo(event, productionLine) {
      var week = d3.time.format("%d-%b-%y")(new Date(event.value));
      var weekDform = dateUtil.weekStart(event.value);
      var month = d3.time.format("%Y-%m")(new Date(event.value));
      var monthNum = new Date(event.value).getMonth();
      var humanMonth = d3.time.format("%B")(new Date(event.value));
      var monthTotal = 0;
      var textEntry = "";
      textEntry += "<table style='display: inline-block;border-style: dashed;padding:2px;margin:2px;><thead><tr><td colspan='4'></td></tr></thead><tbody><caption style='font-weight: bold;color:black;text-align:center;'>" + week + "</caption>";
      angular.forEach(event.series, function (data) {
        if (data.key !== "TOTAL") {
          textEntry += "<tr><td class='legend-color-guide'><div style='background-color: " + data.color + ";'></div></td><td class='key'>" + data.key + "</td><td class='value'>" + Math.round(data.value) + "</td><td class='value'>" + Math.round(vm.monthData[productionLine][data.key][month]) + "</td></tr>";
          monthTotal += vm.monthData[productionLine][data.key][month];
        } else {
          textEntry += "<tr><td class='legend-color-guide'><div style='background-color: " + data.color + ";'></div></td><td class='key'>" + data.key + "</td><td class='value'>" + Math.round(data.value) + "</td><td class='value'>" + Math.round(monthTotal) + "</td></tr>";
        }
      });
      textEntry += "</tbody>";
      textEntry += "</table>";

      textEntry += "<table style='display: inline-block;border-style: dashed;padding:2px;margin:2px;'>";
      textEntry += "<caption style='font-weight: bold;color:black;text-align:center;'>Projects in "+humanMonth+"</caption>";
      textEntry += "<tr>";
      angular.forEach(event.series, function (data) {
        if (data.key !== "TOTAL" && vm.monthProjectsByGateway && vm.monthProjectsByGateway[productionLine][data.key][month].length > 0) {
          textEntry += "<td><strong>" + data.key + "</strong></td>";
          textEntry += "<tr>";
          angular.forEach(vm.monthProjectsByGateway[productionLine][data.key][month], function (projectId) {
            textEntry += "<td>" + projectId + "</td>";
          });
          textEntry += "</tr>";
        }
      });
      textEntry += "</tr>";
      textEntry += "</table>";
       
      textEntry +="<table style='border-style: dashed;padding:2px;margin:2px;'>";
      textEntry += "<caption style='font-weight: bold;color:black;text-align:center;'>Totals in "+humanMonth+"</caption>";
      textEntry += "<tr>";
      textEntry += "<td> Year Total:" + Math.round(vm.yearTotals[productionLine]) +"m<sup>2</sup>" + "</td>";
      textEntry += "</tr>";
      textEntry += "<tr>";
      textEntry += "<td> Month Total:" + Math.round(vm.monthTotals[productionLine][month]) +"m<sup>2</sup>" + "</td>";
      textEntry += "</tr>";
      textEntry += "<tr>";
      textEntry += "<td> Week Total:" + Math.round(vm.weekTotals[productionLine][weekDform]) +"m<sup>2</sup>" + "</td>";
      textEntry += "</tr>";
      textEntry += "</table>";
      
      
      return textEntry;
    }
    
    function buildAreaClickInfo(d, e, productionLine){
      var date = new Date(d.value);
      var week = dateUtil.weekStart(date);
      var humanWeek = d3.time.format("%d-%b-%y")(new Date(week));
      var textEntry = "";
      textEntry += "<table>";
      textEntry += "<thead>";
      textEntry += "<tr>";
      textEntry += "<th>Projects for " + e.series + " this Week ("+humanWeek+"):</th>";
      textEntry += "</tr>";
      textEntry += "</thead>";
      textEntry += "<tbody>";
      textEntry += "<thead>";
      textEntry += "<tr>";
      angular.forEach(vm.projectsByGateway[productionLine][e.series][week], function (projectId) {
        textEntry += "<td>" + projectId + "</td>";
      });
      textEntry += "</tr>";
      textEntry += "</tbody>";
      textEntry += "</table>";
      return textEntry;
    }
    
    function destroyChart() {
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (modifyChartTimeout) {
        $timeout.cancel(modifyChartTimeout);
      }
      
      
      angular.element($window).off("resize");
    }
    
    function setWeekCapacity(size, d, productionLine) {
      var week = d3.time.format("%d-%b-%y")(new Date(d.data.x));
      vm.week = week;
      vm.productionLine = productionLine;
      var modalInstance = $uibModal.open({
        templateUrl: "weekCapacityModal.html",
        controller: ["$uibModalInstance", "week", "productionLine", "capacity", "weekend", weekCapacityController],
        controllerAs: "vm",
        size: size,
        resolve: {
          week: function () {
            return vm.week;
          },
          productionLine: function () {
            return productionLine;
          },
          capacity: function () {
            return vm.capacity;
          },
          weekend: function () {
            return vm.weekend;
          }
        }
      });
      modalInstance.result.then(function ( resolvedResponse ) {
        $confirm({
          text: "Update Success: Week: " + vm.week + " set to " + resolvedResponse,
          cancel: "Ok"
        }, {
          templateUrl: require("../core/info-modal.html")
        });
      }, function ( rejectionResponse ) {
        $confirm({
          text: "Update Failure: " + rejectionResponse,
          cancel: "Ok"
        }, {
          templateUrl: require("../core/info-modal.html")
        });
      });
    }
    
    function weekCapacityController( $uibModalInstance, week, productionLine, capacity, weekend ) {
      var vm = this;

      vm.week = week;
      vm.productionLine = productionLine;
      vm.capacity = capacity;
      vm.weekend = weekend;
      
      vm.update = update;
      vm.cancel = cancel;

      function update() {
        var promises = [];
        var days = 5;
        if (vm.weekend) {
          days = 7;
        }
        for(var i = 0; i < days; i++){
          var date = dateUtil.plusDays(vm.week, i);
          promises.push(plannerService.setCapacity(vm.productionLine, date, vm.capacity));
          //promises.push($q.resolve(date));
        }
        $q.all(promises)
        .then(function () {
          $uibModalInstance.close(vm.capacity);
        });

      }
      function cancel() {
        $uibModalInstance.dismiss("cancel");
      }
    }
  
  }

})();
