(function () {
  "use strict";

  angular
    .module("innView.measures")
    .factory("resourceService", resourceService);

  /**
   * @ngInject
   */
  function resourceService($log, $filter, dateUtil, schema) {

    var service = {
      getBenchmark: getBenchmark,
      setBenchmark: setBenchmark
    };

    return service;

    ////////////
    
    function setBenchmark(benchmarks, productionLine, date, elem, amount) {
      var lineCapacity = benchmarks[productionLine];
      lineCapacity[date][elem] = amount;
      return lineCapacity.$save();
    }
    
    
    function getBenchmark() {
      return schema.getObject(schema.getRoot().child("benchmarks"));
    }
  }
  
})();
