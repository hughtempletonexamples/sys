(function () {
  "use strict";

  angular
    .module("innView.measures")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/designIssued", {
        templateUrl: require("./designIssued.html"),
        controller: "DesignIssuedController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/forecast", {
        templateUrl: require("./forecast.html"),
        controller: "ForecastController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/forecastBar", {
        templateUrl: require("./forecastBar.html"),
        controller: "ForecastBarController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/resource", {
        templateUrl: require("./resource.html"),
        controller: "ResourceController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/designBuffer", {
        templateUrl: require("./designBuffer.html"),
        controller: "MeasuresController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/deliveredBuffer", {
        templateUrl: require("./deliveredBuffer.html"),
        controller: "MeasuresController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/products", {
        templateUrl: require("./product.html"),
        controller: "ProductController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("performance");
  }

})();
