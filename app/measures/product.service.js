(function () {
  "use strict";

  angular
    .module("innView.measures")
    .factory("productService", productService);

  /**
   * @ngInject
   */
  function productService($log, schema, referenceDataService, $q, projectsService, areasService, panelsService, dateUtil) {
    
    var today = new Date();
    var todayWeekStart = dateUtil.weekStart(today);
    
    var service = {
      getData: getData,
      getDataBP: getDataBP
    };

    return service;

    ////////////
    
    function getDataBP( areaRef ) {
      return panelsService.panelsForArea(areaRef)
              .$loaded()
              .then(function ( panels ) {
                return panels;
              });
    }
    
    
    function getData(fromDate, toDate, isTest) {
     
      var matchingAreas = {};
      var matchingPanels = {};
      var areaStartDate = [];
      var operativeNames = [];
      var productTypeProductionLine = {};
      var chartData = {};
      var areaPlans = {};
      var additionalData = {};
      var projects;

      var deferred = $q.defer();
      
      var weeks = getWeeks(6, 21);
      
      if(angular.isUndefined(fromDate)){
        fromDate = weeks[0];
      }
      
      if(angular.isUndefined(toDate)){
        toDate  = weeks[weeks.length - 1];
      }

      projectsService.getProjects()
        .$loaded()
        .then(function (proj) {
          projects = proj;
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function () {
          var plansRef = schema.getRoot().child("productionPlans/areas");
          return plansRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var planKey = snapPlan.key;
              var plan = schema.getObject(plansRef.child(planKey));
              areaPlans[planKey] = plan;
            });
          });
        })
        .then(processProjects)
        .then(populateOperativeNames)
        .then(getProductTypes)
        .then(aggregatePanels)
        .then(function () {
          processPanels();
          deferred.resolve(chartData);
        });

      return deferred.promise;

      function processProjects() {
        var promises = [];
        angular.forEach(projects, function (project) {
          if (angular.isDefined(project.deliverySchedule.published)) {
            // get areas with published delivery date within specified range
            promises.push(findAreasWithMatchingDeliveryDate(project));
          }
        });
        return $q.all(promises);
      }

      function findAreasWithMatchingDeliveryDate(project) {
        return areasService.getProjectAreas(project.$id)
          .$loaded()
          .then(function (projectAreas) {
            angular.forEach(projectAreas, function (area) {
              if (angular.isDefined(additionalData[area.$id]) && angular.isDefined(additionalData[area.$id].buildPlannedDays)) {
                var buildPlannedDaysArray = Object.keys(additionalData[area.$id].buildPlannedDays);
                buildPlannedDaysArray.sort();
                area._startDate = buildPlannedDaysArray[0];
              } else if (angular.isDefined(areaPlans[area.$id]) && angular.isDefined(areaPlans[area.$id].plannedStart)) {
                area._startDate = areaPlans[area.$id].plannedStart;
              }
              if (angular.isDefined(area._startDate) && angular.isDefined(area.revisions) && angular.isDefined(area.revisions[project.deliverySchedule.published])) {
                if (area._startDate >= fromDate && area._startDate <= toDate) {
                  matchingAreas[area.$id] = area;
                  areaStartDate[area.$id] = area._startDate;
                }
              }
            });
          });
      }
      
      function getProductTypes(){
        var productTypes = referenceDataService.getProductTypes();
        productTypes
          .$loaded()
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
          });
      }

      function populateOperativeNames(){
        var operatives = schema.getArray(schema.getRoot().child("operatives"));
        operatives
          .$loaded()
          .then(function () {
            angular.forEach(operatives, function (operative) {
              operativeNames[operative.$id] = operative.name;
            });
            operatives.$destroy();
          });
      }

      function aggregatePanels() {
        var promises = [];     
        var panelsArr = [];

        angular.forEach(matchingAreas, function (area, areaRef) {
          promises.push(
            panelsService.panelsForArea(areaRef)
              .$loaded()
              .then(function (panels) {

                angular.forEach(panels, function (panel) {
                  
                  panel._productionLine = productTypeProductionLine[panel.type];
                  panel._startDate = areaStartDate[areaRef];
                  var framingStyle = "None";
                  if (angular.isDefined(panel.additionalInfo) && angular.isDefined(panel.additionalInfo.framingStyle) && panel.additionalInfo.framingStyle !== "undefined") {
                    framingStyle = panel.additionalInfo.framingStyle;
                    if(framingStyle.indexOf("Head") !== -1){
                      framingStyle = framingStyle.replace(" Head", "");
                    }
                    if(framingStyle.indexOf("Sill") !== -1){
                      framingStyle = framingStyle.replace(" Sill", "");
                    }
                  }
                  if (angular.isUndefined(matchingPanels[panel._productionLine])) {
                    matchingPanels[panel._productionLine] = {};
                  }
                  if (angular.isUndefined(matchingPanels[panel._productionLine][framingStyle])) {
                    matchingPanels[panel._productionLine][framingStyle] = {};
                  }

                  var startDate = new Date(panel._startDate);
                  var week = dateUtil.weekStart(startDate);

                  angular.forEach(weeks, function (week) {
                    var start = new Date(week);
                    var startWeek = dateUtil.weekStart(start);
                    if (angular.isUndefined(matchingPanels[panel._productionLine][framingStyle][startWeek])) {
                      matchingPanels[panel._productionLine][framingStyle][startWeek] = 0;
                    }
                  });
                  if (panelsArr.indexOf(panel.id) === -1 && angular.isDefined(panel.dimensions) && angular.isDefined(panel.dimensions.area) && angular.isDefined(matchingPanels[panel._productionLine][framingStyle][week])){
                    matchingPanels[panel._productionLine][framingStyle][week] += panel.dimensions.area;
                  }
                  panelsArr.push(panel.id);
                });
              })
          );
        
        });
        return $q.all(promises);
      }
      
      function processPanels() {
        angular.forEach(matchingPanels, function ( theData, productionLine ) {
          if (angular.isUndefined(chartData[productionLine])) {
            chartData[productionLine] = [];
          }
          angular.forEach(theData, function ( panelData, framingStyle ) {
            var dataOb = {
              "key": framingStyle,
              "values": []
            };
            angular.forEach(panelData, function ( meterSquared, week ) {
              var monthDate = new Date(week);
              var value;
              if (isTest) {
                var value = {
                  "x": week,
                  "y": meterSquared
                };
              } else {
                var value = {
                  "x": monthDate.getTime(),
                  "y": meterSquared
                };
              }
              dataOb.values.push(value);
            });
            chartData[productionLine].push(dataOb);
          });
        });
      }
      
    }
    
    function getWeeks( weeksAmount, dayzGone ) {
      var weekHolder = [];
      var monday = dateUtil.weekStart(todayWeekStart);
      var firstMonday = dateUtil.minusDays(monday, dayzGone);
      weekHolder.push(firstMonday);
      for (var j = 1; j <= weeksAmount; j++) {
        firstMonday = dateUtil.plusDays(firstMonday, 7);
        weekHolder.push(firstMonday);
      }
      return weekHolder;
    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }

 
  }
  
})();
