(function () {
  "use strict";

  angular
    .module("innView.measures", [
      "nvd3",
      "ngRoute",
      "firebase",
      "floatThead",
      "innView.core",
      "innView.projects",
      "innView.admin"
    ]);

})();
