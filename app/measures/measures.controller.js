(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("MeasuresController", MeasuresController);

  /**
   * @ngInject
   */
  function MeasuresController(measuresService, $timeout, $scope, $window, $location) {

    var vm = this;
    
    var removeWeeksTimeout;
    var modifyChartTimeout;
    
    $scope.$on("$destroy", destroyChart);

    vm.options1 = {
      chart: {
        type: "multiBarChart",
        height: 300,
        margin: {
          top: 20,
          right: 20,
          left: 45
        },
        showControls: false,
        clipEdge: true,
        duration: 500,
        grouped: true,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function (d) {
            return d3.time.format("%b-%d")(new Date(d));
          }
        },
        yAxis: {
          axisLabelDistance: -20,
          tickFormat: function (d) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };
      
    vm.options2 = {
      chart: {
        callback: function (e) {
          modifyChart();
        },
        legend: {
          dispatch: {
            legendClick: function (e) {
              modifyChart();
            },
            stateChange: function (e) {
              modifyChart();
            }
          }
        },
        type: "multiChart",
        height: 300,
        margin: {
          top: 20,
          right: 20,
          left: 45
        },
        color: d3.scale.category10().range(),
        //useInteractiveGuideline: true,
        bars1: {grouped: true},
        bars2: {grouped: true},
        duration: 500,
        xAxis: {
          showMaxMin: false,
          axisLabel: "Week",
          tickFormat: function (d) {
            return d3.time.format("%b-%d")(new Date(d));
          }
        },
        yAxis: {
          axisLabel: "Count",
          axisLabelDistance: 10,
          tickFormat: function (d) {
            return d3.format(",.1f")(d);
          }
        },
        yAxis2: {
          axisLabelDistance: 10,
          tickFormat: function (d) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };

    vm.options3 = {
      chart: {
        type: "multiBarChart",
        height: 300,
        margin: {
          top: 20,
          right: 20,
          left: 45
        },
        clipEdge: true,
        duration: 500,
        grouped: true,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function (d) {
            return d3.time.format("%b-%d")(new Date(d));
          }
        },
        yAxis: {
          axisLabelDistance: -20,
          tickFormat: function (d) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };

    angular.element($window).on("resize", function () {
      if ($window.innerWidth < 600){
        removeWeeks();
      } else {
        modifyChart();
      }
    });

    vm.data1 = [];
    vm.data2 = [];
    vm.data3 = [];
    vm.loader = true;
    

    measuresService.generateData(false, 24, 84)
      .then(function (data) {
        
        vm.loader = false;

        vm.data1 = [
          {
            type: "bar",
            yAxis: 1,
            key: "SIP",
            color: "red",
            values: data.graphData1["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "HSIP",
            color: "pink",
            values: data.graphData1["HSIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "IFAST",
            color: "yellow",
            values: data.graphData1["IFAST"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF",
            color: "green",
            values: data.graphData1["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS",
            color: "blue",
            values: data.graphData1["CASS"]
          }
        ];

        vm.data2 = [
          {
            type: "bar",
            yAxis: 1,
            key: "SIP",
            color: "red",
            values: data.graphData2["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "HSIP",
            color: "pink",
            values: data.graphData2["HSIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "IFAST",
            color: "yellow",
            values: data.graphData2["IFAST"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF",
            color: "green",
            values: data.graphData2["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS",
            color: "blue",
            values: data.graphData2["CASS"]
          }
        ];

        vm.data3 = [
          {
            type: "bar",
            yAxis: 2,
            key: "SIP",
            color: "red",
            values: data.graphData3["SIP"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "HSIP",
            color: "pink",
            values: data.graphData3["HSIP"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "IFAST",
            color: "yellow",
            values: data.graphData3["IFAST"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "TF",
            color: "green",
            values: data.graphData3["TF"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "CASS",
            color: "blue",
            values: data.graphData3["CASS"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "SIP Capacity",
            color: "grey",
            values: data.graphFutureData3["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "HSIP Capacity",
            color: "grey",
            values: data.graphFutureData3["HSIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "IFAST",
            color: "yellow",
            values: data.graphData3["IFAST"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF Capacity",
            color: "grey",
            values: data.graphFutureData3["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS Capacity",
            color: "grey",
            values: data.graphFutureData3["CASS"]
          }
        ];
        
        vm.data4 = [
          {
            type: "bar",
            yAxis: 1,
            key: "SIP",
            color: "red",
            values: data.graphData4["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "HSIP",
            color: "pink",
            values: data.graphData4["HSIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "IFAST",
            color: "yellow",
            values: data.graphData4["IFAST"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF",
            color: "green",
            values: data.graphData4["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS",
            color: "blue",
            values: data.graphData4["CASS"]
          }
        ];
        vm.data5 = [
          {
            type: "bar",
            yAxis: 1,
            key: "SIP",
            color: "red",
            values: data.graphData5["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "HSIP",
            color: "pink",
            values: data.graphData5["HSIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "IFAST",
            color: "yellow",
            values: data.graphData5["IFAST"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF",
            color: "green",
            values: data.graphData5["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS",
            color: "blue",
            values: data.graphData5["CASS"]
          }
        ];
        
        var currentUrl = $location.url();
        
        if (currentUrl === "/designBuffer"){
          vm.options1.chart.yDomain = [0, data.maxDesignedBuffer];
          vm.options2.chart.yDomain1 = [0, data.maxDesignedBuffer];
          vm.options2.chart.yDomain2 = [0, data.maxDesignedBuffer];
        } else{
          vm.options1.chart.yDomain = [0, data.maxDeliveryBuffer];
          vm.options2.chart.yDomain1 = [0, data.maxDeliveryBuffer];
          vm.options2.chart.yDomain2 = [0, data.maxDeliveryBuffer];
        }

      });
      
    function modifyChart() {

      modifyChartTimeout = $timeout(function () {
        var xpos = [];
        var b = true;
        var allRects = d3.select(".chart3").selectAll("g.nv-group").selectAll("rect.positive");
        angular.forEach(allRects, function (rects) {
          angular.forEach(rects, function (rect) {
            var d = rect.__data__;
            if (d.series === 1 && d.key === "TF (right axis)") {
              b = !b;
              if (b) {
                var transform = d3.select(rect).attr("transform");
                var x = d3.select(rect).attr("x");
                var width = d3.select(rect).attr("width");
                var widthHalf = width / 2;
                var newX = x - widthHalf;
                xpos.push({
                  "transform": transform,
                  "date": d.x,
                  "x": newX
                });
              }
            }
          });
        });
        d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll("line").remove();
        var allTexts = d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll("text");
        angular.forEach(allTexts, function (texts) {
          angular.forEach(texts, function (text) {
            var d = text.__data__;
            if (d !== "Week") {
              d3.select(text).remove();
            } else {
              for (var i = 0; i < xpos.length; i++) {
                d3.select(text.parentNode).append("text")
                  .attr("transform", xpos[i].transform)
                  .text(function () {
                    return d3.time.format("%b-%d")(new Date(xpos[i].date));
                  })
                  .attr("y", function () {
                    return 12;
                  })
                  .attr("x", function () {
                    return xpos[i].x;
                  })
                  .style("fill", "black");
              }
            }
          });
        });
        d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll(".tick").remove();
      }, 100);
    }

    function removeWeeks() {

      removeWeeksTimeout = $timeout(function () {
        d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll("line").remove();
        d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll("text").remove();
        d3.select(".chart3").select(".nv-x.nv-axis > g").selectAll("g").selectAll(".tick").remove();
      }, 100);
    }
    
    function destroyChart() {

      if (modifyChartTimeout) {
        $timeout.cancel(modifyChartTimeout);
      }
      if (removeWeeksTimeout) {
        $timeout.cancel(removeWeeksTimeout);
      }
      
      angular.element($window).off("resize");
    }

  }

})();
