(function () {
  "use strict";

  angular
    .module("innView.measures")
    .controller("ForecastController", ForecastController);

  /**
   * @ngInject
   */
  function ForecastController(forecastService, $scope, $log, $window, $timeout, dateUtil) {

    var vm = this;
    
    vm.chartAPI = {};
    vm.options = {};
    
    var modifyChartTimeout;
    
    var staticAverages = {};
    staticAverages["SIP"] = 3875 / 4;
    staticAverages["TF"] = 3798 / 4;
    staticAverages["CASS"] = 1171 / 4;

    var staticMaxAverages = {};
    staticMaxAverages["SIP"] = 5067 / 4;
    staticMaxAverages["TF"] = 4805 / 4;
    staticMaxAverages["CASS"] = 1740 / 4;
    
    $scope.$on("$destroy", destroyChart);
    
    angular.forEach(staticAverages, function (averages, productionLine) {
      
      vm.options[productionLine] = {
        chart: {
          callback: function (e) {
            theCallBack(e, productionLine);
          },
          legend: {
            dispatch: {
              legendClick: function (e) {
                if (vm.chartAPI){
                  vm.chartAPI[productionLine].refreshWithTimeout(100);
                }
              },
              stateChange: function (e) {
                if (vm.chartAPI){
                  vm.chartAPI[productionLine].refreshWithTimeout(100);
                }
              }
            }
          },
          hasClick: false,
          type: "stackedAreaChart",
          height: 300,
          margin: {
            top: 20,
            right: 20,
            bottom: 30,
            left: 60
          },
          x: function (d) {
            return d[0];
          },
          y: function (d) {
            return d[1];
          },
          useVoronoi: false,
          clipEdge: true,
          duration: 100,
          useInteractiveGuideline: true,
          showXAxis: "true",
          showYAxis: "true",
          stacked: {
            dispatch: {
              areaClick: function (e, u) {
                areaClick(e, productionLine);
              }
            }
          },
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format("%d-%b-%y")(new Date(d));
            },
            tickPadding: 40
          },
          yAxis: {
            tickFormat: function (d) {
              return d3.format(",.2f")(d);
            }
          }
        }
      };

    });

    angular.element($window).on("resize", function (){
      angular.forEach(staticAverages, function (averages, productionLine) {
        if (vm.chartAPI) {
          vm.chartAPI[productionLine].refreshWithTimeout(100);
        }
      });
    });
    
    forecastService.getData(false, 48, 0)
      .then(function (data) {
        vm.chartData = data.chartData;
        vm.monthData = data.monthData;
        vm.projectsByGateway = data.projectsByGateway;
        vm.monthProjectsByGateway = data.monthProjectsByGateway;
      });
      
    function modifyChart(productionLineHolder) {

      modifyChartTimeout = $timeout(function () {
        
        var productionLine = productionLineHolder;
        var theClass = "." + productionLine;
        
        var allRects = d3.select(theClass).selectAll("g.nv-group").selectAll("path");
        var parent = d3.select(theClass).select(".nv-x.nv-axis");

        var weekPos = []; 
        var monthPos = [];
        
        //last text is max y axis
        var maxY = 0;
        var allYAxis = d3.select(theClass).select(".nv-y.nv-axis > g").selectAll("text");
        if (angular.isDefined(allYAxis[0])){
          maxY = allYAxis[0][allYAxis[0].length-1].innerHTML;
          maxY = maxY.replace(",","");
        } 

        // drawing the line
        var svg = d3.select(theClass).select("svg");        
        
        //remove all previous placements
        svg.select(".average").remove();
        svg.select(".maxAverage").remove();
        parent.selectAll("text").remove();
        parent.selectAll(".monthRect").remove();
        
        var textStaticAverages = staticAverages[productionLine] + " m2";
        var textStaticMaxAverages = staticMaxAverages[productionLine] + " m2";
        
        if (staticAverages[productionLine] <= maxY) {  
          drawLine("average", staticAverages[productionLine], "black", textStaticAverages, theClass, productionLine, false);
        }
        
        if (staticMaxAverages[productionLine] <= maxY) {          
          drawLine("maxAverage", staticMaxAverages[productionLine], "black", textStaticMaxAverages, theClass, productionLine, true);
        }
          
        // more space at the bottom
        svg.style("height", "350px");
          
        angular.forEach(allRects[0], function (path, key) {
          var transform = d3.select(path).attr("transform");
          var splitted = transform.split(",");
          var transformX = ~~splitted [0].split("(")[1];
          var transformY = 0;
          
          var dates = vm.chartData[productionLine][0].values;
          
          if (angular.isDefined(dates[key - 1])) {
            
            var date = new Date(dates[key][0]);
            var month = date.getMonth();

            var previousDate = new Date(dates[key - 1][0]);
            var previousMonth = previousDate.getMonth();
            
            if (month !== previousMonth) {

              monthPos.push({
                "transformY": 50,
                "transformX": transformX,
                "height": "25px",
                "date": dates[key][0]
              });
            }

          }
          
          weekPos.push({
            "transformY": transformY,
            "transformX": transformX,
            "date": dates[key][0],
            "style":"text-anchor: left;",
            "dy":".71em",
            "y":7
          });

        });
        
        //append weeks
        if ($window.innerWidth > 900){
          angular.forEach(weekPos, function (week) {
            parent.append("text")
              .attr("transform", "translate(" + [week.transformX, week.transformY] + ")rotate(45)")
              .attr("style", week.style)
              .attr("dy", week.dy)
              .attr("y", week.y)
              .text(function () {
                return d3.time.format("%d-%b")(new Date(week.date));
              })
              .style("fill", "black");
          });
        }
        
        //append rects
        var once = true;
        angular.forEach(monthPos, function (month, i) {
          if (once){
            once = false;
            parent.append("rect")
              .attr("class", "monthRect")
              .attr("x", 0)
              .attr("transform", "translate(" + [0, month.transformY] + ")")
              .attr("y", 3)
              .attr("width", month.transformX+"px")
              .attr("height", month.height)
              .style("fill", "rgb(255,255,255)")
              .style("stroke-width", "2")
            .style("opacity","0.6")
              .style("stroke", "rgb(0,0,0)");
          }
          if (i === monthPos.length-1){
            var width = weekPos[weekPos.length-1].transformX - month.transformX;
          }else{
            var width = monthPos[i+1].transformX - month.transformX;
          }

          parent.append("rect")
           .attr("class", "monthRect")
           .attr("transform", "translate(" + [month.transformX, month.transformY] + ")")
           .attr("x", 0)
           .attr("y", 3)
           .attr("width", width+"px")
           .attr("height", month.height)
           .style("fill", "rgb(255,255,255)")
           .style("stroke-width","2")
           .style("opacity","0.6")
           .style("stroke","rgb(0,0,0)");
         
        });
        
        //append months
        if ($window.innerWidth > 900) {
          var once = true;
          angular.forEach(monthPos, function (month, i) {
            if (once) {
              once = false;
              var textTransformX = month.transformX / 2 - 20;
              if (month.transformX > 40) {
                parent.append("text")
                  .attr("class", "monthText")
                  .attr("x", 0)
                  .attr("transform", "translate(" + [textTransformX, month.transformY] + ")")
                  .attr("y", 22)
                  .attr("width", month.transformX + "px")
                  .attr("height", month.height)
                  .style("text-anchor", "center")
                  .text(function () {
                    return d3.time.format("%b")(new Date(weekPos[i].date));
                  })
                  .style("fill", "black")
                  .style("font-size", "1.3em");
              }
            }
            if (i === monthPos.length - 1) {
              var width = weekPos[weekPos.length - 1].transformX - month.transformX - 25;
            } else {
              var width = monthPos[i + 1].transformX - month.transformX - 25;
            }

            var textTransformX = month.transformX + (width / 2);

            if (width > 40) {
              parent.append("text")
                .attr("class", "monthText")
                .attr("transform", "translate(" + [textTransformX, month.transformY] + ")")
                .attr("x", 0)
                .attr("y", 22)
                .attr("width", width + "px")
                .attr("height", month.height)
                .style("text-anchor", "center")
                .text(function () {
                  return d3.time.format("%b")(new Date(month.date));
                })
                .style("fill", "black")
                .style("font-size", "1.3em");

            }
          });
        }
        
      }, 100);
    }
    
    function drawLine(lineId, value, color, text, theClass, productionLine, dash) {
      
      var chart = vm.chartAPI[productionLine].getScope().chart;
      var svg = d3.select(theClass).select("svg");
      var yScale = chart.yAxis.scale();
      var margin = chart.margin();
      var height = chart.height();
      var width = svg.style("width").split("px")[0];
      
      if (dash) {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-dasharray", "10, 5")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      } else {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-width", "1px")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      }
 
      svg.append("text")
        .style("stroke", color)
        .attr("class", lineId + "-text")
        .attr("x", +width - margin.right / 2)
        .attr("y", yScale(value) + margin.top)
        .attr("text-anchor", "end")
        .text(text);
    }
    
    function theCallBack(e, productionLine){
      if (angular.isDefined(e)) {
        var tooltip = e.interactiveLayer.tooltip;
        tooltip.contentGenerator(function (event) {
          var textEntry = buildInteractiveInfo(event, productionLine);
          vm.options[productionLine].chart.textEntry = textEntry;
          return textEntry;
        });
        modifyChart(productionLine);
      }
    }
    
    function areaClick(e, productionLine){
      if (vm.chartAPI[productionLine] && angular.isDefined(e)) {
        if (!vm.options[productionLine].chart.hasClick) {
          var chart = vm.chartAPI[productionLine].getScope().chart;
          var tooltip = chart.interactiveLayer.tooltip;
          tooltip.contentGenerator(function (d) {
            var textEntry = buildAreaClickInfo(d, e, productionLine);
            return textEntry;
          });
          vm.options[productionLine].chart.hasClick = true;
        } else {
          vm.chartAPI[productionLine].refreshWithTimeout(100);
          vm.options[productionLine].chart.hasClick = false;
        }
      }
    }
    
    function buildInteractiveInfo(event, productionLine) {
      var week = d3.time.format("%d-%b-%y")(new Date(event.value));
      var month = d3.time.format("%Y-%m")(new Date(event.value));
      var humanMonth = d3.time.format("%B")(new Date(event.value));
      var monthTotal = 0;
      var textEntry = "";
      textEntry += "<table style='display: inline-block;border-style: dashed;padding:2px;margin:2px;><thead><tr><td colspan='4'></td></tr></thead><tbody><caption style='font-weight: bold;color:black;text-align:center;'>" + week + "</caption>";
      angular.forEach(event.series, function (data) {
        if (data.key !== "TOTAL") {
          textEntry += "<tr><td class='legend-color-guide'><div style='background-color: " + data.color + ";'></div></td><td class='key'>" + data.key + "</td><td class='value'>" + Math.round(data.value) + "</td><td class='value'>" + Math.round(vm.monthData[productionLine][data.key][month]) + "</td></tr>";
          monthTotal += vm.monthData[productionLine][data.key][month];
        } else {
          textEntry += "<tr><td class='legend-color-guide'><div style='background-color: " + data.color + ";'></div></td><td class='key'>" + data.key + "</td><td class='value'>" + Math.round(data.value) + "</td><td class='value'>" + Math.round(monthTotal) + "</td></tr>";
        }
      });
      textEntry += "</tbody>";
      textEntry += "</table>";

      textEntry += "<table style='display: inline-block;border-style: dashed;padding:2px;margin:2px;'>";
      textEntry += "<caption style='font-weight: bold;color:black;text-align:center;'>Projects in "+humanMonth+"</caption>";
      textEntry += "<tr>";
      angular.forEach(event.series, function (data) {
        if (data.key !== "TOTAL" && vm.monthProjectsByGateway[productionLine][data.key][month].length > 0) {
          textEntry += "<td><strong>" + data.key + "</strong></td>";
          textEntry += "<tr>";
          angular.forEach(vm.monthProjectsByGateway[productionLine][data.key][month], function (projectId) {
            textEntry += "<td>" + projectId + "</td>";
          });
          textEntry += "</tr>";
        }
      });
      textEntry += "</tr>";
      textEntry += "</table>";
      return textEntry;
    }
    
    function buildAreaClickInfo(d, e, productionLine){
      var date = new Date(d.value);
      var week = dateUtil.weekStart(date);
      var humanWeek = d3.time.format("%d-%b-%y")(new Date(week));
      var textEntry = "";
      textEntry += "<table>";
      textEntry += "<thead>";
      textEntry += "<tr>";
      textEntry += "<th>Projects for " + e.series + " this Week ("+humanWeek+"):</th>";
      textEntry += "</tr>";
      textEntry += "</thead>";
      textEntry += "<tbody>";
      textEntry += "<thead>";
      textEntry += "<tr>";
      angular.forEach(vm.projectsByGateway[productionLine][e.series][week], function (projectId) {
        textEntry += "<td>" + projectId + "</td>";
      });
      textEntry += "</tr>";
      textEntry += "</tbody>";
      textEntry += "</table>";
      return textEntry;
    }
    
    function destroyChart() {
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (modifyChartTimeout) {
        $timeout.cancel(modifyChartTimeout);
      }
      
      angular.element($window).off("resize");
    }
    
  }

})();
