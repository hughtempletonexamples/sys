(function () {
  "use strict";

  require("angular-material/angular-material.css");
  require("ng-material-datetimepicker/css/material-datetimepicker.css");
  require("./stylesheets/newux-override.css");

  require("angular");
  require("angular-route");
  require("angular-animate");
  require("angular-messages");
  require("angular-confirm");
  require("angular-aria");
  require("angular-material");
  require("angular-nvd3");
  require("ng-material-datetimepicker");

  require("./module-newux");
  require("./config.route");

  require("./core");
  require("./login");
  require("./home");
  require("./projects/newux");
  require("./manage");
  require("./admin");
  require("./productionperformance");
  require("./materialrequests");
  require("./materialanalysis");
  require("./measures");
  require("./dashboard/newux");
})();
