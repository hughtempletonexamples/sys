(function () {
  "use strict";

  angular.module("innView", [
    "ngRoute",
    "ngAnimate",
    "ngTouch",
    "ngMessages",
    "ui.bootstrap",
    "firebase",
    "angular-confirm",
    "nvd3",
    "innView.core",
    "innView.login",
    "innView.home",
    "innView.projects",
    "innView.manage",
    "innView.admin",
    "innView.productionperformance",
    "innView.materialrequests",
    "innView.materialanalysis",
    "innView.measures",
    "innView.dashboard"
  ]);
})();
