(function () {
  "use strict";

  angular
    .module("innView.admin")
    .controller("ExportController", ExportController);

  /**
   * @ngInject
   */
  function ExportController($log, Blob, FileSaver, XLSX, exportService, dateUtil) {
    var XLSX_MIMETYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    var vm = this;

    vm.exportXLSX = exportXLSX;

    init();

    ////////////

    function init() {
      vm.fromDate = dateUtil.subtractWorkingDays(new Date(), 20);
      vm.toDate = new Date();
    }

    function exportXLSX(valid) {
      $log.log("Is valid: " + valid);
      if (valid) {
        $log.log("Exporting...");
        var fromDate = dateUtil.toIsoDate(new Date(vm.fromDate));
        var toDate = dateUtil.toIsoDate(new Date(vm.toDate));
        exportService.getData(fromDate, toDate)
          .then(function (data) {

            var workbook = exportService.createWorkbook(data);

            var options = { bookType:"xlsx", bookSST:false, type:"binary" };

            var xlsx = XLSX.write(workbook, options);

            var filename = "export_from_" + fromDate + "_to_" + toDate + ".xlsx";
            FileSaver.saveAs(new Blob([stringToArrayBuffer(xlsx)],{type: XLSX_MIMETYPE}), filename);
            $log.log("Export done.");
          });
      }

      // Utility function to create buffer from string.
      // Based on documentation for js-xslx library.
      function stringToArrayBuffer(string) {
        var buffer = new ArrayBuffer(string.length);
        var view = new Uint8Array(buffer);

        for (var i = 0; i != string.length; i++) {
          view[i] = string.charCodeAt(i) & 0xFF;
        }
        return buffer;
      }
    }
  }
})();
