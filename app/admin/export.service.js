(function () {
  "use strict";

  angular
    .module("innView.admin")
    .factory("exportService", exportService);

  /**
   * @ngInject
   */
  function exportService($q, $filter, schema, referenceDataService, projectsService, areasService, panelsService, plannerService, dateUtil) {
    var DATE_FORMAT = "yyyy-MM-dd";
    var TIME_FORMAT = "HH:mm:ss";

    var productionPlans = {};

    var service = {
      getData: getData,
      createWorkbook: createWorkbook
    };

    init();

    return service;

    ////////////

    function init() {
      // Polyfill of array find function, required by IE
      if (!Array.prototype.find) {
        Array.prototype.find = function (predicate) {
          if (this === null) {
            throw new TypeError("Array.prototype.find called on null or undefined");
          }
          if (!angular.isFunction(predicate)) {
            throw new TypeError("predicate must be a function");
          }
          var list = Object(this);
          var length = list.length >>> 0;
          var thisArg = arguments[1];
          var value;

          for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
              return value;
            }
          }
          return undefined;
        };
      }

      return plannerService.getProductionPlansByPlannedStart()
        .$loaded()
        .then(function (allPlans) {
          angular.forEach(allPlans, function (plan) {
            productionPlans[plan.$id] = plan;
          });

          return;
        });
    }

    function getData(fromDate, toDate) {
      var matchingAreas = {};
      var matchingPanels = [];
      var areaUnpublishedDate = [];
      var operativeNames = [];
      var productTypeProductionLine = {};

      var deferred = $q.defer();

      var projects = projectsService.getProjects();
      projects.$loaded()
        .then(processProjects)
        .then(populateOperativeNames)
        .then(getProductTypes)
        .then(aggregatePanels)
        .then(processPanels)
        .then(function (allPanelsData) {
          deferred.resolve(allPanelsData);
        });

      return deferred.promise;

      function processProjects(projects) {
        var promises = [];
        angular.forEach(projects, function (project) {
          if (angular.isDefined(project.deliverySchedule.published)) {
            // get areas with published delivery date within specified range
            promises.push(findAreasWithMatchingDeliveryDate(project));
          }
        });
        return $q.all(promises);
      }

      function findAreasWithMatchingDeliveryDate(project) {
        return areasService.getProjectAreas(project.$id)
          .$loaded()
          .then(function (projectAreas) {
            angular.forEach(projectAreas, function (area) {
              if (angular.isDefined(area.revisions) && angular.isDefined(area.revisions[project.deliverySchedule.published])) {
                var revisionDate = area.revisions[project.deliverySchedule.published];
                if (revisionDate >= fromDate && revisionDate <= toDate) {
                  matchingAreas[area.$id] = area;
                  areaUnpublishedDate[area.$id] = project.deliverySchedule.published;
                }
              }
            });
          });
      }
      
      function getProductTypes(){
        var productTypes = referenceDataService.getProductTypes();
        productTypes
          .$loaded()
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
          });
      }

      function populateOperativeNames(){
        var operatives = schema.getArray(schema.getRoot().child("operatives"));
        operatives
          .$loaded()
          .then(function () {
            angular.forEach(operatives, function (operative) {
              operativeNames[operative.$id] = operative.name;
            });
            operatives.$destroy();
          });
      }

      function aggregatePanels() {
        var promises = [];

        angular.forEach(matchingAreas, function (area, areaRef) {
          promises.push(
            panelsService.panelsForArea(areaRef)
              .$loaded()
              .then(function (panels) {
                var deliveryDate = panelsDeliveryDate(area, panels);
                angular.forEach(panels, function (panel) {
                  panel.deliveryDate = deliveryDate;
                  matchingPanels.push(panel);
                });
              })
          );
        
        });
        return $q.all(promises);
      }
      
      function panelsDeliveryDate(area, panels) {  
        
        var x = 0;
        var earliestBuildStart;
        var buildStartDates = [];
        var daysDifference;
        var daysDifferenceArray = [];
        var days = [];
        var daysRemovedNegatives = [];
        var closestNumber;
        var deliveryDate = "";

        if(panels.length){
        
          angular.forEach(area.revisions, function (revisionDate, revisionRef) {
            
            if (areaUnpublishedDate[area.$id] !== revisionRef) {

              angular.forEach(panels, function (panel) {

                var buildStartDate;
                var completedStatus = determineCompletedStatus(panel.qa);
                if (completedStatus === "In Progress") {
                  buildStartDate = $filter("date")(getQAValue(panel.qa, "started"), DATE_FORMAT);
                  buildStartDates.push(buildStartDate);
                }
              });

              if (buildStartDates.length) {

                buildStartDates.sort(function (a, b) {
                  return Date.parse(a) - Date.parse(b);
                });

                earliestBuildStart = buildStartDates[0];

                daysDifference = dateUtil.daysBetween(earliestBuildStart, revisionDate);

                daysDifferenceArray.push({
                  revisionDate: revisionDate,
                  daysDifference: daysDifference
                });
                
                days.push(daysDifference);

              }
              
            }

          });
          
          if (days.length) {
            days = unique(days);
            closestNumber = getClosestNum(0, days);
            angular.forEach(daysDifferenceArray, function (value) {
              if (value.daysDifference === closestNumber) {
                deliveryDate = value.revisionDate;
              }
            });
          }

          return deliveryDate;        
        }else{
          return deliveryDate;
        }

      }

      function processPanels() {
        var promises = [];
        var allPanelsData = [];
        angular.forEach(matchingPanels, function (panel) {
          promises.push(
            extractPanelData(
              projects.find(function (item) {
                return item.$id === panel.project;
              }),
              matchingAreas[panel.area],
              panel
            )
          );
        });

        return $q.all(promises);
      }

      function extractPanelData(project, area, panel) {

        var panelData = {};

        var idParts = panel.id.split("-");
        var panelType = idParts[4];
        var panelNumber = idParts[5];

        var panelData = {};
        panelData["dateCreated"] = $filter("date")(matchingAreas[panel.area].timestamps.created, DATE_FORMAT);
        panelData["project"] = project.id;
        panelData["panelNumber"] = panel.id;
        panelData["projectName"] = project.name;
        panelData["jobsheetReference"] = project.id + "-" + area.phase + "-" + area.floor + "-" + panelType;
        panelData["phase"] = area.phase;
        panelData["productionLine"] = productTypeProductionLine[panel.type];
        panelData["floor"] = area.floor;
        panelData["number"] = panelNumber;
        panelData["designHeight"] = panel.dimensions.height;
        panelData["designLength"] = panel.dimensions.length;
        panelData["designArea"] = panel.dimensions.area;
        panelData["designWeight"] = panel.dimensions.weight;
        panelData["designWidth"] = angular.isDefined(panel.dimensions.width) ? panel.dimensions.width : panel.dimensions.depth;
        panelData["designMP"] = "";
        panelData["jobsheets"] = area.phase + "-" + area.floor + "-" + panelType;
        panelData["dateCompleted"] = $filter("date")(getQAValue(panel.qa, "completed"), DATE_FORMAT);
        panelData["actualHeight"] = getQAValue(panel.qa, "midHeight");
        var midLength = getQAValue(panel.qa, "midLength");
        panelData["actualLength"] = midLength !== "" ? midLength : getQAValue(panel.qa, "midWidth");
        panelData["actualLRDiag"] = getQAValue(panel.qa, "diagonalWidth");
        panelData["actualRLDiag"] = getQAValue(panel.qa, "diagonalHeight");
        panelData["operatorSignoff"] = "";
        panelData["supervisorSignoff"] = "";
        panelData["completedStatus"] = determineCompletedStatus(panel.qa);

        panelData["deliveryDate"] = panel.deliveryDate;
        panelData["areaType"] = area.type;
        panelData["panelType"] = panelType;
        panelData["uploadDate"] = $filter("date")(panel.timestamps.created, DATE_FORMAT);
        var productionPlan = productionPlans[panel.area];
        panelData["areaPlannedStart"] = productionPlan ? $filter("date")(productionPlans[panel.area].plannedStart, DATE_FORMAT) : "";
        panelData["areaPlannedFinish"] = productionPlan ? $filter("date")(productionPlans[panel.area].plannedFinish, DATE_FORMAT) : "";
        var startedDate = getQAValue(panel.qa, "started");
        var finishAssembly = getQAValue(panel.qa, "finishAssembly");
        var startLayup = getQAValue(panel.qa, "startLayup");
        var finishDate = getQAValue(panel.qa, "completed");
        panelData["buildStartDate"] = $filter("date")(startedDate, DATE_FORMAT);
        panelData["buildStartTime"] = $filter("date")(startedDate, TIME_FORMAT);
        panelData["buildFinishDate"] = $filter("date")(finishDate, DATE_FORMAT);
        panelData["buildFinishTime"] = $filter("date")(finishDate, TIME_FORMAT);
        var buildDuration = getDuration(finishDate, startedDate);
        var wipDuration = getDuration(startLayup, finishAssembly);
        var assemblyDuration = getDuration(finishAssembly, startedDate);
        var LayupDuration = getDuration(finishDate, startLayup);
        panelData["assemblyDuration"] = assemblyDuration > 0 ? assemblyDuration : "";
        panelData["wipDuration"] = wipDuration > 0 ? wipDuration : "";
        panelData["LayupDuration"] = LayupDuration > 0 ? LayupDuration : "";
        panelData["buildDuration"] = buildDuration > 0 ? buildDuration : "";
        panelData["projectDatum"] = project.datumType;
        panelData["line"] = angular.isDefined(area.line) ? area.line : 1;
        panelData["operatives"] = getOperatives(panel.qa, "operatives");
        panelData["startedOperatives"] = getOperatives(panel.qa, "startedOperatives");
        panelData["finishAssemblyOperatives"] = getOperatives(panel.qa, "finishAssemblyOperatives");
        panelData["startLayupOperatives"] = getOperatives(panel.qa, "startLayupOperatives");
        panelData["completedOperatives"] = getOperatives(panel.qa, "completedOperatives");
        panelData["framingStyle"] = "";
        if(angular.isDefined(panel.additionalInfo) && angular.isDefined(panel.additionalInfo.framingStyle)){
          panelData["framingStyle"] = panel.additionalInfo.framingStyle;
        }
            
        return panelData;

      }
      
      function getDuration(finishDate, startedDate){
        if (finishDate !== "" && startedDate !== ""){
          return (finishDate - startedDate) / 1000;
        }else{
          return "";
        }
      }
      
      function getOperatives(qa, type) {
        var operatives = [];
        if (angular.isDefined(qa) && angular.isDefined(qa[type])) {
          angular.forEach(qa[type], function (opreative, operativeKey) {
            var opName = operativeNames[operativeKey];
            operatives.push(opName);
          });
          return operatives.join(", ");
        } else {
          return "";
        }
      }
    
    }
    
    function getQAValue(qa, key) {
      return angular.isDefined(qa) && qa[key] ? qa[key] : "";
    }

    function determineCompletedStatus(qa) {

      if (angular.isDefined(qa)) {
        if (angular.isDefined(qa["completed"])) {
          return "Completed";
        } else if (angular.isDefined(qa["started"])) {
          return "In Progress";
        }
      } else {
        return "Planned";
      }
    }
    
    function unique(ary) {
      // concat() with no args is a way to clone an array
      var u = ary.concat().sort();
      for (var i = 1; i < u.length; ) {
        if (u[i - 1] === u[i])
          u.splice(i, 1);
        else
          i++;
      }
      return u;
    }
    
    function getDublicates(arr) {
      var cache = {};
      var results = [];
      for (var i = 0, len = arr.length; i < len; i++) {
        if (cache[arr[i]] === true) {
          results.push(arr[i]);
        } else {
          cache[arr[i]] = true;
        }

      }
      return arr;
    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }
    
    function removeNegatives(x) {
      var temp;
      for (var i = x.length - 1; i >= 0; i--) {
        if (x[i] < 0) {
          if (i !== x.length - 1) {
            // swap current value with last value
            temp = x[i];
            x[i] = x[x.length - 1];
            x[x.length - 1] = temp;
          }
          x.pop();
        }
      }
      return x;
    }
    
    function getNegatives(x) {
      var temp = [];
      for (var i = x.length - 1; i >= 0; i--) {
        if (x[i] < 0) {
          temp.push(x[i]);
        }
      }
      return temp;
    }
    
    function negativesToPositives(x) {
      var temp = [];
      for (var i = x.length - 1; i >= 0; i--) {
        if (x[i] < 0) {
          temp.push(Math.abs(x[i]));
        }
      }
      return temp;
    }
    
    function getClosestNum(num, ar)
    {
      var i = 0;
      var closest;
      var closestDiff;
      var currentDiff;
      var dublicates = 0;
      var closestToPositive;

      if (ar.length)
      {
        closest = ar[0];
        for (var i; i < ar.length; i++)
        {
          closestDiff = Math.abs(num - closest);
          currentDiff = Math.abs(num - ar[i]);
          if (currentDiff < closestDiff)
          {
            closest = ar[i];
          }
          closestDiff = null;
          currentDiff = null;
        }

        closestToPositive = Math.abs(closest);

        for (i = 0; i < ar.length-1; i++) {
          if (ar[i] === closestToPositive) {
            dublicates++;
          }
        }

        if(dublicates > 0){
          return closestToPositive;
        }
        
        //returns first element that is closest to number
        return closest;
      }
      //no length
      return false;
    }

    function createWorkbook(data) {
      var wb = new Workbook();
      wb.SheetNames.push("DATA");
      wb.Sheets["DATA"] = createWorksheet(data);
      return wb;
    }

    function createWorksheet(data) {
      var worksheet = {};
      var headers = [
        "Date Created",
        "Project",
        "Panel Number",
        "Project Name",
        "Jobsheet Reference",
        "Phase",
        "Production Line",
        "Floor",
        "Number",
        "Design Height",
        "Design Length",
        "Design Area",
        "Design Weight",
        "Design Width",
        "Design MP",
        "Jobsheets",
        "Date Completed",
        "Actual Height",
        "Actual Length",
        "Actual L-R-Diag",
        "Actual R-L-Diag",
        "Operator signoff",
        "Supervisior signoff",
        "Completed Status",
        "Delivery Date",
        "Area Type",
        "Panel Type",
        "Panel Upload Date",
        "Area Planned Start",
        "Area Planned Finish",
        "Build Start Date",
        "Build Start Time",
        "Build Finish Date",
        "Build Finish Time",
        "Assembly Duration",
        "WIP Duration",
        "Layup Duration",
        "Build Duration",
        "Project Datum",
        "Line",
        "Operatives",
        "Operatives Started",
        "Operatives Assembly Finish",
        "Operatives Layup Started",
        "Operatives Completed",
        "Framing Style"
        
      ];
      // range is used to keep track of ranges in the sheet
      // without it, you don't get any data in your spreadsheet
      var range = {
        s: {
          c: 0,
          r: 0
        },
        e: {
          c: 0,
          r: 0
        }
      };

      var cell = {};

      // set column headers
      for (var column = 0; column <= headers.length; column++) {
        if(range.e.c < column) {
          range.e.c = column;
        }

        cell = {
          v: headers[column],
          t: "s"
        };

        worksheet[XLSX.utils.encode_cell({c:column, r:0})] = cell;
      }

      for (var row = 0; row <= data.length; row++) {
        if (range.e.r < row+1) {
          range.e.r = row+1;
        }

        if (angular.isDefined(data[row])) {

          var keys = Object.keys(data[row]);
          for (column = 0; column <= keys.length; column++) {

            if(range.e.c < column) {
              range.e.c = column;
            }

            // Create cell object, v is the actual value
            cell = {
              v: data[row][keys[column]]
            };

            if (cell.v == null) {
              continue;
            }

            // Create the cell reference.
            var cell_ref = XLSX.utils.encode_cell({c:column, r:row+1});

            // Determine the cell type.

            // We're ignoring dates because excel uses an odd
            // mechanism, counting days since 01/00/1900. It also has
            // leap year bugs, so it's simpler just to use
            // preformatted strings.
            if (angular.isNumber(cell.v)) {
              cell.t = "n"; //Number
            } else if (typeof cell.v === "boolean") {
              cell.t = "b"; //Boolean
            } else {
              cell.t = "s"; //String
            }

            // Add the cell to the worksheet.
            worksheet[cell_ref] = cell;

          }
        }
      }

      // add the ranges to the worksheet
      worksheet["!ref"] = XLSX.utils.encode_range(range);

      return worksheet;
    }

    function Workbook() {
      if (!(this instanceof Workbook)) {
        return new Workbook();
      }
      this.SheetNames = [];
      this.Sheets = {};
    }
  }
})();
