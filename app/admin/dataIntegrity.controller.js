(function () {
  "use strict";
  angular
          .module("innView.admin")
          .controller("dataIntegrityController", dataIntegrityController);

  /**
   * @ngInject
   */
  function dataIntegrityController(dataIntegrityService) {

    var vm = this;

    getData().then(function (data){
      vm.tableData = data;      
    });

    vm.checkIfTrue = checkIfTrue;
    vm.checker;
    vm.sort = undefined;
    vm.toggleView = toggleView();
    vm.change = change;
    vm.checked;
    vm.change = change;
    vm.counteroo = 0;
    vm.gatwayNames =["G1 - Marketing", "G2 - Sales", "G3 - Bid", "G4 - Tender", "G5 - Pre-Order", "G6 - Delivery Strategy", "G7 - Pre-Construction", "G8 - Site Start", "G9 - Site Handover", "G10 - Feedback"];
    function change() {
      vm.counteroo++;
      if (vm.counteroo > 3) {
        vm.counteroo = 0;
        vm.sort = "";
      }
    }
    
    function toggleView(columnKey) {
      vm.checked = true;
    }
    function getData() {
      return dataIntegrityService.getData("2018-03-03", "2018-08-08");
    }
    function checkIfTrue(values) {
      var counter;
      if (angular.isDefined(values)) {
        if (!(Object.keys(values).length === 0)) {
          angular.forEach(values, function (data) {
            if (data === false) {
              counter = 1;
            }
          });
          if (counter === 1) {
            return false;
          } else {
            return true;
          }
        } else {
          return false;
        }
      }
    }
  }
})();