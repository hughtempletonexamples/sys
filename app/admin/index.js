(function () {
  "use strict";

  require("angular-route");

  require("xlsx/dist/jszip");
  require("xlsx/dist/xlsx.min");

  require("./admin.module");
  require("./export.controller");
  require("./export.service");
  require("./audit.controller");
  require("./datums.controller");
  require("./datums.service");
  require("./dataIntegrity.controller");
  require("./dataIntegrity.service");
  
  require("./adminProjects.controller");

  require("./config.route");
})();
