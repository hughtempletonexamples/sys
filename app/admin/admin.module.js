(function () {
  "use strict";

  angular
    .module("innView.admin", [
      "ngRoute",
      "firebase",
      "ngFileSaver",
      "innView.core"
    ]);
})();
