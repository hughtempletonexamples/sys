(function () {
  "use strict";

  angular
    .module("innView.admin")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/admin/export", {
        templateUrl: require("./export.html"),
        controller: "ExportController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
    $routeProvider
      .when("/admin/audit", {
        templateUrl: require("./audit.html"),
        controller: "AuditController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
    $routeProvider
      .when("/admin/datums", {
        templateUrl: require("./datums.html"),
        controller: "DatumsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
    $routeProvider
      .when("/admin/adminProjects", {
        templateUrl: require("./adminProjects.html"),
        controller: "AdminProjectsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
    $routeProvider
      .when("/admin/dataIntegrity", {
        templateUrl: require("./dataIntegrity.html"),
        controller: "dataIntegrityController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
    $routeProvider
      .when("/admin", {
        redirectTo: "/admin/export"
      });

  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }
})();
