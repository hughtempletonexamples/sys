(function () {
  "use strict";

  angular
    .module("innView.admin")
    .controller("AdminProjectsController", AdminProjectsController);

  /**
   * @ngInject
   */
  function AdminProjectsController(projectsService, areasService) {
    var vm = this;

    vm.activeToggle = activeToggle;
    vm.updateAreaLine = updateAreaLine;

    vm.projects = [];

    init();

    ////////////

    function init() {
      projectsService.getProjects()
        .$loaded()
        .then(function (projects) {
          angular.forEach(projects, function (project) {
            if (angular.isUndefined(project.active)) {
              project.active = true;
            }
          });
          vm.projects = projects;
        });
    }

    function activeToggle(project) {
      return projectsService.updateProject(project);
    }
    
    function updateAreaLine(projectKey, value) {

      if (angular.isDefined(projectKey)) {
        return areasService.getProjectAreas(projectKey)
          .$loaded()
          .then(function (areas) {
            angular.forEach(areas, function (area) {
              area.line = value;
              return areasService.updateProjectArea(area);    
            });
          });
      }
    }
  }
})();
