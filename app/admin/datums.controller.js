(function () {
  "use strict";

  angular
    .module("innView.admin")
    .controller("DatumsController", DatumsController);

  /**
   * @ngInject
   */
  function DatumsController($log, schema, datumsService) {
    
    var vm = this;
    
    vm.updateOverride = updateOverride;
    
    init();

    ////////////

    function init() {
      schema.getArray(schema.getRoot().child("datums"))
        .$loaded()
        .then(function (datums){
          vm.datums = datums;
        });
    }
    
    function updateOverride(datumName, datumData){
      datumsService.updateDatum(datumName, datumData);
    }
    
  }
})();
