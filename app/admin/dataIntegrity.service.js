(function () {
  "use strict";

  angular
          .module("innView.admin")
          .factory("dataIntegrityService", dataIntegrityService);

  /**
   * @ngInject
   */
  function dataIntegrityService($q, $log, referenceDataService, projectsService, areasService, materialrequestsService) {

    var projectCopy = {};
    //add items to gateway checks !!# must be identicle to how it is laid out on the server #!!
    // to mark the ones that i currently can reach i have marked them with !!# //check #!!    
    var returnData = {};
    var checkList = {};
    var gatewayChecker = {};
    var gatewayList = {};
    var arrFromObject = [];

    var config = {
      "G3": {
        "name": false, //check
        "key": false, //check
        "Estimated Areas": false, //check
        "Site Access": false, //check
        "Datum Type": false, //check
        "Site Start": false//check
      },
      "G4": {
        "Chain Of Custody": false, //check
        "Offload Method": false, //check
        "Week Duration": false, //check
        "Notify Users": false, //check
        "areas": false, //check
        "revisions": false//check
      },
      "G5": {
        "Special Requirements": false, //check
        "Revised Panel Data(50%)": false, //check
        "10% Percent Of Areas Uploaded": false//check
      },
      "G6": {
        "30% Percent Of Areas Uploaded": false, //check
        "Revised Panel Data(75%)": false, //check
        "Material Request": false//check
      },
      "G7": {
        "50% Percent Of Areas Uploaded": false, //check
        "Revised Panel Data(100%)": false//check
      },
      "G8": {
        "80% Percent Of Areas Uploaded": false//check
      },
      "G9": {
        "All Panels Uploaded": false//check
      }
    };

    var service = {
      getData: getDataAll,
      getDataForProject: getDataForProject
    };

    return service;

    function getDataAll() {

      var deferred = $q.defer();
      var projects = projectsService.getProjects();

      projects.$loaded()
              .then(getGateways)
              .then(processProjects)
              .then(turnToObject)
              .then(function () {
                deferred.resolve(gatewayChecker);
              });

      return deferred.promise;


      function processProjects(gateways) {

        gatewayList = gateways;
        var promises = [];
        var n = 0;
        var counter = 0;

        angular.forEach(projects, function (project) {
          if (project.active) {
            if (angular.isDefined(project.deliverySchedule.published)) {
              if (angular.isUndefined(gatewayChecker[project.id])) {
                gatewayChecker[project.id] = {};
                angular.forEach(gateways, function (gateway, key) {
                  if (angular.isUndefined(gatewayChecker[project.id][gateway.$id])) {
                    if (gateway.$id !== "G10") {
                      gatewayChecker[project.id][gateway.$id] = {};
                      counter++;
                    }
                    if (counter >= 9) {
                      gatewayChecker[project.id]["G10"] = {};
                      counter = 0;
                    }
                  }
                });
              }
              if (angular.isUndefined(returnData[project.id])) {
                returnData[project.id] = {};
                if (n === 0) {
                  angular.copy(project, projectCopy);
                  returnData[project.id] = projectCopy;
                  n++;
                }
              }
              if (angular.isUndefined(projectCopy["materialRequest"])) {
                promises.push(materialRequestCalc(project));
              }
            }
          }
        });
        return $q.all(promises);

      }

      function materialRequestCalc(project) {

        return materialrequestsService.getMaterialRequestByProjectId(project.id)
                .$loaded()
                .then(function (materialRequest) {
                  if (angular.isDefined(materialRequest[0])) {
                    projectCopy["Material Request"] = true;
                  } else {
                    projectCopy["Material Request"] = false;
                  }
                })
                .then(function () {
                  angular.forEach(projectCopy, function (item, key) {
                    calculateBoolean(item, key, project.id, project);
                  });
                  return findAreasWithMatchingDeliveryDate(project);
                });

      }

      function LoopDataKeys(projectId, areaKey, gatewayDataKeys, gatewayId) {

        if (angular.isDefined(checkList[projectId]["areas"][areaKey])) {

          if (angular.isDefined(checkList[projectId][gatewayDataKeys])) {
            if (checkList[projectId][gatewayDataKeys] === false) {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = false;
            } else {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = true;
            }
          }

          if (angular.isDefined(checkList[projectId]["areas"][areaKey][gatewayDataKeys])) {
            if (checkList[projectId]["areas"][areaKey][gatewayDataKeys] === false) {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = false;
            } else {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = true;
            }
          }

        }

      }

      function loopSpecialDataSets(projectId, gatewayId) {

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Special Requirements")) {
          var specReq = "Special Requirements";
          if (angular.isDefined(checkList[projectId][specReq])) {
            gatewayChecker[projectId][gatewayId][specReq] = checkList[projectId]["Special Requirements"];
          }
        }

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Week Duration")) {
          var weekReq = "Week Duration";
          if (angular.isDefined(checkList[projectId][weekReq])) {
            gatewayChecker[projectId][gatewayId][weekReq] = checkList[projectId]["Week Duration"];
          }
        }

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Material Request")) {
          var specReq = "Material Request";
          if (angular.isDefined(checkList[projectId][specReq])) {
            gatewayChecker[projectId][gatewayId][specReq] = checkList[projectId]["Material Request"];
          }
        }

      }

      function compareCheckLists() {

        angular.forEach(gatewayChecker, function (gateways, projectId) {
          if (angular.isDefined(projectId)) {
            angular.forEach(gateways, function (gateway, gatewayId) {
              var configCopy = angular.copy(config);
              gatewayChecker[projectId][gatewayId] = configCopy[gatewayId];
              angular.forEach(gatewayChecker[projectId][gatewayId], function (values, gatewayDataKeys) {
                angular.forEach(checkList[projectId]["areas"], function (area, areaKey) {
                  angular.forEach(area, function (item, dataKey) {
                    if (checkList[projectId].hasOwnProperty(dataKey)) {
                      LoopDataKeys(projectId, areaKey, gatewayDataKeys, gatewayId);
                    }
                  });
                });
                loopSpecialDataSets(projectId, gatewayId);
              });
            });
          }
        });
        return gatewayChecker;

      }

      function calculateBoolean(item, key, id, project) {
        var isValid = true;
        if (angular.isUndefined(item)) {
          isValid = false;
        }
        if (item === false) {
          isValid = false;
        }
        if (angular.isObject(item)) {
          checkList[id][key] = [item];
        }
        if (angular.isUndefined(checkList[id])) {
          checkList[id] = {key: id};
        }
        if (angular.isUndefined(checkList[id][key])) {
          checkList[id][key] = isValid;
        }
        if (project.hasOwnProperty("specialRequirements")) {
          if (angular.isUndefined(checkList[id]["Special Requirements"])) {
            checkList[id]["Special Requirements"] = "";
            checkList[id]["Special Requirements"] = true;
          }
        }
        if (project.hasOwnProperty("notifyUsers")) {
          if (angular.isUndefined(checkList[id]["Notify Users"])) {
            checkList[id]["Notify Users"] = "";
            checkList[id]["Notify Users"] = true;
          }
        }
        if (project.hasOwnProperty("weekDuration")) {
          if (angular.isUndefined(checkList[id]["Week Duration"])) {
            checkList[id]["Week Duration"] = "";
            checkList[id]["Week Duration"] = true;
          }
        }
        if (project.hasOwnProperty("chainOfCustody")) {
          if (angular.isUndefined(checkList[id]["Chain Of Custody"])) {
            checkList[id]["Chain Of Custody"] = "";
            checkList[id]["Chain Of Custody"] = true;
          }
        }
        if (project.hasOwnProperty("siteAccess")) {
          if (angular.isUndefined(checkList[id]["Site Access"])) {
            checkList[id]["Site Access"] = "";
            checkList[id]["Site Access"] = true;
          }
        }
        if (project.hasOwnProperty("datumType")) {
          if (angular.isUndefined(checkList[id]["Datum Type"])) {
            checkList[id]["Datum Type"] = "";
            checkList[id]["Datum Type"] = true;
          }
        }
        if (project.hasOwnProperty("siteStart")) {
          if (angular.isUndefined(checkList[id]["Site Start"])) {
            checkList[id]["Site Start"] = "";
            checkList[id]["Site Start"] = true;
          }
        }
      }

      function findAreasWithMatchingDeliveryDate(project) {

        return areasService.getProjectAreas(project.$id)
                .$loaded()
                .then(function (projectAreas) {
                  angular.forEach(projectAreas, function (area) {
                    //do area math here
                    if (area.hasOwnProperty("actualPanels") && area.hasOwnProperty("estimatedPanels")) {
                      if ((area.estimatedPanels / 2) - (area.actualPanels) >= 0) {
                        checkList[project.id]["Revised Panel Data(50%)"] = true;
                      }
                      if (((area.estimatedPanels / 4) * 3) - (area.actualPanels) >= 0) {
                        checkList[project.id]["Revised Panel Data(75%)"] = true;
                      }
                      if (area.estimatedPanels - area.actualPanels >= 0) {
                        checkList[project.id]["Revised Panel Data(100%)"] = true;
                      }
                    }
                    if (angular.isUndefined(checkList[project.id]["areas"])) {
                      checkList[project.id]["areas"] = {};
                    }
                    if (angular.isUndefined(checkList[project.id]["areas"][area.$id])) {
                      checkList[project.id]["areas"][area.$id] = area;
                      angular.forEach(checkList[project.id]["areas"][area.$id], function (item, key) {
                        if (item === null) {
                          checkList[project.id]["areas"][area.$id][key] = false;
                        }
                        if (angular.isObject(item)) {
                          checkList[project.id]["areas"][area.$id][key] = true;
                        }
                        if (angular.isDefined(item) && String(key).slice(0, 1) !== "$") {
                          checkList[project.id]["areas"][area.$id][key] = true;
                        } else {
                          checkList[project.id]["areas"][area.$id][item] = false;
                        }
                      });
                    }
                    if (checkList[project.id]["areas"][area.$id].hasOwnProperty("offloadMethod")) {
                      if (angular.isUndefined(checkList[project.id]["Offload Method"])) {
                        checkList[project.id]["Offload Method"] = "";
                        checkList[project.id]["Offload Method"] = true;
                      }
                    }

                    if (checkList[project.id].hasOwnProperty("estimatedAreas")) {
                      if (angular.isUndefined(checkList[project.id]["Estimated Areas"])) {
                        checkList[project.id]["Estimated Areas"] = "";
                        checkList[project.id]["Estimated Areas"] = true;
                      }
                    }

                  });
                })
                .then(manipulateAreaData)
                .then(compareCheckLists);

      }

      function manipulateAreaData() {

        projectCopy;
        angular.forEach(checkList, function (project, projectId) {
          var above10PercentCount = 0;
          var above30PercentCount = 0;
          var above50PercentCount = 0;
          var above80PercentCount = 0;
          var completedAreaPanelUploads = 0;
          angular.forEach(project.areas, function (area, areaId) {
            //calculates if area is above certain percentages
            if (angular.isDefined(area.actualPanels)) {
              angular.forEach(project.areas, function (area, key) {
                if (area.hasOwnProperty("actualPanels")) {
                  above10PercentCount++;
                  above30PercentCount++;
                  above50PercentCount++;
                  above80PercentCount++;
                  completedAreaPanelUploads++;
                }
              });
              if (above10PercentCount > (Object.keys(project.areas).length / 10)) {
                checkList[project.key]["10% Percent Of Areas Uploaded"] = true;
              }
              if (above30PercentCount > (Object.keys(project.areas).length / 3.3)) {
                checkList[project.key]["30% Percent Of Areas Uploaded"] = true;
              }
              if (above50PercentCount > (Object.keys(project.areas).length / 2)) {
                checkList[project.key]["50% Percent Of Areas Uploaded"] = true;
              }
              if (above80PercentCount > ((Object.keys(project.areas).length / 5) * 4)) {
                checkList[project.key]["80% Percent Of Areas Uploaded"] = true;
              }
              if (completedAreaPanelUploads === (Object.keys(project.areas).length)) {
                checkList[project.key]["All Panels Uploaded"] = true;
              }
            }
          });
        });

      }

      function getGateways() {
        return referenceDataService.getGateways().$loaded();//get gateways
      }

      function turnToObject() {
        arrFromObject = Object.keys(gatewayChecker).map(function (key) {//turns gatewayw checker to an array
          if (angular.isUndefined(arrFromObject[key])) {
            arrFromObject[key] = gatewayChecker[key];
          }
        });
      }

    }



    function getDataForProject(projectId) {

      var deferred = $q.defer();

      var project = {};


      projectsService.getProject(projectId)
              .then(getGateways)
              .then(processProjects)
              .then(turnToObject)
              .then(function () {
                deferred.resolve(gatewayChecker);
              });

      return deferred.promise;


      function processProjects(gateways) {

        gatewayList = gateways;
        var promises = [];
        var n = 0;
        var counter = 0;
        if (project.active) {
          if (angular.isUndefined(gatewayChecker[project.id])) {
            gatewayChecker[project.id] = {};
            angular.forEach(gateways, function (gateway, key) {
              if (angular.isUndefined(gatewayChecker[project.id][gateway.$id])) {
                if (gateway.$id !== "G10") {
                  gatewayChecker[project.id][gateway.$id] = {};
                  counter++;
                }
                if (counter >= 9) {
                  gatewayChecker[project.id]["G10"] = {};
                  counter = 0;
                }
              }
            });
          }
          if (angular.isUndefined(returnData[project.id])) {
            returnData[project.id] = {};
            if (n === 0) {
              angular.copy(project, projectCopy);
              returnData[project.id] = projectCopy;
              n++;
            }
          }
          if (angular.isUndefined(projectCopy["materialRequest"])) {
            promises.push(materialRequestCalc(project));
          }

        }

        return $q.all(promises);

      }

      function materialRequestCalc(project) {

        return materialrequestsService.getMaterialRequestByProjectId(project.id)
                .$loaded()
                .then(function (materialRequest) {
                  if (angular.isDefined(materialRequest[0])) {
                    projectCopy["Material Request"] = true;

                  } else {
                    projectCopy["Material Request"] = false;
                  }
                })
                .then(function () {
                  angular.forEach(projectCopy, function (item, key) {
                    calculateBoolean(item, key, project.id, project);
                  });
                  return findAreasWithMatchingDeliveryDate(project);
                });
      }

      function LoopDataKeys(projectId, areaKey, gatewayDataKeys, gatewayId) {
        if (angular.isDefined(checkList[projectId]["areas"][areaKey])) {

          if (angular.isDefined(checkList[projectId][gatewayDataKeys])) {
            if (checkList[projectId][gatewayDataKeys] === false) {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = false;
            } else {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = true;
            }
          }

          if (angular.isDefined(checkList[projectId]["areas"][areaKey][gatewayDataKeys])) {
            if (checkList[projectId]["areas"][areaKey][gatewayDataKeys] === false) {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = false;
            } else {
              gatewayChecker[projectId][gatewayId][gatewayDataKeys] = true;
            }
          }

        }

      }

      function loopSpecialDataSets(projectId, gatewayId) {

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Special Requirements")) {
          var specReq = "Special Requirements";
          if (angular.isDefined(checkList[projectId][specReq])) {
            gatewayChecker[projectId][gatewayId][specReq] = checkList[projectId]["Special Requirements"];
          }
        }

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Week Duration")) {
          var weekReq = "Week Duration";
          if (angular.isDefined(checkList[projectId][weekReq])) {
            gatewayChecker[projectId][gatewayId][weekReq] = checkList[projectId]["Week Duration"];
          }
        }

        if (gatewayChecker[projectId][gatewayId].hasOwnProperty("Material Request")) {
          var specReq = "Material Request";
          if (angular.isDefined(checkList[projectId][specReq])) {
            gatewayChecker[projectId][gatewayId][specReq] = checkList[projectId]["Material Request"];
          }
        }

      }

      function compareCheckLists() {

        angular.forEach(gatewayChecker, function (gateways, projectId) {
          if (angular.isDefined(projectId)) {
            angular.forEach(gateways, function (gateway, gatewayId) {
              var configCopy = angular.copy(config);
              gatewayChecker[projectId][gatewayId] = configCopy[gatewayId];
              angular.forEach(gatewayChecker[projectId][gatewayId], function (values, gatewayDataKeys) {
                angular.forEach(checkList[projectId]["areas"], function (area, areaKey) {
                  angular.forEach(area, function (item, dataKey) {
                    if (checkList[projectId]["areas"].hasOwnProperty(dataKey)){
                      LoopDataKeys(projectId, areaKey, gatewayDataKeys, gatewayId);
                    }
                  });
                });
                loopSpecialDataSets(projectId, gatewayId);
              });
            });
          }
        });
        return gatewayChecker;

      }

      function calculateBoolean(item, key, id, project) {
        var isValid = true;
        if (angular.isUndefined(item)) {
          isValid = false;
        }
        if (item === false) {
          isValid = false;
        }
        if (angular.isObject(item)) {
          checkList[id][key] = [item];
        }
        if (angular.isUndefined(checkList[id])) {
          checkList[id] = {key: id};
        }
        if (angular.isUndefined(checkList[id][key])) {
          checkList[id][key] = isValid;
        }
        if (project.hasOwnProperty("specialRequirements")) {
          if (angular.isUndefined(checkList[id]["Special Requirements"])) {
            checkList[id]["Special Requirements"] = "";
            checkList[id]["Special Requirements"] = true;
          }
        }
        if (project.hasOwnProperty("notifyUsers")) {
          if (angular.isUndefined(checkList[id]["Notify Users"])) {
            checkList[id]["Notify Users"] = "";
            checkList[id]["Notify Users"] = true;
          }
        }
        if (project.hasOwnProperty("weekDuration")) {
          if (angular.isUndefined(checkList[id]["Week Duration"])) {
            checkList[id]["Week Duration"] = "";
            checkList[id]["Week Duration"] = true;
          }
        }
        if (project.hasOwnProperty("chainOfCustody")) {
          if (angular.isUndefined(checkList[id]["Chain Of Custody"])) {
            checkList[id]["Chain Of Custody"] = "";
            checkList[id]["Chain Of Custody"] = true;
          }
        }
        if (project.hasOwnProperty("siteAccess")) {
          if (angular.isUndefined(checkList[id]["Site Access"])) {
            checkList[id]["Site Access"] = "";
            checkList[id]["Site Access"] = true;
          }
        }
        if (project.hasOwnProperty("datumType")) {
          if (angular.isUndefined(checkList[id]["Datum Type"])) {
            checkList[id]["Datum Type"] = "";
            checkList[id]["Datum Type"] = true;
          }
        }
        if (project.hasOwnProperty("siteStart")) {
          if (angular.isUndefined(checkList[id]["Site Start"])) {
            checkList[id]["Site Start"] = "";
            checkList[id]["Site Start"] = true;
          }
        }
      }

      function findAreasWithMatchingDeliveryDate(project) {

        return areasService.getProjectAreas(project.$id)
                .$loaded()
                .then(function (projectAreas) {
                  angular.forEach(projectAreas, function (area) {
                    //do area math here
                    if (area.hasOwnProperty("actualPanels") && area.hasOwnProperty("estimatedPanels")) {
                      if ((area.estimatedPanels / 2) - (area.actualPanels) >= 0) {
                        checkList[project.id]["Revised Panel Data(50%)"] = true;
                      }
                      if (((area.estimatedPanels / 4) * 3) - (area.actualPanels) >= 0) {
                        checkList[project.id]["Revised Panel Data(75%)"] = true;
                      }
                      if (area.estimatedPanels - area.actualPanels >= 0) {
                        checkList[project.id]["Revised Panel Data(100%)"] = true;
                      }
                    }
                    if (angular.isUndefined(checkList[project.id]["areas"])) {
                      checkList[project.id]["areas"] = {};
                    }
                    if (angular.isUndefined(checkList[project.id]["areas"][area.$id])) {
                      checkList[project.id]["areas"][area.$id] = area;
                      angular.forEach(checkList[project.id]["areas"][area.$id], function (item, key) {
                        if (item === null) {
                          checkList[project.id]["areas"][area.$id][key] = false;
                        }
                        if (angular.isObject(item)) {
                          checkList[project.id]["areas"][area.$id][key] = true;
                        }
                        if (angular.isDefined(item) && String(key).slice(0, 1) !== "$") {
                          checkList[project.id]["areas"][area.$id][key] = true;
                        } else {
                          checkList[project.id]["areas"][area.$id][item] = false;
                        }
                      });
                    }
                    if (checkList[project.id]["areas"][area.$id].hasOwnProperty("offloadMethod")) {
                      if (angular.isUndefined(checkList[project.id]["Offload Method"])) {
                        checkList[project.id]["Offload Method"] = "";
                        checkList[project.id]["Offload Method"] = true;
                      }
                    }

                    if (checkList[project.id].hasOwnProperty("estimatedAreas")) {
                      if (angular.isUndefined(checkList[project.id]["Estimated Areas"])) {
                        checkList[project.id]["Estimated Areas"] = "";
                        checkList[project.id]["Estimated Areas"] = true;
                      }
                    }

                  });
                })
                .then(manipulateAreaData)
                .then(compareCheckLists);

      }

      function manipulateAreaData() {

        projectCopy;
        angular.forEach(checkList, function (project, projectId) {
          var above10PercentCount = 0;
          var above30PercentCount = 0;
          var above50PercentCount = 0;
          var above80PercentCount = 0;
          var completedAreaPanelUploads = 0;
          angular.forEach(project.areas, function (area, areaId) {
            //calculates if area is above certain percentages
            if (angular.isDefined(area.actualPanels)) {
              angular.forEach(project.areas, function (area, key) {
                if (area.hasOwnProperty("actualPanels")) {
                  above10PercentCount++;
                  above30PercentCount++;
                  above50PercentCount++;
                  above80PercentCount++;
                  completedAreaPanelUploads++;
                }
              });
              if (above10PercentCount > (Object.keys(project.areas).length / 10)) {
                checkList[project.key]["10% Percent Of Areas Uploaded"] = true;
              }
              if (above30PercentCount > (Object.keys(project.areas).length / 3.3)) {
                checkList[project.key]["30% Percent Of Areas Uploaded"] = true;
              }
              if (above50PercentCount > (Object.keys(project.areas).length / 2)) {
                checkList[project.key]["50% Percent Of Areas Uploaded"] = true;
              }
              if (above80PercentCount > ((Object.keys(project.areas).length / 5) * 4)) {
                checkList[project.key]["80% Percent Of Areas Uploaded"] = true;
              }
              if (completedAreaPanelUploads === (Object.keys(project.areas).length)) {
                checkList[project.key]["All Panels Uploaded"] = true;
              }
            }
          });
        });

      }

      function getGateways(pro) {
        project = pro;
        return referenceDataService.getGateways().$loaded();//get gateways
      }

      function turnToObject() {
        arrFromObject = Object.keys(gatewayChecker).map(function (key) {//turns gatewayw checker to an array
          if (angular.isUndefined(arrFromObject[key])) {
            arrFromObject[key] = gatewayChecker[key];
          }
        });
      }

    }
  }
})();