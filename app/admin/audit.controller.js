(function () {
  "use strict";

  angular
    .module("innView.admin")
    .controller("AuditController", AuditController);

  /**
   * @ngInject
   */
  function AuditController($log, $scope, $window, schema) {

    var vm = this;
    
    vm.amountToShow = 20;
    vm.auditLength = 0;
    
    $scope.$on("$destroy", destroyWatch);

    init();

    ////////////

    function init() {
      auditsByChunk();
    }

    function auditsByChunk() {
      schema.getArray(schema.getRoot().child("auditEntries").limitToLast(vm.amountToShow))
        .$loaded()
        .then(function (auditEntries) {
          auditEntries.reverse();
          angular.forEach(auditEntries,function (entry,key){
            if(angular.isDefined(entry.productionLine) && angular.isDefined(entry.project)){
              auditEntries[key].log = entry.log;
            }
          });
  
          vm.auditEntries = auditEntries;
        });
    }
    
    function destroyWatch(){
      if (vm.auditEntries && vm.auditEntries.$destroy) {
        vm.auditEntries.$destroy();
        delete vm.auditEntries;
      }
    }
    
    $window.addEventListener("scroll", function () {
      if ($window.scrollY === $window.document.body.scrollHeight - $window.innerHeight) {
        vm.amountToShow += 20;
        auditsByChunk();
      }
    });

  }
})();
