(function () {
  "use strict";

  angular
    .module("innView.admin")
    .factory("datumsService", datumsService);

  /**
   * @ngInject
   */
  function datumsService($log, $q, schema, referenceDataService, projectsService, panelsService, functionsService) {
    
    var datumTypes = {};
    var productTypes = {};
    var productionLines = {};
    
    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;

    var service = {
      averageMeterSquareOfDatum: averageMeterSquareOfDatum,
      updateDatum: updateDatum,
      damumByName: damumByName
    };
    
    init();

    return service;

    ////////////

    function init() {
      productionLines = referenceDataService.getProductionLines();
      productTypes = referenceDataService.getProductTypes();
      datumTypes = referenceDataService.getDatumTypes();
    }

    function averageMeterSquareOfDatum() {
      
      var projectRefsOfDatums = {};

      var deferred = $q.defer();

      var projects = projectsService.getProjects();
      projects.$loaded()
        .then(projectRefsOfDatumTypes)
        .then(panelsForProjects)
        .then(panelsAverage)
        .then(function () {
          deferred.resolve(projectRefsOfDatums);
        });

      return deferred.promise;

      function projectRefsOfDatumTypes(projects) {
        angular.forEach(datumTypes, function (datum) {
          
          var datumHolder = projectRefsOfDatums[datum.$id];
          if (angular.isUndefined(datumHolder)) {
            
            datumHolder = projectRefsOfDatums[datum.$id] = {
              projectRefs: [],
              name:"",
              productionLines:{}
            };
          }
          
          angular.forEach(productionLines, function (productionLine) {
            var productionLineKey = productionLine.$id;
            var productionLine = datumHolder.productionLines[productionLineKey];
            if(angular.isUndefined(productionLine)) {
              productionLine = datumHolder.productionLines[productionLineKey] = {
                panelCount: 0,
                panelAreaTotal: 0,
                panelAvg: 0,
              };
            }
          });
          
          angular.forEach(projects, function (project) {
            var projectDatumTypes = project.datumType;
            if (projectDatumTypes && projectDatumTypes === datum.$id) {
              datumHolder.projectRefs.push(project.$id);
            }
          });
        });
      }
      
      function panelsForProjects() {
        var promises = [];
        var productTypeProductionLine = buildMap(productTypes, "productionLine");
        angular.forEach(projectRefsOfDatums, function (datum, datumKey) {
          var datumProjectRefs = datum.projectRefs;
          var datumHolder = projectRefsOfDatums[datumKey];
          if (datumProjectRefs.length > 0) {
            angular.forEach(datumProjectRefs, function (projectRef) {
              promises.push(
                panelsService.panelsForProject(projectRef)
                .$loaded()
                .then(function (panels) {
                  angular.forEach(panels, function (panel) {
                    var type = panel.type;
                    var panelType = null;
                    //Identify If ExtH
                    var idParts = PANEL_ID_REGEX.exec(panel.id);
                    if (idParts) {
                      panelType = idParts[5];
                    }
                    type =  functionsService.logicHSIP(type, panelType, panel.dimensions.height, panel.dimensions.length)
                    var productionLineKey = productTypeProductionLine[type];
                    if (angular.isDefined(productionLineKey)) {
                      var productionLine = datumHolder.productionLines[productionLineKey];
                      productionLine.panelCount++;
                      productionLine.panelAreaTotal += panel.dimensions.area;
                    }
                  });
                })
                );
            });
          }
        });
        return $q.all(promises);
      }
      
      function panelsAverage() {
        var promises = [];
        angular.forEach(projectRefsOfDatums, function (datum, datumKey) {
          var datumProjectRefs = datum.projectRefs;
          delete datum.projectRefs;
          datum.name = datumKey;
          if (datumProjectRefs.length > 0) {
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var datumForProductionLines = datum.productionLines[productionLineKey];
              if (angular.isDefined(productionLineKey) && datumForProductionLines.panelCount > 0) {
                datumForProductionLines.panelAreaTotal = Math.round(datumForProductionLines.panelAreaTotal);
                datumForProductionLines.panelAvg = Math.round(datumForProductionLines.panelAreaTotal / datumForProductionLines.panelCount);
              }
            });
            promises.push(
              updateDatum(datumKey, datum)
              );
          }
        });
        return $q.all(promises);
        
      }

    }

    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }
    
    function updateDatum(datumName, datumData) {
      
      var existingDatums = damumByName(datumName);

      return existingDatums
        .$loaded()
        .then(updateDatum);

      function updateDatum() {
        var existing;
        angular.forEach(existingDatums, function (d) {
          if (d.name === datumData.name) {
            existing = d;
          }
        });

        if (existing) {
          angular.merge(existing, datumData);
          return existingDatums.$save(existing);
        } else {
          var newDatumData = angular.extend({}, datumData);
          return existingDatums.$add(newDatumData);
        }
      }
    }
    
    function damumByName(datumName) {
      return schema.getArray(schema.getRoot()
                             .child("datums")
                             .orderByChild("name")
                             .equalTo(datumName));
    }

  }
})();
