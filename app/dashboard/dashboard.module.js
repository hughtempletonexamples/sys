(function () {
  "use strict";

  angular
    .module("innView.dashboard", [
      "nvd3",
      "ngRoute",
      "firebase",
      "innView.core",
      "innView.projects"
    ]);

})();
