(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .factory("dashboardService", dashboardService);

  /**
   * @ngInject
   */
  function dashboardService($log, $q, $filter, schema, dateUtil, panelsService, referenceDataService, settingsService, firebaseUtil) {
    
    var initialised = false;
    var areaPlans = {};
    var additionalData = {};
    var productionStats = {};

    var defaultBenchOperators = {};
    var buildDaysArray = [];
    var defaultLineCapacity = {};
    var lineBenches = {};
    var defaultBench = {};
    var thirdPartySuppliers = {};
    var productTypeProductionLine = {};

    var designHandoverPeriod;
    var designHandoverBuffer;
    
    var projects = schema.getArray(schema.getRoot().child("projects"));
    var areas = schema.getArray(schema.getRoot().child("areas"));
    var components = schema.getArray(schema.getRoot().child("components"));
    
    var service = {
      humanResource: humanResources,
      areasForProduction: areasForProduction,
      areasForBooking: areasForBooking,
      areasForLogistics: areasForLogistics,
      areasForDesign: areasForDesign,
      
      updateBookingData: updateBookingData,
      updateAddData: updateAddData,
      updateBookingCheckData: updateBookingCheckData,
      updateAddCheckData: updateAddCheckData,
      updateBookingAddCheckDataDelivered: updateBookingAddCheckDataDelivered,
      addAreaToBooking: addAreaToBooking,
      setPanel: setPanel,
      removeAreaFromBooking: removeAreaFromBooking,
      removeBooking: removeBooking
    };

    return service;

    ////////////
    
    
    function initialise() {
      if (initialised) {
        return $q.resolve();
      }
      else {
        return init();
      }

      function init() {
        
        var productTypes = referenceDataService.getProductTypes();
        var productionLines = referenceDataService.getProductionSubLines();
        var suppliers = referenceDataService.getSuppliers();

        return settingsService.get()
          .then(function (settings) {
            designHandoverPeriod = settings.designHandoverPeriod;
            designHandoverBuffer = settings.designHandoverBuffer;
            return firebaseUtil.loaded(productTypes, productionLines, suppliers);
          })
          .then(function () {
            productTypeProductionLine = buildMap(productTypes, "productionLine");
            defaultLineCapacity = buildMap(productionLines, "defaultCapacity");
            defaultBench = buildMap(productionLines, "defaultBench");
            defaultBenchOperators = buildMap(productionLines, "defaultBenchOperators");
            thirdPartySuppliers = buildMap(suppliers, "thirdParty");
          })
          .then(function () {
            var deferredList = [];
            var benchmarks = schema.getRoot().child("benchmarks");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var benchmark = schema.getObject(benchmarks.child(productionLineKey));
              lineBenches[productionLineKey] = benchmark;

              deferredList.push(benchmark.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            var deferredList = [];
            var stats = schema.getRoot().child("stats");
            angular.forEach(productionLines, function (productionLine) {
              var productionLineKey = productionLine.$id;
              var stat = schema.getObject(stats.child(productionLineKey));
              productionStats[productionLineKey] = stat;

              deferredList.push(stat.$loaded());
            });

            return $q.all(deferredList);
          })
          .then(function () {
            var plansRef = schema.getRoot().child("productionPlans/areas");
            return plansRef.once("value", function (snapshot) {
              snapshot.forEach(function (snapPlan) {
                var planKey = snapPlan.key;
                var plan = schema.getObject(plansRef.child(planKey));
                areaPlans[planKey] = plan;
              });
            });
          })
          .then(function () {
            initialised = true;
          });
      }
    }
    
    function buildMap( items, property ) {
      var map = {};
      angular.forEach(items, function ( item ) {
        map[item.$id] = item[property];
      });
      return map;
    }
    
    function updateBookingCheckData(areaRef, type, check){

      var initials = schema.status.userName.match(/\b\w/g) || [];
      initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();

      var obj = {};
      obj.status = check;

      var obj = {
        "status": check,
        "timeDate": firebaseUtil.serverTime(),
        "user": initials
      };

      return schema.getObject(schema.getRoot().child("bookings").child(areaRef))
              .$loaded()
              .then(function ( object ) {
                object[type] = obj;

                return object.$save();
              });
    }
    
    function updateBookingData( bookingRef, label, newData ) {
      
      if(dateUtil.validDate(newData)){
        newData = newData.getTime();
      }

      return schema.getObject(schema.getRoot().child("bookings").child(bookingRef))
              .$loaded()
              .then(function ( booking ) {
                if(angular.isUndefined(booking[label])){
                  booking[label] = null;
                }
                booking[label] = newData;
      
                return booking.$save();
              });
              
    }
    
    function updateAddData( areaRef, label, newData ) {
      
      if(dateUtil.validDate(newData)){
        newData = newData.getTime();
      }

      return schema.getObject(schema.getRoot().child("additionalData/areas").child(areaRef))
              .$loaded()
              .then(function ( additionalData ) {
                if(angular.isUndefined(additionalData[label])){
                  additionalData[label] = null;
                }
                additionalData[label] = newData;
      
                return additionalData.$save();
              });
              
    }

    function updateAddCheckData(areaRef, type, check){

      var initials = schema.status.userName.match(/\b\w/g) || [];
      initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();

      var obj = {};
      obj.status = check;

      var obj = {
        "status": check,
        "timeDate": firebaseUtil.serverTime(),
        "user": initials
      };

      return schema.getObject(schema.getRoot().child("additionalData/areas").child(areaRef))
              .$loaded()
              .then(function ( object ) {
                object[type] = obj;

                return object.$save();
              });
    }
    
    function updateAreaInAddData( areaRef, label, newData ) {

      return schema.getObject(schema.getRoot().child("additionalData/areas").child(areaRef))
              .$loaded()
              .then(function ( areaAdditionalData ) {
                if(angular.isUndefined(areaAdditionalData[label])){
                  areaAdditionalData[label] = {};
                }
                areaAdditionalData[label][newData] = true;
      
                return areaAdditionalData.$save();
              });
              
    }

    function addBookings( bookingRef, label, areaRef ) {

      return schema.getObject(schema.getRoot().child("bookings").child(bookingRef))
              .$loaded()
              .then(function ( booking ) {
                if (angular.isUndefined(booking[label])) {
                  booking[label] = {};
                }
                booking[label][areaRef] = true;
                return booking.$save();
              })
              .then(function () {
                return updateAreaInAddData(areaRef, "bookings", bookingRef);
              });

    }
    
    function updateBookingAddCheckDataDelivered( bookingRef, check ) {

      var initials = schema.status.userName.match(/\b\w/g) || [];
      initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();

      var obj = {
        "status": check,
        "timeDate": firebaseUtil.serverTime(),
        "user": initials
      };

      var bookingHolder;
      var bookingAreaMap = {};
      var panelsForBookings = {};
      var areasHolder = {};

      return schema.getObject(schema.getRoot().child("bookings").child(bookingRef))
              .$loaded()
              .then(function ( booking ) {
                booking.delivered = obj;
                bookingHolder = booking;
                return booking.$save();
              })
              .then(function () {
                var promises = [];
                angular.forEach(bookingHolder.areas, function ( areaState, areaRef ) {
                  var additionalDataArea = schema.getObject(schema.getRoot().child("additionalData/areas").child(areaRef));
                  areasHolder[areaRef] = additionalDataArea;
                  promises.push(additionalDataArea.$loaded());

                  var panels = panelsService.panelsForArea(areaRef);
                  panelsForBookings[areaRef] = panels;
                  promises.push(panels.$loaded());
                });
                return $q.all(promises);
              })
              .then(function () {
                var promises = [];
                angular.forEach(areasHolder, function ( areaAdditionalData, areaRef ) {
                  angular.forEach(areaAdditionalData.bookings, function ( status, bookingRef ) {
                    var booking = schema.getObject(schema.getRoot().child("bookings").child(bookingRef));
                    if (angular.isUndefined(bookingAreaMap[areaRef])) {
                      bookingAreaMap[areaRef] = [];
                    }
                    bookingAreaMap[areaRef].push(booking);
                    promises.push(booking.$loaded());
                  });
                });
                return $q.all(promises);
              })
              .then(function () {
                return referenceDataService.getProductTypes().$loaded();
              })
              .then(function (productTypes) {
                var deferredList = [];
        
                var productTypeProductionLine = buildMap(productTypes, "productionLine");

                //update area delivered
                angular.forEach(bookingAreaMap, function ( bookings, areaRef ) {
                  var allDelivered = 0;
                  var panelsBooked = 0;
                  var area = areas.$getRecord(areaRef);

                  var productionLine = productTypeProductionLine[area.type] || null;

                  if (productionLine !== null) {
                    angular.forEach(bookings, function ( booking ) {
                      if (angular.isDefined(booking.delivered) && booking.delivered.status) {
                        allDelivered++;
                      }
                    });
                    angular.forEach(panelsForBookings[areaRef], function ( panel ) {
                      if (angular.isDefined(panel.booking)) {
                        panelsBooked++;
                      }
                    });

                    if (angular.isDefined(bookingAreaMap[areaRef]) && angular.isDefined(panelsForBookings[areaRef])) {
                      if (allDelivered === bookingAreaMap[areaRef].length && panelsBooked === panelsForBookings[areaRef].length) {
                        if (!area.delivered || angular.isUndefined(area.delivered)) {
                          if(check){
                            area.delivered = true;
                          }else{
                            area.delivered = false;  
                          }
                          deferredList.push(areas.$save(area));
                        }
                      } else {
                        if (area.delivered || angular.isUndefined(area.delivered)) {
                          if(check){
                            area.delivered = true;
                          }else{
                            area.delivered = false;  
                          }
                          deferredList.push(areas.$save(area));
                        }
                      }
                    } else {
                      if (bookingAreaMap[areaRef].length && angular.isDefined(bookingAreaMap[areaRef][0].delivered) && bookingAreaMap[areaRef][0].delivered.status) {
                        if (!area.delivered || angular.isUndefined(area.delivered)) {
                          if(check){
                            area.delivered = true;
                          }else{
                            area.delivered = false;  
                          }
                          deferredList.push(areas.$save(area));
                        }
                      } else {
                        if (area.delivered || angular.isUndefined(area.delivered)) {
                          if(check){
                            area.delivered = true;
                          }else{
                            area.delivered = false;  
                          }
                          deferredList.push(areas.$save(area));
                        }
                      }
                    }

                  } else {
                    if (!area.delivered || angular.isUndefined(area.delivered)) {
                      if (check) {
                        area.delivered = true;
                      } else {
                        area.delivered = false;
                      }
                      deferredList.push(areas.$save(area));
                    } else {
                      if (check) {
                        area.delivered = true;
                      } else {
                        area.delivered = false;
                      }
                      deferredList.push(areas.$save(area));
                    }
                  }

                });

                return $q.all(deferredList);
              });

    }
    
    function addAreaToBooking(bookingRef, selectedArea) {

      var updates = {};

      if(selectedArea !== "" && angular.isDefined(bookingRef)){
        if (selectedArea._productionLine === null) {
          if(selectedArea._bookings.length === 0){
            return addBookings(bookingRef, "areas", selectedArea.$id);
          }else{
            return $q.when();
          }
        }else{
          return panelsService.panelsForArea(selectedArea.$id)
            .$loaded()
            .then(function (panels) {
              var changes = 0;
              updates = {};
              angular.forEach(panels, function ( panel ) {
                if(angular.isUndefined(panel.booking)){
                  changes++;
                }
              });
              if(changes > 0){
                return addBookings(bookingRef, "areas", selectedArea.$id)
                  .then(function (){
                    var deffered = [];
                    angular.forEach(panels, function ( panel ) {
                      if(angular.isUndefined(panel.booking)){
                        deffered.push(setPanel( panel.$id , "booking", bookingRef));
                      }
                    });
                    return $q.all(deffered);
                  });              
              }else{
                return $q.when();
              }
            });
        }
      }
    }
    
    function setPanel( panelId , label, newData) {
      return schema.getObject(schema.getRoot().child("panels").child(panelId))
              .$loaded()
              .then(function ( panel ) {
                if (angular.isUndefined(panel[label])) {
                  panel[label] = {};
                }
                panel[label] = newData;

                return panel.$save();
              });
    }
    
    function removeAreaFromBooking(area, bookingRef){
      if (angular.isDefined(area) && angular.isDefined(bookingRef)) {
        return panelsService.panelsForArea(area.$id)
          .$loaded()
          .then(function (panels) {
            var promises = [];
            angular.forEach(panels, function ( panel ) {
              if(angular.isDefined(panel.booking) && panel.booking === bookingRef){
                promises.push(setPanel( panel.$id , "booking", null));
              }
            });
            return $q.all(promises);
          })
          .then(function () {
            return schema.getObject(schema.getRoot().child("bookings").child(bookingRef))
                    .$loaded()
                    .then(function ( booking ) {
                      delete booking.areas[area.$id];
                      return booking.$save();
                    });
          })
          .then(function () {
            return removeDeliveredFromArea(area.$id);
          })
          .then(function () {
            return schema.getObject(schema.getRoot().child("additionalData/areas").child(area.$id))
                    .$loaded()
                    .then(function ( areaAdditionalData ) {
                      if (angular.isDefined(areaAdditionalData.bookings) && angular.isDefined(areaAdditionalData.bookings[bookingRef])) {
                        delete areaAdditionalData.bookings[bookingRef];
                      }
                      return areaAdditionalData.$save();
                    });
          });
      }
      
    }
    
    function removeDeliveredFromArea(areaRef){
      return schema.getObject(schema.getRoot().child("areas").child(areaRef))
              .$loaded()
              .then(function ( area ) {
                if(angular.isDefined(area.delivered)){
                  area.delivered = null;
                }
                return area.$save();
              });
    }
    
    function removeBooking(bookingRef, areas){
      if (angular.isDefined(bookingRef)) {
        return schema.getArray(schema.getRoot().child("panels").orderByChild("booking").equalTo(bookingRef))
          .$loaded()
          .then(function (panels) {
            var promises = [];
            angular.forEach(panels, function ( panel ) {
              if(angular.isDefined(panel.booking) && panel.booking === bookingRef){
                promises.push(setPanel( panel.$id , "booking", null));
              }
            });
            return $q.all(promises);
          })
          .then(function () {
            var promises = [];
            angular.forEach(areas, function ( areaState, areaRef ) {
              promises.push(removeDeliveredFromArea(areaRef));
            });
            return $q.all(promises);
          })
          .then(function () {
            var promises = [];
            angular.forEach(areas, function ( areaState, areaRef ) {
              promises.push(
                schema.getObject(schema.getRoot().child("additionalData/areas").child(areaRef))
                .$loaded()
                .then(function ( areaAdditionalData ) {
                  if (angular.isDefined(areaAdditionalData.bookings) && angular.isDefined(areaAdditionalData.bookings[bookingRef])) {
                    delete areaAdditionalData.bookings[bookingRef];
                  }
                  return areaAdditionalData.$save();
                })
              );
            });
            return $q.all(promises);
          })
          .then(function () {
            return schema.getObject(schema.getRoot().child("bookings").child(bookingRef)).$remove();
          });
      }
     
    }
    
    function humanResources(){

      var deferred = $q.defer();
      
      initialise()
        .then(function (){
          return firebaseUtil.loaded(projects, areas);
        })
        .then(function () {
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function (){
          return plannerData();
        })
        .then(function (data){
          deferred.resolve(data);
        });
        
      return deferred.promise;
        
        
      function plannerData(){

        var resourceDays = {};
        var today = new Date();
        var nextMonday = dateUtil.plusDays(today, 7);
        var planStart = dateUtil.weekStart(today);
        var planDays = 7;
        for (var i = 0; i < planDays; i++) {
          var buildDate = dateUtil.plusDays(planStart, i);
          if(angular.isUndefined(resourceDays[buildDate])){
            resourceDays[buildDate] = {};
          }
          angular.forEach(defaultBench, function ( areaPlan, productionLine ) {
            if(angular.isUndefined(resourceDays[buildDate][productionLine])){
              resourceDays[buildDate][productionLine] = {
                operators: 0,
                meterSquared: 0,
                fBenches: 0,
                fPeople: 0,
                panels: 0,
                fm2: 0,
                aPeople: 0
              };   
            }
          });
        }
        for (var i = 0; i < planDays; i++) {
          var buildDate = dateUtil.plusDays(planStart, i);
          angular.forEach(areas, function ( area, key ) {
            if (buildDate < nextMonday){
              if (angular.isDefined(additionalData[area.$id]) && angular.isDefined(additionalData[area.$id].buildPlannedDays) && angular.isDefined(additionalData[area.$id].buildPlannedDays[buildDate])) {
                var buildOnDay = additionalData[area.$id].buildPlannedDays[buildDate];
                var productionLine = productTypeProductionLine[area.type] || null;
                if (area.line > 1) {
                  if (productionLine === "SIP") {
                    productionLine = productionLine + area.line;
                  }
                }
                var buildDateData = resourceDays[buildDate][productionLine];
                var operators = defaultBenchOperators[productionLine];
                var meterSquared = defaultBench[productionLine];
                var aBench = 0;
                var aPeople = 0;

                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].operators)){
                  operators = lineBenches[productionLine][buildDate].operators || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].meterSquared)){
                  meterSquared = lineBenches[productionLine][buildDate].meterSquared || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].aBench)){
                  aBench = lineBenches[productionLine][buildDate].aBench || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].aPeople)){
                  aPeople = lineBenches[productionLine][buildDate].aPeople || 0;
                }

                var calculatedM2 = calculateM2(area, buildOnDay);

                buildDateData.aBench = aBench;
                buildDateData.aPeople = aPeople;
                buildDateData.operators = operators;
                buildDateData.meterSquared = meterSquared;
                buildDateData.fm2 += calculatedM2;
                buildDateData.panels += buildOnDay;
              }
            }else{
              if (angular.isDefined(areaPlans[area.$id]) && angular.isDefined(areaPlans[area.$id].buildDays) && angular.isDefined(areaPlans[area.$id].buildDays[buildDate])) {
                var buildOnDay = areaPlans[area.$id].buildDays[buildDate];
                var productionLine = productTypeProductionLine[area.type] || null;
                if (area.line > 1) {
                  if (productionLine === "SIP") {
                    productionLine = productionLine + area.line;
                  }
                }
                var buildDateData = resourceDays[buildDate][productionLine];
                var operators = defaultBenchOperators[productionLine];
                var meterSquared = defaultBench[productionLine];
                var aBench = 0;
                var aPeople = 0;

                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].operators)){
                  operators = lineBenches[productionLine][buildDate].operators || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].meterSquared)){
                  meterSquared = lineBenches[productionLine][buildDate].meterSquared || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].aBench)){
                  aBench = lineBenches[productionLine][buildDate].aBench || 0;
                }
                if (angular.isDefined(lineBenches[productionLine]) && angular.isDefined(lineBenches[productionLine][buildDate]) && angular.isDefined(lineBenches[productionLine][buildDate].aPeople)){
                  aPeople = lineBenches[productionLine][buildDate].aPeople || 0;
                }

                var calculatedM2 = calculateM2(area, buildOnDay);

                buildDateData.aBench = aBench;
                buildDateData.aPeople = aPeople;
                buildDateData.operators = operators;
                buildDateData.meterSquared = meterSquared;
                buildDateData.fm2 += calculatedM2;
                buildDateData.panels += buildOnDay;
              }
            }
          });
        }

        var m2PerBench = 0;
        var m2PerBenchArr = [];
        var benchCount = 0;
        var daym2plusTotal = 0;
        angular.forEach(resourceDays, function ( productionLineData, date ) {
          angular.forEach(productionLineData, function ( buildDateData, productionLine ) {
            benchCount = 0;
            m2PerBench = 0;
            daym2plusTotal = 0;
            m2PerBenchArr = [];
            buildDateData.fm2 = parseInt($filter("number")(buildDateData.fm2, 0));
            m2PerBench = buildDateData.meterSquared;
            //puts bench m2 on top to total days m2 to get bench above days m2
            daym2plusTotal = buildDateData.fm2 + buildDateData.meterSquared;
            while (m2PerBench < daym2plusTotal) {
              benchCount++;
              m2PerBench += buildDateData.meterSquared;
              m2PerBenchArr.push(m2PerBench);
            }
            buildDateData.fBenches = benchCount;
            buildDateData.fPeople = buildDateData.operators * benchCount;
          });
        });

        var returnData = {
          lineBenches: lineBenches,
          resourceDays: resourceDays
        };

        return returnData;
      }
      
    }
      
      
    function areasForBooking(selectDate) {
      
      var buildAreas = [];

      var deferred = $q.defer();
      var dayzGone = 21;
      var monday = dateUtil.weekStart(selectDate);
      var sunday = dateUtil.plusDays(monday, 6);

      var firstMonday = dateUtil.minusDays(monday, dayzGone);
      var lastMonday = dateUtil.plusDays(monday, dayzGone);
      
      var lastSun = dateUtil.plusDays(lastMonday, 6);
      
      var start = new Date(firstMonday).setHours(0,0,0,0);
      var end = new Date(lastSun).setHours(23,59,59,999);

      var bookingsArr = [];
      var groupByBookingDate = {};
      var panelsForBookings = {};
      var bookingAreaMap = {};
      
      var daysOfWeeks = getWeekDays(selectDate);
      var weeks = getWeeks(6, dayzGone);

      initialise()
        .then(function () {
          return firebaseUtil.loaded(projects, areas, components);
        })
        .then(function () {
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function () {
          return schema.getArray(schema.getRoot().child("bookings").orderByChild("bookedTime").startAt(start).endAt(end)).$loaded();
        })
        .then(function (bookings) {
          bookingsArr = bookings;    
          return getPanelsForBookings();
        })
        .then(function () {
          return processBookings();
        })
        .then(function () {
          return selectAreasForBooking();
        })
        .then(function () {
          return referenceDataService.getComponents().$loaded();
        })
        .then(function (srdComponents) {
          return areasForWeek( srdComponents );
        })
        .then(function ( data ) {
          deferred.resolve(data);
        });

      return deferred.promise;
     
      
      function getWeekDays(){
        var week = {};
        for(var i = 0; i < 6; i++){
          var day = dateUtil.plusDays(monday, i);
          if(angular.isUndefined(week[day])){
            week[day] = {
              deliverys:0,
              booked:0,
              unplanned:0
            };
          }
        }
        return week;
      }
      
      function getWeeks(weeksAmount, dayzGone) {
        var weekHolder = {};
        if(angular.isUndefined(weekHolder[firstMonday])){
          weekHolder[firstMonday] = {deliverys: 0, booked: 0};
        }
        for (var j = 1; j <= weeksAmount; j++) {
          firstMonday = dateUtil.plusDays(firstMonday, 7);
          if (angular.isUndefined(weekHolder[firstMonday])) {
            weekHolder[firstMonday] = {deliverys: 0, booked: 0};
          }
        }
        return weekHolder;
      }
      
      function getPanelsForBookings(){
        panelsForBookings = {};
        var deferredList = [];
        angular.forEach(bookingsArr, function ( booking, key ) {
          var isoMonday = dateUtil.weekStart(booking.bookedTime);
          var date = dateUtil.toIsoDate(booking.bookedTime);
          if (angular.isDefined(daysOfWeeks[date])) {
            daysOfWeeks[date].booked++;
          }
          if (angular.isDefined(weeks[isoMonday])) {
            weeks[isoMonday].booked++;
          }
          angular.forEach(booking.areas, function ( areaState, areaRef ) {
            if (angular.isUndefined(bookingAreaMap[areaRef])) {
              bookingAreaMap[areaRef] = [];
            }
            bookingAreaMap[areaRef].push(booking);
            if (date >= monday && date <= sunday){
              var panels = panelsService.panelsForArea(areaRef);
              panelsForBookings[areaRef] = panels;
              deferredList.push(panels.$loaded());
            }
          });
        });
        return $q.all(deferredList);
      }
      
      function processBookings(){
        groupByBookingDate = {};

        angular.forEach(bookingsArr, function ( booking, key ) {
          var date = dateUtil.toIsoDate(booking.bookedTime);
          if (date >= monday && date <= sunday){
            if(angular.isUndefined(groupByBookingDate[booking.bookedTime])){
              groupByBookingDate[booking.bookedTime] = {};
            }
            if(angular.isUndefined(groupByBookingDate[booking.bookedTime][booking.$id])){
              groupByBookingDate[booking.bookedTime][booking.$id] = {
                booking: booking,
                areas: {}
              };
            }
            angular.forEach(booking.areas, function ( areaState, areaRef ) {
              if(angular.isUndefined(groupByBookingDate[booking.bookedTime][booking.$id].areas[areaRef])){
                var panelCounter = 0;
                var area = areas.$getRecord(areaRef);
                if (area !== null) {
                  var project = projects.$getRecord(area.project);
                  angular.forEach(panelsForBookings[areaRef], function ( panel ) {
                    if (angular.isDefined(panel.booking) && panel.booking === booking.$id) {
                      panelCounter++;
                    }
                  });
                  area._id = getAreaId(area, project);
                  groupByBookingDate[booking.bookedTime][booking.$id].areas[areaRef] = {
                    "area": area,
                    "bookedPanels": panelCounter
                  };
                }
              }
            });
          }
        });   
   
      }

      function selectAreasForBooking() {
        buildAreas = [];
        var deferredList = [];

        angular.forEach(areas, function ( area, key ) {
          
          if (!thirdPartySuppliers[area.supplier]) {
            var project = projects.$getRecord(area.project);
            
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if (project.active) {
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                
                area._bookings = [];
                
                if (angular.isDefined(bookingAreaMap[area.$id])){
                  var bookings = bookingAreaMap[area.$id];
                  bookings.sort(function (a, b) {
                    return Date.parse(a.bookingTime) - Date.parse(b.bookingTime);
                  });
                  area._bookings = bookings;
                }
                
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._idWithoutType = getAreaIdWithoutType(area, project);
                area._id = getAreaId(area, project);
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);
                if(angular.isDefined(project.designer)){
                  area._designer = project.designer;
                }else{
                  area._designer = "";
                }
                if (area.line > 1) {
                  var productionLine = productTypeProductionLine[area.type] || null;
                  if (productionLine === "SIP") {
                    area._productionLine = productionLine + area.line;
                    area._line = area.line;
                  } else {
                    area._productionLine = productionLine;
                    area._line = 1;
                  }
                } else {
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }

                buildAreas.push(area);
                
              }
            }
          }

        });

        buildAreas = $filter("orderBy")(buildAreas, "_deliveryDate");

        return $q.all(deferredList);
      }
      
      function areasForWeek(srdComponents) {

        var chartData = {
          "booked": [],
          "deliverys": []
        };

        var componentsHolder = {};

        var componentsMap = createAreasMap(components, srdComponents);
        
        angular.forEach(buildAreas, function ( area, key ) {
          var date = dateUtil.weekStart(area._dispatchDate);
          if (angular.isDefined(weeks[date])){
            var project = projects.$getRecord(area.project);

            var componentKey = componentsMap[area.$id];
            if (angular.isUndefined(componentKey)) {
              var componentKey = {};
              componentKey.name = "NONE";
              componentKey.id = 0;
              componentKey.comments = 0;
            }

            var component = componentsHolder[componentKey.name];
            if (angular.isUndefined(component)) {
              component = componentsHolder[componentKey.name] = {
                componentId: componentKey.id,
                comments: componentKey.comments,
                areas: [],
                areasDeliveryDate: [],
                areasDispatchDate: [],
                areasLatestRevision: [],
                areasDelivered: [],
                delivered: true,
                areasActive: [],
                active: true,
                same: false,
                isSame: [],
                dispatchDate: new Date("2100-12-30"),
                latestRevision: new Date("2100-12-30")
              };
            }
            var areaDate = new Date(area._dispatchDate);
            var areaDateDelivery = new Date(area._deliveryDate);
            var revision = new Date(area.revisions[project.deliverySchedule.unpublished]);

            if (angular.isDefined(project.deliverySchedule.published)) {
              revision = new Date(area.revisions[project.deliverySchedule.published]);
            }

            var isSame = component.isSame[area.$id];
            var dispatchDate = component.areasDispatchDate[area.$id];
            var deliveryDate = component.areasDeliveryDate[area.$id];
            var latestRevision = component.areasLatestRevision[area.$id];
            var areaDelivered = component.areasDelivered[area.$id];
            var areaActive = component.areasActive[area.$id];

            if (angular.isUndefined(deliveryDate)) {
              deliveryDate = component.areasDeliveryDate[area.$id] = areaDateDelivery;
            }
            
            if (angular.isUndefined(dispatchDate)) {
              dispatchDate = component.areasDispatchDate[area.$id] = areaDate;
            }

            if (angular.isUndefined(latestRevision)) {
              latestRevision = component.areasLatestRevision[area.$id] = revision;
            }

            if (angular.isUndefined(isSame)) {
              if (dateUtil.toIsoDate(deliveryDate) === dateUtil.toIsoDate(latestRevision)) {
                isSame = component.isSame[area.$id] = true;
              } else {
                isSame = component.isSame[area.$id] = false;
              }
            }

            if (angular.isUndefined(areaDelivered)) {
              if (angular.isDefined(area.delivered) && area.delivered) {
                areaDelivered = component.areasDelivered[area.$id] = true;
              } else {
                areaDelivered = component.areasDelivered[area.$id] = false;
              }
            }

            if (angular.isUndefined(areaActive)) {
              if (area.active) {
                areaActive = component.areasActive[area.$id] = true;
              } else {
                areaActive = component.areasActive[area.$id] = false;
              }
            }

            if (!areaDelivered) {
              component.delivered = false;
            }

            if (!areaActive) {
              component.active = false;
            }

            component.areas.push(area);
            if (areaDate < component.dispatchDate) {
              component.dispatchDate = areaDate;
              var isoDispatchDate = dateUtil.toIsoDate(areaDate);
              var isoMondayDispatchDate = dateUtil.weekStart(areaDate);
              if (angular.isDefined(daysOfWeeks[isoDispatchDate])) {
                daysOfWeeks[isoDispatchDate].deliverys++;
              }
              if (angular.isDefined(weeks[isoMondayDispatchDate])) {
                weeks[isoMondayDispatchDate].deliverys++;
              }
            }
            if (areaDateDelivery < component.deliveryDate) {
              component.deliveryDate = areaDateDelivery;
            }
            if (latestRevision < component.latestRevision) {
              component.latestRevision = latestRevision;
            }
            if (dateUtil.toIsoDate(component.deliveryDate) === dateUtil.toIsoDate(component.latestRevision)) {
              component.same = true;
            }
          }

        });
        
        var componentsByDay = {};
        
        angular.forEach(componentsHolder, function ( component, componentName ) {
          var dispatchDate = dateUtil.toIsoDate(component.dispatchDate);
          if (dispatchDate >= monday && dispatchDate <= sunday){
            if(angular.isUndefined(componentsByDay[dispatchDate])){
              componentsByDay[dispatchDate] = {};
            }
            if(angular.isUndefined(componentsByDay[dispatchDate][componentName])){
              componentsByDay[dispatchDate][componentName] = {};
            }
            componentsByDay[dispatchDate][componentName] = component;
          }
        });

        angular.forEach(weeks, function ( data, week ) {
          chartData.booked.push({x: week, y: data.booked});
          chartData.deliverys.push({x: week, y: data.deliverys});
        });

        var returnData = {
          groupByBookingDate: groupByBookingDate,
          chartData: chartData,
          daysOfWeeks: daysOfWeeks,
          areasByDispatch: componentsByDay
        };

        return returnData;

      }

      function getAreaId( area, project ) {
        return [project.id, area.phase, area.floor, area.type].join("-");
      }
      
      function getAreaIdWithoutType( area, project ) {
        return [project.id, area.phase, area.floor].join("-");
      }
      
      function createAreasMap( components, srdComponents ) {
        var areasMap = {};
        var areasMapCounter = {};

        angular.forEach(components, function ( component ) {
          if (angular.isUndefined(areasMapCounter[component.type])) {
            areasMapCounter[component.type] = 0;
          }
          areasMapCounter[component.type]++;
          angular.forEach(component.areas, function ( value, area ) { 
            var componentName = component.type + areasMapCounter[component.type];
            areasMap[area] = {
              id: component.$id,
              name: componentName,
              comments: component.comments
            };
          });
        });

        return areasMap;
      }

    }
    
    function areasForLogistics(selectDate) {
      
      var buildAreas = [];

      var deferred = $q.defer();
      var dayzGone = 21;
      var monday = dateUtil.weekStart(selectDate);
      var sunday = dateUtil.plusDays(monday, 6);

      var firstMonday = dateUtil.minusDays(monday, dayzGone);
      var lastMonday = dateUtil.plusDays(monday, dayzGone);
      
      var lastSun = dateUtil.plusDays(lastMonday, 6);
      
      var start = new Date(firstMonday).setHours(0,0,0,0);
      var end = new Date(lastSun).setHours(23,59,59,999);

      var bookingsArr = [];
      var groupByBookingDate = {};
      var panelsForBookings = {};
      var bookingAreaMap = {};
      
      var daysOfWeeks = getWeekDays(selectDate);
      var weeks = getWeeks(6, dayzGone);

      initialise()
        .then(function () {
          return firebaseUtil.loaded(projects, areas, components);
        })
        .then(function () {
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function () {
          return schema.getArray(schema.getRoot().child("bookings").orderByChild("bookedTime").startAt(start).endAt(end)).$loaded();
        })
        .then(function (bookings) {
          bookingsArr = bookings;    
          return getPanelsForBookings();
        })
        .then(function () {
          return processBookings();
        })
        .then(function () {
          return selectAreasForBooking();
        })
        .then(function () {
          return referenceDataService.getComponents().$loaded();
        })
        .then(function (srdComponents) {
          return areasForWeek( srdComponents );
        })
        .then(function ( data ) {
          deferred.resolve(data);
        });

      return deferred.promise;
     
      
      function getWeekDays(){
        var week = {};
        for(var i = 0; i < 6; i++){
          var day = dateUtil.plusDays(monday, i);
          if(angular.isUndefined(week[day])){
            week[day] = {
              deliverys:0,
              booked:0,
              unplanned:0
            };
          }
        }
        return week;
      }
      
      function getWeeks(weeksAmount, dayzGone) {
        var weekHolder = {};
        if(angular.isUndefined(weekHolder[firstMonday])){
          weekHolder[firstMonday] = {deliverys: 0, booked: 0};
        }
        for (var j = 1; j <= weeksAmount; j++) {
          firstMonday = dateUtil.plusDays(firstMonday, 7);
          if (angular.isUndefined(weekHolder[firstMonday])) {
            weekHolder[firstMonday] = {deliverys: 0, booked: 0};
          }
        }
        return weekHolder;
      }
      
      function getPanelsForBookings(){
        panelsForBookings = {};
        var deferredList = [];
        angular.forEach(bookingsArr, function ( booking, key ) {
          var isoMonday = dateUtil.weekStart(booking.bookedTime);
          var date = dateUtil.toIsoDate(booking.bookedTime);
          if (angular.isDefined(daysOfWeeks[date])) {
            daysOfWeeks[date].booked++;
          }
          if (angular.isDefined(weeks[isoMonday])) {
            weeks[isoMonday].booked++;
          }
          angular.forEach(booking.areas, function ( areaState, areaRef ) {
            if (angular.isUndefined(bookingAreaMap[areaRef])) {
              bookingAreaMap[areaRef] = [];
            }
            bookingAreaMap[areaRef].push(booking);
            if (date >= monday && date <= sunday){
              var panels = panelsService.panelsForArea(areaRef);
              panelsForBookings[areaRef] = panels;
              deferredList.push(panels.$loaded());
            }
          });
        });
        return $q.all(deferredList);
      }
      
      function processBookings(){
        groupByBookingDate = {};

        angular.forEach(bookingsArr, function ( booking, key ) {
          var date = dateUtil.toIsoDate(booking.bookedTime);
          if (date >= monday && date <= sunday){
            if(angular.isUndefined(groupByBookingDate[booking.bookedTime])){
              groupByBookingDate[booking.bookedTime] = {};
            }
            if(angular.isUndefined(groupByBookingDate[booking.bookedTime][booking.$id])){
              groupByBookingDate[booking.bookedTime][booking.$id] = {
                booking: booking,
                areas: {}
              };
            }
            angular.forEach(booking.areas, function ( areaState, areaRef ) {
              if (angular.isUndefined(groupByBookingDate[booking.bookedTime][booking.$id].areas[areaRef])) {
                var panelCounter = 0;
                var area = areas.$getRecord(areaRef);
                if (area !== null) {
                  var project = projects.$getRecord(area.project);
                  angular.forEach(panelsForBookings[areaRef], function ( panel ) {
                    if (angular.isDefined(panel.booking) && panel.booking === booking.$id) {
                      panelCounter++;
                    }
                  });
                  area._id = getAreaId(area, project);
                  groupByBookingDate[booking.bookedTime][booking.$id].areas[areaRef] = {
                    "area": area,
                    "bookedPanels": panelCounter
                  };
                }
              }
            });
          }
        });   
   
      }

      function selectAreasForBooking() {
        buildAreas = [];
        var deferredList = [];

        angular.forEach(areas, function ( area, key ) {
          
          if (!thirdPartySuppliers[area.supplier]) {
            var project = projects.$getRecord(area.project);
            
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if (project.active) {
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                
                area._bookings = [];
                
                if (angular.isDefined(bookingAreaMap[area.$id])){
                  var bookings = bookingAreaMap[area.$id];
                  bookings.sort(function (a, b) {
                    return Date.parse(a.bookingTime) - Date.parse(b.bookingTime);
                  });
                  area._bookings = bookings;
                }
                
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._idWithoutType = getAreaIdWithoutType(area, project);
                area._id = getAreaId(area, project);
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);
                if(angular.isDefined(project.designer)){
                  area._designer = project.designer;
                }else{
                  area._designer = "";
                }
                if (area.line > 1) {
                  var productionLine = productTypeProductionLine[area.type] || null;
                  if (productionLine === "SIP") {
                    area._productionLine = productionLine + area.line;
                    area._line = area.line;
                  } else {
                    area._productionLine = productionLine;
                    area._line = 1;
                  }
                } else {
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }

                buildAreas.push(area);
                
              }
            }
          }

        });

        buildAreas = $filter("orderBy")(buildAreas, "_deliveryDate");

        return $q.all(deferredList);
      }
      
      function areasForWeek(srdComponents) {

        var chartData = {
          "booked": [],
          "deliverys": []
        };

        var componentsHolder = {};
        var riskDays = undefined;
        
        var componentsMap = createAreasMap(components, srdComponents);
        
        angular.forEach(buildAreas, function ( area, key ) {
          var date = dateUtil.weekStart(area._dispatchDate);
          if (angular.isDefined(weeks[date])){
            var project = projects.$getRecord(area.project);

            var componentKey = componentsMap[area.$id];
            if (angular.isUndefined(componentKey)) {
              var componentKey = {};
              componentKey.name = "NONE";
              componentKey.id = 0;
              componentKey.comments = 0;
            }
            
            var projectManagerName = "";
            var projectManagerContact = "";
            
            if (angular.isDefined(project.deliveryManager) && angular.isDefined(project.deliveryManager.name)) {
              projectManagerName = project.deliveryManager.name;
            }
            if (angular.isDefined(project.deliveryManager) && angular.isDefined(project.deliveryManager.mobile)) {
              projectManagerContact = project.deliveryManager.mobile;
            }

            var component = componentsHolder[componentKey.name];
            if (angular.isUndefined(component)) {
              component = componentsHolder[componentKey.name] = {
                componentId: componentKey.id,
                comments: componentKey.comments,
                projectManagerName: projectManagerName,
                projectManagerContact: projectManagerContact,
                siteAccess: project.siteAccess,
                siteAddress: project.siteAddress,
                postCode: project.postCode,
                estimatedAreaTotal: 0,
                estimatedCountTotal: 0,
                actualAreaTotal: 0,
                actualCountTotal: 0,
                completedAreaTotal: 0,
                riskDays: 0,
                completedCountTotal: 0,
                riskDaysArray: [],
                areas: [],
                areasDispatchDate: [],
                areasDeliveryDate: [],
                areasLatestRevision: [],
                areasDelivered: [],
                delivered: true,
                areasActive: [],
                active: true,
                same: false,
                isSame: [],
                plannedFinish: new Date("1970-12-30"),
                deliveryDate: new Date("2100-12-30"),
                dispatchDate: new Date("2100-12-30"),
                latestRevision: new Date("2100-12-30")
              };
            }
            
            component.estimatedAreaTotal += getOptionalNumber(area.estimatedArea);
            component.estimatedCountTotal += getOptionalNumber(area.estimatedPanels);
            component.actualAreaTotal += getOptionalNumber(area.actualArea);
            var areaActualCount = getOptionalNumber(area.actualPanels);
            component.actualCountTotal += areaActualCount;
            component.completedAreaTotal += getOptionalNumber(area.completedArea);
            component.completedCountTotal += getOptionalNumber(area.completedPanels);

            if (areaPlans[area.$id] && areaPlans[area.$id].plannedFinish && areaPlans[area.$id].riskDate) {
              var areaRiskDays = dateUtil.daysBetweenWeekDaysOnly(areaPlans[area.$id].plannedFinish, areaPlans[area.$id].riskDate);
              component.riskDaysArray.push(areaRiskDays);
              var plannedFinish = new Date(areaPlans[area.$id].plannedFinish);
              if (plannedFinish > component.plannedFinish) {
                component.plannedFinish = plannedFinish;
              }
            }
            
            if(component.riskDaysArray.length > 0){
              component.riskDays = lowestRisk(component.riskDaysArray);
            }

            var areaDate = new Date(area._dispatchDate);
            var areaDateDelivery = new Date(area._deliveryDate);
            var revision = new Date(area.revisions[project.deliverySchedule.unpublished]);

            if (angular.isDefined(project.deliverySchedule.published)) {
              revision = new Date(area.revisions[project.deliverySchedule.published]);
            }

            var isSame = component.isSame[area.$id];
            var dispatchDate = component.areasDispatchDate[area.$id];
            var deliveryDate = component.areasDeliveryDate[area.$id];
            var latestRevision = component.areasLatestRevision[area.$id];
            var areaDelivered = component.areasDelivered[area.$id];
            var areaActive = component.areasActive[area.$id];

            if (angular.isUndefined(deliveryDate)) {
              deliveryDate = component.areasDeliveryDate[area.$id] = areaDateDelivery;
            }
            
            if (angular.isUndefined(dispatchDate)) {
              dispatchDate = component.areasDispatchDate[area.$id] = areaDate;
            }

            if (angular.isUndefined(latestRevision)) {
              latestRevision = component.areasLatestRevision[area.$id] = revision;
            }

            if (angular.isUndefined(isSame)) {
              if (dateUtil.toIsoDate(deliveryDate) === dateUtil.toIsoDate(latestRevision)) {
                isSame = component.isSame[area.$id] = true;
              } else {
                isSame = component.isSame[area.$id] = false;
              }
            }

            if (angular.isUndefined(areaDelivered)) {
              if (angular.isDefined(area.delivered) && area.delivered) {
                areaDelivered = component.areasDelivered[area.$id] = true;
              } else {
                areaDelivered = component.areasDelivered[area.$id] = false;
              }
            }

            if (angular.isUndefined(areaActive)) {
              if (area.active) {
                areaActive = component.areasActive[area.$id] = true;
              } else {
                areaActive = component.areasActive[area.$id] = false;
              }
            }

            if (!areaDelivered) {
              component.delivered = false;
            }

            if (!areaActive) {
              component.active = false;
            }

            component.areas.push(area);
            if (areaDate < component.dispatchDate) {
              component.dispatchDate = areaDate;
              var isoDispatchDate = dateUtil.toIsoDate(areaDate);
              var isoMondayDispatchDate = dateUtil.weekStart(areaDate);
              if (angular.isDefined(daysOfWeeks[isoDispatchDate])) {
                daysOfWeeks[isoDispatchDate].deliverys++;
              }
              if (angular.isDefined(weeks[isoMondayDispatchDate])) {
                weeks[isoMondayDispatchDate].deliverys++;
              }
            }
            if (areaDateDelivery < component.deliveryDate) {
              component.deliveryDate = areaDateDelivery;
            }
            if (latestRevision < component.latestRevision) {
              component.latestRevision = latestRevision;
            }
            if (dateUtil.toIsoDate(component.deliveryDate) === dateUtil.toIsoDate(component.latestRevision)) {
              component.same = true;
            }
          }

        });
        
        var componentsByDay = {};
        
        angular.forEach(componentsHolder, function ( component, componentName ) {
          var dispatchDate = dateUtil.toIsoDate(component.dispatchDate);
          if (dispatchDate >= monday && dispatchDate <= sunday){
            if(angular.isUndefined(componentsByDay[dispatchDate])){
              componentsByDay[dispatchDate] = {};
            }
            if(angular.isUndefined(componentsByDay[dispatchDate][componentName])){
              componentsByDay[dispatchDate][componentName] = {};
            }
            componentsByDay[dispatchDate][componentName] = component;
          }
        });

        angular.forEach(weeks, function ( data, week ) {
          chartData.booked.push({x: week, y: data.booked});
          chartData.deliverys.push({x: week, y: data.deliverys});
        });

        var returnData = {
          groupByBookingDate: groupByBookingDate,
          chartData: chartData,
          daysOfWeeks: daysOfWeeks,
          areasByDispatch: componentsByDay
        };

        return returnData;

      }

      function getAreaId( area, project ) {
        return [project.id, area.phase, area.floor, area.type].join("-");
      }
      
      function getAreaIdWithoutType( area, project ) {
        return [project.id, area.phase, area.floor].join("-");
      }
      
      function createAreasMap( components, srdComponents ) {
        var areasMap = {};
        var areasMapCounter = {};

        angular.forEach(components, function ( component ) {
          if (angular.isUndefined(areasMapCounter[component.type])) {
            areasMapCounter[component.type] = 0;
          }
          areasMapCounter[component.type]++;
          angular.forEach(component.areas, function ( value, area ) { 
            var componentName = component.type + areasMapCounter[component.type];
            areasMap[area] = {
              id: component.$id,
              name: componentName,
              comments: component.comments
            };
          });
        });

        return areasMap;
      }

    }
    
    function areasForProduction(selectDate) {
      
      var buildAreas = [];

      var deferred = $q.defer();
      
      var weeks = {};
      var daysOfWeeks = getWeekDays();

      initialise()
        .then(function () {
          return firebaseUtil.loaded(projects, areas);
        })
        .then(function () {
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function () {
          return selectAreasForProduction();
        })
        .then(function (buildAreas) {
          weeks = getWeeks(6, 21);
          return areasForWeek(buildAreas);
        })
        .then(function (data) {
          deferred.resolve(data);
        });

      return deferred.promise;
      
      function getWeekDays(){
        var weekStart = dateUtil.weekStart(selectDate);
        var week = {};
        for(var i = 0; i < 6; i++){
          var day = dateUtil.plusDays(weekStart, i);
          if(angular.isUndefined(week[day])){
            week[day] = {};
          }
        }
        return week;
      }
      
      function getWeeks(weeksAmount, dayzGone) {
        var weekHolder = {};
        var week = {};
        var today = new Date();
        var thisMonday = dateUtil.weekStart(today);
        var monday = dateUtil.weekStart(selectDate);
        var nextMonday = dateUtil.plusDays(thisMonday, 7);
        var firstMonday = dateUtil.minusDays(monday, dayzGone);
        for (var j = 0; j <= weeksAmount; j++) {
          for (var i = 0; i < 6; i++) {
            var buildDate = dateUtil.plusDays(firstMonday, i);
            angular.forEach(defaultBench, function ( bench, productionLine ) {
              if (angular.isUndefined(week[productionLine])) {
                week[productionLine] = {};
              }
              if (angular.isUndefined(weekHolder[productionLine])) {
                weekHolder[productionLine] = {};
              }
              if (angular.isUndefined(week[productionLine][buildDate])) {
                week[productionLine][buildDate] = {
                  planM2: 0,
                  planPanels: 0,
                  manufacturedM2: 0,
                  manufacturedPanels: 0                   
                };
              }
              if (angular.isUndefined(weekHolder[productionLine][firstMonday])) {
                weekHolder[productionLine][firstMonday] = {
                  planM2: 0,
                  planPanels: 0,
                  manufacturedM2: 0,
                  manufacturedPanels: 0                  
                  
                };
              }
            });
            //for plan data
            angular.forEach(areas, function ( area, key ) {
              var productionLine = productTypeProductionLine[area.type] || null;
              if (buildDate < nextMonday){
                if (angular.isDefined(additionalData[area.$id]) && angular.isDefined(additionalData[area.$id].buildPlannedDays) && angular.isDefined(additionalData[area.$id].buildPlannedDays[buildDate])) {
                  var buildOnDay = additionalData[area.$id].buildPlannedDays[buildDate];
                  var m2 = 0;
                  var avg = 0;
                  if (angular.isDefined(area.actualPanels) && area.actualPanels > 0) {
                    if (angular.isDefined(area.actualArea) && area.actualArea > 0) {
                      avg = area.actualArea / area.actualPanels;
                      m2 = avg * buildOnDay;
                    }
                  } else if (angular.isDefined(area.numberOfPanels) && area.numberOfPanels > 0) {
                    if (angular.isDefined(area.areaOfPanels) && area.areaOfPanels > 0) {
                      avg = area.areaOfPanels / area.numberOfPanels;
                      m2 = avg * buildOnDay;
                    }
                  } else if (angular.isDefined(area.estimatedPanels) && area.estimatedPanels > 0) {
                    if (angular.isDefined(area.estimatedArea) && area.estimatedArea > 0) {
                      avg = area.estimatedArea / area.estimatedPanels;
                      m2 = avg * buildOnDay;
                    }
                  }
                  week[productionLine][buildDate].planM2 += m2;
                  week[productionLine][buildDate].planPanels += buildOnDay;
                } 
              }else{
                if (angular.isDefined(areaPlans[area.$id]) && angular.isDefined(areaPlans[area.$id].buildDays) && angular.isDefined(areaPlans[area.$id].buildDays[buildDate])) {
                  var buildOnDay = areaPlans[area.$id].buildDays[buildDate];
                  var m2 = 0;
                  var avg = 0;
                  if (angular.isDefined(area.actualPanels) && area.actualPanels > 0) {
                    if (angular.isDefined(area.actualArea) && area.actualArea > 0) {
                      avg = area.actualArea / area.actualPanels;
                      m2 = avg * buildOnDay;
                    }
                  } else if (angular.isDefined(area.numberOfPanels) && area.numberOfPanels > 0) {
                    if (angular.isDefined(area.areaOfPanels) && area.areaOfPanels > 0) {
                      avg = area.areaOfPanels / area.numberOfPanels;
                      m2 = avg * buildOnDay;
                    }
                  } else if (angular.isDefined(area.estimatedPanels) && area.estimatedPanels > 0) {
                    if (angular.isDefined(area.estimatedArea) && area.estimatedArea > 0) {
                      avg = area.estimatedArea / area.estimatedPanels;
                      m2 = avg * buildOnDay;
                    }
                  }
                  week[productionLine][buildDate].planM2 += m2;
                  week[productionLine][buildDate].planPanels += buildOnDay;
                }
              }
              //for manufactured data
              if (productionLine !== null){
                week[productionLine][buildDate].manufacturedPanels = 0;
                if(angular.isDefined(productionStats[productionLine][buildDate])){
                  if(angular.isDefined(productionStats[productionLine][buildDate].completedM2)){
                    week[productionLine][buildDate].manufacturedM2 = productionStats[productionLine][buildDate].completedM2;
                  }
                }        
                if(angular.isDefined(productionStats[productionLine][buildDate] && angular.isDefined(productionStats[productionLine][buildDate].completedCount))){
                  if(angular.isDefined(productionStats[productionLine][buildDate].completedCount)){
                    week[productionLine][buildDate].manufacturedPanels = productionStats[productionLine][buildDate].completedCount;
                  }
                }
              }
            });
          }
          firstMonday = dateUtil.plusDays(firstMonday, 7);
        }
        
        var maxArr = [];
        var max = 0;

        angular.forEach(week, function ( productionLineData, productionLine ) {
          angular.forEach(productionLineData, function ( dayData, buildDate ) {
            var monday = dateUtil.weekStart(buildDate);
            weekHolder[productionLine][monday].planM2 += dayData.planM2;
            weekHolder[productionLine][monday].planPanels += dayData.planPanels;
            weekHolder[productionLine][monday].manufacturedM2 += dayData.manufacturedM2;
            weekHolder[productionLine][monday].manufacturedPanels += dayData.manufacturedPanels;
          });
        });
        
        angular.forEach(weekHolder, function ( productionLineData, productionLine ) {
          angular.forEach(productionLineData, function ( weekData, buildDate ) {
            maxArr.push(weekData.planM2);
            maxArr.push(weekData.manufacturedM2);
          });
        });
        
        max = maxArr.reduce(function ( a, b ) {
          return Math.max(a, b);
        });
        
        var returnData = {
          weekHolder: weekHolder,
          week: week,
          max: max
        };
        
        return returnData;
      }

      function selectAreasForProduction() {
        buildAreas = [];
        var deferredList = [];

        angular.forEach(areas, function ( area, key ) {
          if (!thirdPartySuppliers[area.supplier]) {
            var areaStartDefined = false;
            var project = projects.$getRecord(area.project);
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if (project.active) {
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._id = getAreaId(area, project);
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);
                if(angular.isDefined(project.designer)){
                  area._designer = project.designer;
                }else{
                  area._designer = "";
                }
                if (area.line > 1) {
                  var productionLine = productTypeProductionLine[area.type] || null;
                  if (productionLine === "SIP") {
                    area._productionLine = productionLine + area.line;
                    area._line = area.line;
                  } else {
                    area._productionLine = productionLine;
                    area._line = 1;
                  }
                } else {
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }

                if (angular.isDefined(areaPlans[area.$id]) && angular.isDefined(areaPlans[area.$id].plannedStart) && angular.isDefined(areaPlans[area.$id].plannedFinish)) {
                  area._startDate = areaPlans[area.$id].plannedStart;
                  area._finishDate = areaPlans[area.$id].plannedFinish;
                  area._riskDays = getRiskDays(area);
                  areaStartDefined = true;
                }

                if(areaStartDefined){
                  buildAreas.push(area);
                }
                
              }
            }
          }

        });

        buildAreas = $filter("orderBy")(buildAreas, "_startDate");
        
        function getRiskDays(area) {
          if (useUnpublishedOnly(area)) {
            return null;
          }

          if (!area._finishDate || !area._riskDate) {
            return undefined;
          }

          var riskDate = useUnpublished(area) ? area._unpublishedRiskDate : area._riskDate;

          return dateUtil.daysBetweenWeekDaysOnly(area._finishDate, riskDate);
        }

        function useUnpublishedOnly(area) {
          return area._hasUnpublishedOnly && !useUnpublished(area);
        }
        function useUnpublished(area) {
          return area._hasUnpublished;
        }        
        
        return $q.all(deferredList);
      }
      
      function areasForWeek(){

        var areasByStart = {};
        var chartData = {
          "planned":[],
          "manufactured":[]
        };
        angular.forEach(buildAreas, function ( area, key ) {
          var panelWeek = dateUtil.weekStart(area._startDate);
          if (angular.isUndefined(areasByStart[panelWeek])) {
            areasByStart[panelWeek] = {};
          }
          if (angular.isUndefined(areasByStart[panelWeek][area._startDate])) {
            areasByStart[panelWeek][area._startDate] = [];
          }
          areasByStart[panelWeek][area._startDate].push(area);
        });
        
        angular.forEach(weeks.weekHolder, function ( productionLineData, productionLine ) {
          angular.forEach(productionLineData, function ( data, week ) {
            if (angular.isUndefined(chartData.planned[productionLine])) {
              chartData.planned[productionLine] = [];
              
            }
            if (angular.isUndefined(chartData.manufactured[productionLine])) {
              chartData.manufactured[productionLine] = [];
              
            }
            var plannedM2 = data.planM2.toFixed(1);
            var manufacturedM2 = data.manufacturedM2.toFixed(1);
            
            chartData.planned[productionLine].push({x: week, y: parseFloat(plannedM2)});
            chartData.manufactured[productionLine].push({x: week, y: parseFloat(manufacturedM2)});
          });
        });
        
        var returnData = {
          daysOfWeeks: daysOfWeeks,
          productionStats: productionStats,
          chartData: chartData,
          weeks: weeks.week,
          max: weeks.max,
          additionalData: additionalData,
          areasByStart: areasByStart
        };
        
        return returnData;
        
      }
      
      function getAreaId( area, project ) {
        return [project.id, area.phase, area.floor, area.type].join("-");
      }

    }
    
    function areasForDesign(selectDate) {
      
      var weekStartReturn = "";
      
      var buildAreas = [];

      var deferred = $q.defer();
      
      var weeks = {};
      var daysOfWeeks = {};

      initialise()
        .then(function () {
          //push forward for design handover but show as this week
          daysOfWeeks = getWeekDays();
          //selectDate = dateUtil.plusDays(selectDate, designHandoverPeriod);
          weekStartReturn = dateUtil.weekStart(selectDate);
          return firebaseUtil.loaded(projects, areas);
        })
        .then(function () {
          var areasRef = schema.getRoot().child("additionalData/areas");
          return areasRef.once("value", function ( snapshot ) {
            snapshot.forEach(function ( snapPlan ) {
              var areaKey = snapPlan.key;
              var area = schema.getObject(areasRef.child(areaKey));
              additionalData[areaKey] = area;
            });
          });
        })
        .then(function () {
          return selectAreasForDesign();
        })
        .then(function (buildAreas) {
          daysOfWeeks = getWeekDays();
          weeks = getWeeks(6, 21);
          return areasForWeek(buildAreas);
        })
        .then(function (data) {
          deferred.resolve(data);
        });

      return deferred.promise;
      
      function getWeekDays(){
        var weekStart = dateUtil.weekStart(selectDate);
        var week = {};
        for(var i = 0; i < 6; i++){
          var day = dateUtil.plusDays(weekStart, i);
          if(angular.isUndefined(week[day])){
            week[day] = {};
          }
        }
        return week;
      }
      
      function bufferDFM(statsMap, storeData){
        var designIssued = {
          "totalCompleteCount": 0,
          "totalDesignedCount": 0,
          "totalCompleteM2": 0,
          "totalDesignedM2": 0
        };
        angular.forEach(statsMap, function (data, productionLine){
          angular.forEach(data, function (stats, date){
            if(date <= storeData){
              designIssued.totalCompleteM2 += stats.completedM2 || 0;
              designIssued.totalCompleteCount += stats.completedCount || 0;
              designIssued.totalDesignedM2 += stats.designedM2 || 0;
              designIssued.totalDesignedCount += stats.designedCount || 0;
            }
          });

        });
          
        return designIssued;
      }
      
      function getWeeks(weeksAmount, dayzGone, b) {
        var weekHolder = {};
        var week = {};
        var monday = dateUtil.weekStart(selectDate);
        var firstMonday = dateUtil.minusDays(monday, dayzGone);
        for (var j = 0; j <= weeksAmount; j++) {
          for (var i = 0; i < 6; i++) {
            var buildDate = dateUtil.plusDays(firstMonday, i);
            if (angular.isUndefined(week[buildDate])) {
              week[buildDate] = {
                areaOfPanels: 0,
                numberOfPanels: 0,
                predictedPlanM2: 0,
                predictedPlanPanels: 0,
                actualPlanM2: 0,
                actualPlanPanels: 0,
                designIssued: bufferDFM(productionStats, buildDate)
              };
            }
            if (angular.isUndefined(weekHolder[firstMonday])) {
              weekHolder[firstMonday] = {
                areaOfPanels: 0,
                numberOfPanels: 0,
                predictedPlanM2: 0,
                predictedPlanPanels: 0,
                actualPlanM2: 0,
                actualPlanPanels: 0
              };
            }
            angular.forEach(buildAreas, function ( area, key ) {
              if(buildDate === area._designHandoverDate){
                if (area.actualArea > 0 && area.actualPanels > 0) {
                  week[buildDate].actualPlanM2 += area.actualArea;
                  week[buildDate].actualPlanPanels += area.actualPanels;
                }else{
                  if (area.estimatedArea > 0 && area.estimatedPanels > 0) {
                    week[buildDate].predictedPlanM2 += area.estimatedArea;
                    week[buildDate].predictedPlanPanels += area.estimatedPanels;
                  }
                  if (area.areaOfPanels > 0 && area.numberOfPanels > 0) {
                    week[buildDate].areaOfPanels += area.areaOfPanels;
                    week[buildDate].numberOfPanels += area.numberOfPanels;
                  }
                }
              }
            });
          }
          firstMonday = dateUtil.plusDays(firstMonday, 7);
        }

        angular.forEach(week, function ( dayData, buildDate ) {
          var monday = dateUtil.weekStart(buildDate);
          weekHolder[monday].numberOfPanels += dayData.numberOfPanels;
          weekHolder[monday].areaOfPanels += dayData.areaOfPanels;
          weekHolder[monday].predictedPlanM2 += dayData.predictedPlanM2;
          weekHolder[monday].predictedPlanPanels += dayData.predictedPlanPanels;
          weekHolder[monday].actualPlanM2 += dayData.actualPlanM2;
          weekHolder[monday].actualPlanPanels += dayData.actualPlanPanels;
        });
        
        var returnData = {
          weekHolder: weekHolder,
          week: week
        };
        
        return returnData;
      }

      function selectAreasForDesign() {
        buildAreas = [];
        var deferredList = [];

        angular.forEach(areas, function ( area, key ) {
          if (!thirdPartySuppliers[area.supplier] && angular.isDefined(productTypeProductionLine[area.type])) {
            var project = projects.$getRecord(area.project);
            var deliveryDate = area.revisions[project.deliverySchedule.published];
            var unpublishedDeliveryDate = area.revisions[project.deliverySchedule.unpublished];
            if (project.active) {
              if (deliveryDate || unpublishedDeliveryDate) {
                // Populate "_deliveryDate" even if there isn't one, so it appears in the planner, but flag
                // as "_hasUnpublishedOnly" so we don't include it in the capacity calculations
                area._projectId = project.id;
                area._deliveryDate = deliveryDate || unpublishedDeliveryDate;
                area._hasUnpublished = unpublishedDeliveryDate && !(deliveryDate && deliveryDate == unpublishedDeliveryDate);
                area._hasUnpublishedOnly = !deliveryDate;
                area._id = getAreaId(area, project);
                area._dispatchDate = dateUtil.subtractWorkingDays(area._deliveryDate, 1);
                area._riskDate = dateUtil.subtractWorkingDays(area._deliveryDate, 2);
                area._designHandoverDate = dateUtil.subtractWorkingDays(area._deliveryDate, designHandoverPeriod);                
                if(angular.isDefined(areaPlans[area.$id])){
                  area._finishDate = areaPlans[area.$id].plannedFinish;
                  area._startDate = areaPlans[area.$id].plannedStart;
                }                
                if(angular.isDefined(project.designer)){
                  area._designer = project.designer;
                }else{
                  area._designer = "";
                }
                if (area.line > 1) {
                  var productionLine = productTypeProductionLine[area.type] || null;
                  area._riskDays = getRiskDays(area);
                  if (productionLine === "SIP") {
                    area._productionLine = productionLine + area.line;
                    area._line = area.line;
                  } else {
                    area._productionLine = productionLine;
                    area._line = 1;
                  }
                } else {
                  area._productionLine = productTypeProductionLine[area.type] || null;
                  area._line = 1;
                  area._riskDays = getRiskDays(area);
                }
                if (area._hasUnpublished) {
                  area._unpublishedDeliveryDate = unpublishedDeliveryDate;
                  area._unpublishedDispatchDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 1);
                  area._unpublishedRiskDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, 2);
                  area._unpublishedDesignHandoverDate = dateUtil.subtractWorkingDays(unpublishedDeliveryDate, designHandoverPeriod);
                }
                buildAreas.push(area);
                
              }
            }
          }

        });
        
        
        function getRiskDays(area) {
          if (useUnpublishedOnly(area)) {
            return null;
          }

          if (!area._finishDate || !area._riskDate) {
            return undefined;
          }

          var riskDate = useUnpublished(area) ? area._unpublishedRiskDate : area._riskDate;

          return dateUtil.daysBetweenWeekDaysOnly(area._finishDate, riskDate);
        }

        function useUnpublishedOnly(area) {
          return area._hasUnpublishedOnly && !useUnpublished(area);
        }
        function useUnpublished(area) {
          return area._hasUnpublished;
        } 
        
        

        buildAreas = $filter("orderBy")(buildAreas, "_designHandoverDate");

        return $q.all(deferredList);
      }
      
      function areasForWeek(){

        var areasByDesignHandoverDate = {};
        
        var chartData = {
          "predictedPlanM2": [],
          "actualPlanM2": [],
          "areaOfPanels": []
        };
        
        angular.forEach(buildAreas, function ( area, key ) {
          var panelWeek = dateUtil.weekStart(area._designHandoverDate);
          if (angular.isUndefined(areasByDesignHandoverDate[panelWeek])) {
            areasByDesignHandoverDate[panelWeek] = {};
          }
          if (angular.isUndefined(areasByDesignHandoverDate[panelWeek][area._designHandoverDate])) {
            areasByDesignHandoverDate[panelWeek][area._designHandoverDate] = [];
          }
          areasByDesignHandoverDate[panelWeek][area._designHandoverDate].push(area);
        });
        
        angular.forEach(weeks.weekHolder, function ( data, week ) {
          var predictedPlanM2 = 0;
          var actualPlanM2 = 0;
          var areaOfPanels = 0;
          if (angular.isDefined(data.predictedPlanM2)){
            predictedPlanM2 = data.predictedPlanM2;
          }
          if (angular.isDefined(data.actualPlanM2)){
            actualPlanM2 = data.actualPlanM2;
          }
          if (angular.isDefined(data.areaOfPanels)){
            areaOfPanels = data.areaOfPanels;
          }
          chartData["areaOfPanels"].push({x: week, y: areaOfPanels});
          chartData["actualPlanM2"].push({x: week, y: actualPlanM2});
          chartData["predictedPlanM2"].push({x: week, y: predictedPlanM2});
        });
        
        var returnData = {
          designHandoverPeriod: designHandoverPeriod,
          weekStartReturn: weekStartReturn,
          daysOfWeeks: daysOfWeeks,
          chartData: chartData,
          weeks: weeks.week,
          additionalData: additionalData,
          areasByDesignHandoverDate: areasByDesignHandoverDate
        };
        
        return returnData;
        
      }
      
      function getAreaId( area, project ) {
        return [project.id, area.phase, area.floor, area.type].join("-");
      }

    }
    
    function calculateM2(area, buildOnDay){
      var m2 = 0;
      var avg = 0;
      if (angular.isDefined(area.actualPanels) && area.actualPanels > 0) {
        if (angular.isDefined(area.actualArea) && area.actualArea > 0) {
          avg = area.actualArea / area.actualPanels;
          m2 = avg * buildOnDay;
        }
      } else if (angular.isDefined(area.numberOfPanels) && area.numberOfPanels > 0) {
        if (angular.isDefined(area.areaOfPanels) && area.areaOfPanels > 0) {
          avg = area.areaOfPanels / area.numberOfPanels;
          m2 = avg * buildOnDay;
        }
      } else if (angular.isDefined(area.estimatedPanels) && area.estimatedPanels > 0) {
        if (angular.isDefined(area.estimatedArea) && area.estimatedArea > 0) {
          avg = area.estimatedArea / area.estimatedPanels;
          m2 = avg * buildOnDay;
        }
      }
      return m2;
    }
    
    function getOptionalNumber( value ) {
      if (angular.isDefined(value)) {
        return parseFloat(value);
      }
      return 0;
    }
    
    function lowestRisk( entryArr ) {
      var lowest = 0;
      angular.forEach(entryArr, function (element) {
        if(element < lowest){
          lowest = element;
        }
      });
      return lowest;
    }
  
  }

})();


