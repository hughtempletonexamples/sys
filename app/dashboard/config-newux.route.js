(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/dashboard/humanresource", {
        templateUrl: require("./humanresource.html"),
        controller: "humanResourceController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/dashboard/logistics", {
        templateUrl: require("./logistics.html"),
        controller: "LogisticsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/dashboard/booking", {
        templateUrl: require("./booking.html"),
        controller: "BookingController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/dashboard/production", {
        templateUrl: require("./production.html"),
        controller: "ProductionController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/dashboard/design", {
        templateUrl: require("./design.html"),
        controller: "DesignController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
