(function () {
  "use strict";

  angular
    .module("innView.dashboard", [
      "ngRoute",
      "ngMaterial",
      "ngMaterialDatePicker",
      "firebase",
      "innView.core",
      "innView.projects"
    ]);

})();
