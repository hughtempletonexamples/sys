(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .controller("humanResourceController", humanResourceController);

  /**
   * @ngInject
   */
  function humanResourceController(dashboardService, resourceService, schema, $scope, $window, $location) {
    var vm = this;
    
    vm.updateBench = updateBench;
    vm.goto = goto;
    vm.loaded = false;
    
    $scope.$on("$destroy", destroyHumanResource);
    
    var benchmarks = schema.getRoot().child("benchmarks");
    benchmarks.on("value", init);
      
    function init(){
      dashboardService.humanResource()
        .then(function (data){
          vm.loaded = true;
          vm.humanResource = data;
        });
    }
      
    function updateBench(benchmarks, productionLine, date, elem, amount){
      return resourceService.setBenchmark(benchmarks, productionLine, date, elem, amount);
    }
    
    function destroyHumanResource() {
      benchmarks.off("value", init);
      if (vm.humanResource) {
        delete vm.humanResource;
      }
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/dashboard/" + page);
      }
    }

  }
})();
