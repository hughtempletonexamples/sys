(function () {
  "use strict";

  require("./dashboard.module");

  require("./humanResource.controller");
  
  require("./booking.controller");
  
  require("./logistics.controller");
  
  require("./dashboard.service");

  require("./config.route");

})();
