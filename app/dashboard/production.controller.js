(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .controller("ProductionController", ProductionController);

  /**
   * @ngInject
   */
  function ProductionController(dashboardService, referenceDataService, schema, dateUtil, $timeout, $interval, $scope, $window, $filter, $location) {
    var vm = this;
    
    var refreshTimeout;
    var modifyChartTimeout;
    
    var staticAverages = {};
    staticAverages["SIP"] = {
      value:1000,
      color:"red"
    };
    staticAverages["TF"] = {
      value:700,
      color:"blue"
    };
    staticAverages["CASS"] = {
      value:300,
      color:"green"
    };
    vm.fromSelect = false;
    vm.initialLoad = true;
    vm.loaded = false;
    vm.selectDate  = dateUtil.todayAsIso();
    vm.init = init();
    vm.updateAddData = updateAddData;
    vm.updateAddCheckData = updateAddCheckData;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.hauliers = referenceDataService.getHaulierTypes();
    vm.vehicles = referenceDataService.getVehicleTypes();
    vm.getWeekDays = getWeekDays;
    vm.goto = goto;

    $scope.$on("$destroy", destroyBooking);

    $window.onresize = function (event) {
      modifyChart();
    };
    
    vm.options = {
      chart: {
        callback: function (e) {
          modifyChart();
        },
        type: "multiChart",
        height: 400,
        margin: {
          top: 20,
          right: 30,
          bottom: 45,
          left: 45
        },
        useInteractiveGuideline: false,
        bars1: {grouped: true},
        bars2: {grouped: true},
        duration: 500,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function ( d ) {
            return "Week " + $filter("date")(new Date(d), "w");
          }
        },
        yAxis1: {
          axisLabel: "m2 Output",
          tickFormat: function ( d ) {
            return d3.format(",.1f")(d);
          }
        }
      },
      yAxis2: {
        tickFormat: function (d) {
          return d3.format(",.1f")(d);
        }
      }
    }; 

    vm.productionPlans = schema.getRoot().child("productionPlans/areas");
    vm.productionPlans.on("child_changed", toDo);
    
    function toDo() {
      init();
      vm.productionPlans.off("child_changed", toDo);
      vm.onTimeout = $interval(function () {
        vm.productionPlans.on("child_changed", toDo);
      }, 5000);
    }
      
    function init(){          
      vm.backButton = true;
      vm.forwardButton = true;
      vm.weekStart = dateUtil.weekStart(vm.selectDate);
      dashboardService.areasForProduction(vm.selectDate)
        .then(function (data){
          vm.backButton = false;
          vm.forwardButton = false;
          vm.production = data;
          vm.chartData = data.chartData;
          if(vm.initialLoad){
            vm.loaded = true;
          }

          vm.data = [{
            type: "bar",
            yAxis: 1,
            key: "SIP Planned",
            color: "red",
            values: data.chartData.planned["SIP"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "TF Planned",
            color: "blue",
            values:  data.chartData.planned["TF"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "CASS Planned",
            color: "green",
            values:  data.chartData.planned["CASS"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "SIP Manufactured",
            color: "grey",
            opacity: 0.35,
            values:  data.chartData.manufactured["SIP"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "TF Manufactured",
            color: "grey",
            opacity: 0.35,
            values:  data.chartData.manufactured["TF"]
          },
          {
            type: "bar",
            yAxis: 2,
            key: "CASS Manufactured",
            color: "grey",
            opacity: 0.35,
            values:  data.chartData.manufactured["CASS"]
          }]; 

          vm.options.chart.yDomain1 = [0, data.max];
          vm.options.chart.yDomain2 = [0, data.max];

          if (vm.fromSelect || vm.initialLoad) {
            refreshTimeout = $timeout(function () {
              vm.chartAPI.refreshWithTimeout(50);
            }, 5);
          }
          vm.initialLoad = false;
          vm.fromSelect = false;
        });
 
    }
    
    
    
    
    function modifyChart() {
      modifyChartTimeout = $timeout(function () {
        var xpos = [];
        var allRects = d3.select(".productionChart").selectAll("g.nv-group").selectAll("rect.positive");
        angular.forEach(allRects, function (rects) {
          angular.forEach(rects, function (rect) {
            var d = rect.__data__;
            if (d.series === 2 && d.key === "CASS Manufactured (right axis)") {
              var transform = d3.select(rect).attr("transform");
              var x = d3.select(rect).attr("x");
              var width = d3.select(rect).attr("width");
              var newX = x - width;
              xpos.push({
                "transform": transform,
                "date": d.x,
                "x": newX
              });
            }
          });
        });
        d3.select(".productionChart").select(".nv-x.nv-axis > g").selectAll("g").selectAll("line").remove();
        var allTexts = d3.select(".productionChart").select(".nv-x.nv-axis > g").selectAll("g").selectAll("text");
        if (d3.select(".productionChart")[0][0].clientWidth > 550) {
          angular.forEach(allTexts, function (texts) {
            angular.forEach(texts, function (text) {
              var d = text.__data__;
              if (d !== "Week") {
                d3.select(text).remove();
              } else {
                for (var i = 0; i < xpos.length; i++) {
                  d3.select(text.parentNode).append("text")
                    .attr("transform", xpos[i].transform)
                    .text(function () {
                      return  "Week " + $filter("date")(new Date(xpos[i].date), "w");
                    })
                    .attr("y", function () {
                      return 12;
                    })
                    .attr("x", function () {
                      return xpos[i].x;
                    })
                    .style("fill", "black");
                }
              }
            });
          });
        }
        d3.select(".productionChart").select(".nv-x.nv-axis > g").selectAll("g").selectAll(".tick").remove();
      }, 100);
    }
    
    function drawLine(lineId, value, color, text, theClass, dash) {
      
      var chart = vm.chartAPI.getScope().chart;
      var svg = d3.select(theClass).select("svg");
      var yScale = chart.yAxis.scale();
      var margin = chart.margin();
      var height = chart.height();
      var width = svg.style("width").split("px")[0];
      
      if (dash) {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-dasharray", "10, 5")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      } else {
        svg.append("line")
          .style("stroke", color)
          .style("stroke-width", "1px")
          .attr("class", lineId)
          .attr("x1", margin.left)
          .attr("y1", yScale(value) + margin.top)
          .attr("x2", +width - margin.right)
          .attr("y2", yScale(value) + margin.top);
      }
 
      svg.append("text")
        .style("stroke", color)
        .attr("class", lineId + "-text")
        .attr("x", +width - margin.right / 2)
        .attr("y", yScale(value) + margin.top)
        .attr("text-anchor", "end")
        .text(text);
    }
    
    function backWeek(){
      var newDate = dateUtil.minusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function forwardWeek(){
      var newDate = dateUtil.plusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function destroyBooking() {
      vm.productionPlans.off("child_changed", toDo);
      if (vm.production) {
        delete vm.production;
      }
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (modifyChartTimeout) {
        $timeout.cancel(modifyChartTimeout);
      }
      if (refreshTimeout) {
        $timeout.cancel(refreshTimeout);
      }
      if (vm.onTimeout){
        $timeout.cancel(vm.onTimeout);
      }
    }
    
    function updateAddCheckData( areaRef, type, check ) {
      return dashboardService.updateAddCheckData(areaRef, type, check)
              .then(init);
    }
    function getWeekDays() {
      var monday = dateUtil.weekStart(new Date());
      var week = {};
      for (var i = 0; i < 6; i++) {
        var day = dateUtil.plusDays(monday, i);
        if (angular.isUndefined(week[day])) {
          week[day] = {
            deliverys: 0,
            booked: 0,
            unplanned: 0
          };
        }
      }
      return week;
    }    

    function updateAddData( area, label, newData ) {
      return dashboardService.updateAddData(area, label, newData)
              .then(init);
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/dashboard/" + page);
      }
    }

  }
})();
