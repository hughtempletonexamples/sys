(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .controller("BookingController", BookingController);

  /**
   * @ngInject
   */
  function BookingController(dashboardService, referenceDataService, panelsService, schema, dateUtil, $mdSidenav, $scope, $mdToast, $window, $q, $filter, $mdDialog, $document, $location, $timeout, firebaseUtil) {
    var vm = this;
    
    var refreshTimeout;
    
    vm.selectDate  = dateUtil.todayAsIso();
    vm.init = init();
    vm.areaChecker = areaChecker;
    vm.incBooking = incBooking;
    vm.addToDelivery = addToDelivery;
    vm.updateBookingData = updateBookingData;
    vm.updateBookingCheckData = updateBookingCheckData;
    vm.removeAreaFromBooking = removeAreaFromBooking;
    vm.removeBooking = removeBooking;
    vm.updateBookingAddCheckDataDelivered = updateBookingAddCheckDataDelivered;
    vm.showPanelsDelivered = showPanelsDelivered;
    vm.addAreaToBooking = addAreaToBooking;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.collapseAll = collapseAll;
    vm.hauliers = referenceDataService.getHaulierTypes();
    vm.vehicles = referenceDataService.getVehicleTypes();
    vm.toggleRight = buildToggler("right");
    vm.loaded = false;
    vm.isOpenRight = function () {
      return $mdSidenav("right").isOpen();
    };
    vm.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav("right").close()
        .then(function () {
        });
    };
    vm.goto = goto;
    vm.fromSelect = false;
    vm.initialLoad = true;
    vm.expandables = {};
    vm.bookingExpandables = {};
    
    $scope.$on("$destroy", destroyBooking);
    
    function collapseAll(data) {
      data.expanded = !data.expanded;
    }
    
    vm.options = {
      chart: {
        type: "multiBarChart",
        height: 400,
        margin: {
          top: 20,
          right: 20,
          bottom: 45,
          left: 45
        },
        grouped: true,
        duration: 500,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function ( d ) {
            return "Week " + $filter("date")(new Date(d), "w");
          }
        },
        yAxis1: {
          axisLabel: "No of Deliveries",
          tickFormat: function ( d ) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };

    var bookings = schema.getRoot().child("bookings");
    bookings.on("value", init);
      
    function init(){
      vm.backButton = true;
      vm.forwardButton = true;
      vm.weekStart = dateUtil.weekStart(vm.selectDate);
      dashboardService.areasForBooking(vm.selectDate)
        .then(function (data){
          if(vm.initialLoad){
            vm.loaded = true;
          }
          vm.data = [{
            type: "bar",
            yAxis: 1,
            key: "Deliverys",
            color: "red",
            values: data.chartData["deliverys"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "Booked",
            color: "blue",
            values: data.chartData["booked"]
          }];
          if(vm.fromSelect || vm.initialLoad){
            vm.expandables = data.areasByDispatch;
            vm.bookingExpandables = data.groupByBookingDate;
            refreshTimeout = $timeout(function (){ 
              vm.chartAPI.refreshWithTimeout(50);
            },5);
          }
          vm.backButton = false;
          vm.forwardButton = false;
          vm.booking = data;
          vm.initialLoad = false;
          vm.fromSelect = false;
        });
    }

    function areaChecker(area){
      var isShow = false;
      if ((angular.isDefined(area.actualPanels) && area.actualPanels > 0) || area._productionLine === null){
        isShow = true;
      }
      return isShow;
    }
    
    function buildToggler(navID) {
      return function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {

          });
      };
    }
    
    function backWeek(){
      hideToast();
      var newDate = dateUtil.minusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function forwardWeek(){
      hideToast();
      var newDate = dateUtil.plusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function destroyBooking() {
      vm.loaded = false;
      bookings.off("value", init);
      if (vm.booking) {
        delete vm.booking;
      }
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (refreshTimeout) {
        $timeout.cancel(refreshTimeout);
      }
    }
     
    function parentElement() {
      return angular.element($document.body);
    }
    
    function incBooking() {
      return schema.getRoot().child("config/bookedCounter").transaction(function ( currentValue ) {
        return (currentValue || 0) + 1;
      }, function ( err, committed, counter ) {
        if (committed) {
          
          var monday = dateUtil.weekStart(vm.selectDate);
          var bookedTime = new Date(monday).getTime();
          var counter = counter.val();
          var initials = schema.status.userName.match(/\b\w/g) || [];
          initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();

          var obj = {
            "id": counter,
            "booked":{
              "status": true,
              "timeDate": firebaseUtil.serverTime(),
              "user": initials
            },
            "bookedTime": bookedTime
          };
          
          return schema.getRoot().child("bookings").push(obj);
        }
      })
      .then(init);

    }
    
    function addToDelivery(area){
      vm.selectedArea = area;
      $mdToast.show (
           $mdToast.simple()
           .textContent("Area Selected: " + area._id)                       
           .hideDelay(false)
        );
    }
    
    function showAToast(note){
      $mdToast.show (
           $mdToast.simple()
           .textContent(note)                       
           .hideDelay(3000)
        );
    }
    
    function hideToast(){
      $mdToast.hide();
      vm.selectedArea = "";
    }
    
    function addAreaToBooking(bookingRef, selectedArea) {  
      hideToast();
      return dashboardService.addAreaToBooking(bookingRef, selectedArea)
              .then(init);
    }

    function removeAreaFromBooking( area, bookingRef ) {
      return dashboardService.removeAreaFromBooking(area, bookingRef)
              .then(function () {
                $mdToast.show(
                  $mdToast.simple()
                  .textContent("Area removed from booking")
                  .hideDelay(10000)
                );
              });
    }
    
    function removeBooking( bookingRef, areas ) {
      return dashboardService.removeBooking(bookingRef, areas)
              .then(function () {
                $mdToast.show(
                  $mdToast.simple()
                  .textContent("Booking removed")
                  .hideDelay(10000)
                );
              });
    }
    
    function showPanelsDelivered(area, bookingRef) {
      if(area._productionLine !== null){
        $mdDialog.show({
          parent: parentElement(),
          templateUrl: require("./panelsDelivered.html"),
          locals: {
            area: area,
            bookingRef: bookingRef
          },
          controller: panelsDeliveredDialogController,
          controllerAs: "vm"
        }).then(function () {
          init();
        });
      }
    }
    
    function panelsDeliveredDialogController($mdDialog, $log, area, bookingRef) {
      var vm = this;

      vm.panelsDeliveredTemplate = [];
      vm.area = area;
      vm.bookingRef = bookingRef;

      initialize();

      ///////////

      function initialize() {
        if (angular.isDefined(area) && angular.isDefined(bookingRef)) {
          panelsService.panelsForArea(area.$id)
            .$loaded()
            .then(function (panels) {
              var sortedPanels = [];
              angular.forEach(panels, function ( panel ) {
                if(angular.isDefined(panel.booking) && panel.booking === bookingRef){
                  panel.isBooked = true;
                  sortedPanels.push(panel);
                }
              });
              vm.panelsDeliveredTemplate = sortedPanels;
            });  
        }
      }

      vm.closeDialog = function () {
        $mdDialog.hide();
      };
      
      vm.togglePanels = function (panelRef, isBooked) {
        
        if(isBooked){
          return dashboardService.setPanel( panelRef , "booking", vm.bookingRef)
                  .catch(showError);
        }else{
          return dashboardService.setPanel( panelRef , "booking", null)
                  .catch(showError);
        }
        
      };

      function showError(error) {
        $mdDialog.show(
          $mdDialog.alert()
          .clickOutsideToClose(true)
          .title("There was a problem")
          .textContent(error)
          .ok("Ok")
          .parent(parentElement())
        );
      }
    }

    function updateBookingAddCheckData( bookingRef, type, check ) {
      return dashboardService.updateBookingAddCheckData(bookingRef, type, check);
    }
    
    function updateBookingAddCheckDataDelivered( bookingRef, check ) {
      return dashboardService.updateBookingAddCheckDataDelivered(bookingRef, check)
              .then(function () {
                $mdToast.show($mdToast.simple().textContent("Booking delivered updated").hideDelay(10000));
                init();
              });
    }
    
    function updateBookingCheckData(bookingRef, label, newData){ 
      return dashboardService.updateBookingCheckData(bookingRef, label, newData); 
    }
    
    function updateBookingData( bookingRef, label, newData ) {
      return dashboardService.updateBookingData( bookingRef, label, newData );
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/dashboard/" + page);
      }
    }

  }
})();
