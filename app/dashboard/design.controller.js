(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .controller("DesignController", DesignController);

  /**
   * @ngInject
   */
  function DesignController(dashboardService, referenceDataService, schema, areasService, dateUtil, $scope, $document, $window, $mdDialog, $filter, $timeout, $interval, $location) {
    var vm = this;
    
    var refreshTimeout;
    
    vm.selectDate  = dateUtil.todayAsIso();
    vm.init = init();
    vm.updateAddData = updateAddData;
    vm.updateAddCheckData = updateAddCheckData;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.hauliers = referenceDataService.getHaulierTypes();
    vm.vehicles = referenceDataService.getVehicleTypes();
    vm.goto = goto;
    vm.fromSelect = false;
    vm.initialLoad = true;
    vm.loaded = false;
    vm.editAreaPanelsAndMeterSquared = editAreaPanelsAndMeterSquared;
    vm.areasWithActuals = areasWithActuals;
    
    $scope.$on("$destroy", destroyBooking);
    
    vm.options = {
      chart: {
        type: "multiBarChart",
        height: 400,
        margin: {
          top: 20,
          right: 20,
          bottom: 45,
          left: 45
        },
        grouped: true,
        duration: 500,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function ( d ) {
            return "Week " + $filter("date")(new Date(d), "w");
          }
        },
        yAxis1: {
          axisLabel: "No of Deliveries",
          tickFormat: function ( d ) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };
    
    vm.productionPlans = schema.getRoot().child("productionPlans/areas");
    vm.productionPlans.on("child_changed", toDo);
    
    function toDo() {
      init();
      vm.productionPlans.off("child_changed", toDo);
      vm.onTimeout = $interval(function () {
        vm.productionPlans.on("child_changed", toDo);
      }, 5000);
    }
 
    function init(){
      vm.backButton = true;
      vm.forwardButton = true;
      dashboardService.areasForDesign(vm.selectDate)
        .then(function (data){
          vm.weekStart = data.weekStartReturn;
          vm.backButton = false;
          vm.forwardButton = false;
          vm.booking = data;
          if(vm.initialLoad){
            vm.loaded = true;
          }
          vm.data = [
            {
              type: "bar",
              yAxis: 1,
              key: "m2 (design entered)",
              color: "green",
              values: data.chartData["areaOfPanels"]
            },
            {
              type: "bar",
              yAxis: 1,
              key: "m2 (estimated)",
              color: "red",
              values: data.chartData["predictedPlanM2"]
            },
            {
              type: "bar",
              yAxis: 1,
              key: "m2 (actual)",
              color: "black",
              values: data.chartData["actualPlanM2"]
            }
          ];
          if(vm.fromSelect || vm.initialLoad){
            refreshTimeout = $timeout(function (){ 
              vm.chartAPI.refreshWithTimeout(50);
            },5);
          }
          vm.initialLoad = false;
          vm.fromSelect = false;
        });
    }
    
    function backWeek(){
      var newDate = dateUtil.minusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function forwardWeek(){
      var newDate = dateUtil.plusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function destroyBooking() {
      vm.productionPlans.off("child_changed", toDo);
      if (vm.booking) {
        delete vm.booking;
      }
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (refreshTimeout) {
        $timeout.cancel(refreshTimeout);
      }
      if (vm.onTimeout){
        $timeout.cancel(vm.onTimeout);
      }
    }
    
    function areasWithActuals(areasArr){
      var count = 0;
      angular.forEach(areasArr, function ( area, key ) {
        area.panStatus = false;
        if(angular.isDefined(area.actualPanels) && area.actualPanels > 0){
          area.panStatus = true;
          count++;
        }
      });
      return count;
    }
    
    
    function updateAddCheckData( areaRef, type, check ) {
      return dashboardService.updateAddCheckData(areaRef, type, check)
              .then(init);
    }

    function updateAddData( area, label, newData ) {
      return dashboardService.updateAddData(area, label, newData)
              .then(init);
    }
    
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page
      }else{
        $location.url("/dashboard/" + page);
      }
    }
    
    function parentElement() {
      return angular.element($document.body);
    }
    
    function editAreaPanelsAndMeterSquared(areaId) {
      $mdDialog.show({
        parent: parentElement(),
        templateUrl: require("./editAreaPanelsAndMeterSquared.html"),
        locals: {
          areaId: areaId
        },
        controller: areaDialogController,
        controllerAs: "vm"
      });
    }
    
    function areaDialogController($mdDialog, areaId) {
      var vm = this;
      
      vm.areaTemplate = {};

      var area = schema.getObject(schema.getRoot().child("areas").child(areaId));
      
      init();

      ////////////

      function init() {
        if (angular.isDefined(areaId)) {
          area
            .$loaded()
            .then(function ( area ) {
              if (angular.isDefined(area.numberOfPanels)) {
                vm.areaTemplate.numberOfPanels = area.numberOfPanels;
              }
              if (angular.isDefined(area.areaOfPanels)) {
                vm.areaTemplate.areaOfPanels = area.areaOfPanels;
              }
            });
        }
      }
        
      vm.closeDialog = function () {
        $mdDialog.hide();
      };

      vm.saveArea = function (isValid) {
        if(isValid) {
          var modifiedArea = updateAreaFromTemplate(area, vm.areaTemplate);
          areasService
            .updateProjectArea(modifiedArea)
            .then(vm.closeDialog)
            .catch(showError);

        }
      };

      function updateAreaFromTemplate(area, template) {

        if (angular.isDefined(template.numberOfPanels)) {
          area.numberOfPanels = template.numberOfPanels;
        }
        
        if (angular.isDefined(template.areaOfPanels)) {
          area.areaOfPanels = template.areaOfPanels;
        }
        
        return area;
      }
    }
    
    function showError( error ) {
      $mdDialog.show(
              $mdDialog.alert()
              .clickOutsideToClose(true)
              .title("There was a problem")
              .textContent(error)
              .ok("Ok")
              .parent(parentElement())
              );
    }

  }
})();
