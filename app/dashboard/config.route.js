(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/dashboard/humanresource", {
        templateUrl: require("./humanresource.html"),
        controller: "humanResourceController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/dashboard/booking", {
        templateUrl: require("./booking.html"),
        controller: "bookingController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
