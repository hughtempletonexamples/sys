(function () {
  "use strict";
  
  require("angular-route");
  require("angular-material");
  require("angular-file-saver");

  require("./dashboard-newux.module");
  
  require("./dashboard.service");

  require("./humanResource.controller");
  
  require("./booking.controller");
  
  require("./logistics.controller");

  require("./production.controller");
  
  require("./design.controller");

  require("./config-newux.route");

})();
