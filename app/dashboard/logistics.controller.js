(function () {
  "use strict";

  angular
    .module("innView.dashboard")
    .controller("LogisticsController", LogisticsController);

  /**
   * @ngInject
   */
  function LogisticsController(dashboardService, referenceDataService, panelsService, schema, dateUtil, $mdSidenav, $scope, $mdToast, $window, $q, $filter, $mdDialog, $document, $location, $timeout, firebaseUtil) {
    var vm = this;
    
    var refreshTimeout;
    
    vm.selectDate  = dateUtil.todayAsIso();
    vm.init = init();
    vm.areaChecker = areaChecker;
    vm.calculatePercentage = calculatePercentage;
    vm.round = round;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.collapseAll = collapseAll;
    vm.keysLength = keysLength;
    vm.hauliers = referenceDataService.getHaulierTypes();
    vm.vehicles = referenceDataService.getVehicleTypes();
    vm.toggleRight = buildToggler("right");
    vm.loaded = false;
    vm.isOpenRight = function () {
      return $mdSidenav("right").isOpen();
    };
    vm.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav("right").close()
        .then(function () {
        });
    };
    vm.goto = goto;
    vm.fromSelect = false;
    vm.initialLoad = true;
    vm.expandables = {};
    vm.logisticsExpandables = {};
    
    $scope.$on("$destroy", destroyLogistics);
    
    function collapseAll(data) {
      data.expanded = !data.expanded;
    }
    
    vm.options = {
      chart: {
        type: "multiBarChart",
        height: 500,
        margin: {
          top: 20,
          right: 20,
          bottom: 45,
          left: 45
        },
        grouped: true,
        duration: 500,
        xAxis: {
          axisLabel: "Week",
          showMaxMin: false,
          tickFormat: function ( d ) {
            return "Week " + $filter("date")(new Date(d), "w");
          }
        },
        yAxis1: {
          axisLabel: "No of Dispatches",
          tickFormat: function ( d ) {
            return d3.format(",.1f")(d);
          }
        }
      }
    };

    var areas = schema.getRoot().child("areas");
    areas.on("value", init);

    function init(){
      vm.backButton = true;
      vm.forwardButton = true;
      vm.weekStart = dateUtil.weekStart(vm.selectDate);
      dashboardService.areasForLogistics(vm.selectDate)
        .then(function (data){
          if(vm.initialLoad){
            vm.loaded = true;
          }
          vm.data = [{
            type: "bar",
            yAxis: 1,
            key: "Dispatches",
            color: "red",
            values: data.chartData["deliverys"]
          },
          {
            type: "bar",
            yAxis: 1,
            key: "Booked",
            color: "blue",
            values: data.chartData["booked"]
          }];
          if(vm.fromSelect || vm.initialLoad){
            vm.expandables = data.areasByDispatch;
            vm.logisticsExpandables = data.groupByLogisticsDate;
            refreshTimeout = $timeout(function (){ 
              vm.chartAPI.refreshWithTimeout(50);
            },5);
          }
          vm.backButton = false;
          vm.forwardButton = false;
          vm.logistics = data;
          vm.initialLoad = false;
          vm.fromSelect = false;
        });
    }

    function areaChecker(area){
      var isShow = false;
      if ((angular.isDefined(area.actualPanels) && area.actualPanels > 0) || area._productionLine === null){
        isShow = true;
      }
      return isShow;
    }
    
    function buildToggler(navID) {
      return function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID)
          .toggle()
          .then(function () {

          });
      };
    }
    
    function backWeek(){
      hideToast();
      var newDate = dateUtil.minusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function forwardWeek(){
      hideToast();
      var newDate = dateUtil.plusDays(vm.selectDate,7);
      vm.selectDate = newDate;
      vm.fromSelect = true;
      init();
    }
    
    function destroyLogistics() {
      vm.loaded = false;
      areas.off("value", init);
      if (vm.logistics) {
        delete vm.logistics;
      }
      if (vm.chartAPI) {
        delete vm.chartAPI;
      }
      if (vm.options) {
        delete vm.options;
      }
      if (refreshTimeout) {
        $timeout.cancel(refreshTimeout);
      }
    }
     
    function goto(type, page){
      if (type === "global"){
        $window.location.href = "/index.html#/" + page;
      }else{
        $location.url("/dashboard/" + page);
      }
    }
    
    function keysLength(obj){
      if (angular.isDefined(obj)){
        return Object.keys(obj).length;
      }
      return 0;
    }
    
    function calculatePercentage(a, b) {
      if (b === 0) {
        return 0;
      }
      return Math.round((a/b)*100);
    }

    function round(value) {
      return Math.round(value * 100) / 100;
    }
    
    function showAToast(note){
      $mdToast.show (
           $mdToast.simple()
           .textContent(note)                       
           .hideDelay(3000)
        );
    }
    
    function hideToast(){
      $mdToast.hide();
      vm.selectedArea = "";
    }
    
  }
})();
