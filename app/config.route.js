(function () {
  "use strict";

  angular
    .module("innView")
    .config(config);

  /**
   * @ngInject
   */
  function config($routeProvider) {
    $routeProvider.otherwise({redirectTo: "/home"});
  }
})();
