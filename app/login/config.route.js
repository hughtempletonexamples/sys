(function () {
  "use strict";

  angular
    .module("innView.login")
    .run(authInterceptor)
    .config(initRoutes);

  /**
   * @ngInject
   */
  function authInterceptor($rootScope, $location, $timeout) {
    var rootChangeError = $rootScope.$on("$routeChangeError", function (event, next, previous, error) {
      if (error === "AUTH_REQUIRED") {
        $location.search("from", $location.path());
        $location.path("/login");
      }
    });

    var sessionExpiredTimer;

    var rootChangeSuccess = $rootScope.$on("$routeChangeSuccess", function (event, current) {
      if (current.locals.auth) {
        var sessionExpiry = current.locals.auth.expires * 1000 - Date.now();
        if (sessionExpiry > 0) {
          $timeout.cancel(sessionExpiredTimer);
          sessionExpiredTimer = $timeout(sessionExpired, sessionExpiry);
        }
      }
    });

    $rootScope.$on("$destroy", rootChangeError);
    $rootScope.$on("$destroy", rootChangeSuccess);

    function sessionExpired() {
      $location.url("/login?logout");
    }
  }

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/login", {
        templateUrl: require("./login.html"),
        controller: "AuthController",
        controllerAs: "vm",
        reloadOnSearch: false,
        resolve: {
          auth: waitForAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function waitForAuth(schema) {
    return schema.waitForAuth();
  }

})();
