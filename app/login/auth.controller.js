(function () {
  "use strict";

  angular
    .module("innView.login")
    .controller("AuthController", AuthController);

  /**
   * @ngInject
   */
  function AuthController($location, $routeParams, schema, auth) {
    var vm = this;

    vm.login = login;

    vm.errorMessage = null;
    vm.redirectTo = $routeParams.from || "/";
    vm.loggedOut = !!$routeParams.logout;

    vm.qaApp = isQaApp();

    vm.email = "";
    vm.password = "";

    init();

    ////////////

    function init() {
      $location.search("from", null);
      $location.search("logout", null);

      if (vm.loggedOut && auth) {
        schema.logout();
      }
    }

    function isQaApp() {
      return $location.absUrl().search("/qa/#") >= 0;
    }

    function login(isValid) {
      if (isValid) {
        vm.loggedOut = false;
        vm.errorMessage = null;

        schema.login(vm.email, vm.password)
          .then(loginSuccess)
          .catch(loginFailed);
      }

      function loginSuccess(authData) {
        $location.url(vm.redirectTo);
      }

      function loginFailed(error) {
        vm.errorMessage = "Unknown email or incorrect password";
      }
    }

  }
})();
