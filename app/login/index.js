(function () {
  "use strict";

  require("./login.module");

  require("./auth.controller");

  require("./config.route");

})();