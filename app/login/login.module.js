(function () {
  "use strict";

  angular
    .module("innView.login", [
      "ngRoute",
      "innView.core"
    ]);
})();
