(function () {
  "use strict";

  angular
    .module("innView.materialrequests")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/materialrequests", {
        templateUrl: require("./materialrequests.html"),
        controller: "MaterialRequestsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialrequests/updateMaterialRequest", {
        templateUrl: require("./createMaterialRequest.html"),
        controller: "MaterialRequestsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialrequests/updateMaterialRequest/:requestKey", {
        templateUrl: require("./updateMaterialRequest.html"),
        controller: "MaterialRequestsController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
