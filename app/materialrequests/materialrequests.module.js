(function () {
  "use strict";

  angular
    .module("innView.materialrequests", [
      "ngRoute",
      "firebase",
      "innView.core"
    ]);

})();
