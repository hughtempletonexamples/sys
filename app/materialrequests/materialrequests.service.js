(function () {
  "use strict";

  angular
    .module("innView.materialrequests")
    .factory("materialrequestsService", materialrequestsService);

  /**
   * @ngInject
   */
  function materialrequestsService($log, schema, firebaseUtil) {

    var service = {
      getMaterialRequest: getMaterialRequest,
      getMaterialRequests: getMaterialRequests,
      updateMaterialRequest: updateMaterialRequest,
      getMaterialRequestByProjectId:getMaterialRequestByProjectId
    };

    return service;

    ////////////
    
    function updateMaterialRequest(materialRequest) {

      if (angular.isDefined(materialRequest.$id)) {
        return getMaterialRequest(materialRequest.$id)
          .$loaded()
          .then(function (existing) {
            angular.extend(existing, materialRequest);
            setModifiedTimestamp(existing);
            return existing.$save();
          });

      } else {

        return getMaterialRequests()
          .$loaded()
          .then(function (materialRequests) {
            var changes = {};
            changes.action = false;
            angular.extend(materialRequest, changes);
            setModifiedTimestamp(materialRequest);
            return materialRequests.$add(materialRequest);
          });

      }

    }
    
    function getMaterialRequest(key) {
      return schema.getObject(schema.getRoot().child("materialrequests").child(key));
    }
    function getMaterialRequestByProjectId(projectId) {

      return schema.getArray(schema.getRoot().child("materialrequests").orderByChild("projectId").equalTo(projectId));
            
    }

    function getMaterialRequests() {
      return schema.getArray(schema.getRoot().child("materialrequests"));
    }
    
    function setModifiedTimestamp(item) {
      // Ensure existing data has a created timestamp
      if (!item.timestamps) {
        item.timestamps = {
          created: firebaseUtil.serverTime()
        };
      }

      item.timestamps.modified = firebaseUtil.serverTime();
    }
  }
  
})();
