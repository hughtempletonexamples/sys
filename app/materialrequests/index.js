(function () {
  "use strict";

  require("./materialrequests.module");

  require("./materialrequests.controller");
  
  require("./materialrequests.service");

  require("./config.route");

})();
