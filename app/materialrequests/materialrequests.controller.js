/* global MATERIALREQUESTFORM:false */
(function () {
  "use strict";

  angular
    .module("innView.materialrequests")
    .controller("MaterialRequestsController", MaterialRequestsController);

  /**
   * @ngInject
   */
  function MaterialRequestsController($log, $http, $confirm, $routeParams, $location, materialrequestsService, projectsService, schema) {
    var vm = this;
    vm.counteroo = 0;
    vm.materialRequest = {};
    vm.beenActioned = false;
    vm.disableSubmit = false;
    vm.goBack = goBack;
    vm.sortBy = sortBy;
    var _selected;
    vm.sortFilter = {"projectId":"none","person":"none","projectName":"none","deliveryDate":"none"};
    vm.viewRequest = viewRequest;
    vm.updateMaterialRequest = updateMaterialRequest;
    vm.change = change;
    vm.updateId = function (value) {
      if (arguments.length) {
        _selected = value;
        vm.materialRequest.projectId = "";
        vm.materialRequest.projectName = "";
      } else {
        if (angular.isDefined(_selected) && angular.isDefined(_selected.id)) {
          vm.materialRequest.projectId = _selected.id;
          vm.materialRequest.projectName = _selected.name;
        } 
        return _selected;
      }
    };
    vm.reasons = {"MD":" Missing from Delivery","AR":"Ancillaries Request","VO":"Variation Order","SR":"Site Request (Additional to budget)"};

    init();

    ////////////

    function init() {
      
      vm.materialRequest.person = schema.status.userName;
      vm.materialRequest.email = schema.status.userEmail;
      
      projectsService.getProjects()
        .$loaded()
        .then(function (projects) {
          var proArr = [];
          angular.forEach(projects, function (project) {
            proArr.push({
              "id":project.id,
              "name":project.name
            });
          });
          vm.projects = proArr;
        });
      if ($routeParams.requestKey) {
        materialrequestsService.getMaterialRequest($routeParams.requestKey)
          .$loaded()
          .then(function (materialrequest){
            vm.materialRequest = materialrequest;
          });
      }else{
        materialrequestsService.getMaterialRequests()
                .$loaded()
                .then(function (materialRequest){
                  vm.materialRequests = [materialRequest.reverse()];
                    
                });

      }
    }
    function sortBy(filter,key){

      if (filter === "person" || filter === "projectId" || filter === "projectName"|| filter === "deliveryDate" ) {
        if (vm.sortFilter[key] === filter) {
          vm.sortFilter[key] = "-" + filter;
        } else {
          vm.sortFilter[key] = filter;
        }
      } else {
        vm.sortFilter[key] = filter;
      }

    }
    function change() {
      vm.counteroo++;
      if (vm.counteroo > 3) {
        vm.counteroo = 0;
        vm.sort = "";
      }
    }
    function viewRequest(text){
      $confirm({
        text: text,
        cancel: "Ok"
      }, {
        templateUrl: require("../core/info-modal.html")
      });
    }
    
    function updateMaterialRequest(isValid){
      vm.disableSubmit = true;
      if(isValid){
        materialrequestsService.updateMaterialRequest(vm.materialRequest)
          .then(function () {
            vm.disableSubmit = false;
            vm.materialRequest = {};
            $location.url("/materialrequests");
          });
      }
    }
    
    function goBack(){
      $location.url("/materialrequests");
    }
  }
})();
