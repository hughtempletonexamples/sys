(function () {
  "use strict";

  require("angular-ui-grid/ui-grid.min.css");
  require("./stylesheets/override.scss");

  require("angular");
  require("angular-route");
  require("angular-animate");
  require("angular-touch");
  require("angular-messages");
  require("angular-ui-bootstrap");
  require("angular-ui-grid/ui-grid");
  require("angular-confirm");
  require("angular-nvd3");

  require("./module");
  require("./config.route");

  require("./core");
  require("./login");
  require("./home");
  require("./projects");
  require("./manage");
  require("./productionperformance");
  require("./admin");
  require("./materialrequests");
  require("./materialanalysis");
  require("./measures");
  require("./dashboard");
})();
