(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .factory("materialByJobsheetService", materialByJobsheetService);

  /**
   * @ngInject
   */
  function materialByJobsheetService($log, $q, referenceDataService, dateUtil, schema) {


    var jobsheetCuttingList = {
      Day: {},
      Week: {},
      Month: {},
      DayTotal: {},
      WeekTotal: {},
      MonthTotal: {},
      CuttingTypes: {},
      JobSheetData: {},
      defaultCuttingTypesPrices: {},
      cuttingTypesPrices: {},
      $destroy: destroyMaterialData
    };

    var watches = [];

    var cuttingTypesList = referenceDataService.getCuttingTypes();

    var service = {
      init: init,
      jobsheetCuttingList: jobsheetCuttingList
    };


    return service;

    ////////////

    function init() {

      destroyMaterialData();

      var deferred = $q.defer();

      cuttingTypesList
        .$loaded()
        .then(function () {
          var deferredList = [];
          clearJobsheetCuttingList();
          var cuttingTypesPricesRef = schema.getRoot().child("defaultCuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            jobsheetCuttingList.defaultCuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          var deferredList = [];
          var cuttingTypesPricesRef = schema.getRoot().child("cuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            jobsheetCuttingList.cuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          return getJobsheets().$loaded();
        })
        .then(getCuttingListArray)
        .then(function () {
          return deferred.resolve(jobsheetCuttingList);
        });
      return deferred.promise;

    }

    function getCuttingListArray(jobsheetList) {

      var promises = [];

      var watch = jobsheetList.$watch(function () {
        init();
      });
      watches.push(watch);

      angular.forEach(jobsheetList, function (jobsheet, jobsheetKey) {

        if (angular.isUndefined(jobsheetCuttingList.JobSheetData[jobsheet.jobsheet])) {
          jobsheetCuttingList.JobSheetData[jobsheet.jobsheet] = {
            "totalRequired": 0,
            "totalActual": 0,
            "hasOptimised": false
          };
        }

        if (angular.isDefined(jobsheet.completed)) {

          var formatDate = dateUtil.toIsoDate(jobsheet.completed, "dd-mm-yyyy");
          var mondayOfWeek = dateUtil.weekStart(formatDate);
          var formatMonth = dateUtil.dateMonth(mondayOfWeek);

          if (angular.isUndefined(jobsheetCuttingList.Day[formatDate])) {
            jobsheetCuttingList.Day[formatDate] = {};
          }
          if (angular.isUndefined(jobsheetCuttingList.Week[mondayOfWeek])) {
            jobsheetCuttingList.Week[mondayOfWeek] = {};
          }
          if (angular.isUndefined(jobsheetCuttingList.Month[formatMonth])) {
            jobsheetCuttingList.Month[formatMonth] = {};
          }

          if (angular.isUndefined(jobsheetCuttingList.DayTotal[formatDate])) {
            jobsheetCuttingList.DayTotal[formatDate] = {
              "totalRequired": 0,
              "requiredAmount": 0,
              "totalActual": 0,
              "actualAmount": 0
            };
          }
          if (angular.isUndefined(jobsheetCuttingList.WeekTotal[mondayOfWeek])) {
            jobsheetCuttingList.WeekTotal[mondayOfWeek] = {
              "totalRequired": 0,
              "requiredAmount": 0,
              "totalActual": 0,
              "actualAmount": 0
            };
          }
          if (angular.isUndefined(jobsheetCuttingList.MonthTotal[formatMonth])) {
            jobsheetCuttingList.MonthTotal[formatMonth] = {
              "totalRequired": 0,
              "requiredAmount": 0,
              "totalActual": 0,
              "actualAmount": 0
            };
          }

          if (angular.isUndefined(jobsheetCuttingList.CuttingTypes[mondayOfWeek])) {
            jobsheetCuttingList.CuttingTypes[mondayOfWeek] = [];
          }

        }

        angular.forEach(jobsheet.cuttingList, function (cutting, cuttingKey) {
          promises.push(
            getCuttingListByRef(cuttingKey)
            .$loaded()
            .then(function (cuttingListData) {

              var watch = cuttingListData.$watch(function () {
                init();
              });
              watches.push(watch);

              if (cuttingListData.cuttingDetails.actual > 0) {
                jobsheetCuttingList.JobSheetData[jobsheet.jobsheet]["hasOptimized"] = true;
              }

              jobsheetCuttingList.JobSheetData[jobsheet.jobsheet]["totalRequired"] += cuttingListData.cuttingDetails.required;
              jobsheetCuttingList.JobSheetData[jobsheet.jobsheet]["totalActual"] += cuttingListData.cuttingDetails.actual;

              if (angular.isDefined(jobsheet.completed)) {

                //create list of cutting types
                if (jobsheetCuttingList.CuttingTypes[mondayOfWeek].indexOf(cuttingListData.type) === -1) {
                  jobsheetCuttingList.CuttingTypes[mondayOfWeek].push(cuttingListData.type);
                }

                var amount = getAmount(cuttingListData.type, formatDate);

                jobsheetCuttingList.DayTotal[formatDate]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.DayTotal[formatDate]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.DayTotal[formatDate]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.DayTotal[formatDate]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

                jobsheetCuttingList.WeekTotal[mondayOfWeek]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.WeekTotal[mondayOfWeek]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.WeekTotal[mondayOfWeek]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.WeekTotal[mondayOfWeek]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

                jobsheetCuttingList.MonthTotal[formatMonth]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.MonthTotal[formatMonth]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.MonthTotal[formatMonth]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.MonthTotal[formatMonth]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

                //build display array 
                if (angular.isUndefined(jobsheetCuttingList.Day[formatDate][cuttingListData.type])) {
                  jobsheetCuttingList.Day[formatDate][cuttingListData.type] = {
                    "totalRequired": 0,
                    "totalActual": 0,
                    "requiredAmount": 0,
                    "actualAmount": 0,
                    "jobsheets": []
                  };
                }
                jobsheetCuttingList.Day[formatDate][cuttingListData.type]["jobsheets"].push(jobsheet.jobsheet);
                jobsheetCuttingList.Day[formatDate][cuttingListData.type]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.Day[formatDate][cuttingListData.type]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.Day[formatDate][cuttingListData.type]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.Day[formatDate][cuttingListData.type]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

                //build display array 
                if (angular.isUndefined(jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type])) {
                  jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type] = {
                    "totalRequired": 0,
                    "totalActual": 0,
                    "requiredAmount": 0,
                    "actualAmount": 0,
                    "jobsheets": []
                  };
                }
                jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type]["jobsheets"].push(jobsheet.jobsheet);
                jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.Week[mondayOfWeek][cuttingListData.type]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

                //build display array 
                if (angular.isUndefined(jobsheetCuttingList.Month[formatMonth][cuttingListData.type])) {
                  jobsheetCuttingList.Month[formatMonth][cuttingListData.type] = {
                    "totalRequired": 0,
                    "totalActual": 0,
                    "requiredAmount": 0,
                    "actualAmount": 0,
                    "jobsheets": []
                  };
                }
                jobsheetCuttingList.Month[formatMonth][cuttingListData.type]["jobsheets"].push(jobsheet.jobsheet);
                jobsheetCuttingList.Month[formatMonth][cuttingListData.type]["totalRequired"] += cuttingListData.cuttingDetails.required;
                jobsheetCuttingList.Month[formatMonth][cuttingListData.type]["totalActual"] += cuttingListData.cuttingDetails.actual;
                jobsheetCuttingList.Month[formatMonth][cuttingListData.type]["requiredAmount"] += cuttingListData.cuttingDetails.required * amount;
                jobsheetCuttingList.Month[formatMonth][cuttingListData.type]["actualAmount"] += cuttingListData.cuttingDetails.actual * amount;

              }
            })
            );
        });
      });
      return $q.all(promises);

      function getAmount(type, formatDate){
        var amount = 0;
        var defaultAmounts = 0;
        
        if(angular.isDefined(jobsheetCuttingList.cuttingTypesPrices) && angular.isDefined(jobsheetCuttingList.defaultCuttingTypesPrices)){
          
          if(angular.isDefined(jobsheetCuttingList.cuttingTypesPrices[type]) && angular.isDefined(jobsheetCuttingList.cuttingTypesPrices[type][formatDate])){
            amount = jobsheetCuttingList.cuttingTypesPrices[type][formatDate];
          }
          if(angular.isDefined(jobsheetCuttingList.defaultCuttingTypesPrices[type])){
            defaultAmounts = jobsheetCuttingList.defaultCuttingTypesPrices[type];
          }

          if (!amount && defaultAmounts) {
            var previousDate = "2015-01-01";
            angular.forEach(defaultAmounts, function (value, date) {
              if (formatDate >= previousDate && formatDate < date) {
                amount = value;
              }
              previousDate = date;
            });
          }
          
        }
        return amount;
      }

    }

    function clearJobsheetCuttingList() {
      jobsheetCuttingList.Day = {};
      jobsheetCuttingList.Week = {};
      jobsheetCuttingList.Month = {};
      jobsheetCuttingList.DayTotal = {};
      jobsheetCuttingList.WeekTotal = {};
      jobsheetCuttingList.MonthTotal = {};
      jobsheetCuttingList.CuttingTypes = {};
      jobsheetCuttingList.JobSheetData = {};
      jobsheetCuttingList.defaultCuttingTypesPrices = {};
      jobsheetCuttingList.cuttingTypesPrices = {};
    }

    function destroyMaterialData() {
      angular.forEach(watches, function (watch) {
        watch();
      });

      watches = [];
    }

    function getCuttingListByRef(ref) {
      var cuttingList = schema.getRoot().child("cuttingList").child(ref);
      return schema.getObject(cuttingList);
    }

    function getJobsheets() {
      var jobsheets = schema.getRoot().child("jobsheets");
      return schema.getArray(jobsheets);
    }


  }

})();
