(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .filter("countTrue", filter);

  function filter() {

    return countTrueFilter;

    function countTrueFilter(input, keyName) {
      var counter = 0;
      if (angular.isDefined(input)) {
        if (!angular.isObject(input)) {
          throw Error("Usage of non-objects with keylength filter!!");
        }
        angular.forEach(input, function (item) {
          if (item[keyName]) {
            counter++;
          }
        });
      }
      return counter;
    }
  }

})();
