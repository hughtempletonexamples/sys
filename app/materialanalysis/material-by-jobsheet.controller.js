(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .controller("MaterialByJobsheetController", MaterialByJobsheetController);

  /**
   * @ngInject
   */
  function MaterialByJobsheetController($log, $scope, materialByJobsheetService, dateUtil) {
    
    var vm = this;

    var daysInWeek = 7;
    var todayDate = new Date();
    var formatDate = dateUtil.toIsoDate(todayDate);
    var formatMonday = dateUtil.weekStart(formatDate);
    var formatMonth = dateUtil.dateMonth(formatMonday);
    
    vm.selectedDate = formatDate;
    vm.selectedMonday = formatMonday;
    vm.selectedMonth = formatMonth;
    
    vm.toggleMainFlag = true;
    vm.toggleMainFlag2 = false;
    vm.toggleCostFlag = false;
    vm.backButton = false;
    vm.forwardButton = false;

    vm.toggleCost = toggleCost;
    vm.toggleMain = toggleMain;
    vm.toggleMain2 = toggleMain2;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.isNumber = isNumber;
    vm.weekRange = weekRange;
    
    vm.dynamicPopover = {
      content: "Hello, World!",
      templateUrl: "myPopoverTemplate.html",
      title: "Jobsheets: "
    };
    
    $scope.$on("$destroy", destroyJobsheetCuttingData);
    
    init();
    
    function init() {
      
      vm.backButton = true;
      vm.forwardButton = true;

      materialByJobsheetService.init()
        .then(function () {
          vm.backButton = false;
          vm.forwardButton = false;
          vm.jobsheetCuttingList = materialByJobsheetService.jobsheetCuttingList;
        });
    }

    function weekRange(mondayDate){
      var week = [];
      for (var i = 0; i<=(daysInWeek-1);i++){
        var day = dateUtil.plusDays(mondayDate, i);
        week.push(day);
      }
      return week;
    }
    
    function toggleMain(){
      vm.toggleMainFlag = !vm.toggleMainFlag;
    }
    
    function toggleMain2(){
      vm.toggleMainFlag2 = !vm.toggleMainFlag2;
    }
    
    function toggleCost(){
      vm.toggleCostFlag = !vm.toggleCostFlag;
    }
    
    function isNumber(value){
      return angular.isNumber(value);
    }

    function backWeek(){
      vm.backButton = false;
      
      var newDate = dateUtil.minusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function forwardWeek(){
      vm.forwardButton = false;
      
      var newDate = dateUtil.plusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function destroyJobsheetCuttingData() {
      if (vm.jobsheetCuttingList && vm.jobsheetCuttingList.$destroy) {
        vm.jobsheetCuttingList.$destroy();
        delete vm.jobsheetCuttingList;
      }
    }

  }
})();
