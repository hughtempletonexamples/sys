(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .factory("cuttingTypePricingService", cuttingTypePricingService);

  /**
   * @ngInject
   */
  function cuttingTypePricingService($log, $q, referenceDataService, dateUtil, schema) {
    
    var cuttingTypesList = referenceDataService.getCuttingTypes();

    var cuttingTypesPricesList = {
      defaultCuttingTypesPrices:{},
      cuttingTypesPrices:{},
      cuttingTypes:{},
      stockCount:{},
      $destroy: destroyMaterialData
    };

    var watches = [];

    var service = {
      init: init,
      updateDefaultPrice: updateDefaultPrice,
      updatePrice: updatePrice,
      cuttingTypesPricesList: cuttingTypesPricesList,
      updateStock: updateStock
    };


    return service;

    ////////////
    
    function init() {
      destroyMaterialData();
      var deferred = $q.defer();
      cuttingTypesList
        .$loaded()
        .then(function () {
          var deferredList = [];
          clearJobsheetCuttingList();
          cuttingTypesPricesList.cuttingTypes = cuttingTypesList;
          var cuttingTypesPricesRef = schema.getRoot().child("defaultCuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            cuttingTypesPricesList.defaultCuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          var deferredList = [];
          var cuttingTypesPricesRef = schema.getRoot().child("cuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            cuttingTypesPricesList.cuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          var deferredList = [];
          var stockCountRef = schema.getRoot().child("stockCount");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(stockCountRef.child(cuttingType.$id));
            cuttingTypesPricesList.stockCount[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          return deferred.resolve(cuttingTypesPricesList);
        });
      return deferred.promise;
    }
    
    function updateDefaultPrice(cuttingType, date, value) {
      var cuttingTypePrice = cuttingTypesPricesList.defaultCuttingTypesPrices[cuttingType];
      cuttingTypePrice[date] = value;
      return cuttingTypePrice.$save();
    }
    
    function updatePrice(cuttingType, date, value) {
      var cuttingTypePrice = cuttingTypesPricesList.cuttingTypesPrices[cuttingType];
      cuttingTypePrice[date] = value;
      return cuttingTypePrice.$save();
    }
    
    function updateStock(stockType, date, value) {
      var cuttingTypePrice = cuttingTypesPricesList.stockCount[stockType];
      cuttingTypePrice[date] = value;
      return cuttingTypePrice.$save();
    }
    
    function clearJobsheetCuttingList() {
      cuttingTypesPricesList.defaultCuttingTypesPrices = {};
      cuttingTypesPricesList.cuttingTypesPrices = {};
      cuttingTypesPricesList.cuttingTypes = {};
      cuttingTypesPricesList.stockCount = {};
    }
    
    function destroyMaterialData() {
      angular.forEach(watches, function (watch) {
        watch();
      });

      watches = [];
    }

  }

})();
