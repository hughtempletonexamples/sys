(function () {
  "use strict";

  angular
    .module("innView.admin")
    .factory("datumCubicAverageService", datumCubicAverageService);

  /**
   * @ngInject
   */
  function datumCubicAverageService($log, $q, referenceDataService, projectsService, panelsService, loadTimbersService) {
    
    var productTypes = {};
    var productionLines = {};
    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;

    var service = {
      calculateUsageByDatum: calculateUsageByDatum
    };
    
    init();

    return service;

    ////////////

    function init() {
      productTypes = referenceDataService.getProductTypes();
      productionLines = referenceDataService.getProductionLines();
    }

    function calculateUsageByDatum() {

      var usageByDatum = {};
      var panelTotalVolume = {};

      var deferred = $q.defer();

      var projects = projectsService.getProjects();
      projects.$loaded()
        .then(requiredUsageForDatum)
        .then(panelAreaForDatum)
        .then(panelMeterAverageForDatum)
        .then(function () {
          deferred.resolve(usageByDatum);
        });

      return deferred.promise;

      function requiredUsageForDatum(projects) {
        var promises = [];
        angular.forEach(projects, function (project) {
          if(angular.isUndefined(usageByDatum[project.datumType])){
            usageByDatum[project.datumType] = {
              volume:0,
              panelAreaTotal:0,
              meters:0,
              average:0,
              panels:[],
              productionTypes:{}
            };
          }
          angular.forEach(productionLines, function (productionLine) {
            var productionLineKey = productionLine.$id;
            if (angular.isUndefined(usageByDatum[project.datumType].productionTypes[productionLineKey])) {
              usageByDatum[project.datumType].productionTypes[productionLineKey] = {
                volume: 0,
                panelAreaTotal: 0,
                meters: 0,
                average: 0,
                panels: []
              };
            }
          });
          promises.push(
            loadTimbersService.getRequiredByProject(project.$id)
            .$loaded()
            .then(function (panelUsage) {
              if (panelUsage.length > 0) {
                angular.forEach(panelUsage, function (usage) {
                  if (angular.isDefined(usage.id)) {
                    if (angular.isUndefined(panelTotalVolume[usage.id])) {
                      panelTotalVolume[usage.id] = {
                        "volume":0,
                        "datumType":project.datumType
                      };
                    }
                    var volume = (usage.usageDetails.height * usage.usageDetails.width * usage.usageDetails.lenghtInMeters); 
                    panelTotalVolume[usage.id].volume += volume;
                  }

                });
              }
            })
            );
        });
        
        return $q.all(promises);
        
      }
      
      function panelAreaForDatum() {

        var promises = [];

        angular.forEach(panelTotalVolume, function (panData, panelKey) {
          promises.push(
            panelsService.getPanelById(panelKey)
            .$loaded()
            .then(function (panels) {
              var panel = panels[0];
              if (angular.isDefined(panel) && angular.isDefined(panel.qa) && angular.isDefined(panel.qa["completed"])) {
                var productTypeProductionLine = buildMap(productTypes, "productionLine");
                var type = panel.type;
                var panelType = null;
                var idParts = PANEL_ID_REGEX.exec(panel.id);
                if (idParts) {
                  panelType = idParts[5];
                }
                type = logicHSIP(type, panelType, panel.dimensions.height, panel.dimensions.length);
        
                var line = productTypeProductionLine[type];

                if (usageByDatum[panData.datumType]["panels"].indexOf(panel.id) === -1) {
                  usageByDatum[panData.datumType]["panels"].push(panel.id);
                }
                
                usageByDatum[panData.datumType].panelAreaTotal += panel.dimensions.area;
                usageByDatum[panData.datumType].volume += panData.volume;

                if (usageByDatum[panData.datumType].productionTypes[line]["panels"].indexOf(panel.id) === -1) {
                  usageByDatum[panData.datumType].productionTypes[line]["panels"].push(panel.id);
                }
                
                usageByDatum[panData.datumType].productionTypes[line].volume += panelTotalVolume[panel.id].volume;
                usageByDatum[panData.datumType].productionTypes[line].panelAreaTotal += panel.dimensions.area;
              }
            })
            );
        });
        
        return $q.all(promises);

      }
      
      function logicHSIP(foundProductType, panelType, height, length) {

        var isHSIP = "H";
        var isIFAST = "F";

        if (foundProductType === "Ext" && foundProductType !== "ExtH" && height != 0 && length != 0) {

          if (angular.isDefined(panelType) && angular.isDefined(height) && angular.isDefined(length)) {

            if (panelType === "H" || (height <= 600 && length <= 2650) || (length <= 600 && height <= 2650)) {
              foundProductType = foundProductType + isHSIP;
            }else if (panelType === "F") {
              foundProductType = foundProductType + isIFAST;
            }

          }
        }

        return foundProductType;
      }
      
      function panelMeterAverageForDatum() {
        angular.forEach(usageByDatum, function (datum) {
          if (datum.volume > 0 && datum.panelAreaTotal > 0) {
            datum.meters = datum.volume / datum.panelAreaTotal;
            datum.average = datum.meters / datum.panels.length;
          }
          angular.forEach(datum.productionTypes, function (data) {
            if (data.volume > 0 && data.panelAreaTotal > 0) {
              data.meters = data.volume / data.panelAreaTotal;
              data.average = data.meters / data.panels.length;
            }
          });
        });
      }

    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }

  }
})();
