(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .controller("LoadTimbersController", LoadTimbersController);

  /**
   * @ngInject
   */
  function LoadTimbersController($window, loadTimbersService) {
    var vm = this;

    var filePanels = [];
    var fileErrors = [];

    var updateSummary = {};
    
    vm.saveInProgress = 0;
    
    vm.upload = upload;
    vm.removeFile = removeFile;
    vm.reset = reset;
    vm.save = save;

    init();

    ////////////

    function init() {
      vm.files = [];
      vm.updateSummary = null;
      filePanels = [];
      fileErrors = [];
      vm.saveInProgress = 0;

      updateLists();
    }

    function upload(files) {
      angular.forEach(files, parseFile);
    }

    function removeFile(index) {
      vm.files.splice(index, 1);
      filePanels.splice(index, 1);
      fileErrors.splice(index, 1);

      updateLists();
    }

    function reset() {
      init();
    }

    function save() {
      vm.saveInProgress = 1;
      updateSummary = {};
      if (vm.panels.length > 0 && vm.errors.length === 0) {
        loadTimbersService.updatePanelsUsage(vm.panels)
          .then(function (result) {
            updateSummary = result;
            return loadTimbersService.createCuttingList(vm.jobsheets);
          })
          .then(function () {
            vm.updateSummary = updateSummary;
          })
          .catch(function (error) {
            $window.alert(error);
          });
      }
    }
    
    function parseFile(file) {
      var index = vm.files.push(file) -1;
      loadTimbersService.loadTimberSchedule(file, false)
        .then(function (result) {
          filePanels[index] = result.panels || [];
          fileErrors[index] = result.errors || [];
          updateLists();
        })
        .catch(function (error) {
          filePanels[index] = [];
          fileErrors[index] = [ "Failed to parse '" + file.name + "'"];
          updateLists();
        });

    }

    function updateLists() {
      vm.panels = [].concat.apply([], filePanels);
      vm.errors = [].concat.apply([], fileErrors);
      vm.jobsheets = jobsheetCount(vm.panels);
    }
    
    function jobsheetCount(panels){
      var keys = [];
      var counter = 0;
      angular.forEach(panels, function (item) {
        if (angular.isDefined(item) && angular.isDefined(item.jobsheet)) {
          if(keys.indexOf(item.jobsheet) === -1){
            counter++;
            keys.push(item.jobsheet);
          }
        }
      });
      vm.jobsheetCount = counter;
      return keys;
    }


  }
})();
