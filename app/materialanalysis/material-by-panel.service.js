(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .factory("materialByPanelService", materialByPanelService);
  /**
   * @ngInject
   */
  function materialByPanelService($log, $q, dateUtil, schema, panelsService, loadTimbersService, referenceDataService) {

    var panelCuttingList = {
      Day:{},
      Week:{},
      Month:{},
      DayTotal:{},
      WeekTotal:{},
      MonthTotal:{},
      PanData:{},
      CuttingTypes:{},
      defaultCuttingTypesPrices:{},
      cuttingTypesPrices:{},
      stockCount:{},
      $destroy: destroyMaterialData
    };
    
    var watches = [];
    
    var cuttingTypesList = referenceDataService.getCuttingTypes();

    var service = {
      init: init,
      panelCuttingList: panelCuttingList
    };


    return service;

    ////////////
    
    function init(formatDate){
      
      destroyMaterialData();

      var deferred = $q.defer();

      var date = new Date(formatDate);
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      firstDay.setHours(0,0,0,0);
      lastDay.setHours(23,59,59,999); 

      cuttingTypesList
      .$loaded()
        .then(function () {
          var deferredList = [];
          clearJobsheetCuttingList();
          var cuttingTypesPricesRef = schema.getRoot().child("defaultCuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            panelCuttingList.defaultCuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          var deferredList = [];
          var cuttingTypesPricesRef = schema.getRoot().child("cuttingTypesPrice");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(cuttingTypesPricesRef.child(cuttingType.$id));
            panelCuttingList.cuttingTypesPrices[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          var deferredList = [];
          var stockCountRef = schema.getRoot().child("stockCount");
          angular.forEach(cuttingTypesList, function (cuttingType) {
            var cuttingTypeHolder = schema.getObject(stockCountRef.child(cuttingType.$id));
            panelCuttingList.stockCount[cuttingType.$id] = cuttingTypeHolder;

            deferredList.push(cuttingTypeHolder.$loaded());
          });

          return $q.all(deferredList);
        })
        .then(function () {
          return panelsService.panelsByCompleted(firstDay.getTime(), lastDay.getTime()).$loaded();
        })
        .then(getCuttingListArray)
        .then(function (){
          return deferred.resolve(panelCuttingList);
        });
      return deferred.promise;
      
    }

    function getCuttingListArray(panelList) {

      var promises = [];

      angular.forEach(panelList, function (panel, panelKey) {
        
        if (angular.isDefined(panel.qa.completed)) {

          var formatDate = dateUtil.toIsoDate(panel.qa.completed, "dd-mm-yyyy");
          var mondayOfWeek = dateUtil.weekStart(formatDate);
          var formatMonth = dateUtil.dateMonth(mondayOfWeek);

          if (angular.isUndefined(panelCuttingList.PanData[panel.id])) {
            panelCuttingList.PanData[panel.id] = {
              "hasRequired": false,
              "totalForPanel": 0,
              "dateComplete": formatDate
            };
          } 
          if (angular.isUndefined(panelCuttingList.Day[formatDate])) {
            panelCuttingList.Day[formatDate] = {};
          }
          if (angular.isUndefined(panelCuttingList.Week[mondayOfWeek])) {
            panelCuttingList.Week[mondayOfWeek] = {};
          }
          if (angular.isUndefined(panelCuttingList.Month[formatMonth])) {
            panelCuttingList.Month[formatMonth] = {};
          }

          if (angular.isUndefined(panelCuttingList.DayTotal[formatDate])) {
            panelCuttingList.DayTotal[formatDate] = {
              "totalRequired": 0,
              "totalAmount": 0
            };
          }
          if (angular.isUndefined(panelCuttingList.WeekTotal[mondayOfWeek])) {
            panelCuttingList.WeekTotal[mondayOfWeek] = {
              "totalRequired": 0,
              "totalAmount": 0
            };
          }
          if (angular.isUndefined(panelCuttingList.MonthTotal[formatMonth])) {
            panelCuttingList.MonthTotal[formatMonth] = {
              "totalRequired": 0,
              "totalAmount": 0
            };
          }

          if (angular.isUndefined(panelCuttingList.CuttingTypes[mondayOfWeek])) {
            panelCuttingList.CuttingTypes[mondayOfWeek] = [];
          }
        
        }

      });
      
      angular.forEach(panelList, function (panel, panelKey) {
        
        if (angular.isDefined(panel.qa.completed)) {

          var formatDate = dateUtil.toIsoDate(panel.qa.completed, "dd-mm-yyyy");
          var mondayOfWeek = dateUtil.weekStart(formatDate);
          var formatMonth = dateUtil.dateMonth(mondayOfWeek);

          promises.push(
            loadTimbersService.getRequiredByPanel(panel.$id)
            .$loaded()
            .then(function (predictedPanelData) {

              angular.forEach(predictedPanelData, function (predictedPanel) {

                if (angular.isDefined(predictedPanel)) {

                  //create list of cutting types
                  var type = predictedPanel.usageDetails.width + " x " + predictedPanel.usageDetails.height;
                  if (panelCuttingList.CuttingTypes[mondayOfWeek].indexOf(type) === -1) {
                    panelCuttingList.CuttingTypes[mondayOfWeek].push(type);
                  }

                  panelCuttingList.PanData[panel.id]["hasRequired"] = true;
                  panelCuttingList.PanData[panel.id]["totalForPanel"] = predictedPanel.usageDetails.lenghtInMeters;

                  var amount = getAmount(type, formatDate);

                  panelCuttingList.DayTotal[formatDate]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.DayTotal[formatDate]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;
                  panelCuttingList.WeekTotal[mondayOfWeek]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.WeekTotal[mondayOfWeek]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;
                  panelCuttingList.MonthTotal[formatMonth]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.MonthTotal[formatMonth]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;

                  if (angular.isUndefined(panelCuttingList.Day[formatDate][type])) {
                    panelCuttingList.Day[formatDate][type] = {
                      "totalRequired": 0,
                      "totalAmount": 0,
                      "panels": []
                    };
                  }

                  panelCuttingList.Day[formatDate][type]["panels"].push(panel.id);
                  panelCuttingList.Day[formatDate][type]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.Day[formatDate][type]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;

                  if (angular.isUndefined(panelCuttingList.Week[mondayOfWeek][type])) {
                    panelCuttingList.Week[mondayOfWeek][type] = {
                      "totalRequired": 0,
                      "totalAmount": 0,
                      "panels": []
                    };
                  }

                  panelCuttingList.Week[mondayOfWeek][type]["panels"].push(panel.id);
                  panelCuttingList.Week[mondayOfWeek][type]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.Week[mondayOfWeek][type]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;

                  if (angular.isUndefined(panelCuttingList.Month[formatMonth][type])) {
                    panelCuttingList.Month[formatMonth][type] = {
                      "totalRequired": 0,
                      "totalAmount": 0,
                      "panels": []
                    };
                  }

                  panelCuttingList.Month[formatMonth][type]["panels"].push(panel.id);
                  panelCuttingList.Month[formatMonth][type]["totalRequired"] += predictedPanel.usageDetails.lenghtInMeters;
                  panelCuttingList.Month[formatMonth][type]["totalAmount"] += predictedPanel.usageDetails.lenghtInMeters * amount;
                }
              });
            })
            );
        }
      });

      return $q.all(promises);
      
      function getAmount(type, formatDate){
        var amount = 0;
        var defaultAmounts = 0;
        
        if(angular.isDefined(panelCuttingList.cuttingTypesPrices) && angular.isDefined(panelCuttingList.defaultCuttingTypesPrices)){
          
          if(angular.isDefined(panelCuttingList.cuttingTypesPrices[type]) && angular.isDefined(panelCuttingList.cuttingTypesPrices[type][formatDate])){
            amount = panelCuttingList.cuttingTypesPrices[type][formatDate];
          }
          if(angular.isDefined(panelCuttingList.defaultCuttingTypesPrices[type])){
            defaultAmounts = panelCuttingList.defaultCuttingTypesPrices[type];
          }

          if (!amount && defaultAmounts) {
            var previousDate = "2015-01-01";
            angular.forEach(defaultAmounts, function (value, date) {
              if (formatDate >= previousDate && formatDate < date) {
                amount = value;
              }
              previousDate = date;
            });
          }
          
        }
        return amount;
      }


    }
    
    function clearJobsheetCuttingList() {
      panelCuttingList.Day = {};
      panelCuttingList.Week = {};
      panelCuttingList.Month = {};
      panelCuttingList.DayTotal = {};
      panelCuttingList.WeekTotal = {};
      panelCuttingList.MonthTotal = {};
      panelCuttingList.PanData = {};
      panelCuttingList.CuttingTypes = {};
      panelCuttingList.defaultCuttingTypesPrices = {};
      panelCuttingList.cuttingTypesPrices = {};
      panelCuttingList.stockCount = {};
    }
    
    function destroyMaterialData() {
      angular.forEach(watches, function (watch) {
        watch();
      });

      watches = [];
    }
    
  }

})();
