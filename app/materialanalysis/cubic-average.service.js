(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .factory("cubicAverageService", cubicAverageService);
  /**
   * @ngInject
   */
  function cubicAverageService($log, $q, dateUtil, schema, panelsService, loadTimbersService, referenceDataService, functionsService) {

    var cubicAverageList = {
      DayType:{},
      WeekType:{},
      MonthType:{},
      cuttingTypesPrices:{},
      $destroy: destroyMaterialData
    };
    
    var watches = [];
    
    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
    
    var productTypes = referenceDataService.getProductTypes();

    var service = {
      init: init,
      cubicAverageList: cubicAverageList
    };


    return service;

    ////////////
    
    function init(formatDate){
      
      destroyMaterialData();

      var deferred = $q.defer();

      var date = new Date(formatDate);
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      firstDay.setHours(0,0,0,0);
      lastDay.setHours(23,59,59,999); 

      panelsService.panelsByCompleted(firstDay.getTime(), lastDay.getTime())
        .$loaded()
        .then(getCuttingListArray)
        .then(panelMeterAverage)
        .then(function (){
          return deferred.resolve(cubicAverageList);
        });
      return deferred.promise;
      
    }

    function getCuttingListArray(panelList) {

      var promises = [];

      clearJobsheetCuttingList();
      
      var productTypeProductionLine = buildMap(productTypes, "productionLine");

      angular.forEach(panelList, function (panel, panelKey) {

        var formatDate = dateUtil.toIsoDate(panel.qa.completed, "dd-mm-yyyy");
        var mondayOfWeek = dateUtil.weekStart(formatDate);
        var formatMonth = dateUtil.dateMonth(mondayOfWeek);

        if (angular.isUndefined(cubicAverageList.DayType[formatDate])) {
          cubicAverageList.DayType[formatDate] = {};
        }
        if (angular.isUndefined(cubicAverageList.WeekType[mondayOfWeek])) {
          cubicAverageList.WeekType[mondayOfWeek] = {};
        }
        if (angular.isUndefined(cubicAverageList.MonthType[formatMonth])) {
          cubicAverageList.MonthType[formatMonth] = {};
        }

        if (angular.isUndefined(cubicAverageList.ProductionTypes[mondayOfWeek])) {
          cubicAverageList.ProductionTypes[mondayOfWeek] = [];
        }

      });
      
      angular.forEach(panelList, function (panel, panelKey) {
        
        var formatDate = dateUtil.toIsoDate(panel.qa.completed, "dd-mm-yyyy");
        var mondayOfWeek = dateUtil.weekStart(formatDate);
        var formatMonth = dateUtil.dateMonth(mondayOfWeek);
        
        var type = panel.type;
        var panelType = null;
        var idParts = PANEL_ID_REGEX.exec(panel.id);
        if (idParts) {
          panelType = idParts[5];
        }
        type = functionsService.logicHSIP(type, panelType, panel.dimensions.height, panel.dimensions.length);

        var productionLineKey = productTypeProductionLine[type];

        promises.push(
          loadTimbersService.getRequiredByPanel(panel.$id)
          .$loaded()
          .then(function (predictedPanelData) {
            
            angular.forEach(predictedPanelData, function (predictedPanel) {

              if (angular.isDefined(predictedPanel)) {
                
                var volume = (predictedPanel.usageDetails.height * predictedPanel.usageDetails.width * predictedPanel.usageDetails.lenghtInMeters);

                //create list of cutting types
                if (cubicAverageList.ProductionTypes[mondayOfWeek].indexOf(productionLineKey) === -1) {
                  cubicAverageList.ProductionTypes[mondayOfWeek].push(productionLineKey);
                }
                
                setUpIncrement("DayType", formatDate, productionLineKey, volume, panel);
                setUpIncrement("WeekType", mondayOfWeek, productionLineKey, volume, panel);
                setUpIncrement("MonthType", formatMonth, productionLineKey, volume, panel);

              }
            });
            

          })
          );

      });
      
      function setUpIncrement(type, date, pKey, volume, panel){
        if (angular.isUndefined(cubicAverageList[type][date][pKey])) {
          cubicAverageList[type][date][pKey] = {
            "volume": 0,
            "panelAreaTotal": 0,
            "meters": 0,
            "average": 0,
            "panels": []
          };
        }
        
        
        cubicAverageList[type][date][pKey].volume += volume;

        if (cubicAverageList[type][date][pKey]["panels"].indexOf(panel.id) === -1) {
          cubicAverageList[type][date][pKey]["panels"].push(panel.id);        
          cubicAverageList[type][date][pKey].panelAreaTotal += panel.dimensions.area;
        }
      }
      

      return $q.all(promises);
  
    }
    
    function panelMeterAverage() {
      
      angular.forEach(cubicAverageList.DayType, function (dateData) {
        angular.forEach(dateData, function (pTypeData) {
          if (pTypeData.volume > 0 && pTypeData.panelAreaTotal > 0) {
            pTypeData.meters = pTypeData.volume / pTypeData.panelAreaTotal;
            pTypeData.average = pTypeData.meters / pTypeData.panels.length;
          }
        });
      });
      angular.forEach(cubicAverageList.WeekType, function (dateData) {
        angular.forEach(dateData, function (pTypeData) {
          if (pTypeData.volume > 0 && pTypeData.panelAreaTotal > 0) {
            pTypeData.meters = pTypeData.volume / pTypeData.panelAreaTotal;
            pTypeData.average = pTypeData.meters / pTypeData.panels.length;
          }
        });
      });
      angular.forEach(cubicAverageList.MonthType, function (dateData) {
        angular.forEach(dateData, function (pTypeData) {
          if (pTypeData.volume > 0 && pTypeData.panelAreaTotal > 0) {
            pTypeData.meters = pTypeData.volume / pTypeData.panelAreaTotal;
            pTypeData.average = pTypeData.meters / pTypeData.panels.length;
          }
        });
      });
    }
    
    function clearJobsheetCuttingList() {
      cubicAverageList.DayType = {};
      cubicAverageList.WeekType = {};
      cubicAverageList.MonthType = {};
      cubicAverageList.ProductionTypes = {};
    }
    
    function buildMap(items, property) {
      var map = {};
      angular.forEach(items, function (item) {
        map[item.$id] = item[property];
      });
      return map;
    }
    
    function destroyMaterialData() {
      angular.forEach(watches, function (watch) {
        watch();
      });

      watches = [];
    }
    
  }

})();
