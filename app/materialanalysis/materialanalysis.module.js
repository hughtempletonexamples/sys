(function () {
  "use strict";

  angular
    .module("innView.materialanalysis", [
      "ngRoute",
      "firebase",
      "floatThead",
      "innView.core",
      "innView.projects",
      "ngFileUpload"
    ]);

})();
