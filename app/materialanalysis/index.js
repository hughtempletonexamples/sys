(function () {
  "use strict";

  require("./materialanalysis.module");

  require("./load-timbers.controller");
  
  require("./load-timbers.service");
  
  require("./material-by-jobsheet.controller");
  
  require("./material-by-jobsheet.service");
  
  require("./material-by-panel.controller");
  
  require("./material-by-panel.service");
  
  require("./cutting-type-pricing.controller");
  
  require("./cutting-type-pricing.service");
  
  require("./cubic-average.controller");
  
  require("./cubic-average.service");
  
  require("./datum-cubic-average.controller");
  
  require("./datum-cubic-average.service");
  
  require("./count-true.filter");

  require("./config.route");

})();
