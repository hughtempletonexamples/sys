(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .controller("CuttingTypePricingController", CuttingTypePricingController);

  /**
   * @ngInject
   */
  function CuttingTypePricingController($log, $scope, cuttingTypePricingService, dateUtil) {
    
    var vm = this;

    var daysInWeek = 7;
    var todayDate = new Date();
    var formatDate = dateUtil.toIsoDate(todayDate);
    var formatMonday = dateUtil.weekStart(formatDate);
    var formatMonth = dateUtil.dateMonth(formatMonday);
    
    vm.selectedDate = formatDate;
    vm.selectedMonday = formatMonday;
    vm.selectedMonth = formatMonth;
    
    vm.stockMonth = formatMonth;
    vm.increaseDate = formatDate;
    
    vm.toggleMainFlag = false;
    vm.toggleMainFlag2 = true;
    vm.toggleMainFlag3 = true;
    
    vm.newPrices = {};
    vm.newPriceDate = {};

    vm.toggleMain = toggleMain;
    vm.toggleMain2 = toggleMain2;
    vm.toggleMain3 = toggleMain3;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.isNumber = isNumber;
    vm.weekRange = weekRange;
    vm.updateStock = updateStock;
    vm.updateDefaultPrice = updateDefaultPrice;
    vm.updatePrice = updatePrice;
    
    vm.dynamicPopover = {
      content: "Hello, World!",
      templateUrl: "myPopoverTemplate.html",
      title: "Jobsheets: "
    };
    
    $scope.$on("$destroy", destroyCuttingTypesPricesData);

    cuttingTypePricingService.init()
      .then(function (){
        vm.cuttingTypesPricesList = cuttingTypePricingService.cuttingTypesPricesList;
      });

    function weekRange(mondayDate){
      var week = [];
      for (var i = 0; i<=(daysInWeek-1);i++){
        var day = dateUtil.plusDays(mondayDate, i);
        week.push(day);
      }
      return week;
    }
    
    function userDate(date){
      return dateUtil.toUserDateNew(date);
    }
    
    function updateDefaultPrice(cuttingType, date, value){
      cuttingTypePricingService.updateDefaultPrice(cuttingType, date, value);
    }
    
    function updateStock(cuttingType, date, value){
      cuttingTypePricingService.updateStock(cuttingType, date, value);
    }
    
    function updatePrice(cuttingType, date, value){
      cuttingTypePricingService.updatePrice(cuttingType, date, value);
    }
    
    function toggleMain(){
      vm.toggleMainFlag = !vm.toggleMainFlag;
    }
    
    function toggleMain2(){
      vm.toggleMainFlag2 = !vm.toggleMainFlag2;
    }
    
    function toggleMain3(){
      vm.toggleMainFlag3 = !vm.toggleMainFlag3;
    }
    
    function isNumber(value){
      return angular.isNumber(value);
    }

    function backWeek(){
      vm.backButton = false;
      
      var newDate = dateUtil.minusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function forwardWeek(){
      vm.forwardButton = false;
      
      var newDate = dateUtil.plusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function destroyCuttingTypesPricesData() {
      if (vm.cuttingTypesPricesList && vm.cuttingTypesPricesList.$destroy) {
        vm.cuttingTypesPricesList.$destroy();
        delete vm.cuttingTypesPricesList;
      }
    }

  }
})();
