(function () {
  "use strict";

  angular
    .module("innView.admin")
    .controller("DatumCubicAverageController", DatumCubicAverageController);

  /**
   * @ngInject
   */
  function DatumCubicAverageController($log, $scope, datumCubicAverageService, referenceDataService) {
    
    var vm = this;
    
    vm.datums = referenceDataService.getDatumTypes();
    
    $scope.$on("$destroy", destroyDatumUsageData);

    init();

    ////////////

    function init() {
      referenceDataService.getDatumTypes()
        .$loaded()
        .then(function (datums){
          vm.datums = datums;
          return datumCubicAverageService.calculateUsageByDatum();
        })
        .then(function (usage){
          vm.usage = usage;
        });
    }
    
    function destroyDatumUsageData() {
      if (vm.datums) {
        delete vm.datums;
      }
      if (vm.usage) {
        delete vm.usage;
      }
    }
    
  }
})();
