(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .controller("MaterialByPanelController", MaterialByPanelController);

  /**
   * @ngInject
   */
  function MaterialByPanelController($log, $scope, $timeout, materialByPanelService, dateUtil) {
    
    var vm = this;

    var daysInWeek = 7;
    var todayDate = new Date();
    var formatDate = dateUtil.toIsoDate(todayDate);
    var formatMonday = dateUtil.weekStart(formatDate);
    var formatMonth = dateUtil.dateMonth(formatMonday);
    
    vm.selectedDate = formatDate;
    vm.selectedMonday = formatMonday;
    vm.selectedMonth = formatMonth;
    
    vm.toggleMainFlag = true;
    vm.toggleMainFlag2 = true;
    vm.toggleCostFlag = false;
    vm.backButton = false;
    vm.forwardButton = false;
    
    vm.loader = false;

    vm.toggleMain = toggleMain;
    vm.toggleMain2 = toggleMain2;
    vm.toggleCost = toggleCost;
    vm.backWeek = backWeek;
    vm.forwardWeek = forwardWeek;
    vm.isNumber = isNumber;
    vm.weekRange = weekRange;
    
    vm.dynamicPopover = {
      content: "Hello, World!",
      templateUrl: "myPopoverPanels.html",
      title: "Panels: "
    };
    
    $scope.$on("$destroy", destroyPanelCuttingData);
    
    init(vm.selectedMonday);
    
    var updateData = $timeout(function (){
      init(vm.selectedMonday);
    }, 600000);
    
    function init(date){
      vm.backButton = true;
      vm.forwardButton = true;
      vm.loader = true;
      
      materialByPanelService.init(date)
        .then(function (){
          vm.loader = false;
          vm.backButton = false;
          vm.forwardButton = false;
          vm.panelCuttingList = materialByPanelService.panelCuttingList;
        });
      
    }

    function weekRange(mondayDate){
      var week = [];
      for (var i = 0; i<=(daysInWeek-1);i++){
        var day = dateUtil.plusDays(mondayDate, i);
        week.push(day);
      }
      return week;
    }
    
    function toggleMain(){
      vm.toggleMainFlag = !vm.toggleMainFlag;
    }
    
    function toggleMain2(){
      vm.toggleMainFlag2 = !vm.toggleMainFlag2;
    }
    
    function toggleCost(){
      vm.toggleCostFlag = !vm.toggleCostFlag;
    }
    
    function isNumber(value){
      return angular.isNumber(value);
    }

    function backWeek(){
      
      var newDate = dateUtil.minusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);
      
      if(newMonth !== vm.selectedMonth){
        init(newMonday);
      }

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function forwardWeek(){
      
      var newDate = dateUtil.plusDays(vm.selectedDate,7);
      var newMonday = dateUtil.weekStart(newDate);
      var newMonth = dateUtil.dateMonth(newMonday);
      
      if(newMonth !== vm.selectedMonth){
        init(newMonday);
      }

      vm.selectedDate = newDate;
      vm.selectedMonday = newMonday;
      vm.selectedMonth = newMonth;
    }
    
    function destroyPanelCuttingData() {
      $timeout.cancel(updateData);
      if (vm.panelCuttingList && vm.panelCuttingList.$destroy) {
        vm.panelCuttingList.$destroy();
        delete vm.panelCuttingList;
      }
    }

  }
})();
