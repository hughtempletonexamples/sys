(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .factory("loadTimbersService", loadTimbersService);

  /**
   * @ngInject
   */
  function loadTimbersService($log, $q, FileReader, schema, panelsService, projectsService) {
    
    var panelsCache = {};

    var PANEL_ID_REGEX = /^(\d+)-(\w+)-(\w+)-(\w+)-(\w+)-(\d+)$/;
    
    var cuttingTypes = ["63x38","84x38","89x38","109x38","114x38","135x38","140x38","179x38","184x38","230x38","235x38","140x200","140x220","140x240","140x240","140x260"];
    
    var floorChange = {};
    floorChange["GF"] = "00";
    floorChange["1F"] = "01";
    floorChange["2F"] = "02";
    floorChange["3F"] = "03";
    floorChange["4F"] = "04";
    floorChange["5F"] = "05";
    floorChange["6F"] = "06";
    floorChange["7F"] = "07";
    floorChange["8F"] = "08";
    floorChange["9F"] = "09";
    
    var floorChange2 = {};
    floorChange2["GF"] = "0F";
    floorChange2["1F"] = "FF";
    floorChange2["2F"] = "SF";
    floorChange2["3F"] = "TF";

    var service = {
      loadTimberSchedule: loadTimberSchedule,
      updatePanelsUsage: updatePanelsUsage,
      createCuttingList: createCuttingList,
      getRequiredByPanel: getRequiredByPanel,
      getRequiredByProject: getRequiredByProject
    };

    return service;

    ////////////

    function loadTimberSchedule(file, isHundegggerTest) {
        
      var delimiter = null;
      var jobsheet = null;
      var isHundegger = true;
    
      var jobsheet = file.name.toString();
      
      if(jobsheet.indexOf("_") !== -1){
        jobsheet = jobsheet.split("_").shift();
        isHundegger = false;
        delimiter = ",";
      }else{
        jobsheet = jobsheet.split(".").shift();
        delimiter = ";";
      }

      if (isHundegggerTest){
        delimiter = ",";
      }

      jobsheet = changeFloor(jobsheet, 10);

      return loadFile(file)
        .then(populatePanelsForProject)
        .then(loadFromWorkbook)
        .finally(clearCache);
      
      
      function populatePanelsForProject(file){
        
        if (isHundegger) {
          var jobsheetArr = jobsheet.split("-");
          var projectId = jobsheetArr[1];
          if(angular.isUndefined(panelsCache[projectId])){
            panelsCache[projectId] = [];
          }
          return projectsService.getProject(projectId)
            .then(function (project) {
              return panelsService.panelsForProject(project.$id).$loaded();
            })
            .then(function (panels) {
              angular.forEach(panels, function (panel) {
                panelsCache[projectId].push(panel);
              });
              return file;
            });
        } else {
          return file;
        }
      }

      function loadFile(file) {
        var deferred = $q.defer();
        var reader = new FileReader();
        reader.onload = function (e) {
          deferred.resolve(decode(e.target.result));
        };
        if (angular.isDefined(reader.readAsBinaryString)) {
          reader.readAsBinaryString(file);
        } else {
          reader.readAsArrayBuffer(file);
        }

        return deferred.promise;

        function decode(result) {
          if (angular.isString(result)) {
            return result;
          }

          var data = "";
          var chunkSize = 10240;
          for (var offset = 0; offset < result.byteLength; offset += chunkSize) {
            data += String.fromCharCode.apply(null, new Uint8Array(result.slice(offset, offset+chunkSize)));
          }
          return data;
        }
      }

      function loadFromWorkbook(workbook) {
        var response = {
          panels: [],
          errors: []
        };
        if (isHundegger) {
          return loadFromWorksheetsHundeggerPanelUpdate(response, workbook, jobsheet, delimiter)
            .then(function () {
              return response;
            });
        } else {
          return loadFromWorksheetsPanelUpdate(response, workbook, jobsheet, delimiter)
            .then(function () {
              return response;
            });
        }
      }
      
      function clearCache(result) {
        panelsCache = {};
        return result;
      }
    }

    function loadFromWorksheetsPanelUpdate(response, workbook, jobsheet, delimiter) {
      var deferredList = [];

      var allRows = workbook.split(/\r?\n|\r/);
      var totalm = [];
      var width = [];
      var height = [];
      var panelId = [];
      var amount = [];

      var filteredData = {};

      for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(delimiter);
        if (rowCells[0] !== "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];

            if (value) {
              switch (rowCell) {
              case 43:
                panelId.push(value);
                break;
              case 0:
                totalm.push(Math.round(value));
                break;
              case 1:
                width.push(parseInt(value));
                break;
              case 2:
                height.push(parseInt(value));
                break;
              case 3:
                amount.push(parseInt(value));
                break;
              }
            }
          }
        }
      }

      if(panelId.length && totalm.length && width.length && height.length){
        if(panelId.length === totalm.length){  
          for (var i = 0; i < panelId.length; i++) {
            var panelIdInCVS = panelId[i];
            panelIdInCVS = changeFloor(panelIdInCVS, 10);
            var actualId = panelIdInCVS.split("_").shift();
            var totalLengthInMeters = 0;
            var allGotMeters = true;
            var amountHolder = amount[i];
            var totalmHolder = totalm[i];
            if (amountHolder && totalmHolder){
              totalLengthInMeters = (amountHolder * totalmHolder);
            }else{
              allGotMeters = false;
            }
            var type = width[i] + "x" + height[i];
            var typeReverse = height[i] + "x" + width[i];

            if(actualId){
              if(cuttingTypes.indexOf(type) !== -1 || cuttingTypes.indexOf(typeReverse) !== -1){
                if (angular.isUndefined(filteredData[type])){
                  filteredData[type] = {};
                }
                if (angular.isUndefined(filteredData[type][actualId])){
                  filteredData[type][actualId] = {
                    "totalm" : 0, 
                    "width" : width[i], 
                    "height" : height[i], 
                    "amount" : 0, 
                    "jobsheet" : jobsheet,
                    "allGotMeters" : allGotMeters
                  };
                }
                filteredData[type][actualId].amount += amountHolder;
                filteredData[type][actualId].totalm += totalLengthInMeters;
              }
            }
          }
        }
      }
      
      angular.forEach(filteredData, function (data) {
        angular.forEach(data, function (predictedData, actualId) {
          deferredList.push(
            panelExists(actualId, predictedData.totalm, predictedData.width, predictedData.height, predictedData.amount, predictedData.jobsheet, predictedData.allGotMeters)
            .then(doesJobsheetMatchPanel)
            .then(updateJobsheetInPanel)
            .then(function (panel) {
              return panel;
            })
            .then(function (panel) {
              response.panels.push(panel);
            })
            .catch(function (error) {
              response.errors.push(error);
            })
          );
        });
      });

      return $q.all(deferredList);
    }
    
    function loadFromWorksheetsHundeggerPanelUpdate(response, workbook, jobsheet, delimiter) {
      var deferredList = [];
      var content = [];
      var row = 1;

      var allRows = workbook.split(/\r?\n|\r/);
      var totalm = [];
      var width = [];
      var height = [];
      var panelId = [];
      var amount = [];

      var filteredData = {};
      
      for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(delimiter);
        if (rowCells[singleRow] !== "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (rowCells[0] === "Part No.") {
              row = singleRow + 1;
              content.push(value);
            }
          }
        }
      }

      for (var singleRow = row; singleRow < allRows.length; singleRow++) {
        var rowCells = allRows[singleRow].split(delimiter);
        if (rowCells[0] !== "") {
          for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
            var value = rowCells[rowCell];
            if (value) {
              switch (content[rowCell]) {
              case "Part Name":
                panelId.push(parseInt(value));
                break;
              case "Length":
                totalm.push(value);
                break;
              case "Width":
                width.push(parseInt(value));
                break;
              case "Height":
                height.push(parseInt(value));
                break;
              case "Req.":
                amount.push(parseInt(value));
                break;
              }
            }
          }
        }
      }

      if(panelId.length && totalm.length && width.length && height.length){
        if(panelId.length === totalm.length){  
          for (var i = 0; i < panelId.length; i++) {
            var totalLengthInMeters = 0;
            var allGotMeters = true;
            var amountHolder = amount[i];
            var totalmHolder = totalm[i].match(/\d+/g);
            var jobsheetArr = jobsheet.split("-");
            var projectId = jobsheetArr[1];
            var phase = jobsheetArr[2];
            var floor = jobsheetArr[3];
            var type = jobsheetArr[4];
            var panelNumber = ("000" + panelId[i]).substr(-3);
            var actualId = projectId + "-" + phase + "-" + floor + "-" + type + "-" + panelNumber;
            actualId = changeFloor(actualId, 7);
            if (amountHolder && totalmHolder){
              totalLengthInMeters = (amountHolder * totalmHolder);
            }else{
              allGotMeters = false;
            }
            var type = width[i] + "x" + height[i];
            var typeReverse = height[i] + "x" + width[i];

            if(actualId){
              if(cuttingTypes.indexOf(type) !== -1 || cuttingTypes.indexOf(typeReverse) !== -1){
                if (angular.isUndefined(filteredData[type])){
                  filteredData[type] = {};
                }
                if (angular.isUndefined(filteredData[type][actualId])){
                  filteredData[type][actualId] = {
                    "totalm" : 0, 
                    "width" : width[i], 
                    "height" : height[i], 
                    "amount" : 0, 
                    "jobsheet" : jobsheet,
                    "allGotMeters" : allGotMeters
                  };
                }
                filteredData[type][actualId].amount += amountHolder;
                filteredData[type][actualId].totalm += totalLengthInMeters;
              }
            }
          }
        }
      }
      
      angular.forEach(filteredData, function (data) {
        angular.forEach(data, function (predictedData, actualId) {
          deferredList.push(
            findExistingPanel(actualId, projectId, predictedData.totalm, predictedData.width, predictedData.height, predictedData.amount, predictedData.jobsheet, predictedData.allGotMeters)
            .then(doesJobsheetMatchPanel)
            .then(updateJobsheetInPanel)
            .then(function (panel) {
              return panel;
            })
            .then(function (panel) {
              response.panels.push(panel);
            })
            .catch(function (error) {
              response.errors.push(error);
            })
          );
        });
      });

      return $q.all(deferredList);
    }
    
    function changeFloor(value, location){
      if (value) {
        var floor = value.substr(location, 2);
        for(var key in floorChange)
        {
          if(floorChange[key]===floor){
            return value.replace(floorChange[key], key);
          }
        }
        for(var key in floorChange2)
        {
          if(floorChange2[key]===floor){
            return value.replace(floorChange2[key], key);
          }
        }
      }
      return value;
    }
    
    function findExistingPanel(id, projectId, totalm, width, height, amount, jobsheet, allGotMeters) {
      var panel = {};
      var deferred = $q.defer();
      var usageDetails = {};
      var foundMatch = false;

      usageDetails.amount = amount;
      usageDetails.width = width;
      usageDetails.height = height;
      usageDetails.lenghtInMeters = totalm;
      
      angular.forEach(panelsCache[projectId], function (panelInProject) {
        var idParts = PANEL_ID_REGEX.exec(panelInProject.id);
        if (idParts) {
          var projectId = idParts[1];
          var phase = idParts[3];
          var floor = idParts[4];
          var panelType = idParts[5];
          var panelNumber = idParts[6];
          var dbPanelToMatch = projectId + "-" + phase + "-" + floor + "-" + panelType + "-" + panelNumber;
          if(dbPanelToMatch === id){
            panel.id = panelInProject.id;
            panel.jobsheet = jobsheet;
            panel.usageDetails = usageDetails;
            panel.project = panelInProject.project;
            panel.panel = panelInProject.$id;
            foundMatch = true;
          }
        }
      });
      if (allGotMeters && foundMatch) {
        deferred.resolve(panel);
      }else{
        if (!allGotMeters) {
          deferred.reject("One of the timbers in '" + id + "' has not a valid length"); 
        } else if (!foundMatch){
          deferred.reject("Can not find '" + id + "' in system!");
        }
      }

      return deferred.promise;
    }

    function panelExists(id, totalm, width, height, amount, jobsheet, allGotMeters) {
      var panel = {id: id};
      var deferred = $q.defer();
      var usageDetails = {};
      usageDetails.amount = amount;
      usageDetails.width = width;
      usageDetails.height = height;
      usageDetails.lenghtInMeters = totalm;
      panelsService.getPanelById(panel.id)
        .$loaded()
        .then(function (matchPanel) {
          if (matchPanel.length > 0) {
            panel.id = matchPanel[0].id;
            panel.jobsheet = jobsheet;
            panel.usageDetails = usageDetails;
            panel.project = matchPanel[0].project;
            panel.panel = matchPanel[0].$id;
            if (allGotMeters) {
              deferred.resolve(panel);
            }else{
              deferred.reject(panel.id + "' invalid timber length. Please check csv has all length fields"); 
            }
          } else {
            deferred.reject("Can not find '" + panel.id + "' in system!");
          }
        })
        .catch(function (error) {
          deferred.reject(id + " " + error);
        });

      return deferred.promise;
    }

    function doesJobsheetMatchPanel(panel) {
      var found = false;
      var deferred = $q.defer();
      var jobsheet = panel.jobsheet.substring(0,14);
      var idParts = PANEL_ID_REGEX.exec(panel.id);
      if (idParts) {
        var projectId = idParts[1];
        var phase = idParts[3];
        var floor = idParts[4];
        var panelType = idParts[5];
        var jobsheetFromPanelId = "JS-"+projectId+"-"+phase+"-"+floor+"-"+panelType;
      }

      if (jobsheet === jobsheetFromPanelId){
        found = true;
      }

      if (found) {
        deferred.resolve(panel);
      }else{
        deferred.reject("Panel does not match jobsheet in file name '" + jobsheetFromPanelId + "'");
      }

      return deferred.promise;
    } 
    
    function updateJobsheetInPanel(panel){
      var jobSheet = panel.jobsheet;
      var jobSheetParts = jobSheet.split("-");
      var jobSheetString = jobSheetParts[5];
      var jobSheetNumber = parseInt(jobSheetString);
              
      var deferred = $q.defer();
      getPanel(panel.panel)
        .$loaded()
        .then(function (panData) {
          panData.jobSheet = jobSheetNumber;
          panData.$save()
            .then(function () {
              deferred.resolve(panel);
            }, function (error) {
              deferred.reject("Panel jobsheet not updated '" +panel.jobsheet+ "'");
            });
        });
      return deferred.promise;
    }

    function updatePanelsUsage(panels) {
      var projectUpdates = [];

      angular.forEach(partitonPanelsByProject(panels), function (panelList, projectKey) {
        projectUpdates.push(updateProjectPanels(projectKey, panelList));
      });

      return $q.all(projectUpdates)
        .then(buildSummary);

      function buildSummary(updates) {
        var summary = {
          total: 0,
          details: []
        };
        angular.forEach(updates, function (value) {
          summary.details.push(value);
          summary.total += value.count;
        });

        return $q.when(summary);
      }
    }

    function updateProjectPanels(projectKey, panels) {
      var updateDetail = {};
      
      var project = schema.getObject(schema.getRoot()
                                     .child("projects")
                                     .child(projectKey));
      var existingPanels = getExistingPanelsUsage(projectKey);

      return existingPanels
        .$loaded()
        .then(doPanelUpdates)
        .then(recordUpdateCount)
        .then(function (){
          return doProjectDetail(project, updateDetail);
        })
        .then(returnDetail);

      function doPanelUpdates(existingPanels) {
        var x = panels.map(upsertPanel);
        return $q.all(x);

        function upsertPanel(panel) {
          var existing;
          angular.forEach(existingPanels, function (p) {
            if (p.id === panel.id && p.jobsheet === panel.jobsheet) {
              existing = p;
              existingPanels.$remove(existing);
            }
          });

          var newPanel = angular.extend({}, panel);
          return existingPanels.$add(newPanel);

        }
      }

      function recordUpdateCount(panelUpdates) {
        updateDetail.count = panelUpdates.length;
        return $q.when();
      }

      function returnDetail() {
        return $q.when(updateDetail);
      }
      
      function doProjectDetail(project, updateDetail) {
        var deferred = $q.defer();
        project
          .$loaded()
          .then(function () {
            updateDetail.projectId = project.id;
            updateDetail.projectName = project.name;
            deferred.resolve(updateDetail);
          });
        return deferred.promise;
      }
    }

    function partitonPanelsByProject(panels) {
      var panelsByProject = {};

      angular.forEach(panels, function (panel) {
        var project = panel.project;
        if (!panelsByProject[project]) {
          panelsByProject[project] = [];
        }
        panelsByProject[project].push(panel);
      });

      return panelsByProject;
    }
    
    function createCuttingList(jobsheets){

      var deferred = $q.defer();
      var existingJobsheets = getExistingJobsheets();
      var cuttingList = getCuttingLists();
            
      existingJobsheets.$loaded()
        .then(function () {
          return cuttingList.$loaded();
        })
        .then(alreadyExists)
        .then(function (returnData) {
          deferred.resolve(returnData);
        });

      return deferred.promise;
     
      function alreadyExists() {
        var find = {};
        var promises = [];
        for (var i = 0; i < jobsheets.length; i++) {
          if(angular.isUndefined(find[jobsheets[i]])){
            find[jobsheets[i]] = 0;
          }
          angular.forEach(existingJobsheets, function (jobsheetToCheck) {
            if (jobsheetToCheck.jobsheet === jobsheets[i]) {                  
              find[jobsheets[i]]++;
            }
          });
        }
        for (var i = 0; i < jobsheets.length; i++) {
          if (find[jobsheets[i]] > 0) {
            promises.push(returnCutingListRefs(jobsheets[i]));
          }else{
            promises.push(addCuttingListAndJobsheet(cuttingList, existingJobsheets, jobsheets[i]));
          }
        }
        return $q.all(promises);
      }
    }
    
    function returnCutingListRefs(js){
      
      var deferred = $q.defer();
      var cuttingListArray = [];

      getJobsheet(js)
        .$loaded()
        .then(function (jobsheet) {
          angular.forEach(jobsheet[0].cuttingList, function (item, key) {
            cuttingListArray.push(key);
          });
          deferred.resolve(cuttingListArray);
        });
      return deferred.promise;
    }

    function addCuttingListAndJobsheet(cuttingList, jobsheets, js){

      var deferred = $q.defer();
      var cuttingListArray = [];

      getPredictedByJobsheet(js)
        .$loaded()
        .then(function (required) {
          var promises = [];
          var list = {};
          var sorter = {};
          angular.forEach(required, function (item) {
            if(angular.isUndefined(sorter[item.usageDetails.width + " x " + item.usageDetails.height])){
              sorter[item.usageDetails.width + " x " + item.usageDetails.height] = 0;
            }
            sorter[item.usageDetails.width + " x " + item.usageDetails.height] += item.usageDetails.lenghtInMeters;
          });
          angular.forEach(sorter, function (item, key) {
            list = {
              "type": key,
              "lengths":{
                "fives":0,
                "fours":0,
                "threes":0
              },
              "cuttingDetails":{
                "required": item,
                "actual": 0
              }
            };
            promises.push(addCuttingList(cuttingList, list));
          });
          return $q.all(promises);
        })
        .then(function (cuttingLists) {
          var cuttingList = {};
          var jobsheetData = {};
          var projectId = js.substring(3, 6);
          
          angular.forEach(cuttingLists, function (item) {
            cuttingList[item] =  true;
          });
          
          cuttingListArray = cuttingList;
          
          jobsheetData.jobsheet = js;
          jobsheetData.projectId = projectId;
          jobsheetData.cuttingList = cuttingList;
          return jobsheets.$add(jobsheetData);
        })
        .then(function () {
          deferred.resolve(cuttingListArray);
        });

      function addCuttingList(cuttingList, list){
        var deferred = $q.defer();
        
        cuttingList.$add(list).then(function (ref) {
          var id = ref.key;
          deferred.resolve(id);
        });
        return deferred.promise;
      }
      
      return deferred.promise;
    }
    
    function getPredictedByJobsheet(jobsheet){
      var requiredPanelUsageRef = schema.getRoot().child("requiredPanelUsage").orderByChild("jobsheet").equalTo(jobsheet);
      return schema.getArray(requiredPanelUsageRef);
    }

    function getJobsheet(jobsheetId){
      var panels = schema.getRoot().child("jobsheets").orderByChild("jobsheet").equalTo(jobsheetId);
      return schema.getArray(panels);
    }

    function getExistingJobsheets(){
      var panels = schema.getRoot().child("jobsheets");
      return schema.getArray(panels);
    }

    function getCuttingLists(){
      var cutting = schema.getRoot().child("cuttingList");
      return schema.getArray(cutting);
    }
    
    function getPanel(panelKey) {
      return schema.getObject(schema.getRoot().child("panels").child(panelKey));
    }
    
    function getExistingPanelsUsage(projectKey){
      var requiredPanelUsageRef = schema.getRoot().child("requiredPanelUsage").orderByChild("project").equalTo(projectKey);
      return schema.getArray(requiredPanelUsageRef);
    }
    
    function getRequiredByPanel(ref){
      var cuttingList = schema.getRoot().child("requiredPanelUsage").orderByChild("panel").equalTo(ref);
      return schema.getArray(cuttingList);
    }
    
    function getRequiredByProject(ref){
      var cuttingList = schema.getRoot().child("requiredPanelUsage").orderByChild("project").equalTo(ref);
      return schema.getArray(cuttingList);
    }

  }

})();
