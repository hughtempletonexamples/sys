(function () {
  "use strict";

  angular
    .module("innView.materialanalysis")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/materialanalysis/loadtimbers", {
        templateUrl: require("./loadtimbers.html"),
        controller: "LoadTimbersController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialanalysis/cuttingTypePricing", {
        templateUrl: require("./cuttingTypePricing.html"),
        controller: "CuttingTypePricingController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialanalysis/materialByJobsheet", {
        templateUrl: require("./materialByJobsheet.html"),
        controller: "MaterialByJobsheetController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialanalysis/materialByPanel", {
        templateUrl: require("./materialByPanel.html"),
        controller: "MaterialByPanelController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialanalysis/cubicAverage", {
        templateUrl: require("./cubicAverage.html"),
        controller: "CubicAverageController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/materialanalysis/datumCubicAverage", {
        templateUrl: require("./datumCubicAverage.html"),
        controller: "DatumCubicAverageController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("operations");
  }

})();
