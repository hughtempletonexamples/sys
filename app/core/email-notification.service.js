(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("emailNotificationService", emailNotificationService);

  /**
   * @ngInject
   */
  function emailNotificationService($log, $q, dateUtil, projectsService, plannerService, areasService) {
    
    var service = {
      innviewWeeklyDispatch: innviewWeeklyDispatch,
      innviewWeeklyDispatchUpdater: innviewWeeklyDispatchUpdater
    };

    return service;

    ////////////

    function innviewWeeklyDispatch() {

      var reciepts = [];
      reciepts.push("hugh.templeton@innovaresystems.co.uk");
      reciepts.push("Leanne.Butters@innovaresystems.co.uk");
      reciepts.push("charlotte.sheldon@innovaresystems.co.uk");
      reciepts.push("andrew.bryant@innovaresystems.co.uk");
      reciepts.push("alex.banks@innovaresystems.co.uk");

      return queryAndSendWeeklyNotifications(reciepts);

    }
        
    function innviewWeeklyDispatchUpdater(project) {

      var reciepts = [];
      reciepts.push("hugh.templeton@innovaresystems.co.uk");
      reciepts.push("Leanne.Butters@innovaresystems.co.uk");
      reciepts.push("charlotte.sheldon@innovaresystems.co.uk");
      reciepts.push("andrew.bryant@innovaresystems.co.uk");
      reciepts.push("alex.banks@innovaresystems.co.uk");

      var todayDate = new Date();
      var todayDay = todayDate.getDay();

      var reSendThisWeeklyUpdate = false;
      var reSendNextWeeklyUpdate = false;
      var checkSendWeeklyThisWeek = false;
      var checkSendWeeklyNextWeek = false;

      var nextWeek = dateUtil.plusDays(todayDate, 7);

      if (todayDay === 4) {
        checkSendWeeklyThisWeek = true;
        checkSendWeeklyNextWeek = true;
      } else if (todayDay === 5) {
        checkSendWeeklyThisWeek = true;
        checkSendWeeklyNextWeek = true;
      } else if (todayDay >= 1 && todayDay <= 3) {
        checkSendWeeklyThisWeek = true;
      } else if (todayDay === 6) {
        checkSendWeeklyThisWeek = true;
        checkSendWeeklyNextWeek = true;
      } else if (todayDay === 0) {
        checkSendWeeklyThisWeek = true;
      }

      var thisWeekStart = dateUtil.weekStart(todayDate);
      var thisWeekEnd = dateUtil.plusDays(thisWeekStart, 4);

      var nextWeekStart = dateUtil.weekStart(nextWeek);
      var nextWeekEnd = dateUtil.plusDays(nextWeekStart, 4);

      thisWeekStart = new Date(thisWeekStart);
      thisWeekEnd = new Date(thisWeekEnd);
      nextWeekStart = new Date(nextWeekStart);
      nextWeekEnd = new Date(nextWeekEnd);

      var projectKey = project.$id;

      return getAreasDeliveryDate(projectKey, project)
        .then(function () {
          if (reSendThisWeeklyUpdate) {
            return queryAndSendWeeklyNotifications(thisWeekStart, thisWeekEnd);
          } else {
            return $q.resolve("No updates on weekly notification for this week");
          }
        })
        .then(function () {
          if (reSendNextWeeklyUpdate) {
            return queryAndSendWeeklyNotifications(nextWeekStart, nextWeekEnd);
          } else {
            return $q.resolve("No updates on weekly notification for next week");
          }
        });

      function getAreasDeliveryDate(projectKey, project) {

        return areasService.getProjectAreas(projectKey)
          .$loaded()
          .then(function (areas) {
            angular.forEach(areas, function (area) {
              if (angular.isDefined(project.deliverySchedule) && angular.isDefined(project.deliverySchedule.published) && angular.isDefined(project.deliverySchedule.unpublished)) {
                
                var revisionKey = project.deliverySchedule.published;
                var unpublished = project.deliverySchedule.unpublished;
                  
                if (angular.isDefined(area.revisions) && angular.isDefined(area.revisions[revisionKey])) {

                  var revisionsArr = [];
                  var revision = area.revisions[revisionKey];
                  var revisions = area.revisions;

                  for (var key in revisions) {
                    if (key !== revisionKey && key !== unpublished) {
                      revisionsArr.push(revisions[key]);
                    }
                  }

                  var prevRevision = revisionsArr[revisionsArr.length - 1];
                  var newRevision = revision;

                  revision = new Date(revision);

                  if (checkSendWeeklyThisWeek) {
                    if (revision >= thisWeekStart && revision <= thisWeekEnd) {
                      if (prevRevision !== newRevision) {
                        reSendThisWeeklyUpdate = true;
                      }
                    }
                  }
                  if (checkSendWeeklyNextWeek) {
                    if (revision >= nextWeekStart && revision <= nextWeekEnd) {
                      if (prevRevision !== newRevision) {
                        reSendNextWeeklyUpdate = true;
                      }
                    }
                  }
                }
              }

            });
          });
      }

    }
    
    function sortDates(obj) {
      var keys = Object.keys(obj);
      keys.sort(sorterDates);
      var ret = {};
      angular.forEach(keys, function (key, arrayIndex) {
        ret[key] = obj[key];
      });
      return ret;
    }
    
    function sorterDates(a, b) {
      var keyA = new Date(a);
      var keyB = new Date(b);
      // Compare the 2 dates
      if (keyA < keyB)
        return -1;
      if (keyA > keyB)
        return 1;
      return 0;
    }
    
    function getPanelsCount(area) {
      return area.actualPanels || area.estimatedPanels || 0;
    }

    function getCompletedPanelsCount(area) {
      return area.completedPanels || 0;
    }

    function getCompletedPanelsPercent(area) {
      var count = getPanelsCount(area);
      return count === 0 ? 0 : Math.floor(100 * getCompletedPanelsCount(area) / count);
    }
    
    function queryAndSendWeeklyNotifications(reciepts) {

      var todayDate = new Date();
      var todayDay = todayDate.getDay();
      var nextWeekAhead = [[8,13],[7,12],[6,11],[5,10],[4,9],[3,8],[2,7]];
      var textEntry = "";
      var areaData = {};
      var areaPlan = {};
      var areasAlerts = 0;
      var projects = {};

      var nextWeek = nextWeekAhead[todayDay];

      var nextWeekStart = dateUtil.plusDays(todayDate, nextWeek[0]);
      var nextWeekEnd = dateUtil.plusDays(todayDate, nextWeek[1]);
      var nextWeekStartHuman = dateUtil.toUserDate(nextWeekStart);

      nextWeekStart = new Date(nextWeekStart);
      nextWeekEnd = new Date(nextWeekEnd);

      textEntry += "<p>Hi,</p>";
      textEntry += "<p>Please see below areas due for week start " + nextWeekStartHuman + ":</p>";

      var projects = projectsService.getProjects();

      var  defer = $q.defer();

      return projects
        .$loaded()
        .then(processPlans)
        .then(processAreasDeliveryDate)
        .then(buildTable)
        .then(sendMail)
        .then(function (res) {
          if (res === "None") {
            $log.log("No deliverys found for next week, no emails sent");
            defer.resolve("No deliverys found for next week, no emails sent");
          } else {
            $log.log("Email Sent");
            defer.resolve("Email Sent");
          }
          return defer.promise;
        });


      function processPlans(){
        var promises = [];
        angular.forEach(projects, function (project) {
          if (project.active) {
            var projectKey = project.$id;
            promises.push(getAreasPlans(projectKey));
          }
        });
        return $q.all(promises);
      }

      function processAreasDeliveryDate(){
        var promises = [];
        angular.forEach(projects, function (project) {
          if (project.active) {
            var projectKey = project.$id;
            promises.push(getAreasDeliveryDate(projectKey, project));
          }
        });
        return $q.all(promises);
      }

      function getAreasPlans(projectKey) {
        return plannerService.getProjectAreaProductionPlans(projectKey)
          .$loaded()
          .then(function (plans){
            angular.forEach(plans, function (plan) {
              var areaKey = plan.$id;
              if (angular.isUndefined(areaPlan[areaKey])) {
                areaPlan[areaKey] = plan;
              }
            });
          });
      }

      function getAreasDeliveryDate(projectKey, project) {
        return areasService.getProjectAreas(projectKey)
          .$loaded()
          .then(function (areas) {
            angular.forEach(areas, function (area) {
              var areaKey = area.$id;
              if (angular.isDefined(project.deliverySchedule) && angular.isDefined(project.deliverySchedule.published)) {
                var revisionKey = project.deliverySchedule.published;
                if (angular.isDefined(area.revisions) && angular.isDefined(area.revisions[revisionKey]) && area.supplier === "Innovaré") {
                  var revision = area.revisions[revisionKey];
                  revision = dateUtil.subtractWorkingDays(revision, 1);
                  revision = new Date(revision);
                  var percentage = getCompletedPanelsPercent(area);
                  var areaRiskDays = "";
                  if (angular.isDefined(areaPlan[areaKey])) {
                    var riskDays = dateUtil.daysBetweenWeekDaysOnly(areaPlan[areaKey].plannedFinish, areaPlan[areaKey].riskDate);
                    if (riskDays < 0) {
                      areaRiskDays = "Risk: " + riskDays + " days";
                    } else {
                      areaRiskDays = "Not at risk";
                    }
                  } else {
                    areaRiskDays = "Not at risk";
                  }
                  if (revision >= nextWeekStart && revision <= nextWeekEnd) {
                    var revisionHumanDate = dateUtil.toUserDate(revision);
                    if (angular.isUndefined(areaData[revisionHumanDate])) {
                      areaData[revisionHumanDate] = {};
                    }
                    if (angular.isUndefined(areaData[revisionHumanDate][project.name])) {
                      areaData[revisionHumanDate][project.name] = [];
                    }
                    var areaId = project.id + "-" + area.phase + "-" + area.floor + "-" + area.type;
                    var area = {
                      "date": revisionHumanDate,
                      "project" : project.name,
                      "area": areaId,
                      "percent": percentage,
                      "risk": areaRiskDays
                    };
                    areaData[revisionHumanDate][project.name].push(area);
                    areasAlerts++;
                  }
                }
              }
            });
          });
      }

      function buildTable() {
        
        areaData = sortDates(areaData);

        var dateCount = 0;
        
        for (var date in areaData) {
          var projectArray = areaData[date];
          textEntry += "<table style='border: 1px solid black;width:100%'>";
          textEntry += "<tr>";
          textEntry += "<td style='text-align:center;width:15%;'>"+date+"</td>";
          textEntry += "<td style='width:85%;'>";

          for (var project in projectArray) {
            var areasArray = projectArray[project];
            dateCount = 0;
            textEntry += "<table style='table-layout: fixed;border: 1px solid black;width:100%'>";
            textEntry += "<tr>";
            textEntry += "<th style='border: 1px solid #dddddd;'>Project:</th>";
            textEntry += "<td style='border: 1px solid #dddddd;'>Area</td>";
            textEntry += "<td style='border: 1px solid #dddddd;'>Precent Complete</td>";
            textEntry += "<td style='border: 1px solid #dddddd;'>Risk</td>";
            textEntry += "</tr>";
            for (var i = 0; i < areasArray.length; i++) {
              dateCount++;
              var area = areasArray[i];
              textEntry += "<tr>";
              if (dateCount === 1) {
                textEntry += "<th style='width: 33%;border: 1px solid #dddddd;' rowspan='" + areasArray.length + "'>" + project + "</th>";
              }
              textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.area + "</td>";
              textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.percent + "</td>";
              textEntry += "<td style='width: 33%;border: 1px solid #dddddd;'>" + area.risk + "</td>";
              textEntry += "</tr>";
            }
            textEntry += "</table>";
          }

          textEntry += "</td>";
          textEntry += "</tr>";
          textEntry += "</table>";
          textEntry += "</table>";
        }

      }

      function sendMail() {

        textEntry += "<p>Innview</p>";

        $log.log(textEntry);

        if (areasAlerts) {
          var promises = [];
          for (var i = 0; i < reciepts.length; i++) {
            $log.log("email sent to " + reciepts[i] + "for Innview Weekly Dispatch");
            promises.push($q.when());
            //promises.push(mailTransport.sendMail({from: "'Innview' <sender2.innovaresystems@gmail.com>", to: reciepts[i], subject: "Innview Weekly Dispatch", html: textEntry}));
          }
          return $q.all(promises);
        } else {
          return $q.when("None");
        }

      }

    }
    
  }
  
})();
