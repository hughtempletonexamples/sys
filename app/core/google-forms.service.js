(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("googleFormsService", googleFormsService);

  /**
   * @ngInject
   */
  function googleFormsService(schema) {
    var service = {
      getFormData: getFormData
    };

    return service;

    ////////////

    function getFormData(formName) {
      return schema.getArray(schema.getRoot()
                            .child("googleforms")
                            .child(formName));
    }
  }

})();
