(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("plusPercentMinus", filter);

  function filter($filter) {

    return plusPercentMinusFilter;

    function plusPercentMinusFilter(target, minus) {

      if (angular.isDefined(target) && angular.isDefined(minus)) {
        var percentAddition = 25 / 100 * target;
        target = target + percentAddition;
        target = $filter("number")(target, 0);
        return minus - target;
      } else {
        return 0;
      }

    }
      
  }

})();
