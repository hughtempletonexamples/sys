(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("plusPercent", filter);

  function filter($filter) {

    return plusPercentFilter;

    function plusPercentFilter(target) {
      
      if(angular.isDefined(target)){
        var percentAddition = 25 / 100 * target;
        target = target + percentAddition;
        target = $filter("number")(target, 0);
        return target;
      }else{
        return 0;
      }

    }
  }

})();
