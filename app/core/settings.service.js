(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("settingsService", settings);

  /**
   * @ngInject
   */
  function settings(schema, firebaseUtil) {

    var defaultsRef = schema.getRoot().child("srd/defaultSettings");
    var settingsRef = schema.getRoot().child("settings");

    var service = {
      default: getDefaultSettings,
      get: getSettings,
      set: updateSettings
    };

    return service;

    ////////////


    function getDefaultSettings() {
      var defaults = schema.getObject(defaultsRef);
      return defaults
        .$loaded()
        .then(extractSettings)
        .then(cleanup);

      function cleanup(result) {
        defaults.$destroy();
        return result;
      }
    }

    function getSettings() {
      var defaults = schema.getObject(defaultsRef);
      var settings = schema.getObject(settingsRef);

      return firebaseUtil.loaded(defaults, settings)
        .then(overlaySettings)
        .then(cleanup);

      function overlaySettings() {
        return angular.extend(extractSettings(defaults), extractSettings(settings));
      }

      function cleanup(result) {
        defaults.$destroy();
        settings.$destroy();
        return result;
      }
    }

    function updateSettings(newValues) {
      var defaults = schema.getObject(defaultsRef);
      var settings = schema.getObject(settingsRef);

      return firebaseUtil.loaded(defaults, settings)
        .then(doUpdates)
        .then(save)
        .then(cleanup);

      function doUpdates() {
        angular.forEach(newValues, function (value, key) {
          settings[key] = value !== defaults[key] ? value : null;
        });
      }

      function save() {
        return settings.$save();
      }

      function cleanup(result) {
        defaults.$destroy();
        settings.$destroy();
        return result;
      }
    }

    function extractSettings(settingsObject) {
      var settings = {};
      angular.forEach(settingsObject, function (value, key) {
        settings[key] = value;
      });
      return settings;
    }

  }

})();
