(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innImageUploader", {
      templateUrl: require("./imageUploader.html"),
      controller: imageUploaderController,
      controllerAs: "vm",
      bindings: {
        data:"<",
        show:"<",
        error:"<",
        confirm:"<"
      }
    });

  /**
   * @ngInject
   */
  function imageUploaderController($document,$uibModal,qaService,firebase,schema) {
    var vm = this;
    vm.uploadImage = uploadImage;
    vm.saveErrors = saveErrors;
    vm.openPopup = openPopup;
    vm.progress = 0;
    vm.show = true;
    var $uibModal = $uibModal;
    var $document = $document;
    vm.confirm = vm.confirmErrors;
    vm.hideUploader = false;
    
    function saveErrors(error,imageId) {
      var panelId = vm.data.$id;
      if (error !== null && angular.isDefined(panelId)) {
        return qaService.saveError(panelId, vm.error,imageId);

      }
    }
    
    function uploadImage(data){
      vm.confirm = false;
      if (angular.isDefined(data)) {
        var file = data;
      } else {
        var file = $document[0].getElementById("image").files[0];
      }
      
      var data = vm.data;
      if (file.size >= 1) {
        var key = data.$id;
        var imageId = "-" + makeId(20);
        openPopup("ev", $uibModal, $document);
        return uploadToStorage(imageId,key,file)
          .then(saveErrors(vm.errors,imageId));
      }
      vm.errorSuccess = true;
      vm.show = false;
    }
    
    

    function uploadToStorage(imageId,key,file){
      var storageRef = firebase.storage().ref();
      var imageRef = storageRef.child("images/panels/" + key + "/" + imageId);
      return imageRef.put(file);      
    }    
    
    function makeId(length) {
      var result = "";
      var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
    }    
  }
  
  function openPopup(ev, $uibModal, $document) {

    var parentElem = angular.element($document[0].querySelector(".image-uploader"))

    return $uibModal.open({
      templateUrl: "imgModal.html",
      controller: ["$uibModalInstance","$uibModalStack", imageModalController],
      controllerAs: "vm",
      appendTo: parentElem,
      size: "500px",
      backdrop: "static"
    });

    function imageModalController($uibModalInstance,$uibModalStack) {
      var vm = this;
      vm.close = close;

      vm.ok = "ok";

      function close() {
        var reason = "because";
        angular.element($document[0].querySelector(".inputImage").style.setProperty("display" ,"none", !"important"));
        $uibModalStack.dismissAll(reason);

      }
    }
  }  
  
  
})();

