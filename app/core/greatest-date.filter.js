(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("greatestDate", filter);

  function filter(dateUtil, $filter) {

    return greatestDateFilter;

    ////////////

    function greatestDateFilter(obj, getKey) {
      
      if (angular.isDefined(obj)){
        var keys = Object.keys(obj);
        var date = keys[keys.length-1];
        
        if (dateUtil.validDate(date)){
          if (!getKey){
            return $filter("currency")(obj[date], "£");
          }else{
            return dateUtil.toUserDateNew(date);
          }
        }
      }

    }
   
  }

})();
