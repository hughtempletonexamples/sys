(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("srdName", filter);

  function filter($log, referenceDataService) {
    var srdNameCache = {};

    srdNameFilter.$stateful = true;
    return srdNameFilter;

    ////////////

    function srdNameFilter(srdValue, variant) {
      if (!srdValue || !variant) {
        return "";
      }

      return srdNameCache[variant+"-"+srdValue] || lookupLabel(srdValue, variant);

      function lookupLabel(srdValue, variant) {

        referenceDataService.getItem(variant, srdValue)
              .$loaded()
              .then(function (item) {
                srdNameCache[variant+"-"+srdValue] = item.name;
                item.$destroy();
              });
        return srdNameCache[variant+"-"+srdValue] = srdValue;
      }
    }
  }
})();
