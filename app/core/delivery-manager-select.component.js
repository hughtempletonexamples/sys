(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("deliveryManagerSelect", {
      templateUrl: function ($element, $attrs) {
        if(angular.isDefined($attrs.theme) && "newux" === $attrs.theme) {
          return require("./newux-selectDeliveryManager.html");
        }
        return require("./selectDeliveryManager.html");
      },
      controller: deliveryManagerSelectController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        name: "@",
        required: "@",
        dmNameChange: "=",
        ngModel: "=",
        disabled: "=ngDisabled",
        theme: "@"
      }
    });

  /**
   * @ngInject
   */
  function deliveryManagerSelectController($log, usersService) {
    var vm = this;

    vm.users = usersService.getAllUsers();
    vm.selectOptions = getSelectOptions();
    vm.isDeliveryManager = isDeliveryManager;

    vm.$onDestroy = function () {
      vm.users.$destroy();
    };

    ////////////
    function getSelectOptions() {
      return "user.$id as user.name for user in vm.users | filter:vm.isDeliveryManager";
    }

    function isDeliveryManager(user) {
      return angular.isDefined(user.roles["delivery-manager"]) && user.roles["delivery-manager"];
    }
  }

})();
