(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("objectKey", filter);

  function filter() {
    var vm = this;
    return objectKeyFilter;

    function objectKeyFilter(obj) {
      var objectKey = {};
      if (angular.isDefined(obj)) {
        vm.sorted = Object.keys(obj).sort();
        angular.forEach(vm.sorted, function (key, data) {
          if (angular.isUndefined(objectKey[key])) {
            objectKey[key] = obj[key];
          }
        });
       
      }
      return objectKey;
    }
  }

})();
