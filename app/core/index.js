(function () {
  "use strict";

  //Because we use iife functions to encapsulate our code, we need to
  //globally expost the firebase variable, so that it's available to
  //other modules.
  require("expose?firebase!firebase");
  require("angularfire");

  require("./module");
  require("./date-util");
  require("./firebase-util");
  require("./schema.service");
  require("./settings.service");
  require("./audit.service");
  require("./reference-data.service");
  require("./email-notification.service");
  require("./users.service");
  require("./functions.service");
  require("./google-forms.service");
  require("./user.filter");
  require("./operative.filter");
  require("./risk-days.filter");
  require("./revision-count.filter");
  require("./object-limitTo.filter");
  require("./key-length.filter");
  require("./object-key.filter");
  require("./objectKeys.filter");
  require("./greatest-date.filter");
  require("./plus-percent.filter");
  require("./plus-percent-minus.filter");
  require("./decimal-if-number.filter");
  require("./navbar.component");
  require("./srd-select.component");
  require("./date-input.component");
  require("./month-input.component");
  require("./date-future-input.component");
  require("./date-request-input.component");
  require("./date-planner-input.component");
  require("./image-uploader.component");
  require("./delivery-manager-select.component");
  require("./day-errors.component");
  require("./srd.filter");

})();
