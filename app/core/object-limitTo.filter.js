(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("objectLimitTo", filter);

  function filter() {

    return objectLimitToFilter;

    ////////////

    function objectLimitToFilter(obj, limit, begin) {
      
      var keys = Object.keys(obj);
      if (keys.length < 1) {
        return [];
      }

      if (limit > 0) {
        var ret = new Object;
        var count = 0;
        angular.forEach(keys, function (key, arrayIndex) {
          if (count >= limit) {
            return false;
          }
          ret[key] = obj[key];
          count++;
        });
        return ret;
      } else if (limit < 0) {
        var ret = new Object;
        var lastOf = begin + limit + keys.length;
        var count = 0;
        angular.forEach(keys, function (key, arrayIndex) {
          if (count >= lastOf) {
            ret[key] = obj[key];
          }
          count++;
        });
        return ret;

      }
    }
  }

})();
