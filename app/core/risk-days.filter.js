(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("riskDays", filter);

  function filter() {

    return riskDaysFilter;

    ////////////

    function riskDaysFilter(days) {
      if (angular.isUndefined(days)) {
        return "!";
      }

      if (days == null) {
        return "";
      }

      return days > 0 ? "+" + days : days.toString();
    }
  }

})();