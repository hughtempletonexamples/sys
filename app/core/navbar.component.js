(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innNavbar", {
      templateUrl: function ($element, $attrs) {
        if(angular.isDefined($attrs.theme) && "newux" === $attrs.theme) {
          return require("./newux-navbar.html");
        }
        return require("./navbar.html");
      },
      controller: navBarController,
      controllerAs: "vm",
      transclude: true,
      bindings: {
        theme: "@"
      }
    });

  /**
   * @ngInject
   */
  function navBarController(schema) {
    var vm = this;

    vm.status = schema.status;
  }

})();
