(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("decimalIfNumber", filter);

  function filter($filter) {

    return decimalIfNumberFilter;

    function decimalIfNumberFilter(number, isWeekend) {
      if(number === 0){
        if(isWeekend){
          return "";
        }else{
          return 0;
        }    
      }else{
        return $filter("number")(number, 1);
      }
    }
  }

})();
