(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("user", filter);

  function filter(schema) {
    var userNameCache = {};

    userNameFilter.$stateful = true;
    return userNameFilter;

    ////////////

    function userNameFilter(userId) {
      if (!userId) {
        return "";
      }

      return userNameCache[userId] || lookupUserName(userId);

      function lookupUserName(userId) {
        var user = schema.getObject(schema.getRoot().child("users").child(userId));
        user
          .$loaded()
          .then(function () {
            userNameCache[userId] = user.name;
            user.$destroy();
          });

        return userNameCache[userId] = "...";
      }
    }
  }

})();
