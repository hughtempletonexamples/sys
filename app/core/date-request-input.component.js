(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innRequestDateInput", {
      templateUrl: require("./inputDateRequest.html"),
      controller: dateFutureInputController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        name: "@",
        required: "@",
        deliv : "@",
        dateString: "=ngModel",
        ngChange: "&?ngChange"
      }
    });

  /**
   * @ngInject
   */
  function dateFutureInputController($log, $scope, $filter, $timeout, dateUtil) {
    var vm = this;

    vm.isOpen = false;
    vm.datepickerOptions = datepickerOptions();
    vm.dateFormat = dateFormat();
    vm.alternativeFormats = alternativeFormats();
    vm.dateChanged = dateChanged;
    vm.toggleOpen = toggleOpen;

    init();

    ////////////

    function init() {
      $scope.$watch(
        "vm.dateString",
        function handleDateStringChange(newValue) {
          if (angular.isDefined(newValue)) {
            vm.date = toDate(newValue);
          }
        }
      );
    }

    function dateChanged() {
      vm.dateString = fromDate(vm.date);
      if (vm.ngChange) {
        $timeout(vm.ngChange, 0);
      }
    }

    function datepickerOptions() {
      var today = new Date();
      var minDate = "";
      if(vm.deliv){
        var notDayAfter = dateUtil.plusDays(today, 1);
        minDate = new Date(notDayAfter);
      }else{
        var notDayAfter = dateUtil.plusDays(today, 2);
        minDate = new Date(notDayAfter);
      }
      
      
      return {
        minDate: minDate,
        formatDay: "d",
        showWeeks: false,
        maxMode: "day",
        minMode: "day",
        startingDay: 1
      };
    }

    function dateFormat() {
      return "d!/M!/yyyy";
    }

    function alternativeFormats() {
      return [ "d!/M!/yy", "yyyy-MM-dd" ];
    }

    function toggleOpen() {
      vm.isOpen = !vm.isOpen;
    }

    function fromDate(date) {
      return $filter("date")(date, "yyyy-MM-dd");
    }

    function toDate(dateString) {
      if (dateString) {
        return new Date(dateString);
      } else {
        return null;
      }
    }
  }

})();
