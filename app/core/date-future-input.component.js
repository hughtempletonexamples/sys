(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innDateFutureInput", {
      templateUrl: require("./inputDate.html"),
      controller: dateFutureInputController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        name: "@",
        required: "@",
        dateString: "=ngModel",
        ngChange: "&?ngChange"
      }
    });

  /**
   * @ngInject
   */
  function dateFutureInputController($scope, $filter, $timeout) {
    var vm = this;

    vm.isOpen = false;
    vm.datepickerOptions = datepickerOptions();
    vm.dateFormat = dateFormat();
    vm.alternativeFormats = alternativeFormats();
    vm.dateChanged = dateChanged;
    vm.toggleOpen = toggleOpen;

    init();

    ////////////

    function init() {
      $scope.$watch(
        "vm.dateString",
        function handleDateStringChange(newValue) {
          if (angular.isDefined(newValue)) {
            vm.date = toDate(newValue);
          }
        }
      );
    }

    function dateChanged() {
      vm.dateString = fromDate(vm.date);
      if (vm.ngChange) {
        $timeout(vm.ngChange, 0);
      }
    }

    function datepickerOptions() {
      return {
        minDate: new Date(),
        formatDay: "d",
        showWeeks: false,
        maxMode: "day",
        minMode: "day",
        startingDay: 1
      };
    }

    function dateFormat() {
      return "d!/M!/yyyy";
    }

    function alternativeFormats() {
      return [ "d!/M!/yy", "yyyy-MM-dd" ];
    }

    function toggleOpen() {
      vm.isOpen = !vm.isOpen;
    }

    function fromDate(date) {
      return $filter("date")(date, "yyyy-MM-dd");
    }

    function toDate(dateString) {
      if (dateString) {
        return new Date(dateString);
      } else {
        return null;
      }
    }
  }

})();
