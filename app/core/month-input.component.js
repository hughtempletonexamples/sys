(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innMonthInput", {
      templateUrl: require("./inputMonth.html"),
      controller: dateInputController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        name: "@",
        required: "@",
        dateString: "=ngModel",
        ngChange: "&?ngChange"
      }
    });

  /**
   * @ngInject
   */
  function dateInputController($scope, $filter, $timeout) {
    var vm = this;

    vm.isOpen = false;
    vm.datepickerOptions = datepickerOptions();
    vm.datepickerMode = "month";
    vm.dateFormat = dateFormat();
    vm.alternativeFormats = alternativeFormats();
    vm.dateChanged = dateChanged;
    vm.toggleOpen = toggleOpen;

    init();

    ////////////

    function init() {
      $scope.$watch(
        "vm.dateString",
        function handleDateStringChange(newValue) {
          if (angular.isDefined(newValue)) {
            vm.date = toDate(newValue);
          }
        }
      );
    }

    function dateChanged() {
      vm.dateString = fromDate(vm.date);
      if (vm.ngChange) {
        $timeout(vm.ngChange, 0);
      }
    }

    function datepickerOptions() {
      return {
        formatDay: "d",
        showWeeks: false,
        maxMode: "month",
        minMode: "month",
        startingDay: 1
      };
    }

    function dateFormat() {
      return "M!/yyyy";
    }

    function alternativeFormats() {
      return [ "M!/yy", "yyyy-MM" ];
    }

    function toggleOpen() {
      vm.isOpen = !vm.isOpen;
    }

    function fromDate(date) {
      return $filter("date")(date, "yyyy-MM");
    }

    function toDate(dateString) {
      if (dateString) {
        return new Date(dateString);
      } else {
        return null;
      }
    }
  }

})();
