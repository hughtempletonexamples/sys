(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("keyLength", filter);

  function filter() {

    return keyLengthFilter;

    function keyLengthFilter(input, plus) {
      var keyLength = 0;
      if (angular.isDefined(input)) {
        if (!angular.isObject(input)) {
          throw Error("Usage of non-objects with keylength filter!!");
        }
        if (angular.isUndefined(plus)) {
          plus = 0;
        }
        keyLength = Object.keys(input).length + plus;
      }
      return keyLength;
    }
  }    

})();
