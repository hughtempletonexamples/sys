(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("audit", audit);

  /**
   * @ngInject
   */
  function audit($log, $q, schema, firebaseUtil) {

    var auditEntries;
    var status;

    var service = {
      log: log,
      project: project,
      productionLine: productionLine,
      panel:panel
    };

    init();

    return service;

    ////////////

    function init() {
      auditEntries = schema.getRoot().child("auditEntries");
      status = schema.status;
    }

    function log(text) {
      var entry = auditEntry(text);

      return appendLogEntry(entry);
    }

    function project(text, project, projectId) {
      var entry = auditEntry(text);

      return getProjectId()
        .then(function (projectId) {
          entry.project = project;
          entry.projectId = projectId;
          entry.link = "";
          return appendLogEntry(entry);
        })
        .catch(function (error) {
          $q.reject("Error in auditService.project(" + projectId + "): " + error);
        });

      function getProjectId() {
        var deferred = $q.defer();
        if (projectId) {
          deferred.resolve(projectId);
        }
        else {
          var projectIdRef = schema.getRoot().child("projects").child(project).child("id");
          projectIdRef.once("value", function (val) {
            deferred.resolve(val.val());
          });
        }
        return deferred.promise;
      }
    }
    
    
    function panel(text,link, productionLine,projectId) {
      var entry = auditEntry(text);
      entry.projectId = projectId;
      entry.link = link;
      entry.productionLine = productionLine;
      return appendLogEntry(entry);

    }    
    

    function productionLine(text, productionLine) {
      var entry = auditEntry(text);
      entry.productionLine = productionLine;

      return appendLogEntry(entry);
    }

    function auditEntry(text) {
      return {
        log: text,
        timestamp: firebaseUtil.serverTime(),
        user: status.userId,
        userName: status.userName
      };
    }

    function appendLogEntry(entry) {
      var deferred = $q.defer();
      auditEntries.push(entry, callback);

      return deferred.promise;

      // Mockfirebase.push() is not thenable :-(
      function callback(error) {
        if (!error) {
          deferred.resolve();
        }
        else {
          $log.error(error);
          deferred.reject(error);
        }
      }
    }

  }

})();
