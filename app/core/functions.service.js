(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("functionsService", functionsService);

  /**
   * @ngInject
   */
  function functionsService() {
    var service = {
      logicHSIP: logicHSIP,
      uploaderLogicHSIP: uploaderLogicHSIP
    };

    return service;

    ////////////
    
    function logicHSIP(foundProductType, panelType, height, length) {

      var isHSIP = "H";
      var isIFAST = "F";

      if (foundProductType === "Ext" && foundProductType !== "ExtH") {

        if (angular.isDefined(panelType) && angular.isDefined(height) && angular.isDefined(length)) {

          if (panelType === "H" || (height <= 600 && length <= 2650) || (length <= 600 && height <= 2650)) {
            foundProductType = foundProductType + isHSIP;
          }else if(panelType === "F") {
            foundProductType = foundProductType + isIFAST;
          }

        }

      }
      return foundProductType;
    }
    
    function uploaderLogicHSIP(foundProductType, panelType, height, length) {

      var isHSIP = "H";
      var isIFAST = "F";

      if (foundProductType === "Ext" && foundProductType !== "ExtH" && foundProductType !== "ExtF") {

        if (angular.isDefined(panelType)) {

          if (panelType === "H") {
            foundProductType = foundProductType + isHSIP;
          }else if(panelType === "F") {
            foundProductType = foundProductType + isIFAST;
          }

        }

      }
      return foundProductType;
    }
   
  }
})();
