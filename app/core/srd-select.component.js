(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innSrdSelect", {
      templateUrl: function ($element, $attrs) {
        if(angular.isDefined($attrs.theme) && "newux" === $attrs.theme) {
          return require("./newux-selectReferenceData.html");
        }
        return require("./selectReferenceData.html");
      },
      controller: srdSelectController,
      controllerAs: "vm",
      bindings: {
        id: "@",
        name: "@",
        required: "@",
        variant: "@",
        ngModel: "=",
        disabled: "=ngDisabled",
        theme: "@"
      }
    });

  /**
   * @ngInject
   */
  function srdSelectController($log, referenceDataService) {
    var vm = this;

    vm.items = getReferenceData(vm.variant);
    vm.selectOptions = getSelectOptions(vm.variant);

    vm.$onDestroy = function () {
      vm.items.$destroy();
    };

    ////////////

    function getReferenceData(variant) {
      switch (variant) {
      case "chainOfCustody":
        return referenceDataService.getChainOfCustodyTypes();
      case "datum":
        return referenceDataService.getDatumTypes();
      case "component":
        return referenceDataService.getComponents();
      case "vehicleType":
        return referenceDataService.getVehicleTypes();
      case "phase":
        return referenceDataService.getPhases();
      case "floor":
        return referenceDataService.getFloors();
      case "productType":
        return referenceDataService.getProductTypes();
      case "supplier":
        return referenceDataService.getSuppliers();
      case "offloadMethod":
        return referenceDataService.getOffloadMethods();
      case "gateway":
        return referenceDataService.getGateways();
      default:
        $log.error("Unknown reference data type '" + variant + "'");
        return [];
      }
    }

    function getSelectOptions(variant) {
      var label;

      switch (variant) {
      case "phase":
      case "floor":
        label = "$id";
        break;
      default:
        label = "name";
      }
      return "item.$id as item." + label + " for item in vm.items |orderBy: 'seq'";
    }
  }

})();
