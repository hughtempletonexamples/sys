(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("referenceDataService", referenceDataService);

  /**
   * @ngInject
   */
  function referenceDataService($log, schema) {
    var service = {
      getChainOfCustodyTypes: getChainOfCustodyTypes,
      getComponents: getComponents,
      getDatumTypes: getDatumTypes,
      getVehicleTypes: getVehicleTypes,
      getHaulierTypes: getHaulierTypes,
      getPhases: getPhases,
      getFloors: getFloors,
      getProductionLines: getProductionLines,
      getProductionSubLines: getProductionSubLines,
      getProductTypes: getProductTypes,
      getPanelTypes: getPanelTypes,
      getSuppliers: getSuppliers,
      getOffloadMethods: getOffloadMethods,
      getCuttingTypes: getCuttingTypes,
      getStockCount: getStockCount,
      getGateways: getGateways,
      getItem: getItem
    };

    return service;

    ////////////

    function getChainOfCustodyTypes() {
      return getReferenceData("chainOfCustodyTypes");
    }
    
    function getComponents() {
      return getReferenceData("components");
    }

    function getDatumTypes() {
      return getReferenceData("datumTypes");
    }

    function getVehicleTypes() {
      return getReferenceData("vehicleTypes");
    }
    
    function getHaulierTypes() {
      return getReferenceData("haulierTypes");
    }

    function getPhases() {
      return getReferenceData("phases");
    }

    function getFloors() {
      return getReferenceData("floors");
    }

    function getProductionLines() {
      return getReferenceData("productionLines");
    }
    
    function getProductionSubLines() {
      return getReferenceData("productionSubLines");
    }

    function getProductTypes() {
      return getReferenceData("productTypes");
    }

    function getPanelTypes() {
      return getReferenceData("panelTypes");
    }

    function getSuppliers() {
      return getReferenceData("suppliers");
    }

    function getOffloadMethods() {
      return getReferenceData("offloadMethods");
    }
    
    function getCuttingTypes(){
      return getReferenceData("cuttingTypes");
    }
    
    function getStockCount(){
      return getReferenceData("stockCount");
    }
    
    function getGateways(){
      return getReferenceData("gateways");
    }


    function getItem(variant, id) {
      var srdRef = schema
            .getRoot()
            .child("srd")
            .child(variant)
            .child(id);
      return schema.getObject(srdRef);
    }

    function getReferenceData(key) {
      var srdRef = schema
        .getRoot()
        .child("srd")
        .child(key);
      return schema.getArray(srdRef);
    }
  }
})();
