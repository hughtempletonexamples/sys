(function () {
  "use strict";

  angular
    .module("innView.core")
    .component("innDayErrors", {
      templateUrl: require("./dayErrors.html"),
      controller: errorsByDayController,
      controllerAs: "vm",
      bindings: {
        date: "@"
      }
    });

  /**
   * @ngInject
   */
  function errorsByDayController(schema,$mdDialog,$q,$document) {
    var vm = this;
    vm.openPopup = openPopup;
    var daysErrors = {};
    getAllErrorsForDay(vm.date);
    
    var daysErrors = {};
    
    function getAllErrorsForDay(date){
      var errors = schema.getObject(schema.getRoot().child("errors").orderByChild(date));
      
      daysErrors = {};      
      errors.$loaded()
              .then(function (errors){
                var promises = [];
                angular.forEach(errors,function (panelInfo,key){
                  promises.push(getErrorImage(panelInfo,key,date));
                }); 
                return $q.all(promises);
              })
              .then(function (){
                vm.errorsLength = 0;
                
                angular.forEach(daysErrors[vm.date],function (data,key){
                  if(angular.isDefined(data.errorMsg)){
                    vm.errorsLength += 1;
                  }
                });
                vm.errors = daysErrors;
              });
    }

    
    function getErrorImage(panelInfo,key,date){
      
      daysErrors[vm.date] = {}; 
      
      var error = panelInfo.error;
      if (angular.isDefined(error)) {
        if (panelInfo.date === vm.date) {
          var image = getPanelImage(key,panelInfo.panel);
          
          return image
                  .then(function (url) {
                    daysErrors[vm.date][key] = {};
                    if (url.length >= 1) {
                      daysErrors[vm.date][key].imageUrl = url;
                    } 
                    return getPanelByKey(panelInfo.panel);
                  }).then(function (panel) {
                    daysErrors[vm.date][key].panelRef = panel.id;
                    if (angular.isDefined(error)) {
                      daysErrors[vm.date][key].errorMsg = error;
                    }
                    return $q.when(daysErrors);
                  });
        }
      }
    }
    
    function openPopup(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      // Modal dialogs should fully cover application
      // to prevent interaction outside of dialog
      var errors = vm.errors;
      $mdDialog.show({
        controller: DialogController,
        controllerAs: "vm",
        templateUrl: require("./errors.tmpl.html"),
        parent: angular.element($document[0].body),
        clickOutsideToClose: true,
        locals: {dataToPass: vm}
      });

      function DialogController($scope, dataToPass) {
        var vm = this;
        vm.date = dataToPass.date;
        vm.errors = dataToPass.errors;

        vm.hide = function () {
          $mdDialog.hide();
        };

        vm.cancel = function () {
          $mdDialog.cancel();
        };

        vm.answer = function (answer) {
          $mdDialog.hide(answer);
        };
      }
    }
    
    function getPanelImage(key,panelId) {


      var image = schema.getStorageRoot().child("images").child("panels").child(panelId).child(key);

      return getDownloadUrl(image);
                
                
             
    }  
  
    function getDownloadUrl(image){
      return image.getDownloadURL()
              .then(function (url) {
                return url;
              }).catch(function (error) {
                return false;
              });
    }
  
    function getPanelByKey(key){
      return schema.getObject(schema.getRoot().child("panels").child(key)).$loaded();
    }  

  }

})();


