(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("objectKeys", filter);

  function filter() {

    return objectKeysFilter;

    function objectKeysFilter(object) {
      return Object.keys(object);
    }
  }

})();
