(function () {
  "use strict";

  angular.module(
    "innView.core",
    [
      "firebase.database",
      "firebase.auth"
    ]);
})();
