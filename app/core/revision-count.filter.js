(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("revisionCount", filter);

  function filter() {

    return revisionCountFilter;

    ////////////

    function revisionCountFilter(count) {
      if (angular.isUndefined(count)) {
        return "-";
      }

      if (count == null) {
        return "";
      }
      
      var code = 65 + count;
      var returnString = String.fromCharCode(code);
      
      if (code > 90){
        code = code - 26;
        returnString = String.fromCharCode(65) + String.fromCharCode(code);
      }else if (code > 115){
        code = code - 52;
        returnString = String.fromCharCode(66) + String.fromCharCode(code);
      }else if (code > 140){
        code = code - 78;
        returnString = String.fromCharCode(67) + String.fromCharCode(code);
      }

      return returnString;
    }
  }

})();
