(function () {
  "use strict";

  angular
    .module("innView.core")
    .filter("operative", filter);

  function filter(schema) {
    var operativeNameCache = {};

    operativeNameFilter.$stateful = true;
    return operativeNameFilter;

    ////////////

    function operativeNameFilter(operativeId) {
      if (!operativeId) {
        return "";
      }

      return operativeNameCache[operativeId] || lookupName(operativeId);

      function lookupName(operativeId) {
        var operative = schema.getObject(schema.getRoot().child("operatives").child(operativeId));
        operative
          .$loaded()
          .then(function () {
            operativeNameCache[operativeId] = operative.name;
            operative.$destroy();
          });

        return operativeNameCache[operativeId] = "...";
      }
    }
  }

})();
