(function () {
  "use strict";

  angular
    .module("innView.core")
    .factory("usersService", usersService);

  /**
   * @ngInject
   */
  function usersService(schema) {
    var service = {
      getAllUsers: getAllUsers,
      getUserById: getUserById
    };

    return service;

    ////////////
    
    function getUserById(uid) {
      var user = schema
        .getRoot()
        .child("users")
        .child(uid);
      return schema.getObject(user);
    }
    
    function getAllUsers() {
      var users = schema
        .getRoot()
        .child("users");
      return schema.getArray(users);
    }
    
  }
})();
