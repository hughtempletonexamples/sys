(function () {
  "use strict";

  angular
    .module("innView.qa")
    .factory("prekitService", prekitService);

  /**
   * @ngInject
   */
  function prekitService($log, $q, firebaseUtil, operativesService, schema) {

    var service = {
      startPanel: startPanel,
      completePanel: completePanel,
      jobsheetsForProject: jobsheetsForProject,
      getJobsheets: getJobsheets,
      getExistingCuttingList: getExistingCuttingList
    };

    return service;

    ////////////

    function startPanel(jobsheetId) {
      var jobsheetArray;
      return getJobsheets(jobsheetId)
        .$loaded()
        .then(function (result) {
          jobsheetArray = result;
          var jobsheet = jobsheetArray[0];
          if (angular.isUndefined(jobsheet["started"])) {
            jobsheet["operatives"] = selectedOperatives();
            jobsheet["startedOperatives"] = selectedOperatives();
            jobsheet["started"] = firebaseUtil.serverTime();
            return jobsheetArray.$save(jobsheet);
          } else {
            return $q.when();
          }
        });
    }

    function completePanel(js) {
      var existingOperatives = js["operatives"] || {};
      var jobsheetArray;
      return getJobsheets(js.jobsheet)
        .$loaded()
        .then(function (result) {
          jobsheetArray = result;
          var jobsheet = jobsheetArray[0];
          if (angular.isUndefined(jobsheet["completed"])) {
            jobsheet["operatives"] = angular.extend({}, existingOperatives, selectedOperatives());
            jobsheet["completedOperatives"] = selectedOperatives(); 
            jobsheet["completed"] = firebaseUtil.serverTime();
            return jobsheetArray.$save(jobsheet);
          } else {
            return $q.when();
          }
        });
    }

    function selectedOperatives() {
      var map = {};
      angular.forEach(operativesService.selected(), function (id) {
        map[id] = true;
      });
      return map;
    }
    
    function getExistingCuttingList(js){
      var promises = [];
      angular.forEach(js.cuttingList, function (item, key) {
        var holder = getCuttingListByRef(key);
        promises.push(holder.$loaded());
      });
      return $q.all(promises);
    }
    
    function getCuttingListByRef(ref){
      var query = schema.getRoot().child("cuttingList").child(ref);
      return schema.getObject(query);
    }
    
    function getJobsheets(jobsheet) {
      var query = schema.getRoot().child("jobsheets").orderByChild("jobsheet").equalTo(jobsheet);
      var jobsheets = schema.getArray(query);
      return jobsheets;
    }
    
    function jobsheetsForProject(projectId) {
      var query = schema.getRoot().child("jobsheets").orderByChild("projectId").equalTo(projectId);
      var jobsheets = schema.getArray(query);
      return jobsheets;
    }
    
  }
})();
