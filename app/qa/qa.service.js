(function () {
  "use strict";

  angular
    .module("innView.qa")
    .factory("qaService", qaService);

  /**
   * @ngInject
   */
  function qaService($log, $q,$filter, referenceDataService, plannerService,schema, areasService, panelsService, operativesService, firebaseUtil) {

    var service = {
      getIncompleteAreasForProductionLine: getIncompleteAreasForProductionLine,
      startPanel: startPanel,
      finishAssembly: finishAssembly,
      completePanel: completePanel,
      undoPrevious:  undoPrevious,
      saveError:saveError
    };

    var initialised = false;

    var productionLineProductTypes = {};
    var productionPlans;

    return service;

    ////////////

    function initialise() {
      if (initialised) {
        return $q.resolve();
      } else {
        return init();
      }

      function init() {
        return referenceDataService.getProductTypes()
          .$loaded()
          .then(function (productTypes) {
            angular.forEach(productTypes, function (productType) {
              if (angular.isDefined(productionLineProductTypes[productType.productionLine])) {
                productionLineProductTypes[productType.productionLine].push(productType.$id);
              } else {
                productionLineProductTypes[productType.productionLine] = [productType.$id];
              }
            });
            return $q.when();
          })
          .then(function () {
            initialised = true;
          });
      }
    }
    
    function saveError(panelId,error,imageId){
      var panelsErrors = schema.getObject(schema.getRoot().child("errors"));

      return panelsErrors.$loaded()
          .then(function (errorsList){
            var data = {};
            var date = $filter("date")(new Date(), "yyyy-MM-dd");
            
            if (angular.isUndefined(errorsList)) {
              errorsList = {};
            }
            var data = {
              "date": date,
              "panel": panelId,
              "error": error
            };
            panelsErrors[imageId] = data;
            return panelsErrors.$save();
          });
      
    }
  

    function getIncompleteAreasForProductionLine(productionLine) {
      return initialise()
        .then(function () {
          var applicableProductTypes = productionLineProductTypes[productionLine] ? productionLineProductTypes[productionLine] : [] ;
          var incompleteAreas = [];

          return plannerService.getProductionPlansByPlannedStart()
            .$loaded()
            .then(function (plans) {

              var promises = [];

              angular.forEach(plans, function (plan) {
                promises.push(
                  areasService.getProjectArea(plan.$id)
                    .$loaded()
                    .then(function (area) {
                      var completedPanels = area.completedPanels || 0;
                      var actualPanels = area.actualPanels || 0;
                      if (isApplicableType(area.type) && completedPanels < actualPanels) {
                        incompleteAreas.push(area);
                      }
                    })
                );
              });

              return $q.all(promises);
            })
            .then(function () {
              return incompleteAreas;
            })
            .catch(function (error) {
              $log.log("Unable to process incomplete areas: " + angular.toJson(error));
            });

          function isApplicableType(type) {
            return applicableProductTypes.indexOf(type) != -1;
          }
        });
    }

    function startPanel(panelId) {
      var panelArray;
      return panelsService.getPanelById(panelId)
        .$loaded()
        .then(function (result) {
          panelArray = result;

          var panel = result[0];
          if (angular.isUndefined(panel.qa)) {
            panel.qa = {};
          }
          if (angular.isUndefined(panel.qa["started"])) {
            panel.qa["started"] = firebaseUtil.serverTime();
            panel.qa["operatives"] = selectedOperatives();
            panel.qa["startedOperatives"] = selectedOperatives();
            return panelArray.$save(panel);
          } else {
            return $q.when();
          }
        });
    }
    
    function finishAssembly(panelId) {
      var panelArray;

      return panelsService.getPanelById(panelId)
        .$loaded()
        .then(function (result) {
          panelArray = result;

          var panel = result[0];
          
          if (angular.isUndefined(panel.qa["finishAssembly"])) {
            panel.qa["finishAssembly"] = firebaseUtil.serverTime();
            panel.qa["operatives"] = angular.extend({}, panel.qa["operatives"], selectedOperatives());
            panel.qa["finishAssemblyOperatives"] = selectedOperatives();
            return panelArray.$save(panel);
          } else {
            return $q.when();
          }
        });
    }
    
    function undoPrevious(panelId) {
      var panelArray;

      return panelsService.getPanelById(panelId)
        .$loaded()
        .then(function (result) {
          panelArray = result;

          var panel = result[0];

          if (angular.isDefined(panel.qa.finishAssembly)) {
            delete panel.qa["finishAssembly"];
            delete panel.qa["finishAssemblyOperatives"];
            return panelArray.$save(panel);
          } else{
            return $q.when();
          }

        });
    }
    
    function completePanel(updatedPanel) {
      var existingOperatives = updatedPanel.qa["operatives"] || {};
      updatedPanel.qa["operatives"] = angular.extend({}, existingOperatives, selectedOperatives());
      updatedPanel.qa["completedOperatives"] = selectedOperatives();

      panelsService.getPanelById(updatedPanel.id)
        .$loaded()
        .then(function (result) {
          var firebasePanel = result[0];
          angular.extend(firebasePanel, updatedPanel);
          return result.$save(firebasePanel);
        })
        .then(function () {
          panelsService.markPanelsComplete(updatedPanel.area, [updatedPanel.$id]);
        })
        .then(function () {
          return $q.resolve(true);
        })
        .catch(function (error) {
          return $q.reject(error);
        });
    }

    function selectedOperatives() {
      var map = {};
      angular.forEach(operativesService.selected(), function (id) {
        map[id] = true;
      });
      return map;
    }

  }
})();
