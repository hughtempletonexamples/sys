(function () {
  "use strict";

  angular
    .module("innView.qa")
    .component("qaInnNavbar", {
      templateUrl: require("./qaNavBar.html"),
      controller: navBarController,
      controllerAs: "vm",
      transclude: true
    });

  /**
   * @ngInject
   */
  function navBarController(schema, operativesService,$scope) {
    var vm = this;

    vm.status = schema.status;
    vm.operatives = getOperatives;
    vm.selected = operativesService.selected();

    vm.showSelectPopup = false;
    vm.requestPin = requestPin;
    vm.selectOperatives = selectOperatives;
    vm.isSelected = isSelected;
    vm.toggleSelection = toggleSelection;
    vm.digitEntered = digitEntered;
    vm.done = done;
    vm.cancel = cancel;

    var availableOperatives;
    var selectedOperatives = new Map();

    ////////////

    function digitEntered(newValue){
      if (String(newValue).length === 4) {
        setIfPinCorct(vm.selectedOpp, vm.pin);
        vm.pin = "";
      }
    }
    
    function getOperatives() {
      if (!availableOperatives) {
        availableOperatives = operativesService.list();
      }
      return availableOperatives;
    }

    function selectOperatives() {
      selectedOperatives.clear();
      angular.forEach(vm.selected, function (operativeId) {
        selectedOperatives.set(operativeId, true);
      });
      vm.showSelectPopup = true;
    }

    function isSelected(operativeId) {
      return selectedOperatives.get(operativeId);
    }
    
    function requestPin(operative){     
      if(isSelected(operative.$id)){
        vm.showPinPopup = false;
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
      }else{
        vm.showPinPopup = !vm.showPinPopup;
        var operativesList = getOperatives();
        angular.forEach(operativesList, function (data, key) {
          if (angular.isDefined(data.$id) && data.$id === operative.$id) {
            vm.selectedOpp = operative;
          }
        });
      }
    }
    
    function setIfPinCorct(operative,enteredPin){
      vm.errMssg = [];
      vm.errMssg[operative.$id] = undefined;
      if(operative.pin === parseInt(enteredPin)){
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
        vm.errMssg[operative.$id] = false;
        vm.showPinPopup = !vm.showPinPopup;          
      }else{
        vm.errMssg[operative.$id] = true;
      } 
    }    

    function toggleSelection(operativeId) {
      selectedOperatives.set(operativeId, !isSelected(operativeId));
    }

    function done() {
      var operatives = [];
      angular.forEach(availableOperatives, function (operative) {
        if (selectedOperatives.get(operative.$id)) {
          operatives.push(operative.$id);
        }
      });
      operativesService.select(operatives);
      vm.showSelectPopup = false;
    }

    function cancel() {
      vm.showSelectPopup = false;
    }
  }

})();
