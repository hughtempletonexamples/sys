(function () {
  "use strict";

  angular
    .module("innView.qa")
    .config(initRoutes);

  /**
   * @ngInject
   */
  function initRoutes($routeProvider) {
    $routeProvider
      .when("/", {
        templateUrl: require("./qa.html"),
        controller: "QAController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/selectProductionLine", {
        templateUrl: require("./selectProductionLine.html"),
        controller: "QAController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/selectJobsheet", {
        templateUrl: require("./selectJobsheet.html"),
        controller: "PrekitController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/:productionLine", {
        templateUrl: require("./selectPanel.html"),
        controller: "QAController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/selectJobsheet/:jobsheetId", {
        templateUrl: require("./prekitQA.html"),
        controller: "PrekitController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      })
      .when("/:productionLine/:panelId", {
        templateUrl: require("./panelQA.html"),
        controller: "QAController",
        controllerAs: "vm",
        resolve: {
          auth: requireAuth
        }
      });
  }

  /**
   * @ngInject
   */
  function requireAuth(schema) {
    return schema.requireAuth("production");
  }

})();
