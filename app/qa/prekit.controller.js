(function () {
  "use strict";

  angular
    .module("innView.qa")
    .controller("PrekitController", PrekitController);

  /**
   * @ngInject
   */
  function PrekitController($log, $location, $routeParams, $scope, $interval, $confirmModalDefaults, dateUtil, prekitService, projectsService, operativesService, schema) {
    var vm = this;

    vm.selectProject = selectProject;
    vm.startPanel = startPanel;
    vm.saveTotal = saveTotal;
    vm.save = save;
    vm.goBack = goBack;
    
    vm.whichHTML = "selectProductionLine";
    vm.jobsheetQA = false;
    vm.selectJobsheet = true;
    vm.lengthOfs = [];
    vm.lengthOfs["fives"] = "5.4";
    vm.lengthOfs["fours"] = "4.8";
    vm.lengthOfs["threes"] = "3.6";
    vm.projects = [];
    vm.selectedProject = null;
    
    //operatives popup
    vm.status = schema.status;
    vm.operatives = getOperatives;
    vm.selected = operativesService.selected();
    vm.showSelectPopup = false;
    vm.selectOperatives = selectOperatives;
    vm.isSelected = isSelected;
    vm.toggleSelection = toggleSelection;
    vm.setIfPinCorct = setIfPinCorct;
    vm.digitEntered = digitEntered;
    vm.done = done;
    vm.cancel = cancel;
    vm.requestPin = requestPin;
    var availableOperatives;
    var selectedOperatives = new Map();
    var projectsList =[];

    init();

    ////////////

    function init() {
      if ($routeParams.jobsheetId) {
        $confirmModalDefaults.templateUrl = require("../core/confirm-dialog.html");
        vm.productionLine = $routeParams.productionLine;
        prekitService.getJobsheets($routeParams.jobsheetId)
        .$loaded()
        .then(function (jobsheetArray) {
          vm.jobsheet = jobsheetArray[0];
          return prekitService.getExistingCuttingList(jobsheetArray[0]);
        })
        .then(function (result) {
          vm.cuttingList = result;
        })
        .then(function () {
          vm.jobsheetQA = true;
          vm.selectJobsheet = false;
          vm.duration = 0;
          if (angular.isUndefined(vm.durationInterval)) {
            if (angular.isUndefined(vm.jobsheet["completed"])) {
              vm.duration = calculateElapsedTime(vm.jobsheet["started"]);
              initDurationInterval();
            } else {
              vm.duration = calculateElapsedTime(vm.jobsheet["started"], vm.jobsheet["completed"]);
            }
          }
        });
      } else {
        projectsList = projectsService.getProjects();
        projectsList
          .$loaded()
          .then(function (projects) {
            projects.sort(compareById);
            projects.reverse();
            vm.projects = projects;  
          })
          .then(function () {
            if (angular.isDefined($routeParams.selectProject)) {
              selectProject($routeParams.selectProject);
            }
          })
          .catch(function (error) {
            $log.log("Unable to load incompleteAreas: " + angular.toJson(error));
          });
      }

      $scope.$on("destroy", cleanup);

      function initDurationInterval() {
        vm.durationInterval = $interval(
          function () {
            vm.duration = calculateElapsedTime(vm.jobsheet["started"]);
          },
          1000
        );
      }

      function calculateElapsedTime(from, to) {
        if (angular.isUndefined(to)) {
          to = new Date();
        }
        return to - dateUtil.asDate(from);
      }
    }
    
    
    function digitEntered(newValue){
      if (String(newValue).length === 4) {
        setIfPinCorct(vm.selectedOpp, vm.pin);
        vm.pin = "";
      }
    }   

    function cleanup() {
      if (angular.isDefined(vm.durationInterval)) {
        $interval.cancel(vm.durationInterval);
        vm.durationInterval = undefined;
      }
    }
    
    function compareById(a, b) {
      if (a.projectId < b.projectId) {
        return -1;
      } else if (a.id > b.id) {
        return 1;
      } else {
        return 0;
      }
    }

    function compareByJobsheetNo(a, b) {
      var aIdParts = a.jobsheet.split("-");
      var bIdParts = b.jobsheet.split("-");
      if (aIdParts[5] < bIdParts[5]) {
        return -1;
      } else if (aIdParts[5] > bIdParts[5]) {
        return 1;
      } else {
        return 0;
      }
    }

    function selectProject(projectKey) {
      vm.selectedProject = projectKey;
      prekitService.jobsheetsForProject(projectKey)
        .$loaded()
        .then(function (jobsheets) {
          jobsheets.sort(compareByJobsheetNo);
          vm.jobsheets = jobsheets;
        });
    }

    function startPanel(jobsheetId) {
      vm.jobsheetId = jobsheetId;
      if (operativesService.selected().length > 0) {
        prekitService.startPanel(jobsheetId)
          .then(function () {
            $location.url("/selectJobsheet/" + jobsheetId);
          });
      } else {
        vm.selectOperatives();
      }
    }

    function save(valid) {
      if (valid) {
        if (operativesService.selected().length > 0) {
          prekitService.completePanel(vm.jobsheet);
          goBack();
        } else {
          vm.selectOperatives();
        }
      }
    }

    function goBack() {
      var url = "/selectJobsheet";
      if (angular.isDefined(vm.jobsheet)){
        url = url + "?selectProject=" + vm.jobsheet.projectId;
      }
      $location.url(url);
    }
    
    function saveTotal(list){
      if (angular.isDefined(list)){
        list.cuttingDetails.actual = (((5.4 * list.lengths.fives || 0) + (4.8 * list.lengths.fours || 0) + (3.6 * list.lengths.threes || 0)) * 1000);
        list.$save();
      }
    }
    
    //operatives popup
    function getOperatives() {
      if (!availableOperatives) {
        availableOperatives = operativesService.list();
      }
      return availableOperatives;
    }

    function selectOperatives() {
      selectedOperatives.clear();
      angular.forEach(vm.selected, function (operativeId) {
        selectedOperatives.set(operativeId, true);
      });
      vm.showSelectPopup = true;
    }

    function isSelected(operativeId) {
      
      selectedOperatives.get(operativeId);
      if(selectedOperatives[operativeId] === true){
        vm.color = "green";
        return selectedOperatives.get(operativeId);
      }else{
        vm.color = "white";
        return selectedOperatives.get(operativeId);
      }
    }
    
    function requestPin(operative){
      
      if(vm.isSelected(operative.$id)){
        vm.showPinPopup = false;
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
      }else{
        vm.showPinPopup = !vm.showPinPopup;
        var operativesList = getOperatives();
        angular.forEach(operativesList, function (data, key) {
          if (angular.isDefined(data.$id) && data.$id === operative.$id) {
            var operativePin = data.pin;
            vm.selectedOpp = operative;
          }
        });
      }
    }
    
    function setIfPinCorct(operative,enteredPin){
      vm.errMssg = [];
      vm.errMssg[operative.$id] = undefined;
      if(operative.pin === parseInt(enteredPin)){
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
        vm.errMssg[operative.$id] = false;
        vm.showPinPopup = !vm.showPinPopup;          
      }else{
        vm.errMssg[operative.$id] = true;
      }
      
    }

    function toggleSelection(operativeId) {
      selectedOperatives.set(operativeId, !isSelected(operativeId));
    }

    function done() {
      var operatives = [];
      angular.forEach(availableOperatives, function (operative) {
        if (selectedOperatives.get(operative.$id)) {
          operatives.push(operative.$id);
        }
      });

      operativesService.select(operatives);

      vm.showSelectPopup = false;

      if (operatives.length > 0) {
        if (vm.whichHTML === "prekitQA") {
          prekitService.completePanel(vm.jobsheet);
          goBack();
        } else if (vm.whichHTML === "selectJobsheet" && vm.jobsheetId) {
          prekitService.startPanel(vm.jobsheetId)
            .then(function () {
              $location.url("/selectJobsheet/" + vm.jobsheetId);
            });
        }
      }
    }

    function cancel() {
      vm.showSelectPopup = false;
    }
  }
})();
