(function () {
  "use strict";

  angular
    .module("innView.qa")
    .factory("operativesService", operativesService);

  /**
   * @ngInject
   */
  function operativesService(schema) {
    var selectedOperatives = [];

    var service = {
      list: getAvailableOperatives,
      select: setSelectedOperatives,
      selected: getSelectedOperatives
    };

    return service;

    ////////////

    function getAvailableOperatives() {
      return schema.getArray(schema.getRoot()
                             .child("operatives")
                             .orderByChild("active")
                             .equalTo(true));
    }

    function setSelectedOperatives(operatives) {
      selectedOperatives.length = 0;
      operatives.forEach(function (operative) {
        selectedOperatives.push(operative);
      });
    }

    function getSelectedOperatives() {
      return selectedOperatives;
    }

  }

})();
