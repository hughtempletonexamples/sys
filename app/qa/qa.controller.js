(function () {
  "use strict";

  angular
    .module("innView.qa")
    .controller("QAController", QAController);

  /**
   * @ngInject
   */
  function QAController($log, $location, $routeParams, $scope,audit, $interval, $confirmModalDefaults, dateUtil, qaService, panelsService, operativesService, schema) {
    var vm = this;

    vm.selectArea = selectArea;
    vm.selectJobSheet = selectJobSheet;
    vm.startPanel = startPanel;
    vm.finishAssembly = finishAssembly;
    vm.undoPrevious = undoPrevious;
    vm.requestPin = requestPin;
    vm.setIfPinCorct = setIfPinCorct;
    vm.save = save;
    vm.getValidation = getValidation;
    vm.goBack = goBack;
    vm.whichHTML = "selectProductionLine";
    vm.panelId = "";
    vm.showErrors = false;
    vm.confirmErrors = false;
    vm.areas = [];
    vm.panels = [];
    vm.panelsFiltered = [];
    vm.jobSheetsCount = 0;
    vm.colSize = "col-xs-9";
    
    //operatives popup
    vm.status = schema.status;
    vm.operatives = getOperatives;
    vm.selected = operativesService.selected();
    vm.showSelectPopup = false;
    vm.selectOperatives = selectOperatives;
    vm.isSelected = isSelected;
    vm.digitEntered = digitEntered;
    vm.toggleSelection = toggleSelection;
    vm.done = done;
    vm.cancel = cancel;
    vm.saveErrors = saveErrors;
    var availableOperatives;
    var selectedOperatives = new Map();
    vm.errorSuccess = false;
    init();

    vm.errorsList = {
      "error1":"Posnums on material not matching DFM Drawings",
      "error2":"Material Cut Incorrectly (Missing/Undersized/Oversized)",
      "error3":"Material Poor Quality (Timber - Twisted/Knots. OSB - Flaky Edges/Bubbled)",
      "error4":"Panel out of Square",
      "error5":"Failed Rip Off Test",
      "error6":"Insufficient detail on DFM drawing",
      "error7":"Dimensional Information missing (Diagonals/Stud Offset/Length/Width)",
      "error8":"Inefficient Use of Board (OSB/MP - Excessive Cuts/Splits)",
      "error9":"Inefficient Use of Timber (C24/LVL/Glulam)",
      "error10":"Panel scrapped due to Revised Panel Issued"
    };
    
   
    
    
    ////////////
    function digitEntered(newValue) {
      if (String(newValue).length === 4) {
        setIfPinCorct(vm.selectedOpp, vm.pin);
        vm.pin = "";
      }
    }
    function auditCreateArea(projectId) {
      var id = vm.panel.id;
      return audit.panel("panel QA changed " + id ,"/#/manage/status/"+ projectId +"/"+ vm.panel.area, vm.productionLine,projectId);
    }
    
    function saveErrors(error){
      var panelId = vm.panel.$id;
      if(error !== null && angular.isDefined(panelId)){
        var data = qaService.saveError(panelId,error);
        vm.errorSuccess = true;
      }
    }

    function init() {
      if ($routeParams.panelId) {
        $confirmModalDefaults.templateUrl = require("../core/confirm-dialog.html");
        vm.productionLine = $routeParams.productionLine;
        panelsService.getPanelById($routeParams.panelId)
          .$loaded()
          .then(function (panel) {
            vm.panel = panel[0];
            vm.duration = 0;
            if (angular.isUndefined(vm.durationInterval)) {

              if (angular.isDefined(vm.panel.qa) && angular.isUndefined(vm.panel.qa["completed"])) {
                vm.duration = calculateElapsedTime(vm.panel.qa["started"]);
                initDurationInterval();
              } else {
                vm.duration = calculateElapsedTime(vm.panel.qa["started"], vm.panel.qa["completed"]);
              }
            }
            return panel[0];
          })
          .then(function (panel){
            return getProject(panel.project);
          })
          .then(function (project){
            auditCreateArea(project.id);
          });
      } else if ($routeParams.productionLine) {
        vm.productionLine = $routeParams.productionLine;
        qaService.getIncompleteAreasForProductionLine($routeParams.productionLine)
          .then(function (incompleteAreas) {
            vm.areas = incompleteAreas;
          })
          .then(function () {
            if (angular.isDefined($routeParams.selectedArea)) {
              selectArea($routeParams.selectedArea);
            }
          })
          .then(function () {
            if (angular.isDefined($routeParams.selectedJobSheet)) {
              selectJobSheet($routeParams.selectedJobSheet);
            }
          })
          .catch(function (error) {
            $log.log("Unable to load incompleteAreas: " + angular.toJson(error));
          });
      }

      $scope.$on("destroy", cleanup);

      function initDurationInterval() {
        vm.durationInterval = $interval(
          function () {
            vm.duration = calculateElapsedTime(vm.panel.qa["started"]);
          },
          1000
        );
      }
      
      function getProject(projectId) {
        return schema.getObject(schema.getRoot().child("projects").child(projectId)).$loaded()
            .then(function (project){
              return project;
            });
      }

      function calculateElapsedTime(from, to) {
        if (angular.isUndefined(to)) {
          to = new Date();
        }
        return to - dateUtil.asDate(from);
      }
    }

    function cleanup() {
      if (angular.isDefined(vm.durationInterval)) {
        $interval.cancel(vm.durationInterval);
        vm.durationInterval = undefined;
      }
    }
    
    function getValidation(value, range){
      return value + range;
    }

    function selectArea(areaKey) {
      var count = 0;
      vm.selectedArea = areaKey;
      panelsService
        .panelsForArea(areaKey)
        .$loaded()
        .then(function (panels) {

          angular.forEach(panels, function (panel) {
            if (panel.jobSheet) {
              count++;
            }
          });

          vm.jobSheetsCount = count;
          if (vm.jobSheetsCount) {
            vm.colSize = "col-xs-6";
          }else{
            vm.colSize = "col-xs-9";
          }

          vm.panels = panels.sort(panelsService.comparator);
          vm.panelsFiltered = panels.sort(panelsService.comparator);
        });
    }

    function selectJobSheet(jobSheet) {
      var panelsFiltered = [];
      jobSheet = parseInt(jobSheet);
      vm.selectedJobSheet = jobSheet;
      panelsService
        .panelsForArea(vm.selectedArea)
        .$loaded()
        .then(function (panels) {
          var sortedPanels = panels.sort(panelsService.comparator)
          angular.forEach(sortedPanels, function (panel) {
            if(panel.jobSheet && panel.jobSheet === jobSheet){
              panelsFiltered.push(panel);
            }
          });
          vm.panelsFiltered = panelsFiltered;
        });
    }
    
    function undoPrevious(panelId){
      qaService.undoPrevious(panelId);
    }

    function startPanel(panelId) {
      vm.panelId = panelId;
      if (operativesService.selected().length > 0) {
        qaService.startPanel(panelId)
          .then(function () {
            $location.url("/" + $routeParams.productionLine + "/" + panelId);
          });
      } else {
        vm.selectOperatives();
      }
    }
    
    function finishAssembly(panelId) {
      vm.panelId = panelId;
      if (operativesService.selected().length > 0) {
        qaService.finishAssembly(panelId);
      } else {
        vm.selectOperatives();
      }
    }

    function save(valid) {
      if (valid) {
        if (operativesService.selected().length > 0) {
          qaService.completePanel(vm.panel);
          goBack();
        } else {
          vm.selectOperatives();
        }
      }
    }

    function goBack() {
      var url = "/" + $routeParams.productionLine;
      if (angular.isDefined(vm.panel)){
        url = url + "?selectedArea=" + vm.panel.area + "&selectedJobSheet=" + vm.panel.jobSheet;
      }
      $location.url(url);
    }
    
    //operatives popup
    function getOperatives() {
      if (!availableOperatives) {
        availableOperatives = operativesService.list();
      }
      return availableOperatives;
    }

    function selectOperatives() {
      selectedOperatives.clear();
      angular.forEach(vm.selected, function (operativeId) {
        selectedOperatives.set(operativeId, true);
      });
      vm.showSelectPopup = true;
    }

    function isSelected(operativeId) {
      
      selectedOperatives.get(operativeId);
      if(selectedOperatives[operativeId] === true){
        vm.color = "green";
        return selectedOperatives.get(operativeId);
      }else{
        vm.color = "white";
        return selectedOperatives.get(operativeId);
      }
    }
    
    function requestPin(operative){
      
      if(vm.isSelected(operative.$id)){
        vm.showPinPopup = false;
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
      }else{
        vm.showPinPopup = !vm.showPinPopup;
        var operativesList = getOperatives();
        angular.forEach(operativesList, function (data, key) {
          if (angular.isDefined(data.$id) && data.$id === operative.$id) {
            var operativePin = data.pin;
            vm.selectedOpp = operative;
          }
        });
      }
    }
    
    function setIfPinCorct(operative,enteredPin){
      vm.errMssg = [];
      vm.errMssg[operative.$id] = undefined;
      if(operative.pin === parseInt(enteredPin)){
        selectedOperatives.set(operative.$id, !isSelected(operative.$id));
        vm.errMssg[operative.$id] = false;
        vm.showPinPopup = !vm.showPinPopup;          
      }else{
        vm.errMssg[operative.$id] = true;
      }
      
    }

    function isSelected(operativeId) {
      return selectedOperatives.get(operativeId);
    }

    function toggleSelection(operativeId) {
      selectedOperatives.set(operativeId, !isSelected(operativeId));
    }

    function done() {
      var operatives = [];
      angular.forEach(availableOperatives, function (operative) {
        if (selectedOperatives.get(operative.$id)) {
          operatives.push(operative.$id);
        }
      });

      operativesService.select(operatives);

      vm.showSelectPopup = false;

      if (operatives.length > 0) {
        if (vm.whichHTML === "panelQA") {
          qaService.completePanel(vm.panel);
          goBack();
        } else if (vm.whichHTML === "selectPanel" && vm.panelId) {
          qaService.startPanel(vm.panelId)
            .then(function () {
              $location.url("/" + $routeParams.productionLine + "/" + vm.panelId);
            });
        }
      }
    }

    function cancel() {
      vm.showSelectPopup = false;
    }
  }
})();
