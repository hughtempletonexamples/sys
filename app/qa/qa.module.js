(function () {
  "use strict";

  angular
    .module("innView.qa", [
      "innView.core",
      "innView.projects",
      "innView.manage"
    ]);
})();
