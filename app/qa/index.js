(function () {
  "use strict";

  require("./qa.module");
  require("./qa.controller");
  require("./qa.service");
  require("./prekit.controller");
  require("./prekit.service");
  require("./operatives.service");
  require("./qanavbar.component");
  require("./config.route");
  require("./job-sheet.filter");


})();
