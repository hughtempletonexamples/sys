/**
 * This is a terribly shoddy script to help sort out the data issues in INN-145
 *
 * The basic problem is that for some reason, when loading panels,
 * some panels were mis-named, duplicating the names of other areas.
 *
 * The purpose of this script it to identify those duplicates (so they
 * can be deleted) then create a csv of the missing data so it can be
 * uploaded to the system
 *
 * The json files were obtained by using the Firebase REST API to
 * query the panels.
 *
 * The actual panels were obtained with this query:
 * https://innview.firebaseio.com/panels.json?orderBy=%22area%22&equalTo=%22-KI8PvPT3djZ9VfU1mLk%22&auth=iNyOWPIRi8mlNzlm3SzMbupiVw23GCr2q2jkWcUy&print=pretty
 *
 * And the expected panels were obtained by converting the original
 * excel spreadsheets to json.
 *
 */
(function () {
  "use strict";

  var actualPanels = require("./panels.json");
  var expectedPanels = require("./expected_panels_full.json").panels;

  var processedPanels = identifyDuplicates(actualPanels);

  var missingPanels = identifyMissingPanels(processedPanels.knownPanels, expectedPanels);

  processedPanels.duplicates.sort();

  printDuplicatePanels(processedPanels.duplicates);

  printMissingPanels(missingPanels);

  ////////////

  function identifyDuplicates(actualPanels) {
    var knownPanels = {};
    var duplicates = [];

    var panelKeys = Object.keys(actualPanels);

    var currentPanelId;
    panelKeys.forEach(function (panelKey) {
      currentPanelId = actualPanels[panelKey].id;
      if (knownPanels[currentPanelId]) {
        if(knownPanels[currentPanelId].qa) {
          duplicates.push({id: currentPanelId, key: panelKey});
        } else if (actualPanels[panelKey]["qa"] != null) {
          duplicates.push({id: currentPanelId, key: knownPanels[currentPanelId].key});
        } else {
          duplicates.push({id: currentPanelId, key: panelKey});
        }
      } else {
        knownPanels[currentPanelId] = { key: panelKey, hasQA: actualPanels[panelKey]["qa"] != null };
      }
    });

    return {
      duplicates: duplicates,
      knownPanels: knownPanels
    };
  }

  function identifyMissingPanels(knownPanels, expectedPanels) {
    var missing = [];
    expectedPanels.forEach(function (panel) {
      if(!knownPanels[panel["Panel Ref"]]) {
        missing.push(panel);
      }
    });
    return missing;
  }

  function printDuplicatePanels(duplicates) {
    /*eslint-disable*/

    console.log("Duplicate panels: ");
    for(var i = 0; i < duplicates.length; i++) {
      //console.log(duplicates[i].id + ": " + "https://innview.firebaseio.com/panels/" + duplicates[i].key);
      console.log("curl -X DELETE 'https://innview.firebaseio.com/panels/" + duplicates[i].key + ".json?auth=iNyOWPIRi8mlNzlm3SzMbupiVw23GCr2q2jkWcUy'");
    }
    console.log("Total: " + duplicates.length);
    console.log("");
    /*eslint-enable*/
  }

  function printMissingPanels(missingPanels) {
    /*eslint-disable*/
    console.log("Missing panels: ");
    // poor man's csv output
    console.log("Panel Ref" + "," + "Type" + "," + "Framing Style" + "," + "Length" + "," + "Height" + "," + "Area" + "," + "Depth" + "," + "Stud Size" + "," + "Sheathing" + "," + "Components" + "," + "Nealing" + "," + "Spandrel" + "," + "Doors" + "," + "Windows" + "," + "Pockets" + "," + "Weight" + "," + "Qty");
    for(var j = 0; j < missingPanels.length; j++) {
      //console.log(missingPanels[j]);
      console.log(missingPanels[j]["Panel Ref"] + "," +
                  missingPanels[j]["Type"] + "," +
                  missingPanels[j]["Framing Style"] + "," +
                  missingPanels[j]["Length"] + "," +
                  missingPanels[j]["Height"] + "," +
                  missingPanels[j]["Area"] + "," +
                  missingPanels[j]["Depth"] + "," +
                  missingPanels[j]["Stud Size"] + "," +
                  (missingPanels[j]["Sheathing"] ? missingPanels[j]["Sheathing"] : "") + "," +
                  missingPanels[j]["Components"] + "," +
                  (missingPanels[j]["Nailing"] ? missingPanels[j]["Nailing"] : "") + "," +
                  missingPanels[j]["Spandrel"] + "," +
                  missingPanels[j]["Doors"] + "," +
                  missingPanels[j]["Windows"] + "," +
                  missingPanels[j]["Pockets"] + "," +
                  missingPanels[j]["Weight"] + "," +
                  missingPanels[j]["Qty"]);
    }
    /*eslint-enable*/
  }
})();
